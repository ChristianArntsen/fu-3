<?php
$status = 'Clocked Out';
$table_html = '';
$table_html .= "<table id='time_shift_history' class='hidden'>
<thead><tr><td>Time In</td><td>Time Out</td><td>Total</td></tr></thead>
<tbody id='time_table'>";
foreach($last_entries as $entry)
{
	$table_html .= '<tr><td>'.$entry['shift_start'].'</td><td>'.($entry['shift_end']=='0000-00-00 00:00:00'?'':$entry['shift_end']).'</td><td>'.$entry['total'].'</td>';
	if ($entry['shift_end'] == '0000-00-00 00:00:00') $status = "Clocked In @ ".$entry['shift_start'];
}
$table_html .= "</tbody></table>";


?>
<div id='current_status'>
	<?=$status?>
</div>
<div id='clock_button_holder'></div>
<div id='clock_button_holder'>
	<a id='show_history' href='javascript:timeclock.toggle_history()'>Show history</a>
</div>
<?=$table_html?>
<style>
	#current_status {
		font-size: 20px;
		text-align: center;
		padding: 25px;
	}
	#clock_in, #clock_out {
		color: white;
		padding: 10px 25px;
		background: #369;
		border-radius: 4px;
	}
	#clock_button_holder, #show_history_holder {
		text-align:center;
		height:30px;
	}
	#show_history {
		font-size:12px;
	}
	.hidden {
		display:none;
	}
</style>
<script>
	var timeclock = {
		clocked_in:<?php echo ($clocked_in)?'true':'false';?>,
		initialize:function() {
			this.build_link();
		},
		toggle_history:function() {
			console.log('in here');
			var hidden = $('#time_shift_history').hasClass('hidden');
			console.log('hidden '+hidden);
			if (hidden) 
			{
				$('#time_shift_history').removeClass('hidden');
				$('#show_history').html('Hide history');
			}
			else
			{
				$('#time_shift_history').addClass('hidden');
				$('#show_history').html('Show history');
			}
			$.colorbox.resize();
		},
		build_link:function() {
			var link_html = '';
			if (this.clocked_in)
				link_html = "<a id='clock_in' href='javascript:timeclock.clock_out()'>Clock Out</a>";
			else
				link_html = "<a id='clock_out' href='javascript:timeclock.clock_in()'>Clock In</a>";
			$('#clock_button_holder').html(link_html);
		},
		build_time_table:function(data) {
			var table_html = '';
			var	status_html = "Clocked Out";
			for (var i in data) 
			{
				var line = data[i];
				table_html += "<tr><td>"+line.shift_start+"</td><td>"+(line.shift_end=='0000-00-00 00:00:00'?'':line.shift_end)+"</td><td>"+(line.total==null?'':line.total)+"</td></tr>";
				if (line.shift_end == '0000-00-00 00:00:00')
					status_html = "Clocked In @ "+line.shift_start;
			}
			$('#current_status').html(status_html);
			$('#time_table').html(table_html);
		},
		clock_in:function() {
			$.ajax({
		       type: "POST",
		       url: "index.php/timeclock/clock_in",
		       data:"",
		       dataType:'json',
		       success: function(response){
					timeclock.clocked_in = true;
					timeclock.build_link();
					timeclock.build_time_table(response);
		       }
		     });
		},
		clock_out:function() {
			$.ajax({
		       type: "POST",
		       url: "index.php/timeclock/clock_out",
		       data:"",
		       dataType:'json',
		       success: function(response){
					timeclock.clocked_in = false;
					timeclock.build_link();
					timeclock.build_time_table(response);
		       }
		     });
		}
	};
$(document).ready(function() {
	timeclock.initialize();
});

</script>
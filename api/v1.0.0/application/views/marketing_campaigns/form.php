<script type="text/javascript" src="<?php echo base_url();?>js/nic_edit.js"/>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.ajaxupload.css" />
<style type="text/css">
  /* css overrides*/
  .label-override {
      width: 120px !important;
  }
  .radio-override{
    float:right;
    text-align: left;
    margin-right: 45px;
    margin-top: -5px;
  }
  .campaign-groups{
    list-style: none;
  }
  .email_recipient_count {

  }
  .text_recipient_count {

  }
  #campaign_send_date {
  	width:130px;
  	margin-right:15px;
  }
  #campaign-form-left{width:385px;float:left;}
  #campaign-form-right{width:600px;}
  .wysiwyg-title-iframe{height: 52px !important;}
  .wysiwyg-contents-iframe{min-height: 150px !important;height: 150px !important;}
  #campaign_header,#campaign_title,#campaign_content{width:353px !important;}
  #campaign_content{min-height:150px !important;}
  .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
  .ui-timepicker-div dl { text-align: left; }
  .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
  .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
  .ui-timepicker-div td { font-size: 90%; }
  .ui-datepicker .ui-datepicker-buttonpane button{padding-top:0px !important; padding-bottom: 1.5em !important;}
  .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
  #text-counter{font-size:11px; color:#67696b;}
  .label-override-content {
      width: 290px !important;
  }
  #required_fields_message{color:red;}
  .hidden{visibility: hidden;}

	img.editable:hover {
		border: 1px solid red;
		cursor: pointer;
		margin-right: -1px;
		margin-top: -1px;
	}
</style>
<!--<script src="<?php echo base_url();?>js/jquery.ajaxupload.js" type="text/javascript" language="javascript" charset="UTF-8"></script>-->
<!--<script src="<?php echo base_url();?>js/jquery-1.7.1.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>-->
<!--script src="<?php echo base_url();?>js/jquery-ui-1.8.16.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script-->
<!--script src="<?php echo base_url();?>js/jquery-ui-timepicker.js" type="text/javascript" language="javascript" charset="UTF-8"></script-->
<script src="<?php echo base_url();?>js/jquery.dateformat.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
<?php
echo form_open('marketing_campaigns/save/'.$campaign_info->campaign_id,array('id'=>'campaign_form'));
?>
<div id='marketing_campaign_contents'>

	<div id='marketing_campaign_types'>
		<!--ul id="error_message_box"></ul-->
		<div class='marketing_campaign_type selected' id='email_tab'>eMail</div>
		<div class='marketing_campaign_type' id='text_tab'>Text</div>
		<div class='clear'></div>
		<?php echo form_hidden('campaign_type', ($campaign_info->type != '' ? $campaign_info->type : 'email'));?>
	</div>
	<div id='marketing_campaign_options'>
	  <?php
		   if($campaign_info->is_sent == 1)
		   {
		     echo '<div style="text-align:center;padding-bottom:15px;"><span style="color:red;">'.lang('marketing_campaigns_already_sent').'</span></div>';
		   }
		  ?>
		<?php echo form_input(array(
			'name'=>'campaign_name',
			'size'=>'25',
	   		'style' => 'width: 220px;',
			'id'=>'campaign_name',
			'placeholder'=>lang('marketing_campaigns_name'),
			'value'=> $campaign_info->name)
		);?>
		<?php echo form_dropdown('campaign_template', $tpl_selections, $campaign_info->template, 'id="campaign_template"  style="width:230px;"');?>
	</div>
	<div id='marketing_campaign_recipients'>
		<div id='recipient_count_holder'>
			Recipients <span id='recipient_count'><?php echo ($campaign_info->recipient_count != '' ? $campaign_info->recipient_count:0)?></span><input id='email_count_holder' type='hidden' value=0 /><input id='text_count_holder' type='hidden' value=0 />
		</div>
		<div id='recipient_list'>
		    <div id="individuals">
		      <ul class="campaign-groups">
		   </ul>
		    <ul id="individual-list" class="campaign-groups">
		    <?php foreach($groups as $id=>$group): if(!in_array($id, $campaign_info->groups)) continue; ?>
		      <li>
		        <?php
		        	echo form_checkbox('campaign_group[]', $id, (in_array($id, $campaign_info->groups)) ? 'TRUE' : NULL);
		            echo "&nbsp;&nbsp;$group (<span class='email_recipient_count'>{$group_count[$id]['email']}</span>/<span class='text_recipient_count'>{$group_count[$id]['phone']}</span>)";
		        ?>
		      </li>
		    <?php endforeach; ?>

		    </ul>
		      <div class='clear'></div>
		    </div>
			<?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','placeholder'=>lang('sales_start_typing_customer_name'),  'accesskey' => 'c'));?>
		</div>
		<?php //echo form_label(lang('marketing_campaigns_group_name').':', 'group_name',array('class'=>'wide label-override')); ?>
		<div class='form_field radio-override'>
		    <div>
		      <?php echo form_hidden('ready_to_send', 0); ?>
		    </div>
		</div>
	</div>
	<div id='marketing_campaign_template'>
		<div id="for-edit-holder">
			<div id='subject_holder'>
				<?php echo form_input(array(
					'name'			=>	'campaign_subject',
					'size'			=>	'25',
			    	'style' 		=>	'width: 97%;',
					'id'			=>	'campaign_subject',
					'placeholder' 	=> 	'Subject',
					'value'			=>	$campaign_info->subject)
				);?>
			</div>
			<div id="campaign-form-right">
				<div id='template_placeholder'>

				</div>
			</div>
		<?php echo form_hidden('campaign_title', $campaign_info->title);
		   echo form_hidden('campaign_content', $campaign_info->content);
		   echo form_hidden('campaign_content_2', $campaign_info->content_2);
		   echo form_hidden('campaign_content_3', $campaign_info->content_3);
		   echo form_hidden('campaign_content_4', $campaign_info->content_4);
		   echo form_hidden('campaign_content_5', $campaign_info->content_5);
		   echo form_hidden('campaign_content_6', $campaign_info->content_6);
		   echo form_hidden('campaign_image', $campaign_info->image);
		   echo form_hidden('campaign_image_2', $campaign_info->image_2);
		   echo form_hidden('campaign_image_3', $campaign_info->image_3);
		   echo form_hidden('campaign_image_4', $campaign_info->image_4); ?>
		  <?php echo form_hidden('campaign_id', $campaign_info->campaign_id); ?>
		  <?php echo form_hidden('is_sent', $campaign_info->is_sent); ?>

		</div> <!-- eo of for-edit-holder div -->
		<div id='campaign-form-text'>
				<?php $this->load->view('marketing_campaigns/text_marketing/text_template'); ?>
		</div>

	</div>
	<div id='marketing_campaign_send_date'>
		  <?php echo form_label('Send:', 'send',array('class'=>'wide label-override')); ?>
		     <?php
		      $attr = array(
		          'id'   => 'send_now',
		          'name' => 'send_now',
		          'value'=> '1',
		          'style'=> 'margin-top:7px;',
		          'checked' => (!$campaign_info->queued && $campaign_info->send_date <= date('Y-m-d h:i:s'))
		      );
		      echo form_radio($attr); ?> Now
		     <?php
		      $attr = array(
		          'id'   => 'send_later',
		          'name' => 'send_now',
		          'value'=> '0',
		          'style'=> 'margin-top:7px;',
		          'checked' => ($campaign_info->queued || $campaign_info->send_date > date('Y-m-d h:i:s'))
		      );
		      echo form_radio($attr); ?> At

		<?php
			$times = array('6am'=>'6am','7am'=>'7am','8am'=>'8am','9am'=>'9am','10am'=>'10am','11am'=>'11am','12pm'=>'12pm','1pm'=>'1pm','2pm'=>'2pm','3pm'=>'3pm','4pm'=>'4pm','5pm'=>'5pm','6pm'=>'6pm');
			if ($campaign_info->queued || $campaign_info->send_date < date('Y-m-d h:i:s')?'disabled':'')
				$disabled = 'disabled = "disabled"';
			echo form_dropdown('campaign_send_hour', $times, date('ga', strtotime($campaign_info->send_date)), $disabled);

			echo ' on ';
			$attr = array(
				'name'=>'campaign_send_date',
				'size'=>'25',
		    	'id'=>'campaign_send_date',
		    	'placeholder'=>'Date',
				'value'=> (!empty($campaign_info->send_date)) ? date('Y-m-d', strtotime($campaign_info->send_date)) : '');
			if ($campaign_info->queued || $campaign_info->send_date < date('Y-m-d h:i:s')?'disabled':'')
				$attr['disabled'] = 'disabled';
			echo form_input($attr);
			?>
	</div>
	<div id='marketing_campaign_buttons'>
		<?php
		//print_r($campaign_info);
		//echo '<br/>'.date('ga', strtotime($campaign_info->send_date));
		echo form_submit(array(
			'name'=>'submitButton',
		 	'id'=>'submitButton',
			'value'=> ($campaign_info->is_sent == 1) ? 'Close' : 'Send',
			'class'=>'submit_button float_right')
		);
		?>
		<?php
		if ($campaign_info->is_sent != 1)
		{
			echo form_submit(array(
				'name'=>'saveButton',
				'id'=>'saveButton',
				'value'=>'Save for later',
				'class'=>'submit_button float_right')
			);
		}
		?>
		<div class='clear'></div>
	</div>
</div>
<?php
echo form_close();
?>
<script type='text/javascript'>
	 var ctype = '<?= $campaign_info->type ?>';
	var switch_interface = function(){
        var v = $('#campaign_type').val();
     	$('#marketing_campaign_types .selected').removeClass('selected');
     	$('#'+v+'_tab').addClass('selected');
        var tpl = $('#campaign_template');
        var ttl = $('#campaign_title');
        //var tlw = $('#campaign_title-wysiwyg-iframe');
        //var hdr = $('#campaign_header');

//        var f   = [tpl, ttl, tlw, hdr];

  //      for(ffi = 0; ffi < f.length; ffi++)
    //    {
      //    var cmpf = f[ffi];
        //  if(v == 'text'){ if(cmpf) cmpf.attr('disabled', 'disabled'); }
          //else { if(cmpf) cmpf.attr('disabled', null); }
        //}

        var oc = $('#option-container');
        var tc = $('#text-counter');
        if(v == 'text'){
            if(oc) oc.hide();
            if(tc) tc.show();
			$('#campaign_content_box').hide();
            $('#for-edit-holder').hide();
			$('#campaign_text_content_box').show();
          	//request_text_tpl();
          	tpl.hide();
			$('#campaign-form-text').show();
        	$('.toolbar').hide();
        }
        else {
            if(oc) oc.show();
            if(tc) tc.hide();
        	tpl.show();
			$('#campaign_content_box').show();
			$('#campaign_text_content_box').hide();
            $('#for-edit-holder').show();
   			$('#campaign-form-text').hide();

            $('.toolbar').show();
        }
        marketing.populate_recipient_count();
        $.colorbox.resize();
     };
	var marketing = {
		date_is_after_now:function() {
			var send_date = $('#campaign_send_date').val();
			var send_hour = $('#campaign_send_hour').val();
			var cleaned_send_hour = parseInt(send_hour.replace('am','').replace('pm',''));
			var add_twelve = (send_hour.indexOf('pm') > 0 && cleaned_send_hour != 12)?true:false;
			send_hour = (add_twelve)?cleaned_send_hour+12:cleaned_send_hour;
			send_hour += ':00';
			var curdate = new Date();
			var senddate =  new Date(send_date +' '+send_hour);

			return (curdate.getTime() < senddate.getTime());
		},
		set_up_editing:function() {
			var myNicEditor = new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','hr','image','removeformat','xhtml']});
	          myNicEditor.setPanel('myNicPanel');
	          if ($('#mailer-header').length > 0)
		          myNicEditor.addInstance('mailer-header');
	          if ($('#mailer-contents').length > 0)
	    	      myNicEditor.addInstance('mailer-contents');
	          if ($('#mailer-contents-2').length > 0)
		          myNicEditor.addInstance('mailer-contents-2');
	          if ($('#mailer-contents-3').length > 0)
		          myNicEditor.addInstance('mailer-contents-3');
	          if ($('#mailer-contents-4').length > 0)
		          myNicEditor.addInstance('mailer-contents-4');
	          if ($('#mailer-contents-5').length > 0)
		          myNicEditor.addInstance('mailer-contents-5');
	          if ($('#mailer-contents-6').length > 0)
		          myNicEditor.addInstance('mailer-contents-6');
		},
		change_email_count:function(amt) {
			var count = parseInt($('#email_count_holder').val());
            count += parseInt(amt);
			$('#email_count_holder').val(count);
		},
		change_text_count:function(amt) {
			var count = parseInt($('#text_count_holder').val());
            count += parseInt(amt);
			$('#text_count_holder').val(count);
		},
		populate_recipient_count:function() {
			var type = $('#campaign_type').val();
			var count = $('#'+type+'_count_holder').val();
      		$('#recipient_count').html(count);
		},
		send_now_selected:function(){
			//$("#campaign_send_date").datepicker( "setDate", new Date() );
    		//$('#campaign_send_date').val($.format.date(new Date(),'M d, yy'));
			console.log($.format.date(new Date(),'ha').toLowerCase());
			//$('#campaign_send_hour').val($.format.date(new Date(),'ha').toLowerCase());
			//$('#campaign_send_date').attr('disabled', 'disabled');
		//	$('#campaign_send_hour').attr('disabled', 'disabled');
		},
		send_later_selected:function(){
		    $('#campaign_send_date').attr('disabled', null);
		    $('#campaign_send_hour').attr('disabled', null);
		},
		save_template_values:function() {
			var tl = $('#mailer-header');
        	var mc = $('#mailer-contents');
			var mc_2 = $('#mailer-contents-2');
			var mc_3 = $('#mailer-contents-3');
			var mc_4 = $('#mailer-contents-4');
			var mc_5 = $('#mailer-contents-5');
			var mc_6 = $('#mailer-contents-6');

			var title = $('#campaign_title');
			var contents = $('#campaign_content');
			var contents_2 = $('#campaign_content_2');
			var contents_3 = $('#campaign_content_3');
			var contents_4 = $('#campaign_content_4');
			var contents_5 = $('#campaign_content_5');
			var contents_6 = $('#campaign_content_6');

			console.log('stuff');

        	tl.length != 0 ? title.val(tl.html()) : '';
        	mc.length != 0 ? contents.val(encodeURIComponent(mc.html())) :'';
			mc_2.length != 0 ? contents_2.val(encodeURIComponent(mc_2.html())) :'';
			mc_3.length != 0 ? contents_3.val(encodeURIComponent(mc_3.html())) :'';
			mc_4.length != 0 ? contents_4.val(encodeURIComponent(mc_4.html())) :'';
			mc_5.length != 0 ? contents_5.val(encodeURIComponent(mc_5.html())) :'';
			mc_6.length != 0 ? contents_6.val(encodeURIComponent(mc_6.html())) :'';
		},
		populate_template_values:function() {
			var title = $('#campaign_title');
			var contents = $('#campaign_content');
			var contents_2 = $('#campaign_content_2');
			var contents_3 = $('#campaign_content_3');
			var contents_4 = $('#campaign_content_4');
			var contents_5 = $('#campaign_content_5');
			var contents_6 = $('#campaign_content_6');

			var tl = $('#mailer-header');
        	var mc = $('#mailer-contents');
			var mc_2 = $('#mailer-contents-2');
			var mc_3 = $('#mailer-contents-3');
			var mc_4 = $('#mailer-contents-4');
			var mc_5 = $('#mailer-contents-5');
			var mc_6 = $('#mailer-contents-6');
			         	console.log('stuff 3');

			tl.html((title.val()!=''?title.val():($.trim(tl.html()) == ''?'Edit Title':tl.html())));
			mc.html((contents.val()!=''?contents.val():($.trim(mc.html()) == ''?'Edit Contents':mc.html())));
			mc_2.html((contents_2.val()!=''?contents_2.val():($.trim(mc_2.html()) == ''?'Edit Contents':mc_2.html())));
			mc_3.html((contents_3.val()!=''?contents_3.val():($.trim(mc_3.html()) == ''?'Edit Contents':mc_3.html())));
			mc_4.html((contents_4.val()!=''?contents_4.val():($.trim(mc_4.html()) == ''?'Edit Contents':mc_4.html())));
			mc_5.html((contents_5.val()!=''?contents_5.val():($.trim(mc_5.html()) == ''?'Edit Contents':mc_5.html())));
			mc_6.html((contents_6.val()!=''?contents_6.val():($.trim(mc_6.html()) == ''?'Edit Contents':mc_6.html())));
		}
	}
  var wysiwyg_rendered = false;

var photo_editing;
//validation and submit handling
$(document).ready(function()
{
	// Editable image handling
	$('img.editable').die('click').live('click', function(e){
		var img = $(this);
		var ratio = parseFloat(img.width() / img.height()).toFixed(2);
		var image_id = img.attr('data-image-id');
		if(!image_id){
			image_id = 0;
		}
		if(image_id == 0){
			img.attr('data-default-src', img.attr('src'));
		}

		photo_editing = img;
		$.colorbox2({'maxHeight':750, 'width':1100, 'href':'<?php echo site_url('upload'); ?>/index/marketing_campaigns?crop_ratio='+ ratio +'&image_id=' + image_id});
	});

	$(document).unbind('changeImage').bind('changeImage', function(event){
		var default_image_src = photo_editing.attr('data-default-src');

		$.post('<?php echo site_url('marketing_campaigns/save_image'); ?>/'+ event.image_id, {replaced_image_url:default_image_src}, function(response){
			$(photo_editing).attr({'src': response.url +'?ts=' + response.last_update, 'data-image-id': event.image_id});
			$.colorbox2.close();
		},'json');
	});

	if(ctype.length > 0 && ctype == 'text')
    {
        $('#campaign_type').val('text');
		switch_interface();
		$('#character_count').html(160-$('#text_contents').val().length);
    }
	$('#text_tab').click(function(){
		if (!$(this).hasClass('disabled')) {
			$('#campaign_type').val('text');
			switch_interface();
			$('#text_contents').focus();
		}
	});
	$('#email_tab').click(function(){
		if (!$(this).hasClass('disabled')) {
			$('#campaign_type').val('email');
			switch_interface();
		}
	});
	$('#submitButton').click(function(e){
		console.log('clicking submit');
		e.preventDefault();
		$('#ready_to_send').val(1);
		$('#campaign_form').submit();
	});
	$('#saveButton').click(function(e){
		console.log('clicking save');
		e.preventDefault();
		$('#ready_to_send').val(0);
		$('#campaign_form').submit();
	});
	   // check for what type is the campaign

  $('#campaign_template').change(function(){
    $('#campaign_template option:selected').each(function(){
      $.ajax({
        url: 'index.php/marketing_campaigns/get_tpl/',
        type: "POST",
        data: {
            tpl: $(this).val()
        },
        success: function( data ) {
        	// Previous values
        	marketing.save_template_values();

        	$('#campaign-form-right').html(data);
			// There is new a new template loaded now and we must reload these dom elements for the new template
			marketing.populate_template_values();

			marketing.set_up_editing();
			$.colorbox.resize();
        },
        dataType:'text'
      });
    });
  });
var campaign_is_sent = parseInt('<?= $campaign_info->is_sent; ?>') || 0;
console.log('campaign_is_sent  <?= $campaign_info->is_sent; ?>');
var status = '<?= $campaign_info->status; ?>';

if(status == 'sending') campaign_is_sent = 1;

  // check on if there is a template path
  var tpl_sel = "<?php echo $campaign_info->template; ?>";

  if(tpl_sel.length > 0)
  {
    $.ajax({
        url: "<?= site_url('marketing_campaigns/get_tpl/'); ?>",
        type: "POST",
        data: {
            tpl: tpl_sel
        },
        success: function( data ) {
          $('#campaign-form-right').html(data);
          console.log('tpl '+tpl_sel);
//          var title = $('#campaign_title').val();
  //        if(title.length > 0)
    //      {
      //      $('#mailer-header').html(title);
        //    $('#campaign_title').val('');
          //  $('#campaign_title').val(title);
          //}
          //var contents = $('#campaign_content').val();
          //if(contents.length > 0)
         // {
          //  $('#mailer-contents').html(contents);
         // }
			marketing.populate_template_values();
          if(campaign_is_sent != 1) marketing.set_up_editing();
        },
        dataType:'text'
      });

      $.colorbox.resize({maxHeight:700});
  }
console.log('setting up date time picker');
   var d = new Date();
  $('#campaign_send_date').datepicker({
      dateFormat:'M d, yy'/*,
      defaultDate:d*/
     // ampm:true,
     // showMinute:false,
     // hour:6,
     // minute:0,
     // minuteMin:0,
     // minuteMax:0
    });

  /*$('#customer').click(function()
    {
    	$(this).attr('value','');
    });*/
   var cust = $('#customer');
	cust.click(function(){cust.autocomplete('search','')});
	cust.autocomplete({
		source: '<?php echo site_url("marketing_campaigns/recipient_search/ln_and_pn"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			//$("#customer").attr('value',"<?php echo lang('sales_start_typing_customer_name'); ?>");
      		cust.val('');
      		//<input type="checkbox" name="campaign_group[]" value="0">
      		if (ui.item.is_group)
			{
				$("#individual-list").append(
	       			'<li id="campaign_group-li-'+ui.item.value+'"><input id="campaign_group-'+ui.item.value+'" type="checkbox" checked="checked" value="'+ui.item.value+'" name="campaign_group[]">&nbsp;&nbsp;'+ui.item.label+'</li>');
				$('#campaign_group-'+ui.item.value).change(function(){
			        $('#campaign_group-li-'+ui.item.value).remove();
			        marketing.change_email_count(-ui.item.email);
			        marketing.change_text_count(-ui.item.text);
			    });
			    marketing.change_email_count(ui.item.email);
			    marketing.change_text_count(ui.item.text);
			}
			else
			{
				$("#individual-list").append(
			        '<li id="campaign_individuals-li-'+ui.item.value+'"><input id="campaign_individuals-'+ui.item.value+'" type="checkbox" checked="checked" value="'+ui.item.value+'" name="campaign_individuals[]">&nbsp;&nbsp;'+ui.item.label+'</li>');
			    $('#campaign_individuals-'+ui.item.value).change(function(){
			      	$('#campaign_individuals-li-'+ui.item.value).remove();
				    marketing.change_email_count(-(ui.item.email == '' ? 0 : 1));
				    marketing.change_text_count(-(ui.item.email == '' ? 0 : 1));
			    });
			    marketing.change_email_count((ui.item.email == '' ? 0 : 1));
			    marketing.change_text_count((ui.item.email == '' ? 0 : 1));
			}
			marketing.populate_recipient_count();
	 	}
	});


  //$('#customer').blur(function()
  //{
  //  $(this).attr('value',"<?php echo lang('sales_start_typing_customer_name'); ?>");
  //});

	$('#send_now').change(function(){
		if($(this).attr('checked'))
		{
    		marketing.send_now_selected();
		}
	});
	$('#send_later').change(function(){
  		if ($(this).attr('checked'))
    	{
    		marketing.send_later_selected();
   		}
	});

  var individuals = <?= $campaign_info->individuals ?>;

  if(individuals.length > 0)
  {
    for(i=0;i<individuals.length; i++)
    {
      var ih = individuals[i];
      $("#individual-list").append(
        '<li id="campaign_individuals-li-'+ih.id+'"><input id="campaign_individuals-'+ih.id+'" type="checkbox" checked="checked" value="'+ih.id+'" name="campaign_individuals[]">&nbsp;&nbsp;'+ih.name+'</li>'
      );
       $('#campaign_individuals-'+ih.id).change(function(){
        $('#campaign_individuals-li-'+ih.id).remove();
      });
    }
  }



    /***********************************************************************
     * Text marketing handler
     ***********************************************************************/

     var cnt = $('#campaign_content');
     if(cnt.length>0 && cnt.val().length > 0) $('#campaign_type').trigger('change');
    /***********************************************************************
     * eo text marketing
     ***********************************************************************/

    /***********************************************************************
     * check if already sent, then disable all controls
     ***********************************************************************/
//   var is_sent = '<?= $campaign_info->is_sent; ?>' || 0;
   if(campaign_is_sent == 1)
    {
      $(':label[for="group_name"]').html('Recipients:');
      $('#customer').remove();
      $("#campaign_form :input").attr("disabled","disabled");
      $('#text_tab').addClass('disabled');
      $('#email_tab').addClass('disabled');
      $("#submitButton").attr("disabled",null);
      $("#submitButton").click(function(e){
        e.preventDefault();
        $.colorbox.close();
      });

    }
    else
    {
      /***********************************************************************
     * close handler
       ***********************************************************************/
      window.onbeforeunload = function(){
        if ($("#colorbox").css("display")=="block" && campaign_is_sent != 1) {
            return 'Are you sure you want to leave without saving?';
        }
      }

      var originalClose = $.colorbox.close;
      $.colorbox.close = function(){
        if (confirm('Are you sure you want to close without saving?')) {
          $.colorbox.close = originalClose;
          originalClose();
        }
      };

      /***********************************************************************
       * end close handler
       ***********************************************************************/
    }

    /***********************************************************************
     * submit handler
     ***********************************************************************/

    var submitting = false;
    $('#campaign_form').validate({
		submitHandler:function(form)
		{
			console.log('submit handling '+(campaign_is_sent?'sent':'not sent'));
			if (campaign_is_sent) return;
			console.log('after return')
			if ($('#ready_to_send').val() === '1')
			{
				if (!marketing.date_is_after_now() && !$('#send_now').attr('checked'))
				{
					alert('Send date must be set to a later time');
					return;
				}
				else if ($('#campaign_template').val() === '0' && $('#campaign_type').val() == 'email')
				{
					alert('Template required');
					return;
				}
			}
			//Load all values from template
			marketing.save_template_values();

	      // turn off onunload handler during submit
	      window.onbeforeunload = null;
	      $.colorbox.close = originalClose;
	      if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
	        success:function(response)
	        {
	    	  $.colorbox.close();
	          post_campaign_form_submit(response);
	          submitting = false;
	        },
	        dataType:'json'
	      });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			//campaign_title:{required:function(){return ($('#ready_to_send').val() === '1')}},
			campaign_name:{required:true},
			campaign_type:{required:function(){return ($('#ready_to_send').val() === '1')}},
			//campaign_content:{required:function(){return ($('#ready_to_send').val() === '1')}},
			campaign_send_date:{required:function(){return ($('#ready_to_send').val() === '1')}}
		},
		messages:
		{
			//campaign_title:{required:"<?php echo lang('marketing_campaigns_title_required'); ?>"},
			campaign_name:{required:"<?php echo lang('marketing_campaigns_name_required'); ?>"},
			campaign_type:{required:"<?php echo lang('marketing_campaigns_type_required'); ?>"},
			//campaign_content:{required:"<?php echo lang('marketing_campaigns_content_required'); ?>"},
			campaign_send_date:{required:"<?php echo lang('marketing_campaigns_send_date_required'); ?>"}
		}
	});
	marketing.send_now_selected();
});
</script>
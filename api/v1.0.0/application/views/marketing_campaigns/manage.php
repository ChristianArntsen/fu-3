<?php $this->load->view("partial/header"); ?>
<style type="text/css">
  .on-queue, .sent{width:25px;height:25px;}
  .on-queue{background: url("<?= base_url('images/yellow-circle.png'); ?>") no-repeat;}
  .sent{color:#396905}
  #marketing-status{height:78px; }
  #marketing-status h4{text-align:center;font-size:18px;}
  #email-status, #text-status{height:78px;width: 168px;}
  #marketing_message {
  	font-size:24px;
  	text-align:center;
  	padding-bottom:15px;
  }
  .info{
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border: 1px solid #1d599a;
    height: 75px;
    margin-top: 5px;
    width: 168px;
    background: url("<?= base_url('images/status-bg.gif'); ?>") repeat-x;
    color: #fff;
    text-align: center;
    font-weight:bold;
  }

  .info table td.header{font-size: 12px;line-height: 25px;}
  .info table td.body{font-size: 20px;width: 50%;}
  .info table td.footer{font-size: 10px;}
</style>
<link rel='stylesheet' href="css/facebook_widget.css" />
<script type="text/javascript">

$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({
    //initialWidth:1200, 
    width:660,
    escKey: false,
    overlayClose: false,
    onComplete:function(){
   		$.colorbox.resize({width:660});
    }
  });

    init_table_sorting();
    enable_select_all();
    enable_checkboxes();
    enable_row_selection();
    enable_search('<?php echo site_url("$controller_name/suggest")?>','<?php echo lang("common_confirm_search")?>');
    enable_delete('<?php echo lang($controller_name."_confirm_delete")?>','<?php echo lang($controller_name."_none_selected")?>');
	
	$('#generate_barcodes').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcodes");?>/'+selected.join('~'));
    });

	$('#generate_barcode_labels').click(function()
    {
    	var selected = get_selected_values();
    	if (selected.length == 0)
    	{
    		alert('<?php echo lang('items_must_select_item_for_barcode'); ?>');
    		return false;
    	}

    	$(this).attr('href','<?php echo site_url("giftcards/generate_barcode_labels");?>/'+selected.join('~'));
    });
});

function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(
		{
			sortList: [[6,0]],
			headers:
			{
				0: { sorter: false},
				7: { sorter: false}
			}
		});
	}
}

function post_campaign_form_submit(response)
{
	if(!response.success)
	{
		set_feedback(response.message,'error_message',true);
		if(jQuery.inArray(response.campaign_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
		}
	}
	else
	{
    //This is an update, just update one row
		if(jQuery.inArray(response.campaign_id,get_visible_checkbox_ids()) != -1)
		{
			update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
			set_feedback(response.message,'success_message',false);

		}
		else //refresh entire table
		{
			do_search(true,function()
			{
				//highlight new row
				highlight_row(response.campaign_id);
				set_feedback(response.message,'success_message',false);
			});
		}

    if(response.send_now == 1)
    {
      set_feedback('Sending...','success_message',true);

      $.ajax({
        url: "<?= site_url('marketing_campaigns/send_now/'); ?>",
        type: "POST",
        data: {
            campaign_id: response.campaign_id
        },
        success: function( data ) {
        	load_stats_header('marketing_campaigns');
          set_feedback('Campaign sent successfully.','success_message',true);
          update_row(response.campaign_id,'<?php echo site_url("$controller_name/get_row")?>');
        },
        dataType:'json'
      });
    }
	}
}
//FACEBOOK CODE
	window.my_config = 
	{
		access_token : '',
		page_id : '',
		name : '',		
		user_accounts : '',
		<?php if (base_url() == 'http://localhost:8888/') { ?>
		app_id : '317906091649413', // app_id for LOCALHOST testing only
		app_secret : '5a47cce8b870df193418cbdc1490913b', //app_secret for LOCALHOST testing only
		<?php } else if (base_url() == 'http://dev.foreupsoftware.com') { ?>
		app_id : '583489044999943', // App ID for ForeUP Dev
		app_secret : '5f8b249b9473fd74b13814be106d45ad', //app_secret for ForeUP Dev
		<?php } else if (base_url() == 'http://mobile.foreupsoftware.com') { ?>
		app_id : '398875546898011', // App ID for Mobile.ForeUP Dev
		app_secret : '31d25e25007a63720f3f2e26fce3160a', //app_secret for Mobile.ForeUP Dev
		<?php } else { ?>
		app_id : '411789792224848', // App ID for ForeUP Master
		app_secret : 'f69cd64778c32cb7dda1c1ec6960e1b0', //app_secret for ForeUP Master
		<?php } ?>
		extended_access_token : '',
		can_post_with_extended_access_token : false
	};	
	
	var facebook_obj = {
		logout: function(){	
			FB.logout(function(response){});
		}
	}
	
$(document).ready(function(){
	/**********************************************************************************
	 **********************************FACEBOOK CODE***********************************
	 **********************************************************************************/	
	function update_my_config()
	{
		
		window.my_config.page_id = '<?php echo $this->session->userdata('facebook_page_id'); ?>';
		window.my_config.name = '<?php echo $this->session->userdata('facebook_page_name'); ?>';
		window.my_config.extended_access_token = '<?php echo $this->session->userdata('facebook_extended_access_token'); ?>';	
		if (window.my_config.extended_access_token !== '') {
			window.my_config.can_post_with_extended_access_token = true;			
		} 		
		update_facebook_widget_appearance();
	}
	
	function update_facebook_widget_appearance()
	{
		if (window.my_config.can_post_with_extended_access_token)
		{
			$('.facebook_post').show();
			$('.facebook_login').hide();
			$('#facebook_logout').show();
			$('#message').show();
		} else {
			$('.facebook_post').hide();
			$('.facebook_login').show();
			$('#facebook_logout').hide();
			$('#message').hide();
		}
	}

	
	update_my_config();
					
	/*Load Facebook SDK*/
	  window.fbAsyncInit = function() {    
	    FB.init({
	      appId      : window.my_config.app_id,
	      channelUrl : '<?php echo site_url();?>', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });		    
	  };
		  
	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "http://connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));
	   
	 
	$('.post').live('click', function(){
		if (window.my_config.can_post_with_extended_access_token)
		{						
			post_facebook_message();
		}
		else
		{
			login();			
		}						
	});
	
	//EVERYTHING STARTS HERE  
	 /********1********/
	$('.facebook_login').live('click', function(){
		login();
	});
	
	$('#facebook_logout').click(function(){
		//the user was already logged out of their official facebook session right after logging in. This method just clears their facebook settings from the database
		clear_facebook_settings();
	});
	
	/********2********/
	function login(){		
		FB.login(function(response) {
	        if (response.authResponse) {	            
				//get an extended access token from facebook which will be stored in our database and used to make future posts
	            fetch_long_lived_access_token(response.authResponse.accessToken);
	        } else {
	            console.log("canceled");
	        }
		},{scope: 'manage_pages publish_stream'});//http://stackoverflow.com/questions/8717388/the-user-hasnt-authorized-the-application-to-perform-this-action
	}		
	
	function clear_facebook_settings()
	{
		var URL = '<?php echo site_url("marketing_campaigns/clear_facebook_settings");?>';
			$.post(URL, function(response) {		    
			    window.my_config.extended_access_token = '';
			    window.my_config.can_post_with_extended_access_token = false;
			    window.my_config.page_id = '';
				window.my_config.name = '';	
								
				update_facebook_widget_appearance();				
	    	});	
	}
	
	/********3********/
       
	function fetch_long_lived_access_token(short_lived_access_token)
	{	
		var URL;
		URL = "https://graph.facebook.com/oauth/access_token?client_id=" + window.my_config.app_id + "&client_secret=" + window.my_config.app_secret + "&grant_type=fb_exchange_token&fb_exchange_token=" + short_lived_access_token;								
		$.ajax({
	        url: URL,
	        type: "POST",
	        success: function(response) {
			    var token_elements = response.split('=');
			    extended_access_token = token_elements[1].split('&');
			    window.my_config.extended_access_token = extended_access_token[0];
			    window.my_config.can_post_with_extended_access_token = true;
			    
				//post the facebook extended access token to server
				URL = '<?php echo site_url("marketing_campaigns/update_facebook_access_token");?>';
				data = {extended_access_token: window.my_config.extended_access_token};						
				
				$(URL, data, function(response) {
	
			    });	
			    
			    //show a colorbox that will allow them to select which page is the golf course pages. NOTE: this pop up shows them all pages for which they are administrator		    
			    $.colorbox({'href':'index.php/marketing_campaigns/facebook_page_select/width~300', 'title':"Select the Course Page", 'overlayClose':false, 				
					onClosed: function(){
						update_facebook_widget_appearance();
					}				
				});		 
		    },
	        dataType:'json'
        });
		// $.getJSON(URL, function(response) {
// 
		    // var token_elements = response.split('=');
		    // extended_access_token = token_elements[1].split('&');
		    // window.my_config.extended_access_token = extended_access_token[0];
		    // window.my_config.can_post_with_extended_access_token = true;
// 		    
			// //post the facebook extended access token to server
			// URL = '<?php echo site_url("marketing_campaigns/update_facebook_access_token");?>';
			// data = {extended_access_token: window.my_config.extended_access_token};						
// 			
			// $(URL, data, function(response) {
// 
		    // });	
// 		    
		    // //show a colorbox that will allow them to select which page is the golf course pages. NOTE: this pop up shows them all pages for which they are administrator		    
		    // $.colorbox({'href':'index.php/marketing_campaigns/facebook_page_select/width~300', 'title':"Select the Course Page", 'overlayClose':false, 				
				// onClosed: function(){
					// update_facebook_widget_appearance();
				// }				
			// });		 
	    // });
	    
	    return;
	}		
	/********5********/
	function post_facebook_message(options)
	{
		var message, URL, success_message;
		message = $('#message').val();
		URL = "https://graph.facebook.com/" + window.my_config.page_id + "/feed?access_token=" + window.my_config.extended_access_token + "&message=" + message;				
		
		$('.facebook_messages').mask("<?php echo lang('common_wait'); ?>");
		$.ajax({
	        url: URL,
	        type: "POST",
	        success: function(){
		    	$('.facebook_messages').unmask();
		    	$('#message').val('');
		    	
		    	success_message = "Successfully Posted to " + window.my_config.name + " Facebook Page";	
		    	
		    	//functioned defined in other script
		    	set_feedback(success_message, 'success_message', false);    	
		    },
	        dataType:'json'
        });
		// $.getJSON(URL, function(response) {	      
// 	      
	    // })
	    // .success(function(){
	    	// $('.facebook_messages').unmask();
	    	// $('#message').val('');
// 	    	
	    	// success_message = "Successfully Posted to " + window.my_config.name + " Facebook Page";	
// 	    	
	    	// //functioned defined in other script
	    	// set_feedback(success_message, 'success_message', false);    	
	    // })
	    // .error(function() { 
   			 // $('.facebook_messages').unmask();
   			 // login();
		// });		
	}	
	var ft = $('#facebook_title');
	ft.click(function(){
		if (ft.hasClass('expanded'))
		{
			ft.removeClass('expanded');
    		$('#facebook_content').hide();
		}
		else
		{
    		ft.addClass('expanded');
    		$('#facebook_content').show();
		}
	});	
});
</script>

<!--table id="title_bar">
	<tr>
		<td colspan='3'>
			<div id='marketing_message'>
	    		<?= $marketing_message ?>
	    	</div>
		</td>
	</tr>
  <tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('module_'.$controller_name); ?>
		</td>
		<td id="title_search">
			<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
				<input type="text" name='search' id='search'/>
				<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
			</form>
		</td>
	</tr>
</table-->

<table id="contents">
	<tr>
		<td id="commands">
<?php if ($email_credits > 0) { ?>
			<div id="new_button">
				<?php echo 
					anchor("$controller_name/view/-1/width~650",
					lang($controller_name.'_new'),
					array('class'=>'colbox none new', 
						'title'=>lang($controller_name.'_new')));
				?>
				
				<?php
        /* echo
					anchor("$controller_name/generate_barcode_labels",
					lang("common_barcode_labels"),
					array('id'=>'generate_barcode_labels', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_labels'))); 
				?>
				
				<?php echo 
					anchor("$controller_name/generate_barcodes",
					lang("common_barcode_sheet"),
					array('id'=>'generate_barcodes', 
						'class' => 'generate_barcodes_inactive',
						'target' =>'_blank',
						'title'=>lang('common_barcode_sheet'))); */
				?>
					
				<?php echo 
					anchor("$controller_name/delete",
					lang("common_delete"),
					array('id'=>'delete', 
						'class'=>'delete_inactive')); 
				?>
                <?php /*echo anchor("$controller_name/excel_export",
					lang('common_excel_export'),
					array('class'=>'none import'));*/
				?>

			
			</div>
		<?php } ?>
		<div class="facebook_messages logged_in">
         	<div id='facebook_title'>Facebook<span class='icon minimize_icon'></span></div>
        	<div id='facebook_content' style='display:none'>
				<div id="selected_page" class="truncated"></div>
	        	<ol id="selectable">
	
				</ol>
				<textarea id="message" rows="8" maxlength="150" placeholder="Enter message to post to page"></textarea>
		    	<input class="facebook_login" type="button" value="Login">		    	
		    	<input class="post facebook_post" type="button" value="Post to Page">         	  
	        	<!--input class="post" type="button" value="Post" style='display:none'/>
			  	<input class="login" type="button" value="Login"/>
			  	<input class="logout" type="button" value="Logout" style='display:none'/-->
			  	<span id='facebook_logout'>Logout</span>
			</div>			        	
    	</div>
		</td>
		<td style="width:10px;"></td>
		<td id="item_table">
			<div id='table_top'>
				<?php echo form_open("$controller_name/search",array('id'=>'search_form')); ?>
					<input type="text" name ='search' id='search' placeholder="Search"/>
					<img src='<?php echo base_url()?>images/spinner_small.gif' alt='spinner' id='spinner' />
				</form>
			</div>
			<div class='fixed_top_table'>
				<div class='header-background'></div>
				<div id="table_holder">
				<?php echo $manage_table; ?>
				</div>
			</div>
			<div id="pagination">
				<?php echo $this->pagination->create_links();?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
	
<?php $this->load->view("partial/header"); ?>
<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<div>
		<?php echo lang('reports_export_to_excel'); ?>: <input type="radio" name="export_excel" id="export_excel_yes" value='1' /> <?php echo lang('common_yes'); ?>
		<input type="radio" name="export_excel" id="export_excel_no" value='0' checked='checked' /> <?php echo lang('common_no'); ?>
	</div>

<?php
if ($type == 'inventory') {
	echo form_label(lang('reports_filter_by'), 'reports_department_label', array('class'=>'required')); ?>
	<div id='report_department_filter'>
		<input type="radio" name="report_filter" id="department_radio" value='department' checked/>
		<?php echo lang('reports_department').' '.form_dropdown('department', $departments, 'all', 'id="department"'); ?>
	</div>
	<div id='report_category_filter'>
		<input type="radio" name="report_filter" id="category_radio" value='category' />
		<?php echo lang('reports_category').' '.form_dropdown('category', $categories, 'all', 'id="category"'); ?>
	</div>
	<div id='report_subcategoroy_filter'>
		<input type="radio" name="report_filter" id="subcategory_radio" value='subcategory' />
		<?php echo lang('reports_subcategory').' '.form_dropdown('subcategory', $subcategories, 'all', 'id="subcategory"'); ?>
	</div>
	<div id='report_supplier_filter'>
		<input type="radio" name="report_filter" id="supplier_id_radio" value='supplier' />
		<?php echo lang('reports_supplier').' '.form_dropdown('supplier_id', $suppliers, 'all', 'id="supplier_id"'); ?>
	</div>
	<script>
		$('#department, #category, #subcategory, #supplier').change(function(e){
			var id = $(e.target).attr('id');
			$('#'+id+'_radio').click();
		})
	</script>
	<?php
}
echo form_button(array(
	'name'=>'generate_report',
	'id'=>'generate_report',
	'content'=>lang('common_submit'),
	'class'=>'submit_button')
);
?>

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	<?php if ($type == 'inventory') { ?>
		$("#generate_report").click(function()
		{
			var sale_type = $("#sale_type").val();
			var department = $('#sale_department').val();
			var filter = $('input[name=report_filter]:checked').attr('id').replace('_radio', '');
			var value = $('#'+filter).val();
			
			var export_excel = 0;
			if ($("#export_excel_yes").attr('checked'))
			{
				export_excel = 1;
			}
			
			window.location = window.location+'/' + export_excel + '/'+encodeURIComponent(filter)+'/'+encodeURIComponent(encodeURIComponent(value));
		});
	<?php } else { ?>
		$("#generate_report").click(function()
		{
			var export_excel = 0;
			if ($("#export_excel_yes").attr('checked'))
			{
				export_excel = 1;
			}
			
			window.location = window.location+'/' + export_excel;
		});	
	<?php } ?>
});
</script>
<?php $this->load->view("partial/header");
$printer_name = $this->config->item('updated_printing')?'foreup':'foreup';
?>
<style>
div.keypad {
	overflow: hidden;
	position: absolute;
	height: 190px;
	padding-top: 10px;
	width: 280px;
	background-color: white;
	box-shadow: 2px 2px 8px #222;
	z-index: 999999;
}

div.keypad div.number_pad {
	overflow: hidden;
	margin: 0px auto;
	padding: 0px;
	width: 260px;
	display: block;
}

div.keypad input {
	margin: 10px;
	display: block !important;
	width: auto !important;
}

#cbox2Content {
	background: none repeat scroll 0 0 #EFEFEF;
}

#cbox2title {
    background: -moz-linear-gradient(center top , #3CACD2, #3090C3, #2474B4) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-radius: 0 0 0 0;
    border-top: 1px solid #9FD7E9;
    color: #FFFFFF;
    font-size: 16px;
    font-weight: lighter;
    padding-left: 20px;
    text-align: left;
    text-shadow: -1px -1px 0 #000000;
}

#splitpayment_register .reg_item_seat {
	width: 6%;
}

#splitpayment_register .reg_item_name {
	width: 42%;
}

th.reg_item_name {
    width: 38%;
}

th.reg_item_seat, td.reg_item_seat {
	width: 10%;
	background-color: #F0F0F0;
	font-weight: bold;
}

th.reg_item_seat {
	text-indent: 0px !important;
}

th.reg_item_seat > div {
	margin-left: -6px;
}
</style>
<script>
(function ( $ ) {
	var template = '<div class="keypad" style="display: none; position: absolute;">' +
		'<div class="number_pad">' +
			'<div class="number_pad_number number_one" data-char="1"></div>' +
			'<div class="number_pad_number number_two" data-char="2"></div>' +
			'<div class="number_pad_number number_three" data-char="3"></div>' +
			'<div class="number_pad_number number_four" data-char="4"></div>' +
			'<div class="number_pad_number number_five" data-char="5"></div>' +
			'<div class="number_pad_number number_six" data-char="6"></div>' +
			'<div class="number_pad_number number_seven" data-char="7"></div>' +
			'<div class="number_pad_number number_eight" data-char="8"></div>' +
			'<div class="number_pad_number number_nine" data-char="9"></div>' +
			'<div class="number_pad_number number_login" data-char="enter"></div>' +
			'<div class="number_pad_number number_zero" data-char="0"></div>' +
			'<div class="number_pad_number number_backspace" data-char="del"></div>' +
		'</div>' +
	'</div>';

	function add_character(input, character){
		input.val(input.val() + '' + character);
		input.focus();
		return true;
	}

	function remove_character(input){
		var curValue = input.val();
		input.val(curValue.substring(0, curValue.length - 1));
		input.focus();
	}

	function close(keypad){
		keypad.hide();
		return value;
	}

    $.fn.keypad = function( options ) {
        // Default options
        var settings = $.extend({
            position: 'left'
        }, options );

        $(document).bind('click', function(e){
			if(!$(e.target).hasClass('has-keypad') && !$(e.target).hasClass('keypad')){
				$('div.keypad').hide();
			}
		});

        // Attach keypad code to element on page
        return this.each(function(){

			var element = $(this);
			element.addClass('has-keypad');

			// Place hidden keypad next to element
			$('body').append(template);

			$(this).click(function(e){
				var height = parseInt($(this).height(), 10);
				var width = parseInt($(this).width(), 10);
				var padY = parseInt($(this).css('padding-top'), 10) + parseInt($(this).css('padding-bottom'), 10);
				var padX = parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10);
				var top = $(this).offset().top + height + padY - 200;
				var left = $(this).offset().left + width + padX + 5;

				$('div.keypad').css({'top':top, 'left':left}).show();
			});

			$('div.keypad').find('.number_pad_number').click(function(e){
				var keypad = $(this).parents('div.keypad');
				var character = $(this).attr('data-char');

				if(character == 'enter'){
					close(keypad);
				}else if(character == 'del'){
					remove_character(element);
				}else{
					add_character(element, character);
				}
				return false;
			});
		});
    };
}( jQuery ));
</script>
<script type='text/javascript' src='js/keyboard/jquery.keyboard.js'></script>
<script type='text/javascript' src='js/keyboard/qwerty.js'></script>
<script type='text/javascript' src='js/keyboard/form.js'></script>
<script type='text/javascript' src='js/keyboard/jquery.selection.js'></script>
<script type='text/javascript' src='js/fnb.js'></script>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/fnb.css?<?php echo APPLICATION_VERSION; ?>" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/keyboard.css?<?php echo APPLICATION_VERSION; ?>" />
<div style='display: none;'>
	<div id='fnb_login_container'>
		<?php $this->load->view('food_and_beverage/login');?>
	</div>
</div>
<?php
// ONLY IF WE'RE NOT LOGGED INTO THE F & B
if (!$this->session->userdata('fnb_logged_in')) { ?>
<script>
	$(document).ready(function(){
		fnb.login.show();
	});
</script>
<?php } else if (!$this->sale_lib->get_table_id()) { ?>
<script>
	$(document).ready(function(){
		fnb.show_tables();
	});
</script>
<?php } ?>
<div id='sales_register_holder'>
	<div id='cart_section'>
		<div id="register_container" class="sales">
			<table>
				<!-- food_and_beverage/return hidden controls -->
               <tr>
                       <td>
	                       <div style='display:none'>
	                               <?php echo form_open("food_and_beverage/change_mode",array('id'=>'mode_form')); ?>
	                               <span><?php echo lang('sales_mode') ?></span>
	                               <?php
	                                       echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"  id="mode"');
	                                       echo form_close();                                               ?>
	                               </div>
               			</td>
               </tr>

		    	<tr>
		    		<td id="register_items_container" class='<?=$mode?>'>
		    			<!-- Actual food_and_beverage/return controls and search box -->
						<div id="table_top" class="table_top_sale">
						<?php
							$this->load->view("food_and_beverage/table_top", array("mode" => $mode));
						?>
						</div>
						<!-- Register area -->
						<div class='fixed_top_table'>
							<div class='header-background'></div>

						<div id="register_holder">
							<div id="register_box">
								<?php
									$register_box_info = array("cart"					=> $cart,
															   "giftcard_error_line"	=> $giftcard_error_line,
															   "items_module_allowed"	=> $items_module_allowedd,
															   "basket"					=> $basket);
									$this->load->view("food_and_beverage/register_box", $register_box_info);
								?>
							</div>
						    <div id='sale_details'>
				            	<table>
				            		<tr>
				            			<td>
							            	<div class='subtotal_box'>
					                        	<div class="left float"><?php echo lang('sales_sub_total'); ?>:</div>
						                        <div id="basket_total" class="right float"><?php echo to_currency($basket_subtotal); ?></div>
						                        <div class='clear'></div>
					                        </div>
					                        <div id="items_in_basket" class="left"><?php echo $items_in_basket; ?> Items</div>
							            </td>
						                <td>
							                <div id='taxes_holder'>
						                        <?php foreach($basket_taxes as $name=>$value) { ?>
						                        <div>
						                            <div class="right register_taxes"><?php echo to_currency($value); ?></div>
						                            <div class="left register_taxes"><?php echo $name; ?></div>
						                            <div class='clear'></div>
						                        </div>
						                        <?php }; ?>
							                </div>
							                <div id='taxable_box'>
							                	<div class='right'><?php echo form_checkbox(array('name'=>'is_taxable', 'id'=>'is_taxable', 'checked'=>$taxable_checked, 'onclick'=>"fnb.toggle_taxable(this)", "$taxable_disabled"=>"$taxable_disabled")); ?></div>
							                	<div class="left"><?php echo lang('sales_taxable'); ?></div>
							                    <div class='clear'></div>
						                    </div>
							                <div id="register_total">
					                            <div id="basket_final_total" class="right"><?php echo to_currency($basket_total); ?></div>
						                        <div class="left"><?php echo lang('sales_total'); ?>:</div>
					                        </div>
						                </td>
			          				</tr>
			          			</table>
				            </div>
						</div>
						</div>
					</td>
				</tr>
			</table>
			<div id='totals_box'>
				<table id="sales_items">
	                <?php foreach($payments as $payment) {?>
	                    <?php if (strpos($payment['payment_type'], lang('sales_giftcard'))!== FALSE) {?>
	                <tr>
                        <td class="left"><?php echo $payment['payment_type']. ' '.lang('sales_balance') ?>:</td>
                        <td class="right"><?php echo to_currency($this->Giftcard->get_giftcard_value(end(explode(':', $payment['payment_type']))) - $payment['payment_amount']);?></td>
	                </tr>
	                    <?php }?>
	                <?php }?>
                </table>
     			<?php $this->load->view('food_and_beverage/payments_list');?>
     			<div class='clear'></div>
			</div>
		</div>
		<div id="overall_sale">
			<table id="customer_info_filled">
        		<?php
					$customer_info = array("customer_id"				=> $customer_id,
										   "customer"					=> $customer,
										   "customer_quickbuttons"		=> $customer_quickbuttons,
										   "customer_account_number"	=> $customer_account_number,
										   "customer_email"				=> $customer_email,
										   "customer_phone"				=> $customer_phone,
										   "cab_name"					=> $cab_name,
										   "customer_account_balance"	=> $customer_account_balance,
										   "is_member"					=> $is_member,
										   "cmb_name"					=> $cmb_name,
										   "customer_member_balance"	=> $customer_member_balance,
										   "giftcard"					=> $giftcard,
										   "giftcard_balance"			=> $giftcard_balance);
					$this->load->view("food_and_beverage/customer_info_filled", $customer_info);
				?>
			</table>
	</div><!-- END OVERALL-->
	</div>
	<!-- Start primary column -->
	<div class='main_column'>
		<div id="Payment_Types" >
				<div class='pay_box'>
					<div id='payments_button' class='fnb_button'>
						<?php echo ($mode == 'sale' ? 'Pay Now' : 'Return Now')?>
					</div>
					<?php
					// Only show this part if there is at least one payment entered.
					//if(count($payments) > 0)
					{
							?>
						<div id="finish_sale">
							<?php echo form_open("food_and_beverage/complete",array('id'=>'finish_sale_form')); ?>
							<?php
							$process_sale = '';
							if ($payments_cover_total && $amount_due == '0.00')
							{
								echo '<h1>Change: '.$amount_due.'</h1>';
								echo '<script>$("body").mask("'.lang("sales_completing_sale").'");</script>';
								$process_sale = '$(document).ready(function(){	mercury.complete_sale();});';
							}
							else if ($payments_cover_total && $mode == 'sales')
							{
								echo '<script>$(document).ready(function(){mercury.complete_sale_window();});</script>';
							}
							?>
							</form>
						</div>
					<?php }	?>
						<a class='small_button new_quickbutton fnb_button' id='suspend_sale_button' href='javascript:void(0)'>
							<span id='payment_suspend_sale' class=' payment_button_medium'><?=lang('food_and_beverage_select_table');?></span>
						</a>
					<!--span id='payment_suspend_sale' class='payment_button payment_button_wide'>Suspend Sale</span-->
					<span id='open_cash_drawer' class=' payment_button_medium fnb_button'>Cash Drawer</span>
					<?php if ($this->session->userdata('user_level') < 2) { ?>
					<span id='override_sale' class=' payment_button_medium fnb_button'>Override</span>
					<?php } ?>
					<?php if ($this->config->item('track_cash')) { ?>
						<?php echo anchor(site_url('food_and_beverage/closeregister?continue=home'),
							"<span id='payment_suspend_sale' class=' payment_button_medium fnb_button'>".lang('sales_close_register')."</span>"); ?>
					<?php } ?>
			   			<a class='small_button new_quickbutton fnb_button' id='fnb_logout' href='javascript:void(0)'>
							<span id='payment_log_out' class=' payment_button_medium'>Logout</span>
						</a>
					<div class='clear'></div>
				</div>
				<div class='clear'></div>
			</div>
		<div class='item_menus'>
			<?php
				$menu_sections = array();
				$menu_section_header = '';
				$menu_section_html = '';
				$current_section = '';
				$first_section = false;
				if (count($items) > 0)
				{
					foreach ($items as $item)
					{
						$menu_section_name = trim($item['category']) != '' ? $item['category'] : 'Uncategorized';
						$menu_section_name = strtolower(str_replace(' ', '_', $menu_section_name));
						if (!$menu_sections[$menu_section_name])
							$menu_sections[$menu_section_name] = array();
						$menu_sections[$menu_section_name]['section'] = $item['category'];
						if (!$menu_sections[$menu_section_name]['items'])
							$menu_sections[$menu_section_name]['items'] = array();
						$menu_sections[$menu_section_name]['items'][] = $item;
					}
					foreach ($menu_sections as $section_id => $menu_section)
					{
						$first_section = ($first_section ? $first_section : $section_id);
						$menu_section_header .= "<div id='section_$section_id' class='menu_section_button fnb_button' onclick='fnb.show_section(\"{$section_id}\")'>{$menu_section['section']}</div>";
						$menu_section_html .= "<div id='section_{$section_id}_contents' class='menu_section_item_holder' style='display:none'>";
						foreach ($menu_section['items'] as $item)
							$menu_section_html .= "<div id='item_{$item['item_id']}' class='menu_item fnb_button' onclick='fnb.add_item({$item['item_id']})'>{$item['name']}</div>";
						$menu_section_html .= '<div class="clear"></div></div>';
					}
				}
			?>
			<div id='menu_section_button_holder'>
				<?=$menu_section_header?>
			</div>
			<div id='menu_section_item_holder'>
				<?=$menu_section_html?>
			</div>
			<script>
				fnb.show_section('<?=$first_section?>');
			</script>
		</div>

		<?php if ($this->config->item('print_after_sale')) { ?>
		<div style='height:1px; width:1px; overflow: hidden'>
		       <applet name="jZebra" code="jzebra.PrintApplet.class" archive="<?php echo base_url()?>/jzebra.jar" width="100" height="100">
		           <param name="printer" value="<?=$printer_name?>">
		           <!-- <param name="sleep" value="200"> -->
		       </applet>
		</div>
		<?php } ?>
		<div class='contextMenu' id='myTransactionMenu' style='display:none'>
			<ul>
			    <li id="print_transaction_receipt">Print Receipt</li>
			    <?php for ($i = 0; $i <5; $i++) {
			   		//$rec_tran = $recent_transactions[$i];
			    ?>
			    <li id="add_tip_<?=$i?>">Add Tip -</li>
			    <?php } ?>
			    <li id="add_tip">Add Tip</li>
			</ul>
		</div>
		<div id="feedback_bar"></div>
	</div>
	<div class='clear'></div>
</div>

<div id='payments_html' style='display:none'>
	<div id='payment_html_contents'>
	<?php $this->load->view('food_and_beverage/payments_list');?>
	<?php echo form_open("food_and_beverage/add_payment",array('id'=>'add_payment_form')); ?>
	<ul id="error_message_box"></ul>
	<fieldset id="giftcard_payment_info">
	<!-- <legend><?php echo lang("giftcards_charge_giftcard"); ?></legend> -->
	<div id='member_account_info' class="field_row clearfix">
	<?php echo form_label('Amount tendered:', 'amount_tendered',array('class'=>'required')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'amount_tendered',
			'id'=>'amount_tendered',
			'size'=>'20',
			'autocomplete'=>'off',
			'value'=>to_currency_no_money($basket_amount_due),
			'style'=>'text-align:right;'
			)
		);?>
		</div>
	</div>
	<div id='giftcard_data' style='display:none'>
		<div id='member_account_info_3' class="field_row clearfix">
		<?php echo form_label('Giftcard #:', 'giftcard_number',array('class'=>'required')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'giftcard_number',
				'id'=>'giftcard_number',
				'size'=>'20',
				'value'=>'',
				'autocomplete'=>'off',
				'maxlength'=>'16')
			);?>
			</div>
		</div>
		<div class='clear' style='text-align:center'>
		<?php
		echo form_button(array(
			'name'=>'back',
			'id'=>'back',
			'value'=>'Back',
			'content'=> 'Back',
			'class'=>'back_button float_right')
		);
		?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
		</div>
	</div>
	<div id='punch_card_data' style='display:none'>
		<div id='member_account_info_2' class="field_row clearfix">
		<?php echo form_label('Punch Card #:', 'punch_card_number',array('class'=>'required')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
				'name'=>'punch_card_number',
				'id'=>'punch_card_number',
				'size'=>'20',
				'value'=>'',
				'autocomplete'=>'off',
				'maxlength'=>'16')
			);?>
			</div>
		</div>
		<div class='clear' style='text-align:center'>
		<?php
		echo form_button(array(
			'name'=>'back',
			'id'=>'back2',
			'value'=>'Back',
			'content'=> 'Back',
			'class'=>'back_button float_right')
		);
		?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit2',
			'value'=>lang('common_submit'),
			'class'=>'submit_button float_right')
		);
		?>
		</div>
	</div>
	</fieldset>

	<style>
		html[xmlns] div.hidden {
			display:none;
		}
	</style>

	<div id=''>

	</div>
	<div id="make_payment">
	<?php
		$payment_window_info = array("mode" 	 => $mode,
	                           		 "cab_name"  => $cab_name,
	                           		 "cmb_name"  => $cmb_name);
        $this->load->view("food_and_beverage/payment_window", $payment_window_info);
    ?>
	</div>
	<div id="split_payments_html">
	<?php $this->load->view('food_and_beverage/split_payments'); ?>
	</div>
</div>
</div>

<script type='text/javascript'>

	//validation and submit handling
	$(document).ready(function()
	{
		<?php if ($this->sale_lib->get_table_number()) { ?>
	   			$('#menubar_stats').html('Table #<?=$this->sale_lib->get_table_number()?>');
	   	<?php } ?>
//		$("#amount_tendered").val($('#giftcard_amount').val());
		var at = $('#amount_tendered');
			at.live('keypress',function(event){
				// Submit on enter
				console.log('at.k '+event.keyCode);
		        if ( event.keyCode == 13) {
		        	$('#payment_cash').click();
		        }
			});
		var submitting = false;
	    $('#add_payment_form2').validate({
			submitHandler:function(form)
			{
				giftcard_swipe = false;
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				submitting_payment = true;
				//$("input:[name=payment_type]").val('Gift Card');
				//$("input:[name=payment_gc_number]").val($('#giftcard_number').val());
				$("#amount_tendered").val($('#giftcard_amount').val());

				//$("#amount_tendered").focus();
				//mercury.add_payment();
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
				amount_tendered: "required",
				giftcard_number: function(element) {
		        	return $("input:[name=payment_type]").val() == 'Gift Card';
		      	}
	   		},
			messages:
			{
	     		amount_tendered: "<?php echo lang('giftcards_value_required'); ?>",
	     		giftcard_number: "<?php echo lang('giftcards_number_required'); ?>"
			}
		});

		<?php
			if ($selected_suspended_sale_id != '') {
				echo "fnb.setup_suspend_button('".$selected_suspended_sale_id."');";
			} else {
				echo "fnb.setup_suspend_button('', '". lang('food_and_beverage_select_table') ."');";
			}
		?>
	});
	</script>

	<script>
	var giftcard_swipe = false;
		$(document).ready(function(){
			var gcn = $('#giftcard_number');
			gcn.keydown(function(event){
				// Allow: backspace, delete, tab, escape, and enter
		        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
		             // Allow: Ctrl+A
		            (event.keyCode == 65 && event.ctrlKey === true) ||
		             // Allow: home, end, left, right
		            (event.keyCode >= 35 && event.keyCode <= 39)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        else {
		            // Ensure that it is a number and stop the keypress
		            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		            	console.log('kc '+event.keyCode);
		            var kc = event.keyCode;
		            if (giftcard_swipe && !((kc >= 48 && kc <=57) || (kc >= 96 && kc <= 105) || kc == 13)) //Allow numbers only and enter
		            {
		            	console.log('numbers only');
	            		event.preventDefault();
	            	}
	            	else if (kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/|| (event.shiftKey && kc == 53) /*percentage sign*/)
		            {
		            	console.log('blocking special characters');
		            	giftcard_swipe = true;
		                event.preventDefault();
		            }
		        }
			});
			//gcn.focus();
		})
	</script>

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
<?php
if(isset($error))
{
	echo "set_feedback('$error','error_message',false);";
}

if (isset($warning))
{
	echo "set_feedback('$warning','warning_message',false);";
}

if (isset($success))
{
	echo "set_feedback('$success','success_message',false);";
}
?>
var mercury = {
	is_active:function(){
		return <?php echo (($this->config->item('mercury_id')!='' && $this->config->item('mercury_password')!='') || $this->config->item('ets_key')!='')?1:0; ?>;
	},
	payment_window:function(declined){
		declined = declined==undefined?false:declined;
		//$.colorbox.close();

		var receipt_id = $('#payment_receipt_id').val();
		var amount_tendered = Number($('#amount_tendered').val());
		var receipt_due_amount = get_receipt_total(receipt_id);
		var total_due_amount = parseFloat($('#due_amount').split("$")[1]);
		var total_tax = 0;

		/*
		if ($('.register_taxes.right')[0]) {
			total_tax = Number($($('.register_taxes.right')[0]).html().replace(/[^0-9\.]+/g,""));
			if ($('.register_taxes.right')[1])
				total_tax += Number($($('.register_taxes.right')[1]).html().replace(/[^0-9\.]+/g,""));
		}
		var total_in_cart = Number($('#basket_total').html().replace(/[^0-9\.]+/g,""));
		*/

		if (amount_tendered > receipt_due_amount){
			alert('Error: Charging more than due.');
		}else if (amount_tendered <= 0){
			alert('Error: Amount must be greater than 0.');
		}else{
			$.colorbox({href:'index.php/food_and_beverage/open_payment_window/POS/Sale/OneTime/'+amount_tendered.toFixed(2)+'/'+(Number(total_tax)).toFixed(2)+'/'+declined,'width':702,'height':600});
		}
	},
	payments_window:function(){
		var submitting = false;
		$.colorbox2({
			inline:true,
			href:'#payment_html_contents',
			title:'Add Payment',
			onComplete:function(){
				$('#amount_tendered').focus();
				$('#amount_tendered').select();

				$('#add_payment_form').validate({
				    submitHandler:function(form)
				    {
				        if (submitting) return;
				        submitting = true;
				        $('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
				        $(form).ajaxSubmit({
					        success:function(response)
					        {
					        	var receipt_id = $('#payment_receipt_id').val();
					        	$('#giftcard_number').val('');
					        	$('#giftcard_data').hide();
								$('#punch_card_number').val('');
								$('#payment_receipt_id').val('');
					        	$('#punch_card_data').hide();
					        	$('#member_account_info').show();
								$('#make_payment').show();
						        $.colorbox.resize();

					            submitting = false;

								if (response.payments_cover_total && parseInt(response.amount_due) == 0)
								{
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid');
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid');
									$.colorbox2.close();
									mercury.complete_sale();
								}
								else if (response.payments_cover_total)
								{
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid');
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid');
									$.colorbox2.close();
									mercury.complete_sale_window();
								}
								else
								{
									$('div.receipt[data-receipt-id="'+receipt_id+'"]').addClass('paid');
									$.get('<?php echo site_url('food_and_beverage/receipt_paid'); ?>/' + sale_id +'/'+ receipt_id);

		                      	    $('#cboxLoadedContent').unmask();
		                      	    $('#giftcard_data').hide();
									$('#punch_card_data').hide();
						        	$('#member_account_info').show();
									$('#make_payment').show();
									$.colorbox2.close();

									fnb.update_page_sections(response);
						        }
						        $('#cboxLoadedContent').unmask();
					        },
				            dataType:'json'
				        });
				    },
			       errorLabelContainer: "#error_message_box",
			       wrapper: "li"
			    });
			},
			width:500
		});
	},
	giftcard_window:function(){
		$.colorbox({href:'index.php/food_and_beverage/open_giftcard_window/'+$('#amount_tendered').val(),'width':400,'title':'Charge Gift Card', onComplete:function(){$('#giftcard_number').focus()}});
	},
	close_window:function(){
		$.colorbox.close();
	},
	add_payment:function() {
		console.log('trying add_payment');
	    $('#add_payment_form').submit();



	    console.log('finished add_payment');
	    //$('#submit').click();
	},
	delete_payment:function(payment_type, payment_amount) {
		var answer = true;
		if (payment_type == 'Cash' ||
			payment_type == 'Check' ||
			payment_type == 'Gift Card' ||
			payment_type == 'Punch Card' ||
			payment_type == 'Debit Card' ||
			payment_type == 'Credit Card')

			//window.location = 'index.php/food_and_beverage/delete_payment/'+payment_type;
			answer = true;
		else {
			var answer = confirm('Refund '+payment_amount+' to '+payment_type);
			//if (answer)
			//	window.location = 'index.php/food_and_beverage/delete_payment/'+payment_type;
		}

		if (answer)
		{
			$('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
			$.ajax({
               type: "POST",
               url: "index.php/food_and_beverage/delete_payment/",
               data: {
               		payment_id:payment_type
               },
               success: function(response){
               		$('.payments_and_tender').replaceWith(response.payments);
					$('#amount_tendered').val(response.amount_due).select();
               		$('#cboxLoadedContent').unmask();
               		$.colorbox.resize();
			   },
			   dataType:'json'
            });
        }
	},
	complete_sale_window:function(){
		$.colorbox2({href:"index.php/food_and_beverage/complete_sale_window",width:650,height:170});
	},
	complete_sale:function(){
		//submitting_sale = true;
		$("#finish_sale_form").submit();
	},
	encrypted_data:'',
    encrypted_key:'', //if it is one at a time
    purchase:'1.00'

};

var submitting_sale = false;
var submitting_payment = false;
var has_cc_payment = 0;
sale_id = <?php echo (int) $sale_id; ?>;

$(document).ready(function()
{
	// ********* Begin split payments code *********
	$('#payments_button').colorbox({
		inline: true,
		href: '#split_payments_html',
		title: 'Split Payments',
		width: 1100,
		height: 750,
		cache: false
	});

	// Create new receipt
	$('#split_payments a.new-ticket').live('click', function(){
		var newReceiptId = parseInt($('.receipt-container').length) + 1;
		$('#split_payments div.receipt').removeClass('selected');
		$('div.receipts div.scroll-content').append(
			'<div class="receipt-container">'+
			'<div class="receipt selected" data-receipt-id="'+newReceiptId+'">'+
				'<div class="title">'+
					'<span>Receipt #'+newReceiptId+'</span>'+
					'<a class="fnb_button delete-receipt">Delete</a>'+
					'<span class="is-paid">PAID</span>'+
				'</div>'+
			'</div>'+
			'<a class="fnb_button pay disabled">Pay Now</a></div>'
		);
		var numReceipts = parseInt($('div.scroll-content div.receipt-container').length);

		if(numReceipts % 2 == 1){
			var receiptScroll = (numReceipts - 1) * 310;
		}else{
			var receiptScroll = (numReceipts - 2) * 310;
		}

		// Scroll to newly created receipt
		$('#split_payments div.scroll-content').css("left", -receiptScroll);
		return false;
	});

	// Pay receipt
	$('#split_payments a.pay').live('click', function(){

		if($(this).hasClass('disabled')){
			return false;
		}
		var receipt = $(this).siblings('div.receipt')
		var receiptId = receipt.attr('data-receipt-id');
		var receiptTotal = parseFloat(get_receipt_total(receiptId));
		var cartTotal = parseFloat($('#register_container div.due_amount').text().split("$")[1]);

		// If this is the last receipt being paid and there is a remainde
		// cent, add it to the receipt.
		var remainder = (cartTotal - receiptTotal).toFixed(2);
		if(remainder == -0.01 || remainder == 0.01){
			receiptTotal += parseFloat(remainder);
		}
		console.debug(cartTotal);
		console.debug(receiptTotal);
		console.debug(remainder);

		$('#amount_tendered').val(receiptTotal.toFixed(2));
		$('#payment_html_contents div.due_amount').html('$'+receiptTotal.toFixed(2));
		$('#payment_receipt_id').val(receiptId);

		mercury.payments_window();
		return false;
	});

	// Add item to selected receipt
	$('#split_payments a.add-to-receipt').live('click', function(){
		var name = $(this).text();
		var lineNum = $(this).attr('data-line');
		var itemId = $(this).attr('data-item-id');
		var price = $(this).attr('data-price');
		var seat = $(this).attr('data-seat');
		var targetReceipt = $('div.receipt.selected');
		var receipt_id = targetReceipt.attr('data-receipt-id');

		if(targetReceipt.hasClass('paid')){
			return false;
		}

		// Add item to proper receipt
		if(targetReceipt.find('a.item[data-line='+lineNum+']').length <= 0){
			$('div.receipt.selected').append('<a class="fnb_button item" data-line="'+ lineNum +'" data-item-id="'+ itemId +'" data-price="'+ price +'" href="#">'+
			'<span class="seat">'+seat+'</span>'+
			name +
			'</a>');

			// Post item to server to save it in back end
			$.post('<?php echo site_url('food_and_beverage/add_receipt_item'); ?>/'+receipt_id,
				{line:lineNum, item_id:itemId, sale_id:sale_id},
				function(response){

			});
			targetReceipt.siblings('a.pay').removeClass('disabled');
		}
		return false;
	});

	// Select a receipt
	$('#split_payments div.receipt').live('click', function(){
		$('#split_payments div.receipt').removeClass('selected');
		$(this).addClass('selected');
		return false;
	});

	// Delete receipt
	$('#split_payments a.delete-receipt').live('click', function(){
		var receipt = $(this).parents('div.receipt');
		var receiptContainer = receipt.parents('div.receipt-container');
		var receiptId = receipt.attr('data-receipt-id');

		receiptContainer.remove();
		$.post('<?php echo site_url('food_and_beverage/delete_receipt'); ?>/'+sale_id+'/'+receiptId, null, function(response){

		});
		return false;
	});

	// Remove item from receipt
	$('#split_payments div.receipt a.item').live('click', function(){

		var lineNum = $(this).attr('data-line');
		var itemId = $(this).attr('data-item-id');
		var targetReceipt = $(this).parents('div.receipt');
		var receiptId = targetReceipt.attr('data-receipt-id');

		if(targetReceipt.hasClass('paid')){
			return false;
		}

		$.post('<?php echo site_url('food_and_beverage/remove_receipt_item'); ?>/'+sale_id+'/'+receiptId,
			{line:lineNum, item_id:itemId},
			function(response){

			}
		);

		$(this).remove();
		if(targetReceipt.find('a.item').length == 0){
			targetReceipt.siblings('a.pay').addClass('disabled');
		}

		return false;
	});

	// Scroll receipts RIGHT
	$('#split_payments a.right').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;
		var numReceipts = parseInt($('div.scroll-content div.receipt-container').length);
		var totalReceiptWidth = numReceipts * 310;

		if(Math.abs(curVal - 620) < totalReceiptWidth){
			$('div.scroll-content').css("left", curVal - 620);
		}
		return false;
	});

	// Scroll receipts LEFT
	$('#split_payments a.left').live('click', function(e){
		var curVal = $('div.scroll-content').position().left;

		if(curVal <= -620){
			$('div.scroll-content').css("left", curVal + 620);
		}else{
			$('div.scroll-content').css("left", 0);
		}
		return false;
	});
	// ********* End split payments code **********


	//Prevent people from leaving the page without completing sale
	$('#menubar_navigation td a').click(function(e){
		if (($('.reg_item_top').length > 0 || $('#payment_contents').length > 0) &&
			!confirm('You have not completed this sale. Are you sure you want to leave this page?')){
			e.preventDefault();
		}
	});

	//Print receipt if in queue
	$('.colbox').colorbox({
		width:550,
		href:function(){
			return $(this).attr('href')+'/'+$('#amount_tendered').val();
		}
	});
	//$('input:checkbox:not([safari])').checkbox();
	$('ul.sf-menu').superfish();
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('#new_customer_button').colorbox({'maxHeight':700, 'width':550});
	$('.edit_customer').colorbox({'maxHeight':700, 'width':550});
	$('#quickbutton_menu').qtip({
	   content: {
	      text: $('#quickbutton_section'),
	   },
	   show: {
	      event: 'click'
	   },
	   hide: {
	      event: 'click'
	   },
	   position: {
	      my: 'top center',  // Position my top left...
	      at: 'bottom center', // at the bottom right of...
	      target: $('#quickbutton_menu'), // my target
	      adjust: {
	          method: 'none shift'
	      }
	   }
	});
	<?php if(!(count($payments) > 0 && $payments_cover_total)) { ?>
	$('#item').focus();
	<?php } ?>
	$('#comment').change(function()
	{
		$.post('<?php echo site_url("food_and_beverage/set_comment");?>', {comment: $('#comment').val()});
	});

	$('#email_receipt').change(function()
	{
		$.post('<?php echo site_url("food_and_beverage/set_email_receipt");?>', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
	});


    $("#finish_sale_button").click(function()
    {
		if (submitting_sale)
			console.log('already submitting');
		else
    	{
			submitting_sale = true;
    		$('#finish_sale_form').submit();
    	}
    });
    $('#finish_sale_button').dblclick(function(){});
    $("#finish_sale_no_receipt_button").click(function()
    {
        $('#no_receipt').val(1);
		if (submitting_sale)
			console.log('already submitting');
		else
		{
			submitting_sale = true;
			$('#finish_sale_form').submit();
		}
    });
    $('#finish_sale_no_receipt_button').dblclick(function(){});
	$('#open_cash_drawer').click(function(){
       open_cash_drawer();
    });
    $('#override_sale').click(function(){
    	fnb.override();
    });
    <?php if ($this->config->item('print_after_sale')) { ?>
       var submitting = false;
    $('#finish_sale_form').validate({
               submitHandler:function(form)
               {
                   if (submitting) return;
                   submitting = true;
                   $(form).mask("<?php echo lang('common_wait'); ?>");
                   $(form).ajaxSubmit({
                   success:function(response)
                   {
                   		if (response.success === false)
                   		{
                   			$.colorbox.close();
                   			fnb.login.show();
                   			set_feedback(response.message,'error_message',false);
                   			return;
                   		}
                   		fnb.update_page_sections(response);
                        //post_item_form_submit(response);
		                submitting = false;
		                //Old Print method... to be phased out... has issues when printing
		                <?php if ($this->config->item('print_sales_receipt')) { ?>
		                //Print receipt
		                if ($('#no_receipt').val() != 1)
		                {
			                var receipt_data = '';
			                // Items
							var data = response.receipt_data;

			                has_cc_payment = 0;
			                var receipt_data = build_receipt(data);
			                console.log('print_two_other <?=$this->config->item('print_two_receipts_other')?>');
 			                console.log('print_two <?=$this->config->item('print_two_receipts')?>');
 			                console.log('has_cc_payment'+(has_cc_payment ? 'yes' : 'no'));
 			                if (('<?=$this->config->item('print_two_receipts_other')?>' == '1' && !has_cc_payment) || (has_cc_payment && '<?=$this->config->item('print_two_receipts')?>' == '1')) {
				                for(var i = 1; i <= 2; i++)
       				                print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
			                }
			                else
				                print_receipt(receipt_data, false, data.sale_id, response.receipt_data);
			                //Reload page or go to teesheet
		                }
		                <?php } ?>
    					//fnb.update_page_sections(response);
    					$('#menubar_stats').html('');
					   	fnb.show_tables();
						$.colorbox2.close();
		           },
                       dataType:'json'
               });

               },
               errorLabelContainer: "#error_message_box",
               wrapper: "li"
       });
<?php } ?>
    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            $('#cancel_sale_form').submit();
    	}
    });
    $("#delete_button").click(function()
    {
    	if (confirm('<?php echo lang("sales_confirm_cancel_sale"); ?>'))
    	{
            //$('#delete_form').submit();
            fnb.delete_checked_lines();
    	}
    });

	$("#payment_types").change(checkPaymentTypeGiftcard).ready(checkPaymentTypeGiftcard);
	var ct = $('#column_transactions');
	ct.click(function(){
    	if (ct.hasClass('expanded'))
		{
			ct.removeClass('expanded');
    		$('#recent_transactions').hide();
		}
		else
		{
    		ct.addClass('expanded');
    		$('#recent_transactions').show();
		}
	});
	var ssh = $('#suspended_sales_header');
    ssh.click(function(){
    	if (ssh.hasClass('expanded'))
		{
			ssh.removeClass('expanded');
    		$("#suspended_sales").hide();
		}
		else
		{
    		ssh.addClass('expanded');
    		$("#suspended_sales").show();
		}
	});
    <?php if ($receipt_data) {
		?>
	 	console.log('attempting to print receipt');

		//*setTimeout(function(){*/print_receipt("<?=$receipt_data?>", true)/*}, 1000)*/;
 	<?php }
 	echo $process_sale;
	?>
});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value", "");
		fnb.add_item(response.item_id);
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		if ($("#select_customer_form").length == 1)
		{
			$("#customer").attr("value",'');
			selectCustomer(response.person_id)
		}
		else
		{
			window.location = '<?php echo site_url('food_and_beverage/index');?>';
		}
	}
}

function checkPaymentTypeGiftcard()
{
	if ($("#payment_types").val() == "<?php echo lang('sales_giftcard'); ?>")
	{
		$("#amount_tendered").val('');
		$("#amount_tendered").focus();
	}
	else
	{

	}
}
function findPrinter() {
//     var applet = document.jZebra;
  //  if (applet != null) {
        // Searches for locally installed printer with "zebra" in the name
    //}
}
function monitorFinding() {
	var applet = document.jZebra;
	if (applet != null) {
	   if (!applet.isDoneFinding()) {
	      window.setTimeout('monitorFinding()', 100);
	   } else {
	      var printer = applet.getPrinterName();
              console.log(printer == null ? "Printer not found" : "Printer \"" + printer + "\" found");
	   }
	} else {
            console.log("Applet not loaded!");
        }
      }
function build_receipt(data){
	console.dir(data);
	var receipt_data = '';
	var cart = data.cart;
    //Itemized
    for (var line in cart)
    {
        receipt_data += add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n';
    	if (parseInt(cart[line].discount) > 0)
    		receipt_data += add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))
    }
    // Totals
    receipt_data += '\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n';
    var taxes = data.taxes;
    for (var tax in taxes)
           receipt_data += add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n';

       receipt_data += add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n';

    // Payment Types
    receipt_data += '\nPayments:\n';
    var payments = data.payments;
    for (var payment in payments)
    {
	    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
	    receipt_data += add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n';
    }
       // Change due
       receipt_data += '\n'+add_white_space('Change Due: ',(data.amount_change))+'\n';
    <?php if ($this->config->item('print_tip_line')) { ?>
    receipt_data += '\n\n'+add_white_space('Tip: ','$________.____');
    receipt_data += '\n\n'+add_white_space('TOTAL CHARGE: ','$________.____');
    receipt_data += '\n\n\nX_____________________________________________\n';
	<?php } ?>
    receipt_data += chr(27)+chr(97)+chr(49);
	receipt_data += "\x1Dh" +chr(80);  // ← Set height
    receipt_data += "\n\n";// Some spacing at the bottom
    var sale_num = data.sale_id.replace('POS ', '');
    var len = sale_num.length;
	if (len == 3)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
	else if (len == 4)
        receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 5)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 6)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 7)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 8)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 9)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
    else if (len == 10)
        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
    // Sale id and barcode
    receipt_data += '\n\nSale ID: '+data.sale_id+'\n';

    return receipt_data;
}
function print_receipt(receipt, add_delay, sale_id, data) {
	try
	{
		add_delay = add_delay == undefined ? false : add_delay;
		var applet = document.jZebra;
		if (applet != null)
		{
	        //if ($.isFunction(applet.findPrinter))
	    	{
	    		applet.findPrinter("<?=$printer_name?>");
	    		//var t = new Date();
	    		//var ct = 0;
	   			//while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
		           // Wait
		        //   console.log('not done finding yet');
		        //   c = new Date();
		        //   ct = c.getTime();
		       // }
		       if (add_delay) {
			       	var t = new Date();
		    		var ct = 0;
		   			while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
			           // Wait
			        //   console.log('not done finding yet');
			           //c = new Date();
			           //ct = c.getTime();
			        }
			        setTimeout(function(){

				        // Send characters/raw commands to applet using "append"
				        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify

				        applet.append("<?php echo $this->config->item('name')?>\n");
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify
				        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
				        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
				        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
				        applet.append("<?php echo $this->config->item('phone')?>\n\n");
				        applet.append(chr(27) + chr(97) + chr(48));// Left justify
				        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
				        var d = new Date();
				        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
				        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
				        var ap = (d.getHours() < 12)?'am':'pm';
				        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");

				        applet.append(receipt);
				        //applet.appendImage('/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('../../images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
				        applet.append(chr(27) + chr(105)); //Cuts the receipt

				        // Send characters/raw commands to printer
				        //applet.forceAccept();
				        applet.print();
				        open_cash_drawer();
				    }, 1);
			    }
			    else {
			    	console.log('not timing out');
					// Send characters/raw commands to applet using "append"
			        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
			        applet.append(chr(27)+chr(64));//Resets the printer
			        //applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify

			        applet.append("<?php echo $this->config->item('name')?>\n");
			        applet.append(chr(27)+chr(64));//Resets the printer
			        applet.append(chr(27) + "\x61" + "\x31"); // center justify
			        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
			        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
			        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
			        applet.append("<?php echo $this->config->item('phone')?>\n\n");
			        applet.append(chr(27) + chr(97) + chr(48));// Left justify
			        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
			        <?php //if ($customer != '') { ?>
			        if (data != undefined && data.customer != undefined)
				        applet.append("Customer: <?php //echo $customer; ?>"+data.customer+"\n");
			        <?php //} ?>
			        var d = new Date();
			        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
			        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
			        var ap = (d.getHours() < 12)?'am':'pm';
			        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");
			        //applet.append("<?php echo $cab_name.': '.$customer_account_balance; ?>\n");
			        //applet.append("<?php echo $cmb_name.': '.$customer_member_balance; ?>\n");
			        <?php if ($giftcard) { ?>
			        //applet.append("Giftcard Balance: <?php echo $giftcard_balance; ?>\n");
			        <?php } ?>

			        applet.append(receipt);
			        //applet.appendImage('/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        //applet.appendImage('../../images/test/test.png', "ESCP");
			        //applet.append("\r\n");
			        <?php if ($this->config->item('online_booking')) { ?>
		        	applet.append("\nAnd remember, you can book your\nnext tee time online at\n<?=$this->config->item('website')?>");
		        	<?php } ?>

			        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
			        applet.append(chr(27) + chr(105)); //Cuts the receipt

			        // Send characters/raw commands to printer
			        //applet.forceAccept();
			        applet.print();
			        open_cash_drawer();
		        }
			}
		}
	}
	catch(err)
	{
		set_feedback('Unable to print receipt at this time. Please print manually','error_message',false);
	}
}
function print_postscript_receipt(receipt, customer) {
    var applet = document.jZebra;
    if (applet != null) {
    	if ($.isFunction(applet.findPrinter))
    	{
	        applet.findPrinter("<?=$printer_name?>");
	   		while (!applet.isDoneFinding()) {
	           // Wait
	           console.log('not done finding yet');
	        }
	        // Send characters/raw commands to applet using "append"
	        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
	        applet.appendHTML("<html>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('name')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>')+"</div>");
	        applet.appendHTML("<div style='text-align:center; font-size:6px;'>"+add_nbsp('<?php echo $this->config->item('phone')?>')+"</div><br/>");
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>')+"</div>");
	        if (customer != undefined && customer != '')
	        	applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Customer: '+customer)+"</div>");
	        var d = new Date();
	        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
	        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
	        var ap = (d.getHours() < 12)?'am':'pm';
	        applet.appendHTML("<div style='font-size:6px;'>"+add_nbsp('Date: '+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap)+"</div><br/>");

	        applet.appendHTML(receipt);
	        applet.appendHTML("<br/>");
	        applet.appendHTML('<div style="font-size:6px;">______________________________________________</div>');
	        //applet.appendImage('/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
	        //applet.append("\r\n");
	        //applet.appendImage('../../images/test/test.png', "ESCP");
	        //applet.append("\r\n");

	        // Send characters/raw commands to printer
	        //applet.forceAccept();
	        console.log('Printing HTML');

			<?php if ($this->config->item('online_booking')) { ?>
	        applet.appendHTML("<br/><div style='text-align:center;'>And&nbsp;remember,&nbsp;you&nbsp;can&nbsp;book&nbsp;your<br/>next&nbsp;tee&nbsp;time&nbsp;online&nbsp;at<br/><?=$this->config->item('website')?></div>");
	        <?php } ?>
	        applet.printHTML();
	        console.log('Done Printing HTML');
        }
    }
}
function hexdec (hex_string) {
    // Returns the decimal equivalent of the hexadecimal number
    //
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/hexdec
    // +   original by: Philippe Baumann
    // *     example 1: hexdec('that');
    // *     returns 1: 10
    // *     example 2: hexdec('a0');
    // *     returns 2: 160
    hex_string = (hex_string + '').replace(/[^a-f0-9]/gi, '');
    return parseInt(hex_string, 16);
}
function update_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function delete_recent_transaction(transaction_id)
{
    $('#transaction_'+transaction_id).remove();
}
function add_nbsp(string) {
	return string.replace(' ', '&nbsp;');
}
function chr(i) {
      return String.fromCharCode(i);
}
function open_cash_drawer() {
    var applet = document.jZebra;
    if (applet != null) {
    	applet.findPrinter("<?=$printer_name?>");
        applet.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
        //applet.forceAccept();
        applet.print();
    }
}
function boldAndCenter() {
    var applet = document.jZebra;
    if (applet != null) {
        applet.append(chr(27) + chr(69) + "\r");  // bold on
        applet.append(chr(27) + "\x61" + "\x31"); // center justify
    }
}
function add_white_space(str_one, str_two)
{
       var width = 42;
       var strlen_one = str_one.length;
       var strlen_two = str_two.length;
       var white_space = '';
       var white_space_length = 0;
       if (strlen_one + strlen_two >= width)
               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
       else
               white_space_length = width - (strlen_one + strlen_two);

       for (var i = 0; i < white_space_length; i++)
               white_space += ' ';
       return str_one+white_space+str_two;
}

// FNB Overridden functions, because FNB JS is currently minified
fnb.update_page_sections = function(response) {

	if (response) {

		if(response['sale_id']){
			sale_id = response.sale_id;
		}

		if (response['message']) {
			set_feedback(response['message']['text'], response['message']['type'], response['message']['persist']);
		}
		if (response['register_box']) {
			$("#register_box").html(response['register_box']);
		}
		if (response['basket_info']) {
			fnb.update_basket_totals('all', response['basket_info']);
		}
		if (response['customer_info_filled']) {
			$("#customer_info_filled").html(response['customer_info_filled']);
		}
		if (response['payment_window']) {
			$("#make_payment").html(response['payment_window']);
		}
		if (response['payments']){
			$('.payments_and_tender').replaceWith(response.payments);
			$('.colbox').colorbox();
		}
		if (response['suspended_sales']) {
			$("#suspended_sales").html(response['suspended_sales']);
		}
		if (response['table_top']) {
			$("#table_top").html(response['table_top']);
		}
		if (response['amount_due']){
			$('#amount_tendered').val(response.amount_due).select();
		}

		if (response['suspend_button_title']) {
			fnb.setup_suspend_button('', response['suspend_button_title']);
		}
		if (response['split_payments']) {
			$("#split_payments_html").html(response['split_payments']);
		}

		if (response['is_cart_empty']) {
			// remove payment type values
			$(".payments_and_tender :form").each(function() {
				$(this).remove();
			});
		}
		if (response['table_top']) {
			if (response['mode'] == 'sale')
			{
				$("#register_items_container").removeClass('return');
				$('#mode').val('sale');
				$("#payments_button").html('Pay Now');
			}
			else
			{
				$('#mode').val('return');
				$("#register_items_container").addClass('return');
				$("#payments_button").html('Return Now');
			}
			$("#table_top").html(response['table_top']);
		}
		if (response['table_number'])
		{
			$('#menubar_stats').html('Table #'+response['table_number']);
		}
		return true;
	} else {
		document.location = document.location;
	}
	return false;
};

function get_receipt_total(receiptId){
	var numSplits = {};
	var receiptTotal = 0;

	// First, loop through all items on all receipts and figure out
	// which items are split across what number of receipts
	$('div.receipt-container').find('a.item').each(function(index, val){
		var key = $(this).attr('data-item-id') + "-" + $(this).attr('data-line');

		if(numSplits[key]){
			numSplits[key] += 1;
		}else{
			numSplits[key] = 1;
		}
	});

	// Add up the items of the receipt ID passed. Take into account which items are split
	// and divide price by number of splits
	$('div.receipt[data-receipt-id="'+receiptId+'"]').find('a.item').each(function(index, val){
		var key = $(this).attr('data-item-id') + "-" + $(this).attr('data-line');
		var price = parseFloat($(this).attr('data-price'));

		var splitPrice = parseFloat(price / numSplits[key]);
		receiptTotal += parseFloat(splitPrice.toFixed(2));
	});

	return receiptTotal.toFixed(2);
}
</script>
<div class='payments_and_tender'>
	<?php
	foreach($payments as $payment_id=>$payment)
	{
		$invoice_id = $payments[$payment_id]['invoice_id'];
		if ($invoice_id != '')
		{
			//echo anchor("food_and_beverage/confirm_delete_payment/".urlencode(urlencode($payment_id)).'/height~200', '<span class="payment_currency">'.to_currency($payment['payment_amount']).'</span><span class="payment_label">'.$payment['payment_type'].'</span><div class="clear"></div>', array('class'=>'colbox'));
			echo "<a href='javascript:mercury.delete_payment(\"".$payment_id."\", \"".to_currency($payment['payment_amount'])."\")'><span class='payment_currency'>".to_currency($payment['payment_amount'])."</span><span class='payment_label'>".$payment['payment_type'].":</span><div class='clear'></div></a>";
		}
		else if ($payment_id != 'Raincheck')
		{
			echo form_open("food_and_beverage/edit_payment/$payment_id",array('id'=>'edit_payment_form'.$payment_id));
			//echo anchor("food_and_beverage/delete_payment/".urlencode(urlencode($payment_id)), '<span class="payment_currency">'.to_currency($payment['payment_amount']).'</span><span class="payment_label">'.$payment['payment_type'].'</span><div class="clear"></div>');
			echo "<a class='delete_item' href='javascript:mercury.delete_payment(\"".$payment_id."\")'></a><span class='payment_currency'>".to_currency($payment['payment_amount'])."</span><span class='payment_label'>".$payment['payment_type'].":</span><div class='clear'></div>";
			?>
			</form>
	<?php
		}
	}
	if ($raincheck_info) {?>
		<?php echo anchor("food_and_beverage/remove_raincheck/",
			'<div><span class="payment_currency">&nbsp;</span><span class="payment_label">Raincheck:</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['gf_used']).'</span><span class="payment_label">Green Fees ('.to_currency($raincheck_info['gf_total']).')</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['cf_used']).'</span><span class="payment_label">Cart Fees ('.to_currency($raincheck_info['cf_total']).')</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['tax_used']).'</span><span class="payment_label">Taxes ('.to_currency($raincheck_info['tax_total']).')</span><div class="clear"></div></div>'
			);
 	} ?>
 	<div class='due_amount' id='due_amount'><?php echo to_currency($basket_amount_due);?></div>
 	<div class="float_left due_amount_label"><?php echo lang('sales_amount_due'); ?>:</div>
 	<?php// echo form_input(array('name'=>'amount_tendered','id'=>'amount_tendered','value'=>to_currency_no_money($basket_amount_due),'size'=>'10', 'accesskey' => 'p', 'autocomplete'=>'false'));?>
	<?php /*
		if ($mode == 'return' && count($credit_card_payments) > 0)
		{
			echo form_label('Credit Card Payment:', 'comment');
			foreach($credit_card_payments as $cc_payment)
			{
				$cardholder_name = '';
				echo " <div class='form_field'>".$cc_payment['payment_type'].' - $'.$cc_payment['amount'].$cardholder_name." (";
				if ($cc_payment['amount_refunded'] > 0)
					echo "Refund Issued";//"$".$cc_payment['amount_refunded']." refunded";
				else
					echo "<a title='Refund to CC' class='colbox' href='index.php/food_and_beverage/confirm_refund/{$cc_payment['invoice']}'>Refund to CC</a>";
				echo ")</div>";
			}
		} */
	?>
	<?php
	if ($mode == 'return' && count($sale_payments) > 0)
	{
		echo "<div class='header'>Payments collected for sale $sale_id:</div>";
		foreach ($sale_payments as $sale_payment)
		{
			echo "<div class='form_field'>".$sale_payment['payment_type'].' - $'.$sale_payment['payment_amount'];
			if ($sale_payment['invoice_id'])
				echo " (<a title='Refund to CC' class='colbox' href='index.php/food_and_beverage/confirm_refund/{$sale_payment['invoice_id']}'>Refund to CC</a>)";
			echo "</div>";
		}
	}
	?>

</div>
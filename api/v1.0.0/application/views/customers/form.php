<?php
echo form_open('customers/save/'.$person_info->person_id,array('id'=>'customer_form'));
//print_r($price_classes);
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_basic_info">
<legend><?php echo lang("customers_basic_information"); ?></legend>
<?php $this->load->view("people/form_basic_info"); ?>

</fieldset>
<fieldset id="customer_info">
<legend><?php echo lang("customers_settings"); ?><?php if ($person_info->person_id != '' && $person_info->person_id > 0){?> - <a href='index.php/customers/billing/<?=$person_info->person_id;?>/width~1200' id='billing_link'>Billing Information</a><?php } ?></legend>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_account_number').':', 'account_number'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_number',
		'id'=>'account_number',
		'size'=>'40',
		'value'=>$person_info->account_number)
	);?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_price_class').':', 'price_class'); ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('price_class', $price_classes, $person_info->price_class);
	?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_non_taxable').':', 'taxable'); ?>
	<div class='form_field'>
	<?php echo form_checkbox('taxable', '1', $person_info->taxable == '' ? FALSE : (boolean)!$person_info->taxable);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('customers_member').':', 'member'); ?>
	<div class='form_field'>
	<?php echo form_checkbox('member', '1', $person_info->member == '' ? FALSE : (boolean)$person_info->member);?>
	</div>
</div>
<div id='member_account_info' class="field_row clearfix" <?php echo ($person_info->member)?"":"style='display:none'"?>>
<?php echo form_label(($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')).':', 'member_account_balance'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'member_account_balance',
		'id'=>'member_account_balance',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->member_account_balance)
	);?>
	<?	echo form_checkbox('member_account_balance_allow_negative', 'member_account_balance_allow_negative', ($person_info->member_account_balance_allow_negative) ? true:FALSE).' Allow negative';?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')).':', 'account_balance'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'account_balance',
		'id'=>'account_balance',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->account_balance)
	);?>
	<?	echo form_checkbox('account_balance_allow_negative', 'account_balance_allow_negative', ($person_info->account_balance_allow_negative) ? true:FALSE).' Allow negative';?>
	</div>
</div>
<?php if ($this->config->item('use_loyalty')) { ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_loyalty_points').':', 'loyalty_points'); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'loyalty_points',
		'id'=>'loyalty_points',
		'class'=>'customer_balances',
		'size'=>'10',
		'value'=>$person_info->loyalty_points)
	);?>
	</div>
</div>
<?php } ?>

<?php
$first_label = true;
$group_array = array('0'=>'Select Group');
$group_checkboxes = '';
foreach($groups as $group)
{
	$group_array[$group['group_id']] = $group['label'];

	$group_checkboxes .= "<div id='checkbox_holder_".$group['group_id']."'class='field_row clearfix ".(($group['person_id'] != '' && $group['person_id'] == $person_info->person_id)?'':'hidden')."'>".
			form_label('', 'groups').
			"<div class='form_field'>".
				form_checkbox('groups[]', $group['group_id'], ($group['person_id'] != '' && $group['person_id'] == $person_info->person_id) ? true:FALSE, "id='group_checkbox_{$group['group_id']}' group_id='{$group['group_id']}'").' '.$group['label'].
			"</div>
		</div>";

$first_label = false;
} ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_groups').':', 'groups');  ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('groups', $group_array, '');
	?>
	</div>
</div>
<div class='group_checkboxes'>
<?= $group_checkboxes?>
</div>
<?php
$first_label = true;
$pass_array = array('0'=>'Select Pass');
$pass_checkboxes = '';
foreach($passes as $pass)
{
	$pass_array[$pass['pass_id']] = $pass['label'];

	$pass_checkboxes .= "<div id='checkbox_holder_".$pass['pass_id']."'class='field_row clearfix ".(($pass['person_id'] != '' && $pass['person_id'] == $person_info->person_id)?'':'hidden')."'>".
			form_label('', 'passes').
			"<div class='form_field'>".
				form_checkbox('passes[]', $pass['pass_id'], ($pass['person_id'] != '' && $pass['person_id'] == $person_info->person_id) ? true:FALSE, "id='pass_checkbox_{$pass['pass_id']}' pass_id='{$pass['pass_id']}'").' '.$pass['label'].
				"<div class='dates'>".
				"<span class='date_label'>Start:</span>".
				form_input(array(
					'name'=>'start_date_'.$pass['pass_id'],
					'id'=>'start_date_'.$pass['pass_id'],
					'size'=>'10',
					'value'=>$pass['start_date'],
					'class'=>'start_date')).
				"<span class='date_label'>End:</span>".
				form_input(array(
					'name'=>'expiration_'.$pass['pass_id'],
					'id'=>'expiration_'.$pass['pass_id'],
					'size'=>'10',
					'value'=>$pass['expiration'],
					'class'=>'expiration')).
				"</div>".
			"</div>
		</div>";

$first_label = false;
} ?>
<div class="field_row clearfix">
<?php echo form_label(lang('customers_passes').':', 'passes');  ?>
	<div class='form_field'>
	<?php
		echo form_dropdown('passes', $pass_array, '');
	?>
	</div>
</div>
<div class='pass_checkboxes'>
<?= $pass_checkboxes?>
</div>
<?php
if (!$this->permissions->is_employee())
{
?>
<div id='customer_password_box' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_username').':<span class="required">*</span>', 'username',array('class'=>'')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'username',
			'size'=>'30',
			'id'=>'username',
			'value'=>$person_info->username));?>
		</div>
	</div>

	<?php
	$password_label_attributes = $person_info->person_id == "" ? '<span class="required">*</span>':'';
	?>

	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_password').':'.$password_label_attributes, 'password',array()); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'size'=>'30',
			'name'=>'password',
			'id'=>'password'
		));?>
		</div>
	</div>


	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_repeat_password').':'.$password_label_attributes, 'repeat_password',array()); ?>
		<div class='form_field'>
		<?php echo form_password(array(
			'autocomplete'=>'off',
			'size'=>'30',
			'name'=>'repeat_password',
			'id'=>'repeat_password'
		));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<span style='font-size:11px; padding:10px;'><?php echo lang('customers_password_change_notification');?></span><br/>
		<?php if ($person_info->username) { ?>
		<span style='font-size:11px; padding:10px 16px;'><?php echo lang('customers_or_send_password_change_email')." <span id='send_password_change_email'>".lang('customers_click_here').'</span>';?></span>
		<?php } ?>
	</div>
</div>
<script type='text/javascript'>
	$('#customer_password_box').expandable({
		title : 'Customer Online Credentials:'
	});
</script>
<?php
}
?>
</fieldset>
<div class='clear' style='text-align:center'>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</div>
<?php
echo form_close();
?>
<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	// Image library handling
	$('#select-image').colorbox2({'maxHeight':750,'width':1100});

	$(document).unbind('changeImage').bind('changeImage', function(event){
		$.post('<?php echo site_url('customers/save_image'); ?>/<?php echo $person_info->person_id; ?>', {image_id: event.image_id}, function(response){
			$('#image-info img').attr('src', response.thumb_url);
			$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/customers?crop_ratio=1&image_id=' + event.image_id);
			$.colorbox2.close();
		},'json');
	});

	$('#remove-image').click(function(event){
		var url = $(this).attr('href');

		if(confirm('Remove this image?')){
			$.post(url, {image_id:0}, function(response){
				$('#image-info img').attr('src', response.thumb_url);
				$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/customers?crop_ratio=1&image_id=' + response.image_id);
			},'json');
		}
		return false;
	});

	$('#send_password_change_email').click(function(e){
		console.log('sending_reset_password_email');
		var username = $('#username').val();
		$.ajax({
           type: "POST",
           url: 'index.php/customer_login/do_reset_password_notify',
           data: {'username':username,'course_id':"<?=$this->session->userdata('course_id')?>"},
           success: function(response){
           		alert('Password Reset Email sent successfully.')
		   }
        });
	});
	$('#billing_link').colorbox({'title':"Member Billing <input id='customer_name' type='text' placeholder='Search customers' value='<?=addslashes($person_info->last_name);?>, <?=addslashes($person_info->first_name);?>'/>", onComplete:function(){
		customer.billing.initialize_customer_search();
	}});
	$('.expiration, .start_date').datepicker({dateFormat:'yy-mm-dd'});
	var member_box = $('input[name=member]');
	var member_account_box = $('#member_account_info');
	member_box.change(function(e){
		(member_box.attr('checked')) ? member_account_box.show() : member_account_box.hide();
	})
	$('#groups').change(function(){
		var group_select = $(this);
		var group_id = group_select.val();
		$('#checkbox_holder_'+group_id).show();
		$('#group_checkbox_'+group_id).attr('checked', 'checked');
		group_select.val(0);
		$.colorbox.resize();
	});
	$('input[name=groups[]]').click(function(){
		var group_checkbox = $(this);
		var group_id = group_checkbox.attr('group_id');
		var checked = group_checkbox.attr('checked');
		console.log('gid '+group_id+' checked '+checked);
		if (!checked)
			$('#checkbox_holder_'+group_id).hide();
		$.colorbox.resize();
	})
	$('#passes').change(function(){
		var pass_select = $(this);
		var pass_id = pass_select.val();
		$('#checkbox_holder_'+pass_id).show();
		$('#pass_checkbox_'+pass_id).attr('checked', 'checked');
		pass_select.val(0);
		$.colorbox.resize();
	});
	$('input[name=passes[]]').click(function(){
		var pass_checkbox = $(this);
		var pass_id = pass_checkbox.attr('group_id');
		var checked = pass_checkbox.attr('checked');
		if (!checked)
			$('#checkbox_holder_'+pass_id).hide();
		$.colorbox.resize();
	})
	var submitting = false;
    $('#customer_form').validate({
		submitHandler:function(form)
		{
			//prevent member and account balances from going below if 0 if a neg balance is not checked
			var invalid_input, href, is_available, customer_id, username;
			$('.customer_balances').each(function(){
				var checkbox = $(this).siblings(),
					is_checked = $(checkbox).attr('checked'),
					amount = parseInt($(this).val());

				if (!is_checked && (amount < 0))
				{
					invalid_input = $(this);
				}
			});

			if (invalid_input){
				set_feedback("<?php echo lang('negative_balance_not_allowed'); ?>",'error_message',false);
				$(invalid_input).select();
				return;
			}

			//check if username is available
			is_available = false;
			customer_id =
			username = $('#username').val();
			href = "index.php/customers/validate_user_name/<?=$person_info->person_id;?>/"+encodeURIComponent(username);
			$.ajax({
               type: "POST",
               url: href,
               async: false,
               success: function(response){
               		is_available = response;
			   }
            });

            if (is_available === 'false'){
            	set_feedback("<?php echo lang('username_in_use'); ?>",'error_message',false);
            	return;
            }

            // console.log(is_available);
            // return;

			//validate username with ajax call to the server
			// var username = $('#username').val();
			// console.log(username);
			// console.log(	"Slammer");
			// return;
			//end of username validation

			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
				post_person_form_submit(response);
                submitting = false;
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			first_name: "required",
			last_name: "required",
			<?php
			if (!$this->permissions->is_employee() && $this->config->item('online_booking_protected'))
			{
			?>
    		username:
			{

				minlength: 5
			},
			password:
			{
				minlength: 8
			},
			repeat_password:
			{
 				equalTo: "#password"
			},
			<?php } ?>
    		email: "email"
   		},
		messages:
		{
     		first_name: "<?php echo lang('common_first_name_required'); ?>",
     		last_name: "<?php echo lang('common_last_name_required'); ?>",
     		<?php
			if (!$this->permissions->is_employee() && $this->config->item('online_booking_protected'))
			{
			?>
     		username:
     		{
     			minlength: "<?php echo lang('employees_username_minlength'); ?>"
     		},
     		password:
			{
				minlength: "<?php echo lang('employees_password_minlength'); ?>"
			},
			repeat_password:
			{
				equalTo: "<?php echo lang('employees_password_must_match'); ?>"
     		},
     		<?php } ?>
     		email: "<?php echo lang('common_email_invalid_format'); ?>"
		}
	});
});

</script>
<style>
	#send_password_change_email {
		color:#ccc;
		cursor:pointer;
	}
	html[xmlns] div.hidden {
		display:none;
	}
	.dates {
		float:right;
	}
	.dates .date_label {
		padding:0px 5px 0px 10px;
	}
	.new_charge_data {
		float:left;
	}
	#new_charge_form {
		font-size: 12px;
		padding: 5px 30px 0px;
	}
	#amount {
		width:60px;
	}
	.first_row {
		margin-bottom:10px;
	}
	#description {

	}
	#subscription {

	}
	#start_date {
		width:85px;
	}
	#save_new_charge {
		float:right;
		margin-top:0px;
	}
</style>

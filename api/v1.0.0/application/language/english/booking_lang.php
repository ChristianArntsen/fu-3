<?php
$lang['booking_error_no_teetimes'] = 'No teetimes available. Please select a different day/time.';
$lang['booking_billing_info']='Billing Info';
$lang['booking_payment_method']='Payment method';
$lang['booking_one_required']='one required';
$lang['booking_billing_month_day']='Billing month/day';
$lang['booking_billing_day']='Billing day';
$lang['booking_week']='Week';
$lang['booking_teesheet']='Tee Sheet';
?>
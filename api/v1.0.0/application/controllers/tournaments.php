<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Tournaments extends Secure_area implements iData_controller
{
	function __construct()
	{		
		parent::__construct('tournaments');		
	}

	function index()
	{
		$config['base_url'] = site_url('tournaments/index');				
		$config['total_rows'] = $this->Tournament->count_all();		
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
 		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();		
		$data['manage_table']=get_tournament_manage_table($this->Tournament->get_all($config['per_page'], $this->uri->segment(3)),$this);
	
		$this->load->view('tournaments/manage',$data);
	}   
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_tournament_manage_table_data_rows($this->Tournament->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
	}
	
	function suggest()
	{		
		$suggestions = $this->Tournament->get_search_suggestions($this->input->get('term'),100);		
		echo json_encode($suggestions);
	}
	
	function get_row(){}

	function view($tournament_id=-1)
	{		
		$data = array();
		$data['tournament_info']=$this->Tournament->get_info($tournament_id);	
		
		//cart fee tax item
		$item_number = $this->session->userdata('course_id').'_1';
		$item_id = $this->Item->get_item_id($item_number);
		$data['cart_fee_tax_item'] = $this->Item_taxes->get_info($item_id);
		
		//green fee tax item		
		$item_number = $this->session->userdata('course_id').'_3';
		$item_id = $this->Item->get_item_id($item_number);
		$data['green_fee_tax_item'] = $this->Item_taxes->get_info($item_id);
		
		$this->load->view("tournaments/form",$data);
	}

	function award($tournament_id)
	{
		$data = array();
		$data['tournament_info']=$this->Tournament->get_info($tournament_id);							 		
		$this->load->view("tournaments/award_form",$data);
	}

	function save_awards($tournament_id=-1)
	{			
		$person_id = $this->input->post("person_id");		
		$description = $this->input->post("tournament_winner_description");
		$amount = $this->input->post("tournament_winner_amount");
		$deleted = $this->input->post("deleted");
		$previous_award_amount = $this->input->post("previous_award_amount");
		
		foreach($this->input->post('tournament_winner_id') as $row => $tournament_winner_id)
			{						
				$tournament_winners[] = array(
					'tournament_winner_id' => $tournament_winner_id,
					'tournament_id' => $tournament_id,					
					'person_id' => $person_id[$row],
					'description' => $description[$row],					
					'amount' => $amount[$row],
					'previous_award_amount' => $previous_award_amount[$row],
					'deleted' => $deleted[$row]				
					);					
			}			
		
		if($this->Tournament_winners->save($tournament_winners, $tournament_id)){
			//update the remaining balance for the tournament
			$remaining_balance = $this->input->post("remaining_pot_balance");
			$this->Tournament->update_remaining_pot_balance($tournament_id, $remaining_balance );
			
			echo json_encode(array('success'=>true,'message'=>lang('tournaments_winners_updated').' '.
				$tournament_data['name'],'tournament_id'=>$tournament_data['tournament_id']));
		}
		else 
		{
			echo json_encode(array('success'=>false,'message'=>lang('tournaments_error_adding_updating').' '.
			$tournament_data['name'],'tournament_id'=>$tournament_id));
		}	
	}
	
	function save($tournament_id=-1)
	{
		$carts_issued = $this->input->post('carts_issued')? 1 : 0;		
		// $carts_issued = $carts_issued == '' ? 0 : $carts_issued;
		$tax_included = $this->input->post('tax_included')? 1 : 0;
				
		$tournament_data = array(
			'name'=>$this->input->post('tournament_name'),
			'green_fee'=>$this->input->post('green_fee'),
			'carts_issued'=>$carts_issued,			
			'cart_fee'=>$this->input->post('cart_fee'),
			'pot_fee'=>$this->input->post('pot_fee'),
			'customer_credit_fee'=>$this->input->post('customer_credit_fee'),
			'member_credit_fee'=>$this->input->post('member_credit_fee'),
			'total_cost'=>$this->input->post('total_tournament_price'),
			'tax_included'=>$tax_included						
		);		
		
		$tournament_items = array();
		$total_tax = 0;
		
		foreach($this->input->post('tournament_inventory_item_id') as $index => $item_id)
		{						
			$tournament_items[] = array(
				'item_id' => $item_id,
				'price' => $this->input->post("tournament_inventory_item_price_{$item_id}"),					
				'quantity' => $this->input->post("tournament_inventory_item_quantity_{$item_id}")				
				);
				
			$item = $this->Item->get_info($item_id);
			$tax_info = $this->Item_taxes->get_info($item_id);			
			


			//get taxes for the tournament_inventory_items	
			$price = $tournament_items[$index]['price'];
			$quantity = $tournament_items[$index]['quantity'];					
			foreach($tax_info as $key=>$tax)
			{
				//$name = $tax['percent'].'% ' . $tax['name'];
			
				if ($tax['cumulative'])
				{
					$tax_amount=(($price*$quantity) + $prev_tax)*(($tax['percent'])/100);					
				}
				else
				{
					$tax_amount=($price*$quantity)*(($tax['percent'])/100);
				}
				$total_tax += number_format($tax_amount,2);
				
			}				
		}	
		$tournament_data['inventory_item_taxes'] = $total_tax;
		
			
		if( $this->Tournament->save( $tournament_data, $tournament_id ) )
		{
			//New Tournament
			if($tournament_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('tournaments_successful_adding').' '.
				$tournament_data['name'],'tournament_id'=>$tournament_data['tournament_id']));				
			}
			else //previous tournament
			{
				echo json_encode(array('success'=>true,'message'=>lang('tournaments_successful_updating').' '.
				$tournament_data['name'],'tournament_id'=>$tournament_id));
			}
			
			//Save all the tournament inventory items						
			$this->Tournament_inventory_items->save($tournament_items, $tournament_id);				
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('tournaments_error_adding_updating').' '.
			$tournament_data['name'],'tournament_id'=>$tournament_id));
		}

	}
	
	function delete()
	{
		$tournaments_to_delete = $this->input->post('ids');

		if($this->Tournament->delete_list($tournaments_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('tournaments_successful_deleted').' '.
			count($tournaments_to_delete).' '.lang('tournaments_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('tournaments_cannot_be_deleted')));
		}
	}
	
		/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 590;
	}
}
?>
<?php		
require_once ("secure_area.php");
class Reservations extends Secure_area 
{
	function __construct()
	{
		parent::__construct('reservations');
		$this->load->model('schedule');
        $this->load->model('track');
        $this->load->model('reservation');
        $this->load->model('customer');
        $this->load->model('sale');
		$this->load->model('course');
        $this->load->library('sale_lib');
        $this->load->helper('url');		
	}
	function test_weather() {
		$this->load->model('weather');
		$this->weather->zip_code = '84663';
		$results = $this->weather->getLatest();
		print_r($results);
	}
	function index()
	{
		$dataArray = array();
		$tsMenu = '';
        if ($this->input->post('teesheetMenu'))
            $this->schedule->switch_tee_sheet();
		$current_teesheet = $this->session->userdata('schedule_id');
		//build a menu with the tee sheet id's
		$tsMenu = $this->schedule->get_tee_sheet_menu($current_teesheet);
		//echo (date('Ymd', strtotime('-1 week'))-100).'0000';
		$tracks = $this->track->get_all()->result_array();
        $JSONData = $this->schedule->getJSONTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000');
		//$default_schedule = $this->schedule->get_all(1)->result();
		//print_r($default_schedule);
		//print_r($JSONData);
		$dataArray = array(
            'tsMenu'				=> $tsMenu,
            'current_teesheet'		=> $current_teesheet,
            'JSONData'				=> $JSONData,
            'json_tracks'			=> json_encode($tracks),
            'tracks'				=> $tracks,
            'openhour'				=> $this->session->userdata('openhour'),
            'closehour'				=> $this->session->userdata('closehour'),
            'increment'				=> $this->session->userdata('increment'),
            'holes'					=> $this->session->userdata('holes'),
            'fntime'				=> $this->session->userdata('frontnine'),
            //'fdweather'			=> $this->schedule->make_5_day_weather(),
            'user_level'			=> $this->session->userdata('user_level'),
            //'associated_courses'	=> $this->session->userdata('associated_courses'),
            'user_id'				=> $this->session->userdata('user_id'),
            'purchase'				=> $this->session->userdata('sales'),
			'controller_name'		=> strtolower(get_class())
        );
		//print_r($dataArray);
		
        $this->load->view("reservations/manage", $dataArray);
	}

	function update_facebook_page_id()
	{
		$page_id = $this->input->post('page_id');
		$page_name = $this->input->post('page_name');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = $page_id;
		$course_info->facebook_page_name = $page_name;
		
		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', $page_id);
			$this->session->set_userdata('facebook_page_name', $page_name);
			$result = true;		
		}
		else 
		{
			$result = false;	
		}
		echo json_encode(array('success'=>$result));
				
	}
	
	function update_facebook_access_token()
	{
		$access_token = $this->input->post('extended_access_token');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_extended_access_token = $access_token;
		
		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_extended_access_token', $access_token);
			$result = true;		
		}
		else 
		{
			$result = false;	
		}
		echo json_encode(array('success'=>$result,'new token'=>$access_token));
				
	}

	function make_5_day_weather()
	{
		echo json_encode($this->schedule->make_5_day_weather());
	}

    function get_stats()
    {
    	$view = $this->input->post('view');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $dow = $this->input->post('dow');
        echo $this->schedule->get_stats($view, $year, $month, $day, $dow);
    }
	function get_teetime_hover_data()
	{
		$reservation_id = substr($this->input->post('teetime_id'), 0, 20);
		//echo 'trying to get here';
		$data['teetime_info'] = $this->reservation->get_info($reservation_id);
		$sale_ids = $this->sale->get_sale_ids_by_teetime($reservation_id);
		$data['person_info'] = array();
		if ($data['teetime_info']->person_id != 0)
		{
			$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id);
			if ($data['teetime_info']->person_id_2 != 0)
			{
				$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_2);
				if ($data['teetime_info']->person_id_3 != 0)
				{
					$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_3);
					if ($data['teetime_info']->person_id_4 != 0)
					{
						$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_4);
						if ($data['teetime_info']->person_id_5 != 0)
							$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_5);
					}
				}
			}
		}
		//print_r($sale_ids);
		//echo $this->db->last_query();
		echo $this->load->view('reservations/teetime_info', $data);
		foreach ($sale_ids->result_array() as $result)
		//while ($result = $sale_ids->fetch_assoc())
		{
			$sale_id = $result['sale_id'];
			//echo $result['sale_id'].'<br/>';
			$sale_info = $this->Sale->get_info($sale_id)->row_array();
			$this->sale_lib->copy_entire_sale($sale_id);
			$data['cart']=$this->sale_lib->get_cart();
			$data['payments']=$this->sale_lib->get_payments();
			$this->load->view("reservations/teetime_sales_info",$data);
			$this->sale_lib->clear_all();
			//echo $this->sales->receipt($result['sale_id']);
		}
	}
	/*
	Inserts/updates a teesheet
	*/
	function save($existing_reservation_id=false)
	{
		// Load in libraries and models	
		$this->load->library('name_parser');
		$this->load->model('item');
		$this->load->library('sale_lib');
		
		$split_reservations = $this->input->post('split_teetimes');
		$title_array = $person_id_array = $person_name_array = array();
				
		if (!$existing_reservation_id)
			$reservation_id = $this->schedule->generateID('tt');
		else 
			$reservation_id = $existing_reservation_id;

		$go_to_register = false;
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		//$cur_reservation_info = $this->reservation->get_info($reservation_id);
		
		$type = $this->input->post('event_type');
		$replaceables = array("'", '"', "\\", "/");
			
		$event_data = array(
			'reservation_id'=>$reservation_id,
			'track_id'=>($this->input->post('new_track')?$this->input->post('new_track'):$this->input->post('track_id')),//$this->input->post('teesheet_id'),
			'start'=>$this->input->post('start'),
			'end'=>$this->input->post('end'),
			'side'=>$this->input->post('side'),
			'allDay'=>'false'
		);
		// NON-TEETIME RESERVATIONS.... TODO:take out session data for online booking
		if ($this->session->userdata('schedule_type') != 'tee_sheet' && $this->input->post('teetime_time'))
		{
			$event_data['end'] = date('YmdHi', strtotime(($event_data['start']+1000000).' + '.$this->input->post('teetime_time').' min'))-1000000;
			if ($event_data['end'] % 100 > 59)
                $event_data['end'] += 40;
		}
		if ($type)
			$event_data['type'] = $type;
	
		if ($type == 'closed') {
			$event_data['title'] = str_replace($replaceables, '', $this->input->post('closed_title'));
			$event_data['details'] = $this->input->post('closed_details');
			$event_data['holes'] = 9;
		}
		else if ($type == 'tournament' || $type == 'league' || $type == 'event') {
			$event_data['title'] = str_replace($replaceables, '', $this->input->post('event_title'));
			$event_data['details'] = $this->input->post('event_details');
			$event_data['holes'] = $this->input->post('event_holes');
			$event_data['player_count'] = $this->input->post('event_players');
			$event_data['carts'] = $this->input->post('event_carts');
			$event_data['paid_player_count'] = $this->input->post('event_players');
			$event_data['paid_carts'] = $this->input->post('event_carts');
			$event_data['status'] = 'checked in';
		}
		else if ($type == 'teetime' || $type == 'reservation') {
			/*
			 *  SAVING PLAYER DATA--- UP TO 5
			 */
			$person_data = $person_data_2 = $person_data_3 = $person_data_4 = $person_data_5 = array();
			if ($this->input->post('save_customer_1') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data['first_name'] = $np->getFirstName();
					$person_data['last_name'] = $np->getLastName();
					$person_data['phone_number'] = $this->input->post('phone');
					$person_data['email'] = $this->input->post('email');
					$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_2') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_2')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_2['first_name'] = $np->getFirstName();
					$person_data_2['last_name'] = $np->getLastName();
					$person_data_2['phone_number'] = $this->input->post('phone_2');
					$person_data_2['email'] = $this->input->post('email_2');
					$person_id = ($this->input->post('person_id_2') == '')?false:$this->input->post('person_id_2');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_2, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_3') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_3')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_3['first_name'] = $np->getFirstName();
					$person_data_3['last_name'] = $np->getLastName();
					$person_data_3['phone_number'] = $this->input->post('phone_3');
					$person_data_3['email'] = $this->input->post('email_3');
					$person_id = ($this->input->post('person_id_3') == '')?false:$this->input->post('person_id_3');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_3, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_4') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_4')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_4['first_name'] = $np->getFirstName();
					$person_data_4['last_name'] = $np->getLastName();
					$person_data_4['phone_number'] = $this->input->post('phone_4');
					$person_data_4['email'] = $this->input->post('email_4');
					$person_id = ($this->input->post('person_id_4') == '')?false:$this->input->post('person_id_4');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_4, $customer_data, $person_id); 
				}
			}
			if ($this->input->post('save_customer_5') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_5')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_5['first_name'] = $np->getFirstName();
					$person_data_5['last_name'] = $np->getLastName();
					$person_data_5['phone_number'] = $this->input->post('phone_5');
					$person_data_5['email'] = $this->input->post('email_5');
					$person_id = ($this->input->post('person_id_5') == '')?false:$this->input->post('person_id_5');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_5, $customer_data, $person_id); 
				}
			}
			/*
			 *  END SAVE PLAYER DATA
			 */
			
			$event_data['details'] = $this->input->post('teetime_details');
			$event_data['holes'] = $this->input->post('teetime_holes');
			$event_data['player_count'] = $this->input->post('players');
			$event_data['carts'] = $this->input->post('carts');
			$event_data['person_id'] = (isset($person_data['person_id']))?$person_data['person_id']:$this->input->post('person_id');
			$event_data['person_name'] = str_replace($replaceables, '', $this->input->post('teetime_title'));
			$event_data['person_id_2'] = (isset($person_data_2['person_id']))?$person_data_2['person_id']:$this->input->post('person_id_2');
			$event_data['person_name_2'] = str_replace($replaceables, '', $this->input->post('teetime_title_2'));
			$event_data['person_id_3'] = (isset($person_data_3['person_id']))?$person_data_3['person_id']:$this->input->post('person_id_3');
			$event_data['person_name_3'] = str_replace($replaceables, '', $this->input->post('teetime_title_3'));
			$event_data['person_id_4'] = (isset($person_data_4['person_id']))?$person_data_4['person_id']:$this->input->post('person_id_4');
			$event_data['person_name_4'] = str_replace($replaceables, '', $this->input->post('teetime_title_4'));
			$event_data['person_id_5'] = (isset($person_data_5['person_id']))?$person_data_5['person_id']:$this->input->post('person_id_5');
			$event_data['person_name_5'] = str_replace($replaceables, '', $this->input->post('teetime_title_5'));
			$reservation_title = str_replace($replaceables, '', $this->input->post('teetime_title'));
			//$person_info = $person_info_2 = $person_info_3 = $person_info_4 = $person_info_5 = stdClass;
			/*
			 * CREATE TITLE FOR RESERVATION FROM CUSTOMER NAMES
			 */
			if ($event_data['person_id'] != 0)
			{
				$person_info = $this->Customer->get_info($event_data['person_id']);
				$title_array[1] = ($person_info->last_name != '')?$person_info->last_name:$person_info->first_name;
               	$person_id_array[1] = $event_data['person_id'];
				$person_name_array[1] = $event_data['person_name'];
			}
			else if ($event_data['person_name'] != '')
				$title_array[1] = $person_name_array[1] = $event_data['person_name'];
			if ($event_data['person_id_2'] != 0)
			{
				$person_info_2 = $this->Customer->get_info($event_data['person_id_2']);
				$title_array[2] = ($person_info_2->last_name != '')?$person_info_2->last_name:$person_info_2->first_name;
       			$person_id_array[2] = $event_data['person_id_2'];
				$person_name_array[2] = $event_data['person_name_2'];
			} 
			else if ($event_data['person_name_2'] != '')
				$title_array[2] = $person_name_array[2] = $event_data['person_name_2'];
			if ($event_data['person_id_3'] != 0)
			{
				$person_info_3 = $this->Customer->get_info($event_data['person_id_3']);
				$title_array[3] = (($person_info_3->last_name != '')?$person_info_3->last_name:$person_info_3->first_name);
				$person_id_array[3] = $event_data['person_id_3'];
				$person_name_array[3] = $event_data['person_name_3'];
			} 
			else if ($event_data['person_name_3'] != '')
				$title_array[3] = $person_name_array[3] = $event_data['person_name_3'];
			if ($event_data['person_id_4'] != 0)
			{
				$person_info_4 = $this->Customer->get_info($event_data['person_id_4']);
				$title_array[4] = (($person_info_4->last_name != '')?$person_info_4->last_name:$person_info_4->first_name);
				$person_id_array[4] = $event_data['person_id_4'];
				$person_name_array[4] = $event_data['person_name_4'];
			} 
			else if ($event_data['person_name_4'] != '')
				$title_array[4] = $person_name_array[4] = $event_data['person_name_4'];
			if ($event_data['person_id_5'] != 0)
			{
				$person_info_5 = $this->Customer->get_info($event_data['person_id_5']);
				$title_array[5] = (($person_info_5->last_name != '')?$person_info_5->last_name:$person_info_5->first_name);
				$person_id_array[5] = $event_data['person_id_5'];
				$person_name_array[5] = $event_data['person_name_5'];
			} 
			else if ($event_data['person_name_5'] != '')
				$title_array[5] = $person_name_array[5] = $event_data['person_name_5'];

			$reservation_title = count($title_array) > 0 ? implode(' - ', $title_array):$reservation_title;
			/*
			 * END CREATE TITLE FOR RESERVATION FROM CUSTOMER NAMES
			 */
			$event_data['title'] = $reservation_title;
			$event_data['booking_source'] = 'POS';
			$event_data['booker_id'] = $employee_id;
			
			/*
			 * PURCHASING RESERVATIONS
			 */
			if ($this->input->post('purchase_quantity')) {
				$go_to_register = true;
				//TODO: Build a person price class array to pass into add_green_fees_to_cart
				$person_price_class_array = array(
					0=>(isset($person_info->price_class) && $person_info->price_class !== 0)?$person_info->price_class:'',
					1=>(isset($person_info_2->price_class) && $person_info_2->price_class !== 0)?$person_info_2->price_class:'',
					2=>(isset($person_info_3->price_class) && $person_info_3->price_class !== 0)?$person_info_3->price_class:'',
					3=>(isset($person_info_4->price_class) && $person_info_4->price_class !== 0)?$person_info_4->price_class:'',
					4=>(isset($person_info_5->price_class) && $person_info_5->price_class !== 0)?$person_info_5->price_class:''
				);
				$this->add_green_fees_to_cart($reservation_id, $person_price_class_array);
				$this->sale_lib->delete_customer();
				$this->sale_lib->delete_customer_quickbuttons();
				$this->sale_lib->set_customer($event_data['person_id']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id'], $event_data['person_name']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_2'], $event_data['person_name_2']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_3'], $event_data['person_name_3']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_4'], $event_data['person_name_4']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_5'], $event_data['person_name_5']);
			}
			else if ($this->input->post('checkin')) {
				$event_data['paid_player_count'] = $this->input->post('players');
				$event_data['paid_carts'] = $this->input->post('carts');
				$event_data['status'] = 'checked in';
				//$this->teesheet->record_reservation_checkin($reservation_id, $this->input->post('players'));
			}
			/*
			 * END PURCHASING RESERVATION
			 */
		}

		if ($this->input->post('delete_teetime') == 1)
			$event_data['status'] = 'deleted';
		// TODO: Needs revamping ... changing to reservation	
		$json_ready_events = array();
		/*
		 * SPLITTING RESERVATION
		 */
		if ($split_reservations && !$this->input->post('purchase_quantity'))
		{
			$cur_reservation_info = (array)$this->reservation->get_info($reservation_id);
			$cur_reservation_info['status'] = 'deleted';
			$save_response = $this->reservation->save($cur_reservation_info,$reservation_id, $json_events);
			$json_ready_events = array_merge($json_ready_events, $json_events); 
				
			//$this->reservation->delete($existing_reservation_id);
			//$json_ready_events[] = array('TTID'=>$existing_reservation_id,'status'=>'deleted');
				
			$json_events = array();
			$carts = $event_data['carts'];
			$paid = $cur_reservation_info['paid_player_count'];
			$paid_carts = $cur_reservation_info['paid_carts'];
			//foreach($title_array as $index => $title)
			$player_count = ($event_data['player_count'] > count($title_array))?$event_data['player_count']:count($title_array);
			for ($i = 1; $i <= $player_count; $i++)
			{
				$event_data['person_id_2'] = $event_data['person_id_3'] = $event_data['person_id_4'] = $event_data['person_id_5'] = 0;
				$event_data['person_name_2'] = $event_data['person_name_3'] = $event_data['person_name_4'] = $event_data['person_name_5'] = '';
				$event_data['person_id'] = $person_id_array[$i];
				$event_data['person_name'] = $person_name_array[$i];
				$event_data['title'] = ($title != '')? $title : $person_name_array[$i];
				if ($event_data['title'] == '')
				{
					$event_data['title'] = $person_name_array[1];
					$event_data['person_id'] = ($person_id_array[1])?$person_id_array[1]:0;
					$event_data['person_name'] = $person_name_array[1];
				}
				else if (!$event_data['person_id'])
					$event_data['person_id'] = 0;
                //$event_data['title'] = ($title_array[$index] != '') ? $title_array[$index]:($title_array[0]);
				$event_data['player_count'] = 1;
				$event_data['carts'] = $carts > 0 ? 1:0;
				$event_data['paid_player_count'] = $paid > 0 ? 1:0;
				$event_data['paid_carts'] = $paid_carts > 0 ? 1:0;
				$reservation_id = $this->schedule->generateID('tt');
				$event_data['reservation_id'] = $reservation_id;
				$save_response = $this->reservation->save($event_data,$reservation_id, $json_events);
				$sql_array[] = $this->db->last_query();
				$json_ready_events = array_merge($json_ready_events, $json_events); 
				$carts--;
				$paid--;
				$paid_carts--;
			}
		}
		/*
		 * END SPLITTING RESERVATION : BEGIN SAVING RESERVATION
		 */
		else 
			$save_response = $this->reservation->save($event_data,$reservation_id, $json_ready_events); 
		/*
		 * END SAVING RESERVATION
		 */
		
		$message = '';
		if ($this->session->userdata('schedule_id') != $this->session->userdata('default_schedule_id') && $existing_reservation_id)// && $this->session->userdata('teesheet_reminder_count') < 3)
		{
			$message = lang('teesheets_not_default_teesheet');
			//$this->session->set_userdata('teesheet_reminder_count', $this->session->userdata('teesheet_reminder_count')+1);
		}
		if($save_response['success'])
		{
			//New reservation
			if(!$reservation_id)
			{
				echo json_encode(array('success'=>true,'sql'=>$this->db->last_query(),'message'=>$message, 'reservations'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation']));
			}
			else //previous reservation
			{
				echo json_encode(array('success'=>true,'sql'=>$this->db->last_query(),'message'=>$message, 'reservations'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation']));
			}
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'sql'=>$this->db->last_query(),'message'=>lang('teesheets_reservation_saving_error'), 'reservations'=>$json_ready_events));
		}
	}
	function customer_search($type='')
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}
	function teetime_search()
	{
		$customer_id = $this->input->post('customer_id');
		//echo json_encode(array('search_results'=>$customer_id));
		//return;
		$search_results = $this->reservation->search($customer_id);
		echo json_encode(array('search_results'=>$search_results));
	}

	function teesheet_updates($course_id, $teetimes = 'stu ff')
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
		
		function sendMsg($id, $msg) {
		  echo "id: $id" . PHP_EOL;
		  echo "data: $msg" . PHP_EOL;
		  echo PHP_EOL;
		  ob_flush();
		  flush();
		}
		
		$contents = 'stuff2';
		$return_teetimes = array();
		$return_teetimes = $this->getJSONTeeTimes2();
		sendMsg(time(),$return_teetimes);
	}
	/*
	 * fetch_all_data is used by local_teesheet.js to grab all the data needed to populate the local database
	 */
	function fetch_all_data()
	{
		$teetimes = $this->schedule->getTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000')->result_array();
		$teesheets = $this->schedule->get_all()->result_array();
		
		echo json_encode(array('teetimes'=>$teetimes, 'teesheets'=>$teesheets));
		
	}
	function switch_teetime_sides($reservation_id)
	{
		$this->reservation->switch_sides($reservation_id);
	}
	function mark_teetime_teedoff($reservation_id)
	{
		$events = $this->reservation->mark_as_teedoff($reservation_id);
		echo json_encode(array('reservations'=>$events));
	}
	function mark_teetime_turned($reservation_id)
	{
		$events = $this->reservation->mark_as_turned($reservation_id);
		echo json_encode(array('reservations'=>$events));
	}
	function mark_teetime_finished($reservation_id)
	{
		$events = $this->reservation->mark_as_finished($reservation_id);
		echo json_encode(array('reservations'=>$events));
	}
	function check_in($reservation_id, $count = 0)
	{
		echo json_encode(array('reservations'=>$this->reservation->check_in($reservation_id, $count)));
	}
	function view_move($reservation_id, $side)
	{
		$data = array('teetime_id'=>$reservation_id, 'side'=>$side);
		$data['teetime_info'] = $this->reservation->get_info($reservation_id);
		$teesheet_info = $this->schedule->get_info($this->session->userdata('schedule_id'));
		$data['teetimes'] = $this->schedule->get_teetime_array($this->config->item('open_time'), $this->config->item('close_time'), $teesheet_info->increment, $this->session->userdata('schedule_id'));
	 	$data['teesheets'] = $this->schedule->get_teesheet_array();
	 	$data['tracks'] = $this->track->get_track_array();
	 	$data['difference'] = (strtotime($data['teetime_info']->end) - strtotime($data['teetime_info']->start))/60;
		$data['frontnine'] = $teesheet_info->frontnine;
			//print_r($teesheet_info);
			//print_r($data['teetimes']);
		$this->load->view('reservations/move', $data);
	}
	function get_teetime_dropdown()
	{
		$teesheet_id = $this->input->post('schedule_id');
		$teesheet_info = $this->schedule->get_info($teesheet_id);
		//echo 'heyo '.$this->config->item('open_time').' - '.$this->config->item('close_time').' - '.$teesheet_info->increment.' - '.$teesheet_id;
		//return;
		$teetimes = $this->schedule->get_teetime_array($this->config->item('open_time'), $this->config->item('close_time'), $teesheet_info->increment, $teesheet_id);
		$frontnine = $teesheet_info->frontnine;
		$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $teesheet_info->increment);
		$frontnine += ($dif > $teesheet_info->increment/2)?($teesheet_info->increment - $dif):-$dif;
		
		echo json_encode(
			array(
				'teetime_dropdown' => form_dropdown('new_time', $teetimes, ''),
				'difference_input' => form_hidden('difference', $teesheet_info->increment),
				'frontnine_input' => form_hidden('frontnine', $frontnine),
				'tracks_dropdown' => form_dropdown('track_id', $this->track->get_track_array($teesheet_id), '')
			)
		);
	}
	function view_repeat($reservation_id)
	{
		$data = array('reservation_id'=>$reservation_id);
		$data['reservation_info'] = $this->reservation->get_info($reservation_id);
		$this->load->view('reservations/repeat', $data);
	}
	function move($reservation_id, $player_count)
	{
		$new_date = $this->input->post('new_date');
		$new_time = $this->input->post('new_time');
		/*
		 * Added new_teesheet parameter, but still need to adjust the difference value...
		 * We'll use the same functionality that save uses to adjust the 'difference' value to normalize if for the new teesheet. Once we've adjusted the difference, we should be done.
		 */
		$teesheet_id = $this->input->post('new_teesheet');
		$track_id = $this->input->post('track_id');
		$difference = $this->input->post('difference');
		$frontnine = $this->input->post('frontnine');
		$time_1 = ($new_time+$difference < 1000)?'0'.($new_time+$difference):$new_time+$difference;
		$time_2 = ($new_time+$frontnine < 1000)?'0'.($new_time+$frontnine):$new_time+$frontnine;
		$time_3 = ($new_time+$frontnine+$difference < 1000)?'0'.($new_time+$frontnine+$difference):$new_time+$frontnine+$difference;
		$date_string = date('YmdHi', strtotime($new_date.' '.$new_time.' -1 month'));
		$end_date_string = date('YmdHi', strtotime($new_date.' '.$time_1.' -1 month'));
		$back_date_string = date('YmdHi', strtotime($new_date.' '.$time_2.' -1 month'));
		$back_end_date_string = date('YmdHi', strtotime($new_date.' '.$time_3.' -1 month'));
		$available = $this->reservation->check_availability(array('start'=>$date_string,'player_count'=>$player_count, 'teesheet_id'=>$teesheet_id));
		if (strtotime($new_date) === false)
			echo json_encode(array('success'=>false, 'message'=>'Not a valid date', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string)));
		else if (!$available)
			echo json_encode(array('success'=>false, 'message'=>'Teetime', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string), 'frontnine'=>$frontnine));
		else
		{
			$this->reservation->change_date($reservation_id, $date_string, $end_date_string, $back_date_string, $back_end_date_string, $teesheet_id, $track_id);
			echo json_encode(array('success'=>true, 'message'=>'Successfully changed date', 'sql'=>$this->db->last_query(), 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string),'teesheet_id'=>$teesheet_id, 'available'=>$available));
		}
	}
	function repeat($reservation_id)
	{
		$repeats = $this->input->post('repeats');
		$sunday = $this->input->post('sunday');
		$monday = $this->input->post('monday');
		$tuesday = $this->input->post('tuesday');
		$wednesday = $this->input->post('wednesday');
		$thursday = $this->input->post('thursday');
		$friday = $this->input->post('friday');
		$saturday = $this->input->post('saturday');
		$start_date = date('Ymd', strtotime($this->input->post('start_date')));
		$end_date = ($repeats != 'once')?date('Ymd', strtotime($this->input->post('end_date'))):date('Ymd', strtotime($this->input->post('start_date')));
		
		$reservation_info = $this->reservation->get_info($reservation_id);
		$event_data = (array)$reservation_info;
		$event_data['paid_player_count'] = 0;
		$event_data['paid_carts'] = 0;
		$event_data['status'] = '';
		$event_data['date_booked'] = date('Y-m-d H:i:s');
		unset($event_data['reservation_id']);
		$start_time = date('Hi', strtotime($reservation_info->start));
		$end_time = date('Hi', strtotime($reservation_info->end));
		$event_date = date('Ymd', strtotime("{$reservation_info->start} +1 month +1 day"));
		$event_date = ($start_date > $event_date)?$start_date:$event_date; 
		$json_ready_events = array();
		while ($event_date <= $end_date)
		{
			$event_data['start'] = (int)($event_date.$start_time) - 1000000;
			$event_data['end'] = (int)($event_date.$end_time) - 1000000;
			if ($repeats == 'once' || $repeats == 'daily')
			{
				$this->reservation->save($event_data, false, $json_ready_events);
			}
			else if ($repeats == 'weekly')
			{
				$dow = date('w',strtotime($event_date));
				if (($sunday && $dow == 0) ||
					($monday && $dow == 1) ||
					($tuesday && $dow == 2) ||
					($wednesday && $dow == 3) ||
					($thursday && $dow == 4) ||
					($friday && $dow == 5) ||
					($saturday && $dow == 6))
						$this->reservation->save($event_data,false, $json_ready_events);
			}	
			$event_date = date('Ymd', strtotime("$event_date +1 day"));
		}
		
		echo json_encode(array('success'=>true, 'repeats'=>$repeats, 'sunday'=>$sunday, 'monday'=>$monday, 'end_date'=>$end_date, 'reservation_info'=>$reservation_info, 'reservations'=>$json_ready_events));
	}
	function add_green_fees_to_cart ($reservation_id, $person_price_class_array = array()) 
	{
		$item_num = '7';
        $cart_num = '5';
        $holes = $this->input->post('teetime_holes');
        $quantity = $this->input->post('purchase_quantity');
        $start_time = (int)substr($this->input->post('start'),8);
		$adjusted_start_time = $this->input->post('start') + 1000000;
        $dow = date('w',strtotime($adjusted_start_time));
		$carts = $this->input->post('carts')-$this->input->post('paid_carts');
		
		$this->sale_lib->empty_cart();
	    $this->sale_lib->empty_basket();
	    
	    if ($this->session->userdata('schedule_type') != 'tee_sheet')
		{
			$increment = $this->session->userdata('increment');
			//echo 'increment '.$increment;
			$play_time_length = $this->input->post('teetime_time');
			if ($play_time_length == $increment * 1)
				$item_num = '1';
			if ($play_time_length == $increment * 2)
				$item_num = '2';
			if ($play_time_length == $increment * 3)
				$item_num = '3';
			if ($play_time_length == $increment * 4)
				$item_num = '4';
			$not_holiday = true;
			if ($this->config->item('holidays') && $this->Appconfig->is_holiday())//TODO: build is_holiday function
			{
				$price_category_index = 7;
				$not_holiday = false;
			}
			$prices = $this->Fee->get_info();
	        $item_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$item_num);
	        $teesheet_id = $this->session->userdata('schedule_id');
			$course_id = $this->session->userdata('course_id');
			for ($i=0;$i<$quantity;$i++) {
				//TODO: Handle person_price_class_array here... the prices don't trump the holiday, but they trump the time of day
				if (isset($person_price_class_array[$i]) && $person_price_class_array[$i] != '' && $not_holiday)
				{
					$p_cat = $person_price_class_array[$i];
					$pci = str_replace('price_category_', '', $p_cat);
				}
				else 
				{
					$pci = $price_category_index;
					$p_cat = 'price_category_'.$pci;
				}
				//echo 'pci '.$pci.' pcat '.$p_cat;
				$this->sale_lib->add_item($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	            $this->sale_lib->add_item_to_basket($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	        }
		}
		else 
		{
			$type = 'regular';
			$price_category_index = null;
			$not_holiday = true;
			if ($this->config->item('holidays') && $this->Appconfig->is_holiday())//TODO: build is_holiday function
			{
				$price_category_index = 7;
				$not_holiday = false;
			}
			else if ((int)$this->config->item('early_bird_hours_begin') <= $start_time && (int)$this->config->item('early_bird_hours_end') > $start_time)
	        	$price_category_index = 2;
			else if ((int)$this->config->item('morning_hours_begin') <= $start_time && (int)$this->config->item('morning_hours_end') > $start_time)
	        	$price_category_index = 3;
			else if ((int)$this->config->item('afternoon_hours_begin') <= $start_time && (int)$this->config->item('afternoon_hours_end') > $start_time)
	        	$price_category_index = 4;
			else if ((int)$start_time >= $this->config->item('super_twilight_hour'))
		    	$price_category_index = 6;
			else if ((int)$start_time >= $this->config->item('twilight_hour'))
		    	$price_category_index = 5;
			
			// Check against settings for if it is currently the weekend
			if (($dow == 5 && $this->config->item('weekend_fri'))|| ($dow == 6 && $this->config->item('weekend_sat')) || ($dow == 0 && $this->config->item('weekend_sun')))
	            $type = 'weekend';
	        if ($holes == 9) {
	            $cart_num = '1';
				$item_num = '3';
			}
	        else if ($holes == 18) {
	            $cart_num = '5';
				$item_num = '7';
			}
			if ($type=='weekend')
			{
				$cart_num += 1;
				$item_num += 1;
			}
			
	        $prices = $this->Fee->get_info();
	        $item_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$item_num);
	        $cart_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$cart_num);
			$teesheet_id = $this->session->userdata('schedule_id');
			$course_id = $this->session->userdata('course_id');
			for ($i=0;$i<$quantity;$i++) {
				//TODO: Handle person_price_class_array here... the prices don't trump the holiday, but they trump the time of day
				if (isset($person_price_class_array[$i]) && $person_price_class_array[$i] != '' && $not_holiday)
				{
					$p_cat = $person_price_class_array[$i];
					$pci = str_replace('price_category_', '', $p_cat);
				}
				else 
				{
					$pci = $price_category_index;
					$p_cat = 'price_category_'.$pci;
				}
				$this->sale_lib->add_item($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	            $this->sale_lib->add_item_to_basket($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	            if ($i < $carts) {
	                $this->sale_lib->add_item($cart_id,1,0,$prices[$teesheet_id][$course_id.'_'.$cart_num]->$p_cat,null,null,$pci,$cart_num);
	                $this->sale_lib->add_item_to_basket($cart_id,1,0,$prices[$teesheet_id][$course_id.'_'.$cart_num]->$p_cat,null,null,$pci,$cart_num);
	            }
	        }
		}
        //echo $item_id.' - '.$cart_id;
		$this->sale_lib->set_teetime($reservation_id);
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function get_row()
	{
		$teesheet_id = $this->input->post('row_id');
		$data_row=get_teesheet_data_row($this->schedule->get_info($teesheet_id),$this);
		echo $data_row;
	}
	
	/*
	This deletes teetimes from the teetimes table
	*/
	function delete($reservation_id = -1)
	{
		if ($reservation_id == -1)
			$reservation_id=$this->input->post('id');
		
		if($this->reservation->delete($reservation_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	function getJSONTeeTimes($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
        echo $this->schedule->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true);
    }
	function getJSONTeeTimes2($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
        return $this->schedule->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true);
    }
	function get_json_teetimes($start, $end) {
	    echo $this->schedule->getJSONTeeTimes($start,$end, '');
    }
	function logout()
	{
            $this->Employee->logout();
	}
    
	/*
	Loads the teetime edit form
	*/
	function view($reservation_id=-1)
	{
		$this->load->model('customer');
		$data['teesheet_holes'] = $this->session->userdata('holes');
		$data['reservation_info']=$this->reservation->get_info($reservation_id);
		$data['track_info']=$this->track->get_info($data['reservation_info']->track_id);
		$data['customer_info'] = array();
		if ($data['reservation_info']->person_id != 0 && $data['reservation_info']->person_id != '')
			$data['customer_info'][$data['reservation_info']->person_id] = $this->Customer->get_info($data['reservation_info']->person_id);
		if ($data['reservation_info']->person_id_2 != 0 && $data['reservation_info']->person_id_2 != '')
			$data['customer_info'][$data['reservation_info']->person_id_2] = $this->Customer->get_info($data['reservation_info']->person_id_2);
		if ($data['reservation_info']->person_id_3 != 0 && $data['reservation_info']->person_id_3 != '')
			$data['customer_info'][$data['reservation_info']->person_id_3] = $this->Customer->get_info($data['reservation_info']->person_id_3);
		if ($data['reservation_info']->person_id_4 != 0 && $data['reservation_info']->person_id_4 != '')
			$data['customer_info'][$data['reservation_info']->person_id_4] = $this->Customer->get_info($data['reservation_info']->person_id_4);
		if ($data['reservation_info']->person_id_5 != 0 && $data['reservation_info']->person_id_5 != '')
			$data['customer_info'][$data['reservation_info']->person_id_5] = $this->Customer->get_info($data['reservation_info']->person_id_5);
		if ($data['reservation_info']->title == '') {
			$data['checkin_text'] = 'Walk In';
			$data['save_text'] = 'Reserve';
		}
		else {
			$data['checkin_text'] = 'Check In';
			$data['save_text'] = 'Update';
		}
		//$data['cross_booking'] = $this->;
		//echo $this->session->userdata('schedule_type');
		if ($this->session->userdata('schedule_type') != 'tee_sheet')//$this->config->item('simulator'))
			$this->load->view("reservations/form_simulator",$data);
		else
			$this->load->view("reservations/form",$data);
	}
	function send_confirmation_email($reservation_id = ''){
		if ($reservation_id != '')
		{
			$person_array = array();
			$this->load->model('Course');
			$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			$teetime_data = $this->reservation->get_info($reservation_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_2);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_3);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_4);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_5);
			// Send emails to each golfer
			foreach ($person_array as $person_info)
			{
				if ($person_info->email)
				{
					$email_data = array(
						'person_id'=>$teetime_data->person_id,
						'course_name'=>$this->session->userdata('course_name'),
						'course_phone'=>$course_info->phone,
						'course_id'=>$this->session->userdata('course_id'),
						'first_name'=>$person_info->first_name,
						'booked_date'=>date('n/j/y', strtotime($teetime_data->start+1000000)),
						'booked_time'=>date('g:ia', strtotime($teetime_data->start+1000000)),
						'booked_holes'=>$teetime_data->holes,
						'booked_players'=>$teetime_data->player_count,
						'reservation_id'=> $teetime_data->reservation_id
					);
					$this->reservation->send_confirmation_email($person_info->email, 'Tee Time Reservation Confirmation', $email_data, $course_info->email, $this->session->userdata('course_name'));
				}
			}
		}
		echo json_encode(array());
	}
    function print_teesheet($date)
	{
        $this->schedule->print_teesheet($date);
	}
	function get_message()
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');
		
		echo "id: 12345".PHP_EOL;
		echo "data: message3".PHP_EOL;
		echo PHP_EOL;
		ob_flush();
		flush();
	}
	function generate_stats()
	{
		$results = $this->Dash_data->fetch_reservation_data();		
		echo json_encode($results);	
	}
}
?>
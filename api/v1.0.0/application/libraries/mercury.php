<?php
class Mercury
{
	//Can handle manual entry of data, but has to be done through IPAD terminal
	//Things that need to happen... handle errors... 4 basic types
	//handle tokens... store at minimum... preferrably use page 9 bottom for best practises
	
	private $IpPort = '9100';//Only used for gift cards
	private $MerchantID = '';//Merchant id: string "395347305=E2E"
	private $OperatorID = '';//Operator id: string
	private $Method = 'HC';//HC, E2E, E2ETKN, TOKEN
	private $Type = 'Credit';//Credit, US Debit, Admin, Gift, EBT, FSA, CheckAuth
	private $TranType = 'Credit';//Transaction type: string 'Credit'
	private $TranCode = 'Sale';//Transaction code: string ('Sale', 'Return'*, 'VoidSale'*,'VoidReturn'*) only supporting the previous four
	private $PartialAuth = 'Allow';//Partial authorization permitted: string 'Allow'
	//will support the following at some point,'PreAuth'*,PreAuthCapture'*,
	//'Adjust'*, 'FSASale'*, 'RreverseFSASale'*, 'CardLookUp', 'NoNSFSale' *Can use ByRecordNo
	private $InvoiceNo = '0';//Invoice number: int
	private $RefNo = '0';//Reverence number: int
	private $Memo = 'ForeUP v1.1';//Memo: string
	// For MToken requests
	private $RecordNo = '';//Record number: int
	private $Frequency = '';//Frequency: string 'OneTime', 'Recurring'
	// Account data fields
	private $EncryptedFormat = 'MagneSafe'; //Encryption strategy: "Magnesafe" 
	private $AccountSource = 'Swiped'; //Source of encrypted block: "Swiped", "Keyed", "Contactless"
	private $EncryptedBlock	= '';//Encrypted Track1, Track2 or Manual data from IPAD
	private $EncryptedKey = '';//Derived Unique Key from Secure Card Reader
	// Ammount data fields
	private $Purchase = '0';//Purchase: float/string
	
	
	function process_payment($TranCode = 'Sale', $params)
	{
		$this->setMerchantID($params->MerchantID);
		$this->setOperatorID($params->OperatorID);	
		$this->setEncryptedBlock($params->EncryptedBlock);
		$this->setEncryptedKey($params->EncryptedKey);
		$this->setPurchase($params->Purchase);
		$this->setInvoiceNo($params->InvoiceNo);
		$this->setRefNo($params->InvoiceNo);
		$this->setTranCode($TranCode);
		
		switch($TranCode){
			case 'TestSale':
				$this->setMerchantID('395347305=E2E');
				$this->setOperatorID('Test');
				$this->setTranCode('Sale');
				break;
			case 'Sale':
				break;
			case 'Return':
				break;
			case 'VoidSale':
				break;
			case 'VoidReturn':
				break;
			default:
				return false;
				break;
		}
		
		$transaction_xml = $this->getPaymentXML();
		$xml = $this->sendMercuryXML($transaction_xml, $type);
			echo json_encode(array('transaction_xml'=>$transaction_xml,'xml'=>$xml));
		return;
	}
	
	function getMercurySOAPClient(){
	     if($this->getType() == "Gift"){
	         $url = "https://g1.mercurydev.net/ws/ws.asmx?WSDL";
	     }else{
	         $url = "https://w1.mercurydev.net/ws/ws.asmx?WSDL";
	     }
	     $ini = ini_set("soap.wsdl_cache_enabled","0");
	     $client = new SoapClient($url, array("trace" => 1, "exception" => 0, "features" => SOAP_SINGLE_ELEMENT_ARRAYS));
	
	     return $client;
	}
	
	function sendMercuryXML($xml, $type = ''){
		//Type should currently be blank or 'gift'
	    if ($this->getType() == 'Gift')
			$Transaction_type = 'GiftTransaction';
		else {
			$Transaction_type = 'CreditTransaction';
		}
		 $xml = '<?xml version="1.0"?>' . $xml;
	     // get the soap object
	     $client = $this->getMercurySOAPClient($type);
	     $pw = $this->getMercuryPassword($type);
	     $parameters = array('tran' => $xml,'pw' => $pw);
	     $result = $client->__soapCall($Transaction_type,
		 	array('CreditTransaction' =>  $parameters));
	
	     return $result;
	}
	function getMercuryPassword($type)
	{
		if ($type == 'test')
			return '123E2E';
		
		return;
	}
	function getPaymentXML()
	{
		
		// 3 ways ... 1 just process the payment 2 request token 3 pay via token
		$partialAuth =  "<PartialAuth>{$this->getPartialAuth()}</PartialAuth>";
		if ($this->getRecordNo() == '') 
		{
			return "<TStream>".
						"<Transaction>". 
							"<MerchantID>{$this->getMerchantID()}</MerchantID>". 
							"<OperatorID>{$this->getOperatorID()}</OperatorID>". 
							"<TranType>{$this->getTranType()}</TranType>". 
							"<TranCode>{$this->getTranCode()}</TranCode>". 
							"<InvoiceNo>{$this->getInvoiceNo()}</InvoiceNo>". 
							"<RefNo>{$this->getRefNo()}</RefNo>". 
							"<Memo>{$this->getMemo()}</Memo>". 
							"<Account>".
								"<EncryptedFormat>{$this->getEncryptedFormat()}</EncryptedFormat>". 
								"<AccountSource>{$this->getAccountSource()}</AccountSource>". 
								"<EncryptedBlock>{$this->getEncryptedBlock()}</EncryptedBlock>". 
								"<EncryptedKey>{$this->getEncryptedKey()}</EncryptedKey>".
							"</Account>". 
							"<Amount>".
								"<Purchase>{$this->getPurchase()}</Purchase>". 
							"</Amount>".
						"</Transaction>". 
					"</TStream>";
		}
		else
		{
			return "<TStream> 
						<Transaction>
							<MerchantID>395347306=TOKEN</MerchantID> 
							<OperatorID>Test</OperatorID> 
							<TranType>Credit</TranType> 
							<TranCode>ReturnByRecordNo</TranCode> 
							<PartialAuth>Allow</PartialAuth> 
							<InvoiceNo>0025</InvoiceNo> 
							<RefNo>0025</RefNo>
							<RecordNo>BuFzLtekgFrTsiCOxI59PCQUfZe32C3YYXgXuPuFU64yEAQQADIQAAIX</RecordNo>
							<Frequency>OneTime</Frequency> 
							<Amount>
								<Purchase>3.75</Purchase> 
							</Amount>
						</Transaction> 
					</TStream>";
		}
	}
	
	function parseResponseXML($xml)
	{
		/*Sample xml response
		 * <?xml version="1.0"?> 
		 * 
		 	<RStream>
				<CmdResponse> 
					<ResponseOrigin>Processor</ResponseOrigin> 
		 * 			<DSIXReturnCode>000000</DSIXReturnCode> 
		 * 			<CmdStatus>Approved</CmdStatus> 
		 * 			<TextResponse>AP</TextResponse> 
		 * 			<UserTraceData></UserTraceData>
				</CmdResponse> 
		 * 		<TranResponse>
					<MerchantID>395347305=E2E</MerchantID> 
		 * 			<AcctNo>549999XXXXXX6781</AcctNo> 
		 * 			<ExpDate>XXXX</ExpDate> 
		 * 			<CardType>M/C</CardType> 
		 * 			<TranCode>Sale</TranCode>
					<AuthCode>MC0225</AuthCode> 
		 * 			<CaptureStatus>Captured</CaptureStatus> 
		 * 			<RefNo>0001</RefNo> 
		 * 			<InvoiceNo>1</InvoiceNo> 
		 * 			<OperatorID>test</OperatorID>
					<Amount> 
		 * 				<Purchase>2.25</Purchase> 
		 * 				<Authorize>2.25</Authorize>
		 * 				<Balance>80.25</Balance>//only returned on NoNSFSale transactions
					</Amount> 
		 * 			<AcqRefData>bMCC6429311215</AcqRefData> 
		 * 			<ProcessData>|00|410100201000</ProcessData>
		 * 			<RecordNo>BuFzLtekgFrTsiCOxI59PCQUfZe32C3YYXgXuPuFU64yEAQQADIQAAIX</RecordNo>//only if Token is Requested
				</TranResponse> 
		 * 	</RStream>
		 */
	}
	function handleError() {
		//Connectivity Error
		
		//Check Mismatch Error (Data is encrypted but account is not set up for it or vice versa)
		
		//Invalid field
		
		//Corrupted Data
	}
	// Set or get any of the values
	function setMerchantID($val) { $this->MerchantID = $val;}
	function getMerchantID() { return $this->MerchantID;}
	
	function setOperatorID($val) { $this->OperatorID = $val;}
	function getOperatorID() { return $this->OperatorID;}
	
	function setTranType($val) { $this->TranType = $val;}
	function getTranType() { return $this->TranType;}
	
	function setType($val) { $this->Type = $val;}
	function getType() { return $this->Type;}
	
	function setPartialAuth($val) { $this->PartialAuth = $val;}
	function getPartialAuth() { return $this->PartialAuth;}
	
	function setTranCode($val) { $this->TranCode = $val;}
	function getTranCode() { return $this->TranCode;}
	
	function setInvoiceNo($val) { $this->InvoiceNo = $val;}
	function getInvoiceNo() { return $this->InvoiceNo;}
	
	function setRefNo($val) { $this->RefNo = $val;}
	function getRefNo() { return $this->RefNo;}
	
	function setRecordNo($val) { $this->RecordNo = $val;}
	function getRecordNo() { return $this->RecordNo;}
	
	function setMemo($val) { $this->Memo = $val;}
	function getMemo() { return $this->Memo;}
	
	function setEncryptedFormat($val) { $this->EncryptedFormat = $val;}
	function getEncryptedFormat() { return $this->EncryptedFormat;}
	
	function setAccountSource($val) { $this->AccountSource = $val;}
	function getAccountSource() { return $this->AccountSource;}
	
	function setEncryptedBlock($val) { $this->EncryptedBlock = $val;}
	function getEncryptedBlock() { return $this->EncryptedBlock;}
	
	function setEncryptedKey($val) { $this->EncryptedKey = $val;}
	function getEncryptedKey() { return $this->EncryptedKey;}
	
	function setPurchase($val) { $this->Purchase = $val;}
	function getPurchase() { return $this->Purchase;}
}
/*
 * Sample XML for E2E Credit transactions
 * https://portal.mercurypay.com/securemarketing/resources/SampleCode/SDKSampleRequestXML/Credit.htm#_Toc184200524
 */
?>

<?php
class Auto_mailer extends CI_Model
{	
	/*
	Determines if a given auto_mailer_id is a customer
	*/
	function exists($auto_mailer_id)
	{
		$course_ids = array();
		
		$this->db->from('auto_mailers');	
		$this->db->where('auto_mailer_id',$auto_mailer_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
        $query = $this->db->get();
		//echo $this->db->last_query();
		return ($query->num_rows()==1);
	}
	
	/*
	Returns all the customers
	*/
	function get_all($limit=10000, $offset=0, $cron = false)
	{
		$course_id = '';
	    if (!$this->permissions->is_super_admin() && !$cron)
		{
	        $this->db->where('course_id', $this->session->userdata('course_id'));
        }	
	    $this->db->from('auto_mailers');
		$this->db->where("deleted = 0 $course_id");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$auto_mailers = $this->db->get();
		//echo $this->db->last_query();
        return $auto_mailers;
	}
	
	function count_all()
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->from('auto_mailers');
		$this->db->where("deleted", 0);
		//$this->db->get();
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular customer
	*/
	function get_info($auto_mailer_id)
	{
		$this->db->from('auto_mailers');	
		$this->db->where('auto_mailer_id',$auto_mailer_id);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get all the fields from customer table
			$fields = $this->db->list_fields('auto_mailers');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	
	/*
	Gets information about multiple customers
	*/
	function get_multiple_info($recipient_ids)
	{
		$this->db->from('auto_mailers');
		$this->db->join('people', 'people.auto_mailer_id = auto_mailers.auto_mailer_id');		
		$this->db->where_in('auto_mailers.auto_mailer_id',$recipient_ids);
		$this->db->order_by("last_name", "asc");
		return $this->db->get();		
	}
	
	
	/*
	Inserts or updates an auto_mailer
	*/
	function save(&$auto_mailer_data,$auto_mailer_id=false)
	{
		$success=false;
		//echo 'saving am';
		//Run these queries as a transaction, we want to make sure we do all or nothing
		if (!$auto_mailer_id || !$this->exists($auto_mailer_id))
		{
			//echo '<br/>does not exist am';
			$auto_mailer_id = $auto_mailer_data['auto_mailer_id'];
			$auto_mailer_data['creation_date'] = date('Y-m-d H:i:s');
			if ($this->db->insert('auto_mailers',$auto_mailer_data))
			{
				$auto_mailer_data['auto_mailer_id']=$this->db->insert_id();
				$success = true;
			}
		}
		else
		{
			$this->db->where('auto_mailer_id', $auto_mailer_id);
			$success = $this->db->update('auto_mailers',$auto_mailer_data);
			$auto_mailer_data['auto_mailer_id']=$auto_mailer_id;
		}
	
		return $success;
	}
	/*
	Deletes one recipient
	*/
	function delete($recipient_id)
	{
		$course_id = '';
        $this->db->where('auto_mailers.course_id', $this->session->userdata('course_id'));
        $this->db->where("auto_mailer_id", $recipient_id);
		return $this->db->update('auto_mailer', array('deleted' => 1));
	}
	
	/*
	Deletes a list of recipients
	*/
	function delete_list($recipient_ids)
	{
		$this->db->where('auto_mailers.course_id', $this->session->userdata('course_id'));
        $this->db->where_in('auto_mailer_id',$recipient_ids);
		return $this->db->update('auto_mailer', array('deleted' => 1));
 	}
 	
 	/*
	Get search suggestions to find recipients
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailers.course_id', $this->session->userdata('course_id'));
        }
        $suggestions = array();
		
		$this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);		
		}
		
		$this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
		$this->db->where("deleted = 0 $course_id");
		$this->db->like("email",$search);
		$this->db->order_by("email", "asc");		
		$by_email = $this->db->get();
		foreach($by_email->result() as $row)
		{
			$suggestions[]=array('label'=> $row->email);		
		}

		$this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("phone_number",$search);
		$this->db->order_by("phone_number", "asc");		
		$by_phone = $this->db->get();
		foreach($by_phone->result() as $row)
		{
			$suggestions[]=array('label'=> $row->phone_number);		
		}
		
		$this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("account_number",$search);
		$this->db->order_by("account_number", "asc");		
		$by_account_number = $this->db->get();
		foreach($by_account_number->result() as $row)
		{
			$suggestions[]=array('label'=> $row->account_number);		
		}
		
		$this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
		$this->db->where("deleted = 0 $course_id");		
		$this->db->like("company_name",$search);
		$this->db->order_by("company_name", "asc");		
		$by_company_name = $this->db->get();
		foreach($by_company_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->company_name);		
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	
	}
	
	/*
	Get search suggestions to find customers
	*/
	function get_recipient_search_suggestions($search,$limit=25,$type='')
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailers.course_id', $this->session->userdata('course_id'));
        }
        $suggestions = array();
		
		if ($type == 'last_name' || $type == '' || $type == 'ln_and_pn') {
			$this->db->from('auto_mailer');
			$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
			$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
			$this->db->order_by("last_name", "asc");		
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=array('value'=> $row->auto_mailer_id, 'label' => $row->last_name.', '.$row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email);		
			}
		}
		
		if ($type == 'account_number' || $type == '') {
			$this->db->from('auto_mailer');
			$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
			$this->db->where("deleted = 0 $course_id");		
			$this->db->like("account_number",$search);
			$this->db->order_by("account_number", "asc");		
			$by_account_number = $this->db->get();
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->auto_mailer_id, 'label' => $row->account_number);		
			}
		}

			
		if ($type == 'phone_number' || $type == 'ln_and_pn') {
			$this->db->from('auto_mailer');
			$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('phone_number != ""');		
			$this->db->like("phone_number",$search);
			$this->db->order_by("phone_number", "asc");		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->auto_mailer_id, 'label' => $row->phone_number, 'name'=>$row->last_name.', '.$row->first_name, 'email'=>$row->email);		
			}
		}

		if ($type == 'email') {
			$this->db->from('auto_mailer');
			$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('email != ""');		
			$this->db->like("email",$search);
			$this->db->order_by("email", "asc");		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->auto_mailer_id, 'label' => $row->email, 'name'=>$row->last_name.', '.$row->first_name, 'phone_number'=>$row->phone_number);		
			}
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	/*
	Preform a search on recipients
	*/
	function search($search, $limit=20, $group_id = 'all', $offset = 0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
            $this->db->where('auto_mailers.course_id', $this->session->userdata('course_id'));
        }
        $this->db->from('auto_mailer');
		$this->db->join('people','auto_mailers.auto_mailer_id=people.auto_mailer_id');		
		
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		email LIKE '%".$this->db->escape_like_str($search)."%' or 
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		account_number LIKE '%".$this->db->escape_like_str($search)."%' or 
		company_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");		
		$this->db->order_by("last_name", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		
		$this->db->limit($limit);
		return $this->db->get();
	}
}
?>

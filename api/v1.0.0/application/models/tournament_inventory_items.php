<?php
class Tournament_inventory_items extends CI_Model
{
	/*
	Gets tournament items for a particular tournament
	*/
	function get_info($tournament_id)
	{
		$this->db->from('tournament_inventory_items');
		$this->db->where('tournament_id',$tournament_id);
		//return an array of tournament inventory items for a tournament
		return $this->db->get()->result();
	}
	
	/*
	Inserts or updates a tournament's items
	*/
	function save(&$tournament_items_data, $tournament_id)
	{		
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->delete($tournament_id); 
		
		foreach ($tournament_items_data as $row)
		{
			$row['tournament_id'] = $tournament_id;
			$this->db->insert('tournament_inventory_items',$row);		
		}
		
		$this->db->trans_complete();
		return true;
	}
	
	/*
	Deletes item kit items given an item kit
	*/
	function delete($tournament_id)
	{
		return $this->db->delete('tournament_inventory_items', array('tournament_id' => $tournament_id)); 
	}
}
?>

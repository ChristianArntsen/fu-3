<?php
class Giftcard extends CI_Model
{
	/*
	Determines if a given giftcard_id is an giftcard
	*/
	function exists( $giftcard_id )
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
        
        $this->db->from('giftcards');
		$this->db->where('giftcard_id',$giftcard_id);
		$this->db->where_in('course_id', array_values($course_ids));
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	/*
	Determines if a given giftcard_id is expired
	*/
	function is_expired( $giftcard_id )
	{
		//echo 'is expired';
		$this->db->select('expiration_date');
		$this->db->from('giftcards');
		$this->db->where('giftcard_id',$giftcard_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$result = $this->db->get()->result_array();
		//print_r($result);
		return ($result[0]['expiration_date'] != '0000-00-00' && strtotime($result[0]['expiration_date']) < time());
	}

	/*
	Returns all the giftcards
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('giftcards');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("giftcard_number", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('giftcards');
		$this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
	function cleanup()
	{
		$giftcard_data = array('giftcard_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('giftcards',$giftcard_data);
	}
    
	/*
	Gets information about a particular giftcard
	*/
	function get_info($giftcard_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('giftcards');
		$this->db->where('giftcard_id',$giftcard_id);
		$this->db->where("deleted = 0 $course_id");
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $giftcard_id is NOT an giftcard
			$giftcard_obj=new stdClass();

			//Get all the fields from giftcards table
			$fields = $this->db->list_fields('giftcards');

			foreach ($fields as $field)
			{
				$giftcard_obj->$field='';
			}

			return $giftcard_obj;
		}
	}

	/*
	Get an giftcard id given an giftcard number
	*/
	function get_giftcard_id($giftcard_number, $check_deleted = false)
	{
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('giftcards');
		$this->db->where('giftcard_number',$giftcard_number);
		if (!$check_deleted)
			$this->db->where("deleted = 0");
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->giftcard_id;
		}

		return false;
	}

	/*
	Gets information about multiple giftcards
	*/
	function get_multiple_info($giftcard_ids)
	{
		$this->db->from('giftcards');
		$this->db->where_in('giftcard_id',$giftcard_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("giftcard_number", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a giftcard
	*/
	function save(&$giftcard_data,$giftcard_id=false)
	{
		if (strtotime($giftcard_data['expiration_date']))
			$giftcard_data['expiration_date'] = date('Y-m-d ', strtotime($giftcard_data['expiration_date']));
		else 
			$giftcard_data['expiration_date'] = '0000-00-00';
		//echo $giftcard_data['expiration_date'];
		if ($giftcard_data['customer_id'] == '' || $this->Customer->exists($giftcard_data['customer_id']))
        {
			if (!$giftcard_id or !$this->exists($giftcard_id))
			{
				if($this->db->insert('giftcards',$giftcard_data))
				{
					$giftcard_data['giftcard_id']=$this->db->insert_id();
					return true;
				}
				return false;
			}
	
			$this->db->where('giftcard_id', $giftcard_id);
			return $this->db->update('giftcards',$giftcard_data);
		}
		else {
			return false;
		}
	}

	/*
	Updates multiple giftcards at once
	*/
	function update_multiple($giftcard_data,$giftcard_ids)
	{
		$this->db->where_in('giftcard_id',$giftcard_ids);
		return $this->db->update('giftcards',$giftcard_data);
	}

	/*
	Deletes one giftcard
	*/
	function delete($giftcard_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("giftcard_id = '$giftcard_id' $course_id");
		return $this->db->update('giftcards', array('deleted' => 1, 'giftcard_number'=>null));
	}

	/*
	Deletes a list of giftcards
	*/
	function delete_list($giftcard_ids)
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('giftcard_id',$giftcard_ids);
		return $this->db->update('giftcards', array('deleted' => 1, 'giftcard_number'=>null));
 	}

	function get_linked_course_ids(&$course_ids)
	{
		$this->load->model('course');
		return $this->course->get_linked_course_ids($course_ids);
	}
 	/*
	Get search suggestions to find giftcards
	*/
	function get_search_suggestions($search,$limit=25, $ids = false)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $course_ids = join(',',$course_ids); 
			$course_id = "AND course_id IN ({$course_ids})";
	    }
        $suggestions = array();

		$this->db->from('giftcards');
		$this->db->where("(giftcard_number LIKE '$search%' OR details LIKE '%$search%')");
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("giftcard_number", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->giftcard_number, 'value' => ($ids ? $row->giftcard_id : $row->giftcard_number));
		}

		$this->db->from('giftcards');
		$this->db->join('people','giftcards.customer_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('giftcards').".deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name.($ids ? ' - '.$row->giftcard_number : ''), 'value' => ($ids ? $row->giftcard_id : $row->giftcard_number));		
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on giftcards
	*/
	function search($search, $limit=20, $offset = 0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $course_ids = join(',',$course_ids); 
			$course_id = "AND course_id IN ({$course_ids})";
	    }
        $this->db->from('giftcards');
		$this->db->join('people','giftcards.customer_id=people.person_id', 'left');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or 
                giftcard_number LIKE '%".$this->db->escape_like_str($search)."%' or 
                details LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('giftcards').".deleted=0 $course_id");
		$this->db->order_by("giftcard_number", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();	
	}
	
	public function get_giftcard_value( $giftcard_number )
	{
		if ( !$this->exists( $this->get_giftcard_id($giftcard_number)))
			return 0;
		
		$this->db->from('giftcards');
		$this->db->where('giftcard_number',$giftcard_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->limit(1);
		return $this->db->get()->row()->value;
	}
	
	function update_giftcard_value( $giftcard_number, $value )
	{
		$course_id = '';
	    if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->where("giftcard_number = '$giftcard_number' $course_id");
		$this->db->update('giftcards', array('value' => $value));
	}
}
?>

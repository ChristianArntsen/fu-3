<?php

	//Date 2/22/12
 /*
  * Features:
  * Holiday creation and deletion
  * Giftcard import
  * Giftcard purchasing
  * Giftcard expiration
  * Simulator online booking
  * DB fix for green fees and green fee types
  * Green fee types individual for each teesheet
  * Auto update teesheet (again)
  * DB fix keys for item_number, account_number, item_kit_number, and giftcard_number
  * Receipt printing
  * 7/8 minute teetimes
 */
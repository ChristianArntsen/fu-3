<script>
var data = {};
data.Payment_ID = '<?php echo $PaymentID; ?>';
var $ = window.parent.$;
var App = window.parent.App;

$.post(App.api_table + 'receipts/<?php echo $receipt_id; ?>/payments', data, function(payment){
	// Add payment to receipt
	var receipt = App.receipts.get(<?php echo $receipt_id; ?>);
	receipt.get('payments').add(payment);
	if(receipt.isPaid()){
		$.colorbox2.close();
	}

	// Close table (if everything is paid)
	App.closeTable();
},'json');
</script>
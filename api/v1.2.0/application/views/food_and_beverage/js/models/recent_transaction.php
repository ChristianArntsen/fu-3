var RecentTransaction = Backbone.Model.extend({
	idAttribute: "sale_id",
	defaults: {
		"total":0.00,
		"customer_name":"",
		"sale_time": "",
		"tips": [],
		"payments": []
	},

	initialize: function(attrs, options){
		var tips = new TipCollection( this.get('tips') );
		tips.url = App.api + 'recent_transactions/' + this.get('sale_id') + '/tips';

		this.set({'tips': tips});
	}
});

var Tip = Backbone.Model.extend({
	idAttribute: "type",
	defaults: {
		"amount": 0.00,
		"type": "",
		"invoice_id": 0,
		"tip_recipient": 0
	},

	initialize: function(attrs, options){
		this.set('type', attrs.type.replace('/',''));
	}
});

var TipCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new Tip(attrs, options);
	}
});
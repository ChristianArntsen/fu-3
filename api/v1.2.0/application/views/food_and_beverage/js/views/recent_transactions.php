var RecentTransactionsView = Backbone.View.extend({
	tagName: "ul",
	id: "recent_transactions",

	initialize: function() {
		this.listenTo(this.collection, "add remove change", this.render);
	},

	render: function() {
		var self = this;

		_.each(this.collection.models, function(transaction){
			self.renderTransaction(transaction);
		});
		return this;
	},

	renderTransaction: function(transaction){
		this.$el.append( new RecentTransactionView({'model':transaction}).render().el );
		return this;
	}
});
var CartListItemView = Backbone.View.extend({
	tagName: "tr",
	className: "reg-item-top",
	template: _.template( $('#template_cart_item').html() ),

	events: {
		"click a.edit_item": "openEditWindow",
		"click a.send_message": "openMessageWindow",
		"click": "selectItem"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model, "change:incomplete", this.markIncomplete);
		this.listenTo(this.model.get('modifiers'), "change", this.render);
		this.listenTo(this.model.get('sides'), "add remove change", this.render);
		this.listenTo(this.model.get('soups'), "add remove change", this.render);
		this.listenTo(this.model.get('salads'), "add remove change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}

		if(this.model.get('is_ordered')){
			this.$el.addClass('ordered');
		}else{
			this.$el.removeClass('ordered');
		}

		if(this.model.get('is_paid')){
			this.$el.addClass('paid');
		}else{
			this.$el.removeClass('paid');
		}

		// If the item has any required modifiers or sides, then automatically
		// pop up the edit window
		if(!this.model.get('showed_edit') && (
				this.model.get('modifiers').findWhere({'required': true}) ||
				this.model.get('number_soups') > 0 ||
				this.model.get('number_salads') > 0 ||
				this.model.get('number_sides') > 0
		)){
			// Set flag in item, so window doesn't keep popping up later
			this.model.set({'showed_edit': true});
			this.openEditWindow();
		}
		return this;
	},

	openEditWindow: function(){
		var item = this.model;
		var editItemWindow = new EditItemView({model:item});
		App.Page.itemEdit = editItemWindow;

		var highlightNotCompleted = false;
		if(item.get('incomplete')){
			var highlightNotCompleted = true;
		}

		$.colorbox({
			html: editItemWindow.render().el,
			width: 960,
			onClosed: function(){
				if(highlightNotCompleted){
					item.set({ 'incomplete': !item.isComplete() });
				}
				editItemWindow.remove();
			}
		});
		$('#cboxLoadedContent').css({'margin-top':'0px'});
		$('#cboxTitle').css({'display':'none'});
		return false;
	},

	openMessageWindow: function(){
		var item = this.model;
		var sendMessageWindow = new SendMessageView({model: item});

		$.colorbox({
			html: sendMessageWindow.render().el,
			width: 600,
			title: 'Send Message',
			onClosed: function(){
				sendMessageWindow.remove();
			}
		});

		return false;
	},

	selectItem: function(event){
		if(this.model.get('is_ordered')){
			return false;
		}

		if(this.model.get('selected')){
			this.model.set({"selected":false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected":true});
			this.$el.addClass('selected');
		}
		return false
	},

	markIncomplete: function(event){
		if(this.model.get('incomplete')){
			this.$el.addClass('incomplete');
		}else{
			this.$el.removeClass('incomplete');
		}
		return false
	}
});

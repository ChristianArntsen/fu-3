<style>
#register_log {
	width: 400px;
}

#register_log label {
	width: 140px;
	margin-right: 20px;
	float: left;
	text-align: right;
}

#register_log input {
	width: 70px;
}

#register_log div.form_field {
	margin-bottom: 5px;
}
</style>
<script type="text/html" id="template_close_register">
<div class="row">
	<div class="form_field">
		<label>Change</label>
		<input type="text" name="change" id="register_close_change" value="" />
	</div>
	<div class="form_field">
		<label>Ones</label>
		<input type="text" name="ones" id="register_close_ones" value="" />
	</div>
	<div class="form_field">
		<label>Fives</label>
		<input type="text" name="fives" id="register_close_fives" value="" />
	</div>
	<div class="form_field">
		<label>Tens</label>
		<input type="text" name="tens" id="register_close_tens" value="" />
	</div>
	<div class="form_field">
		<label>Twenties</label>
		<input type="text" name="twenties" id="register_close_twenties" value="" />
	</div>
	<div class="form_field">
		<label>Fifties</label>
		<input type="text" name="fifties" id="register_close_fifties" value="" />
	</div>
	<div class="form_field">
		<label>Hundreds</label>
		<input type="text" name="hundreds" id="register_close_hundreds" value="" />
	</div>
	<div class="form_field" style="margin-top: 15px; padding-top: 10px; border-top: 1px solid #D0D0D0;">
		<label style="font-weight: bold;">Closing Amount</label>
		<input type="text" name="amount" id="register_close_amount" value="<%=accounting.formatMoney(close_amount, '')%>" />
	</div>
</div>
<div class="row" style="padding: 20px; overflow: hidden;">
	<a href="#" class="close fnb_button" style="float: none; display: block; font-size: 14px; width: 100px; margin: 0 auto;" href="#">Close Register</a>
</div>
</script>
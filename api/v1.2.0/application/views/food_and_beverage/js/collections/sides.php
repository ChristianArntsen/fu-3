var SideCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new Side(attrs, options);
	}
});
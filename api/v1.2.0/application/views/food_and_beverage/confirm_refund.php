<?php

?>
<style>
	#issue_refund_wrapper {
		width:100%;
	}
</style>
<div id="issue_refund_wrapper">
	<fieldset>
	<?php echo form_open("food_and_beverage/issue_refund/".$cc_payment['invoice'],array('id'=>'refund_form')); ?>
	<ul id="error_message_box"></ul>
	
	<div class="field_row clearfix">
	<?php echo form_label('Name on Card:', 'payment_name'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['cardholder_name'] ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Account #:', 'payment_account'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['masked_account'] ?>
		</div>
	</div>
		<div class="field_row clearfix">
	<?php echo form_label('Card Type:', 'payment_card_type'); ?>
		<div class='form_field'>
			<?php echo $cc_payment['card_type'] ?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label('How much:', 'payment_amount'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'amount','value'=>$cc_payment['amount'], 'id'=>'amount'));?>
		</div>
	</div>
	<div class="field_row clearfix">
		By clicking submit, you are returning the stated amount to the credit card listed above.
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_submit'),
		'class'=>'submit_button float_left')
	);
	?>
	</form>
	</fieldset>
	</div>
	<script>
	$(document).ready(function(){
		var submitting = false;	
		$('#refund_form').validate({
			submitHandler:function(form)
			{
				if (submitting) return;
				if ($('#amount').val() > parseFloat('<?php echo $cc_payment['amount']?>'))
				{
					alert('You cannot refund more than $<?php echo $cc_payment['amount']?>');
					return;
				}
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				//console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
						console.log(response);
						submitting = false;
		                if (response.success)
			                window.location = 'index.php/sales';
			            else
			            	set_feedback('Credit card return error', 'error_message', true);    
		                $.colorbox.close();
					},
					dataType:'json'
				});
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li"
		});
	});
	</script>
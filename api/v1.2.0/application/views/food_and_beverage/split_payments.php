<style>
#split_payments div.items {
	width: 425px;
	float: left;
	height: 680px;
	background-color: white;
	border-right: 1px solid #E0E0E0;
}

#split_payments div.receipt-nav {
	height: 75px;
	width: 646px;
	float: right;
}

#split_payments div.receipt-nav a.left,
#split_payments div.receipt-nav a.right {
	float: left;
	width: 75px;
	height: 75px;
	margin: 0px;
	padding: 0px;
	background: #d6d6d6;
	background: -moz-linear-gradient(top,  #d6d6d6 0%, #c9c9c9 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d6d6d6), color-stop(100%,#c9c9c9));
	background: -webkit-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -o-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: -ms-linear-gradient(top,  #d6d6d6 0%,#c9c9c9 100%);
	background: linear-gradient(to bottom,  #d6d6d6 0%,#c9c9c9 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d6d6d6', endColorstr='#c9c9c9',GradientType=0 );
	color: #666;
	border: 1px solid #AAA;
	font-size: 40px;
	line-height: 75px;
	text-align: center;
	font-weight: bold;
}
#split_payments div.receipt-nav a.left:active,
#split_payments div.receipt-nav a.right:active {
	background: #E0E0E0 !important;
}

#split_payments div.receipt-nav a.right {
	float: right;
}

#split_payments div.receipt-nav a.left {
	margin-left: -1px;
}

#split_payments div.receipt-container {
	width: 290px;
	display: block;
	float: left;
	margin: 10px 0px;
	padding: 10px;
	overflow: hidden;
	background: none;
}

#split_payments div.new-container {
	width: 100px;
	min-height: 500px;
	display: block;
	float: left;
	margin: 10px;
	width: 86px;
	padding: 0px;
	overflow: hidden;
	background: none;
}
/*
#split_payments div.new-container a.new-ticket {
	border-radius: 40px;
	height: 80px;
	width: 80px;
	font-weight: bold;
	font-size: 60px;
	margin: 0px;
	padding: 0px;
	display: block;
	line-height: 80px;
	float: none;
	margin-top: 190px;
}
*/

#split_payments a.new-ticket {
	margin-top: 10px;
	width: 150px;
	float: left;
	margin-left: 152px;
	display: block;
}

#split_payments div.receipt-container a.pay {
	width: auto;
	display: block;
	float: none;
	margin: 10px 0px 0px 0px;
	font-size: 20px;
	font-weight: bold;
}

#split_payments div.receipt-container a.pay.disabled {
	background: #D9D9D9 !important;
	border: 1px solid #CCC;
	color: #AAA;
	cursor: arrow;
	text-shadow: none !important;
}

#split_payments div.receipt a {
	width: auto;
	float: none;
	display: block;
	padding: 0px 0px 0px 0px;
	line-height: 35px;
	height: 35px;
	margin: 0px 0px 10px 0px;
	position: relative;
}

#split_payments div.receipt a span.seat {
	position: absolute;
	top: 0px;
	bottom: 0px;
	display: block;
	width: 35px;
	line-height: 35px;
	text-align: center;
	background-color: #E0E0E0;
	box-shadow: inset 0px 0px 3px rgba(0,0,0,0.4);
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	font-weight: bold;
	color: #222;
	text-shadow: none;
}

#split_payments div.receipt div.title {
	display: block;
	width: auto;
	overflow: hidden;
	padding-bottom: 5px;
	margin-bottom: 5px;
	border-bottom: 1px solid #CCC;
}

#split_payments div.receipt div.title > span {
	font-size: 14px;
	color: #666;
	font-weight: normal;
	float: left;
	line-height: 16px;
	padding: 5px 0px;
}

#split_payments div.receipt div.title a.fnb_button.delete-receipt {
	float: right;
	padding: 5px 8px;
	height: auto;
	width: auto;
	line-height: 16px;
	margin: 0px;
	background: #d14d4d; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d', endColorstr='#c03939'); /* for IE */
	background: -webkit-linear-gradient(top, #d14d4d, #c03939);
	background: -moz-linear-gradient(top,  #d14d4d,  #c03939); /* for firefox 3.6+ */
}

#split_payments div.receipt {
	display: block;
	width: auto;
	padding: 10px;
	border: none;
	box-shadow: 0px 0px 3px #777;
	height: 450px;
	background: url("images/backgrounds/texture.png") repeat scroll;
}

#split_payments div.receipt.selected {
	border: 1px solid #CC2222;
	padding: 9px;
}

#split_payments div.receipt span.is-paid {
	display: none;
	font-size: 18px !important;
	font-weight: bold !important;
	color: #CCC;
	float: right !important;
}

#split_payments div.receipt.paid a.delete-receipt {
	display: none;
}

#split_payments div.receipt.paid span.is-paid {
	display: block;
}

#split_payments div.receipt.paid a.item {
	background: #D9D9D9 !important;
	border: 1px solid #BBB !important;
	color: #444 !important;
	cursor: arrow;
	text-shadow: none !important;
}

#split_payments div.receipt.paid a.item > span.seat {
	box-shadow: none;
	color: #666 !important;
	border-right: 1px solid #BBB;
}

#split_payments .header_cell {
	width: auto !important;
	margin-left: 0px !important;
}

#split_payments div.receipts {
	float: right;
	display: block;
	width: 630px;
	height: 575px;
	overflow-x: hidden;
	overflow-y: hidden;
	position: relative;
}

#split_payments div.scroll-content {
	height: 600px;
	width: 5000px;
	display: block;
	overflow: hidden;
	position: relative;
}

#split_payments a.add-to-receipt {
	display: block;
	margin: 0px;
	padding: 5px;
	width: auto;
}

#split_payments table td {
	padding: 15px;
}
</style>
<div id="split_payments">
	<div class="items">
		<table id="splitpayment_register" class="items">
			<!-- table header with labels -->
			<thead>
				<tr>
					<th class="reg_item_seat" id="reg_item_seat"><div class="header_cell header">Seat<span class="sortArrow">&nbsp;</span></div></th>
					<th class="reg_item_name" id="splitpayment_item_name"><div class="header_cell header"><?php echo lang('sales_item_name'); ?><span class="sortArrow">&nbsp;</span></div></th>
					<th class="reg_item_discount" id="splitpayment_item_discount"><div class="header_cell header"><?php echo lang('sales_discount'); ?><span class="sortArrow">&nbsp;</span></div></th>
					<th class="reg_item_total" id="splitpayment_item_total"><div class="header_cell header"><?php echo lang('sales_total'); ?><span class="sortArrow">&nbsp;</span></div></th>
				</tr>
			</thead>
			<!-- Items in the cart -->
			<tbody id="splitpayment_cart_contents">
				<?php if(count($cart)==0) {?>
				<tr>
					<td colspan='4' style="height:60px;border:none;"></td>
				</tr>
				<?php
				}
				else
				{
					$alternating_class = 'even';
					foreach(array_reverse($cart, true) as $line=>$item)
					{
						$alternating_class = ($alternating_class == 'even')?'odd':'even';
						$error_class = ($giftcard_error_line &&  $giftcard_error_line == $line)?'giftcard_number_error':'';
						$cur_item_info = isset($item['item_id']) ? $this->Item->get_info($item['item_id']) : $this->Item_kit->get_info($item['item_kit_id']);
					?>
						<tr data-item-id="<?php echo $item['item_id']; ?>" data-line="<?php echo $item['line']; ?>" data-seat="<?php echo $item['seat']; ?>" data-price="<?php echo $item['total']; ?>" class="reg_item_top <?php echo $alternating_class.' '.$error_class;?>" id="splitpayment_item_top_<?php echo $line ?>">
							<?php
								$is_teetime = $this->Item->is_teetime($item['item_id']);
								$is_cart = $this->Item->is_cart($item['item_id']);
								$price_category_index = ($item['price_category']) ? $item['price_category']:substr($item['item_number'], strpos($item['item_number'], '_')+1, strrpos($item['item_number'], '_')-strpos($item['item_number'], '_')-1);
								$teetime_type_index = ($item['teetime_type']) ? $item['teetime_type']:substr($item['item_number'], strrpos($item['item_number'], '_')+1);
							?>
							<td class="reg_item_seat" id="splitpayment_item_seat_<?php echo $line; ?>">
								<div><?php echo (int) $item['seat']; ?></div>
							</td>
							<td class="reg_item_name" id="splitpayment_item_name_<?php echo $line ?>">
								<div><?php echo $item['name']; ?></div>
							</td>
							<td class="reg_item_discount editable" id="splitpayment_item_discount_<?php echo $line ?>">
								<div><?php echo $item['discount']; ?></div>
							</td>
							<td class="reg_item_total" id="splitpayment_item_total_<?php echo $line ?>">
								<div class='total_price_box'><?php echo to_currency($item['total']); ?></div>
							</td>
						</tr>
					<?php
					}
				}
				?>
			</tbody>
		</table>
	</div>
	<div class="receipt-nav">
		<a class="left"><</a>
		<a class="new-receipt fnb_button new-ticket">+ New Receipt</a>
		<a class="right">></a>
	</div>
	<div class="receipts">
		<div class="scroll-content">
			<?php if(!empty($receipts)){
			$first = true; ?>
			<?php foreach($receipts as $receipt){
			$btnClass = "";
			$receiptPaid = "";
			if($receipt['date_paid'] != NULL){
				$receiptPaid = " paid";
				$btnClass = " disabled";
			}
			?>
			<div class="receipt-container">
				<div class="receipt<?php if($first){ echo ' selected'; } ?><?php echo $receiptPaid; ?>" data-payment-amount="<?php echo $receipt['total_paid']; ?>" data-receipt-id="<?php echo $receipt['receipt_id']; ?>" data-total="<?php echo $receipt['total']; ?>">
				<div class="title">
					<span>Receipt #<?php echo $receipt['receipt_id']; ?></span>
					<a class="fnb_button delete-receipt">Delete</a>
					<span class="is-paid">PAID</span>
				</div>
				<?php if(!empty($receipt['items'])){
				foreach($receipt['items'] as $item){ ?>
					<a class="fnb_button item" data-item-id="<?php echo $item['item_id']; ?>" data-line="<?php echo $item['line']; ?>" data-price="<?php echo $item['unsplit_total']; ?>" data-seat="<?php echo $item['seat']; ?>">
						<span class="seat"><?php echo (int) $item['seat']; ?></span>
						<?php echo $item['name']; ?>
					</a>
				<?php } }else{
				$btnClass = " disabled";
				} ?>
				</div>
				<a class="fnb_button pay<?php echo $btnClass; ?>" href="#">Pay Now</a>
			</div>
			<?php $first = false; } } else { ?>
			<div class="receipt-container">
				<div class="receipt" data-receipt-id="1">
					<div class="title">
						<span>Receipt #1</span>
						<a class="fnb_button delete-receipt">Delete</a>
						<span class="is-paid">PAID</span>
					</div>
				</div>
				<a class="fnb_button pay disabled" href="#">Pay Now</a>
			</div>
			<?php } ?>
			<!-- <div class="new-container">
				<a class="fnb_button new-ticket" href="#">+</a>
			</div> -->
		</div>
	</div>
</div>
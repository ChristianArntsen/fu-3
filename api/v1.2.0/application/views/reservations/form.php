<div class='background'>
<?php
echo form_open('/reservations/save/'.$reservation_info->reservation_id,array('id'=>'teetime_form'));
//echo 'teesheet holes: '.$teesheet_holes.'<br/>';
//print_r($reservation_info);
?>
<div id='radioset' class='teebuttons'>
	<input type='radio' value='teetime' id='type_teetime' name='event_type'  <?php echo ($reservation_info->type == 'teetime' || $reservation_info->type == '')?'checked':''; ?>><label id='teetime_label' for='type_teetime' >Tee Time</label>
    <input type='radio' value='tournament' id='type_tournament' name='event_type' <?php echo ($reservation_info->type == 'tournament')?'checked':''; ?>><label id='tournament_label' for='type_tournament'>Tournament</label>
    <input type='radio' value='league' id='type_league' name='event_type' <?php echo ($reservation_info->type == 'league')?'checked':''; ?>><label id='league_label' for='type_league'>League</label>
    <input type='radio' value='event' id='type_event' name='event_type' <?php echo ($reservation_info->type == 'event')?'checked':''; ?>><label id='event_label' for='type_event'>Event</label>
    <input type='radio' value='closed' id='type_closed' name='event_type' <?php echo ($reservation_info->type == 'closed')?'checked':''; ?>><label id='closed_label' for='type_closed'>Block</label>
    <div class='clear'></div>
</div>
<div id='closed_table'  <?php echo ($reservation_info->type == 'closed')?'':"style='display:none'"?>>
	<div>
		<input tabindex=1 class='' id='closed_title' name='closed_title' placeholder='Title' value='<?php echo $reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
	</div>
	<div class='popup_divider'></div>
   <div>	
       	<textarea tabindex=2 class='' placeholder='Details' id='closed_details' name='closed_details' style='width:94%;height:50px;'><?php echo $reservation_info->details; ?></textarea>
	</div>
	<div class='event_buttons_holder'>
  		<span class='saveButtons'>
			<button id='closed_save'>Save</button>
    	</span>
    	<button id='closed_delete' class='deletebutton'>Delete</button>
    	<div class='clear'></div>
    </div>
</div>
<div id='event_table'  <?php echo ($reservation_info->type == 'tournament' || $reservation_info->type == 'league' || $reservation_info->type == 'event')?'':"style='display:none'"?>>
	<div class='event_info'>
		<input class='' id='event_title' name='event_title' placeholder='Title' value='<?php echo $reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
    	<span class='flag_icon'></span>
	 	<span class='holes_buttonset'>
            <input type='radio' id='event_holes_9' name='event_holes' value='9' <?php echo (($teesheet_holes == '9' && $reservation_info->holes != '18') || $reservation_info->holes == '9')?'checked':''; ?>/>
            <label for='event_holes_9' id='event_holes_9_label'>9</label>
            <input type='radio' id='event_holes_18' name='event_holes' value='18'  <?php echo (($teesheet_holes == '18' && $reservation_info->holes != '9') || $reservation_info->holes == '18')?'checked':''; ?>/>
            <label for='event_holes_18' id='event_holes_18_label'>18</label>
        </span>
	 	<div class='player_buttons'>
	        <span class='players_icon'></span>
            <div class='players'>
            	<span class='players_buttonset'>
        			<input class='teetime_players' id='event_players' name='event_players' value='<?php echo $reservation_info->player_count; ?>'/>
        		</span>
    		</div>
            <span class='carts_icon'></span>
	        <div class='carts'>
            	<span class='carts_buttonset'>
    				<input class='teetime_carts' id='event_carts' name='event_carts' value='<?php echo $reservation_info->carts; ?>'/>
    			</span>
    		</div>
       	</div>
       	<div class='clear'></div>
    </div>
    <div class='popup_divider'></div>
    <div>
       	<textarea tabindex=4 class='details_box' placeholder='Details' id='event_details' name='event_details' style='width:94%;height:50px;'><?php echo $reservation_info->details; ?></textarea>
    </div>
    <div class='event_buttons_holder'>
 		<span class='saveButtons'>
			<button id='event_save'>Save</button>
    	</span>
    	<button id='event_delete' class='deletebutton'>Delete</button>
     	<div class='clear'></div>
   </div>
</div>
<div id='teetime_table' <?php echo ($reservation_info->type == 'teetime' || $reservation_info->type == '')?'':"style='display:none'"?>>
	<div id='teetime_people_row_1'>
		<div class='player_info'>
			<input tabindex=1 class='' id='teetime_title' name='teetime_title' placeholder='Last, First name' value='<?php echo ($reservation_info->person_name)?$reservation_info->person_name:$reservation_info->title; ?>' style='width:160px, margin-right:5px;'/>
			<input type='hidden' id='person_id' name='person_id' value='<?php echo $reservation_info->person_id; ?>'/>
	   		<input tabindex=2 type=text value='<?php echo $customer_info[$reservation_info->person_id]->email; ?>' id='email' name='email' placeholder='Email' class='ttemail'/>
	   		<input  tabindex=3 type=text value='<?php echo $customer_info[$reservation_info->person_id]->phone_number; ?>' id='phone' name='phone' placeholder='888-888-8888' class='ttphone'/>
	 	</div>
	 	<span <?php echo (!$track_info->reround_track_id)?"style='display:none'":''; ?>>
		 	<span class='flag_icon'></span>
		 	<span class='holes_buttonset'>
		        <input type='radio' id='teetime_holes_9' name='teetime_holes' value='9' <?php echo (($teesheet_holes == '9' && $reservation_info->holes != '18') || $reservation_info->holes == '9' || (!$track_info->reround_track_id && $reservation_info->holes != '18'))?'checked':''; ?>/>
		        <label for='teetime_holes_9' id='teetime_holes_9_label'>9</label>
		        <input type='radio' id='teetime_holes_18' name='teetime_holes' value='18'  <?php echo (($teesheet_holes == '18' && $reservation_info->holes != '9') || $reservation_info->holes == '18' || ($track_info->reround_track_id && $reservation_info->holes != '9'))?'checked':''; ?>/>
		        <label for='teetime_holes_18' id='teetime_holes_18_label'>18</label>
		    </span>
	    </span>
	    <input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
		<!--label for='save_customer_1' title='Save to Customers'>
			<span class='checkbox_image' id='save_customer_1_image'></span>
    	</label>
   		<a class='expand_players down' href='#' title='Expand'>Expand</a-->
   		<div class='clear'></div>
   	</div>
   	<div class='popup_divider'></div>
    <div id='teetimes_row_2' >
        <div id='players' class='player_buttons'>
        	<span class='players_icon'></span>
	 		<span class='players_buttonset'>
    			<input type='radio' id='players_1' name='players' value='1' <?php echo ($reservation_info->player_count == '1' || $reservation_info->player_count == '0')?'checked':''; ?>/>
    			<label for='players_1' id='players_1_label'>1</label>
    			<input type='radio' id='players_2' name='players' value='2' <?php echo ($reservation_info->player_count == '2')?'checked':''; ?>/>
    			<label for='players_2' id='players_2_label'>2</label>
    			<input type='radio' id='players_3' name='players' value='3' <?php echo ($reservation_info->player_count == '3')?'checked':''; ?>/>
    			<label for='players_3' id='players_3_label'>3</label>
    			<input type='radio' id='players_4' name='players' value='4' <?php echo ($reservation_info->player_count == '4')?'checked':''; ?>/>
    			<label for='players_4' id='players_4_label'>4</label>
    			<input type='radio' id='players_5' name='players' value='5' <?php echo ($reservation_info->player_count == '5')?'checked':''; ?>/>
    			<label for='players_5' id='players_5_label'>5</label>
			</span>
		</div>
	    <div id='carts'>
	    	<span class='carts_icon'></span>
	 		<span class='carts_buttonset'>
				<input type='radio' id='carts_0' name='carts' value='0' <?php echo ($reservation_info->carts == '0')?'checked':''; ?>/>
				<label for='carts_0' id='carts_0_label'>0</label>
				<input type='radio' id='carts_1' name='carts' value='1' <?php echo ($reservation_info->carts == '1')?'checked':''; ?>/>
				<label for='carts_1' id='carts_1_label'>1</label>
				<input type='radio' id='carts_2' name='carts' value='2' <?php echo ($reservation_info->carts == '2')?'checked':''; ?>/>
				<label for='carts_2' id='carts_2_label'>2</label>
				<input type='radio' id='carts_3' name='carts' value='3' <?php echo ($reservation_info->carts == '3')?'checked':''; ?>/>
				<label for='carts_3' id='carts_3_label'>3</label>
				<input type='radio' id='carts_4' name='carts' value='4' <?php echo ($reservation_info->carts == '4')?'checked':''; ?>/>
				<label for='carts_4' id='carts_4_label'>4</label>
				<input type='radio' id='carts_5' name='carts' value='5' <?php echo ($reservation_info->carts == '5')?'checked':''; ?>/>
				<label for='carts_5' id='carts_5_label'>5</label>
			</span>
		</div>
		<!--div>
			<button id='paid_players' disabled><?php echo $reservation_info->paid_player_count; ?> paid</button>
		</div>
		<div>
			<button id='paid_carts' disabled><?php echo $reservation_info->paid_carts; ?> paid</button>
		</div-->
		<div class='clear'></div>
		 <div class='event_controls_holder'>
		 	<!--a class='expand_players down' href='#' title='Expand'>Expand</a-->
   		
	   		<input type='checkbox' value='1' name='expand_players' id='expand_players'/><span id='expand_players_text' style='font-size:14px; padding-left:5px;'>List all</span>
			<input type='checkbox' value='1' name='split_teetimes' id='split_teetimes'/><span id='split_teetime_text' style='font-size:14px; padding-left:5px;'>Split</span>
		</div>
		
	</div>
	
    <div id='teetimes_row_3'>
    	<div id='teetime_people_table_holder'>
    		<table id='teetime_people_table' style='display:none'>
    			<tbody>
    				<tr  id='teetime_people_row_label'>
    					<td colspan='4'>Additional players</td>
    				</tr>
    				<?php for ($i = 2; $i <= 5; $i++) { ?>
    				<tr id='teetime_people_row_<? echo $i?>' >
    					<td>
    						<span class='clear_data' onclick='clear_player_data(<? echo $i?>)'>x</span>
    					</td>
			        	<td>
			        		<input tabindex=<? echo (($i-1)*3)+1?> class='' placeholder='Last, First name' id='teetime_title_<? echo $i?>' name='teetime_title_<? echo $i?>' value='<?php $person_name = 'person_name_'.$i; echo $reservation_info->$person_name; ?>' style='width:160px, margin-right:5px;'/>
			        		<input type='hidden' id='person_id_<? echo $i?>' name='person_id_<? echo $i?>' value='<?php $person_id = 'person_id_'.$i; echo $reservation_info->$person_id; ?>'/>
			        	</td>
			            <td>
			        		<input tabindex=<? echo (($i-1)*3)+2?> type=text placeholder='Email' value='<?php echo $customer_info[$reservation_info->$person_id]->email; ?>' id='email_<? echo $i?>' name='email_<? echo $i?>' class='ttemail'/>
			        	</td>
			        	<td>
			        		<input  tabindex=<? echo (($i-1)*3)+3?> type=text placeholder='888-888-8888' value='<?php echo $customer_info[$reservation_info->$person_id]->phone_number; ?>' id='phone_<? echo $i?>' name='phone_<? echo $i?>' class='ttphone'/>
			        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        	</td>
			        	<!--td>
			        		<label for='save_customer_<? echo $i?>'>
			        			<span class='checkbox_image' id='save_customer_<? echo $i?>_image'></span>
				        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        		</label>
			        	</td-->
			        </tr>
			        <?php } ?>
    			</tbody>
    		</table>
   		</div>
	</div>
	<div class='popup_divider'></div>
    <div>
    	 <textarea tabindex=20 class='' placeholder='Details' id='details' name='teetime_details' style='width:94%;height:50px;'><?php echo $reservation_info->details; ?></textarea>
    </div>
   <div class='event_buttons_holder'>
   		<button id='teetime_delete' class='deletebutton' style='float:left;'>Delete</button>
    	<span class='saveButtons'>
        <?php if ($this->config->item('sales')) {?>
        	<span class='pay_icon'></span>
	 		<!--button id='teetime_purchase' disabled>Purchase</button-->
        <?php 
        	for ($i = 1; $i <= 5; $i++) {
        		//if ($i >= $reservation_info->paid_player_count) 
        			echo "<button class='purchase_button' id='purchase_$i'>$i</button>";
			}
        ?>	
        	<!--button id='purchase_1'>1</button><button id='purchase_2'>2</button><button id='purchase_3'>3</button><button id='purchase_4'>4</button><button id='purchase_5'>5</button-->
        <?php } else { ?>
        	<button id='teetime_checkin'><? echo $checkin_text; ?></button>
        	<input type='hidden' id='checkin' name='checkin' value='0'/>
        <?php } ?>
        </span>
        <button id='teetime_save' style='float:left;'><? echo $save_text; ?></button>
    	<button id='teetime_confirm' class='confirmButton' style='display:none'>Confirm</button>
        <button id='teetime_cancel' style='display:none' class='cancelbutton'>Cancel</button>
        <div class='clear'></div>
    </div>
<input type='hidden' id='purchase_quantity' name='purchase_quantity' value='0'/>
<input type='hidden' id='delete_teetime' name='delete_teetime' value='0'/>
<input type='hidden' name='start' value='<? echo $reservation_info->start; ?>'/>
<input type='hidden' name='end' value='<? echo $reservation_info->end; ?>'/>
<input type='hidden' name='track_id' value='<? echo $reservation_info->track_id; ?>'/>
<input type='hidden' name='side' value='<? echo $reservation_info->side; ?>'/>
<input type='hidden' name='paid_carts' value='<? echo $reservation_info->paid_carts; ?>'/>
<input type='hidden' name='paid_player_count' value='<? echo $reservation_info->paid_player_count; ?>'/>
<?php form_close(); ?>
</div>
<script type='text/javascript'>
function clear_player_data(row) {
	if (row == 1)
	{
		$('#teetime_title').val('');
		$('#person_id').val('');
		$('#email').val('');
		$('#phone').val('');
	}
	else
	{
		$('#teetime_title_'+row).val('');
		$('#person_id_'+row).val('');
		$('#email_'+row).val('');
		$('#phone_'+row).val('');
	}
}
function focus_on_title(title) {
	<?php if ($reservation_info->type == 'teetime' || $reservation_info->type == '')
				$focus = 'teetime_title';
		  else if ($reservation_info->type == 'closed')
		  		$focus = 'closed_title';
		  else
				$focus = 'event_title';
		  ?>
		  title = (title == undefined)?'<?php echo $focus;?>':title;
		  //console.log('trying to focus on title: '+title);
	
    $('#'+title).focus().select();
}
$(document).ready(function(){
	$("#phone").mask2("(999) 999-9999");
	$("#phone_2").mask2("(999) 999-9999");
	$("#phone_3").mask2("(999) 999-9999");
	$("#phone_4").mask2("(999) 999-9999");
	$("#phone_5").mask2("(999) 999-9999");
		
	var submitting = false;
	$('#teetime_form').bind('keypress', function(e) {
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		   $('#teetime_save').click();
		 }
    });
    $('#teetime_form').validate({
		submitHandler:function(form)
		{
			var type = $('input[name=event_type]:checked').val();
			var proceed_with_save = true;
			if ($('#delete_teetime').val() == 1)
			{
				proceed_with_save = confirm('Are you sure you want to delete this event?');
			}
			else if ((type == 'teetime' && $('#teetime_title').val() == '') ||
				    ((type == 'tournament' || type == 'league' || type == 'event') && $('#event_title').val() == '') ||
				     (type == 'closed' && $('#closed_title').val() == ''))
				 {
				     alert('Please enter a name/title for this event');
				     proceed_with_save = false;
				 }
			if (proceed_with_save)
			{
				if (submitting) return;
				submitting = true;
				$(form).mask('<?php echo lang('common_wait'); ?>');
				//console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
						console.log('------save reservation------');
						console.dir(response);
						Calendar_actions.update_teesheet(response.reservations);
		                submitting = false;
		                post_teetime_form_submit(response);
		                if (response.send_confirmation != '')
		                	Calendar_actions.send_confirmation(response.send_confirmation);
		                if (response.go_to_register)
		                	window.location = '/index.php/sales'; 
		            	currently_editing = '';
		        		$.colorbox.close();
		            },
					dataType:'json'
				});
			}
			else
				$('#delete_teetime').val(0) ;
		},
		errorLabelContainer: '#error_message_box',
 		wrapper: 'li'
	});
	
	$('#teetime_name').focus().select();
	$('#expand_players').click(function(e){
    	var checked = $(e.target).attr('checked');
    	if (checked)
    	{
	    	$('#teetime_people_table').show();
	    }
    	else
    	{
	    	$('#teetime_people_table').hide();
	    }
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();
	
	    $.colorbox.resize();
    }); 
    for (var q = 1; q <= 5; q++) {
		// Save customer event listeners
	    $('#save_customer_'+q).click(function() {
	    	if ($(this).attr('checked'))
	        	$(this).prev().addClass('checked');
	        else
	   	    	$(this).prev().removeClass('checked');
	    });      
	}
	$('#purchase_1').click(function () {$('#purchase_quantity').val(1);});
	$('#purchase_2').click(function () {$('#purchase_quantity').val(2);});
	$('#purchase_3').click(function () {$('#purchase_quantity').val(3);});
	$('#purchase_4').click(function () {$('#purchase_quantity').val(4);});
	$('#purchase_5').click(function () {$('#purchase_quantity').val(5);});
	$('#teetime_checkin').click(function() {$('#checkin').val(1);});
	$('#teetime_delete').click(function(){$('#delete_teetime').val(1);});
	$('#event_delete').click(function(){$('#delete_teetime').val(1);});
	$('#closed_delete').click(function(){$('#delete_teetime').val(1);});
	// Button icons and styles
	$('#paid_carts').button();
	$('#paid_players').button();
	$('#teetime_save').button();
    $('#event_save').button();
    $('#closed_save').button();
    $('#teetime_delete').button();
    $('#event_delete').button();
    $('#closed_delete').button();
    $('.purchase_button').parent().buttonset();
    $('#teetime_checkin').button();
    $('#teetime_holes_9').button();
    $('#event_holes_9').button();
    $('#teetime_confirm').button();
    $('#teetime_cancel').button();
    $('.teebuttons').buttonset();
    $('.holes_buttonset').buttonset();
    $('#players_0').button();
    $('#carts_0').button();
    $('.players_buttonset').buttonset();
    $('.carts_buttonset').buttonset();
    $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( '#teetime_title' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title').val(ui.item.label);
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title').val(ui.item.label);
		}
	});
	$( '#teetime_title_2' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title_2').val(ui.item.label);
			$('#email_2').val(ui.item.email).removeClass('teetime_email');
			$('#phone_2').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_2').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_2').val(ui.item.label);
		}
	});
	$( '#teetime_title_3' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title_3').val(ui.item.label);
			$('#email_3').val(ui.item.email).removeClass('teetime_email');
			$('#phone_3').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_3').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_3').val(ui.item.label);
		}
	});
	$( '#teetime_title_4' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title_4').val(ui.item.label);
			$('#email_4').val(ui.item.email).removeClass('teetime_email');
			$('#phone_4').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_4').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_4').val(ui.item.label);
		}
	});
	$( '#teetime_title_5' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title_5').val(ui.item.label);
			$('#email_5').val(ui.item.email).removeClass('teetime_email');
			$('#phone_5').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#person_id_5').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title_5').val(ui.item.label);
		}
	});
	$('#teetime_title, #teetime_title_2, #teetime_title_3, #teetime_title_4, #teetime_title_5').keyup(function(){
		var focus = $(this);
		if (focus.val() == '')
		{
			console.dir(focus);
			var index = focus.attr('id').replace('teetime_title_', '');
			index = (index == 'teetime_title' ? 1 :index);
		
			clear_player_data(index);
		}
	})
	// Phone number events
	/*$('#phone').mask('999-999-9999');
    $('#phone_2').mask('999-999-9999');
    $('#phone_3').mask('999-999-9999');
    $('#phone_4').mask('999-999-9999');
    $('#phone_5').mask('999-999-9999');*/
    $('#phone').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#phone' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone').val(ui.item.label);
		}
		
	});
    $('#phone_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $( '#phone_2' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_2').val(ui.item.name).removeClass('teetime_name');
			$('#email_2').val(ui.item.email).removeClass('teetime_email');
			$('#phone_2').val(ui.item.label);
			$('#person_id_2').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_2').val(ui.item.label);
		}
		
	});
    $('#phone_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $( '#phone_3' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_3').val(ui.item.name).removeClass('teetime_name');
			$('#email_3').val(ui.item.email).removeClass('teetime_email');
			$('#phone_3').val(ui.item.label);
			$('#person_id_3').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_3').val(ui.item.label);
		}
		
	});
    $('#phone_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $( '#phone_4' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_4').val(ui.item.name).removeClass('teetime_name');
			$('#email_4').val(ui.item.email).removeClass('teetime_email');
			$('#phone_4').val(ui.item.label);
			$('#person_id_4').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_4').val(ui.item.label);
		}
		
	});
    $('#phone_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    $( '#phone_5' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_5').val(ui.item.name).removeClass('teetime_name');
			$('#email_5').val(ui.item.email).removeClass('teetime_email');
			$('#phone_5').val(ui.item.label);
			$('#person_id_5').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone_5').val(ui.item.label);
		}
		
	});
	// Email
	$('#email').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#email' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email').val(ui.item.label);
		}
		
	});
    
	$('#email_2').keypress(function (e) {
    	$('#save_customer_2').attr('checked', true);
    	$('#save_customer_2').prev().addClass('checked');
    });
    $( '#email_2' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_2').val(ui.item.name).removeClass('teetime_name');
			$('#phone_2').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_2').val(ui.item.label);
			$('#person_id_2').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_2').val(ui.item.label);
		}
		
	});
	$('#email_3').keypress(function (e) {
    	$('#save_customer_3').attr('checked', true);
    	$('#save_customer_3').prev().addClass('checked');
    });
    $( '#email_3' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_3').val(ui.item.name).removeClass('teetime_name');
			$('#phone_3').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_3').val(ui.item.label);
			$('#person_id_3').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_3').val(ui.item.label);
		}
		
	});
	$('#email_4').keypress(function (e) {
    	$('#save_customer_4').attr('checked', true);
    	$('#save_customer_4').prev().addClass('checked');
    });
    $( '#email_4' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_4').val(ui.item.name).removeClass('teetime_name');
			$('#phone_4').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_4').val(ui.item.label);
			$('#person_id_4').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_4').val(ui.item.label);
		}
		
	});
	$('#email_5').keypress(function (e) {
    	$('#save_customer_5').attr('checked', true);
    	$('#save_customer_5').prev().addClass('checked');
    });
    $( '#email_5' ).autocomplete({
		source: '<?php echo site_url('reservations/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title_5').val(ui.item.name).removeClass('teetime_name');
			$('#phone_5').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email_5').val(ui.item.label);
			$('#person_id_5').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email_5').val(ui.item.label);
		}
		
	});

    //Calendar_actions.event.make_styled_buttons();
    
    
	// CODE BELOW REQUIRES REVIEW
	
    $('#type_teetime').click(function(){
    	$('#teetime_table').show();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('teetime_title');
    });
    $('#type_tournament').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_league').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_event').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_closed').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').show();
    	$('#event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('closed_title');
    });

	$('.teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('.teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('.teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('.teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    
      
	
});
</script>
<?php $this->load->view("partial/course_header"); ?>
<script>
$(function(){
	$('#edit_account').bind('submit', function(e){
		var url = $(this).attr('action');
		var data = $(this).serialize();

		$.post(url, data, function(response){
			if(response.success){
				set_feedback(response.message, 'success_message', false);
			}else{
				set_feedback(response.message, 'error_message', false);
			}
		},'json');

		return false;
	});

	$('#accordion').accordion({
		autoHeight: false,
		changestart: function(e, ui) {
			var url = ui.newHeader.attr('data-url');

			if(!url || ui.newContent.html()){
				return true;
			}

			$.get(url, null, function(response){
				ui.newContent.html(response);
				//$('#accordion').accordion("refresh");
			});
			return true;
		}
	});

	$('#birthday_visible').datepicker({
		defaultDate:new Date(),
		dateFormat:'mm/dd/yy',
		changeMonth: true,
		changeYear: true,
		yearRange: "1900:" + new Date().getFullYear(),
		altFormat: "yy-mm-dd",
		altField: "#birthday"
	});

	$('a.cancel').click(function(e){

		var teetime = $(this).parents('li');

		if(confirm("Are you sure you want to cancel this reservation?")){

			var url = $(this).attr('href');
			$.get(url, null, function(response){
				if(response.success){
					set_feedback(response.message, 'success_message', false);
					teetime.fadeOut();
				}else{
					set_feedback(response.message, 'error_message', false);
				}
			},'json');

		}
		return false;
	});
});
</script>
<style>
html, table, div, .ui-widget {
		font-family:Quicksand, Helvetica,Arial, sans-serif;
	}

    html {
		background: url(<?php echo base_url(); ?>images/backgrounds/course.png)no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		font-family: Quicksand, Helvetica, Arial, sans-serif;
	}
   .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider {
        margin: 10px 10px 10px 20px;
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left;
        width: 863px;
	background: #efefef;
        height:400px;
}
.tab_content {
	padding:0px 20px;
	font-size: 1.2em;
        height:330px;
        overflow: auto;
}
    ul.tabs {
	margin: 0;
	padding: 0px 0px 0px 4px;
	float: left;
	list-style: none;
	height: 36px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 860px;
	background:url(<?php echo base_url(); ?>images/backgrounds/top_bar_bg_black.png) repeat-x;
}
ul.tabs li {
	float: left;
	margin: 0px 2px;
	padding: 0;
	height: 30px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 30px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	margin-top:5px;
	overflow: hidden;
	position: relative;
	background: #d5d5d5;
	border-radius:5px 5px 0px 0px;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 0px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:85px;
        text-align: center;
        line-height: 18px;
	border-radius:5px 5px 0px 0px;
}
ul.tabs li a div {
    height:12px;
    font-size:12px;
    font-weight:bold;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
label.ui-button {
	padding:0px 5px 0px;
}
.ui-button .ui-button-text {
	line-height: 15px;
}
.ui-button-text-only .ui-button-text {
	padding:5px 10px;
}
div label.ui-state-active {
	border-right: none;
	box-shadow: inset -2px 2px 30px 3px black;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#222222', endColorstr='#333333'); /* for IE */
}
.flag_icon, .time_icon, .player_icon {
	display: block;
	height: 32px;
	width: 43px;
	background: url(<?php echo base_url(); ?>images/icons/shadow_icons.png) no-repeat 0px -208px;
	float: left;
	margin: 6px 0px 0px 5px;
}
.player_icon {
	float:right;
	background-position:-4px -9px;
	margin-right:25px;
}
.flag_icon {
	background-position: 0px -162px;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #efefef;
	border-bottom: 1px solid #efefef; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set, #hole_button_set {
    float:left;
    margin:10px 60px 0 15px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding: 5px;
    border-bottom: 1px solid #ccc;
    box-shadow:0px 1px 0px 0px white;
}
.teetime {
    border:none;
    padding:8px 15px;
    background:transparent;
    width:48%;
    float:left;
    border-radius:0px;
    border-bottom:1px solid #ccc;
    border-right:1px solid #ccc;
    box-shadow:1px 1px 0px 0px white;
}
div.even {
	border-right:none 0px;
	box-shadow:0px 1px 0px 0px white;
	padding-right:0px;
}
div.odd {
	padding-left:0px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:15%;
    margin-top:5px;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.loading_golf_ball {
    font-size:14px;
    padding-top:94px;
    text-align:center;
}
.be_header {
    color:#336699;
    margin:15px auto 5px;
    text-align: center;
}
.be_form {
    width:65%;
    margin:auto;

}
.login_button {
	height:32px;
}
#employee_basic_info.login_info {
    margin-right:5px;
}
.register_form {
    width:85%;
}
.be_form td input {
    margin-top:3px;
    margin-left:5px;
}
.be_fine_print {
    color:#999999;
    font-size:11px;
    text-align:center;
}
.be_form td hr {
    color:#336699;
    margin:11px 0px 7px;
}
.be_reserve_buttonset {
    float:left;
    margin-right:22px;
}
.be_buttonset_label {
    line-height: 30px;
    color:#336699;
    margin-right:10px;
}
#cboxLoadedContent .teetime .course_name {
    font-size:1.4em;
    margin-top:6px;
}
#cboxLoadedContent .teetime .info {
    padding-left:10px;
    margin-top:7px;
}
#cboxLoadedContent .teetime .time, #cboxLoadedContent .teetime .holes, #cboxLoadedContent .teetime .date {
    color:#336699;
    font-size:1em;
    padding:2px 6px;
    float:left;
    line-height:17px;
    margin-right:4px;
}
#content_area .teetime .holes, #content_area .teetime .price, #content_area .teetime .price_label, #content_area .teetime .spots {
    padding:2px 6px 2px 4px;
    float:left;
    line-height:17px;
    margin-right:14px;
    font-size:14px;
    font-weight:lighter;
    border:none;
   	background:transparent;
}
#content_area .teetime .price_label {
    margin-right:2px;
    padding-right:0px;
    line-height:19px;
}
#cboxLoadedContent .teetime .info .ui-icon {
    float:left;
    margin-right:5px;
}
#content_area .teetime .ui-icon {
    float:left;
    margin-right:5px;
}
#cboxLoadedContent .teetime .reservation_info {
    float:left;
    width:70%;
}
#cboxLoadedContent .teetime .price_box {
    float:right;
    text-align: right;
    width:27%;
    font-size:2.5em;
    line-height: 50px;
}
#content_area .teetime .price_details {
    padding-left: 90px;
}
#course_name {
    font-size:2em;
    text-align:center;
}
#teesheet_name {
	text-align: center;
}
.column {
	width:186px;
	margin:0 10px 0 0;
}
.booking_rules {
	width: 176px;
	padding:5px;
	font-size:11px;
}
.booking_rules p {
	margin-bottom:3px;
}
.wrapper {
	margin:20px auto;
	width:1100px;
}
.menu_item_home {
	height:62px;
	padding:10px 10px 0 0;
}

.dailyweather {
	width: 41px;
	float: left;
	text-align:center;
	margin-right:1px;
}
#weather {
padding: 5px;
}
div.ui-datepicker {
	width:14.4em;
	margin-bottom:10px;
}
div#booking_rules_content, div#available_deals_content {
	padding:5px;
}
.curtemp {
	font-size: 25px;
	float: right;
}
.wcity {
	font-size: 10px;
	margin-left: 20px;
	float: right;
	margin-top: -4px;
}
#hole_button_set {
	width:100px;
}
#player_button_set {
	width:140px;
}
.teetime .time {
	float:left;
	color:#303030;
}
.teetime a {
	color:#303030;
	float:right;
}
.teetime .bottom span {
	color:#303030;
}
.flag_icon_sm, .person_icon_sm, .cart_icon_sm, .no_cart_icon_sm {
	width:20px;
	height:16px;
	display:block;
	background:transparent url(<?php echo base_url(); ?>images/icons/icons.png) no-repeat -173px -10px;
	float:left;
}
.flag_icon_sm {
	width:16px;
}
.person_icon_sm {
	background-position:-173px -50px
}
.cart_icon_sm {
	background-position:-173px -92px;
}
.no_cart_icon_sm {
	background-position:-172px -131px;
}
.ui-state-default a, .ui-state-default a:link {
	background: #349ac5; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3'); /* for IE */
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top,  #349AC5,  #4173B3); /* for firefox 3.6+ */
	color: white;
	display: block;
	font-size: 14px;
	font-weight: lighter;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
	padding:5px 20px;
}

#booking_rules {
	background:url(<?php echo base_url(); ?>images/backgrounds/bg3.png) repeat;
}
#booking_rules div.ui-datepicker-header {
	background:none;
	border:none;
	font-weight:lighter;
	font-size:14px;
	padding:6px 0px 8px;
}
#booking_rules_content {
	background:white;
	box-shadow:0px 1px 10px 0px #777, 0px 6px 0px -3px white, 0px 7px 10px -3px #777, 0px 11px 0px -6px white;
	margin-bottom:12px;
	padding:10px;
}

.account_header {
    background: url("<?php echo base_url() ?>/images/backgrounds/top_bar_bg_black.png") repeat-x scroll 0 0 transparent;
    height: 37px;
    line-height: 37px;
    margin: 0px;
    padding: 0px;
    border-bottom: 1px solid #999999;
    border-left: 1px solid #999999;
    width: 949px;
    display: block;
    font-weight: normal;
    padding-left: 10px;
    color: white;
}

#overview_page {
	clear:none;
	display: block;
	width:960px;
	margin: 0px auto;
	background-color: none;
	overflow: hidden;
}

.content {
    background: none repeat scroll 0 0 #EFEFEF;
    overflow: hidden;
    min-height: 300px;
    border: 1px solid #CCCCCC;
    padding: 10px;
    width: auto;
}

label {
	width: 125px !important;
}

.form_col{
	float: left;
	width: 400px;
	display: block;
	overflow: hidden;
	margin-right: 20px;
}

.login_info {
	display: block;
	float: left;
	width: 935px;
}

.login_info label {
	width: 200px !important;
}

.login_info h4 {
	margin-top: 15px;
	border-bottom: 1px solid white;
	margin-bottom: 10px;
	padding: 0px;
}

.login_info h4 span {
	padding: 5px 0px 5px 2px;
	border-bottom: 1px solid #D0D0D0;
	display: block;
	width: auto;
}

h3.ui-accordion-header {
	background-color: #E0E0E0 !important;
	background-image: none !important;
	border: 1px solid #CCC;
	height: 35px;
	line-height: 35px;
	padding-left: 30px;
	color: #444;
}

h3.ui-state-active {
	color: #000;
}

.ui-accordion-content {
	background-color: white !important;
	background-image: none !important;
}

#accordion {
	padding: 15px;
	min-height: 400px;
}

#edit_account {
	margin-bottom: 20px;
}

/* Teetime list */
#accordion ul {
	margin: 0px 0px 15px 0px;
	padding: 0px;
	display: block;
	overflow: hidden;
}

#accordion ul li {
	display: block;
	width: auto;
	margin: 0px;
	padding: 10px 5px;
	list-style-type: none;
	border-bottom: 1px solid #E5E5E5;
	position: relative;
	overflow: hidden;
}

#accordion ul li > span {
	display: block;
}

#teetimes li h3 > span {
	float: right;
	font-weight: normal;
	margin-right: 250px;
}

#teetimes li a.cancel {
	position: absolute;
	right: 10px;
	display: block;
	top: 10px;
	background: #d14d4d; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d', endColorstr='#c03939'); /* for IE */
	background: -webkit-linear-gradient(top, #d14d4d, #c03939);
	background: -moz-linear-gradient(top,  #d14d4d,  #c03939); /* for firefox 3.6+ */
    border: 1px solid #232323;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px 0 rgba(255, 255, 255, 0.5) inset, 0 3px 1px -2px rgba(255, 255, 255, 0.2);
    color: white;
    display: block;
    font-size: 14px;
    font-weight: normal;
    height: 25px;
    line-height: 25px;
    padding0px;
    text-align: center;
    text-shadow: 0 -1px 0 black;
    width: 100px;
}

#accordion h2 {
	display: block;
	color: #AAA;
}

ul li h3 > span.amount {
	font-weight: normal;
	margin-right: 15px;
	background-color: #E0E0E0;
	border: 1px solid #CCC;
	display: block;
	padding: 3px 6px;
	float: right;
}

table th {
    border: 1px solid #BBBBBB;
    background-color: #DDD;
    color: #333333;
    font-size: 12px;
    height: 25px;
    line-height: 25px;
    padding: 2px 6px 0;
    text-align: left;
}

table {
	font-size: 14px;
}

table td {
	padding: 4px;
}

h3.muted {
	font-weight: normal;
	color: #CCC;
	font-size: 24px;
	padding-right: 0px;
	margin-right: 0px;
}
</style>
<div id="feedback_bar"></div>
	<div id="overview_page">
	    <h4 class="account_header">Personal Information</h4>
		<div class="content">
			<pre><?php //print_r($account_info); ?></pre>
			<form name="edit_account" id="edit_account" method="post" action="<?php echo site_url('member/save'); ?>">
				<fieldset>
					<div class="form_col">
						<div class="field_row">
							<label>First Name</label>
							<div class="form_field">
								<?php echo form_input('first_name', $account_info['first_name']); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Last Name</label>
							<div class="form_field">
								<?php echo form_input('last_name', $account_info['last_name']); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Phone</label>
							<div class="form_field">
								<?php echo form_input('phone_number', $account_info['phone_number']); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Cell Phone</label>
							<div class="form_field">
								<?php echo form_input('cell_phone_number', $account_info['cell_phone_number']); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Email</label>
							<div class="form_field">
								<?php echo form_input('email', $account_info['email'], "style='width: 220px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Birthday</label>
							<div class="form_field">
								<?php if($account_info['birthday'] == '0000-00-00') { $account_info['birthday'] = ''; } ?>
								<?php echo form_input('birthday_visible', $account_info['birthday'], "style='width: 80px;' id='birthday_visible'"); ?>
								<input type="hidden" name="birthday" id="birthday" value="<?php echo $account_info['birthday']; ?>" />
							</div>
						</div>
					</div>
					<div class="form_col">
						<div class="field_row">
							<label style="margin-bottom: 25px;">Address</label>
							<div class="form_field">
								<?php echo form_input('address_1', $account_info['address_1'], "style='width: 250px;'"); ?>
								<?php echo form_input('address_2', $account_info['address_2'], "style='width: 250px; margin-top: 5px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>City</label>
							<div class="form_field">
								<?php echo form_input('city', $account_info['city'], "style='width: 200px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>State</label>
							<div class="form_field">
								<?php echo form_input('state', $account_info['state'], "style='width: 200px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Zip</label>
							<div class="form_field">
								<?php echo form_input('zip', $account_info['zip'], "style='width: 100px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Country</label>
							<div class="form_field">
								<?php echo form_input('country', $account_info['country'], "style='width: 200px;'"); ?>
							</div>
						</div>
					</div>
					<div class="login_info">
						<h4><span>Login Info</span></h4>
						<div class="field_row">
							<label>Username</label>
							<div class="form_field">
								<?php echo form_input('username', $account_info['username'], "style='width: 150px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>New Password</label>
							<div class="form_field">
								<?php echo form_password('password', '', "style='width: 150px;'"); ?>
							</div>
						</div>
						<div class="field_row">
							<label>Confirm New Password</label>
							<div class="form_field">
								<?php echo form_password('password_confirm', '', "style='width: 150px;'"); ?>
							</div>
						</div>
					</div>
					<input name="submit" style="float: left; margin: 20px 5px 0px 0px;" value="Save Changes" type="submit" class="submit_button" />
				</fieldset>
			</form>
		</div>
		<h4 class="account_header"><?php echo $this->session->userdata('course_name'); ?></h4>
		<div class="content" id="accordion">
			<h3>Upcoming Reservations</h3>
			<div>
				<ul id="teetimes">
				<?php if(!empty($teetimes)){ ?>
				<?php foreach($teetimes as $teetime){ ?>
					<li>
						<a href="<?php echo site_url('be/cancel_reservation/'.$teetime['TTID'].'/'.$teetime['person_id']); ?>" class="cancel delete_inactive">Cancel</a>
						<h3>
							<?php echo date('D, F jS', $teetime['start_timestamp']); ?> @
							<?php echo date('g:i a', $teetime['start_timestamp']); ?>
							 - <?php echo $teetime['holes']; ?> holes
							<span><?php echo $teetime['player_count']; ?> Player<?php if($teetime['player_count'] > 1){ ?>s<?php } ?>,
								<?php if($teetime['carts'] != 0){ echo $teetime['carts']; } else { echo 'No'; } ?>
								Carts
							</span>
						</h3>
						<span style="color: #999;"><?php echo $teetime['details']; ?></span>
					</li>
				<?php } } else { ?>
					<li><h3 class="muted">No reservations found</h3></li>
				<?php } ?>
				</ul>
			</div>
			<h3 data-url="<?php echo site_url('member/get_balances'); ?>">Account Balances &amp; Giftcards</h3>
			<div></div>
			<h3 data-url="<?php echo site_url('member/get_purchases'); ?>">Purchase History</h3>
			<div></div>
		</div>
</div>
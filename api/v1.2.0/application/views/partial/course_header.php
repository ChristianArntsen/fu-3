<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <base href="<?php echo base_url();?>" />
	<title><?php echo $this->session->userdata('course_name').' -- '.lang('common_powered_by').' ForeUP' ?></title>
	<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/menubar.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/general.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/popupbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/register.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/receipt.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/reports.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/tables.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/thickbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/colorbox.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/datepicker.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/editsale.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.loadmask.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/footer.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/contactable.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/css3.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/ui-lightness/jquery-ui-1.8.14.custom.css?<?php echo APPLICATION_VERSION; ?>" />
	<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/phppos_print.css?<?php echo APPLICATION_VERSION; ?>"  media="print"/>

	<script type="text/javascript">
	var SITE_URL= "<?php echo site_url(); ?>";
	</script>
	<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.color.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.form.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.tablesorter.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.validate.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<!--script src="<?php echo base_url();?>js/jquery.maskedinput.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script-->
	<script src="<?php echo base_url();?>js/jquery.contactable.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.customSelect.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/thickbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.colorbox.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/common.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/manage_tables.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.loadmask.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/date.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/datepicker.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery.syncscroll.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/placeholder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script type="text/javascript">
	Date.format = '<?php echo get_js_date_format(); ?>';

	$(function(){
		$('div.dropdown-menu > a').click(function(event, ui){
			$(this).siblings('ul').show();
			return false;
		});

		$('html').click(function(event, ui){
			if($(event.target).parents().index($('div.dropdown-menu > ul')) == -1){
				$('div.dropdown-menu > ul').hide();
			}
		});
	});
	</script>
<style type="text/css">
html {
    overflow: auto;
}
.menu_item img {
	height:35px;
	margin:5px 0px 0px 10px;
}
td.menu_item_home {
	padding:0px;
	height:35px;
	width:auto;
}
td.menu_item_home a {
	width:auto;
}
td.menu_item {
	text-align:left;
}
div#menubar_background, div#menubar_full {
	background-position:0px -15px;
}
#menubar_background, #menubar_full{
	background:none;
	width: auto;
	display: block;
	box-shadow: 0px 1px 0px 0px #B9B4AB, 0px 2px 0px 0px #ECE9E4;
	padding: 3px 3px 0px 3px;
}
#course_logo {
    float:left;
    margin-right: 20px;
 }
#course_name {
	font-size:1.5em;
	float:left;
	color:#5A5A5A;
	line-height: 44px;
	margin-right: 25px;
}
#teesheet_name {
	float:left;
	line-height: 3em;
	margin-right: 15px;
}
#teesheetMenu {
	margin-top:15px;
}
#menubar_container {
	width:100%;
}
span.customStyleSelectBox {
	color: #5A5A5A;
	padding: 0px;
	text-align: left;
	font-size: 20px;
	font-weight: lighter;
}
span.customStyleSelectBoxInner {
    background: url("<?php echo base_url(); ?>images/canvas-list-nav-item-arrow-white.png") no-repeat scroll right center transparent;
}
.congrats_box {
	background:transparent;
	font-size:18px;
	text-align:center;
	font-weight:bold;
	color:#333;
}
.congratulations {
	font-size:34px;
	margin-top:40px;
}
#customer_menu {
	float:right;
	font-size:16px;
	background:none;
	margin-left:15px;
	margin-right:30px;
	margin-top:14px;
	color:#5a5a5a;
	display: block;
}

div.dropdown-menu {
	display: block;
	position: relative;
}

ul.menu {
	display: none;
	position: absolute;
	margin-bottom: -20px;
	right: 0px;
	background-color: white;
	width: 150px;
	margin: 0px;
	padding: 5px 0px;
	box-shadow: 0px 0px 2px #AAA;
}

ul.menu > li {
	display: block;
	margin: 0px;
	padding: 5px 10px;
	list-style-type: none;
}

ul.menu a {
	text-decoration: none;
	font-size: 14px;
	color: #333;
}

ul.menu a:hover {
	color: #2B5F87;
}

#account_menu {
	float: right;
}

#account_menu > a {
	font-size: 20px;
}

#navigation_menu {
	float: left;
	margin: 6px 15px 10px 10px;
}

div.dropdown-menu > a {
	font-size: 20px;
	color: #333;
	position: relative;
	padding: 5px;
	display: block;
}

#signin {
	float: right;
	margin-right: 15px;
	font-size: 20px;
	color: #333;
	margin-top: 10px;
}
</style>

</head>
<body>
<div id="menubar_background">
<div id="menubar_full">
	<table id="menubar_container">

		<tr id="menubar_navigation">
			<td class="menu_item menu_item_home">
				<span id="course_logo">
					<a href="http://foreup.com"><?php echo img(
					array(
						'src' => base_url().'images/login/login_logo_sm.png'
					)); ?></a>
				</span>
				<div class="dropdown-menu" id="navigation_menu">
					<a href="#">
						<?php if($this->uri->segment(2) == 'account_overview'){ ?>
						Account Overview
						<?php }else{ ?>
						Reservations
						<?php } ?>
						&#x25BE;
					</a>
					<ul class="menu" id="account_menu">
						<li><a href="<?php echo site_url('be/reservations/'.$this->session->userdata('course_id')); ?>">Reservations</a></li>
						<!-- <li><a href="">Newsletter</a></li>
						<li><a href="">Member Directory</a></li>
						<li><a href="">Events</a></li> -->
					</ul>
				</div>
            	<span id="course_name"> <span style="font-size: 18px; line-height: 24px; display: inline-block;">at</span> <?php echo $this->session->userdata('course_name')?></span>
            	<span id="teesheet_name">
             	<?php if ($teesheet_menu != '' && false) {?>
		            <form action="" method="post">
		                    - <?php echo $teesheet_menu; ?>
		                    <input type="submit" value="changets" name="changets" id="changets" style="display:none"/>
		            </form>
		            <script>
		            function changeTeeSheet() {
					    var teesheet_id = $('#teesheetMenu').val();
					    $('#changets').click();
					}
					</script>
		        <?php }
		        else {
		        	echo "<span class='customStyleSelectBox'>$teesheet_info->title</span>";
            	 } ?>
            	</span>
            	<?php if ($this->session->userdata('customer_id')) {?>
				<div class="dropdown-menu" id="account_menu">
					<a href="#">
						<?php echo $this->session->userdata('first_name'); ?>
						<?php echo $this->session->userdata('last_name'); ?>
						&#x25BE;
					</a>
					<ul class="menu" id="account_menu">
						<li><a href="<?php echo site_url('be/account_overview'); ?>">Account Overview</a></li>
						<li><a href="<?php echo site_url('be/logout'); ?>"><?php echo lang("common_logout"); ?></a></li>
					</ul>
				</div>
				<?php } else { ?>
				<a id="signin" href="<?php echo site_url('customer_login/login'); ?>">Sign In</a>
				<?php } ?>
            </td>
		</tr>
	</table>
</div>
</div>
<?php if ($booked) {?>
	<div class='congrats_box congratulations'>Congratulations,</div>
	<div class='congrats_box'>your Reservation has been booked!</div>
<?php } ?>
<div id="content_area_wrapper">
<div id="content_area">
<script>
	$(document).ready(function(){
		$('#teesheetMenu').customStyle({height:24});
	})
</script>
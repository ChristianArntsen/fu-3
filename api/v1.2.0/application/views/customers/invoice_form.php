<ul id="error_message_box"></ul>
<?php
if ($cart_line)
	echo form_open('sales/save_invoice_details/'.$cart_line,array('id'=>'invoice_form'));
?>
<fieldset id="invoice_payment_info">
<legend><?php echo lang("invoices_basic_information"); ?></legend>

<table id='invoice_details_table'>
	<thead>
		<tr>
			<th>Product</th>
			<th>QTY</th>
			<th style='text-align:center;'>Price</th>
			<th style='text-align:center;'>Tax</th>
			<th style='text-align:center;'>Total</th>
			<th style='text-align:center;'>Paid</th>
			<th style='text-align:center;'>Remaining Balance</th>
			<th>Payment Amount</th>			
		</tr>
	</thead>
	<tbody>
		<?php $total_due = 0; ?>
		<?php foreach ($invoice_info['invoice_items'] as $key => $item){ ?>
			<tr>
				<td><?php echo $item['description']; ?></td>
				<td><?php echo $item['quantity']; ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency($item['amount']); ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo $item['tax']; ?>%</td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity']); ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency($item['paid_amount']); ?></td>
				<td style='text-align:right; padding-right:10px;'><div class='remaining_balance'><?php echo to_currency(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']); ?></div></td>
				<td>
					<?php $payment = isset($item['payment_amount']) ? round($item['payment_amount'], 2)  : round(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount'],2) ?>
					<div>
						<?php echo form_input(
							array(
								'name'=>'invoice_item[]',
								'size'=>'8',
								'id'=>'value',
								'class'=>'payment_amount',
								'value'=> to_currency_no_money($payment),
								'style'=>'text-align:right;  padding-right:10px;'
							)
						); ?>
					</div>
				</td>
			</tr>
			<?php $total_payments += isset($item['payment_amount']) ? $item['payment_amount']  : ($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']; ?>						
			<?php $total_due += ($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']; ?>
		<?php } ?>	
		<?php 
		if(count($invoice_info['overdue_items']) > 0) {
		?>
			<tr>
				<td colspan=8 style='font-weight:bold; padding:20px 0px 10px;'>****************************************************************** OVERDUE ITEMS ******************************************************************</td>
			</tr>
		<?php
			foreach ($invoice_info['overdue_items'] as $key => $item){ ?>
			<tr>
				<td>(<?php echo date('Y-m-d',strtotime($item['month_billed'])); ?>) <?php echo $item['description']; ?></td>
				<td><?php echo $item['quantity']; ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency($item['amount']); ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo $item['tax']; ?>%</td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity']); ?></td>
				<td style='text-align:right; padding-right:10px;'><?php echo to_currency($item['paid_amount']); ?></td>
				<td style='text-align:right; padding-right:10px;'><div class='remaining_balance'><?php echo to_currency(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']); ?></div></td>
				<td>
					<?php $payment = isset($item['payment_amount']) ? round($item['payment_amount'], 2)  : round(($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount'],2) ?>
					<div>
						<?php echo form_input(
							array(
								'name'=>'overdue_item[]',
								'size'=>'8',
								'id'=>'value',
								'class'=>'payment_amount',
								'value'=> to_currency_no_money($payment),
								'style'=>'text-align:right;  padding-right:10px;'
							)
						); ?>
					</div>
				</td>
			</tr>
			<?php $total_payments += isset($item['payment_amount']) ? $item['payment_amount']  : ($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']; ?>						
			<?php $total_due += ($item['amount'] + ($item['amount'] * ($item['tax']/100)))*$item['quantity'] - $item['paid_amount']; ?>
		<?php 
			}
		}
		?>	
		<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><span>Total Due:</span></td>
				<td><div class='total' id='something' style='text-align:right; padding-right:10px;'><?php echo to_currency($total_due); ?></div></td>
				<td><div class='total' id='invoice_payment_total'><?php echo to_currency($total_payments); ?></div></td>
		</tr>
		<hr />	
	</tbody>	
</table>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#submit','#invoice_payment_info').click(function(e){
		e.preventDefault();
		update_total_payments();		
		$('#invoice_payment_info').submit();
	});
	
    $('.payment_amount','#invoice_details_table').blur(function()
    {    	    	
        update_total_payments();
    });    
    
    function update_total_payments()
    {    	
    	var total = 0, payment = 0, remaining_balance = 0, row = '';
    	$('.payment_amount', '#invoice_details_table').each(function(){    
    		row = $(this).parent().parent().parent();    		
    		remaining_balance = $('.remaining_balance', row).html();
    		remaining_balance = parseFloat(remaining_balance.replace(/[^0-9.-]+/g, '')).toFixed(2);
    		
    		payment = $(this).val();
    		payment = payment.replace(/\,/g,'');  //remove commas if used when number is greater than 999  		
    		payment = !isNaN(payment) ? payment : 0; //if no value was enetered set the payment to 0
    		payment = parseFloat(payment.replace(/[^0-9.-]+/g, '')).toFixed(2);    		
    		
    		//check for invalid payment amount
    		payment = payment < 0 ? 0 : payment;    		
    		payment = parseFloat(payment) > parseFloat(remaining_balance) ? remaining_balance : payment;
    		
    		//reset the payment value now that it is validated
    		$(this).val(parseFloat(payment).toFixed(2));    		
    		
    		total += parseFloat(payment);
    	});
    	
    	//set the total payment amount
    	$('#invoice_payment_total','#invoice_details_table').html("$" + total.toFixed(2) + "");    	
    }
	
	var submitting = false;
    $('#invoice_form').validate({
		submitHandler:function(form)
		{									
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{		
					var price = $('#invoice_payment_total').html();
					var context = '';	
					price = parseFloat(price.replace(/[^0-9.-]+/g, '')).toFixed(2);	
					context = '#reg_item_price_' + response.cart_line;
					$('div', context).html("$" + price + "");						
					
					context = '#reg_item_total_' + response.cart_line;
					$('div', context).html("$" + price + "");
					
					$.colorbox.close();
	                submitting = false;	
	                sales.update_basket_totals(null, response.basket_info);		                           
				},
				dataType:'json'
			});	
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			giftcard_number:
			{
				required:true
			},
			value:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			giftcard_number:
			{
				required:"<?php echo lang('giftcards_number_required'); ?>",
				number:"<?php echo lang('giftcards_number'); ?>"
			},
			value:
			{
				required:"<?php echo lang('giftcards_value_required'); ?>",
				number:"<?php echo lang('giftcards_value'); ?>"
			}
		}
	});
});
</script>

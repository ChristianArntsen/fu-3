<?php
echo form_open('customers/save_billing/'.$billilng_info->billing_id,array('id'=>'invoice_form'));
$billing_base_url = site_url('customers/save_billing');
// echo "<pre>";
// print_r($billings->result_array());
// echo "</pre>";
?>
	<input type='hidden' id='billing_base_url' name='billing_base_url' value='<?=$billing_base_url;?>'/>
	<table id='member_billing_table'>
		<tbody>
			<tr>
				<td id='invoice_box_holder'>
					<div class='invoice_box_title'>
						Recurring Billing <span id='new_recurring'>+</span>
					</div>
					<div class='recurring_billing_box_holder'>
					<?php if ($billings->num_rows()>0) {
							foreach ($billings->result_array() as $billing)
							{
								// echo "<div class='recurring_billing_box' id='{$billing['billing_id']}'>\${$billing['total']} - {$billing['frequency']} - Pay m {$billing['pay_member_balance']}<div class='delete_billing'><span>-</span></div></div>";
								echo "<div class='recurring_billing_box' id='{$billing['billing_id']}'>\${$billing['total']} - {$billing['frequency']} - {$billing['title']}<div class='delete_billing'><span>X</span></div></div>";
							}
						} else { ?>
						<div class='recurring_billing_box no_results'>
							No Recurring Billings
						</div>
					<?php } ?>
					</div>
					<div class='invoice_box_title'>
						Current Invoices <span id='new_invoice'>+</span>
					</div>
					<div class='invoice_box_holder'>
					<?php if ($invoices->num_rows()>0) {
							foreach ($invoices->result_array() as $invoice)
							{
								$display_date = $invoice['month_billed'] != '0000-00-00' ? $invoice['month_billed'] : $invoice['date'];
								$from_recurring_billing = $invoice['billing_id'] != '0' ? ' Auto Generated' : '';
								$remaining_balance = ($invoice['total'] - $invoice['paid']);
								$paid_in_full = $remaining_balance > 0 ? false : true;
								$balance_text = $paid_in_full ? '<span>Paid In Full</span>' : "<span>Due: <span style='color:red;'>".to_currency($remaining_balance)."</span> <a data-id='{$invoice['invoice_number']}' class='pay_invoice' href='#'>PAY</a></span>";
								$email_text = $person_info->email != '' ? " | <span><a data-id='{$invoice['invoice_id']}' class='email_invoice' href='#'>EMAIL</a></span>" : '';
								// PDF and PAY at the bottom
								// echo "<div class='invoice_box' id='{$invoice['invoice_id']}'><div class='top_invoice_row'>Invoice: #{$invoice['invoice_number']}{$from_recurring_billing} <span class='invoice_date'>".date('m/d/y', strtotime($display_date))."</span></div><div>Total: \${$invoice['total']} <a href='index.php/customers/load_invoice/{$invoice['invoice_id']}/0/1' target='_blank'>PDF</a> | <a data-id='{$invoice['invoice_number']}' class='pay_invoice' href='#'>PAY</a><div class='delete_invoice'><span>X</span></div></div></div>";

								// PDF and PAY on top line
								// echo "<div class='invoice_box' id='{$invoice['invoice_id']}'><div class='top_invoice_row'>Invoice: #{$invoice['invoice_number']}{$from_recurring_billing}<a href='index.php/customers/load_invoice/{$invoice['invoice_id']}/0/1' target='_blank'> PDF</a> | <a data-id='{$invoice['invoice_number']}' class='pay_invoice' href='#'>PAY</a> <span class='invoice_date'>".date('m/d/y', strtotime($display_date))."</span></div><div>Total: \${$invoice['total']} {$balance_text} <div class='delete_invoice'><span>X</span></div></div></div>";

								//PDF and PAY Split Levels
								echo "<div class='invoice_box' id='{$invoice['invoice_id']}'><div class='top_invoice_row'>Invoice: #{$invoice['invoice_number']}{$from_recurring_billing}<a href='index.php/customers/load_invoice/{$invoice['invoice_id']}/0/1' target='_blank'> PDF</a> {$email_text} <span class='invoice_date'>".date('m/d/y', strtotime($display_date))."</span></div><div>Total: \${$invoice['total']} {$balance_text} <div class='delete_invoice'><span>X</span></div></div></div>";
							}
						} else { ?>
							<div class='invoice_box no_results'>
								No Invoices
							</div>
					<?php } ?>
					</div>
				</td>
				<td style='width:750px;' id='invoice_creation_box'>
					<ul id="error_message_box"></ul>
					<div id='invoice_creation_options'>
						<?php if ($person_info->person_id) { ?>
							<div id='create_new_recurring_billing'>Start Recurring Billing</div>
							<div id='create_new_invoice'>Create New Invoice</div>
							<div class='disabled' id='create_multiple_invoices'>Create Multiple Invoices</div>
						<?php } else { ?>
							<div class='disabled' id='create_new_recurring_billing'>Start Recurring Billing</div>
							<div class='disabled' id='create_new_invoice'>Create New Invoice</div>
							<div id='create_multiple_invoices'>Create Multiple Invoices <div id='expand_indicator'>></div></div>
							<div id='multiple_invoice_options' class='hidden'>
								<div id='options_wrapper'>
									<div id='billing_types'>
										<div id='negative_member_balance' class="float_left field_row clearfix invoice_type">
											<div class="form_field">
												<input type="checkbox" name="billings[]" id="negative_member_balance_checkbox"/>
												<label for='negative_member_balance_checkbox'>Negative <?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'))); ?></label>
											</div>
										</div>
										<div id='negative_credit_balance' class="float_left field_row clearfix invoice_type">
											<div class="form_field">
												<input type="checkbox" name="billings[]" id="negative_credit_balance_checkbox"/>
												<label for='negative_credit_balance_checkbox'>Negative <?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'))); ?></label>
											</div>
										</div>
										<div id='recurring_billings' class="float_left field_row clearfix invoice_type">
											<div class="form_field">
												<input type="checkbox" name="billings[]" id="recurring_billings_checkbox"/>
												<label for='recurring_billings_checkbox'>Recurring Billings</label>
											</div>
										</div>
										<div id='recurring_billings_dates' class='hidden'>
											<!-- <label for="start_date">Start:</label> -->
											<input placeholder="Start Date" class="date" type="text" name="start_date" value="" id="billing_start_date"/>
											<!-- <label for="end_date">End:</label> -->
											<input placeholder="End Date" class="date" type="text" name="end_date" value="" id="billing_end_date" />
										</div>
									</div>
									<a id="billings_link" href='/index.php/customers/load_invoice/0/0/0/0' target='_new'><div id="generate_billings">Generate Billings</div> </a>
								</div>
							</div>
							<!-- <div id="pdf_download_message" class="hidden">Your Invoices Will be Downloaded Shortly</div> -->
							<!-- <div id="dialog-confirm" title="Empty the recycle bin?">
						 		<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
							</div> -->
						<?php } ?>
					</div>
					<fieldset id="billing_info" style='display:none'>
						<!--legend>Billing Information - <a href='#' id='settings_link'><?php echo lang("customers_settings"); ?></a></legend-->
						<div id='invoice_time_options'>
							<div id='time_options_box'>
								<div id='first_row_options' class='time_options_row'>
									<div id='time_components'>
										<div>
											Title:
											<input id="billing_title" name="billing_title" value='<?=$billing['title'];?>' placeholder="Billing Title" />
										</div>
										<div>
											Frequency:
											<input type='hidden' id='frequency' name='frequency' value='one_time'/>
											<select id='frequency_selector'>
												<option value='monthly'>Monthly</option>
												<option value='yearly'>Yearly</option>
											</select>
										</div>
										<div>
											Bill on:
											<select id="bill_month" name="bill_month" class="valid" style='display:none'><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select>
											<select id="bill_day" name="bill_day"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select>
										</div>
										<div id='advanced_options'>advanced</div>
									</div>
									<div id='save_invoice'>Save</div>
									<!-- <div id='advanced_options'>advanced</div> -->
								</div>
								<div id='second_row_options' class='time_options_row'>
									<div>
										Gen. Inv. Days Before:
										<input id="generate_days_before" name="generate_days_before" type="" size=2 class="" value="<?=$billing['generate_days_before']?>">
									</div>
									<div>
										<input type='hidden' id='customer_id' name='customer_id' value='<?=$person_info->person_id;?>'/>
										Delayed Start Date:
										<input id="start_date" name="start_date" type="" placeholder="Start Date" class="">
									</div>
									<div id='month_range'>
										between:
										<select id="start_month" name="start_month" class="valid"><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select>
										<select id="end_month" name="end_month" class="valid"><option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12" selected>Dec</option></select>
									</div>
								</div>
							</div>
						</div>
						<?php $this->load->view('customers/invoice', array('credit_cards'=>$credit_cards, 'popup'=>true));?>
					</fieldset>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script>
window.billing_config =
{
	customer_credit : '',
	member_balance : '',
	cp : '',
	bc : '',
	member_balance_nickname : '',
	customer_credit_nickname : '',
	base_url : ''
};

window.billing_config.customer_credit = '<?php echo ($person_info->account_balance); ?>';
window.billing_config.member_balance = '<?php echo ($person_info->member_account_balance); ?>';
window.billing_config.member_balance_nickname = '<?php echo (($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname')));  ?>';
window.billing_config.customer_credit_nickname = '<?php echo (($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname')));  ?>';
window.billing_config.base_url = "/index.php/customers/load_invoice/0/0/0/0/";

$(document).ready(function()
{
	$('.pay_invoice').click(function(e){
		e.preventDefault();
		var invoice_id, redirect;

		invoice_id = "inv " + $(this).attr('data-id');

		redirect = function(){
			window.location = '<?php echo site_url('sales'); ?>';
		};

		sales.add_item(invoice_id, redirect);
	});

	$('.email_invoice').click(function(e){
		e.preventDefault();
		var invoice_id = $(this).attr('data-id');
		if(confirm('Email a copy of this Invoice?')){
			sales.email_invoice(invoice_id);
		}

	})

	customer.billing.initialize_controls();
	customer.billing.initialize_customer_search();
	$('.date').datepicker({
		'dateFormat': 'yy-mm-dd'
	});

	$('.invoice_box').click(function(e){
		var invoice_id, person_id;

		if ($(this).hasClass('no_results')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		person_id = $('#customer_id').val();
		invoice_id = $(this).attr('id');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$(this).addClass('loaded_invoice');
		customer.invoice.load(invoice_id, person_id);
	});

	$('.recurring_billing_box').click(function(e){
		var billing_id, person_id;

		if ($(this).hasClass('no_results')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		person_id = $('#customer_id').val();
		billing_id = $(this).attr('id');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$(this).addClass('loaded_invoice');
		customer.billing.load(billing_id, person_id);
	});

	$('#create_new_recurring_billing, #new_recurring').click(function(){
		var person_id = $('#customer_id').val();
		if ($('#customer_name').val() === ''){
			alert('Please select a customer before starting a recurring billing');
			return;
		}
		if ($(this).hasClass('disabled')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		$('.loaded_invoice').removeClass('loaded_invoice');
		$('.no_results', '.recurring_billing_box_holder').remove();
		$('#billing_title').val('');
		$('.recurring_billing_box_holder').append('<div id="temp_billing" class="recurring_billing_box loaded_invoice"> New Recurring Billing</div>');
		customer.billing.create('recurring_billing', person_id);
	});

	$('#create_new_invoice, #new_invoice').click(function(){
		var person_id = $('#customer_id').val();
		if ($('#customer_name').val() === ''){
			alert('Please select a customer before starting an invoice');
			return;
		}
		if ($(this).hasClass('disabled')) return;
		$('#temp_billing').remove();
		$('.advanced').removeClass('advanced');
		$('.no_results','.invoice_box_holder').remove();
		$('.loaded_invoice').removeClass('loaded_invoice');
		$('.invoice_box_holder').append('<div id="temp_billing" class="invoice_box loaded_invoice"><div class="top_invoice_row"> New Invoice</div><div></div></div>');
		customer.billing.create('invoice', person_id);
	});

	$('.delete_billing').click(function(){
		var billing_id = $(this).parent().attr('id');
		if(confirm('Delete this recurring billing?')){
			$(this).parent().remove();
			customer.billing.remove('recurring_billing',billing_id);
		}
	});
	$('.delete_invoice').click(function(){
		var invoice_id = $(this).parent().parent().attr('id');
		console.log(invoice_id);
		if(confirm('Delete this invoice?')){
			$(this).parent().parent().remove();
			customer.invoice.remove('invoice',invoice_id);
		}
	});

	$('#create_multiple_invoices').click(function(){
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
	});

	$('li','#multiple_invoice_options').click(function(){
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
	});

	//make the whole button clickable
	// $('.invoice_type', '#multiple_invoice_options').click(function(){
		// var input = $('input',this);
		// $(input).prop('checked', !$(input).prop('checked'));
	// });

	//Generating bulk invoices
	$(':input','#multiple_invoice_options').change(function(){
		update_billings_link();
	});

	//validate fields when generate pdfs is clicked
	$('#generate_billings').click(function(e){
		var start, end,
			num_reports_selected = 0,
			recurring_billings_checked = $('#recurring_billings_checkbox','#multiple_invoice_options').attr('checked');

		start = $('#billing_start_date');
		end = $('#billing_end_date');

		$(':checked','#multiple_invoice_options').each(function(){
			num_reports_selected += 1;
		});

		if (num_reports_selected < 1){
			e.preventDefault();
			set_feedback('Please select the billing you would like to generate','error_message',false);
			return;
		}

		if (recurring_billings_checked && start.val() === '')
		{
			e.preventDefault();
			set_feedback('A start date must be selected','error_message',false);
			return;
		}
		else if (recurring_billings_checked && end.val() === '')
		{
			e.preventDefault();
			set_feedback('An end date must be selected','error_message',false);
			return;
		}
		$('#multiple_invoice_options').toggleClass('hidden');
		$('#expand_indicator').toggleClass('right_ninety_degrees');
		$('#pdf_download_message').removeClass('hidden');

		//clear out the checkboxes
		$(':checked','#multiple_invoice_options').each(function(){
			$(this).attr('checked',false);
		});

		//hide the date options
		$('#recurring_billings_dates').addClass('hidden');
	});

	//hide or show date options
	 $('#recurring_billings_checkbox','#multiple_invoice_options').click(function(){
	 	$('#recurring_billings_dates').toggleClass('hidden');
	 });

	function update_billings_link()
	{
		var negative_member_balance = $("#negative_member_balance_checkbox").attr('checked') ? 1 : 0,
			negative_credit_balance = $("#negative_credit_balance_checkbox").attr('checked') ? 1 : 0,
			recurring_billings = $("#recurring_billings_checkbox").attr('checked') ? 1 : 0,
			start_on = $('#billing_start_date').val() === '' ? 0 : $('#billing_start_date').val(),
			end_on = $('#billing_end_date').val() === '' ? 0 : $('#billing_end_date').val();

			start_on = encodeURIComponent(start_on);
			end_on = encodeURIComponent(end_on);
			link = window.billing_config.base_url;
			link += negative_member_balance + '/' + negative_credit_balance + '/' + recurring_billings + '/' + start_on + '/' + end_on;

			$('#billings_link').attr('href',"" + link + "");
	}
});
</script>
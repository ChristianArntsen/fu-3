<div class='teetime_info_header'><?php echo (($teetime_info->person_name== '')?$teetime_info->title:$teetime_info->person_name).'&nbsp;&nbsp;&nbsp; @ '. date("g:i a", strtotime($teetime_info->start+1000000)); ?></div>
<div class='' style='font-size:10px; text-align:center;'><?php echo '(Booked on: '.date("F j, Y, g:i a", strtotime($teetime_info->date_booked)).')';?></div>
<?php if ($teetime_info->teed_off_time != '0000-00-00 00:00:00') { ?>
<div class='' style='font-size:10px; text-align:center;'><?php echo '(Teed Off: '.date("F j, Y, g:i a", strtotime($teetime_info->teed_off_time)).')';?></div>
<?php } ?>
<br/>
<div class='teetime_info_header'>Details:</div>
<div class='teetime_info_content'><?php echo $teetime_info->details; ?></div>
<br/>
<div class='teetime_info_header'>Players:</div>
<?php
//print_r($person_info);
foreach ($person_info as $player)
{
	echo "<div class='teetime_info_content'>{$player->last_name}, {$player->first_name} ".($player->birthday != '0000-00-00' ? " - Birthday: ".date('n/j/y', strtotime($player->birthday)) : '')."</div>";
}
?>
<br/>
<div class='teetime_info_header'>Receipts:</div>

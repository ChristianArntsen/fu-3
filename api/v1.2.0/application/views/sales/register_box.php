<table id="register">
	<!-- table header with labels -->
	<thead>
		<tr>
			<th class="reg_item_delete" id="reg_item_delete"><div class="header_cell header"><?php echo anchor(null, 'Cancel Sale', array('class'=>'delete_item', 'title'=>'Cancel Sale', 'onclick'=>'sales.cancel_sale()'));?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_select" id="reg_item_select" style="width:32px;"><div class="header_cell header"><?php echo form_checkbox('select_all', 'select_all', false, 'onClick="sales.select_all_toggle(this)"');?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_name" id="reg_item_name"><div class="header_cell header"><?php echo lang('sales_item_name'); ?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_price" id="reg_item_price"><div class="header_cell header"><?php echo lang('sales_price'); ?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_qty" id="reg_item_qty"><div class="header_cell header"><?php echo lang('sales_quantity'); ?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_discount" id="reg_item_discount"><div class="header_cell header"><?php echo lang('sales_discount'); ?><span class="sortArrow">&nbsp;</span></div></th>
			<th class="reg_item_total" id="reg_item_total"><div class="header_cell header"><?php echo lang('sales_total'); ?><span class="sortArrow">&nbsp;</span></div></th>
		</tr>
	</thead>
	<!-- Items in the cart -->
	<tbody id="cart_contents">
		<?php if(count($cart)==0) {?>
		<tr><td colspan='9' style="height:60px;border:none;">
		<div class='warning_message' style='padding:7px;'></div>
		</td></tr>
		<?php
		}
		else
		{
		    $alternating_class = 'even';
			foreach(array_reverse($cart, true) as $line=>$item)
			{
				$alternating_class = ($alternating_class == 'even')?'odd':'even';
				$error_class = ($giftcard_error_line &&  $giftcard_error_line == $line)?'giftcard_number_error':'';
				$cur_item_info = isset($item['item_id']) ? $this->Item->get_info($item['item_id']) : $this->Item_kit->get_info($item['item_kit_id']);
				echo form_open("sales/edit_item/$line");
			?>
				<tr class="reg_item_top <?php echo $alternating_class.' '.$error_class;?>" id="reg_item_top_<?php echo $line ?>">
                    <td class="reg_item_delete" id="reg_item_delete_<?php echo $line ?>"><?php echo anchor(null, 'Delete', array('class'=>'delete_item', 'title'=>'Remove from Cart', 'onclick'=>"sales.delete_item('".$line."')"));?></td>
                    <td class="reg_item_select" id="reg_item_select_<?php echo $line ?>"><?php echo form_checkbox('select_'.$line, 'select', (isset($basket[$line])) ? true : false, 'onClick="sales.select_cart_line('.$line.', this)"');?></td>
                    <?php
                        $is_teetime = $this->Item->is_teetime($item['item_id']);
                        $is_cart = $this->Item->is_cart($item['item_id']);
                        $price_category_index = ($item['price_category']) ? $item['price_category']:substr($item['item_number'], strpos($item['item_number'], '_')+1, strrpos($item['item_number'], '_')-strpos($item['item_number'], '_')-1);
                        $teetime_type_index = ($item['teetime_type']) ? $item['teetime_type']:substr($item['item_number'], strrpos($item['item_number'], '_')+1);
                    ?>
                    <td class="reg_item_name" id="reg_item_name_<?php echo $line ?>"><?php
                        if ($is_teetime) {
                            echo form_dropdown("item_type_".$line, ($this->permissions->course_has_module('reservations')?$this->Fee->get_teetime_types($teetime_type_index):$this->Green_fee->get_teetime_types($teetime_type_index)), $price_category_index.'_'.$teetime_type_index, 'onChange="sales.change_item('.$line.', this)"');
                        }
                        else if ($is_cart) {
                            echo form_dropdown("item_type_".$line, ($this->permissions->course_has_module('reservations')?$this->Fee->get_cart_types($teetime_type_index, $this->session->userdata('schedule_id')):$this->Green_fee->get_cart_types($teetime_type_index, $this->session->userdata('teesheet_id'))), $price_category_index.'_'.$teetime_type_index, 'onChange="sales.change_cart('.$line.', this)"');
                        }
						else if ($item['is_giftcard']) {
							 echo anchor("sales/view_giftcard/-1/$line", $item['name'],  array('class'=>'colbox','title'=>lang('giftcards_giftcard_details')));
                        	if ($item['giftcard_data']['giftcard_number'] == '')
                        		echo "<script>\$(document).ready(function(){\$.colorbox({width:550,href:'index.php/sales/view_giftcard/-1/$line'})})</script>";
						}
						else if ($item['is_punch_card']) {
							 echo anchor("sales/view_punch_card/-1/$line", $item['name'],  array('class'=>'colbox','title'=>lang('giftcards_punch_card_details')));
                        	if ($item['punch_card_data']['punch_card_number'] == '')
                        		echo "<script>\$(document).ready(function(){\$.colorbox({width:550,href:'index.php/sales/view_punch_card/-1/$line'})})</script>";
						}
						else if ($item['is_invoice']){
                              echo anchor("sales/invoice_details/{$item['invoice_id']}/$line/width~900", $item['name'],  array('class'=>'colbox','title'=>lang('invoice_details')));
                      	}
						else if (!empty($item['modifiers'])){
							$cart_popups = $this->session->userdata('cart_popups');
							$key = $item['item_id'].'-'.$line;
							if(empty($cart_popups[$key])){
							  echo "<script>\$(document).ready(function(){\$.colorbox({href:'".site_url("sales/view_modifiers/{$item['item_id']}/{$line}/width~900")."'})})</script>";
								$cart_popups[$key] = true;
								$this->session->set_userdata('cart_popups', $cart_popups);
							}
							  echo anchor("sales/view_modifiers/{$item['item_id']}/{$line}/width~900", $item['name'],  array('class'=>'colbox','title'=>'Item modifiers'));
                      	}
                        else {
							echo $item['name'];//echo anchor("sales/view_modifiers/{$item['item_id']}/{$line}/width~900", $item['name'],  array('class'=>'colbox','title'=>'Item modifiers'));
                        }
                        ?>
                    </td>
                <?php if ($item['is_invoice']){ ?>
                    <td class="reg_item_price" id="reg_item_price_<?php echo $line ?>"><div><?php echo to_currency($item['price']); ?></div></td>
                    <?php echo form_hidden('price_'.$line,$item['price']); ?>
                <?php }else if ($items_module_allowed || true){ ?>
                    <td class="reg_item_price editable" id="reg_item_price_<?php echo $line ?>"><?php echo form_input('price_'.$line,$item['price'], 'onblur="sales.change_line_price('.$line.', this, event)"');?></td>
                <?php } ?>
                    <td class="reg_item_qty editable" id="reg_item_qty_<?php echo $line ?>">
                <?php if((isset($item['is_serialized']) && $item['is_serialized']==1) || (isset($item['is_punch_card']) && $item['is_punch_card']==1)){
                    echo "<div>".$item['quantity']."</div>";
                    echo form_hidden('quantity_'.$line,$item['quantity']);
                }else{
                    echo form_input('quantity_'.$line,$item['quantity'], 'onblur="sales.change_line_quantity('.$line.', this)"');
                }?>
                        </td>
                <?php if(isset($item['is_invoice']) && $item['is_invoice']==1){ ?>
                    <td class="reg_item_discount" id="reg_item_discount_<?php echo $line ?>"><div>0</div></td>
                    <?php   echo form_hidden('discount_'.$line,$item['discount']); ?>

                <?php }else{ ?>
                    <td class="reg_item_discount editable" id="reg_item_discount_<?php echo $line ?>"><?php echo form_input('discount_'.$line,$item['discount'], 'onblur="sales.change_line_discount('.$line.', this)"');?></td>
                    <?php }?>
                    <td class="reg_item_total" id="reg_item_total_<?php echo $line ?>">
						<div class='total_price_box'>
							<?php echo to_currency((($item['price'] + $item['modifier_total']) * $item['quantity']) - ($item['price'] * $item['quantity'] * $item['discount'] / 100)); ?>
						</div>
					</td>
                </tr>
                        </form>
			<?php
			}
		}
		?>

	</tbody>
</table>
<script type="text/javascript">
$('input','.editable').mouseup(function(e){
	console.log("MOUSE UP");
	e.preventDefault();
	$(this).unbind("mouseup");
});
$('input','.editable').focus(function(e){
	e.preventDefault();
	console.log("SELECTING THIS");
	this.select();
});
$(".select_all_onclick").focus(function () {
    $(this).select().mouseup(function (e) {
        e.preventDefault();
        $(this).unbind("mouseup");
    });
});
$('.reg_item_name select').customStyle();
$('.reg_item_type select').customStyle();
$('.colbox').colorbox();
setTimeout(function(){
	if ($(document.activeElement).attr('id') != 'item')
   		$('#item').focus();
},200);
</script>
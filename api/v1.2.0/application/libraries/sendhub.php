<?php
class Sendhub
{
	Protected $key = '44fade6bca0f48b4640ad34d39df630d1ed33322';
	Protected $login ='8013693765';
	Private $name = '';
	Private $phone_number = '';
	Private $contact_id = '';
	Private $group_id = '';
	Private $message = '';
	Private $message_id = '';
	
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}
	
	
	
	private function request($type)
	{
		//$url = 'https://api.sendhub.com/v1/contacts/?username='.$mnum.'&api_key='.$apk;
		$ch = curl_init();
		$name = $n;
		$number = stripslashes(rawurldecode($p));
		$number = str_replace( '(', "", $number );
		$number = str_replace( ')', "", $number );
		$number = str_replace( '-', "", $number );
		$number = trim($number);

		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(    'Content-Type:application/json'    ));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		switch($type)
		{
			case 'add_contact':
				curl_setopt($ch, CURLOPT_URL, 'https://api.sendhub.com/v1/contacts/?username='.$this->login.'&api_key='.$this->key);
				curl_setopt($ch, CURLOPT_POSTFIELDS, '{"name":"'.strtoupper($this->name).'","number":"'.$this->phone_number.'"}');
				break;
			case 'delete_contact':
				curl_setopt($ch, CURLOPT_URL, 'https://api.sendhub.com/v1/contacts/'.$this->contact_id.'/?username='.$this->login.'&api_key='.$this->key);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			case 'edit_contact':
				curl_setopt($ch, CURLOPT_URL, 'https://api.sendhub.com/v1/contacts/'.$this->contact_id.'/?username='.$this->login.'&api_key='.$this->key);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($ch, CURLOPT_POSTFIELDS, '{"id":"'.$this->contact_id.'","name":"'.$this->name.'","number":"'.$this->phone_number.'"}');
				break;
			case 'send_message':
				curl_setopt($ch, CURLOPT_URL, 'https://api.sendhub.com/v1/messages/?username='.$this->login.'&api_key='.$this->key);
				if ($this->send_time)
					curl_setopt($ch, CURLOPT_POSTFIELDS, '{"contacts":['.$this->contact_id.'],"text":"'.($this->message).'","scheduled_at":"'.$this->send_time.'"}');
				else
					curl_setopt($ch, CURLOPT_POSTFIELDS, '{"contacts":['.$this->contact_id.'],"text":"'.($this->message).'"}');
				break;
			case 'read_message':
				curl_setopt($ch, CURLOPT_URL, 'https://api.sendhub.com/v1/messages/'.$this->message_id.'/?username='.$this->login.'&api_key='.$this->key);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, '{"contacts":['.$this->message_id.'],"text":"'.$txt.'"}');
				break;
		}
		//curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, '{"name":"'.strtoupper($name).'","number":"'.$number.'"}');
		//$response = json_decode(curl_exec($ch), true);
		$response = curl_exec($ch);
		curl_close($ch); 
		
		$json_s_p = strpos($response, '{');
		$response = json_decode(substr($response, $json_s_p));
		//print_r($response);
		//foreach($response['items'] as $val)
			//print_r( $val );
		//$header_size = curl_getinfo($ch, CULINFO_HEADER_SIZE);
		//$header = substr($response, 0, $header_size);
		//$body = substr($response, $header_size);
		
		//print_r($body);
		
		return $response;
	}
	public function add_contact($name, $phone_number ){
		$this->name = $name;
		$this->phone_number = $phone_number;
		return $this->request('add_contact');		
	}


	public function delete_contact($id){
		$this->contact_id = $id;
		return $this->request('delete_contact');
	}


	public function edit_contact($id ,$name, $phone_number){
		$this->contact_id = $id;
		$this->name = $name;
		$this->phone_number = $phone_number;
		return $this->request('edit_contact');
	}
	
	public function send_message($message, $contact_ids, $send_time = false) {
		
		$this->message = trim( preg_replace( '/\n+/', "\\"."n", stripslashes(rawurldecode($message))));
		$this->send_time = $send_time;//2011-02-17T20:29:40-0800
		if (!is_array($contact_ids))
		{
			$this->contact_id = $contact_ids;			
			return $this->request('send_message');
		}	
		else 
		{
			$return_val = '';			
			$chunked_to = array_chunk($contact_ids, 20);
			//Separate each group by 20
			foreach($chunked_to as $chunk)
			{		
				$this->contact_id = implode(',', $chunk);
				$return_val = $this->request('send_message');
				log_message("error", 'sendhub send_message contacts - '.json_encode($this->contact_id).' - response '.json_encode($return_val));				
			}
			return $return_val;
		}
	}
	
/* readmsg pulls all msg Json while when vars are entered correctly it pulls a sinlge tread */
	public function read_message($message_id){
		$this->message_id = $message_id;
		return $this->request('read_message');
	}
	
}
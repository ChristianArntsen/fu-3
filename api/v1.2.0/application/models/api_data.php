<?php
class Api_data extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();	
		$this->load->model('Teetime');
		$this->load->model('Teesheet');	
		$this->load->model('teesheet');
		$this->load->model('course');
		$this->load->model('Person');
    }
	
	function get_course_areas()
	{
		// Return a list of course areas that have active courses (and that you have permissions to)
		// And have online booking active
		
		//TODO: make the above true... permissions and online booking
		$this->db->select('course_areas.area_id as area_id, label, course_areas.country AS country');
		$this->db->from('course_areas');
		$this->db->join('courses', 'course_areas.area_id = courses.area_id');
		$this->db->group_by('area_id');
		$this->db->where('active', 1);
		return $this->db->get();
	}
	
	function get_course_list($area_id)
	{
		$this->db->select('course_id, name');
		$this->db->from('courses');
		$this->db->where('active', 1);
		$this->db->where('area_id', $area_id);
		$results = $this->db->get()->result_array();
		
		foreach ($results as $key => $course) {
			$results[$key]['golf_course_id'] = $course['course_id'];
			unset($results[$key]['course_id']);
		}
		
		return $results;
	}

	function get_course_info($course_id)
	{
		$this->db->select('name, address, city, state, zip, phone');
		$this->db->from('courses');
		$this->db->where('course_id', $course_id);
		$this->db->where('active', 1);
		$this->db->limit(1);
		return $this->db->get();
	}
	
	function get_course_links_list($course_id)
	{
		$this->db->select('teesheet_id AS schedule_id, teesheet.holes, title, online_open_time, online_close_time, days_out, days_in_booking_window, foreup_teesheet.increment, foreup_courses.booking_rules');		
		$this->db->from('teesheet');
		$this->db->where('deleted', 0);
		$this->db->where('foreup_teesheet.online_booking', 1);
		$this->db->where('foreup_teesheet.course_id', $course_id);
		$this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$results = $this->db->get();
		
		 return $results;		
	}
	
	function get_course_app_data($course_id)
	{
		//join the course and app data tables for general information
		$this->db->select('name, address, course_latitude, course_longitude,red,green,blue,alpha,home_view,gps_view,scorecard_view, teetime_view, food_and_beverage_view, last_updated');
		$this->db->from('courses');
		$this->db->where('courses.course_id', $course_id);
		$this->db->join('course_app_data', 'courses.course_id = course_app_data.course_id');
		$this->db->limit(1);
		$data = $this->db->get()->row_array(); 
		
		//get all the links for the course
		$this->db->select('link_id,name, tee_1_name, tee_2_name, tee_3_name, tee_4_name, tee_5_name, tee_6_name, tee_7_name, tee_8_name');
		$this->db->from('links');
		$this->db->where('course_id', $course_id);
		$data['links'] = $this->db->get()->result_array();
		
		//get all hole information for each link
		foreach ($data['links'] as $key => $link) {
			$this->db->select('hole_number, par, front_latitude, front_longitude, center_latitude, center_longitude, back_latitude, back_longitude, hole_latitude, hole_longitude, middle_latitude, middle_longitude, tee_1_latitude, tee_1_longitude, tee_2_latitude, tee_2_longitude, tee_3_latitude, tee_3_longitude, tee_4_latitude, tee_4_longitude, tee_5_latitude, tee_5_longitude, tee_6_latitude, tee_6_longitude, tee_7_latitude, tee_7_longitude, tee_8_latitude, tee_8_longitude');				
			$this->db->from('holes');
			$this->db->where('link_id', $link['link_id']);
			$data['links'][$key]['holes'] = $this->db->get()->result_array();
			
			//unset the link_id. It had to be included so we could get the appropriate holes based on the link idea.
			unset($data['links'][$key]['link_id']);			
		}
		
		return $data;
	}
	
	function get_customer_list($course_id, $api_id)
	{
		$this->db->select('course_id, customers.person_id as person_id, first_name, last_name');
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id = people.person_id');		
		$this->db->where('course_id', $course_id);
		$this->db->where('api_id', $api_id);
		$this->db->order_by('course_id');
		$results = $this->db->get()->result_array();
		
		foreach ($results as $key => $guest) {
			$results[$key]['guest_id'] = $guest['person_id'];
			unset($results[$key]['person_id']);
			$results[$key]['golf_course_id'] = $guest['course_id'];
			unset($results[$key]['course_id']);
			
		}
// 		
		return $results;
	}

	function get_customer_info($person_id, $api_id)
	{
		$this->db->select('course_id, customers.person_id as person_id, first_name, last_name, phone_number, email, address_1 as address, city, state, zip, country');
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id = people.person_id');
		$this->db->where('api_id', $api_id);
		$this->db->where('people.person_id', $person_id);
		$this->db->order_by('course_id');
		$this->db->limit(1);
		return $this->db->get();
	}
	
	function save_customer_info(&$person_data, &$customer_data, $person_id = false)
	{
		return $this->Customer->save($person_data,$customer_data, $person_id);
	}
	
	function get_booked_teetimes_list($course_id = false, $schedule_id = false, $date = false, $api_id)
	{
		//calculate tomorrows date from $date
		$tomorrow = strtotime ( '+1 day' , strtotime ( $date ) ) ;
		$tomorrow = date ( 'Y-m-j' , $tomorrow );

		$this->db->select('TTID as reservation_id, start as start_time, player_count as guests' );
		$this->db->from('teetime');				
		$this->db->where('teesheet_id', $schedule_id);					
		$this->db->where('start >=', (date('Ymd', strtotime($date))-100)."0000");
		$this->db->where('start <', (date('Ymd', strtotime($tomorrow))-100)."0000");
		$this->db->where('status !=', 'deleted');
		$this->db->where('api_id', $api_id);
		$results = $this->db->get();
		
		$response = array(
			'success'=>true,
			'booked_reservations'=>array()
		);
		
		if ($results->num_rows() > 0)			
		{
			foreach ($results->result_array() as $key => $teetime) {
				$teetime['start_time'] = $this->Teesheet->format_time_string($teetime['start_time']);				
				$response['booked_reservations'][] = $teetime;				
			}			
		}	
		else 
		{
			$response['message'] = 'no booked reservation';										
		}	
	
		return $response;
	}

	function tee_time_is_available($course_id, $tee_sheet_id, $time, $player_count)
	{				
		$teetime_data = array(
			'course_id'=>$course_id,
			'teesheet_id'=> $tee_sheet_id,
			'start'=>$this->to_db_time_string($time),
			'player_count'=>$player_count
		);				
		
		return $this->Teetime->check_availability($teetime_data);
	}
	
	function cancel_reservation($TTID, $api_id)
	{		
		$this->db->where("(`TTID` = '$TTID' OR `TTID` = '{$TTID}b')");
		$this->db->where('api_id', $api_id);
		$this->db->from('teetime');
		$this->db->limit(2);		
	    $this->db->update('teetime', array('status' => 'deleted', 'date_cancelled' => date('Y-m-d H:i:s')));
		return $this->db->affected_rows() > 0;
	}
	
	function get_tee_time_info($TTID, $api_id)
	{
		$this->db->from('teetime');
		$this->db->where('TTID', $TTID);
		$this->db->where('api_id', $api_id);
		$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
		$this->db->join('courses', 'teesheet.course_id = courses.course_id');
		$results = $this->db->get()->row_array();			
		
		$data = array();
		if ($results) 
		{
			$data = array(
			'start_time'=>$this->Teesheet->format_time_string($results['start']),
			'available_spots'=>$this->MAX_PLAYERS_FOR_TEETIME() - $results['player_count'],	
			'schedule_id'=>$results['teesheet_id'],
			'golf_course_id'=>$results['course_id'],
			'golf_course_name'=>$results['name'],
			'golf_course_address'=>$results['address'],
			'golf_course_city'=>$results['city'],
			'golf_course_state'=>$results['state'],
			'golf_course_zip'=>$results['zip'],
			'golf_course_country'=>$results['country']
			);
		}
		
		return $data;
	}
	
	function join_reservation(&$person_data, &$customer_data, $tee_time_id)
	{
		
		//use 4 as the standard for player count
		$this->db->select('teesheet.course_id, teetime.player_count, teetime.person_id, teetime.person_id_2, teetime.person_id_3, teetime.person_id_4, teetime.person_id_5');
		$this->db->from('teetime');
		$this->db->where('TTID', $tee_time_id);
		$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
		$results = $this->db->get()->row_array();
		$player_count = $results['player_count'];
		// echo $this->db->last_query();
		//get all the customers that are associated with the teetime
		$players = $this->player_names($results);		
		
		$insertIndex = $player_count + 1; //add one because its not zero based in the db

		if ($player_count < 4) {			
			$customer_data['course_id'] = $results['course_id'];
			//create the person and get the person_id
			$response['success'] = $this->Api_data->save_customer_info($person_data, $customer_data);					
			if ($response['success']) {
				//add the new customer to the list of players
				
				$players[] = array( 
					'name'=>$person_data['first_name']." ".$person_data['last_name'],
					'person_id'=>$person_data['person_id']
				);	
				$response['players'] = $players;			
				//create the row with data to update				
				$row["person_id_{$insertIndex}"] = $customer_data['person_id'];
				$row['player_count'] = $player_count + 1;
				$this->db->where('TTID',$tee_time_id);						
				$this->db->from('teetime');					
			    $this->db->update('teetime', $row);												
			}
			else 
			{
				$response['message'] = "The customer info could not be saved";
			}
		}
		else
		{
			$response['message'] = "All spots for tee time are booked";	
		}
		
		return $response;
	}

	function tee_time_details($TTID, $api_key)
	{
		//format_time_string using the teesheet model
		
		$this->db->select('teesheet.course_id, teetime.start, teetime.player_count, teetime.person_id, teetime.person_id_2, teetime.person_id_3, teetime.person_id_4, courses.name, courses.allow_friends_to_invite, courses.payment_required');
		$this->db->from('teetime');
		$this->db->where('TTID', $TTID);
		$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
		$this->db->join('courses', 'teesheet.course_id = courses.course_id');
		$results = $this->db->get()->row_array();			
		
		if ($results) 
		{
			$data = array(
			'start_time'=>$this->Teesheet->format_time_string($results['start']),
			'course_name'=>$results['name'],
			'course_id'=>$results['course_id'],
			'available_spots'=>$this->MAX_PLAYERS_FOR_TEETIME() - $results['player_count'],
			'players'=>$this->player_names($results),
			'payment_url'=> null,
			'booking_url'=>base_url().'index.php/be/reservation/'.$results['course_id'],
			'allow_friends_to_invite'=>$results['allow_friends_to_invite'],
			'payment_required'=>$results['payment_required']		
			);
		}
				
		return $data;		
	}

	function available_tee_times($start_time, $end_time, $teesheet_id, $player_count, $max_price)
	{			
		$this->db->from('teesheet');
		$this->db->where('teesheet_id', $teesheet_id);
		$teesheet = $this->db->get()->row_array();
		
		$time_range = 'full_day';                               	
        $is_api_request = true;
		$days_to_future_date = $this->days_between_dates(date('Y-m-d'), $start_time);				
		
		$start_time = $this->to_db_time_string($start_time);
		$end_time = $this->to_db_time_string($end_time);
		
		$this->teesheet->load_booking_settings($teesheet['course_id'], $teesheet_id);
		$this->teesheet->apply_booking_filters($teesheet['holes'], $player_count, $time_range, '', '', '', '');		      
		
        return $this->teesheet->get_available_teetimes($is_api_request, $teesheet_id, $days_to_future_date, $max_price, $start_time, $end_time);
	}

	/*
	 * converts the open time to total minutes, the start_time to total minutes and then mods the difference with the teesheet increment
	 */
	function is_valid_teetime($teesheet_info, $start_time)
	{		
		$open_time_minutes = ((int)substr($teesheet_info->online_open_time, 0,2) *60) + (int)substr($teesheet_info->online_open_time, 2,2);
		$start_time_minutes = ((int)date('H', strtotime($start_time)) * 60) + (int)date('i', strtotime($start_time));
		$remainder = ($start_time_minutes - $open_time_minutes) % (int)$teesheet_info->increment;	
		return $remainder == 0;
	}
	
	function book_teetime($teesheet_id, $start, $person_id, $player_count, $api_id, $holes, $cart_count, $api_name)
	{
		$status = array('success'=>false);
		$teesheet_info = $this->Teesheet->get_info($teesheet_id);
		$person_info = $this->Person->get_info($person_id);
		$course_info = $this->Course->get_info($teesheet_info->course_id);
		date_default_timezone_set($course_info->timezone);
		$holes = $holes > $teesheet_info->holes ? $teesheet_info->holes : $holes;
		$holes = ($holes % 9 == 0) ? $holes : 9;
		$cart_count = $cart_count ? $cart_count : 0;
		$start = $this->to_db_time_string($start);
		$end  = $start + $teesheet_info->increment;
        if ($end%100 > 59)
            $end += 40;	

		$event_data = array(
			'teesheet_id'=>$teesheet_id,
			'start'=>$start,
			'end'=>$end,
			'side'=>'front',
			'allDay'=>'false',
			'holes'=>$holes,
			'player_count'=>$player_count,
			'type'=>'teetime',
			'carts'=>$cart_count,
			'person_id'=>$person_id,
			'person_name'=>$person_info->last_name.', '.$person_info->first_name,
			'title'=>$person_info->last_name,
			'booking_source'=>'online',
			'booker_id'=>$person_id,
			'details'=>"Reserved through $api_name @ ".date('g:ia n/j T'),
			'api_id'=>$api_id,
			'booking_source'=>'api'	
		);
		
		//This is ready to go - but not currently being offered
		// $email_data = array(
			// 'person_id'=>$person_id,
			// 'course_name'=>$course_info->name,
			// 'course_phone'=>$course_info->phone,
			// 'first_name'=>$person_info->first_name,
			// 'booked_date'=>date('n/j/y', strtotime($start+1000000)),
			// 'booked_time'=>date('g:ia', strtotime($start+1000000)),
			// 'booked_holes'=>$holes,
			// 'booked_players'=>$player_count			
		// );
		
		$response = array();	
		
		if ($this->is_valid_teetime($teesheet_info, $start)) {
			$save_response = $this->Teetime->save($event_data,false,$response,true, $teesheet_info->course_id);
			if ($save_response['success'])
			{									
				$status = array(
					'success'=>true,
					'booked_reservation'=>true,
					'reservation_id'=>$response[0]['id']);								
			}	
			else 
			{	
				$status = array(
					'success'=>true,
					'booked_reservation'=>false,
					'message'=>'Reservation time is no longer available'
				);
			}
		}	 
		else 
		{
			$status = array(
					'success'=>true,
					'booked_reservation'=>false,
					'message'=>'Invalid tee time'
				);
		}		
		
		
		return $status;
	}

	/*
	Private function to get the player names
	*/
	function player_names($results)
	{		
		$players = array();
		for ($i=1; $i <= $results['player_count']; $i++) {			
			$person_id = !$results["person_id_{$i}"] == ''? $results["person_id_{$i}"] : $results["person_id"];
			
			$this->db->select('first_name, last_name');
			$this->db->from('people');
			$this->db->where('person_id', $person_id);
			$name = $this->db->get()->row_array();
			
			$players[] = array(
				'name'=>$name['first_name']." ". $name['last_name'],
				'person_id'=>$person_id				
			);
		}
		
		return $players;
	}
	
	/*
	Private function to return the max player count
	*/
	function MAX_PLAYERS_FOR_TEETIME()
	{
		return 4;
	}
	
	/*
	Private function to convert dates from this format '2013-01-14T19:00:00' to a string that can compare against the db
	*/
	function to_db_time_string($time)
	{
		return date('YmdHi', strtotime($time)) - 1000000;//takes the month field down by 1
	}
	
	/*
	 * Private function to return number of days between two dates
	 */	 
	 function days_between_dates($start, $end) {
		$start_ts = strtotime($start);		
		$end_ts = strtotime(date('Y-m-d',strtotime($end)));		
		$diff = $end_ts - $start_ts;
		$milliseconds_in_day = 86400;
	 return round($diff / $milliseconds_in_day);
	}
}

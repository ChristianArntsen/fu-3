<?php
class Tournament extends CI_Model
{
	/*
	Returns the count of all tournaments
	*/
	function count_all()
	{		
		// $course_id = '';
        // if (!$this->permissions->is_super_admin())
        // {
	        // //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			// //$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			// $course_ids = array();
			// $this->get_linked_course_ids($course_ids);
		    // $this->db->where_in('course_id', array_values($course_ids));
	    // }
        // $this->db->from('tournaments');
		// $this->db->where("deleted = 0");
		// return $this->db->count_all_results();		
		return 1;
	}
	
	/*
	Returns all the tournaments
	*/
	function get_all($limit=10000, $offset=0)
	{		      			   
        $this->db->from('tournaments');
		$this->db->where("deleted = 0");
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();			
	}
	
	/*
	Get search suggestions to find tournaments
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        $suggestions = array();

		$this->db->from('tournaments');
		$this->db->like('name', $search);
		// $this->db->where("deleted = 0");
		$this->db->order_by("name", "asc");
		$by_number = $this->db->get();
		
		foreach($by_number->result() as $row)
		{			
			$suggestions[]=array('label' => $row->name);
		}
		
		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	
	/*
	Preform a search on tournaments
	*/
	function search($search, $limit=20)
	{
		$course_id = '';

        $this->db->from('tournaments');		
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->where("deleted = 0");
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		return $this->db->get();	
	}
	
	/*
	Gets information about a particular tournament
	*/
	function get_info($tournament_id)
	{
        $this->db->from('tournaments');
		$this->db->where('tournament_id',$tournament_id);
		$this->db->where("deleted = 0");		
		$query = $this->db->get();
		
		return $query->row();
	}
	
	/*
	Inserts or updates a tournament
	*/
	function save(&$tournament_data,$tournament_id=false)
	{		
			if (!$tournament_id or !$this->exists($tournament_id))
			{
				if($this->db->insert('tournaments',$tournament_data))
				{
					$tournament_data['tournament_id']=$this->db->insert_id();
					return true;
				}
				return false;
			}
	
			$this->db->where('tournament_id', $tournament_id);
			
			return $this->db->update('tournaments',$tournament_data);		
	}
	
		/*
	Determines if a given tournament_id is a tournament
	*/
	function exists($tournament_id)
	{
        $this->db->from('tournaments');
		$this->db->where('tournament_id',$tournament_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function get_id_by_name($name)
	{		
		$this->db->from('tournaments');
		$this->db->where('name',$name);
		$this->db->where('deleted',0);
		$query = $this->db->limit(1)->get();		
		$query = $query->result_array();

		return isset($query[0]['tournament_id']) ? $query[0]['tournament_id'] : false;
	}
	
	/*
	Deletes a list of tournaments
	*/
	function delete_list($tournament_ids)
	{
        $this->db->where_in('tournament_id',$tournament_ids);
		return $this->db->update('tournaments', array('deleted' => 1));
 	}
	
	/*
	 Updates the remaining pot balance for the tournament
	 */
	 function update_remaining_pot_balance($tournament_id, $remaining_balance)
	 {
	 	$this->db->where('tournament_id',$tournament_id);
		return $this->db->update('tournaments', array('remaining_pot_balance' => $remaining_balance));
	 }
	 
	 function update_tournament_pot($tournament)
	 {
	 
	 	$tournament->accumulated_pot = $tournament->accumulated_pot + $tournament->pot_fee;
		$tournament->remaining_pot_balance = $tournament->remaining_pot_balance + $tournament->pot_fee;
		$this->save($tournament, $tournament->tournament_id);		
	 }
	 
	 
	 /*
	   Gets the tax rate for the green fee and cart fee
	  
  	*/
  	function calculate_tournament_taxes ($tournament_id, $discount, &$taxes)
	{		
		$tournament_data = $this->Tournament->get_info($tournament_id); 		
		
		//Handle discount
		$total_discount = $this->Tournament->tournament_price_before_taxes($tournament_data) * ($discount / 100);		
		$percent_to_discount = $total_discount / ($tournament_data->green_fee + $tournament_data->cart_fee);
		$tournament_data->green_fee = $tournament_data->green_fee - ($tournament_data->green_fee * $percent_to_discount);
		$tournament_data->cart_fee = $tournament_data->cart_fee - ($tournament_data->cart_fee * $percent_to_discount);		

		//green fee tax amount
		$item_number = $this->session->userdata('course_id').'_3';
		$item_id = $this->Item->get_item_id($item_number);
		$green_fee_tax_item = $this->Item_taxes->get_info($item_id);
		$tax_name = $green_fee_tax_item[0]['percent'].'% ' . $green_fee_tax_item[0]['name'];
		$taxes[$tax_name] += $tournament_data->green_fee - $this->Tournament->green_fee_before_taxes($tournament_data);
		
		//cart fee tax amount
		$item_number = $this->session->userdata('course_id').'_1';
		$item_id = $this->Item->get_item_id($item_number);
		$cart_fee_tax_item = $this->Item_taxes->get_info($item_id);
		$tax_name = $cart_fee_tax_item[0]['percent'].'% ' . $cart_fee_tax_item[0]['name'];
		$taxes[$tax_name] += $tournament_data->cart_fee - $this->Tournament->cart_fee_before_taxes($tournament_data);
		
		//inventory item tax amounts
		//inventory item taxes
		foreach ($this->Tournament_inventory_items->get_info($tournament_id) as $item) {
				
			$tax_info = $this->Item_taxes->get_info($item->item_id);
			$tax_rate_1 = $tax_info[0]['percent'];			
			$tax_rate_2 = $tax_info[1]['percent'];			
			$cumulative = $tax_info[1]['cumulative'];
			
			if ($cumulative == 1) {
				$tax_rate_2 = $tax_rate_2 + ($tax_rate_1 * $tax_rate_2 / 100);
			}
	
			$total_item_tax = $tax_rate_1 + $tax_rate_2;
			
			//this is the price before taxes			
			$item->price =  $item->price / (1 + ($total_item_tax / 100));				
			
			foreach($tax_info as $key=>$tax)
			{
				$name = $tax['percent'].'% ' . $tax['name'];
				$tax_amount = 0;
				if ($tax['cumulative'])
				{					
					$prev_tax = $taxes[$tax_info[$key-1]['percent'].'% ' . $tax_info[$key-1]['name']];				
					$tax_amount=(($item->price * $item->quantity + $prev_tax) * (($tax['percent']) / 100));					
				}
				else
				{
					$tax_amount=($item->price*$item->quantity)*(($tax['percent'])/100);
				}

				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += number_format($tax_amount,2);
			}																											
		}
		
		return false;
	}

	function cart_fee_before_taxes($tournament)
	{
		$item_number = $this->session->userdata('course_id').'_1';
		$item_id = $this->Item->get_item_id($item_number);
		$cart_fee_tax_item = $this->Item_taxes->get_info($item_id);
		return $tournament->cart_fee / (1 + $cart_fee_tax_item[0]['percent']/100);
	}
	
	function green_fee_before_taxes($tournament)
	{
		$item_number = $this->session->userdata('course_id').'_3';
		$item_id = $this->Item->get_item_id($item_number);
		$green_fee_tax_item = $this->Item_taxes->get_info($item_id);
		return $tournament->green_fee / (1 + $green_fee_tax_item[0]['percent']/100);
	}
	
	function tournament_price_before_taxes($tournament)
	{
		$tournament_fee_taxes = 0;
		$tournament_inventory_item_taxes = 0;
		
		//green fee and cart fee taxes
		$tournament_fee_taxes += $tournament->cart_fee - $this->Tournament->cart_fee_before_taxes($tournament);
		$tournament_fee_taxes += $tournament->green_fee - $this->Tournament->green_fee_before_taxes($tournament);
		
		//inventory item taxes
		foreach ($this->Tournament_inventory_items->get_info($tournament->tournament_id) as $tournament_inventory_item) {
					
			$item_info = $this->Item->get_info($tournament_inventory_item->item_id);
			$tax_info = $this->Item_taxes->get_info($tournament_inventory_item->item_id);
			$tax_rate_1 = $tax_info[0]['percent'];			
			$tax_rate_2 = $tax_info[1]['percent'];			
			$cumulative = $tax_info[1]['cumulative'];
			
			if ($cumulative == 1) {
				$tax_rate_2 = $tax_rate_2 + ($tax_rate_1 * $tax_rate_2 / 100);
			}
	
			$total_item_tax = $tax_rate_1 + $tax_rate_2;
			
			$price_before_tax =  $tournament_inventory_item->price / (1 + ($total_item_tax / 100));
			$tournament_inventory_item_taxes += $tournament_inventory_item->price - $price_before_tax;
			
		}
		
		
		$total_taxes = $tournament_fee_taxes + $tournament_inventory_item_taxes;
				
		return $tournament->total_cost - $total_taxes;
	}
}
?>

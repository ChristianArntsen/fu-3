<?php
$lang['help_no_topic_to_display']='No topics to display.';
$lang['help_delete_selected_article']='Are you sure you want to delete the selected article?';
$lang['help_last_updated']='Last Updated';
$lang['help_find_this_articles_helpful']='Other users also find these articles helpful';
$lang['help_article_could_be_improved']='We are always wanting to improve. What about this article could be improved?';
$lang['help_please_rephrase']='Sorry, we couldn\'t locate any support for this search. Please rephrase and try again or';
$lang['help_request_answer']='request an answer here';
$lang['help_send_us_email']='Try searching again or send us an email.';
$lang['help_topic']='Topic';
$lang['help_details']='Details';
$lang['help_keywords']='Keywords';
$lang['help_contact_us']='Contact Us';
$lang['help_ask_question']='Ask a question';
$lang['help_search_term']='Have a Question?  Ask or enter a search term here.';
$lang['help_browse_by_topic']='Browse by Topic';
$lang['help_common_questions']='Common Questions';
$lang['help_articles']='Articles';
$lang['help_view_all']='View All';
?>
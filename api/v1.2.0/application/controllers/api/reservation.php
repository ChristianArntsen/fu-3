<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Reservation extends REST_Controller
{
	private $filter_time_range_presets = array(
        'morning'=> array('start'=>'0000','end'=>'1100'),
        'midday'=>  array('start'=>'1000','end'=>'1500'),
        'evening'=> array('start'=>'1400','end'=>'2359'),
        'full_day'=>array('start'=>'0000','end'=>'2359')
        );
		
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Api_data');			
	}
	
	function is_available_get()
	{
		//make it so they can only book between appropriate hours for the course in the teesheet table		
		$course_id = $this->input->get('golf_course_id');
		$tee_sheet_id = $this->input->get('schedule_id');
		$time = $this->input->get('time');		
		$player_count = $this->input->get('guests');
		
		$errors = array();
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: golf_course_id';
		if (!$tee_sheet_id || $tee_sheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$time || $time == '')
			$errors[] = 'Missing parameter: time';
		if (!$player_count || $player_count == '')
			$errors[] = 'Missing parameter: guests';		
		if (count($errors)>0)
			$this->response(array('error', implode(', ', $errors)));
		
		$is_available = $this->Api_data->tee_time_is_available($course_id, $tee_sheet_id, $time, $player_count);
		
		if ($is_available) {
			$response = array(
				'success'=>true,
				'reservation_is_available'=>true
			);
			$this->response($response);
		}
		else 
		{
			$this->response(array('success'=>false,'error'=>'The reservation time is not available'));
		}			
	}
	
	function book_get()
	{			
		$teesheet_id = $this->input->get('schedule_id');
		$start = $this->input->get('time');			
		$person_id = $this->input->get('guest_id');
		$guests = $this->input->get('guests');
		$holes = $this->input->get('holes');
		
		//optional parameters
		$carts = $this->input->get('carts') ? strtolower($this->input->get('carts')) : 0;
		
		//set the carts = to the number of guests
		if ($carts === 'y' || $carts === 'yes' || $carts === 't' || $carts === 'true' || (int)$carts == 1) 		
		{
			$carts = $guests;
		}
		else 
		{
			$carts = 0;			
		}
		
		$errors = array();
		if (!$teesheet_id || $teesheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$start || $start == '')
			$errors[] = 'Missing parameter: time';	
		if (!$person_id || $person_id == '')
			$errors[] = 'Missing parameter: guest_id';		
		if (!$guests || $guests == '')
			$errors[] = 'Missing parameter: guests';	
		if (!$holes || $holes == '')
			$errors[] = 'Missing parameter: holes';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		//I am not calling the reservation/is_available function first because the Teetime->save tells us if the time is unavailable
		$response = $this->Api_data->book_teetime($teesheet_id, $start, $person_id, $guests, $this->rest->api_id, $holes, $carts, $this->rest->key_name);
		$this->response($response);		
	}
	
	function cancel_get($teetime_id)
	{		
		$TTID = $this->input->get('reservation_id');
		
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: reservation_id';
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		$success = $this->Api_data->cancel_reservation($TTID, $this->rest->api_id);
		
		if ($success) 
		{
			$response = array('success'=>true, 'reservation_cancelled'=>true);
			$this->response($response);
		}
		else 
		{
			$this->response(array('success'=>true,'error'=>'The reservation could not be cancelled'));
		}
	}
	
	//tee time info
	function info_get()
	{
		$TTID = $this->input->get('reservation_id');
		
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: reservation_id';
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		$teetime_info = $this->Api_data->get_tee_time_info($TTID, $this->rest->api_id);		
		// echo $this->db->last_query();
		if ($teetime_info)
			$this->response(array('success'=>true,'reservation_info'=>$teetime_info));
		else {
			$this->response(array('success'=>true,'reservation_info'=>false,'message'=>'No results'));			
		}
	}

	function available_times_get()
	{		
		$start_time = $this->input->get('start_time');
		$end_time = $this->input->get('end_time');		
		$time_range = $this->input->get('time_range');		
		$date = $this->input->get('date');
		$teesheet_id = $this->input->get('schedule_id');
		$player_count = $this->input->get('guests') ? $this->input->get('guests') : 1;
		$player_count = $player_count > 4 ? 4 : $player_count;		
		$max_price = $this->input->get('max_price');
		
		if ($time_range && $date)
		{
			$value = $this->filter_time_range_presets[$time_range];			
			$date = date('Y-m-d', strtotime($date));		
			$start_time = "$date"."T".$value['start'];
			$end_time = "$date"."T".$value['end'];
		}
		
		$different_days = date('Ymd', strtotime($start_time)) == date('Ymd', strtotime($end_time)) ? false : true;

		$errors = array();
		if ($time_range && !isset($this->filter_time_range_presets[$time_range]) )
			$errors[] = 'Invalid parameter: time_range. Valid options are morning, midday, evening & full_day';
		if ($time_range && !$date)
			$errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		if (!$time_range && $date)
			$errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		if (!$start_time || $start_time == '')
			$errors[] = 'Missing parameter: start_time';
		if (!$end_time || $end_time == '')
			$errors[] = 'Missing parameter: end_time';
		if (!$teesheet_id || $teesheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';	
		if ($different_days)
			$errors[] = 'Invalid parameters: start_time and end_time must be on the same day';
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
				
		$data = $this->Api_data->available_tee_times($start_time, $end_time, $teesheet_id, $player_count, $max_price);
		
		if ($data) {
			$response = array(
			'success'=>true,			
			'available_reservation_times'=>$data
			);
			$this->response($response);
		} 
		else 
		{
			$this->response(array(
				'success'=>true,
				'available_reservation_times'=>array()
				)
			);
		}		
		
	}
	
    function list_get()
    {
	    $course_id = $this->input->get('golf_course_id');
		$schedule_id = $this->input->get('schedule_id');
		$date = $this->input->get('date') ? $this->input->get('date') : date('Y-m-d');		
		
		$booked_teetimes = $this->Api_data->get_booked_teetimes_list($course_id, $schedule_id, $date, $this->rest->api_id);		
		$this->response($booked_teetimes);
	}
		
	/*
	 * FACEBOOK API
	 */		
	function join_get()
	{
		$name = $this->input->get('name');
		$email = $this->input->get('email');		
		$tee_time_id = $this->input->get('tee_time_id');		
		
		$errors = array();
		if (!$name || $name == '')
			$errors[] = 'Missing parameter: name';
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';
		if (!$tee_time_id || $tee_time_id == '')
			$errors[] = 'Missing parameter: tee_time_id';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
			
		//use the library to get first and last name
		$this->load->library('name_parser');
			$np = new Name_parser();
			$np->setFullName($name);
			$np->parse();
			if (!$np->notParseable()) {
					$first_name=$np->getFirstName();
					$last_name=$np->getLastName();
			}
			
        $person_data = array(
	        //Required 
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=> $email,
			//Optional
			'phone_number'=>$phone_number = $this->input->get('phone_number'),						
		);
		
		
		$customer_data=array(
			'api_id' => $this->rest->api_id,			
		); 	
		
		$data = $this->Api_data->join_reservation($person_data, $customer_data, $tee_time_id);
		
		if ($data['success']) {
			$response = array(
				'success'=>true,
				'joined_tee_time'=>true,
				'person_id'=>$customer_data['person_id'], 
				'players'=>$data['players']
			);
			$this->response($response);
		}
		else 
		{
			$response = array(
				'success'=>true,
				'joined_tee_time'=>false,
				'error'=>$data['message']
			);
			$this->response($response);
		}
	}
	
	/*
	 * FACEBOOK API
	 */
	function tee_time_details_get()
	{
		$TTID = $this->input->get('tee_time_id');
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: tee_time_id';
		if (count($errors)>0)
			$this->response(array('error', implode(', ', $errors)));
		
		$data = $this->Api_data->tee_time_details($TTID, $this->rest->api_id);
		
		if ($data) {
			$response = array('success'=>true,'tee_time_details'=>$data);
			$this->response($response);
		} 
		else 
		{
			$this->response(array('success'=>true,'tee_time_details'=>array(),'error'=>"No information is available for tee_time_id:{$TTID}"));
		}
		
	}			
}
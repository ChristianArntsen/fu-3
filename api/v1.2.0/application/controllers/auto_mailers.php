<?php
require_once ("secure_area.php");
class Auto_mailers extends Secure_area 
{
	function __construct()
	{
		parent::__construct('auto_mailers');
	}

	function index()
	{
		$config['base_url'] = site_url('auto_mailers/index');
		$config['total_rows'] = $this->Auto_mailer->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_auto_mailers_manage_table($this->Auto_mailer->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('auto_mailers/manage',$data);
	}
   

	
	function search($offset = 0)
	{
		$search=$this->input->post('search');
		$data_rows=get_auto_mailers_manage_table_data_rows($this->Auto_mailer->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, 'all', $offset),$this);
		$config['base_url'] = site_url('auto_mailers/index');
        $config['total_rows'] = $this->Auto_mailer->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Auto_mailer->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$campaign_id = $this->input->post('row_id');
		$data_row=get_auto_mailers_data_row($this->Auto_mailer->get_info($campaign_id),$this);
		echo $data_row;
	}

	function view($auto_mailer_id=-1)
	{
		$data = array();
		$data['auto_mailer'] = $this->Auto_mailer->get_info($auto_mailer_id);	
		//echo 'finished one';	
		$data['campaigns'] = $this->Auto_mailer_campaign->get_all($auto_mailer_id)->result_array();
		//echo $this->db->last_query();
		//print_r($data['campaigns']);
	    //$everyone_count = $this->Customer->count_all(true);
		/*foreach ($this->Customer->get_group_info() as $group)
		{
			$data['groups'][$group['group_id']] = $group['label'];
	        $gr = $this->Customer->get_multiple_group_info(array($group['group_id']), 'email');
	
	        $data['group_count'][$group['group_id']]['email'] = $gr->num_rows();
	        $grp = $this->Customer->get_multiple_group_info(array($group['group_id']), 'phone');
	
	        $data['group_count'][$group['group_id']]['phone'] = $grp->num_rows();
	    }*/
	
	    	
	    //$info = $this->Auto_mailer->get_info($campaign_id);
	
		$this->load->view("auto_mailers/form",$data);
	}
	function view_recipients($auto_mailer_id = -1)
	{
		$data = array();
		$data['recipients'] = $this->Recipient->get_all($auto_mailer_id)->result_array();
		$data['auto_mailer_id'] = $auto_mailer_id;
		
		$this->load->view("auto_mailers/form_recipients", $data);
	}
	function save($auto_mailer_id=-1)
	{
		$name = $this->input->post('name');
		$data = array('name'=>$name,'course_id'=>$this->session->userdata('course_id'));
//			echo json_encode(array('success'=>true,'message'=>lang('auto_mailers_saving_success')));
	//		return;
		if($this->Auto_mailer->save($data, $auto_mailer_id)) {
			$this->Auto_mailer_campaign->delete_all($auto_mailer_id);
			//echo $this->db->last_query();
			$c_count = 1;
			while ($this->input->post('trigger_'.$c_count)) {
				//echo 'c_count '.$c_count;
				$campaign_data = array(
					'trigger' => $this->input->post('trigger_'.$c_count),
					'days_from_trigger' => $this->input->post('days_after_'.$c_count),
					'campaign_id' => $this->input->post('campaign_id_'.$c_count),
					'auto_mailer_id' => $data['auto_mailer_id']
				);
				
				if ($campaign_data['campaign_id'] != '')
				{
					$this->Auto_mailer_campaign->save($campaign_data);
				}
				//echo $campaign_id.' - '.$this->db->last_query();
				//print_r($campaign_data);
				$c_count++;
			}
			echo json_encode(array('success'=>true,'message'=>lang('auto_mailers_saving_success')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('auto_mailers_saving_error')));
		}
	}
	function save_recipients($auto_mailer_id = -1)
	{
		$r_count = 1;
		while($this->input->post('person_id_'.$r_count))
		{
			if ($this->input->post('start_date_'.$r_count))
			{
				$data = array(
					'auto_mailer_id'=>$auto_mailer_id,
					'person_id'=>$this->input->post('person_id_'.$r_count),
					'start_date'=>$this->input->post('start_date_'.$r_count)
				);
				//echo 'person_id_'.$r_count.' - '.$this->input->post('person_id_'.$r_count);
				$this->Recipient->save($data);
				//echo 'stuff';
			}
			$r_count++;	
		}
		//echo $this->db->last_query();
		echo json_encode(array('success'=>true,'message'=>lang('auto_mailers_saving_success')));
	}
	function delete()
	{
		$campaigns_to_delete=$this->input->post('ids');

		if($this->Auto_mailer->delete_list($campaigns_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('marketing_campaigns_successful_deleted').' '.
			count($campaigns_to_delete).' '.lang('marketing_campaigns_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('marketing_campaigns_cannot_be_deleted')));
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
    // part of changes for v0.1
    // changed from 550 to 1065,
    // to cater the template view at the right
		return 700;
	}
}
?>
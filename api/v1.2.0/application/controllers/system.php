<?php
// Client Booking Engine
class System extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	}
	
	function feedback() {
		$this->load->view('system/feedback');
	}
	
	function send_feedback() {
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		
		$data = array();
		
		/*$this->load->library('email');
		$this->email->from($email, $name);
		$this->email->to('signorehopkins@gmail.com'); 

		$this->email->subject('Feedback: '.$subject);
		$this->email->message($message);*/
	
		$result = send_sendgrid('feedback@foreup.com', 'Feedback: '.$subject, $message, $email, $name);
			
		if ($result['success'])
			echo json_encode(array('success'=>true,'message'=>lang('common_feedback_sent')));
		else
			echo json_encode(array('success'=>false,'message'=>lang('common_feedback_not_sent')));
	}
}
?>
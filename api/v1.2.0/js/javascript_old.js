var calendar = '';
var calendarback = '';
var showingBackNine = false;
var ttArray = {};
var ttArray2 = {};
$(document).ready(function() {
    //$('#stats').tabs();
    $('#weatherBox').tabs();
    $("#datepicker").datepicker({
        pickerClass:'datepicker_vista',
        onSelect: function(dateText, inst) {
           //trace(dateText);
            //calendar.fullCalendar('changeView', 'agendaDay');
            calendar.fullCalendar('gotoDate',dateText.slice(6), dateText.slice(0,2)-1, dateText.slice(3,5));
            calendarback.fullCalendar('gotoDate',dateText.slice(6), dateText.slice(0,2)-1, dateText.slice(3,5));
        }
    });
    $("#password").keydown(function(e){
        if(e.keyCode == 13) {
        gc.ajaxLogin('ix');
        }
    })
    $("#signin_submit").click(function(){
        gc.ajaxLogin('ix');
    })
    $('#libutton2').click(function(){
        gc.ajaxLogin('sf');
    });
    $('#lipassword2').keydown(function(e){
        if(e.keyCode == 13) {
            $('#libutton2').click();
        }
    });
    $(".login").click(function(e) {
        e.preventDefault();
        $("fieldset#signin_menu").toggle();
        $(".signin2").toggleClass("menu-open");
        $('#username').focus();
    });
    $("fieldset#signin_menu").mouseup(function() {
        return false;
    });
    $(document).mouseup(function(e) {
        if($(e.target).parent("a.signin2").length==0) {
            $(".signin2").removeClass("menu-open");
            $("fieldset#signin_menu").hide();
        }
    }); 
    $.each(calInfo.caldata, function(key, value) { 
        //console.log(new Date(2011, 08, 15, 13, 23, 00, 00));
        value.start = getDateFromTimeString(value.stimestamp);
        value.end = getDateFromTimeString(value.etimestamp);
        //value.start.setTime(value.stimestamp);
    });
    $.each(calInfo.bcaldata, function(key, value) { 
        //console.log(new Date(2011, 08, 15, 13, 23, 00, 00));
        value.start = getDateFromTimeString(value.stimestamp);
        value.end = getDateFromTimeString(value.etimestamp);
        //value.start.setTime(value.stimestamp);
    });
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    //console.log('oh '+openhour);
    //console.log('ch '+closehour);
    //console.log('in '+increment1);

    calendar = $('#calendar').fullCalendar({
            theme:true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            allDaySlot:false,
            defaultView:'agendaDay',
            minTime:calInfo.openhour,
            maxTime:calInfo.closehour,
            slotMinutes:parseInt(calInfo.increment1),
            defaultEventMinutes:parseInt(calInfo.increment2),
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                trace ('select');
                    var title = ' ';//prompt('Reservation Name:');
                    //var players = prompt('Number of players:');

                    //ajax the info to the db

                    //if (title) {
                        $.ajax({
                           type: "get",
                           url: "index.php/api/bookteetime/title/"+title+"/start/"+getTimeString(start)+"/end/"+getTimeString(end)+"/allDay/"+allDay,
                           data: '',//"action=bookteetime&key=K_fuwgfifr11&title="+title+"&start="+getTimeString(start)+"&end="+getTimeString(end)+"&allDay="+allDay,
                           success: function(response){
                               if (response == 'logout')
                                   logout();
                               $('#calendar').fullCalendar('renderEvent',
                                    {
                                        id:response,
                                        title: title,
                                        name:title,
                                        start: start,
                                        end: end,
                                        allDay: allDay,
                                        details:'',
                                        player_count:'',
                                        holes:'',
                                        carts:'',
                                        clubs:'',
                                        initialized:false
                                    },
                                    true // make the event "stick"
                                );
                           }
                         });
                    //}
                    $('#calendar').fullCalendar('unselect');
                   //trace('finish select');
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                //$('.qtip').remove();
                var id = event.id;
                var newstart = getTimeString(event.start);
                var newend = getTimeString(event.end);
                $.ajax({
                   type: "get",
                   url: "index.php/api/moveteetime/id/"+id+"/start/"+newstart+"/end/"+newend+"/nine/front",
                   data: '',//"action=moveteetime&key=K_fuwgfifr11&id="+id+"&start="+newstart+'&end='+newend+'&nine=front',
                   success: function(response){
                       if (response == 'logout')
                           logout();
                       else if (calInfo.holes == 18){
                           var newbStart = parseInt(newstart) + parseInt(calInfo.frontnine);
                           if (newbStart % 100 > 59)
                               newbStart += 40;
                           var newbEnd = parseInt(newend) + parseInt(calInfo.frontnine);
                           if (newbEnd % 100 > 59)
                               newbEnd += 40;
                           //console.log('newStart '+newbStart);
                           var backEventArray = calendarback.fullCalendar('clientEvents', id+'b');
                           if (backEventArray[0] != undefined) {
                               var backEvent = backEventArray[0];
                               backEvent.start = getDateFromTimeString(newbStart);
                               backEvent.end = getDateFromTimeString(newbEnd);
                           
                               calendarback.fullCalendar('updateEvent', backEvent);
                           }
                           //console.log('getting to the end of move tee time');
                       }
                   }
                 });
            },
            timeFormat:{
                // for agendaWeek and agendaDay
                agenda: '', // 5:00 - 6:30

                // for all other views
                '': 'h:mmt'            // 7p
            },
            eventResize: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {
                //$('.qtip').remove();
                var id = event.id;
                var newstart = getTimeString(event.start);
                var newend = getTimeString(event.end);
                $.ajax({
                   type: "get",
                   url: "index.php/api/moveteetime/id/"+id+"/start/"+newstart+"/end/"+newend+"/nine/front",
                   data: '',//"action=moveteetime&key=K_fuwgfifr11&id="+id+"&start="+newstart+'&end='+newend+"&nine=front",
                   success: function(response){
                       //console.log('holes: '+calInfo.holes);
                       if (response == 'logout')
                           logout();
                       else if (calInfo.holes == 18){
                           var newbStart = parseInt(newstart) + parseInt(calInfo.frontnine);
                           if (newbStart % 100 > 59)
                               newbStart += 40;
                           var newbEnd = parseInt(newend) + parseInt(calInfo.frontnine);
                           if (newbEnd % 100 > 59)
                               newbEnd += 40;
                           
                           var backEventArray = calendarback.fullCalendar('clientEvents', id+'b');
                           if (backEventArray[0] != undefined) {
                               var backEvent = backEventArray[0];
                               backEvent.start = getDateFromTimeString(newbStart);
                               backEvent.end = getDateFromTimeString(newbEnd);
                           
                               calendarback.fullCalendar('updateEvent', backEvent);
                               //console.log('getting to the end of move tee time');
                           }
                       }
                   }
                 });
            },
            editable: true,
            events: calInfo.caldata,
            eventAfterRender: function(event, element, view) {
                if (view.name == 'agendaDay') {
                    var den = 252.5;
                    var quar = 63.125;
                    var ofl = 270;
                    if (calInfo.holes == 9) {
                        den = 570;
                        quar = 142.5;
                        ofl = 510;
                    }
                    var lmar = 65;
                    if (ttArray[event.start.getTime()] != '' && ttArray[event.start.getTime()] != undefined && ttArray[event.start.getTime()]['players'] != '' && ttArray[event.start.getTime()]['players'] != undefined)
                        lmar = (ttArray[event.start.getTime()]['players']*quar)+65;
                    else {
                        ttArray[event.start.getTime()] = {};
                        ttArray[event.start.getTime()]['players'] = 0;
                        ttArray[event.start.getTime()]['teetimes'] = 0;
                    }
                    //console.log('start: '+event.start.getTime()+' : '+ttArray[event.start.getTime()]['players']+' : '+lmar+' : '+event.title);
                    var width = den;
                    if (event.type == 'tournament') {
                        
                    }
                    else if (event.player_count == 1)
                        width = den*.25;
                    else if (event.player_count == 2)
                        width = den*.5;
                    else if (event.player_count == 3)
                        width = den*.75;
                    var pixels = $(element).css('width');
                    var top = $(element).css('top');
                    pixels = parseFloat(pixels.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
                    top = parseFloat(top.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
                    $(element).css('width',(width-2)+'px');
                    //console.log('w: '+width+' d: '+den+' lm: '+lmar);
                    $(element).css('left',(lmar)+'px');
                    if (width + lmar - 65 > den) {
                        lmar = ofl;
                        $(element).css('top',top+(ttArray[event.start.getTime()]['teetimes']*4)+'px');
                        $(element).css('left',(lmar)+(ttArray[event.start.getTime()]['teetimes']*8)+'px');
                        event.overflow = true;
                    }
                    else
                        event.overflow = false;
                    
                    ttArray[event.start.getTime()]['players'] += parseInt(event.player_count);
                    ttArray[event.start.getTime()]['teetimes']++;
                }
            },
            eventRender: function(event, element) {
                ttArray = {};
                var dow = '';
                if (event.start.getDay() == 0)
                    dow = 'Sun';
                else if (event.start.getDay() == 1)
                    dow = 'Mon';
                else if (event.start.getDay() == 2)
                    dow = 'Tue';
                else if (event.start.getDay() == 3)
                    dow = 'Wed';
                else if (event.start.getDay() == 4)
                    dow = 'Thu';
                else if (event.start.getDay() == 5)
                    dow = 'Fri';
                else if (event.start.getDay() == 6)
                    dow = 'Sat';

                var ampm = 'am';
                if (event.start.getHours() > 11)
                    ampm = 'pm';

                var hours = '';
                if (event.start.getHours() > 12)
                    hours = event.start.getHours() - 12;
                else
                    hours = event.start.getHours();

                var leadingZero = '';
                if (event.start.getMinutes() < 10)
                    leadingZero = '0';
                var holesMenu = '';
                var holesMenu2 = '';
                var holesHeader = '<td></td>';
                if (calInfo.holes != 18) {
                    holesMenu = "<td><select id='"+event.id+"_h' style='display:none'>"+
                                    "<option value='9'>9</option>"+
                                "</select></td>";
                    holesMenu2 = "<td><select id='"+event.id+"_ci_h' style='display:none'>"+
                                    "<option value='9'>9</option>"+
                                "</select></td>";
                }
                else {
                    holesHeader = "<td>Holes</td>";
                    holesMenu = "<td><select id='"+event.id+"_h'>"+
                                    "<option value='9'>9</option>"+
                                    "<option value='18' selected>18</option>"+
                                "</select></td>";
                    holesMenu2 = "<td><select id='"+event.id+"_ci_h'>"+
                                    "<option value='9'>9</option>"+
                                    "<option value='18' selected>18</option>"+
                                "</select></td>";
                }
                element.qtip({
                    eid:event.id,
                    content:{
                        text:"<table><tbody>"+
                                "<tr>"+
                                "<td colspan=5 style='text-align:center'><div id='"+event.id+"radioset' class='teebuttons'>"+
                                        "<input type='radio' value='teetime' id='"+event.id+"radio1' name='"+event.id+"_type' checked='checked'><label id='"+event.id+"teetime_label' for='"+event.id+"radio1' >Tee Time</label>"+
					"<input type='radio' value='tournament' id='"+event.id+"radio2' name='"+event.id+"_type'><label id='"+event.id+"tournament_label' for='"+event.id+"radio2'>Tournament</label>"+
					/*"<input type='radio' value='block' id='radio3' name='"+event.id+"_type' class='ui-helper-hidden-accessible'><label for='radio3' aria-pressed='false' class='ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right' role='button' aria-disabled='false'><span class='ui-button-text'>Block</span></label>"+*/
				"</div></td>"+
                                "</tr>"+
                                "<tr id='"+event.id+"_ci_r01' ><td>Name</td>"+holesHeader+"<td>Players</td><td>Carts</td><td>Clubs</td></tr>"+
                                "<tr id='"+event.id+"_ci_r02' ><td><input id='"+event.id+"_t' value='"+event.name+"'/></td>"+
                                holesMenu+
                                "<td><select id='"+event.id+"_g'>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_c'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                    "<option value='2'>2</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_cl'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                            "</tr>"+
                                "<tr id='"+event.id+"_ci_r0' style='display:none'><td class='cititle' colspan=4>Confirm Tee Time Details</td></tr>"+
                                "<tr id='"+event.id+"_ci_r1' style='display:none'>"+holesHeader+"<td>Players</td><td>Carts</td><td>Payment</td></tr>"+
                                "<tr id='"+event.id+"_ci_r2' class='cttd' style='display:none'>"+
                                holesMenu2+
                                "<td><select id='"+event.id+"_ci_g'>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_ci_c'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                    "<option value='2'>2</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_ci_p'>"+
                                    "<option value='card'>Card</option>"+
                                    "<option value='cash'>Cash</option>"+
                                    "<option value='special'>Special</option>"+
                                "</select></td>"+
                            "</tr>"+
                            "<tr><td colspan=5>Details</td></tr><tr><td colspan=5>"+
                            "<textarea id='"+event.id+"_d' style='width:375px;height:50px;'>"+event.details+"</textarea></td></tr>"+
                            "<tr><td colspan=5><span class='saveButtons'><button id='"+event.id+"_save'>Reserve</button><button id='"+event.id+"_checkin'>Walk In</button><button id='"+event.id+"_purchase'>Purchase</button></span><button id='"+event.id+"_delete' class='deletebutton'>Delete</button><button id='"+event.id+"_confirm' class='confirmButton' style='display:none'>Confirm</button><button id='"+event.id+"_cancel' style='display:none' class='cancelbutton'>Cancel</button><!--input id='"+event.id+"_save2' type=button value='Reserve'/> <input id='"+event.id+"_delete' type=button value='Delete'/> <input id='"+event.id+"_checkin' type=button value='Walk In'/> <input id='"+event.id+"_confirm' type=button value='Confirm' style='display:none'/> <input id='"+event.id+"_cancel' type=button value='Cancel' style='display:none'/--></td></tr></tbody></table>",
                        title:{
                            text:'Tee Time Details - '+event.name+' on '+dow+'. @ '+hours+':'+leadingZero+event.start.getMinutes()+ampm,
                            button:'Close'
                        }
                    },
                    show:'click',
                    hide:'unfocus',
                    style: { 
                          tip: {
                             corner: true,
                             method: 'polygonal'
                          },
                          width: 400,
                          height:200,
                          padding: 5,
                          classes: 'ui-tooltip-shadow', // Inherit from preset style
                          widget:true
                    },
                    position: {
                        my:'left center',
                        at:'centerMiddle',
                        target:element,
                        viewport: $(window),
                        //corner: { 
                        //}
                        //target: 'centerMiddle',
                        //tooltip: 'leftMiddle'
                        //},
                        adjust: {
                            method:'flip shift',
                            screen:true
                        }
                    },
                    //api:{
                    events:{
                        render: function(evt, api) {
                           //trace('on rendering');
                            //var api = this;
                            trace (event.title == '');
                            if (event.title == '' || event.title == ' '){
                                $('#'+event.id+"_save").text('Reserve');
                                $('#'+event.id+"_checkin").text('Walk In');
                            }
                            else {
                                $('#'+event.id+"_save").text('Update');
                                $('#'+event.id+"_checkin").text('Check In');
                            }
                            //$("input[name:'"+event.id+"_type'][value='"+event.type+"']").attr('checked', true);
                            //$('#radioset').buttonset('refresh');
                            $('#'+event.id+'teetime_label').click(function(){
                                $('#'+event.id+'_g').replaceWith($("<select id='"+event.id+"_g'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select>"));
                                $('#'+event.id+'_c').replaceWith($("<select id='"+event.id+"_c'><option value='0'>0</option><option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                "<option value='2'>2</option></select>"));
                                $('#'+event.id+'_ci_g').replaceWith($("<select id='"+event.id+"_ci_g'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select>"));
                                $('#'+event.id+'_ci_c').replaceWith($("<select id='"+event.id+"_ci_c'><option value='0'>0</option><option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                "<option value='2'>2</option></select>"));
                            });
                            $('#'+event.id+'tournament_label').click(function(){
                                $('#'+event.id+'_g').replaceWith($("<select id='"+event.id+"_g'>"+
                                    "<option value='5'>5</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option>"+
                                    "<option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "<option value='45'>45</option><option value='50'>50</option><option value='55'>55</option><option value='60'>60</option>"+
                                    "<option value='65'>65</option><option value='70'>70</option><option value='75'>75</option><option value='80'>80</option>"+
                                    "<option value='85'>85</option><option value='90'>90</option><option value='95'>95</option><option value='100'>100</option>"+
                                    "</select>"));
                                $('#'+event.id+'_ci_g').replaceWith($("<select id='"+event.id+"_ci_g'>"+
                                    "<option value='5'>5</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option>"+
                                    "<option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "<option value='45'>45</option><option value='50'>50</option><option value='55'>55</option><option value='60'>60</option>"+
                                    "<option value='65'>65</option><option value='70'>70</option><option value='75'>75</option><option value='80'>80</option>"+
                                    "<option value='85'>85</option><option value='90'>90</option><option value='95'>95</option><option value='100'>100</option>"+
                                    "</select>"));
                                $('#'+event.id+'_c').replaceWith($("<select id='"+event.id+"_c'>"+
                                    "<option value='0'>0</option><option value='5'>5</option><option value='10'>10</option>"+
                                    "<option value='15'>15</option><option value='20'>20</option><option value='25'>25</option>"+
                                    "<option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "</select>"));
                                $('#'+event.id+'_ci_c').replaceWith($("<select id='"+event.id+"_ci_c'>"+
                                    "<option value='0'>0</option><option value='5'>5</option><option value='10'>10</option>"+
                                    "<option value='15'>15</option><option value='20'>20</option><option value='25'>25</option>"+
                                    "<option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "</select>"));
                            });
                            
                            
                            $('#'+event.id+"_h").change(function(){
                                $('#'+event.id+'_ci_h').val($(this).val());
                            });
                            $('#'+event.id+"_c").change(function(){
                                $('#'+event.id+'_ci_c').val($(this).val());
                            });
                            $('#'+event.id+"_g").change(function(){
                                $('#'+event.id+'_ci_g').val($(this).val());
                            });

                            $('#'+event.id+"_save").button({
                                icons:{
                                    primary:'ui-icon-clock'
                                }
                            }).next().parent().buttonset();
                            $('#'+event.id+"_delete").button({
                                icons:{
                                    primary:'ui-icon-close'
                                }
                            });
                            $('#'+event.id+"_checkin").button({
                                icons:{
                                    primary:'ui-icon-check'
                                }
                            });
                            $('#'+event.id+"_purchase").button({
                                icons:{
                                    primary:'ui-icon-cart'
                                }
                            });
                            $('#'+event.id+"_confirm").button({
                                icons:{
                                    primary:'ui-icon-check'
                                }
                            });
                            $('#'+event.id+"_cancel").button({
                                icons:{
                                    primary:'ui-icon-close'
                                }
                            });
                            $('.teebuttons').buttonset();
                            $('#'+event.id+"_save").click( function(evt){
                                var id = event.id;
                                var title = $('#'+id+'_t').val().escapeSpecialChars();
                                var ttype = $("input[@name:'"+id+"_type']:checked").val();
                                var players = $('#'+id+'_g').val();
                                var holes = $('#'+id+'_h').val();
                                var carts = $('#'+id+'_c').val();
                                var clubs = $('#'+id+'_cl').val();
                                var details = $('#'+id+'_d').val().escapeSpecialChars();
                                if (details == '')
                                    details = ' ';
                                var tooltip = api.elements.tooltip;
                                var start = getTimeString(event.start);
                                var end = getTimeString(event.end);
                                var backStart = parseInt(start)+parseInt(calInfo.frontnine);
                                var backEnd = parseInt(end)+parseInt(calInfo.frontnine);
                                if (backStart%100>59)
                                    backStart+=40;
                                if (backEnd%100>59)
                                    backEnd+=40;
                                //console.log('s: '+backStart+' e: '+backEnd);
                                if (title == '' || title == ' ')
                                    alert('Please enter a name for the reservation.');
                                else
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/saveteetimedetails/id/"+id+"/title/"+title+"/players/"+players+"/holes/"+holes+"/clubs/"+clubs+"/carts/"+carts+"/details/"+details+"/start/"+backStart+"/end/"+backEnd+"/nine/front/type/"+ttype,
                                       data: '',//"action=saveteetimedetails&key=K_fuwgfifr11&id="+id+"&title="+title+"&players="+players+"&holes="+holes+"&clubs="+clubs+"&carts="+carts+"&details="+details+"&start="+backStart+"&end="+backEnd+"&nine=front&type="+ttype,
                                       success: function(response){
                                           if (response == 'logout')
                                               logout();
                                           //Open modal qtip with edit options.
                                           event.title = title;
                                           event.name = title;
                                           event.player_count = players;
                                           event.holes = holes;
                                           event.carts = carts;
                                           event.clubs = clubs;
                                           event.details = details;
                                           event.type = ttype;
                                           if (event.type == 'tournament') {
                                               event.backgroundColor = '#5f9b5f';
                                               event.borderColor = '#336133';
                                           }
                                           
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt);}
                                           calendar.fullCalendar('updateEvent', event);
                                           if (calInfo.holes == 18 && holes == 18) {
                                               //console.log('creating new back event');
                                               var existingEvent = calendarback.fullCalendar('clientEvents', event.id+'b');
                                               var bEvent = '';
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   bEvent = existingEvent[0];
                                               else
                                                   bEvent = {};
                                               bEvent.id = event.id+'b';
                                               bEvent.start = getDateFromTimeString(backStart);
                                               bEvent.end = getDateFromTimeString(backEnd);
                                               bEvent.allDay = false;
                                               bEvent.title = title;
                                               bEvent.name = title;
                                               bEvent.player_count = players;
                                               bEvent.holes = holes;
                                               bEvent.carts = carts;
                                               bEvent.clubs = clubs;
                                               bEvent.details = details;
                                               bEvent.type = ttype;
                                               if (ttype == 'tournament') {
                                                   bEvent.backgroundColor = '#5f9b5f';
                                                   bEvent.borderColor = '#336133';
                                               }
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   if (holes == 9)
                                                       calendarback.fullCalendar('removeEvents', id+'b');
                                                   else
                                                       calendarback.fullCalendar('updateEvent', bEvent);
                                               else
                                                   calendarback.fullCalendar('renderEvent', bEvent);
                                           }
                                           buildStats(null);
//                                               calendar.fullCalendar('refetchEvents');
                                        }
                                     });
                            });
                            $('#'+event.id+"_delete").click( function(evt){
                                var answer = confirm("Delete reservation for "+event.name);
                                if (answer){
                                    var id = event.id;
                                    var tooltip = api.elements.tooltip;
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/deleteteetime/id/"+id,
                                       data: '',//"action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                       success: function(response){
                                          //trace('after delete');
                                           if (response == 'logout')
                                               logout();
                                           //Open modal qtip with edit options.
                                           //$('#'+event.id+"_delete").unbind('click');
                                           calendar.fullCalendar( 'removeEvents', id);
                                           calendarback.fullCalendar( 'removeEvents', id+'b');
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt); api.destroy(evt);}
                                          //trace('done after delete');
                                           buildStats(null);
                                        }
                                     });
                                }
                                else{

                                }
                            });
                            $('#'+event.id+"_checkin").click( function(evt){
                                var id = event.id;
                                var tooltip = api.elements.tooltip;
                                $('#'+event.id+"_t").attr('disabled', true);
                                $('#'+event.id+"_g").attr('disabled', true);
                                $('#'+event.id+"_c").attr('disabled', true);
                                $('#'+event.id+"_h").attr('disabled', true);
                                $('#'+event.id+"_cl").attr('disabled', true);
                                $('#'+event.id+"_save").hide();
                                $('#'+event.id+"_delete").hide();
                                $('#'+event.id+"_checkin").hide();
                                $('#'+event.id+"_ci_r0").show();
                                $('#'+event.id+"_ci_r1").show();
                                $('#'+event.id+"_ci_r2").show();
                                $('#'+event.id+"_ci_r01").hide();
                                $('#'+event.id+"_ci_r02").hide();
                                $('#'+event.id+"_confirm").show();
                                $('#'+event.id+"_cancel").show();
                                
                                /*$.ajax({
                                   type: "POST",
                                   url: "api.php",
                                   data: "action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                   success: function(response){
                                       trace('after delete');
                                       if (response == 'logout')
                                           logout();
                                       //Open modal qtip with edit options.
                                       //$('#'+event.id+"_delete").unbind('click');
                                       calendar.fullCalendar( 'removeEvents', id);
                                       if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt); api.destroy(evt);}
                                       trace('done after delete');
                                    }
                                 });*/
                            });
                            $('#'+event.id+"_purchase").click( function(evt){
                                window.location = 'index.php/api/item/item_num/2';
/*                                var id = event.id;
                                $.ajax({
                                   type: "POST",
                                   url: "api.php",
                                   data: "action=purchase&key=K_fuwgfifr11&id="+id,
                                   success: function(response){
                                      window.location = 'http://localhost:8888/pos/index.php/sales';
                                    }
                                 });*/
                            });
                            $('#'+event.id+"_cancel").click( function(evt){
                                var id = event.id;
                                var tooltip = api.elements.tooltip;
                                $('#'+event.id+"_t").attr('disabled', false);
                                $('#'+event.id+"_g").attr('disabled', false);
                                $('#'+event.id+"_c").attr('disabled', false);
                                $('#'+event.id+"_h").attr('disabled', false);
                                $('#'+event.id+"_cl").attr('disabled', false);
                                $('#'+event.id+"_save").show();
                                $('#'+event.id+"_delete").show();
                                $('#'+event.id+"_checkin").show();
                                $('#'+event.id+"_ci_r0").hide();
                                $('#'+event.id+"_ci_r1").hide();
                                $('#'+event.id+"_ci_r2").hide();
                                $('#'+event.id+"_ci_r01").show();
                                $('#'+event.id+"_ci_r02").show();
                                $('#'+event.id+"_confirm").hide();
                                $('#'+event.id+"_cancel").hide();
                                /*$.ajax({
                                   type: "POST",
                                   url: "api.php",
                                   data: "action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                   success: function(response){
                                       trace('after delete');
                                       if (response == 'logout')
                                           logout();
                                       //Open modal qtip with edit options.
                                       //$('#'+event.id+"_delete").unbind('click');
                                       calendar.fullCalendar( 'removeEvents', id);
                                       if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt); api.destroy(evt);}
                                       trace('done after delete');
                                    }
                                 });*/
                            });
                            $('#'+event.id+"_confirm").click( function(evt){
                                var id = event.id;
                                var title = $('#'+id+'_t').val();
                                var ttype = $("input[@name:'"+id+"_type']:checked").val();
                                var players = $('#'+id+'_g').val();
                                var holes = $('#'+id+'_h').val();
                                var carts = $('#'+id+'_c').val();
                                var clubs = $('#'+id+'_cl').val();
                                var cplayers = $('#'+id+'_ci_g').val();
                                var choles = $('#'+id+'_ci_h').val();
                                var ccarts = $('#'+id+'_ci_c').val();
                                var cpayment = $('#'+id+'_ci_p').val();
                                var details = $('#'+id+'_d').val().escapeSpecialChars();
                                var tooltip = api.elements.tooltip;
                                var status = 'checked in';
                                var start = getTimeString(event.start);
                                var end = getTimeString(event.end);
                                var backStart = parseInt(start)+parseInt(calInfo.frontnine);
                                var backEnd = parseInt(end)+parseInt(calInfo.frontnine);
                                if (backStart%100>59)
                                    backStart+=40;
                                if (backEnd%100>59)
                                    backEnd+=40;
                                
                                
                                if ($('#'+event.id+'_checkin').text() == 'Walk In')
                                    status = 'walked in';
                                if (title == '') {
                                    alert('Please enter a name for the reservation.');
                                    $('#'+event.id+"_cancel").click();
                                }
                                else
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/savecheckindetails/id/"+id+"/cplayers/"+cplayers+"/choles/"+choles+"/ccarts/"+ccarts+"/cpayment/"+cpayment+"/details/"+details+"/title/"+title+"/players/"+players+"/holes/"+holes+"/carts/"+carts+"/clubs/"+clubs+"/status/"+status+"/nine/front/start/"+backStart+"/end/"+backEnd+"/type/"+ttype,
                                       data: '',//"action=savecheckindetails&key=K_fuwgfifr11&id="+id+"&cplayers="+cplayers+"&choles="+choles+"&ccarts="+ccarts+"&cpayment="+cpayment+"&details="+details+"&title="+title+"&players="+players+"&holes="+holes+"&carts="+carts+"&clubs="+clubs+"&status="+status+"&nine=front&start="+backStart+"&end="+backEnd+"&type="+ttype,
                                       success: function(response){
                                           if (response == 'logout')
                                               logout();
                                           //Open modal qtip with edit options.
                                           event.title = title;
                                           event.cplayer_count = cplayers;
                                           event.player_count = cplayers;
                                           event.holes = choles;
                                           event.choles = choles;
                                           event.carts = ccarts;
                                           event.ccarts = ccarts;
                                           event.cpayment = cpayment;
                                           event.details = details;
                                           event.backgroundColor = '#5c9ccc';
                                           event.borderColor = '#4297d7';
                                           event.status = status;
                                           event.type = ttype;
                                           if (event.carts > 0) {
                                               event.borderColor = '#E17009';
                                           }
                                           //console.dir(api);
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt);}
                                               calendar.fullCalendar('updateEvent', event);
                                           //console.log('holes: '+calInfo.holes)
                                           if (calInfo.holes == 18 && holes == 18) {
                                               //console.log('creating new back event');
                                               var existingEvent = calendarback.fullCalendar('clientEvents', event.id+'b');
                                               var bEvent = '';
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   bEvent = existingEvent[0];
                                               else
                                                   bEvent = {};
                                               bEvent.id = event.id+'b';
                                               bEvent.start = getDateFromTimeString(backStart);
                                               bEvent.end = getDateFromTimeString(backEnd);
                                               bEvent.allDay = false;
                                               bEvent.title = title;
                                               bEvent.name = title;
                                               bEvent.player_count = cplayers;
                                               bEvent.cplayer_count = cplayers;
                                               bEvent.holes = holes;
                                               bEvent.choles = holes;
                                               bEvent.carts = carts;
                                               bEvent.ccarts = carts;
                                               bEvent.clubs = clubs;
                                               bEvent.details = details;
                                               bEvent.type = ttype;
                                               bEvent.status = status;
                                               bEvent.backgroundColor = '#5c9ccc';
                                               bEvent.borderColor = '#4297d7';
                                           
                                               if (ttype == 'tournament') {
                                                   bEvent.backgroundColor = '#5c9ccc';
                                                   bEvent.borderColor = '#4297d7';
                                               }
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   if (holes == 9)
                                                       calendarback.fullCalendar('removeEvents', id+'b');
                                                   else
                                                       calendarback.fullCalendar('updateEvent', bEvent);
                                               else
                                                   calendarback.fullCalendar('renderEvent', bEvent);
                                           }
                                           buildStats(null);
                                        }
                                    });
                            });
                        },
                        show:function(){
                           //trace('onshow');
                            // Rerendering event throws off the qtip
                            // event.backgroundColor = '#CAE4ff';
                            // calendar.fullCalendar('updateEvent', event);
                            var holes = 18;
                            var choles = 18;
                            if (event.holes != 0)
                                holes = event.holes;
                            if (event.choles != 0)
                                choles = event.choles;
                            $("#"+event.id+event.type+"_label").click();
                            $('#'+event.id+"_g").val(event.player_count);
                            $('#'+event.id+"_h").val(holes);
                            $('#'+event.id+"_c").val(event.carts);
                            $('#'+event.id+"_cl").val(event.clubs);
                            $('#'+event.id+"_t").focus().select();
                            if (event.status == 'checked in' || event.status == 'walked in') {
                                $('#'+event.id+"_ci_g").val(event.cplayer_count);
                                $('#'+event.id+"_ci_h").val(event.choles);
                                $('#'+event.id+"_ci_c").val(event.ccarts);
                                $('#'+event.id+"_ci_p").val(event.cpayment);
                            }
                            else {
                                $('#'+event.id+"_ci_g").val(event.player_count);
                                $('#'+event.id+"_ci_h").val(holes);
                                $('#'+event.id+"_ci_c").val(event.carts);
                            }
                           //trace('#'+event.id+"_t");
                            /*$('#'+event.id+"_d").keypress(function(e){
                                var code = (e.keyCode ? e.keyCode : e.which);
                                if(code == 13) { //Enter keycode
                                    //Do something
                                    $('#'+event.id+"_save").click();
                                }
                            });*/
                            $('#'+event.id+"_t").keypress(function(e){
                                var code = (e.keyCode ? e.keyCode : e.which);
                                if(code == 13) { //Enter keycode
                                    //Do something
                                    $('#'+event.id+"_save").click();
                                }
                            });
                           //trace('finish onshow');
                        },
                        visible:function() {
                           //trace('about to focus');
                            $('#'+event.id+"_t").focus().select();
                           //trace('focused');
                        },
                        hide:function() {
                           //trace('onhide');
                            if (event.name == '' || event.name == ' ') {
                                var id = event.id;
                                $.ajax({
                                   type: "get",
                                   url: "index.php/api/deleteteetime/id/"+id,
                                   data: '',//"action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                   success: function(response){
                                       if (response == 'logout')
                                           logout();
                                       //Open modal qtip with edit options.
                                       calendar.fullCalendar( 'removeEvents', id);
                                   }
                                 }); 
                             }
                            //trace('finishonhide');
                        }
                    }
                });
                if (event.initialized == false) {
                    //$('.qtip').remove();
                    //trace('adding qtip in event render');
                    event.initialized = true;
                    element.click();
                }
            },
            eventColor:'#667873',
            viewDisplay: function(view) {
                if (calInfo.holes == 18) {
                    if (view.name == 'agendaDay')
                        showBackNine(view);
                    else if (view.name == 'agendaWeek')
                        hideBackNine(view);
                    
                    $(".calScroller").scroll(function(e) {
                        if (e.currentTarget) {
                            $(".calScroller").scrollTop(e.currentTarget.scrollTop);
                        }
                    });
                    $("#calendar table.fc-header").css('width', '700px');
                }
                buildStats(view);
            }
    });
    calendarback = $('#calendarback').fullCalendar({
            theme:true,
            header: {
                left: '',
                center: '',
                right: ''
            },
            aspectRatio:.671,
            allDaySlot:false,
            defaultView:'agendaDay',
            minTime:calInfo.openhour,
            maxTime:calInfo.closehour,
            slotMinutes:parseInt(calInfo.increment1),
            defaultEventMinutes:parseInt(calInfo.increment2),
            selectable: true,
            selectHelper: true,
            timeFormat:{
                // for agendaWeek and agendaDay
                agenda: '', // 5:00 - 6:30

                // for all other views
                '': 'h:mmt'            // 7p
            },
            select: function(start, end, allDay) {
                trace ('select');
                    var title = '';//prompt('Reservation Name:');
                    //var players = prompt('Number of players:');

                    //ajax the info to the db

                    //if (title) {
                        $.ajax({
                           type: "get",
                           url: "index.php/api/bookteetime/title/"+title+"/start/"+getTimeString(start)+"/end/"+getTimeString(end)+"/allDay/"+allDay+"/nine/back",
                           data: '',//"action=bookteetime&key=K_fuwgfifr11&title="+title+"&start="+getTimeString(start)+"&end="+getTimeString(end)+"&allDay="+allDay+"&nine=back",
                           success: function(response){
                               if (response == 'logout')
                                   logout();
                               $('#calendarback').fullCalendar('renderEvent',
                                    {
                                        id:response,
                                        title: title,
                                        name:title,
                                        start: start,
                                        end: end,
                                        allDay: allDay,
                                        details:'',
                                        player_count:'',
                                        holes:'',
                                        carts:'',
                                        clubs:'',
                                        initialized:false
                                    },
                                    true // make the event "stick"
                                );
                           }
                         });
                    //}
                    $('#calendarback').fullCalendar('unselect');
                   //trace('finish select');
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                //$('.qtip').remove();
                var id = event.id;
                var newstart = getTimeString(event.start);
                var newend = getTimeString(event.end);
                $.ajax({
                   type: "get",
                   url: "index.php/api/moveteetime/id/"+id+"/start/"+newstart+"/end/"+newend,
                   data: '',//"action=moveteetime&key=K_fuwgfifr11&id="+id+"&start="+newstart+'&end='+newend,
                   success: function(response){
                       if (response == 'logout')
                           logout();
                   }
                 });
            },
            eventResize: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {
                //$('.qtip').remove();
                var id = event.id;
                var newstart = getTimeString(event.start);
                var newend = getTimeString(event.end);
                $.ajax({
                   type: "get",
                   url: "index.php/api/moveteetime/id/"+id+"/start/"+newstart+"/end/"+newend,
                   data: '',//"action=moveteetime&key=K_fuwgfifr11&id="+id+"&start="+newstart+'&end='+newend,
                   success: function(response){
                       if (response == 'logout')
                           logout();
                   }
                 });
            },
            editable: true,
            events: calInfo.bcaldata,
            eventAfterRender: function(event, element, view) {
                if (view.name == 'agendaDay') {
                    var den = 252.5;
                    var quar = 63.125;
                    var ofl = 270;
                    if (calInfo.holes == 9) {
                        den = 570;
                        quar = 142.5;
                        ofl = 510;
                    }
                    var lmar = 65;
                    if (ttArray2[event.start.getTime()] != '' && ttArray2[event.start.getTime()] != undefined && ttArray2[event.start.getTime()]['players'] != '' && ttArray2[event.start.getTime()]['players'] != undefined)
                        lmar = (ttArray2[event.start.getTime()]['players']*quar)+65;
                    else {
                        ttArray2[event.start.getTime()] = {};
                        ttArray2[event.start.getTime()]['players'] = 0;
                        ttArray2[event.start.getTime()]['teetimes'] = 0;
                    }
                    //console.log('start: '+event.start.getTime()+' : '+ttArray2[event.start.getTime()]['players']+' : '+lmar+' : '+event.title);
                    var width = den;
                    if (event.type == 'tournament') {
                        
                    }
                    else if (event.player_count == 1)
                        width = den*.25;
                    else if (event.player_count == 2)
                        width = den*.5;
                    else if (event.player_count == 3)
                        width = den*.75;
                    var pixels = $(element).css('width');
                    var top = $(element).css('top');
                    pixels = parseFloat(pixels.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
                    top = parseFloat(top.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
                    $(element).css('width',(width-2)+'px');
                    //console.log('w: '+width+' d: '+den+' lm: '+lmar);
                    $(element).css('left',(lmar)+'px');
                    if (width + lmar - 65 > den) {
                        lmar = ofl;
                        $(element).css('top',top+(ttArray2[event.start.getTime()]['teetimes']*4)+'px');
                        $(element).css('left',(lmar)+(ttArray2[event.start.getTime()]['teetimes']*8)+'px');
                        event.overflow = true;
                    }
                    else
                        event.overflow = false;
                    
                    ttArray2[event.start.getTime()]['players'] += parseInt(event.player_count);
                    ttArray2[event.start.getTime()]['teetimes']++;
                }
            },
            eventRender: function(event, element) {
                ttArray2 = {};
                var dow = '';
                if (event.start.getDay() == 0)
                    dow = 'Sun';
                else if (event.start.getDay() == 1)
                    dow = 'Mon';
                else if (event.start.getDay() == 2)
                    dow = 'Tue';
                else if (event.start.getDay() == 3)
                    dow = 'Wed';
                else if (event.start.getDay() == 4)
                    dow = 'Thu';
                else if (event.start.getDay() == 5)
                    dow = 'Fri';
                else if (event.start.getDay() == 6)
                    dow = 'Sat';

                var ampm = 'am';
                if (event.start.getHours() > 11)
                    ampm = 'pm';

                var hours = '';
                if (event.start.getHours() > 12)
                    hours = event.start.getHours() - 12;
                else
                    hours = event.start.getHours();

                var leadingZero = '';
                if (event.start.getMinutes() < 10)
                    leadingZero = '0';

                element.qtip({
                    eid:event.id,
                    content:{
                        text:"<table><tbody>"+
                                "<tr>"+
                                "<td colspan=5 style='text-align:center'><div id='"+event.id+"radioset' class='teebuttons'>"+
					"<input type='radio' value='teetime' id='"+event.id+"radio1' name='"+event.id+"_type' checked='checked'><label id='"+event.id+"teetime_label' for='"+event.id+"radio1'>Tee Time</label>"+
					"<input type='radio' value='tournament' id='"+event.id+"radio2' name='"+event.id+"_type'><label id='"+event.id+"tournament_label' for='"+event.id+"radio2'>Tournament</label>"+
					/*"<input type='radio' value='block' id='radio3' name='"+event.id+"_type' class='ui-helper-hidden-accessible'><label for='radio3' aria-pressed='false' class='ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right' role='button' aria-disabled='false'><span class='ui-button-text'>Block</span></label>"+*/
				"</div></td>"+
                                "</tr>"+
                                "<tr id='"+event.id+"_ci_r01' ><td>Name</td><td></td><td>Players</td><td>Carts</td><td>Clubs</td></tr>"+
                                "<tr id='"+event.id+"_ci_r02' ><td><input id='"+event.id+"_t' value='"+event.name+"'/></td>"+
                                "<td><select id='"+event.id+"_h' style='display:none'>"+
                                    "<option value='9' selected>9</option>"+
                                    "<option value='18'>18</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_g'>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_c'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                    "<option value='2'>2</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_cl'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                            "</tr>"+
                                "<tr id='"+event.id+"_ci_r0' style='display:none'><td class='cititle' colspan=4>Confirm Tee Time Details</td></tr>"+
                                "<tr id='"+event.id+"_ci_r1' style='display:none'><td></td><td>Players</td><td>Carts</td><td>Payment</td></tr>"+
                                "<tr id='"+event.id+"_ci_r2' class='cttd' style='display:none'>"+
                                "<td><select id='"+event.id+"_ci_h' style='display:none'>"+
                                    "<option value='9' selected>9</option>"+
                                    "<option value='18'>18</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_ci_g'>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='2'>2</option>"+
                                    "<option value='3'>3</option>"+
                                    "<option value='4'>4</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_ci_c'>"+
                                    "<option value='0'>0</option>"+
                                    "<option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                    "<option value='2'>2</option>"+
                                "</select></td>"+
                                "<td><select id='"+event.id+"_ci_p'>"+
                                    "<option value='card'>Card</option>"+
                                    "<option value='cash'>Cash</option>"+
                                    "<option value='special'>Special</option>"+
                                "</select></td>"+
                            "</tr>"+
                            "<tr><td colspan=5>Details</td></tr><tr><td colspan=5>"+
                            "<textarea id='"+event.id+"_d' style='width:375px;height:50px;'>"+event.details+"</textarea></td></tr>"+
                            "<tr><td colspan=5><span class='saveButtons'><button id='"+event.id+"_save'>Reserve</button><button id='"+event.id+"_checkin'>Walk In</button></span><button id='"+event.id+"_delete' class='deletebutton'>Delete</button><button id='"+event.id+"_confirm' class='confirmButton' style='display:none'>Confirm</button><button id='"+event.id+"_cancel' style='display:none' class='cancelbutton'>Cancel</button><!--input id='"+event.id+"_save2' type=button value='Reserve'/> <input id='"+event.id+"_delete' type=button value='Delete'/> <input id='"+event.id+"_checkin' type=button value='Walk In'/> <input id='"+event.id+"_confirm' type=button value='Confirm' style='display:none'/> <input id='"+event.id+"_cancel' type=button value='Cancel' style='display:none'/--></td></tr></tbody></table>",
                        title:{
                            text:'Tee Time Details - '+event.name+' on '+dow+'. @ '+hours+':'+leadingZero+event.start.getMinutes()+ampm,
                            button:'Close'
                        }
                    },
                    show:'click',
                    hide:'unfocus',
                    style: { 
                          tip: {
                             corner: true,
                             method: 'polygonal'
                          },
                          width: 400,
                          height:200,
                          padding: 5,
                          classes: 'ui-tooltip-shadow', // Inherit from preset style
                          widget:true
                    },
                    position: {
                        my:'left center',
                        at:'centerMiddle',
                        target:element,
                        viewport: $(window),
                        //corner: { 
                        //}
                        //target: 'centerMiddle',
                        //tooltip: 'leftMiddle'
                        //},
                        adjust: {
                            method:'flip shift',
                            screen:true
                        }
                    },
                    //api:{
                    events:{
                        render: function(evt, api) {
                           //trace('on rendering');
                            //var api = this;
                            trace (event.title == '');
                            if (event.title == ''){

                            }
                            else {
                                $('#'+event.id+"_save").text('Update');
                                $('#'+event.id+"_checkin").text('Check In');
                            }
                            $('#'+event.id+'teetime_label').click(function(){
                                $('#'+event.id+'_g').replaceWith($("<select id='"+event.id+"_g'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select>"));
                                $('#'+event.id+'_c').replaceWith($("<select id='"+event.id+"_c'><option value='0'>0</option><option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                "<option value='2'>2</option></select>"));
                                $('#'+event.id+'_ci_g').replaceWith($("<select id='"+event.id+"_ci_g'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select>"));
                                $('#'+event.id+'_ci_c').replaceWith($("<select id='"+event.id+"_ci_c'><option value='0'>0</option><option value='.5'>.5</option>"+
                                    "<option value='1'>1</option>"+
                                    "<option value='1.5'>1.5</option>"+
                                "<option value='2'>2</option></select>"));
                            });
                            $('#'+event.id+'tournament_label').click(function(){
                                $('#'+event.id+'_g').replaceWith($("<select id='"+event.id+"_g'>"+
                                    "<option value='5'>5</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option>"+
                                    "<option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "<option value='45'>45</option><option value='50'>50</option><option value='55'>55</option><option value='60'>60</option>"+
                                    "<option value='65'>65</option><option value='70'>70</option><option value='75'>75</option><option value='80'>80</option>"+
                                    "<option value='85'>85</option><option value='90'>90</option><option value='95'>95</option><option value='100'>100</option>"+
                                    "</select>"));
                                $('#'+event.id+'_ci_g').replaceWith($("<select id='"+event.id+"_ci_g'>"+
                                    "<option value='5'>5</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option>"+
                                    "<option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "<option value='45'>45</option><option value='50'>50</option><option value='55'>55</option><option value='60'>60</option>"+
                                    "<option value='65'>65</option><option value='70'>70</option><option value='75'>75</option><option value='80'>80</option>"+
                                    "<option value='85'>85</option><option value='90'>90</option><option value='95'>95</option><option value='100'>100</option>"+
                                    "</select>"));
                                $('#'+event.id+'_c').replaceWith($("<select id='"+event.id+"_c'>"+
                                    "<option value='0'>0</option><option value='5'>5</option><option value='10'>10</option>"+
                                    "<option value='15'>15</option><option value='20'>20</option><option value='25'>25</option>"+
                                    "<option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "</select>"));
                                $('#'+event.id+'_ci_c').replaceWith($("<select id='"+event.id+"_ci_c'>"+
                                    "<option value='0'>0</option><option value='5'>5</option><option value='10'>10</option>"+
                                    "<option value='15'>15</option><option value='20'>20</option><option value='25'>25</option>"+
                                    "<option value='30'>30</option><option value='35'>35</option><option value='40'>40</option>"+
                                    "</select>"));

                            });
                            
                            $('#'+event.id+"_h").change(function(){
                                $('#'+event.id+'_ci_h').val($(this).val());
                            });
                            $('#'+event.id+"_c").change(function(){
                                $('#'+event.id+'_ci_c').val($(this).val());
                            });
                            $('#'+event.id+"_g").change(function(){
                                $('#'+event.id+'_ci_g').val($(this).val());
                            });

                            $('#'+event.id+"_save").button({
                                icons:{
                                    primary:'ui-icon-clock'
                                }
                            }).next().parent().buttonset();
                            $('#'+event.id+"_delete").button({
                                icons:{
                                    primary:'ui-icon-close'
                                }
                            });
                            $('#'+event.id+"_checkin").button({
                                icons:{
                                    primary:'ui-icon-check'
                                }
                            });
                            $('#'+event.id+"_confirm").button({
                                icons:{
                                    primary:'ui-icon-check'
                                }
                            });
                            $('#'+event.id+"_cancel").button({
                                icons:{
                                    primary:'ui-icon-close'
                                }
                            });
                            $('.teebuttons').buttonset();
                            $('#'+event.id+"_save").click( function(evt){
                                var id = event.id;
                                var title = $('#'+id+'_t').val().escapeSpecialChars();
                                var players = $('#'+id+'_g').val();
                                var holes = $('#'+id+'_h').val();
                                if (id.length < 21)
                                    holes = 9;
                                var carts = $('#'+id+'_c').val();
                                var clubs = $('#'+id+'_cl').val();
                                var details = $('#'+id+'_d').val().escapeSpecialChars();
                                if (details == '')
                                    details = ' ';
                                var tooltip = api.elements.tooltip;
                                var start = getTimeString(event.start);
                                var end = getTimeString(event.end);
                                var ttype = $("input[@name:'"+id+"_type']:checked").val();;
                                if (title == '')
                                    alert('Please enter a name for the reservation.');
                                else
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/saveteetimedetails/id/"+id+"/title/"+title+"/players/"+players+"/holes/"+holes+"/clubs/"+clubs+"/carts/"+carts+"/details/"+details+"/start/"+start+"/end/"+end+"/nine/"+back+"/type/"+ttype,
                                       data: '',//"action=saveteetimedetails&key=K_fuwgfifr11&id="+id+"&title="+title+"&players="+players+"&holes="+holes+"&clubs="+clubs+"&carts="+carts+"&details="+details+"&start="+start+"&end="+end+"&nine=back&type="+ttype,
                                       success: function(response){
                                           if (response == 'logout')
                                               logout();
                                           //Open modal qtip with edit options.
                                           event.title = title;
                                           event.name = title;
                                           event.player_count = players;
                                           event.holes = holes;
                                           event.carts = carts;
                                           event.clubs = clubs;
                                           event.details = details;
                                           event.type = ttype;
                                           //console.dir(api);
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt);}
                                           calendarback.fullCalendar('updateEvent', event);
                                           //console.log('rightbefore update front event');
                                           //console.log('holes: '+holes);
                                           
                                           if (holes == 18) {
                                               //console.log('creating new back event');
                                               var existingEvent = calendar.fullCalendar('clientEvents', event.id.slice(0,20));
                                               var bEvent = '';
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   bEvent = existingEvent[0];
                                               else
                                                   bEvent = {};
                                               bEvent.id = event.id.slice(0,20);
                                               bEvent.title = title;
                                               bEvent.name = title;
                                               bEvent.player_count = players;
                                               bEvent.carts = carts;
                                               bEvent.clubs = clubs;
                                               bEvent.details = details;
                                               bEvent.type = ttype;
                                               if (existingEvent != undefined && existingEvent[0] != undefined)
                                                   calendar.fullCalendar('updateEvent', bEvent);
                                               else
                                                   calendar.fullCalendar('renderEvent', bEvent);
                                           }
                                           buildStats(null);
                                        }
                                     });
                            });
                            $('#'+event.id+"_delete").click( function(evt){
                                var answer = confirm("Delete reservation for "+event.name);
                                if (answer){
                                    var id = event.id;
                                    var tooltip = api.elements.tooltip;
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/deleteteetime/id/"+id,
                                       data: '',//"action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                       success: function(response){
                                          //trace('after delete');
                                           if (response == 'logout')
                                               logout();
                                           //Open modal qtip with edit options.
                                           //$('#'+event.id+"_delete").unbind('click');
                                           calendarback.fullCalendar( 'removeEvents', id);
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt); api.destroy(evt);}
                                          //trace('done after delete');
                                           buildStats(null);
                                        }
                                     });
                                }
                                else{

                                }
                            });
                            $('#'+event.id+"_checkin").click( function(evt){
                                var id = event.id;
                                var tooltip = api.elements.tooltip;
                                $('#'+event.id+"_t").attr('disabled', true);
                                $('#'+event.id+"_g").attr('disabled', true);
                                $('#'+event.id+"_c").attr('disabled', true);
                                $('#'+event.id+"_h").attr('disabled', true);
                                $('#'+event.id+"_cl").attr('disabled', true);
                                $('#'+event.id+"_save").hide();
                                $('#'+event.id+"_delete").hide();
                                $('#'+event.id+"_checkin").hide();
                                $('#'+event.id+"_ci_r0").show();
                                $('#'+event.id+"_ci_r1").show();
                                $('#'+event.id+"_ci_r2").show();
                                $('#'+event.id+"_ci_r01").hide();
                                $('#'+event.id+"_ci_r02").hide();
                                $('#'+event.id+"_confirm").show();
                                $('#'+event.id+"_cancel").show();
                            });
                            $('#'+event.id+"_cancel").click( function(evt){
                                var id = event.id;
                                var tooltip = api.elements.tooltip;
                                $('#'+event.id+"_t").attr('disabled', false);
                                $('#'+event.id+"_g").attr('disabled', false);
                                $('#'+event.id+"_c").attr('disabled', false);
                                $('#'+event.id+"_h").attr('disabled', false);
                                $('#'+event.id+"_cl").attr('disabled', false);
                                $('#'+event.id+"_save").show();
                                $('#'+event.id+"_delete").show();
                                $('#'+event.id+"_checkin").show();
                                $('#'+event.id+"_ci_r0").hide();
                                $('#'+event.id+"_ci_r1").hide();
                                $('#'+event.id+"_ci_r2").hide();
                                $('#'+event.id+"_ci_r01").show();
                                $('#'+event.id+"_ci_r02").show();
                                $('#'+event.id+"_confirm").hide();
                                $('#'+event.id+"_cancel").hide();
                            });
                            $('#'+event.id+"_confirm").click( function(evt){
                                var id = event.id;
                                var title = $('#'+id+'_t').val();
                                var ttype = $("input[@name:'"+id+"_type']:checked").val();
                                var players = $('#'+id+'_g').val();
                                var holes = $('#'+id+'_h').val();
                                var carts = $('#'+id+'_c').val();
                                var clubs = $('#'+id+'_cl').val();
                                var cplayers = $('#'+id+'_ci_g').val();
                                var choles = $('#'+id+'_ci_h').val();
                                var ccarts = $('#'+id+'_ci_c').val();
                                var cpayment = $('#'+id+'_ci_p').val();
                                var details = $('#'+id+'_d').val().escapeSpecialChars();
                                var tooltip = api.elements.tooltip;
                                var status = 'checked in';
                                var start = getTimeString(event.start);
                                var end = getTimeString(event.end);
                                if ($('#'+event.id+'_checkin').text() == 'Walk In')
                                    status = 'walked in';
                                if (title == '') {
                                    alert('Please enter a name for the reservation.');
                                    $('#'+event.id+"_cancel").click();
                                }
                                else
                                    $.ajax({
                                       type: "get",
                                       url: "index.php/api/savecheckindetails/id/"+id+"/cplayers/"+cplayers+"/choles/"+choles+"/ccarts/"+ccarts+"/cpayments/"+cpayment+"/details/"+details+"/title/"+title+"/players/"+players+"/holes/"+holes+"/carts/"+carts+"/clubs/"+clubs+"/status/"+status+"/nine/back/type/"+ttype+"/start/"+start+"/end/"+end,
                                       data: '',//"action=savecheckindetails&key=K_fuwgfifr11&id="+id+"&cplayers="+cplayers+"&choles="+choles+"&ccarts="+ccarts+"&cpayment="+cpayment+"&details="+details+"&title="+title+"&players="+players+"&holes="+holes+"&carts="+carts+"&clubs="+clubs+"&status="+status+"&nine=back&type="+ttype+"&start="+start+"&end="+end,
                                       success: function(response){
                                           if (response == 'logout')
                                               logout();
                                           trace('9hole savecheckin: '+choles);
                                           //Open modal qtip with edit options.
                                           event.title = title;
                                           event.cplayer_count = cplayers;
                                           event.player_count = cplayers;
                                           event.choles = choles;
                                           event.holes = choles;
                                           event.ccarts = ccarts;
                                           event.cpayment = cpayment;
                                           event.details = details;
                                           event.backgroundColor = '#5c9ccc';
                                           event.borderColor = '#4297d7';
                                           event.status = 'checked in';
                                           if (event.carts > 0)
                                               event.borderColor = '#E17009';
                                           //console.dir(api);
                                           if (tooltip.is(':visible') && api && api.status && !api.status.disabled && $(evt.target).add(api.elements.target).length >1) {api.hide(evt);}
                                               calendarback.fullCalendar('updateEvent', event);
                                          //trace(event.id.length+' len');
                                          //trace(event.id.slice(0,20)+' slice');
                                           if (event.id.length == 21) {
                                               var frontEventArray = calendar.fullCalendar('clientEvents',event.id.slice(0,20));
                                               if (frontEventArray[0] != undefined) {
                                                   frontEventArray[0].backgroundColor = '#5c9ccc';
                                                   frontEventArray[0].borderColor = '#4297d7';
                                                   frontEventArray[0].title = title;
                                                   frontEventArray[0].cplayer_count = cplayers;
                                                   frontEventArray[0].player_count = cplayers;
                                                   frontEventArray[0].choles = choles;
                                                   frontEventArray[0].ccarts = ccarts;
                                                   frontEventArray[0].cpayment = cpayment;
                                                   frontEventArray[0].details = details;
                                                   frontEventArray[0].status = 'checked in';

                                                   calendar.fullCalendar('updateEvent', frontEventArray[0]);
                                               }
                                           }
                                           buildStats(null);
                                        }
                                    });
                            });
                        },
                        show:function(){
                           //trace('onshow');
                            // Rerendering event throws off the qtip
                            // event.backgroundColor = '#CAE4ff';
                            // calendar.fullCalendar('updateEvent', event);
                            var holes = 9;
                            var choles = 9;
                            if (event.holes != 0)
                                holes = event.holes;
                            if (event.choles != 0)
                                choles = event.choles;
                            $("#"+event.id+event.type+"_label").click();
                            $('#'+event.id+"_g").val(event.player_count);
                            $('#'+event.id+"_h").val(holes);
                            $('#'+event.id+"_c").val(event.carts);
                            $('#'+event.id+"_cl").val(event.clubs);
                            $('#'+event.id+"_t").focus().select();
                            if (event.status == 'checked in' || event.status == 'walked in') {
                                $('#'+event.id+"_ci_g").val(event.cplayer_count);
                                $('#'+event.id+"_ci_h").val(event.choles);
                                $('#'+event.id+"_ci_c").val(event.ccarts);
                                $('#'+event.id+"_ci_p").val(event.cpayment);
                            }
                            else {
                                $('#'+event.id+"_ci_g").val(event.player_count);
                                $('#'+event.id+"_ci_h").val(holes);
                                $('#'+event.id+"_ci_c").val(event.carts);
                            }
                           //trace('#'+event.id+"_t");
                            /*$('#'+event.id+"_d").keypress(function(e){
                                var code = (e.keyCode ? e.keyCode : e.which);
                                if(code == 13) { //Enter keycode
                                    //Do something
                                    $('#'+event.id+"_save").click();
                                }
                            });*/
                            $('#'+event.id+"_t").keypress(function(e){
                                var code = (e.keyCode ? e.keyCode : e.which);
                                if(code == 13) { //Enter keycode
                                    //Do something
                                    $('#'+event.id+"_save").click();
                                }
                            });
                           //trace('finish onshow');
                        },
                        visible:function() {
                           //trace('about to focus');
                            $('#'+event.id+"_t").focus().select();
                           //trace('focused');
                        },
                        hide:function() {
                           //trace('onhide');
                            if (event.name == '') {
                                var id = event.id;
                                $.ajax({
                                   type: "get",
                                   url: "index.php/api/deleteteetime/id/"+id,
                                   data: '',//"action=deleteteetime&key=K_fuwgfifr11&id="+id,
                                   success: function(response){
                                       if (response == 'logout')
                                           logout();
                                       //Open modal qtip with edit options.
                                       calendarback.fullCalendar( 'removeEvents', id);
                                   }
                                 }); 
                             }
                            //trace('finishonhide');
                        }
                    }
                });
                if (event.initialized == false) {
                    //$('.qtip').remove();
                    //trace('adding qtip in event render');
                    event.initialized = true;
                    element.click();
                }
            },
            eventColor:'#667873',
            viewDisplay: function(view) {
                $(".calScroller").scroll(function(e) {
                    if (e.currentTarget) {
                        $(".calScroller").scrollTop(e.currentTarget.scrollTop);
                    }
                });
                $('#calendarback th.fc-col0').html('Back');
            //    calendar.fullCalendar('today');
            }
    });
    //move stats into calendar
    buildStats(null);
    $('.fc-button-next').click(function(){
        calendarback.fullCalendar('next');
    });
    $('.fc-button-prev').click(function(){
        calendarback.fullCalendar('prev');
    });
    $('.fc-button-today').click(function(){
        calendarback.fullCalendar('today');
    });
    //console.log('closing qtips');
    //closeQtips();
    var goToDay = function(e){
        var datetext = $(e.target).text().slice(4);
        var year = calendar.fullCalendar('getDate').getFullYear();
        var slInd = datetext.indexOf('/');
        var month = datetext.slice(0,slInd)-1;
        var date = datetext.slice(slInd+1);
       //trace(month+' : '+date);

        calendar.fullCalendar('changeView', 'agendaDay');
        calendar.fullCalendar('gotoDate',year, month, date);
    }
    $('th.fc-sun').click(goToDay);
    $('th.fc-mon').click(goToDay);
    $('th.fc-tue').click(goToDay);
    $('th.fc-wed').click(goToDay);
    $('th.fc-thu').click(goToDay);
    $('th.fc-fri').click(goToDay);
    $('th.fc-sat').click(goToDay);
});
function showBackNine(viewObj){
    showingBackNine = true;
    var date = viewObj.start;
    //console.log('showingBackNine');
    if ($('#calendar').width() != 348) {
        $('#calendar').width(348);
        //$('#calendar').fullCalendar({aspectRatio:.671});
        $('#calendar').fullCalendar('rerenderEvents');
    }
    if (calendar != null) {
        if (viewObj.name == 'agendaWeek')
            $('#calendar').fullCalendar('option', 'aspectRatio', 1.35);
        else if (viewObj.name == 'agendaDay') {
            $('#calendar').fullCalendar('option', 'aspectRatio', .671);
        }
    }
    $("#calendarback").show();
    $('#calendarback').fullCalendar('render');
    $('#calendar th.fc-col0').html('Front');
    
}
function hideBackNine(viewObj){
    var date = viewObj.start;
    $('#calendar').width(700);
    $("#calendarback").hide();
    if (calendar != null) {
        if (viewObj.name == 'agendaWeek')
            $('#calendar').fullCalendar('option', 'aspectRatio', 1.35);
        else if (viewObj.name == 'agendaDay') {
            $('#calendar').fullCalendar('option', 'aspectRatio', .671);
        }
    }
}
function changeTeeSheet() {
    var teesheet_id = $('#teesheetMenu').val();
    $('#changets').click();
}
function buildWeather(viewObj) {
    //http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%3D'http%3A%2F%2Fxml.weather.yahoo.com%2Fforecastrss%2F59911_f.xml'&format=json&diagnostics=true&callback=cbfunc

    if (!viewObj){
        var view = calendar.fullCalendar('getView').name;
        var date = calendar.fullCalendar('getDate');
    }   
    else {
        var view = viewObj.name;
        var date = viewObj.start;
    }
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var dow = date.getDay();
    var zip = '84663';
    $.ajax({
       type: "POST",
       url: "api.php",
       data: "http://xml.weather.yahoo.com/forecastrss/"+zip+"_f.xml&key=4C1m7SLV34H6.YcOZ0QKmrcwzZC7xvGIha2tSkxYK1rCagGB2SIRO3Rrnask",
       success: function(response){
           //console.dir($.xml2json(response));
           if (view == 'agendaDay') {
               
               var html = '<table cellspacing="0" style="width:100%">'+
                    '<thead>'+
                    '<tr><th>Holes</th><th>Players</th><th>Carts</th></tr>'+
                    '<tr><th>9</th><td>'+cp9+'</td><td>'+cc9+'</td></tr>'+
                    '<tr><th>18</th><td>'+cp18+'</td><td>'+cc18+'</td></tr>'+
                    '<tr class="stattotals"><th>Total</th><td>'+(parseInt(cp9)+parseInt(cp18))+'</td><td>'+(parseInt(cc9)+parseInt(cc18))+'</td></tr>'+
                    '</thead>'+
                    '</table>'+
                    '<div class="statsNote">*These #\'s represent the golfers that actually played over the number reserved.</div>';

                $('#weatherBox').html(html);
           }
           else
               $('#weatherBox').hide();
       }
     });
}
function buildStats(viewObj) {
    if (!viewObj){
        var view = calendar.fullCalendar('getView').name;
        var date = calendar.fullCalendar('getDate');
    }   
    else {
        var view = viewObj.name;
        var date = viewObj.start;
    }
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var dow = date.getDay();
    $.ajax({
       type: "get",
       url: "index.php/api/getstats/view/"+view+"/year/"+year+"/month/"+month+"/day/"+day+"/dow/"+dow,
       data: "",
       success: function(response){
           if (view == 'agendaDay') {
               $('.stats').show();
               $('#weatherBox').tabs('option', 'selected', 0);
               result = $.parseJSON(response);
               var cp9 = 0, cc9 = 0, cp18 = 0, cc18 = 0;
               var tcp9 = 0, tcc9 = 0, tcp18 = 0, tcc18 = 0;

               if (result.nine) {
                   if (result.nine.cplayers != undefined)
                       cp9 = result.nine.cplayers;
                   if (result.nine.ccarts != undefined)
                       cc9 = result.nine.ccarts;
                   if (result.nine.players != undefined)
                       tcp9 = result.nine.players;
                   if (result.nine.carts != undefined)
                       tcc9 = result.nine.carts;
               }
               if (result.eighteen) {
                   if (result.eighteen.cplayers != undefined)
                       cp18 = result.eighteen.cplayers;
                   if (result.eighteen.ccarts != undefined)
                       cc18 = result.eighteen.ccarts;
                   if (result.eighteen.players != undefined)
                       tcp18 = result.eighteen.players;
                   if (result.eighteen.carts != undefined)
                       tcc18 = result.eighteen.carts;
               }
               var stats18 = '';
               if (calInfo.holes == 18) {
                   stats18 = '<tr><th>18</th><td>'+cp18+'/'+tcp18+'</td><td>'+cc18+'/'+tcc18+'</td></tr>';
               }
               var html = '<table cellspacing="0" style="width:100%">'+
                    '<thead>'+
                    '<tr><th>Holes</th><th>Players</th><th>Carts</th></tr>'+
                    '<tr><th>9</th><td>'+cp9+'/'+tcp9+'</td><td>'+cc9+'/'+tcc9+'</td></tr>'+
                    stats18+
                    '<tr class="stattotals"><th>Total</th><td>'+(parseInt(cp9)+parseInt(cp18))+'/'+(parseInt(tcp9)+parseInt(tcp18))+'</td><td>'+(parseInt(cc9)+parseInt(cc18))+'/'+(parseInt(tcc9)+parseInt(tcc18))+'</td></tr>'+
                    '</thead>'+
                    '</table>'+
                    '<div class="statsNote">*These #\'s represent only the golfers that actually played.</div>';

                $('#tabs-1').html(html);
           }
           else {
               $('.stats').hide();
               $('#weatherBox').tabs('option', 'selected', 1);
           }
       }
     });
}
function displayEventEditBox(id, element){
    $.ajax({
       type: "get",
       url: "index.php/api/getteetimedetails/id/"+id,
       data: '',//"action=getTeeTimeDetails&key=K_fuwgfifr11&id="+id,
       success: function(response){
           if (response == 'logout')
               logout();
           //Open modal qtip with edit options.

       }
     });
}
function getTimeString(date) {
    if (date == null)
        return null;
    var year = String(date.getFullYear());
    var month = String(date.getMonth());
    var day = String(date.getDate());
    var hour = String(date.getHours());
    var minutes = String(date.getMinutes());
    if (month.length == 1)
        month = '0'+String(month);
    if (day.length == 1)
        day = '0'+String(day);
    if (hour.length == 1)
        hour = '0'+String(hour);
    if (minutes.length == 1)
        minutes = '0'+String(minutes);

    return String(year)+String(month)+String(day)+String(hour)+String(minutes);
}
function getDateFromTimeString(tstring) {
    tstring = String(tstring);
    var year = tstring.slice(0, 4);
    var month = tstring.slice(4, 6);
    var day = tstring.slice(6, 8);
    var hour = tstring.slice(8, 10);
    var minutes = tstring.slice(10);

    return new Date(year, month, day, hour, minutes, 00, 00);
}
function saveTeeTimeDetails(id){

}
function removeEvent(id) {

}
String.prototype.escapeSpecialChars = function() {
    return this.replace(/\n/g, '')
               .replace(/\'/g, "")
               .replace(/\"/g, '');
               //.replace(/\&/g, "\\&")
               //.replace(/\r/g, "\\r")
               //.replace(/\t/g, "\\t")
               //.replace(/\b/g, "\\b")
               //.replace(/\f/g, "\\f");
};
function moveToDay(date){
	var toDate = new Date(date);
	//alert(toDate);
	$('#calendar').fullCalendar( 'changeView', 'agendaDay' );
	$('#calendar').fullCalendar( 'gotoDate', toDate );
}
function trace(traced){
    var type = typeof traced;
    if (type == 'number' || type == 'string' || type == 'boolean') 
        if (window.console && console.log)
            console.log(traced);
    else if (type == 'object') 
        if (window.console && console.dir)
            console.dir(traced);
    else if (type == null || type == 'undefined') 
        if (window.console && console.log)
            console.log('null or undefined');
}
function logout() {
    window.location = "/logout.php";
}
var customer = {
	delete_household_member: function(person_id){
		$('#member_row_'+person_id).remove();
	},
	add_household_member: function(person_id, name){
		var household_member_html = '<div class="field_row clearfix" id="member_row_'+person_id+'">'+
				'<label for="household_member_notice"><span onclick="customer.delete_household_member('+person_id+')">x</span></label>'+
				'<div class="form_field">'+
				name+					
				'<input type="hidden" id="household_member_id[]" name="household_member_id[]" value="'+person_id+'">'+
				'</div>'+
			'</div>';	
		$('#household_members').append(household_member_html);
		console.log('added member');
	},
	billing : {
		load_color_box: function(customer_id, customer_name){
			$.colorbox({'href':'index.php/customers/billing/'+customer_id+'/width~1200', 'title':"Member Billing <input id='customer_name' type='text' placeholder='customer' value='"+customer_name+"'/>", 
				onComplete:function(){
					customer.billing.initialize_customer_search();
				}
			});
		},
		initialize_customer_search: function () {
			if ($('#customer_name').val() === 'undefined')	$('#customer_name').val('');							
			$("#customer_name").click(function(e){e.preventDefault(); $(this).select();})
			$( "#customer_name" ).autocomplete({
				source: 'index.php/customers/customer_search/last_name',
				delay: 10,
				autoFocus: false,
				minLength: 0,
				select: function(event, ui)
				{					
					customer.billing.load_color_box(ui.item.value, ui.item.label);
				}
			});
		},		
		initialize_invoice_table_controls:function(){			
			$('.line_value','.line_item').blur(function(){				
				var new_value = 0;					
				
				new_value = parseFloat($(this).val());
				new_value = !isNaN(new_value) ? new_value : 0;			
				//make sure the value has two decimal places unless its the quantity								
				if (!$(this).hasClass('quantity')){
					$(this).val((new_value).toFixed(2));
				} 				
									
				customer.billing.update_invoice_totals();						
			});	
			
			$('.delete_row').click(function(){				
				customer.billing.remove_row(this);
				customer.billing.update_invoice_totals();
			});	
		},
		initialize_controls:function(){												
			$('#add_item').click(function(){
				customer.billing.add_row()
			});
			// $('input', '.editable').focus(function(){
	   				// console.log('clicked');
	   				// $(this).select();
	   			// });
			$('#add_customer_email').click(function(){
				var href = $(this).attr('data-href');				
				
				$.colorbox2({'href':href, 'title':'Add Email', 
					onComplete:function(){
						$('#new_email').select();
					}
				});
			});	
								
			$('#start_date').datepicker({dateFormat:'yy-mm-dd'});	
			$('#send_date').datepicker({dateFormat:'yy-mm-dd'});	
			$('#due_date').datepicker({dateFormat:'yy-mm-dd'});	
						
			customer.billing.initialize_customer_search();
			
			// $('#add_credit_card_link').unbind('click').click(function(){										
				// $('#invoice_form').submit();	
			// });
			$('#add_credit_card_link').colorbox2({'width':712,'height':610});
			
			$('#frequency_selector').unbind('change').change(function(){
				$('#frequency').val($(this).val());
				$('#month_range').toggle();
				$('#bill_month').toggle();
			});
			
			//disable second row options when necessary so that tabbing over doesn't take you to the second row
			if ($('#invoice_time_options', '#billing_info').hasClass('advanced')){
					$(':input', '#second_row_options').prop('disabled',false);
			} else {
				$(':input', '#second_row_options').prop('disabled',true);
			}			
					
			var submitting = false;
			$('#invoice_form').validate({
				submitHandler:function(form)
				{										
					var is_visible = true,
						billing_title = $('#billing_title').val();
						is_visible = $('#billing_title').is(':visible');
					if (!billing_title && is_visible){
						set_feedback('Billing Title is required','error_message',false);
						$('#billing_title').select();
						return;
					}					
					var missing_description;
					$('input[name=description[]]').each(function(){
						
						if (!$(this).val()){
							set_feedback('A description for each line item is requred','error_message',false);
							$(this).select();
							missing_description = true;
						}						
					});					
					if (submitting || missing_description) return;					
					submitting = true;
					$(form).mask('Please wait');					
					$(form).ajaxSubmit({
						success:function(response)
						{
							var customer_id = $('#customer_id').val();
							var customer_name = $('#customer_name').val();																					
							customer.billing.load_color_box(customer_id, customer_name);					
			                submitting = false;				                	                
						},
						dataType:'json'
					});
				},
				errorLabelContainer: "#error_message_box",
		 		wrapper: "li",
				rules: 
				{
				},
				messages: 
				{
		    	}
			});	
			$("#save_invoice").unbind('click').click(function(){
				
				if ($(this).text() === 'Save'){
					//re enable all controls before saving or their values will be lost
					$(':input', '#second_row_options').prop('disabled',false);
					$('#invoice_form').submit();					
				} else
				{
					$('#billing_info').hide();
					$('#invoice_creation_options').show();
					$('.loaded_invoice').removeClass('loaded_invoice');
					$.colorbox.resize();
				}		
			});
			$('#pay_member_balance').click(function(){
				var type = 'member';
				if ($(this).attr('checked')){
						if ($(this).hasClass('is_invoice')){				
							customer.invoice.add_customer_credit_row(type);	
						}
						else{
							customer.billing.add_member_balance_row(type);
						}											
				} else {
					customer.billing.remove_balance_row(type);
				}
				
			});
			$('#pay_account_balance').click(function(){
				var type = 'customer';
				if ($(this).attr('checked')){
						if ($(this).hasClass('is_invoice')){				
							customer.invoice.add_customer_credit_row(type);	
						}
						else{
							customer.billing.add_member_balance_row(type);
						}											
				} else {
					customer.billing.remove_balance_row(type);
				}				
			});
			$('#advanced_options').unbind('click').click(function(){
				console.log('clicking advanced_options');
				$('#invoice_time_options', '#billing_info').toggleClass('advanced');
				if ($('#invoice_time_options', '#billing_info').hasClass('advanced')){
					$(':input', '#second_row_options').prop('disabled',false);
				} else {
					$(':input', '#second_row_options').prop('disabled',true);
				}				
				
				$.colorbox.resize();
			});
			customer.billing.initialize_invoice_table_controls();						
		},
		update_invoice_totals:function()
		{
			var row = 0, 
					tax = 0, 
					quantity = 0, 
					price = 0, 
					row_total = 0, 					
					subtotal = 0,
					total_with_tax = 0,
					 new_sub = 0,
					 new_total_with_tax = 0;
				//loop through line items to update the line total and add to subtotal and total with tax				
				$('.line_item').each(function(){
					
					row = $(this);				
					tax = $('.tax',row).val();					
					quantity = $('.quantity',row).val();					
					price = $('.price',row).val();						
					price = !isNaN(price) ? price: 0;
					if (! price > 0) price = 0;	
					$('.row_total',row).html('$' + (quantity*price*(1 + tax/100)).toFixed(2));
					
					new_sub = parseFloat(price);										
					new_total_with_tax = parseFloat((quantity*price*(1 + tax/100)).toFixed(2));					
					
					subtotal += new_sub;
					total_with_tax += new_total_with_tax;
				});				
				$('.subtotal').html('$' + subtotal.toFixed(2));
				$('.total_with_tax').html('$' + total_with_tax.toFixed(2));
				$('.amount_due').html('$' + total_with_tax.toFixed(2));	
		},
		create:function(type, person_id) {
			//Show invoice
			$('#invoice_creation_options').hide();
			$('#billing_info').show();
			//Show correct options
			if (type == 'recurring_billing')
			{
				this.load(-1,person_id);			
				$('#frequency').val('monthly');
			}
			else if (type == 'invoice')
			{
				customer.invoice.load(-1,person_id);
				$('#frequency').val('one_time');
			}	
			this.update_invoice_time_options();			
			$.colorbox.resize();
		},
		load:function(billing_id, person_id) {
			$('#invoice_creation_box').mask();			
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/load_billing/"+billing_id+"/"+person_id,
	            data: '',
	            success: function(response){
	           		$('#invoice_creation_box').unmask();					
					if (typeof response.start_date === 'undefined')	response.start_date = '0000-00-00';	           			           		
	           		$('#start_date').val(response.start_date);
	           		$('#frequency').val(response.frequency);
	           		$('#frequency_selector').val(response.frequency);
	           		$("#receipt_wrapper").replaceWith(response.html);	           		
	           		$('#bill_month').val(response.month);
	           		$('#bill_day').val(response.day);
	           		$('#start_month').val(response.start_month);
	           		$('#end_month').val(response.end_month);
	           		
	           		customer.billing.update_invoice_time_options();
	           		$('#invoice_creation_options').hide();
					
					$('#billing_info').show();			
					
					var url = $('#billing_base_url').val();
					$('#invoice_form').attr('action', url+'/'+billing_id+"/"+person_id);										
					customer.billing.initialize_controls();
					$('#save_invoice').html('Save');
					if (billing_id === -1) customer.billing.add_row();					
					$.colorbox.resize();
			    },
	            dataType:'json'
	        });
		},
		save:function() {
			
		},
		remove:function(type, billing_id){
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_billing/"+billing_id,
	            data: '',
	            success: function(response){
	            	var count = $(".recurring_billing_box_holder > div").length;	
	            	if ((count === 0)){
	            		customer.billing.add_no_billing_or_invoice_placeholder('billing');
	            	}            	          		
	        		$('#billing_info').hide();
	        		$('#invoice_creation_options').show();			
	        		$.colorbox.resize();								
			    },
	            dataType:'json'
	        });
		},
		update_invoice_time_options:function () {
			var frequency = $('#frequency').val();			
			var time_option_box = $('#time_components');
			var month_range = $('#month_range');
			var bill_month = $('#bill_month');
			if (frequency == 'one_time')
			{
				time_option_box.hide();
			}
			else if (frequency == 'monthly')
			{
				month_range.show();
				bill_month.hide();
				time_option_box.show();
			}
			else if (frequency == 'yearly')
			{
				month_range.hide();
				bill_month.show();
				time_option_box.show();
			}
		},
		add_member_balance_row:function(type){
			var description, id;
				
				if (type === 'member')
				{
					description = window.billing_config.member_balance_nickname;
					id = 'member_balance_row';
				}
				else
				{
					description = window.billing_config.customer_credit_nickname;
					id = 'customer_credit_row'
				}
			
			var row_html = "<tr id=" + id + ">"+							
								"<td colspan=2 style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>"+description+"</td>"+
								"<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>;'>1</td>"+
								"<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>$--.--</td>"+
								"<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>--.--%</td>"+
								"<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'>$--.--</td>"							
							"</tr>";
									
			$('#totals_top_row').before(row_html);
			$.colorbox.resize();						
			customer.billing.initialize_invoice_table_controls();
		},	
		remove_balance_row:function(type){
			var id = '#customer_credit_row';
			if (type === 'member') id = '#member_balance_row';				
			
			$(id).remove();
			id += '_hidden';
			console.log(id);
			$(id).remove();
			customer.billing.update_invoice_totals();
		},	
		add_row:function(cc_id){
			var row_html = "<tr class='line_item'>"+
								"<td colspan=2 style='border:1px solid #606060;'><span class='delete_row'>x</span><input name='description[]' placeholder='Description' size=45 /></td>"+
								"<td style='border:1px solid #606060;'><input class='quantity line_value' name='quantity[]' placeholder='Qty' size=2 value='1' /></td>"+
								"<td class='price_column' style='border:1px solid #606060;'>$<input class='price line_value' name='price[]' placeholder='0.00' size=12 /></td>"+
								"<td class='tax_column' style='border:1px solid #606060;'><input class='tax line_value' name='tax[]' placeholder='Tax' size=6 value='0.0'/>%</td>"+
								"<td style='border:1px solid #606060; text-align:right;' class='row_total'>$0.00</td>"+				
							"</tr>";
			$('#totals_top_row').before(row_html);
			$.colorbox.resize();						
			customer.billing.initialize_invoice_table_controls();
		},
		remove_row: function(link, type)
		{						
			$(link).parent().parent().remove();
			
		},
		add_no_billing_or_invoice_placeholder:function(type){
			var html, container;
			if (type === 'billing'){
				html = "<div class='recurring_billing_box no_results'>No Recurring Billings</div>";	
				container = ".recurring_billing_box_holder";
			} else if (type === 'invoice') {
				html = "<div class='invoice_box no_results'>No Invoices</div>";
				container = ".invoice_box_holder";
			}
			console.log(type,container, html);
			$(container).append(html);
			
		}
	},
	invoice:{
		load:function (invoice_id, person_id) {
			$('#invoice_creation_box').mask();			
			var URL = "index.php/customers/load_invoice/"+invoice_id+"/"+person_id;			
			$.ajax({
	            type: "POST",
	            url: URL,
	            data: '',
	            success: function(response){		            	        	
	            	$('#invoice_creation_box').unmask();					
					var button_text = 'Save';	            	         		
	           		$('#frequency').val('one_time');
	           		$("#receipt_wrapper").replaceWith(response.html);
	           		customer.billing.update_invoice_time_options();
	           		$('#invoice_creation_options').hide();
					$('#billing_info').show();			
					customer.billing.initialize_controls();						
					if (invoice_id > 0 ){
						button_text = 'Close';
					} else {						
						customer.billing.add_row();
					}
					var url = $('#billing_base_url').val();
					$('#invoice_form').attr('action', url+'/'+invoice_id+"/"+person_id);
					$('#save_invoice').html(button_text);	
					$.colorbox.resize();			
			    },
			    error: function(result){
			    },
	            dataType:'json'
	        });
		},
		remove:function(type, invoice_id){
			$.ajax({
	            type: "POST",
	            url: "index.php/customers/delete_invoice/"+invoice_id,
	            data: '',
	            success: function(response){	            	         	          	
	        		$('#billing_info').hide();
	        		$('#invoice_creation_options').show();
	        		var count = $(".invoice_box_holder > div").length;	
	            	if ((count === 0)){
	            		customer.billing.add_no_billing_or_invoice_placeholder('invoice');
	            	} 			
	        		$.colorbox.resize();								
			    },
	            dataType:'json'
	        });	        
		},
		add_customer_credit_row:function(type){			
			var description, id, amount, 
				cp = window.billing_table_config.cp, 
				bc = window.billing_table_config.bc;
				
				if (type === 'member')
				{
					description = window.billing_config.member_balance_nickname;
					id = 'member_balance_row';
					amount = -parseFloat(window.billing_config.member_balance).toFixed(2);
				}
				else
				{
					description = window.billing_config.customer_credit_nickname;
					id = 'customer_credit_row';
					amount = -parseFloat(window.billing_config.customer_credit).toFixed(2);
				}
			console.log('adding credit amount of '+amount);
			amount = (amount < 0 ? 0 : amount);
			var row_html = "<tr id=" + id + ">"+							
								"<td colspan=2 style='padding:"+cp+"; border:1px solid "+bc+";'>"+description+"</td>"+
								"<td style='padding:"+cp+"; border:1px solid "+bc+";'>1</td>"+
								"<td style='padding:"+cp+"; border:1px solid "+bc+"; text-align:right;'>$"+amount+"</td>"+
								"<td style='padding:"+cp+"; border:1px solid "+bc+"; text-align:right;'>0.0%</td>"+
								"<td style='padding:"+cp+"; border:1px solid "+bc+"; text-align:right;'>$"+amount+"</td>"							
							"</tr>";
			$('#totals_top_row').before(row_html);				
			//add hidden row for text input	
			id += '_hidden';						
			row_html = "<tr class='line_item hidden' id='" + id + "'>"+
								"<td colspan=2 style='border:1px solid #606060;'><span class='delete_row'>x</span><input name='description[]' value='"+description+"' size=45 /></td>"+
								"<td style='border:1px solid #606060;'><input class='quantity line_value' name='quantity[]' placeholder='Qty' size=2 value='1' /></td>"+
								"<td class='price_column' style='border:1px solid #606060;'>$<input class='price line_value' name='price[]' value='"+amount+"' size=12 /></td>"+
								"<td class='tax_column' style='border:1px solid #606060;'><input class='tax line_value' name='tax[]' value='0.0' placeholder='Tax' size=6 value='0.0'/>%</td>"+
								"<td style='border:1px solid #606060; text-align:right;' class='row_total'>$0.00</td>"+				
							"</tr>";									
			$('#totals_top_row').before(row_html);
			$.colorbox.resize();						
			customer.billing.initialize_invoice_table_controls();
			customer.billing.update_invoice_totals();
		}
	}
};

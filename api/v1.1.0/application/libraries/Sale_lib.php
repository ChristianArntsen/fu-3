<?php
class Sale_lib
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('modifier');
	}

	function get_cart($receipt_id = false)
	{
		if($this->CI->session->userdata('cart') === false){
			$this->set_cart(array());
		}

		$cart = $this->CI->session->userdata('cart');
		return $cart;
	}

	function get_basket()
	{
		if($this->CI->session->userdata('basket') === false){
			$this->set_basket(array());
		}

		return $this->CI->session->userdata('basket');
	}

	function set_cart($cart_data)
	{
		$this->CI->session->set_userdata('cart',$cart_data);
	}

	function set_basket($basket_data)
	{
		$this->CI->session->set_userdata('basket',$basket_data);
	}

	//Alain Multiple Payments
	function get_payments($receipt_id = false)
	{
		if($this->CI->session->userdata('payments') === false){
			$this->set_payments(array());
		}

		$all_payments = $this->CI->session->userdata('payments');
		if($receipt_id !== false){
			return $all_payments[$receipt_id];
		}

		return $all_payments;
	}

	//Alain Multiple Payments
	function set_payments($payments_data)
	{
		$this->CI->session->set_userdata('payments',$payments_data);
	}

	function get_transaction_item_taxes($item_id)
	{
		$tax_array = $this->CI->session->userdata('taxes');
		if ($tax_array === false)
			return array();
		//print_r($tax_array);
		foreach ($tax_array[$item_id] as $index => $taxes)
			$tax_array[$item_id][$index] = (array) $taxes;
		return $tax_array[$item_id];
	}

	function set_transaction_taxes($tax_array)
	{
		$this->CI->session->set_userdata('taxes', $tax_array);
	}
	function clear_transaction_taxes()
	{
		$this->CI->session->unset_userdata('taxes');
	}

	function get_comment()
	{
		return $this->CI->session->userdata('comment') ? $this->CI->session->userdata('comment') : '';
	}

	function set_comment($comment)
	{
		$this->CI->session->set_userdata('comment', $comment);
	}

	function clear_comment()
	{
		$this->CI->session->unset_userdata('comment');
	}

	function get_email_receipt()
	{
		return $this->CI->session->userdata('email_receipt');
	}

	function set_email_receipt($email_receipt)
	{
		$this->CI->session->set_userdata('email_receipt', $email_receipt);
	}

	function clear_email_receipt()
	{
		$this->CI->session->unset_userdata('email_receipt');
	}

	function add_payment($payment_id,$payment_amount, $maximum = false, $receipt_id = false, $invoice_id = false)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$payments=$this->get_payments();
		$customer_id = $this->get_customer();
		$cab_name=($this->CI->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->CI->config->item('customer_credit_nickname');
		$cmb_name=($this->CI->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->CI->config->item('member_balance_nickname');
		$customer_credit = $payment_id == $cab_name;
		$limited_funds = false;
		$max_funds = 0;
		if ($customer_id != -1 && ($customer_credit || $payment_id == $cmb_name)) {
			$customer_info=$this->CI->Customer->get_info($customer_id);
			if ($customer_credit)
			{
				$cart = $this->get_basket();
				$valid_amount = 0;
				$second_line = '';
				$debug = ' ';
				$times_through = 0;
				$total = 0 ;
	//			$debug .= ' CC '.count($cart).' PA '.$payment_amount;
				// Only allow items that belong to the correct category or department
				foreach($cart as $item)
				{
					$p = $item['price'];
                    $q = $item['quantity'];
                    $d = $item['discount'];

					$manual_taxes = $this->CI->session->userdata('manual_taxes');
					$tax_info = $this->CI->Item_taxes->get_info($item['item_id'], $manual_taxes);
					$total_tax = 0;
					if ($customer_info->taxable)
					{
						// IF TAX IS INCLUDED, NO NEED TO CALCULATE IT HERE
                        if ($customer_info->taxable && !$tax_included)
						{
							foreach($tax_info as $key=>$tax)
							{
								$t = $tax['percent'];
								//$name = $tax['percent'].'% ' . $tax['name'];

								if ($tax['cumulative'])
									$tax_amount = (($p * $q) * (1 - $d / 100) + $prev_tax) * (($t) / 100);
								else
									$tax_amount = ($p * $q) * (1 - $d / 100) * ($t / 100);

								$total_tax += number_format($tax_amount,2);
							}
						}
					}
					if ($this->CI->config->item('credit_department') == 1 && $this->CI->config->item('credit_department_name') != 'all')
					{
						if ($item['department'] == $this->CI->config->item('credit_department_name'))
							$valid_amount += $p * $q + $total_tax;
						$second_line = " (".$this->CI->config->item('credit_department_name')." Department only)";
					}
					else if ($this->CI->config->item('credit_category') == 1 && $this->CI->config->item('credit_category_name') != 'all')
					{
						if ($item['category'] == $this->CI->config->item('credit_category_name'))
							$valid_amount += $p * $q + $total_tax;
						$second_line = " (".$this->CI->config->item('credit_category_name')." Category only)";
					}
					else {
						$valid_amount += $p * $q + $total_tax;
					}
					$total = $p * $q + $total_tax;


//$debug .= ' '.$valid_amount.' VA '.$total.' TOT ';
				$times_through++;
				}
//$debug .= ' '.count($cart).' Cart Count '.$total.' Total';
				if ($payment_amount > $valid_amount)
				{
					if ($valid_amount == 0)
						$this->CI->session->set_userdata('display_message', 'Nothing is eligible to be charged to '.$cab_name);
					else
						$this->CI->session->set_userdata('display_message', 'Only '.to_currency($valid_amount).' is eligible to be charged to '.$cab_name);
					$payment_amount = $valid_amount;
				}
				// Watch for negative balance
				if ($customer_info->account_balance_allow_negative == '0')
				{
					$limited_funds = true;
					$max_funds = $customer_info->account_balance;
				}
			}
			else {
				if ($customer_info->member_account_balance_allow_negative == '0')
				{
					$limited_funds = true;
					$max_funds = $customer_info->member_account_balance;
				}
			}
			if ($limited_funds && ($max_funds - $payment_amount < 0 || isset($payments[$payment_id]) && $payments[$payment_id]['payment_amount'] + $payment_amount > $max_funds))
			{
				$this->CI->session->set_userdata('display_message', 'Only '.to_currency($max_funds).' is available on the account.');
				$payment_amount = $max_funds;
			}
			$payment_id .= '- '.$customer_info->last_name.', '.$customer_info->first_name;
		}
		// CHECK HOW MUCH LOYALTY WE CAN USE FOR THIS CUSTOMER WITH THIS CART

		$dollars_per_point = 0;
		$maximum_points = 0;
		$maximum_dollars = 0;
		$total_dollars = 0;
		$existing_payments = 0;
		$existing_loyalty = 0;
		$customer_points = 0;
		$loyalty_point_value = 0;
		if ($payment_id == lang('sales_loyalty'))
		{
			$customer_info=$this->CI->Customer->get_info($customer_id);
			$customer_point_balance = $customer_info->loyalty_points;
			$cart = $this->get_basket();
			foreach ($cart as $item)
			{
				$p = $item['price'];
                $q = $item['quantity'];
                $d = $item['discount'];
				$total_tax = 0;
				// CAN'T FORGET TAXES
				if (isset($item['item_id']) || isset($item['item_kit_id']))
				{
					$manual_taxes = $this->CI->session->userdata('manual_taxes');
					$tax_info = (isset($item['item_id']) ? $this->CI->Item_taxes->get_info($item['item_id'], $manual_taxes) : $this->CI->Item_kit_taxes->get_info($item['item_kit_id'], $manual_taxes)) ;
					if ($customer_info->taxable && !$tax_included)
					{
						foreach($tax_info as $key=>$tax)
						{
							$t = $tax['percent'];
							//$name = $tax['percent'].'% ' . $tax['name'];

							if ($tax['cumulative'])
								$tax_amount = (($p * $q * (1 - $d / 100)) + $prev_tax) * ($t / 100);
							else
								$tax_amount = $p * $q * (1 - $d / 100) * ($t / 100);

							$total_tax += number_format($tax_amount,2);
						}
					}
				}
				// FIND THE $/pt RATIOS FOR EACH ITEM IN THE CART
				$dollars_per_point = $this->CI->Customer_loyalty->get_dollars_per_point($item);
				//print_r($dollars_per_point);
				// TOTAL THE MAXIMUM NUMBER OF POINTS THAT THIS CART MIGHT ALLOW TO BE SPENT
				$maximum_points += (($p * $q * (1 - $d / 100) + $total_tax) / $dollars_per_point['dollars_per_point']) * 100;
				// IF A POINTS EXCHANGE IS POSSIBLE, TOTAL THE MAXIMUM DOLLAR AMOUNT THAT CAN BE SPENT ON THIS CART
				if ($dollars_per_point['dollars_per_point'] > 0)
					$maximum_dollars += $p * $q * (1 - $d / 100) + $total_tax;
				// TOTAL ALL PURCHASE PRICES FOR THIS CART
				$total_dollars += $p * $q * (1 - $d / 100) + $total_tax;
			}
			foreach ($payments as $payment)
			{
				// TOTAL UP ALL EXISTING PAYMENTS
				$existing_payments += $payment['payment_amount'];
				// IF THERE IS AN EXISTING LOYALTY PAYMENT ALREADY APPLIED
				if ($payment['payment_type'] == lang('sales_loyalty'))
					$existing_loyalty += $payment['payment_amount'];
			}
			$dollar_cap = $maximum_dollars;
			//echo "mp $maximum_points md $maximum_dollars td $total_dollars ep $existing_payments";
			// ACCOUNT FOR CUSTOMER POINT BALANCE IN SYSTEM...
			$maximum_dollars = ($maximum_points > $customer_point_balance) ? $maximum_dollars / $maximum_points * $customer_point_balance : $maximum_dollars;
			// CALCULATE WHAT IS OWED TO COMPLETE THIS SALE
			$remaining_balance = $total_dollars - $existing_payments + $existing_loyalty;
			$maximum_dollars = $maximum_dollars > $remaining_balance ? $remaining_balance : $maximum_dollars;
			$payment_amount = $maximum_dollars > $payment_amount ? $payment_amount : $maximum_dollars;
			$payment_amount = floor((($payment_amount + $existing_loyalty > $maximum_dollars) ? $maximum_dollars - $existing_loyalty : $payment_amount)*100)/100;
			$loyalty_point_value = $maximum_points * ($payment_amount + $existing_loyalty) /  $dollar_cap;//$payment_amount / $dollars_per_point['dollars_per_point'] * 100;//
		}
		$invoice_id = (strpos($payment_id, 'VISA') !== false ||
						strpos($payment_id, 'M/C') !== false ||
						strpos($payment_id, 'AMEX') !== false ||
						strpos($payment_id, 'DCVR') !== false||
						strpos($payment_id, 'DINERS') !== false||
						strpos($payment_id, 'JCB') !== false||
						strpos($payment_id, 'Bank Acct') !== false) ? ($invoice_id ? $invoice_id : $this->CI->session->userdata('invoice_id')) : '';

		$payment = array($payment_id=>
			array(
				'payment_type'=>$payment_id,
				'payment_amount'=>$payment_amount,
				'loyalty_point_value'=>$loyalty_point_value,
				'invoice_id'=>$invoice_id,
				'receipt_id'=>$receipt_id,
				'customer_id'=>$customer_id
				)
		);
		$this->CI->session->unset_userdata('invoice_id');

		//payment_method already exists, add to payment_amount
		if(isset($payments[$receipt_id][$payment_id]))
		{
			if ($limited_funds)
				$payments[$receipt_id][$payment_id]['payment_amount']=$payment_amount;
			else if ($maximum)
				$payments[$receipt_id][$payment_id]['payment_amount'] = ($payments[$payment_id]['payment_amount'] + $payment_amount > $maximum) ? $maximum : $payments[$payment_id]['payment_amount'] + $payment_amount;
			else
				$payments[$receipt_id][$payment_id]['payment_amount']+=$payment_amount;
		}
		else
		{
			if(empty($payments[$receipt_id])){
				$payments[$receipt_id] = array();
			}
			//add to existing array
			$payments[$receipt_id] += $payment;
		}

		if ($customer_credit && $payment_amount == 0)
		{}
		else
			$this->set_payments($payments);
		return true;

	}

	//Alain Multiple Payments
	function edit_payment($payment_id,$payment_amount, $receipt_id = 0)
	{
		//echo '<br/>editing';
		$payments = $this->get_payments();
		//print_r($payments);
		if(isset($payments[$receipt_id][$payment_id]))
		{
		//echo '<br/>found payment '.$payment_id.'<br/>';
		//print_r($payments[$payment_id]);
			$payments[$receipt_id][$payment_id]['payment_type'] = $payment_id;
			$payments[$receipt_id][$payment_id]['payment_amount'] = $payment_amount;
			//echo '<br/>setting payments';
			$this->set_payments($payments);
			return true;
		}
		//echo '<br/>did not find payment';

		return false;
	}

	//Alain Multiple Payments
	function delete_payment($payment_id, $receipt_id = 0)
	{
		//$payment_id = str_replace("___", "/", $payment_id);
		//$payment_id = str_replace("~~", "&", $payment_id);

		$payments=$this->get_payments();
		$invoice_id = $payments[$receipt_id][$payment_id]['invoice_id'];
		if ($invoice_id != '')
		{
			// RETURN ETS CHARGE
			if ($this->CI->config->item('ets_key')) {
				$credit_card_info = $this->CI->Sale->get_credit_card_payment($invoice_id);
				$credit_card_info = $credit_card_info[0];

				$this->CI->load->library('Hosted_payments');
				$payment = new Hosted_payments();
				$payment->initialize($this->CI->config->item('ets_key'));
				$session = $payment->set('action', 'void')
					->set('sessionID', $credit_card_info['process_data'])
					->set("transactionID", $credit_card_info['payment_id'])
					->send();
				//print_r($session);
				if ($session->status != 'success')
					return;
				$payment_data = array(
					'amount_refunded' => (string) $session->transactions->amount,
					'voided' => 1
				);
				$this->CI->Sale->update_credit_card_payment($invoice_id, $payment_data);
			}
			// RETURN MERCURY CHARGE
			else {
				$this->CI->load->library('Hosted_checkout');
				$HC = new Hosted_checkout();
				if (!$HC->cancel_payment($invoice_id))
					return;
			}
		}
		unset($payments[$receipt_id][$payment_id]);
		$this->set_payments($payments);
	}

	//Alain Multiple Payments
	function empty_payments()
	{
		$this->CI->session->unset_userdata('raincheck_id');
		$this->CI->session->unset_userdata('payments');
	}

	//Alain Multiple Payments
	function get_payments_total($filter_receipt_id = false)
	{
		$subtotal = 0;
		foreach($this->get_payments() as $receipt_id => $receipt_payments)
		{
			if($filter_receipt_id !== false && $receipt_id != $filter_receipt_id){
				continue;
			}
			foreach($receipt_payments as $payments){
				$subtotal += $payments['payment_amount'];
			}
		}
		return round($subtotal, 2);
	}

	//Alain Multiple Payments
	function get_receipt_payments_total($receipt_id_filter = false)
	{
		if($receipt_id_filter !== false){

			$subtotal = 0;
			foreach($this->get_payments() as $receipt_id => $receipt_payments)
			{
				if($receipt_id != $receipt_id_filter){
					continue;
				}

				foreach($receipt_payments as $payment){
					$subtotal += $payment['payment_amount'];
				}
			}

		}else{

			$subtotal = array();
			foreach($this->get_payments() as $receipt_id => $receipt_payments)
			{
				if(!isset($subtotal[$receipt_id])){
					$subtotal[$receipt_id] = 0;
				}

				foreach($receipt_payments as $payment){
					$subtotal[$receipt_id] += $payment['payment_amount'];
				}
			}
		}

		return $subtotal;
	}

	//Alain Multiple Payments
	function get_amount_due($sale_id = false)
	{
		$amount_due=0;
		$payment_total = $this->get_payments_total();
		$sales_total=$this->get_total($sale_id);
		$amount_due=to_currency_no_money($sales_total - $payment_total);
		return $amount_due;
	}
    function get_basket_amount_due($sale_id = false)
	{
		$amount_due = 0;
		$payment_total = $this->get_payments_total();
		$sales_total = $this->get_basket_total($sale_id);
		$amount_due = to_currency_no_money($sales_total - $payment_total);
		return $amount_due;
	}

	function get_valid_amount($department = '', $category = '', $subcategory = '', $taxable = true)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$cart = $this->get_basket();
		$valid_amount = 0;
		$second_line = '';
		$debug = ' ';
		$times_through = 0;
		$total = 0 ;
		foreach($cart as $item)
		{
			$p = $item['price'];
            $q = $item['quantity'];
            $d = $item['discount'];
			$manual_taxes = $this->CI->session->userdata('manual_taxes');
			if (isset($item['item_kit_id']))
				$tax_info = $this->CI->Item_kit_taxes->get_info($item['item_kit_id'], $manual_taxes);
			else
				$tax_info = $this->CI->Item_taxes->get_info($item['item_id'], $manual_taxes);
			$total_tax = 0;
			if (($taxable || true) && !$tax_included)
			{
				foreach($tax_info as $key=>$tax)
				{
					$t = $tax['percent'];
					//$name = $tax['percent'].'% ' . $tax['name'];

					if ($tax['cumulative'])
						$tax_amount = (($p * $q * (1 - $d / 100)) + $prev_tax) * ($t / 100);
					else
						$tax_amount = $p * $q * (1 - $d / 100) * ($t / 100);

					$total_tax += number_format($tax_amount,2);
				}
			}

			if ($department != '' && $category != '')
			{
				if ($item['department'] == $department && $item['category'] == $category)
					$valid_amount += $p * $q + ($total_tax);
				$second_line = " ({$department} Department and {$category} Category only)";
			}
			else if ($department != '')
			{
				if ($item['department'] == $department)
					$valid_amount += $p * $q + ($total_tax);
				$second_line = " ({$department} Department only)";
			}
			else if ($category != '')
			{
				if ($item['category'] == $category)
					$valid_amount += $p * $q + ($total_tax);
				$second_line = " (".$this->CI->config->item('credit_category_name')." Category only)";
			}
			else {
				$valid_amount += $p * $q + ($total_tax);
			}
			$total = $p * $q + ($total_tax);

			$times_through++;
		}
		return array('valid_amount'=>$valid_amount, 'second_line'=>$second_line);
	}
	function get_customer()
	{
		if(!$this->CI->session->userdata('customer'))
			$this->set_customer(-1);

		return $this->CI->session->userdata('customer');
	}

	function set_customer($customer_id)
	{
		$this->CI->session->set_userdata('customer',$customer_id);
		$this->set_customer_quickbutton($customer_id);
	}
	function get_customer_quickbuttons()
	{
		if(!$this->CI->session->userdata('customer_quickbuttons'))
			return array();//$this->set_customer_quickbbutton(-1);

		return $this->CI->session->userdata('customer_quickbuttons');
	}

	function set_customer_quickbutton($customer_id, $customer_name='')
	{
		if ($customer_name == '') {
			$customer_info = $this->CI->Customer->get_info($customer_id);
			if ($customer_info->last_name != '' && $customer_info->first_name != '')
			$customer_name = $customer_info->last_name.', '.$customer_info->first_name;
		}
		if (trim($customer_name) == '' || $customer_id == -1 || $customer_id == 0)
			return;
		$customer_quickbutton_array = $this->get_customer_quickbuttons();
		$customer_quickbutton_array[$customer_id] = array('id'=>$customer_id, 'name'=>$customer_name);

		$this->CI->session->set_userdata('customer_quickbuttons',$customer_quickbutton_array);
	}
	function delete_customer_quickbutton($customer_id)
	{
		$customer_quickbutton_array = $this->get_customer_quickbuttons();
		unset($customer_quickbutton_array[$customer_id]);

		$this->CI->session->set_userdata('customer_quickbuttons',$customer_quickbutton_array);
	}
	function set_taxable($taxable)
	{
		$this->CI->session->set_userdata('taxable',$taxable);
	}
	function get_teetime()
	{
		if(!$this->CI->session->userdata('teetime'))
			$this->set_teetime(-1);

		return $this->CI->session->userdata('teetime');
	}

	function set_teetime($teetime_id)
	{
		$this->CI->session->set_userdata('teetime',$teetime_id);
	}

	function get_mode()
	{
		if(!$this->CI->session->userdata('sale_mode'))
			$this->set_mode('sale');

		return $this->CI->session->userdata('sale_mode');
	}

	function set_mode($mode)
	{
		$this->CI->session->set_userdata('sale_mode',$mode);
		$this->update_quantities($mode);
	}
	function update_quantities($mode)
	{
		$items = $this->get_basket();
		foreach ($items as $index => $item)
		{
			//Make sure they are negatives if we issuing return and positives if selling
			if ($mode == 'return')
				$items[$index]['quantity'] = ($item['quantity'] < 0)?$item['quantity']:$item['quantity']*-1;
			else
				$items[$index]['quantity'] = ($item['quantity'] > 0)?$item['quantity']:$item['quantity']*-1;
		}

		$this->set_basket($items);

		$cart_items = $this->get_cart();
		foreach ($cart_items as $index => $item)
		{
			//Make sure they are negatives if we issuing return and positives if selling
			if ($mode == 'return')
				$cart_items[$index]['quantity'] = ($item['quantity'] < 0)?$item['quantity']:$item['quantity']*-1;
			else
				$cart_items[$index]['quantity'] = ($item['quantity'] > 0)?$item['quantity']:$item['quantity']*-1;
		}

		$this->set_cart($cart_items);
	}
	function get_invoice_details($line)
	{
		$items = $this->get_cart();
		return $items[$line];
	}
	function update_invoice_details($invoice_info, $line)
	{
		$items = $this->get_cart();
		$items[$line] = $invoice_info;
		$this->set_cart($items);

		$basket_items = $this->get_basket();
		$basket_items[$line] = $invoice_info;
		$this->set_basket($basket_items);
	}
	function get_giftcard_details($line)
	{
		$items = $this->get_cart();
		return array('giftcard_data' => $items[$line]['giftcard_data'], 'value'=>$items[$line]['price']);
	}
	function get_punch_card_details($line)
	{
		$items = $this->get_cart();
		return array('punch_card_data' => $items[$line]['punch_card_data'], 'value'=>$items[$line]['price'], 'item_kit_id'=>$items[$line]['item_kit_id']);
	}
	function get_modifiers($line)
	{
		$items = $this->get_cart();
		return $items[$line]['modifiers'];
	}
	function save_giftcard_details($line)
	{
		$giftcard_action = $this->CI->input->post('gc_action');
		$giftcard_number = $this->CI->input->post('giftcard_number');
		$value = $this->CI->input->post('value');
		$customer_id = $this->CI->input->post('customer_id');
		$expiration_date = $this->CI->input->post('expiration_date');
		$customer_name = $this->CI->input->post('customer');
		$details = $this->CI->input->post('details');
		$department = $this->CI->input->post('department');
		$category = $this->CI->input->post('category');

		$giftcard_data = array(
			'action'=>$giftcard_action,
			'giftcard_number'=>$giftcard_number,
			'customer_id'=>$customer_id,
			'expiration_date'=>$expiration_date,
			'customer_name'=>$customer_name,
			'details'=>$details,
			'category'=>$category,
			'department'=>$department
		);

		$this->edit_item_attribute($line, 'giftcard_data', $giftcard_data);
		$this->edit_item_attribute($line, 'price', $value);
		$this->edit_basket_item_attribute($line, 'giftcard_data', $giftcard_data);
		return $this->edit_basket_item_attribute($line, 'price', $value);
	}
	function save_item_modifiers($line)
	{
		$modifiers = $this->CI->input->post('modifiers');
		$modifierTotal = $this->calculate_modifier_total($modifiers);

		foreach($modifiers as $key => $modifier){
			$modifiers[$key]['options'] = explode(',', $modifier['options']);
		}

		$this->edit_item_attribute($line, 'modifiers', $modifiers);
		$this->edit_basket_item_attribute($line, 'modifiers', $modifiers);
		$this->edit_basket_item_attribute($line, 'modifier_total', $modifierTotal);
		return $this->edit_item_attribute($line, 'modifier_total', $modifierTotal);
	}
	function calculate_modifier_total($modifiers){
		$modifierTotal = 0.00;
		foreach($modifiers as $modifierId => $modifier){
			$modifierTotal += $modifier['selected_price'];
		}

		return $modifierTotal;
	}
	function save_punch_card_details($line)
	{
		$punch_card_number = $this->CI->input->post('punch_card_number');
		$customer_id = $this->CI->input->post('customer_id');
		$item_kit_id = $this->CI->input->post('item_kit_id');
		$expiration_date = $this->CI->input->post('expiration_date');
		$customer_name = $this->CI->input->post('customer');
		$details = $this->CI->input->post('details');

		$punch_card_data = array(
			'punch_card_number'=>$punch_card_number,
			'customer_id'=>$customer_id,
			'item_kit_id'=>$item_kit_id,
			'expiration_date'=>$expiration_date,
			'customer_name'=>$customer_name,
			'details'=>$details
		);

		$this->edit_item_attribute($line, 'punch_card_data', $punch_card_data);
		return $this->edit_basket_item_attribute($line, 'punch_card_data', $punch_card_data);
	}
	function missing_or_repeat_giftcard_number()
	{
		$items = $this->get_basket();
		foreach ($items as $line => $item)
		{
			if ($item['is_giftcard'] && $item['giftcard_data']['action'] != 'reload' &&
				($item['giftcard_data']['giftcard_number'] == '' ||
				$this->CI->Giftcard->exists($this->CI->Giftcard->get_giftcard_id($item['giftcard_data']['giftcard_number']))))
				return $line;
		}
		return false;
	}
	function missing_or_repeat_punch_card_number()
	{
		$items = $this->get_basket();
		foreach ($items as $line => $item)
		{
			if ($item['is_punch_card'] &&
				($item['punch_card_data']['punch_card_number'] == '' ||
				$this->CI->Punch_card->exists($this->CI->Punch_card->get_punch_card_id($item['punch_card_data']['punch_card_number']))))
				return $line;
		}
		return false;
	}
	function tournament_without_customer()
	{
		// $items = $this->get_basket();
		// foreach ($items as $line => $item)
		// {
			// if ($item['tournament_id'])
			// {
				// echo "in the if";
				// return !$this->get_customer();
			// }
		// }
				return $this->get_customer() == -1;
		// return true;
	}

	// Takes a list of items which were paid for, marks which items in
	// cart were paid for, and how much is still due on each item
	function mark_paid_cart_items($receipt_items){
		$cart_items = $this->get_cart();
		$paid_items = array();

		// Organize receipt items by item line number
		foreach($receipt_items as $receipt_item){
			$paid_items[$receipt_item['line']] = $receipt_item;
		}

		// Loop through cart items, check if item was paid for in receipt
		foreach($cart_items as $line => $item){
			if(isset($paid_items[$line])){
				$paid_item = $paid_items[$line];
				$cart_items[$line]['amount_paid'] += $paid_item['total'];
				$cart_items[$line]['amount_due'] = $item['total'] - $cart_items[$line]['amount_paid'];
			}
		}

		$this->set_cart($cart_items);
	}


	function add_item($item_id, $quantity = 1, $discount = 0, $price = null, $description = null,$serialnumber = null, $price_category_index = null, $teetime_type = null, $modifiers = null)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		//make sure item exists
		if(!$this->CI->Item->exists(is_numeric($item_id) ? (int)$item_id : -1))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);
			if(!$item_id){
				return false;
			}
		}
		else
		{
			$item_id = (int) $item_id;
		}
		//Alain Serialization and Description

		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.
        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			//if(isset($item['item_id']) && $item['item_id']==$item_id)
			//{
			//	$itemalreadyinsale=TRUE;
			//	$updatekey=$item['line'];
			//}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$item_info = $this->CI->Item->get_info($item_id);
		$teetime_type = $teetime_type!=null ? $teetime_type:substr($item_info->item_number, strrpos($item_info->item_number, '_')+1);
		$teesheet_id = $this->CI->session->userdata('teesheet_id');

		$prices = ($this->CI->permissions->course_has_module('reservations')?$this->CI->Fee->get_info($item_info->item_number, $teesheet_id):$this->CI->Green_fee->get_info($item_info->item_number, $teesheet_id));

		if (($item_info->category == 'Green Fees' || $item_info->category == 'Carts') && $item_info->invisible)
		{
			$price_category_index = $price_category_index!=null ? $price_category_index:($this->CI->session->userdata('default_price_class')?$this->CI->session->userdata('default_price_class'):1);
			$price = $price!=null ? $price: $this->get_teetime_price($price_category_index, $teetime_type);
		}
		else
		{
			$price = $price!=null ? $price: $item_info->unit_price;
		}

		if(empty($modifiers))
		{
			$modifiers = $this->CI->modifier->get_by_item($item_id);
		}

		$modifier_total = $this->calculate_modifier_total($modifiers);
		$subtotal = $this->calculate_item_subtotal($price + $modifier_total, $quantity, $discount);
		$tax = $this->calculate_item_tax($subtotal, $item_id, 'item');
		// ADJUST IF PRICE INCLUDES TAX
		if ($tax_included)
			$subtotal = $subtotal - $tax;
		$item = array(
			($insertkey) =>
			array(
				'item_id'=>$item_id,
				'modifiers' => $modifiers,
				'modifier_total' => $modifier_total,
				'line'=>$insertkey,
				'name'=>$item_info->name,
				'item_number'=>$item_info->item_number,
				'price_category'=>$price_category_index,
				'teetime_type'=>$teetime_type,
				'description'=>$description!=null ? $description: $item_info->description,
				'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
				'allow_alt_description'=>$item_info->allow_alt_description,
				'is_serialized'=>$item_info->is_serialized,
				'is_giftcard'=>$item_info->is_giftcard,
				'giftcard_data'=>array('giftcard_number'=>'','customer_id'=>'','expiration_date'=>''),
				'quantity'=>$quantity,
				'discount'=>$discount,
				'max_discount'=>$item_info->max_discount,
				'department'=>$item_info->department,
				'category'=>$item_info->category,
				'subcategory'=>$item_info->subcategory,
				'price' => $price,
				'base_price'=>$price,
				'subtotal' => $subtotal,
				'tax' => $tax,
				'total' => $subtotal + $tax,
				'amount_paid' => 0,
				'amount_due' => $total,
				'seat' => $this->get_next_seat()
			)
		);

		//Item already exists and is not serialized, add to quantity
		if($itemalreadyinsale && ($item_info->is_serialized ==0) )
		{
			$items[$updatekey]['quantity'] += $quantity;
		}
		else
		{
			//add to existing array
			$items += $item;
		}

		$this->set_cart($items);
		return true;
	}

	function calculate_item_price($line){



	}

    function add_item_to_basket($item_id,$quantity=1,$discount=0,$price=null,$description=null,$serialnumber=null,$price_category_index=null,$teetime_type=null,$modifiers=null)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		//make sure item exists
		if(!$this->CI->Item->exists(is_numeric($item_id) ? (int)$item_id : -1))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}
		else
		{
			$item_id = (int)$item_id;
		}
		//Alain Serialization and Description

		//Get all items in the cart so far...
		$items = $this->get_basket();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}

			//if(isset($item['item_id']) && $item['item_id']==$item_id)
			//{
			//	$itemalreadyinsale=TRUE;
			//	$updatekey=$item['line'];
			//}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$item_info = $this->CI->Item->get_info($item_id);
		$teetime_type = $teetime_type!=null ? $teetime_type:substr($item_info->item_number, strrpos($item_info->item_number, '_')+1);
		$teesheet_id = $this->CI->session->userdata('teesheet_id');
		$prices = ($this->CI->permissions->course_has_module('reservations')?$this->CI->Fee->get_info($item_info->item_number, $teesheet_id):$this->CI->Green_fee->get_info($item_info->item_number, $teesheet_id));
		if (($item_info->category == 'Green Fees' || $item_info->category == 'Carts') && $item_info->invisible)
		{
			$price_category_index = $price_category_index!=null ? $price_category_index:($this->CI->session->userdata('default_price_class')?$this->CI->session->userdata('default_price_class'):1);
			$price = $price!=null ? $price: $this->get_teetime_price($price_category_index, $teetime_type);
		}
		else
			$price = $price!=null ? $price: $item_info->unit_price;

		if(empty($modifiers)){
			$modifiers = $this->CI->modifier->get_by_item($item_id);
		}

		$modifier_total = $this->calculate_modifier_total($modifiers);
		$subtotal = $this->calculate_item_subtotal($price + $modifier_total, $quantity, $discount);
		$tax = $this->calculate_item_tax($subtotal, $item_id, 'item');
		// ADJUST IF PRICE INCLUDES TAX
		if ($tax_included)
			$subtotal = $subtotal - $tax;

		$item = array(
			($insertkey) =>
			array(
				'item_id'=>$item_id,
				'modifiers' => $modifiers,
				'modifier_total' => $modifier_total,
				'line'=>$insertkey,
				'name'=>$item_info->name,
				'item_number'=>$item_info->item_number,
				'price_category'=>$price_category_index,
				'teetime_type'=>$teetime_type,
				'description'=>$description!=null ? $description: $item_info->description,
				'serialnumber'=>$serialnumber!=null ? $serialnumber: '',
				'allow_alt_description'=>$item_info->allow_alt_description,
				'is_serialized'=>$item_info->is_serialized,
				'is_giftcard'=>$item_info->is_giftcard,
				'giftcard_data'=>array('giftcard_number'=>'','customer_id'=>'','expiration_date'=>''),
				'quantity'=>$quantity,
				'discount'=>$discount,
				'max_discount'=>$item_info->max_discount,
				'department'=>$item_info->department,
				'category'=>$item_info->category,
				'subcategory'=>$item_info->subcategory,
				'price' => $price,
				'base_price'=>$price,
				'subtotal' => $subtotal,
				'tax' => $tax,
				'total' => $subtotal + $tax,
				'amount_paid' => 0,
				'amount_due' => $total,
				'seat' => $this->get_next_seat()
			)
		);

		//Item already exists and is not serialized, add to quantity
		if($itemalreadyinsale && ($item_info->is_serialized ==0) )
		{
			$items[$updatekey]['quantity']+=$quantity;
		}
		else
		{
			//add to existing array
			$items+=$item;
		}

		$this->set_basket($items);
		return true;
	}

	function add_item_kit($external_item_kit_id_or_item_number,$quantity=1,$discount=0,$price=null,$description=null)
	{
		if (strpos($external_item_kit_id_or_item_number, 'KIT') !== FALSE)
		{
			//KIT #
			$pieces = explode(' ',$external_item_kit_id_or_item_number);
			$item_kit_id = (int)$pieces[1];
		}
		else
		{
			$item_kit_id = $this->CI->Item_kit->get_item_kit_id($external_item_kit_id_or_item_number);
		}

		//make sure item exists
		if(!$this->CI->Item_kit->exists($item_kit_id))
		{
			return false;
		}

		$item_kit_info = $this->CI->Item_kit->get_info($item_kit_id);
		$item_kit_items = $this->CI->Item_kit_items->get_info($item_kit_id);
		if ( $item_kit_info->unit_price == null)
		{
			foreach ($item_kit_items as $item_kit_item)
			{
				for($k=0;$k<$item_kit_item->quantity;$k++)
				{
					$this->add_item($item_kit_item->item_id, 1);
				}
			}

			return true;
		}
		else
		{
			$items = $this->get_cart();

	        //We need to loop through all items in the cart.
	        //If the item is already there, get it's key($updatekey).
	        //We also need to get the next key that we are going to use in case we need to add the
	        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

	        $maxkey=0;                       //Highest key so far
	        $itemalreadyinsale=FALSE;        //We did not find the item yet.
			$insertkey=0;                    //Key to use for new entry.
			$updatekey=0;                    //Key to use to update(quantity)

			foreach ($items as $item)
			{
	            //We primed the loop so maxkey is 0 the first time.
	            //Also, we have stored the key in the element itself so we can compare.

				if($maxkey <= $item['line'])
				{
					$maxkey = $item['line'];
				}

				if(isset($item['item_kit_id']) && $item['item_kit_id']==$item_kit_id)
				{
					$itemalreadyinsale=TRUE;
					$updatekey=$item['line'];
				}
			}

			$insertkey=$maxkey+1;
			//array/cart records are identified by $insertkey and item_id is just another field.
			$item = array(($insertkey)=>
			array(
				'item_kit_id'=>$item_kit_id,
				'line'=>$insertkey,
				'item_kit_number'=>$item_kit_info->item_kit_number,
				'name'=>$item_kit_info->name,
				'description'=>$description!=null ? $description: $item_kit_info->description,
				'is_punch_card'=>$item_kit_info->is_punch_card,
				'punch_card_data'=>array('punch_card_number'=>'','customer_id'=>'','expiration_date'=>'','item_kit_id'=>$item_kit_id, 'details'=>''),
				'quantity'=>$quantity,
	            'discount'=>$discount,
				'price'=>$price!=null ? $price: $item_kit_info->unit_price,
				'base_price'=>$price!=null ? $price: $item_kit_info->unit_price
				)
			);

			//Item already exists and is not serialized, add to quantity
			if($itemalreadyinsale)
			{
				$items[$updatekey]['quantity']+=$quantity;
			}
			else
			{
				//add to existing array
				$items+=$item;
			}

			$this->set_cart($items);
			return true;
		}
	}
    function add_item_kit_to_basket($external_item_kit_id_or_item_number,$quantity=1,$discount=0,$price=null,$description=null)
	{
		if (strpos($external_item_kit_id_or_item_number, 'KIT') !== FALSE)
		{
			//KIT #
			$pieces = explode(' ',$external_item_kit_id_or_item_number);
			$item_kit_id = (int)$pieces[1];
		}
		else
		{
			$item_kit_id = $this->CI->Item_kit->get_item_kit_id($external_item_kit_id_or_item_number);
		}

		//make sure item exists
		if(!$this->CI->Item_kit->exists($item_kit_id))
		{
			return false;
		}

		$item_kit_info = $this->CI->Item_kit->get_info($item_kit_id);
		$item_kit_items = $this->CI->Item_kit_items->get_info($item_kit_id);
		if ( $item_kit_info->unit_price == null)
		{
			foreach ($item_kit_items as $item_kit_item)
			{
				for($k=0;$k<$item_kit_item->quantity;$k++)
				{
					$this->add_item_to_basket($item_kit_item->item_id, 1);
				}
			}

			return true;
		}
		else
		{
			$items = $this->get_basket();

	        //We need to loop through all items in the cart.
	        //If the item is already there, get it's key($updatekey).
	        //We also need to get the next key that we are going to use in case we need to add the
	        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

	        $maxkey=0;                       //Highest key so far
	        $itemalreadyinsale=FALSE;        //We did not find the item yet.
			$insertkey=0;                    //Key to use for new entry.
			$updatekey=0;                    //Key to use to update(quantity)

			foreach ($items as $item)
			{
	            //We primed the loop so maxkey is 0 the first time.
	            //Also, we have stored the key in the element itself so we can compare.

				if($maxkey <= $item['line'])
				{
					$maxkey = $item['line'];
				}

				if(isset($item['item_kit_id']) && $item['item_kit_id']==$item_kit_id)
				{
					$itemalreadyinsale=TRUE;
					$updatekey=$item['line'];
				}
			}

			$insertkey=$maxkey+1;

			//array/cart records are identified by $insertkey and item_id is just another field.
			$item = array(($insertkey)=>
			array(
				'item_kit_id'=>$item_kit_id,
				'line'=>$insertkey,
				'item_kit_number'=>$item_kit_info->item_kit_number,
				'name'=>$item_kit_info->name,
				'description'=>$description!=null ? $description: $item_kit_info->description,
				'is_punch_card'=>$item_kit_info->is_punch_card,
				'punch_card_data'=>array('punch_card_number'=>'','customer_id'=>'','expiration_date'=>'','item_kit_id'=>$item_kit_id, 'details'=>''),
				'quantity'=>$quantity,
	            'discount'=>$discount,
				'price'=>$price!=null ? $price: $item_kit_info->unit_price,
				'base_price'=>$price!=null ? $price: $item_kit_info->unit_price
				)
			);

			//Item already exists and is not serialized, add to quantity
			if($itemalreadyinsale)
			{
				$items[$updatekey]['quantity']+=$quantity;
			}
			else
			{
				//add to existing array
				$items+=$item;
			}

			$this->set_basket($items);
			return true;
		}
	}

	function add_tournament($name, $quantity)
	{
		$tournament_id = $this->CI->Tournament->get_id_by_name($name);

		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$tournament_info = $this->CI->Tournament->get_info($tournament_id);

		$cart_fee_before_taxes = $this->CI->Tournament->cart_fee_before_taxes($tournament_info);
		$green_fee_before_taxes =  $this->CI->Tournament->green_fee_before_taxes($tournament_info);
		$tournament_price_before_taxes = $this->CI->Tournament->tournament_price_before_taxes($tournament_info);
		$maximum_discount = floor((($cart_fee_before_taxes + $green_fee_before_taxes) / $tournament_price_before_taxes)*100);

		$tournament = array(($insertkey)=>
		array(
			'tournament_id'=>$tournament_id,
			'line'=>$insertkey,
			'name'=>$tournament_info->name,
			'is_serialized'=>TRUE,
			'quantity'=>$quantity,
            'discount'=>0,
			'price'=>number_format($tournament_price_before_taxes,2),
			'max_discount'=>$maximum_discount
			)
		);

		//add to existing array
		$items+=$tournament;
		$this->set_cart($items);

		return true;
	}

	function add_tournament_to_basket()
	{
		$tournament_id = $this->CI->Tournament->get_id_by_name($name);

		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$tournament_info = $this->CI->Tournament->get_info($tournament_id);
		$cart_fee_before_taxes = $this->CI->Tournament->cart_fee_before_taxes($tournament_info);
		$green_fee_before_taxes =  $this->CI->Tournament->green_fee_before_taxes($tournament_info);
		$tournament_price_before_taxes = $this->CI->Tournament->tournament_price_before_taxes($tournament_info);
		$maximum_discount = floor((($cart_fee_before_taxes + $green_fee_before_taxes) / $tournament_price_before_taxes)*100);

		$tournament = array(($insertkey)=>
		array(
			'tournament_id'=>$tournament_id,
			'line'=>$insertkey,
			'name'=>$tournament_info->name,
			'is_serialized'=>TRUE,
			'quantity'=>$quantity,
            'discount'=>0,
			'price'=>number_format($tournament_price_before_taxes,2),
			'max_discount'=>$maximum_discount
			)
		);

		//add to existing array
		$items+=$tournament;

		$this->set_basket($items);
		return true;
	}

	function add_invoice($invoice_info, $quantity)
	{
		// TODO: ACCOUNT FOR TAX INCLUDED WITHIN THIS FUNCTION
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}
		}

		$insertkey=$maxkey+1;

		//array/cart records are identified by $insertkey and item_id is just another field.
		$customer_info = $this->CI->Customer->get_info($invoice_info['person_id']);
		$invoice_id = $invoice_info['invoice_id'];
		$invoice_number = $invoice_info['invoice_number'];
		$name = "Invoice ".$invoice_info['invoice_number']." - Billed to ".$customer_info->first_name." ".$customer_info->last_name;
		$price = ($invoice_info['total'] -$invoice_info['paid']);
		$paid = $invoice_info['paid'];
		$invoice_items = $this->CI->Invoice->get_items($invoice_id);

		foreach ($invoice_items as $key => $invoice_item) {
			$payment_amount = ($invoice_item['amount'] + ($invoice_item['amount'] * ($invoice_item['tax']/100)))*$invoice_item['quantity'] - $invoice_item['paid_amount'];
			$invoice_items[$key]['payment_amount'] = $payment_amount;
		}
		$invoice_data_array = array(
					'invoice_id'=>$invoice_id,
					'invoice_number'=>$invoice_number,
					'line'=>$insertkey,
					'name'=>$name,
					'is_serialized'=>TRUE,
					'quantity'=>$quantity,
		            'discount'=>0,
					'price'=>$price,
					'paid'=>$paid,
					'max_discount'=>0,
					'is_invoice'=>TRUE,
					'invoice_items'=>$invoice_items
				);
		$invoice = array(
			($insertkey)=>$invoice_data_array
		);

		//add to existing array
		$items+=$invoice;
		$this->set_cart($items);
		$this->set_basket($items);

		return true;
	}
	function add_invoice_for_receipt($invoice_sales_info, $quantity)
	{

		//Get all items in the cart so far...
		$items = $this->get_cart();

        //We need to loop through all items in the cart.
        //If the item is already there, get it's key($updatekey).
        //We also need to get the next key that we are going to use in case we need to add the
        //item to the cart. Since items can be deleted, we can't use a count. we use the highest key + 1.

        $maxkey=0;                       //Highest key so far
        $itemalreadyinsale=FALSE;        //We did not find the item yet.
		$insertkey=0;                    //Key to use for new entry.
		$updatekey=0;                    //Key to use to update(quantity)

		foreach ($items as $item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.

			if($maxkey <= $item['line'])
			{
				$maxkey = $item['line'];
			}
		}

		$insertkey=$maxkey+1;

		$invoice_info = $this->CI->Invoice->get_info($invoice_sales_info['invoice_id']);
		$invoice_info = $invoice_info[0];
		$customer_info = $this->CI->Customer->get_info($invoice_info['person_id']);
		$invoice_id = $invoice_info['invoice_id'];
		$invoice_number = $invoice_info['invoice_number'];
		$name = "Invoice ".$invoice_info['invoice_number']." - Billed to ".$customer_info->first_name." ".$customer_info->last_name;
		$price = $invoice_sales_info['invoice_unit_price'];

		$invoice = array(
			($insertkey)=>
				array(
					'invoice_id'=>$invoice_id,
					'invoice_number'=>$invoice_number,
					'line'=>$insertkey,
					'name'=>$name,
					'is_serialized'=>TRUE,
					'quantity'=>$quantity,
		            'discount'=>0,
					'price'=>$price,
					'paid'=>$price,
					'max_discount'=>0,
					'is_invoice'=>TRUE,
					// 'invoice_items'=>$invoice_items
				)
		);

		$items+=$invoice;
		$this->set_cart($items);
		$this->set_basket($items);

		return true;
	}

	function is_unlimited($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}

		$item = $this->CI->Item->get_info($item_id);

		return $item->is_unlimited;
	}

	function out_of_stock($item_id)
	{
		//make sure item exists
		if(!$this->CI->Item->exists($item_id))
		{
			//try to get item id given an item_number
			$item_id = $this->CI->Item->get_item_id($item_id);

			if(!$item_id)
				return false;
		}

		$item = $this->CI->Item->get_info($item_id);
		$quanity_added = $this->get_quantity_already_added($item_id);

		if ($item->quantity - $quanity_added < 0)
		{
			return true;
		}

		return false;
	}

	function get_quantity_already_added($item_id)
	{
		$items = $this->get_cart();
		$quanity_already_added = 0;
		foreach ($items as $item)
		{
			if(isset($item['item_id']) && $item['item_id']==$item_id)
			{
				$quanity_already_added+=$item['quantity'];
			}
		}

		return $quanity_already_added;
	}

	function get_item_id($line_to_get)
	{
		$items = $this->get_cart();

		foreach ($items as $line=>$item)
		{
			if($line==$line_to_get)
			{
				return isset($item['item_id']) ? $item['item_id'] : -1;
			}
		}

		return -1;
	}

	function get_next_seat()
	{
		$items = $this->get_cart();
		$nextSeat = 0;
		foreach ($items as $line=>$item)
		{
			if(!empty($item['seat']) && $item['seat'] > $nextSeat)
			{
				$nextSeat = $item['seat'];
			}
		}

		return $nextSeat + 1;
	}

	function edit_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id = '')
	{
		$items = $this->get_cart();
		if(isset($items[$line]))
		{
            if ($item_id != '' && $this->CI->Item->exists($item_id)) {
                $item_info = $this->CI->Item->get_info($item_id);
                $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $item_info->unit_price;
			}
	        else {
	            $items[$line]['description'] = $description;
	            $items[$line]['price'] = $price;
			}



			$items[$line]['serialnumber'] = $serialnumber;
			$items[$line]['quantity'] = $quantity;
			$items[$line]['discount'] = (float)$discount > (float)$items[$line]['max_discount'] ? $items[$line]['max_discount'] : $discount;
		//print_r($items[$line]);
                    	$this->set_cart($items);
		}

		return false;
	}
	function edit_basket_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id = '')
	{
		$items = $this->get_basket();
		if(isset($items[$line]))
		{
            if ($item_id != '' && $this->CI->Item->exists($item_id)) {
                $item_info = $this->CI->Item->get_info($item_id);
                $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $item_info->unit_price;
			}
	        else {
	            $items[$line]['description'] = $description;
	            $items[$line]['price'] = $price;
			}



			$items[$line]['serialnumber'] = $serialnumber;
			$items[$line]['quantity'] = $quantity;
			$items[$line]['discount'] = (float)$discount > (float)$items[$line]['max_discount'] ? $items[$line]['max_discount'] : $discount;
		//print_r($items[$line]);
            $this->set_basket($items);
		}

		return false;
	}
	function get_teetime_price($price_category_index = null, $teetime_type_index = null)
	{

		$teetime_type_index = $teetime_type_index != null ? $teetime_type_index : $this->get_teetime_type();
		$price_category_index = $price_category_index != null ? $price_category_index : $this->get_price_category();

		$item_prices = ($this->CI->permissions->course_has_module('reservations')?$this->CI->Fee->get_info($this->CI->session->userdata('course_id').'_'.$teetime_type_index, $this->CI->session->userdata('teesheet_id'),'price_category_'.$price_category_index):$this->CI->Green_fee->get_info($this->CI->session->userdata('course_id').'_'.$teetime_type_index, $this->CI->session->userdata('teesheet_id'),'price_category_'.$price_category_index));
        //print_r($item_prices);
        return (float)$item_prices[$this->CI->session->userdata('teesheet_id')][$this->CI->session->userdata('course_id').'_'.$teetime_type_index]->price;
	}
	function get_price_category()
	{
		return substr($this->CI->input->post('item_number'), 0, strpos($this->CI->input->post('item_number'), '_'));
	}
	function get_teetime_type()
	{
		return substr($this->CI->input->post('item_number'), strpos($this->CI->input->post('item_number'), '_')+1);
	}
    function edit_item_attribute($line, $attribute = '', $value = '', $item_id = '')
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$items = $this->get_cart();
		if(isset($items[$line]))
		{
			if ($this->CI->input->post('item_number')) {
                $item_info = $this->CI->Item->get_info($item_id);
			    $items[$line]['item_id'] = $item_id;
                $items[$line]['name'] = $item_info->name;
				$items[$line]['item_number'] = $this->CI->session->userdata('course_id').'_'.$this->get_teetime_type();
				$items[$line]['price_category'] = $this->get_price_category();
				$items[$line]['teetime_type'] = $this->get_teetime_type();
                $items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $this->get_teetime_price();
			    $items[$line]['base_price'] = $items[$line]['price'];
				$items[$line]['subtotal'] = $this->calculate_item_subtotal($items[$line]['price'] , $items[$line]['quantity'], $items[$line]['discount']);
				$items[$line]['tax'] = $this->calculate_item_tax($items[$line]['subtotal'], $item_info->item_id, 'item');;
				$items[$line]['total'] = $items[$line]['subtotal'] + $items[$line]['tax'];
			}
            else
            {
            	$min_price = $items[$line]['base_price'] * (100 - $items[$line]['max_discount']) / 100;

				if ($this->CI->permissions->is_employee() && !$this->CI->permissions->is_manager() && !$this->CI->session->userdata('purchase_override'))
				{
					if ($attribute == 'discount')
					{
						$new_price = $items[$line]['price'] * (100 - $value) / 100;
						if ($new_price < $min_price)
						{
							$value = $items[$line]['max_discount'];
							$items[$line]['price'] = $items[$line]['base_price'];
						}
					}
					else if ($attribute == 'price')
					{
						$new_price = $value * (100 - $items[$line]['discount']) / 100;
						if ($new_price < $min_price)
						{
							$value = $min_price;
							$items[$line]['discount'] = 0;
						}
					}
				}
                $items[$line][$attribute] = $value;

				// Recalculate item subtotal, total and tax amount
				$item = $items[$line];
				$subtotal = $this->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);

				if(!empty($item['item_id'])){
					$tax = $this->calculate_item_tax($subtotal, $item['item_id'], 'item');
				}else{
					$tax = $this->calculate_item_tax($subtotal, $item['item_kit_id'], 'item_kit');
				}

				if ($tax_included)
				{
					$total = $subtotal;
	                $items[$line]['subtotal'] = $subtotal - $tax;
				}
				else
				{
					$total = $subtotal + $tax;
	                $items[$line]['subtotal'] = $subtotal;
				}

                $items[$line]['tax'] = $tax;
                $items[$line]['total'] = $total;
			}

            $this->set_cart($items);

            return array(
				'seat'=>(int)$items[$line]['seat'],
				'new_price'=>(float)$new_price,
				'min_price'=>(float)$min_price,
				'base_price'=>(float)$items[$line]['base_price'],
				'price'=>(float)$items[$line]['price'],
				'modifier_total'=>(float)$items[$line]['modifier_total'],
				'quantity'=>(int)$items[$line]['quantity'],
				'discount'=>(float)$items[$line]['discount'],
				'max_discount'=>(float)$items[$line]['max_discount'],
				'subtotal'=>$items[$line]['subtotal'],
				'tax'=>$items[$line]['tax'],
				'total'=>$items[$line]['total']
			);
        }
		return false;
	}
    function edit_basket_item_attribute($line, $attribute = '', $value = '', $item_id = '')
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$items = $this->get_basket();
		$types = ($this->CI->permissions->course_has_module('reservations')?$this->CI->Fee->get_type_info():$this->CI->Green_fee->get_type_info());
		$teesheet_id = $this->CI->session->userdata('teesheet_id');
		if(isset($items[$line]))
		{
            if ($this->CI->input->post('item_number')) {
                $item_info = $this->CI->Item->get_info($item_id);
			    $items[$line]['item_id'] = $item_id;
                $items[$line]['item_number'] = $this->CI->session->userdata('course_id').'_'.$this->get_teetime_type();
				$price_category_index = $this->get_price_category();
				$items[$line]['price_category'] = $price_category_index;
                $items[$line]['teetime_type'] = $this->get_teetime_type();
				$price_category = 'price_category_'.$price_category_index;
                $items[$line]['name'] = $item_info->name." ".$types[$teesheet_id]->$price_category;
				$items[$line]['description'] = $item_info->description;
                $items[$line]['price'] = $this->get_teetime_price();
			    $items[$line]['base_price'] = $items[$line]['price'];
				$items[$line]['subtotal'] = $this->calculate_item_subtotal($items[$line]['price'] , $items[$line]['quantity'], $items[$line]['discount']);
				$items[$line]['tax'] = $this->calculate_item_tax($items[$line]['subtotal'], $item_info->item_id, 'item');;
				$items[$line]['total'] = $items[$line]['subtotal'] + $items[$line]['tax'];
			}
            else
            {
            	$min_price = $items[$line]['base_price'] * (100 - $items[$line]['max_discount']) / 100;
            	//$this->CI->permissions->is_employee();
				//	echo 'stuff';
				if ($this->CI->permissions->is_employee() && !$this->CI->permissions->is_manager() && !$this->CI->session->userdata('purchase_override'))
				{
					if ($attribute == 'discount')
					{
						$new_price = $items[$line]['price'] * (100 - $value) / 100;
						if ($new_price < $min_price)
						{
							$value = $items[$line]['max_discount'];
							$items[$line]['price'] = $items[$line]['base_price'];
						}
					}
					else if ($attribute == 'price')
					{
						$new_price = $value * (100 - $items[$line]['discount']) / 100;
						if ($new_price < $min_price)
						{
							$value = $min_price;
							$items[$line]['discount'] = 0;
						}
					}
				}
				$items[$line][$attribute] = $value;

				// Recalculate item subtotal, total and tax amount
				$item = $items[$line];
				$subtotal = $this->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);

				if(!empty($item['item_id'])){
					$tax = $this->calculate_item_tax($subtotal, $item['item_id'], 'item');
				}else{
					$tax = $this->calculate_item_tax($subtotal, $item['item_kit_id'], 'item_kit');
				}

				if ($tax_included)
				{
					$total = $subtotal;
	                $items[$line]['subtotal'] = $subtotal - $tax;
				}
				else
				{
					$total = $subtotal + $tax;
	                $items[$line]['subtotal'] = $subtotal;
				}

                $items[$line]['tax'] = $tax;
                $items[$line]['total'] = $total;
			}
			$this->set_basket($items);

            return array(
				'seat'=>(int)$items[$line]['seat'],
				'new_price'=>(float)$new_price,
				'min_price'=>(float)$min_price,
				'base_price'=>(float)$items[$line]['base_price'],
				'price'=>(float)$items[$line]['price'],
				'modifier_total'=>(float)$items[$line]['modifier_total'],
				'quantity'=>(int)$items[$line]['quantity'],
				'discount'=>(float)$items[$line]['discount'],
				'max_discount'=>(float)$items[$line]['max_discount'],
				'subtotal'=>$items[$line]['subtotal'],
				'tax'=>$items[$line]['tax'],
				'total'=>$items[$line]['total']
			);
        }

		return false;
	}

	function is_valid_receipt($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);

		if(count($pieces)==2 && $pieces[0] == 'POS')
		{
			return $this->CI->Sale->exists($pieces[1]);
			// TODO: DELETE ABOVE RETURN AFTER SALE NUMBER IS IMPLEMENTED
			$sale_id = $this->CI->Sale->get_sale_id($pices[1]);
			return $sale_id != -1;
		}

		return false;
	}

	function is_valid_raincheck($raincheck_number)
	{
		//RID #
		$pieces = explode(' ',$raincheck_number);

		if(count($pieces)==2 && strtoupper($pieces[0]) == 'RID')
		{
			$raincheck_id = $this->CI->Sale->get_raincheck_id($pieces[1]);
			return $this->CI->Sale->raincheck_exists($raincheck_id);
		}

		return false;
	}
	function raincheck_is_used_or_expired($raincheck_number)
	{
		//RID #
		$pieces = explode(' ',$raincheck_number);

		if(count($pieces)==2 && strtoupper($pieces[0]) == 'RID')
		{
			$raincheck_id = $this->CI->Sale->get_raincheck_id($pieces[1]);
			return $this->CI->Sale->raincheck_is_used_or_expired($raincheck_id);
		}

		return false;
	}

	function is_valid_item_kit($item_kit_id)
	{
		//KIT #
		$pieces = explode(' ',$item_kit_id);

		if(count($pieces)==2 && $pieces[0] == 'KIT')
		{
			return $this->CI->Item_kit->exists($pieces[1]);
		}
		else
		{
			return $this->CI->Item_kit->get_item_kit_id($item_kit_id) !== FALSE;
		}
	}

	function is_valid_tournament($name)
	{
		return $this->CI->Tournament->get_id_by_name($name);//tells us if it exists or not
	}

	function is_valid_invoice($invoice_number, &$invoice_info)
	{
		//INV #
		$pieces = explode(' ',$invoice_number);
		if(count($pieces)==2 && strtoupper( $pieces[0]) == 'INV')
		{
			$invoice_info = $this->CI->Invoice->get_by_invoice_number($pieces[1]);
			return $invoice_info;
		}

		return false;
	}

	function invoice_in_cart($invoice_number, $invoice_info)
	{
		$pieces = explode(' ',$invoice_number);

		$items = $this->get_cart();

		if(count($pieces)==2 && $pieces[0] == 'INV')
		{
			foreach ($items as $key => $item) {
				if ($item['invoice_number'] === $pieces[1]) return true;
			}
		}

		return false;
	}

	function return_entire_sale($receipt_sale_id)
	{
		//POS #
		$pieces = explode(' ',$receipt_sale_id);
		$sale_id = $pieces[1];
		//$sale_id = $this->CI->Sale->get_sale_id($sale_id);
		//echo 'sale_id '.$sale_id;
		$this->CI->session->set_userdata('return_sale_id', $sale_id);
		$this->empty_cart();
                $this->empty_basket();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,-$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
			$this->add_item_to_basket($row->item_id,-$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale->get_sale_item_kits($sale_id)->result() as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,-$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
			$this->add_item_kit_to_basket('KIT '.$row->item_kit_id,-$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}
		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);
	}
	function apply_raincheck($raincheck_number)
	{
		//RID #
		$pieces = explode(' ',$raincheck_number);
		$raincheck_id = $this->CI->Sale->get_raincheck_id($pieces[1]);
		$this->CI->session->set_userdata('raincheck_id', $raincheck_id);
	}
	function remove_raincheck()
	{
		$this->delete_payment('Raincheck');
		$this->CI->session->unset_userdata('raincheck_id');
	}
	function get_raincheck() {
		//TODO: ADD IN TAX INCLUDED FUNCTIONALITY HERE
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$raincheck_id = $this->CI->session->userdata('raincheck_id');
		$ri = $this->CI->Sale->raincheck_info($raincheck_id)->row_array();
		$basket = $this->get_basket();
		//print_r($basket);
		//print_r($ri);
		foreach($basket as $item)
		{
			$itt = $item['teetime_type'];
			$ri['cf_used'] += ($itt==1||$itt==2||$itt==5||$itt==6)?($item['price']*$item['quantity']):0;
			$ri['gf_used'] += ($itt==3||$itt==4||$itt==7||$itt==8)?($item['price']*$item['quantity']):0;
		}
		$ri['cf_total'] = $ri['cart_fee'] * $ri['players'];
		$ri['gf_total'] = $ri['green_fee'] * $ri['players'];
		$ri['tax_total'] = $ri['tax'];
		$ri['cf_used'] = ($ri['cf_used'] > $ri['cf_total'])?$ri['cf_total']:$ri['cf_used'];
		$ri['gf_used'] = ($ri['gf_used'] > $ri['gf_total'])?$ri['gf_total']:$ri['gf_used'];
		$ri['tax_used'] = number_format(($ri['cf_used'] + $ri['gf_used'])/($ri['cf_total'] + $ri['gf_total']) * $ri['tax'], 2);
	//	$raincheck_info['cf_taxes'] = $raincheck_info['cart_fee'] * $raincheck_info['players'];
		//$raincheck_info['gf_taxes'] = $raincheck_info['green_fee'] * $raincheck_info['players'];
		$payment_amount = $ri['gf_used'] + $ri['cf_used'] + $ri['tax_used'];
		//echo 'editing payment';
		if (!$this->edit_payment('Raincheck', $payment_amount))
			$this->add_payment('Raincheck', $payment_amount);
		//echo 'done editing payment';
		return $ri;
	}
	function put_cart_into_basket() {
        $this->empty_basket();
        $items = $this->get_cart();
        $this->set_basket($items);
    }
	function copy_entire_sale($sale_id)
	{
		$this->empty_cart();
		/*empty the basket too????????????*/
		$this->empty_basket();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();

		foreach($this->CI->Sale->get_sale_items($sale_id)->result() as $row)
		{
			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
			$this->add_item_to_basket($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber);
		}
		foreach($this->CI->Sale->get_sale_invoices($sale_id)->result_array() as $row)
		{
			$quantity = 1;
			$this->add_invoice_for_receipt($row,$quantity);

		}
		foreach($this->CI->Sale->get_sale_item_kits($sale_id)->result() as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
			$this->add_item_kit_to_basket('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}
		foreach($this->CI->Sale->get_sale_payments($sale_id)->result() as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}

		$this->set_customer($this->CI->Sale->get_customer($sale_id)->person_id);

	}

	function copy_entire_suspended_sale($sale_id)
	{
		$this->empty_cart();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		$cart_popups = array();

		// Retrieve all saved modifiers for suspended sale
		// Stored in dimensional array by line
		$lineModifiers = $this->CI->Sale_suspended->get_sale_item_modifiers($sale_id);

		foreach(array_reverse($this->CI->Sale_suspended->get_sale_items($sale_id)->result()) as $row)
		{
			// Add saved modifiers to item
			$line = $row->line;
			if(!empty($lineModifiers[$line])){
				$itemModifiers = $lineModifiers[$line];
				$modifierTotal = $this->calculate_modifier_total($itemModifiers);
			}else{
				$itemModifiers = null;
				$modifierTotal = 0.00;
			}

			$cart_popups[$row->item_id.'-'.$row->line] = true;

			// Adjust item price back to to normal. Modifier total was added
			// in to item price before sale suspension.
			$row->item_unit_price -= $modifierTotal;

			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber,$row->price_category,null,$itemModifiers);
			$this->add_item_to_basket($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber,$row->price_category,null,$itemModifiers);
		}
		$this->CI->session->set_userdata('cart_popups',$cart_popups);

		foreach(array_reverse($this->CI->Sale_suspended->get_sale_item_kits($sale_id)->result()) as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
			$this->add_item_kit_to_basket('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}

		foreach(array_reverse($this->CI->Sale_suspended->get_sale_payments($sale_id)->result()) as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}
		/*
		$items = $this->get_cart();

		foreach($items as $item){
			$line = $item['line'];
			if(!empty($lineModifiers[$line])){

				$items[$line]['modifiers'] = $lineModifiers[$line];
				$items[$line]['modifier_total'] = to_currency_no_money($this->calculate_modifier_total($lineModifiers[$line]));

				$items[$line]['base_price'] -= $items[$line]['modifier_total'];
				$items[$line]['price'] -= $items[$line]['modifier_total'];

				$items[$line]['base_price'] = to_currency_no_money($items[$line]['base_price']);
				$items[$line]['price'] = to_currency_no_money($items[$line]['price']);
			}
		}

		$this->set_cart($items);
		$this->set_basket($items);
		* */

		$this->set_customer($this->CI->Sale_suspended->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Sale_suspended->get_comment($sale_id));
	}

	function get_suspended_sale_id()
	{
		return $this->CI->session->userdata('suspended_sale_id');
	}

	function set_suspended_sale_id($suspended_sale_id)
	{
		$this->CI->session->set_userdata('suspended_sale_id',$suspended_sale_id);
	}

	function delete_suspended_sale_id()
	{
		$this->CI->session->unset_userdata('suspended_sale_id');
	}
	function copy_entire_table($sale_id)
	{
		$this->empty_cart();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();

		// Retrieve all saved modifiers for suspended sale
		// Stored in dimensional array by line
		$lineModifiers = $this->CI->Table->get_sale_item_modifiers($sale_id);

		foreach(array_reverse($this->CI->Table->get_sale_items($sale_id)->result()) as $row)
		{
			// Add saved modifiers to item
			$line = $row->line;
			if(!empty($lineModifiers[$line])){
				$itemModifiers = $lineModifiers[$line];
				$modifierTotal = $this->calculate_modifier_total($itemModifiers);
			}else{
				$itemModifiers = null;
				$modifierTotal = 0.00;
			}
			// Adjust item price back to to normal. Modifier total was added
			// in to item price before sale suspension.
			$row->item_unit_price -= $modifierTotal;

			$this->add_item($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber,null,null,$itemModifiers);
			$this->add_item_to_basket($row->item_id,$row->quantity_purchased,$row->discount_percent,$row->item_unit_price,$row->description,$row->serialnumber,null,null,$itemModifiers);
		}

		foreach(array_reverse($this->CI->Table->get_sale_item_kits($sale_id)->result()) as $row)
		{
			$this->add_item_kit('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
			$this->add_item_kit_to_basket('KIT '.$row->item_kit_id,$row->quantity_purchased,$row->discount_percent,$row->item_kit_unit_price,$row->description);
		}

		foreach(array_reverse($this->CI->Table->get_sale_payments($sale_id)->result()) as $row)
		{
			$this->add_payment($row->payment_type,$row->payment_amount);
		}

		$this->set_customer($this->CI->Table->get_customer($sale_id)->person_id);
		$this->set_comment($this->CI->Table->get_comment($sale_id));
	}

	function get_table_id()
	{
		return $this->CI->session->userdata('table_id');
	}

	function set_table_id($table_id)
	{
		$this->CI->session->set_userdata('table_id',$table_id);
	}

	function delete_table_id()
	{
		$this->CI->session->unset_userdata('table_id');
	}
    function get_table_number()
	{
		return $this->CI->session->userdata('table_number');
	}

	function set_table_number($table_number)
	{
		$this->CI->session->set_userdata('table_number',$table_number);
	}

	function delete_table_number()
	{
		$this->CI->session->unset_userdata('table_number');
	}
    function get_basket_info(){
        $items_in_basket = $this->get_items_in_basket();
        $subtotal = $this->get_basket_subtotal();
        $total = $this->get_basket_total();
        $amount_due = $this->get_basket_amount_due();
        $taxes = $this->get_basket_taxes();
        $formatted_taxes = array();
        foreach($taxes as $name => $amount){
            $formatted_taxes[] = array('name' => $name, 'amount' => $amount);
        }
        return array('items_in_basket'=>$items_in_basket, 'subtotal'=>$subtotal, 'total'=>$total, 'amount_due'=>$amount_due, 'taxes'=>$formatted_taxes);
    }
	function delete_item($line)
	{
		$cart_popups = $this->CI->session->userdata('cart_popups');
		$item_id = $this->get_item_id($line);
		unset($cart_popups[$item_id.'-'.$line]);
		$this->CI->session->set_userdata('cart_popups',$cart_popups);

		$items=$this->get_cart();
		unset($items[$line]);
		$this->set_cart($items);
	}
        function copy_item_into_basket($line) {
            $items = $this->get_cart();
            $basket_items = $this->get_basket();
            $basket_items[$line] = $items[$line];

            $this->set_basket($basket_items);
/*            $this->add_item_to_basket($items[$line]['item_id'], $items[$line]['quantity'], $items[$line]['discount'],
                    $items[$line]['price'], $items[$line]['description'], $items[$line]['serialnumber']);
 */
        }
    function delete_item_from_basket($line)
	{
        $items=$this->get_basket();
        unset($items[$line]);
        $this->set_basket($items);


    }

	function empty_cart()
	{
		$this->CI->session->unset_userdata('cart');
	}

    function empty_basket()
	{
        $this->CI->session->unset_userdata('basket');
    }

	function delete_customer()
	{
		$this->CI->session->unset_userdata('customer');
	}

	function delete_customer_quickbuttons()
	{
		$this->CI->session->unset_userdata('customer_quickbuttons');
	}

	function clear_mode()
	{
		$this->CI->session->unset_userdata('sale_mode');
	}

	function clear_all()
	{
		$this->clear_mode();
		$this->empty_cart();
		$this->empty_basket();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->clear_transaction_taxes();
		$this->empty_payments();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		$this->delete_suspended_sale_id();
		$this->delete_table_id();
		$this->CI->session->unset_userdata('cart_popups');
	}
    function clear_all_minus_cart()
    {
        $basket_data = $this->get_basket();
        $cart_data = $this->get_cart();
        foreach($basket_data as $line => $item) {
            unset($cart_data[$line]);
        }
        $this->set_cart($cart_data);

		$this->clear_mode();
		$this->empty_basket();
		$this->clear_comment();
		$this->clear_email_receipt();
		$this->clear_transaction_taxes();
		$this->empty_payments();
		$this->delete_customer();
		$this->delete_customer_quickbuttons();
		$this->delete_suspended_sale_id();
		$this->delete_table_id();
		$this->CI->session->unset_userdata('cart_popups');
    }
	function get_taxes($sale_id = false, $from_cart = true)
	{
		$taxes = array();
		$tax_included = $this->CI->config->item('unit_price_includes_tax');

		if ($sale_id)
		{
			$taxes_from_sale = array_merge($this->CI->Sale->get_sale_items_taxes($sale_id), $this->CI->Sale->get_sale_item_kits_taxes($sale_id));
			foreach($taxes_from_sale as $key => $tax_item)
			{
				$p = $tax_item['price'];
                $q = $tax_item['quantity'];
                $d = $tax_item['discount'];
                $t = $tax_item['percent'];
				$total_price = ($p + $tax_item['modifier_total']) * $q;
				$discounted_price = round($total_price - ($total_price * ($d / 100)), 2);
				$name = $t.'% ' . $tax_item['name'];

				if ($tax_included)
                {
                    $actual_price = to_currency_no_money(($discounted_price / (1 + $t /100)));
                    $tax_amount = ($discounted_price - $actual_price);
                }
                else if ($tax_item['cumulative'])
				{
					$prev_tax = $taxes[$taxes_from_sale[$key - 1]['percent'].'% ' . $taxes_from_sale[$key - 1]['name']];
					$tax_amount = ($discounted_price + $prev_tax) * ($t / 100);
				}
				else
				{
					$tax_amount = $discounted_price * ($t / 100);
				}

				if (!isset($taxes[$name]))
				{
					$taxes[$name] = 0;
				}
				$taxes[$name] += number_format($tax_amount,2);
			}
		}
		else
		{
			$customer_id = $this->get_customer();
			$customer = $this->CI->Customer->get_info($customer_id);

			//Do not charge sales tax if we have a customer that is not taxable
			if ($this->CI->session->userdata('taxable') == 'false' || (!$customer->taxable and $customer_id!=-1))
			{
			   return array();
			}

			if($from_cart)
			{
				$items = $this->get_cart();
			}else{
				$items = $this->get_basket();
			}

			foreach($items as $line => $item)
			{
				$p = $item['price'];
				$q = $item['quantity'];
				$d = $item['discount'];
				$total_price = ($p + $item['modifier_total']) * $q;
				$discounted_price = round($total_price - ($total_price * ($d / 100)), 2);

				if (isset($item['tournament_id']) && $item['tournament_id'] != '')
				{
					//the taxes array is changed in the following function
					$this->CI->Tournament->calculate_tournament_taxes($item['tournament_id'], $d, $taxes);
				}
				else
				{
					$manual_taxes = $this->CI->session->userdata('manual_taxes');
					$tax_info = isset($item['item_id']) ? $this->CI->Item_taxes->get_info($item['item_id'], $manual_taxes) : $this->CI->Item_kit_taxes->get_info($item['item_kit_id'], $manual_taxes);
					foreach($tax_info as $key => $tax)
					{
						$t = $tax['percent'];
						$name = $t.'% ' . $tax['name'];

						if ($tax_included)
                        {
                            $actual_price = to_currency_no_money(($discounted_price / (1 + $t /100)));
                            $tax_amount = ($discounted_price - $actual_price);
                        }
						else if (!empty($tax['cumulative']))
						{
							$prev_tax = $taxes[$tax_info[$key - 1]['percent'].'% ' . $tax_info[$key - 1]['name']];
							$tax_amount = ($discounted_price + $prev_tax) * ($t / 100);
						}else
						{
							$tax_amount = $discounted_price * ($t / 100);
						}

						if (!isset($taxes[$name]))
						{
							$taxes[$name] = 0;
						}
						$taxes[$name] += number_format($tax_amount,2);
					}
				}
			}
		}

		return $taxes;
	}

    function get_basket_taxes($sale_id = false)
	{
		return $this->get_taxes($sale_id, false);
	}

	function get_items_in_cart()
	{
		$items_in_cart = 0;
		foreach($this->get_cart() as $item)
		{
		    $items_in_cart+=$item['quantity'];
		}

		return $items_in_cart;
	}

	function get_items_in_basket()
	{
		$items_in_basket = 0;
		foreach($this->get_basket() as $item)
		{
		    $items_in_basket+=$item['quantity'];
		}

		return $items_in_basket;
	}

	function get_invoice_in_cart($line)
	{
		foreach ($this->get_cart() as $key => $value) {
			if ($line == $key) {
				return $value;
			}
		}
	}

	function get_subtotal()
	{
		$subtotal = 0;
		foreach($this->get_cart() as $item)
		{
            $subtotal += $this->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);
		}
		return round($subtotal, 2);
	}

	function get_total($sale_id = false)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$total = 0;
		foreach($this->get_cart() as $item)
		{
            $total += $this->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);
		}
		if (!$tax_included)
        {
			foreach($this->get_taxes($sale_id) as $tax)
			{
				$total += $tax;
			}
		}

		return round($total, 2);
	}

	function calculate_item_subtotal($price, $quantity, $discount){
		return round(($price * ((100 - $discount) / 100)) * $quantity, 2);
	}

	// Takes a item dollar amount and calculates amount of tax for that
	// amount based on item
	function calculate_item_tax($subtotal, $item_id, $item_type){
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$manual_taxes = $this->CI->session->userdata('manual_taxes');
		if($item_type == 'item'){
			$taxes = $this->CI->Item_taxes->get_info($item_id, $manual_taxes);

		}else if($item_type == 'item_kit'){
			$taxes = $this->CI->Item_kit_taxes->get_info($item_id, $manual_taxes);

		}else{
			return false;
		}

		$total_tax = 0;
		$tax_amounts = array();
		foreach($taxes as $key => $tax)
		{
			$t = $tax['percent'];
			$tax_name = $taxes[$key]['percent'].'% '.$taxes[$key]['name'];
			$prev_tax_name = $taxes[$key - 1]['percent'].'% '.$taxes[$key - 1]['name'];

			$prev_tax_amount = 0;
			if(isset($tax_amounts[$prev_tax_name]))
			{
				$prev_tax_amount = $tax_amounts[$prev_tax_name];
			}

			if ($tax_included)
            {
                $actual_price = round(($subtotal / (1 + $t /100)), 2);
                $tax_amount = ($subtotal - $actual_price);
            }
            else if (!empty($tax['cumulative']))
			{
				$tax_amount = ($subtotal + $prev_tax_amount) * ($t / 100);
			}else
			{
				$tax_amount = $subtotal * ($t / 100);
			}

			$tax_amounts[$tax_name] = $tax_amount;
		}

		$total_tax_amount = 0;
		foreach($tax_amounts as $tax_name => $tax)
		{
			$total_tax_amount += $tax;
		}

		return round($total_tax_amount, 2);
	}

	function refresh_item_totals($line){
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$cart = $this->get_cart();
		$item = $cart[$line];

		$subtotal = $this->calculate_item_subtotal($item['price'] + $item['modifier_total'], $item['quantity'], $item['discount']);

		$tax = 0;
		if(!empty($item['item_id'])){
			$tax = $this->calculate_item_tax($subtotal, $item['item_id'], 'item');
		}else if(!empty($item['item_kit_id'])){
			$tax = $this->calculate_item_tax($subtotal, $item['item_kit_id'], 'item_kit');
		}
		// ADJUST SUBTOTAL AND TOTAL IF TAXES ARE INCLUDED IN PRICE
		if ($tax_included)
		{
			$item['subtotal'] = $subtotal - $tax;
			$total = $subtotal;
		}
		else
		{
			$item['subtotal'] = $subtotal;
			$total = $subtotal + $tax;
		}

		$amount_due = $total - $item['amount_paid'];

		$item['tax'] = $tax;
		$item['total'] = $total;
		$item['amount_due'] = $amount_due;
		$cart[$line] = $item;
		$this->set_cart($cart);
	}

	// Returns total of each receipt in sale
	function get_receipt_total($receipt_id = false){
		$receipt_totals = array();
		$receipt_items = array();
		$item_counts = array();

		// Categorize items by receipt_id
		foreach($items as $item){
			$receipt_items[$item['receipt_id']] = $item;
		}

		foreach($items as $item){

		}

		// Loop through receipts,
		foreach($receipt_items as $receipt_id => $items){

			foreach($items as $cart_item){

				$cart_item = $cart_items[$item['item_line']];
				$item['price'] = $cart_item['price'] + $cart_item['modifier_total'];
				$item['seat'] = $cart_item['seat'];
				$item['price'] = round($item['price'] - ($item['price'] * ($cart_item['discount'] / 100)), 2);

				$item['tax'] = $this->sale_lib->get_item_tax_amount($item['item_line']);
				$receipts[$receipt_id]['items'][] = $item;

				if(!isset($item_counts[$item['item_id']])){
					$item_counts[$item['item_id']] = 1;
				}else{
					$item_counts[$item['item_id']] += 1;
				}
			}
		}
	}
        /*function get_basket_total($sale_id = false)
	{
		$total = 0;
		foreach($this->get_basket() as $item)
		{
            $total+=($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100);
		}

		foreach($this->get_taxes($sale_id) as $tax)
		{
			$total+=$tax;
		}

		return to_currency_no_money($total);
	}*/
    function get_basket_subtotal()
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$subtotal = 0;
		foreach($this->get_basket() as $item)
		{
			$subtotal+=(($item['price'] + $item['modifier_total'])*$item['quantity']-($item['price'] + $item['modifier_total'])*$item['quantity']*$item['discount']/100);
		}
		if ($tax_included)
        {
			foreach($this->get_basket_taxes($sale_id) as $tax)
			{
				$subtotal-=$tax;
			}
		}
		return to_currency_no_money($subtotal);
	}

	function get_basket_total($sale_id = false)
	{
		$tax_included = $this->CI->config->item('unit_price_includes_tax');
		$total = 0;
		foreach($this->get_basket() as $item)
		{
            $total+=(($item['price'] + $item['modifier_total'])*$item['quantity']-($item['price'] + $item['modifier_total'])*$item['quantity']*$item['discount']/100);
		}
		if (!$tax_included)
        {
			foreach($this->get_basket_taxes($sale_id) as $tax)
			{
				$total+=$tax;
			}
		}

		return to_currency_no_money($total);
	}

	/*******************************************************************
	/* New functions to replace existing similar functions above
	/*******************************************************************/

	// Calculate subtotal of item
	function calculate_subtotal($price, $quantity, $discount){
		return round(($price * ((100 - $discount) / 100)) * $quantity, 2);
	}

	// Taxes a list of taxes and item amount, returns total tax amount
	function calculate_tax($subtotal, $taxes){

		$total_tax = 0;
		$tax_amounts = array();
		foreach($taxes as $key => $tax)
		{
			$tax_name = $taxes[$key]['percent'].'% '.$taxes[$key]['name'];
			$prev_tax_name = $taxes[$key - 1]['percent'].'% '.$taxes[$key - 1]['name'];

			$prev_tax_amount = 0;
			if(isset($tax_amounts[$prev_tax_name]))
			{
				$prev_tax_amount = $tax_amounts[$prev_tax_name];
			}

			if (!empty($tax['cumulative']))
			{
				$tax_amount = round(($subtotal + $prev_tax_amount) * ($tax['percent'] / 100), 2);
			}else
			{
				$tax_amount = round($subtotal * ($tax['percent'] / 100), 2);
			}

			$tax_amounts[$tax_name] = $tax_amount;
		}

		$total_tax_amount = 0;
		foreach($tax_amounts as $tax_name => $tax)
		{
			$total_tax_amount += $tax;
		}

		return round($total_tax_amount, 2);
	}
}
?>
<?php
class Customer_billing extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($billing_id)
	{
		$this->db->from('customer_billing');
		$this->db->where("billing_id", $billing_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($customer_id = false, $limit=10000, $offset=0, $join = false)
	{
		$this->db->from('customer_billing');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if ($customer_id)
			$this->db->where('customer_billing.person_id', $customer_id);
		if ($join)
		{
			$this->db->join('people', 'people.person_id = customer_billing.person_id');
			$this->db->order_by('last_name, first_name');
		}
		
		$this->db->where('customer_billing.deleted !=', 1);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*
	Returns all billings in a time range
	*/
	function get_all_in_range($start_date, $end_date, $limit = 10000, $offset = 0)
	{
		$month = date('n', strtotime($start_date));
		$day = date('j');
		$today = date('Y-m-d');
		$start_date = date('Y-m-d',strtotime($start_date));
		$start_date_day = date('j',strtotime($start_date));
		$end_date_day = date('j',strtotime($end_date));
		
		return $this->db->query("SELECT
					token,
					cardholder_name,
					billing_id,
					`foreup_customer_billing`.person_id AS person_id,
					pay_account_balance,
					pay_member_balance,
					email_invoice,
					foreup_customer_billing.course_id AS course_id,
					foreup_customer_billing.credit_card_id AS credit_card_id
					FROM (`foreup_customer_billing`) 
					LEFT OUTER JOIN `foreup_customer_credit_cards` ON `foreup_customer_billing`.`credit_card_id` = `foreup_customer_credit_cards`.`credit_card_id`					
					LEFT OUTER JOIN `foreup_customers` ON `foreup_customers`.`person_id` = `foreup_customer_billing`.`person_id`
					WHERE `foreup_customer_billing`.`deleted` != 1 
					AND foreup_customer_billing.course_id = {$this->session->userdata('course_id')}
					AND ((frequency = 'monthly' AND day >= $start_date_day AND day <= $end_date_day) OR (frequency = 'yearly' AND day >= $start_date_day AND day <= $end_date_day AND month = $month))  
					AND `foreup_customers`.`deleted` = 0
					AND `start_date` <= '$start_date'
					AND ((start_month <= end_month AND start_month <= $month AND end_month >= $month) OR (start_month > end_month AND (start_month <= $month OR end_month >= $month))) 
					ORDER BY `foreup_customer_billing`.`credit_card_id` LIMIT $limit OFFSET $offset");
	}
	function count_all()
	{
		$this->db->from('customer_billing');
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('billing_charge_items');
		$this->db->join('billing_charges', 'billing_charge_items.charge_id = billing_charges.charge_id');
		$this->db->join('billing_credit_cards', 'billing_credit_cards.credit_card_id = billing_charges.credit_card_id');
		$this->db->where('billing_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, billing_charge_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}
	
	/*
	Gets information about a particular item
	*/
	function get_info($billing_id)
	{
		$this->db->from('customer_billing');
		$this->db->where("billing_id", $billing_id);
		
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->result_array();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj = new stdClass();
  
			//Get all the fields from items table
			$fields = $this->db->list_fields('customer_billing');

			foreach ($fields as $field)
			{				
				$item_obj->$field='';
			}
			return (array) $item_obj;
		}
	}

	/*
	Inserts or updates an item
	*/
	function save(&$billing_data,$billing_id=false)
	{
		if (!$billing_id or !$this->exists($billing_id))
		{
			if($this->db->insert('customer_billing',$billing_data))
			{				
				$billing_data['billing_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		$billing_data['billing_id']=$billing_id;		
		$this->db->where('billing_id', $billing_id);
        $val = $this->db->update('customer_billing',$billing_data);
		        
		return $val;
	}
	function save_items($items, $billing_id)
	{
		// echo "<pre>";
		// print_r($items);
		// echo "</pre>";
// 		
		// echo "_BILL ID";
		// echo "$billing_id";
		if (count($items) > 0)
		{
			$this->db->where('billing_id', $billing_id);
			//$this->db->where('course_id', $this->session->userdata('course_id'));
			$this->db->delete('customer_billing_items');
		
			return $this->db->insert_batch('customer_billing_items', $items);	
		} else {
			return true;
		}		
	}
	
	function get_items($billing_id)
	{
		$this->db->from('customer_billing_items');
		$this->db->where('billing_id', $billing_id);
		$this->db->order_by('line_number');
		$result = $this->db->get()->result_array();		
		
		return $result;
	}
	
	/*
	Deletes one item
	*/
	function delete($billing_id)
	{
		$this->db->where('billing_id',$billing_id);
		return $this->db->update('customer_billing', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('billing_id',$item_ids);
		return $this->db->update('billing', array('deleted' => 1));
 	}
	
	function get_todays_generate_invoices($course_id, $days = false, $limit = 10)
	{
		$month = date('n');
		if ($days === false)
		{
			$day = date('j');
			$today = date('Y-m-d');
			
		}
		else 
		{
			$day = date('j', strtotime($days.' day'));
			$today = date('Y-m-d', strtotime($days.' day'));
		}
		//SELECT *, CONCAT(YEAR(NOW()),'-',month,'-',day) AS dayo, CONCAT(YEAR(NOW()),'-', month,'-',day) - INTERVAL generate_days_before DAY AS generate_day, DAY(CONCAT(YEAR(NOW()),'-', month,'-',day) - INTERVAL generate_days_before DAY) AS generate_day_of_month FROM foreup_customer_billing WHERE generate_days_before > 0
		$day_sql = "DAY(CONCAT(YEAR(NOW() + INTERVAL generate_days_before DAY),'-', MONTH(NOW() + INTERVAL generate_days_before DAY),'-',day) - INTERVAL generate_days_before DAY)";
		$month_sql = "MONTH(CONCAT(YEAR(NOW() + INTERVAL generate_days_before DAY),'-', month,'-',day) - INTERVAL generate_days_before DAY)";
		$today_day_sql = "DAY('$today' + INTERVAL generate_days_before DAY)";
		$today_month_sql = "MONTH('$today' + INTERVAL generate_days_before DAY)"; 
		$today_sql = "DATE('$today' + INTERVAL generate_days_before DAY)"; 
		//AND ((frequency = 'monthly' AND $day_sql = $day) OR (frequency = 'yearly' AND $day_sql = $day AND $month_sql = $month))  
					
		return $this->db->query("SELECT
					token,
					cardholder_name,
					title,
					billing_id,
					person_id,
					employee_id,
					pay_account_balance,
					pay_member_balance,
					email_invoice,
					generate_days_before,
					frequency,
					month,
					day,
					foreup_customer_billing.course_id AS course_id,
					foreup_customer_billing.credit_card_id AS credit_card_id
					FROM (`foreup_customer_billing`) 
					LEFT OUTER JOIN `foreup_customer_credit_cards` ON `foreup_customer_billing`.`credit_card_id` = `foreup_customer_credit_cards`.`credit_card_id` 
					WHERE `foreup_customer_billing`.`deleted` != 1 
					AND ((frequency = 'monthly' AND day = $today_day_sql) OR (frequency = 'yearly' AND day = $today_day_sql AND month = $today_month_sql))  
					AND `start_date` <= $today_sql
					AND ((start_month <= end_month AND start_month <= $month_sql AND end_month >= $month_sql) OR (start_month > end_month AND (start_month <= $month_sql OR end_month >= $month_sql))) 
					AND last_invoice_generation_attempt < '$today 00:00:00'
					AND `foreup_customer_billing`.`course_id` = $course_id
					ORDER BY `foreup_customer_billing`.`course_id`
					LIMIT $limit");
		
	}
	function get_todays_generate_invoices_courses($days = false)
	{
		$month = date('n');
		if ($days === false)
		{
			$day = date('j');
			$today = date('Y-m-d');
			
		}
		else 
		{
			$day = date('j', strtotime($days.' day'));
			$today = date('Y-m-d', strtotime($days.' day'));
		}
		//SELECT *, CONCAT(YEAR(NOW()),'-',month,'-',day) AS dayo, CONCAT(YEAR(NOW()),'-', month,'-',day) - INTERVAL generate_days_before DAY AS generate_day, DAY(CONCAT(YEAR(NOW()),'-', month,'-',day) - INTERVAL generate_days_before DAY) AS generate_day_of_month FROM foreup_customer_billing WHERE generate_days_before > 0
		$day_sql = "DAY(CONCAT(YEAR(NOW() + INTERVAL generate_days_before DAY),'-', MONTH(NOW() + INTERVAL generate_days_before DAY),'-',day) - INTERVAL generate_days_before DAY)";
		$month_sql = "MONTH(CONCAT(YEAR(NOW() + INTERVAL generate_days_before DAY),'-', month,'-',day) - INTERVAL generate_days_before DAY)";
		$today_day_sql = "DAY('$today' + INTERVAL generate_days_before DAY)";
		$today_month_sql = "MONTH('$today' + INTERVAL generate_days_before DAY)"; 
		$today_sql = "DATE('$today' + INTERVAL generate_days_before DAY)"; 
		//AND ((frequency = 'monthly' AND $day_sql = DAY($today) OR (frequency = 'yearly' AND $day_sql = $day AND $month_sql = $month))  
					
		return $this->db->query("SELECT
					course_id
					FROM (`foreup_customer_billing`) 
					WHERE `foreup_customer_billing`.`deleted` != 1 
					AND ((frequency = 'monthly' AND day = $today_day_sql) OR (frequency = 'yearly' AND day = $today_day_sql AND month = $today_month_sql))  
					AND `start_date` <= $today_sql
					AND ((start_month <= end_month AND start_month <= $today_month_sql AND end_month >= $today_month_sql) OR (start_month > end_month AND (start_month <= $today_month_sql OR end_month >= $today_month_sql))) 
					AND last_invoice_generation_attempt < '$today 00:00:00'
					GROUP BY course_id
					ORDER BY course_id");
		
	}
	
	function get_todays_billings($days = false, $limit = 10)
	{
		$month = date('n');
		if ($days === false)
		{
			$day = date('j');
			$today = date('Y-m-d');
			
		}
		else 
		{
			$day = date('j', strtotime($days.' day'));
			$today = date('Y-m-d', strtotime($days.' day'));
		}
		
		return $this->db->query("SELECT
					token,
					cardholder_name,
					title,
					billing_id,
					person_id,
					employee_id,
					pay_account_balance,
					pay_member_balance,
					email_invoice,
					foreup_customer_billing.course_id AS course_id,
					foreup_customer_billing.credit_card_id AS credit_card_id
					FROM (`foreup_customer_billing`) 
					LEFT OUTER JOIN `foreup_customer_credit_cards` ON `foreup_customer_billing`.`credit_card_id` = `foreup_customer_credit_cards`.`credit_card_id` 
					WHERE `foreup_customer_billing`.`deleted` != 1 
					AND ((frequency = 'monthly' AND day = $day) OR (frequency = 'yearly' AND day = $day AND month = $month))  
					AND `start_date` <= '$today'
					AND foreup_customer_billing.credit_card_id != ''
					AND ((start_month <= end_month AND start_month <= $month AND end_month >= $month) OR (start_month > end_month AND (start_month <= $month OR end_month >= $month))) 
					AND last_billing_attempt < '$today 00:00:00'
					ORDER BY `foreup_customer_billing`.`course_id`
					LIMIT $limit");
		
	}
	
	function mark_invoice_generation_as_started($billing_id)
	{
		$data = array(
			'last_invoice_generation_attempt'=>date('Y-m-d 00:00:00')
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}
	
	function mark_as_started($billing_id)
	{
		$data = array(
			'last_billing_attempt'=>date('Y-m-d 00:00:00'),
			'started'=>1,
			'charged'=>0,
			'emailed'=>0
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}
	
	function mark_as_charged($billing_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}
	
	function mark_as_emailed($billing_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}
	
	function send_summary($email, $name, $billing_list, $headers)
	{
		$data = array();
		$data['billing_list'] = $billing_list;
		$data['headers'] = $headers;
		$data['name'] = $name;
			
		send_sendgrid(		
			$email,			
			"Customer Billing Summary ".date("Y-m-d"),
			$this->load->view('customers/billing_summary', $data, true),			
			'no-reply@foreup.com',
		 	'ForeUP Auto Billing'		 	
		);		
	}
	
	function get_billing_products($course_id, $credit_card_id)
	{
		$month = date('n');
		$day = date('j');
		$today = date('Y-m-d');
		
		
		$this->db->from('billing');
		//$this->db->join('billing_credit_cards', 'billing.credit_card_id = billing_credit_cards.credit_card_id');
		$this->db->where('billing.deleted !=', 1);
		$this->db->where('start_date <=', $today);
		$this->db->where("(credit_card_id = $credit_card_id AND ((monthly = 1 AND monthly_day = $day) OR (annual = 1 AND annual_day = $day AND annual_month = $month)) OR free = 1)");
		$this->db->order_by('billing.credit_card_id');
		$this->db->where("((period_start <= period_end AND period_start <= $month AND period_end >= $month) OR (period_start > period_end AND (period_start <= $month OR period_end > $month)))");
		$this->db->where('course_id', $course_id);
//		$this->db->where("(free = 1 OR credit_card_id = $credit_card_id)");
		return $this->db->get();
	}
}
?>

<?php
require_once("report.php");
class Summary_tips extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_sale_id'), 'align'=>'left'), array('data'=>lang('reports_date'), 'align'=>'left'), array('data'=>lang('reports_tip_amount'), 'align'=> 'right'), array('data'=>lang('reports_tip_recipient'), 'align'=> 'left'));
	}
	
	public function getData()
	{
		$this->db->select('sales_payments.sale_id AS sale_id, sale_time, sales_payments.payment_amount AS payment_amount, tip_recipient, first_name, last_name');
		$this->db->from('sales_payments');
		//$this->db->where('deleted', 0);
		$this->db->join('sales', 'sales.sale_id = sales_payments.sale_id', 'left');
		$this->db->join('people', 'people.person_id = sales_payments.tip_recipient', 'left');
		$this->db->order_by('sale_time DESC');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where("sale_time >  '{$this->params['start_date']}'");
		$this->db->where("sale_time < '{$this->params['end_date']}'");
		if ($this->params['employee_id'])
			$this->db->where('tip_recipient', $this->params['employee_id']);
		$this->db->like('sales_payments.payment_type', 'Tip', 'before');

		$query = $this->db->get();
		//echo $this->db->last_query();        
		return $query->result_array();		
	}
	
	public function getSummaryData()
	{
		$this->db->select('sum(payment_amount) as total');
		$this->db->from('sales_payments');
		//$this->db->where('deleted', 0);
		$this->db->join('sales', 'sales.sale_id = sales_payments.sale_id', 'left');
		//$this->db->order_by('date_issued DESC');
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->like('sales_payments.payment_type', 'Tip', 'before');
		$this->db->where("sale_time >  '{$this->params['start_date']}'");
		$this->db->where("sale_time < '{$this->params['end_date']}'");
		if ($this->params['employee_id'])
			$this->db->where('tip_recipient', $this->params['employee_id']);
        
		$results = $this->db->get()->result_array();		
		
		return $results[0];
	}
}
?>
<?php
class Customer extends Person
{
	/*
	Determines if a given person_id is a customer
	*/
	function exists($person_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);

		$this->db->select('course_id');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$person_id);
		$this->db->limit(1);
		if ($this->session->userdata('course_id'))
			$this->db->where_in('customers.course_id', array_values($course_ids));
        $query = $this->db->get();
		//echo $this->db->last_query();
		return ($query->num_rows()==1);
	}

	function get_id_from_account_number($account_number)
	{
		$this->db->select('person_id');
		$this->db->from('customers');
		$this->db->where('account_number', $account_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$customer_info = $this->db->get()->result_array();
	//echo $this->db->last_query();
	//print_r($customer_info);
		return $customer_info[0]['person_id'];
	}
	function get_by_username($username)
	{
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where("(username = '$username')");
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1)
		{
			return $query->row();
		}

		return false;
	}

	function save_image($person_id, $image_id){
		return $this->db->update('customers',
			array('image_id' => (int) $image_id),
			"person_id = ".(int) $person_id."
				AND course_id = ".(int) $this->session->userdata('course_id')
			);
	}

	function update_password($customer_id, $password)
	{
		$customer_data = array('password' => $password);
		$this->db->where('person_id', $customer_id);
		$success = $this->db->update('customers',$customer_data);

		return $success;
	}

	/*
	Determines if a customer username already exists
	*/
	function username_exists($username)
	{
		$this->load->model('user');
		return $this->user->username_exists($username);
	}

	/*
	Returns all the customers
	*/
	function get_all($limit=10000, $offset=0, $group_id = false)
	{
		$course_id = '';
	    //if (!$this->permissions->is_super_admin())
		{
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	        $this->db->where_in('customers.course_id', array_values($course_ids));
        }
	    $this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		if ($group_id && $group_id != 'all')
		{
			$this->db->join('customer_group_members', 'people.person_id = customer_group_members.person_id');
			$this->db->where('customer_group_members.group_id',$group_id);
		}
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("last_name asc, first_name asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$customers = $this->db->get();
		//echo $this->db->last_query();
        return $customers;
	}
	function get_negative_balances($balance_type = false)
	{
		if ($balance_type == 'member')
			$this->db->where('member_account_balance <', 0);
		else if ($balance_type == 'customer')
			$this->db->where('account_balance <', 0);
		else if ($balance_type == 'both')
			$this->db->where('(member_account_balance < 0 OR account_balance < 0)');
		else
			return;
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		$this->db->where('deleted', 0);
		//$this->db->where('credit_card_id !=', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->order_by('last_name asc, first_name asc');
		$customers = $this->db->get();

		return $customers->result_object();
	}
	function count_all($filter = '', $person_ids = false, $group_ids = false)
	{
		$course_id = '';
     	//if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	        $this->db->where_in('customers.course_id', array_values($course_ids));
        }
        $this->db->select('course_id');
	    $this->db->from('customers');
		if ($filter == 'email')
		{
			$this->db->join('people','people.person_id = customers.person_id');
			$this->db->where('email !=', '');
			$this->db->group_by('email');
		}
		else if ($filter == 'phone')
		{
			$this->db->join('people','people.person_id = customers.person_id');
			$this->db->where('phone_number !=', '');
			$this->db->group_by('phone_number');
		}
		if ($person_ids !== false)
		{
			if (count($person_ids) == 0)
				$this->db->where('customers.person_id', 0);
			else
				$this->db->where_in('customers.person_id',$person_ids);
		}
		if ($group_ids !== false && !in_array(0, $group_ids))
		{
			if (count($group_ids) == 0)
				$this->db->where('customers.person_id', 0);
			else {
				$this->db->join('customer_group_members', 'people.person_id = customer_group_members.person_id');
				$this->db->where_in('customer_group_members.group_id',$group_ids);
			}
		}
		$this->db->where("deleted = 0 $course_id");
		//echo $query->
		$start = time();
		$this->benchmark->mark('query_start');
		$query = $this->db->get();
		//$result = $query->num_rows();
		//$result = $this->db->count_all_results();
		//print_r($result);
		$end = time();
		$this->benchmark->mark('query_end');
		//echo "time ".($end - $start).'<br/>';
		//echo $this->db->last_query();
		//echo '<br/>Benchmark '.$this->benchmark->elapsed_time('query_start','query_end');
		return $query->num_rows();//$results;
	}
	/*
	Determins if a employee is logged in
	*/
	function is_logged_in()
	{
		return ($this->session->userdata('customer_id')!=false);// && $this->session->userdata('type') == 'customer');
	}

	/*
	Gets information about the currently logged in employee.
	*/
	function get_logged_in_customer_info()
	{
		if($this->is_logged_in())
		{
			return $this->get_info($this->session->userdata('customer_id'));
		}

		return false;
	}


	/*
	Gets information about a particular customer
	*/
	function get_info($customer_id, $course_id = false)
	{
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		//$this->db->join('users', 'users.person_id = customers.person_id', 'left');
		$this->db->where('customers.person_id',$customer_id);
		$this->db->limit(1);
		if ($course_id && $course_id != '') {
			$this->db->where('customers.course_id',$course_id);
		}
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);

			//Get all the fields from customer table
			$fields = $this->db->list_fields('customers');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}

			return $person_obj;
		}
	}

	/*
	Gets information about a particular customer by email
	*/
	function get_info_by_email($email, $course_id)
	{
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('people.email', $email);
		$this->db->where('deleted',0);
		$this->db->where('customers.course_id', $course_id);
		$result = $this->db->get()->result_array();
		// echo $this->db->last_query();
		return $result;
	}
	/*
	Gets information about multiple customers
	*/
	function get_multiple_info($customer_ids, $type = 'email')
	{
		//if ($type == 'email')
		//	$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number');
		//else
		//	$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number, sendhub_accounts.phone_number AS sendhub_phone_number, sendhub_accounts.sendhub_id, sendhub_accounts.text_reminder_unsubscribe ');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		//if ($type != 'email')
		//	$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
		$this->db->where_in('customers.person_id',$customer_ids);
		$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}
	/*
	Gets information about multiple customers
	*/
	function get_multiple_info_for_email($customer_ids)
	{
		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where_in('customers.person_id',$customer_ids);
		$this->db->where('opt_out_email', 0);
		$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
	}
	/*
	Gets information about multiple customers
	*/
	function get_multiple_info_for_text($customer_ids)
	{
		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.first_name,people.email, people.last_name, people.phone_number, sendhub_accounts.phone_number AS sendhub_phone_number, sendhub_accounts.sendhub_id, sendhub_accounts.text_reminder_unsubscribe ');
		$this->db->from('customers');
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
		$this->db->where_in('customers.person_id',$customer_ids);
		$this->db->where('opt_out_text', 0);
		$this->db->order_by("last_name asc, first_name asc");
		$result = $this->db->get();
		//echo '<br/>Last Query<br/>';
		//echo $this->db->last_query();
		return $result;
	}
	function get_price_classes()
	{
		$price_class_array = array('0'=>'None');
		for ($i = 4; $i <=30; $i++)
			if ($this->config->item('price_category_'.$i) != '')
				$price_class_array[$i] = $this->config->item('price_category_'.$i);

		return $price_class_array;
	}
	function get_price_class_id($label)
	{
		$label = trim($label);
		$this->db->from('green_fee_types');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('price_category_1', $label);
		$this->db->or_where('price_category_2', $label); $this->db->or_where('price_category_3', $label); $this->db->or_where('price_category_4', $label);
		$this->db->or_where('price_category_5', $label); $this->db->or_where('price_category_6', $label); $this->db->or_where('price_category_7', $label);
		$this->db->or_where('price_category_8', $label); $this->db->or_where('price_category_9', $label); $this->db->or_where('price_category_10', $label);
		$this->db->or_where('price_category_11', $label); $this->db->or_where('price_category_12', $label);	$this->db->or_where('price_category_13', $label);
		$this->db->or_where('price_category_14', $label); $this->db->or_where('price_category_15', $label);	$this->db->or_where('price_category_16', $label);
		$this->db->or_where('price_category_17', $label); $this->db->or_where('price_category_18', $label);	$this->db->or_where('price_category_19', $label);
		$this->db->or_where('price_category_20', $label); $this->db->or_where('price_category_21', $label);	$this->db->or_where('price_category_22', $label);
		$this->db->or_where('price_category_23', $label); $this->db->or_where('price_category_24', $label);	$this->db->or_where('price_category_25', $label);
		$this->db->or_where('price_category_26', $label); $this->db->or_where('price_category_27', $label);	$this->db->or_where('price_category_28', $label);
		$this->db->or_where('price_category_29', $label); $this->db->or_where('price_category_30', $label);	$this->db->or_where('price_category_31', $label);
		$this->db->or_where('price_category_32', $label); $this->db->or_where('price_category_33', $label);	$this->db->or_where('price_category_34', $label);
		$this->db->or_where('price_category_35', $label); $this->db->or_where('price_category_36', $label);	$this->db->or_where('price_category_37', $label);
		$this->db->or_where('price_category_38', $label); $this->db->or_where('price_category_39', $label);	$this->db->or_where('price_category_40', $label);
		$this->db->or_where('price_category_41', $label); $this->db->or_where('price_category_42', $label);	$this->db->or_where('price_category_43', $label);
		$this->db->or_where('price_category_44', $label); $this->db->or_where('price_category_45', $label);	$this->db->or_where('price_category_46', $label);
		$this->db->or_where('price_category_47', $label); $this->db->or_where('price_category_48', $label);	$this->db->or_where('price_category_49', $label);
		$this->db->or_where('price_category_50', $label);

		$this->db->limit(1);

		$price_types = $this->db->get()->result_array();
		//echo json_encode($price_types[0]);
		//return;
		foreach ($price_types[0] as $index => $value)
		{
			if ($label == $value)
				return $index;
		}
		return '';
	}
	/*
	Updates multiple items at once
	*/
	function update_multiple($customer_data,$customer_ids,$groups_data, $remove_groups_data)
	{
		foreach($customer_ids as $customer_id)
		{
			$this->save_group_memberships($groups_data, $customer_id, true, $remove_groups_data);
		}
		$this->db->where_in('person_id',$customer_ids);
		return $this->db->update('customers',$customer_data);
	}


	// GROUP Functionality
	function get_customer_groups($customer_id)
	{
		$this->db->from('customer_groups');
		$this->db->join('customer_group_members', "customer_groups.group_id = customer_group_members.group_id", 'LEFT');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('person_id', $customer_id);
		return $this->db->get()->result_array();
	}
    // PASS Functionality
	function get_customer_passes($customer_id)
	{
		$this->db->from('customer_passes');
		$this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id", 'LEFT');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('person_id', $customer_id);
		return $this->db->get()->result_array();
	}
    // GROUP Functionality
	function get_group_info($customer_id = '', $search = false)
	{
		$this->db->select('foreup_customer_groups.group_id AS group_id, label, person_id, count(person_id) AS member_count');
		$this->db->from('customer_groups');
		if ($customer_id == '')
			$this->db->join('customer_group_members', "customer_groups.group_id = customer_group_members.group_id", 'LEFT');
		else
			$this->db->join('customer_group_members', "customer_groups.group_id = customer_group_members.group_id AND foreup_customer_group_members.person_id = '$customer_id'", 'LEFT');
		//$this->db->join('customers', "customers.person_id = customer_gorup_members.person_id");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if ($search)
			$this->db->where("label LIKE '%".$this->db->escape_like_str($search)."%'");
		//$this->db->where('deleted', 0);
		$this->db->group_by('group_id');

		return $this->db->get()->result_array();
	}
    function get_single_group_info($group_id)
    {
        $this->db->from('customer_groups');
        $this->db->where('group_id', $group_id);
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->limit(1);

        $group_id = $this->db->get()->result_array();
    }

  	function get_multiple_group_info(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
  	{
  		if ($filter == 'email')
			$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.person_id, people.first_name, people.phone_number, people.email');
		else if ($is_group_message)
			$this->db->select('customers.person_id, customers.course_id, customers.username, customers.opt_out_email, customers.opt_out_text, people.person_id, sendhub_accounts.sendhub_id, people.first_name, people.phone_number, people.email, sendhub_accounts.phone_number AS sendhub_phone_number');
		if (!in_array(0, $group_ids))
		{
	    	$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			if (($filter == 'phone' || $filter == 'phone_number') && $is_group_message) {
				$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');
			}
			$this->db->where_in('customer_group_members.group_id',$group_ids);
		}
		else
		{
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}

		$this->db->where('course_id', ($course_id ? $course_id : $this->session->userdata('course_id')));
		$this->db->where('deleted', 0);

		if ($filter == 'email')
		{
			$this->db->where('email !=', '');
			$this->db->group_by('email');
		}
		else if ($filter == 'phone' || $filter == 'phone_number')
		{
			$this->db->where('phone_number !=', '');
			$this->db->group_by('phone_number');
		}
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
  	}
	function get_multiple_group_info_for_email(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
  	{
  		$this->db->select('customers.person_id AS person_id, email, first_name, opt_out_email');
  		if (!in_array(0, $group_ids))
		{
	    	$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			$this->db->where_in('customer_group_members.group_id',$group_ids);
		}
		else
		{
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}

		$this->db->where('course_id', $course_id ? $course_id : $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->where('email !=', '');
		$this->db->group_by('email');
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result;
  	}
  	function get_multiple_group_info_for_text(array $group_ids, $filter = 'none', $is_group_message = false, $course_id = false)
  	{
  		$this->db->select('customers.person_id, customers.course_id, customers.opt_out_email, customers.opt_out_text, people.person_id, sendhub_accounts.sendhub_id, people.first_name, people.phone_number, people.email, sendhub_accounts.phone_number AS sendhub_phone_number');
		if (!in_array(0, $group_ids))
		{
	    	$this->db->from('customer_group_members');
			$this->db->join('people', 'people.person_id = customer_group_members.person_id');
			$this->db->join('customers', 'customers.person_id = people.person_id');
			$this->db->where_in('customer_group_members.group_id',$group_ids);
		}
		else
		{
			$this->db->from('customers');
			$this->db->join('people', 'people.person_id = customers.person_id');
		}
		$this->db->join('sendhub_accounts', 'sendhub_accounts.phone_number = people.phone_number', 'left');

		$this->db->where('course_id', $course_id ? $course_id : $this->session->userdata('course_id'));
		$this->db->where('deleted', 0);
		$this->db->where('people.phone_number !=', '');
		$this->db->group_by('people.phone_number');
		$result = $this->db->get();
		//echo '<br/>Last Query<br/>';
		//echo $this->db->last_query();
		return $result;
  	}

	function save_group_memberships($groups_data, $person_id, $positives_only = false, $remove_groups_data = false)
	{
		if(!$positives_only)
			$this->db->delete('customer_group_members', array('person_id'=>$person_id));
		foreach($groups_data as $group_id)
        	$this->db->insert('customer_group_members', array('person_id'=>$person_id, 'group_id'=>$group_id));

		//Remove each customer from all the groups
		if ($remove_groups_data) {
			foreach ($remove_groups_data as $group_id) {
				$this->db->delete('customer_group_members', array('person_id'=>$person_id, 'group_id'=>$group_id));
			}
		}
	}
	function get_group_id($group_label)
	{
		$this->db->from('customer_groups');
		$this->db->where('label', trim($group_label));
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);

		$group_id = $this->db->get()->result_array();
		//$group_id[] = $group_label;
		//$group_id[] = $this->db->last_query();

		if (isset($group_id[0]) && isset($group_id[0]['group_id']) && $group_id[0]['group_id'] != '')
			return $group_id[0]['group_id'];
		else if ($group_label != '')
			return $this->add_group($group_label);
		else return '';
	}
	function add_group($group_label)
	{
		$this->db->insert('customer_groups', array('label'=>trim($group_label), 'course_id'=>$this->session->userdata('course_id')));
		return $this->db->insert_id();
	}
	function save_group_name($group_id, $group_label)
	{
		$this->db->where('group_id', $group_id);
		return $this->db->update('customer_groups', array('label'=>$group_label));
	}
	function delete_group($group_id)
	{
		$this->db->delete('customer_groups', array('group_id'=>$group_id));
		$this->db->delete('customer_group_members', array('group_id'=>$group_id));
	}
	// PASS Functionality
	function get_pass_info($customer_id = '')
	{
		$this->db->select('foreup_customer_passes.pass_id AS pass_id, start_date, expiration, label, person_id, count(person_id) AS member_count');
		$this->db->from('customer_passes');
		if ($customer_id == '')
			$this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id", 'LEFT');
		else
			$this->db->join('customer_pass_members', "customer_passes.pass_id = customer_pass_members.pass_id AND foreup_customer_pass_members.person_id = '$customer_id'", 'LEFT');
		//$this->db->join('customers', "customers.person_id = customer_gorup_members.person_id");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->where('deleted', 0);
		$this->db->group_by('pass_id');

		return $this->db->get()->result_array();
	}

  function get_multiple_pass_info(array $pass_ids, $filter = 'none')
  {
    $this->db->from('customer_pass_members');
		$this->db->join('people', 'people.person_id = customer_pass_members.person_id');
		$this->db->where_in('customer_pass_members.pass_id',$pass_ids);
		if ($filter == 'email')
		{
			$this->db->where('email !=', '');
			$this->db->group_by('email');
		}
		else if ($filter == 'phone')
		{
			$this->db->where('phone_number !=', '');
			$this->db->group_by('phone_number');
		}
		return $this->db->get();
  }

	function save_pass_memberships($passes_data, $person_id, $positives_only = false)
	{
		if(!$positives_only)
			$this->db->delete('customer_pass_members', array('person_id'=>$person_id));
		foreach($passes_data as $pass_id)
        {
        	$this->db->insert('customer_pass_members', array('person_id'=>$person_id, 'pass_id'=>$pass_id, 'start_date'=>$this->input->post('start_date_'.$pass_id), 'expiration'=>$this->input->post('expiration_'.$pass_id)));
        }
	}
	function get_pass_id($pass_label)
	{
		$this->db->from('customer_passes');
		$this->db->where('label', trim($pass_label));
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);

		$pass_id = $this->db->get()->result_array();
		//$group_id[] = $group_label;
		//$group_id[] = $this->db->last_query();

		if (isset($pass_id[0]) && isset($pass_id[0]['pass_id']) && $pass_id[0]['pass_id'] != '')
			return $pass_id[0]['pass_id'];
		else if ($pass_label != '')
			return $this->add_pass($pass_label);
		else return '';
	}
	function add_pass($pass_label)
	{
		$this->db->insert('customer_passes', array('label'=>trim($pass_label), 'course_id'=>$this->session->userdata('course_id')));
		return $this->db->insert_id();
	}
	function save_pass_name($pass_id, $pass_label)
	{
		$this->db->where('pass_id', $pass_id);
		return $this->db->update('customer_passes', array('label'=>$pass_label));
	}
	function delete_pass($pass_id)
	{
		$this->db->delete('customer_passes', array('pass_id'=>$pass_id));
		$this->db->delete('customer_pass_members', array('pass_id'=>$pass_id));
	}
	/*
	Checks to see if Username is already in use
	*/
	function username_is_available($customer_id, $username)
	{
		$this->db->from('customers');
		$this->db->where('username', $username);
		$this->db->where("person_id != {$customer_id}");
		$this->db->limit(1);
		$results = $this->db->get();

		return ($results->num_rows == 0);
	}

	/*
	Send customer updated username and password
	*/
	function send_username_password_email($person_data, $customer_data, $password)
	{
		$data['person_data'] = $person_data;
		$data['customer_data'] = $customer_data;
		$data['password'] = $password;
		$course_info = $this->Course->get_info($this->session->userdata('course_id'));
		$data['course_info'] = $course_info;
		$view = $this->load->view('customers/username_password_updated', $data, true);


		send_sendgrid(
			$person_data['email'],
			$course_info->name,
			$this->load->view('customers/username_password_updated', $data, true),
			$course_info->email,
		 	$course_info->name
		);
	}
	/*
	Inserts or updates a customer
	*/
	function save(&$person_data, &$customer_data, $customer_id=false, &$giftcard_data=array(), &$groups_data=array(), &$passes_data=array(), $import=false)
	{
		$success = false;
		$this->load->model('user');

		$user_data = array();
		if(!empty($customer_data['username'])){
			$user_data['email'] = $customer_data['username'];
			unset($customer_data['username']);
		}
		if(!empty($customer_data['password'])){
			$user_data['password'] = $customer_data['password'];
			unset($customer_data['password']);
		}

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		if(parent::save($person_data, $customer_id))
		{
			if (isset($customer_data['price_class']))
			{
				$customer_data['price_class'] = ($customer_data['price_class'] === 0 || $customer_data['price_class'] === null) ? '' :$customer_data['price_class'];
 			}

			if (isset($customer_data['username']) && $this->username_exists($customer_data['username']) && !isset($customer_data['member']))
			{
                $success = false;
			}
            else if (!$customer_id or !$this->exists($customer_id))
			{
				$customer_id = $giftcard_data['customer_id'] = $customer_data['person_id'] = $person_data['person_id'];

				$success = $this->db->insert('customers',$customer_data);
				$this->user->save($customer_id, $user_data);

				if (isset($giftcard_data['giftcard_number']) && isset($giftcard_data['value']))
					$this->Giftcard->save($giftcard_data);
			}
			else
			{
				$this->db->where('person_id', $customer_id);
				$success = $this->db->update('customers',$customer_data);
				$this->user->save($customer_id, $user_data);
			}

			if($import)
			{
				$group_ids = array();
				foreach($groups_data as $label)
				{
					$gid = $this->get_group_id($label);
					if ($gid != '')
						$group_ids[] = $gid;
				}
				$groups_data = $group_ids;
			}

			if (count($groups_data) != 0) {
				// BECAUSE THE CUSTOMER SAVE HAPPENS IN SO MANY PLACES WE WANT TO SPECIFICALLY TELL THE MODEL TO CLEAR OUT USER GROUPS BEFORE WE DO
				$groups_data = ($groups_data == 'delete') ? array() : $groups_data;
				$this->save_group_memberships($groups_data, $customer_id);
			}

			if (count($passes_data) != 0) {
				// BECAUSE THE CUSTOMER SAVE HAPPENS IN SO MANY PLACES WE WANT TO SPECIFICALLY TELL THE MODEL TO CLEAR OUT USER PASSES BEFORE WE DO
				$passes_data = ($passes_data == 'delete') ? array() : $passes_data;
				$this->save_pass_memberships($passes_data, $customer_id);
			}

		}
		$this->db->trans_complete();

		return $this->db->trans_status();
	}
	function get_linked_course_ids(&$course_ids)
	{
		$this->load->model('course');
		return $this->course->get_linked_course_ids($course_ids);
	}
	/*
	Deletes one customer
	*/
	function delete($customer_id)
	{
		$this->db->delete('customer_group_members', array('person_id'=>$customer_id));
		//$course_id = '';
       // if (!$this->permissions->is_super_admin())
        $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("person_id = '$customer_id' $course_id");
		return $this->db->update('customers', array('deleted' => 1,'account_number'=>null));//,'username'=>null));
	}

	/*
	Deletes a list of customers
	*/
	function delete_list($customer_ids)
	{
		$this->db->where_in('person_id', $customer_ids);
		$this->db->delete('customer_group_members');

		$this->db->where_in('person_id', $customer_ids);
        $this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->update('customer_billing', array('deleted' => 1));
		//if (!$this->permissions->is_super_admin())
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('person_id',$customer_ids);
		return $this->db->update('customers', array('deleted' => 1,'account_number'=>null));//,'username'=>null));
 	}

 	/*
	Get search suggestions to find customers
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	    	$course_ids = join(',',$course_ids);
			$course_id = "AND foreup_customers.course_id IN ({$course_ids})";
		}
        $suggestions = array();

		$this->db->select('first_name, last_name');
		$this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or
		last_name LIKE '".$this->db->escape_like_str($search)."%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name asc, first_name asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name);
		}

		if (count($suggestions) < $limit)
		{
			$this->db->select('email');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("email",$search, 'after');
			$this->db->order_by("email", "asc");
			$by_email = $this->db->get();
			foreach($by_email->result() as $row)
			{
				$suggestions[]=array('label'=> $row->email);
			}
		}
		if (count($suggestions) < $limit)
		{
			$this->db->select('phone_number');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("phone_number",$search);
			$this->db->order_by("phone_number", "asc");
			$by_phone = $this->db->get();
			foreach($by_phone->result() as $row)
			{
				$suggestions[]=array('label'=> $row->phone_number);
			}
		}
		if (count($suggestions) < $limit)
		{
			$this->db->select('account_number');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("account_number",$search, 'after');
			$this->db->order_by("account_number", "asc");
			$by_account_number = $this->db->get();
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('label'=> $row->account_number);
			}
		}

		// $this->db->from('customers');
		// $this->db->join('people','customers.person_id=people.person_id');
		// $this->db->where("deleted = 0 $course_id");
		// $this->db->like("company_name",$search);
		// $this->db->order_by("company_name", "asc");
		// $by_company_name = $this->db->get();
		// foreach($by_company_name->result() as $row)
		// {
			// $suggestions[]=array('label'=> $row->company_name);
		// }

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Get search suggestions to find customers
	*/
	function get_customer_search_suggestions($search,$limit=25,$type='')
	{
		$split_name = explode(',', $search);
		$added_search_sql = '';
		if (count($split_name) > 1)
		{
			$first_name = trim($split_name[1]);
			$last_name = trim($split_name[0]);
			$added_search_sql = " or (first_name LIKE '$first_name%' AND last_name LIKE '$last_name%')";
		}
		else {
			$split_name = explode(' ', $search);
			if (count($split_name) > 1)
			{
				$first_name = trim($split_name[0]);
				$last_name = trim($split_name[1]);
				$added_search_sql = " or (first_name LIKE '$first_name%' AND last_name LIKE '$last_name%')";
			}
		}
		//Clean search term
		$search = str_replace(array('(',')','-','_',' '), '', $search);
		$course_id = '';
        //if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	    	$course_ids = join(',',$course_ids);
			$course_id = "AND foreup_customers.course_id IN ({$course_ids})";
		}
        $suggestions = array();

		if ($type == 'last_name' || $type == '' || $type == 'ln_and_pn') {
			$this->db->select('foreup_people.person_id as person_id, last_name, first_name, phone_number, email, zip');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or
			last_name LIKE '".$this->db->escape_like_str($search)."%' or
			CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' $added_search_sql) and deleted=0 $course_id");
			$this->db->order_by("last_name asc, first_name asc");
			$this->db->limit($limit);
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->last_name.', '.$row->first_name, 'phone_number'=>"(".substr($row->phone_number, 0, 3).") ".substr($row->phone_number, 3, 3)."-".substr($row->phone_number,6), 'email'=>$row->email, 'zip'=>$row->zip);
			}
		}

		if (($type == 'account_number' || $type == '' || $type = 'ln_and_pn') && count($suggestions) < $limit) {
			$this->db->select('foreup_people.person_id as person_id, account_number');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->like("account_number",$search, 'after');
			$this->db->order_by("account_number", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->account_number);
			}
		}


		if (($type == 'phone_number' || $type == 'ln_and_pn') && count($suggestions) < $limit) {
			$this->db->select('foreup_people.person_id as person_id, last_name, first_name, phone_number, email');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('phone_number != ""');
			$this->db->like("phone_number",$search);
			$this->db->order_by("phone_number", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => "(".substr($row->phone_number, 0, 3).") ".substr($row->phone_number, 3, 3)."-".substr($row->phone_number,6), 'name'=>$row->last_name.', '.$row->first_name, 'email'=>$row->email, 'phone_number' => "(".substr($row->phone_number, 0, 3).") ".substr($row->phone_number, 3, 3)."-".substr($row->phone_number,6));
			}
		}

		if ($type == 'email' && count($suggestions) <  $limit) {
			$this->db->select('foreup_people.person_id as person_id, email, last_name, first_name, phone_number');
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('email != ""');
			$this->db->like("email",$search, 'after');
			$this->db->order_by("email", "asc");
			$this->db->limit($limit);
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('value'=> $row->person_id, 'label' => $row->email, 'name'=>$row->last_name.', '.$row->first_name, 'phone_number'=>"(".substr($row->phone_number, 0, 3).") ".substr($row->phone_number, 3, 3)."-".substr($row->phone_number,6));
			}
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	/*
	Preform a search on customers
	*/
	function search($search, $limit=20, $group_id = 'all', $pass_id = 'all', $offset = 0)
	{
		$course_id = '';
        //if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	    	$course_ids = join(',',$course_ids);
			$course_id = "AND foreup_customers.course_id IN ({$course_ids})";
        }
		$this->db->select("customers.person_id AS person_id, course_id, member, price_class, account_number, account_balance, member_account_balance, loyalty_points, first_name, last_name, email, phone_number");
        $this->db->from('customers');
		$this->db->join('people','customers.person_id=people.person_id');
		if ($group_id == 'no_group')
		{
			$this->db->join('customer_group_members', 'customers.person_id=customer_group_members.person_id', 'left');
			$this->db->where('group_id', null);
		}
		else if ($group_id != 'all')
		{
			$this->db->join('customer_group_members', 'customers.person_id=customer_group_members.person_id');
			$this->db->where('group_id', $group_id);
		}
		if ($pass_id == 'no_pass')
		{
			$this->db->join('customer_pass_members', 'customers.person_id=customer_pass_members.person_id', 'left');
			$this->db->where('pass_id', null);
		}
		else if ($pass_id != 'all')
		{
			$this->db->join('customer_pass_members', 'customers.person_id=customer_pass_members.person_id');
			$this->db->where('pass_id', $pass_id);
		}

		$this->db->where("(first_name LIKE '".$this->db->escape_like_str($search)."%' or
		last_name LIKE '".$this->db->escape_like_str($search)."%' or
		email LIKE '".$this->db->escape_like_str($search)."%' or
		phone_number LIKE '%".$this->db->escape_like_str($search)."%' or
		account_number LIKE '".$this->db->escape_like_str($search)."%' or
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
		$this->db->order_by("last_name asc, first_name asc");
		// Just return a count of all search results
		if ($limit == 0)
			return $this->db->get()->num_rows();
		// Return results
		$this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();
	}
    function cleanup()
	{
		$customer_data = array('account_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('customers',$customer_data);
	}
}
?>

<?php
class Table extends CI_Model
{
	function get_all($order_by_table_id = false)
	{
		$this->db->from('tables');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if ($order_by_table_id)
			$this->db->order_by('table_id');
		else
			$this->db->order_by('sale_id');
		return $this->db->get();
	}

	public function get_info($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get();
	}

	function exists($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows() == 1);
	}

	function get_id_by_table_number($table_number)
	{
		$this->db->from('tables');
		$this->db->where('table_id',$table_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->row_array();

		if ($query->num_rows()==1)
			return $result['sale_id'];
		else
			return false;
	}

	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$success = $this->db->update('tables',$sale_data);

		return $success;
	}

	function save($items, $customer_id, $employee_id, $comment, $payments, $sale_id = false, $table_id = false)
	{
		if (!$sale_id || $sale_id == -1)
		{
			$sale_id = $this->get_id_by_table_number($table_id);
		}

		$payment_types='';
		foreach($payments as $payment_id=>$payment)
		{
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			'sale_time' => date('Y-m-d H:i:s'),
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'employee_id'=>$employee_id,
			'payment_type'=>$payment_types,
			'comment'=>$comment,
			'deleted' => 0,
			'course_id'=>$this->session->userdata('course_id'),
			'table_id'=>$table_id
		);

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
		if ($sale_id)
		{
			$this->db->delete('table_payments', array('sale_id' => $sale_id));
			$this->db->delete('table_items_taxes', array('sale_id' => $sale_id));
			$this->db->delete('table_items', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
			$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
			$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));

			$this->db->where('sale_id', $sale_id);
			$this->db->update('tables', $sales_data);
		}
		else
		{
			$this->db->insert('tables', $sales_data);
			$sale_id = $this->db->insert_id();
		}
		//echo $this->db->last_query();
		foreach($payments as $payment_id=>$payment)
		{
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount']
			);
			$this->db->insert('table_payments',$sales_payments_data);
		}

		foreach($items as $line=>$item)
		{
			if (isset($item['item_id']))
			{
				$cur_item_info = $this->Item->get_info($item['item_id']);

				$sales_items_data = array
				(
					'sale_id'=>$sale_id,
					'item_id'=>$item['item_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'serialnumber'=>$item['serialnumber'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_cost_price' => $cur_item_info->cost_price,
					'item_unit_price'=>$item['price'] + $item['modifier_total']
					);

				$this->db->insert('table_items',$sales_items_data);

				// Insert any modifiers
				if(!empty($item['modifiers'])){
					foreach($item['modifiers'] as $key => $modifier){
						$modifierData = array();
						$modifierData['sale_id'] = $sale_id;
						$modifierData['item_id'] = $item['item_id'];
						$modifierData['line'] = $item['line'];
						$modifierData['modifier_id'] = $modifier['modifier_id'];
						$modifierData['selected_option'] = $modifier['selected_option'];

						if(empty($modifier['selected_price'])){
							$modifier['selected_price'] = '0.00';
						}

						$modifierData['selected_price'] = $modifier['selected_price'];

						$this->db->insert('table_items_modifiers', $modifierData);
					}
				}
			}
			else
			{
				$cur_item_kit_info = $this->Item_kit->get_info($item['item_kit_id']);

				$sales_item_kits_data = array
				(
					'sale_id'=>$sale_id,
					'item_kit_id'=>$item['item_kit_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'quantity_purchased'=>$item['quantity'],
					'discount_percent'=>$item['discount'],
					'item_kit_cost_price' => $cur_item_kit_info->cost_price,
					'item_kit_unit_price'=>$item['price']
				);

				$this->db->insert('table_item_kits',$sales_item_kits_data);
			}

			$customer = $this->Customer->get_info($customer_id);
 			if ($customer_id == -1 or $customer->taxable)
 			{
				if (isset($item['item_id']))
				{
					foreach($this->Item_taxes->get_info($item['item_id']) as $row)
					{
						$this->db->insert('table_items_taxes', array(
							'sale_id' 	=>$sale_id,
							'item_id' 	=>$item['item_id'],
							'line'      =>$item['line'],
							'name'		=>$row['name'],
							'percent' 	=>$row['percent'],
							'cumulative'=>$row['cumulative']
						));
					}
				}
				else
				{
					foreach($this->Item_kit_taxes->get_info($item['item_kit_id']) as $row)
					{
						$this->db->insert('table_item_kits_taxes', array(
							'sale_id' 		=>$sale_id,
							'item_kit_id'	=>$item['item_kit_id'],
							'line'      	=>$item['line'],
							'name'			=>$row['name'],
							'percent' 		=>$row['percent'],
							'cumulative'	=>$row['cumulative']
						));
					}
				}
			}
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}

		return $sale_id;
	}

	function delete($sale_id)
	{
		// Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->delete('table_receipts', array('sale_id' => $sale_id));
		$this->db->delete('table_receipt_items', array('sale_id' => $sale_id));
		$this->db->delete('table_payments', array('sale_id' => $sale_id));
		$this->db->delete('table_items_modifiers', array('sale_id' => $sale_id));
		$this->db->delete('table_items', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits_taxes', array('sale_id' => $sale_id));
		$this->db->delete('table_item_kits', array('sale_id' => $sale_id));
		$this->db->delete('tables', array('sale_id' => $sale_id));

		$this->db->trans_complete();
		return $this->db->trans_status();
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('table_items');
		$this->db->where('sale_id', $sale_id);
		return $this->db->get();
	}

	function get_sale_item_modifiers($sale_id){

		if(empty($sale_id)){
			return false;
		}

		$this->db->select('m.modifier_id, m.name, s.selected_price, m.default_price AS price,
			m.options, s.line, s.item_id, s.selected_option AS selected_option', false);
		$this->db->from('modifiers AS m');
		$this->db->join('table_items_modifiers AS s', 's.modifier_id = m.modifier_id', 'inner');
		$this->db->where(array('s.sale_id' => $sale_id));
		$this->db->order_by('s.line');

		$query = $this->db->get();
		$modifiers = $query->result_array();
		$rows = array();

		// Loop through rows and decode JSON options into array
		foreach($modifiers as $key => $row){

			if(!empty($row['options'])){
				$row['options'] = json_decode($row['options'], true);
				$row['options'][] = 'no';
			}
			// If no options set, just default to 'yes' or 'no'
			if(empty($row['options']) || empty($row['options'][0])){
				$row['options'] = array('yes','no');
			}

			$rows[$row['line']][$row['modifier_id']] = $row;
		}

		return $rows;
	}

	function get_sale_total($sale_id)
	{
		$items = $this->get_sale_items($sale_id)->result_array();
		$item_kits = $this->get_sale_item_kits($sale_id)->result_array();

		$total = 0;
		foreach ($items as $item)
			$total += $item['quantity_purchased'] * $item['item_unit_price'] * (100 - $item['discount_percent']) / 100;
		foreach ($item_kits as $item_kit)
			$total += $item_kit['quantity_purchased'] * $item_kit['item_kit_unit_price'] * (100 - $item_kit['discount_percent']) / 100;
		return $total;
	}

	function get_sale_item_kits($sale_id)
	{
		$this->db->from('table_item_kits');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('table_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}

	function get_customer($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}

	function get_comment($sale_id)
	{
		$this->db->from('tables');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get()->row()->comment;
	}

	function apply_side_type(&$sides, $type){
		if(!isset($sides) || empty($sides)){
			return array();
		}

		$sidesWithType = array();
		foreach($sides as $key => $side){
			$side['type'] = $type;
			$sidesWithType[] = $side;
		}

		return $sidesWithType;
	}

	// Insert an item part of table sale
	function save_item($sale_id, $item){

		$this->delete_item($sale_id, $item['line']);
		$cur_item_info = $this->Item->get_info($item['item_id']);

		// Gather up all side types and place them into a single array
		// with type of side defined as a column
		$sides = array_merge(
			$this->apply_side_type($item['soups'], 'soups'),
			$this->apply_side_type($item['salads'], 'salads'),
			$this->apply_side_type($item['sides'], 'sides')
		);

		$sales_items_data = array(
			'sale_id' =>$sale_id,
			'item_id' => $item['item_id'],
			'line' => $item['line'],
			'seat' => $item['seat'],
			'description' => $item['description'],
			'serialnumber' => $item['serialnumber'],
			'quantity_purchased' => $item['quantity'],
			'discount_percent' => $item['discount'],
			'item_cost_price' => $cur_item_info->cost_price,
			'item_unit_price' => $item['price']
		);

		// Insert item into table
		$success = $this->db->insert('table_items', $sales_items_data);

		// Insert any modifiers
		if(!empty($item['modifiers'])){
			foreach($item['modifiers'] as $key => $modifier){
				$modifierData = array();
				$modifierData['sale_id'] = $sale_id;
				$modifierData['line'] = $item['line'];
				$modifierData['modifier_id'] = $modifier['modifier_id'];
				$modifierData['option'] = $modifier['selected_option'];

				if(empty($modifier['selected_price'])){
					$modifier['selected_price'] = '0.00';
				}
				$modifierData['price'] = $modifier['selected_price'];
				$this->db->insert('table_items_modifiers', $modifierData);
			}
		}

		// Attach any sides to item
		if(!empty($sides)){
			foreach($sides as $side){
				$this->save_item_side($sale_id, $item['line'], $side);
			}
		}

		return $success;
	}

	// Adds a side to a line item
	function save_item_side($sale_id, $line, $side){

		$this->delete_item_side($sale_id, $item['line']);
		$cur_item_info = $this->Item->get_info($item['item_id']);

		$side_data = array(
			'sale_id' => (int) $sale_id,
			'item_id' => (int) $side['item_id'],
			'cart_line' => $line,
			'position' => (int) $side['position'],
			'type' => $side['type'],
			'date_created' => date('Y-m-d h:i:s')
		);

		// Insert item into table
		$success = $this->db->insert('table_item_sides', $side_data);

		// Insert any modifiers
		if(!empty($side['modifiers']) && !empty($side['modifiers'][0])){
			foreach($side['modifiers'] as $key => $modifier){
				$modifierData = array();
				$modifierData['sale_id'] = $sale_id;
				$modifierData['side_position'] = (int) $side['position'];
				$modifierData['side_type'] = $side['type'];
				$modifierData['line'] = $line;
				$modifierData['modifier_id'] = $modifier['modifier_id'];
				$modifierData['option'] = $modifier['selected_option'];

				if(empty($modifier['selected_price'])){
					$modifier['selected_price'] = '0.00';
				}
				$modifierData['price'] = $modifier['selected_price'];
				$this->db->insert('table_item_side_modifiers', $modifierData);
			}
		}

		return $success;
	}

	// Removes a line item from cart and any receipts, including
	// associated sides and modifiers
	function delete_item($sale_id, $line = false){

		$keyData = array('sale_id' => (int) $sale_id, 'line' => (int) $line);

		$this->db->query("SET foreign_key_checks = 0");
		$this->db->delete('table_items_modifiers', $keyData);
		$this->db->delete('table_receipt_items', $keyData);
		$this->delete_item_side($sale_id, $line);
		$success = $this->db->delete('table_items', $keyData);
		$this->db->query("SET foreign_key_checks = 1");

		return $success;
	}

	// Removes a side from an item, including modifiers of that side
	// If 'type' or 'position' is not supplied, it will delete ALL sides
	// for that specific item
	function delete_item_side($sale_id, $line, $type = null, $position = null){

		$keyData = array(
			'sale_id' => (int) $sale_id,
			'cart_line' => (int) $line
		);
		$modifierKeyData = array(
			'sale_id' => (int) $sale_id,
			'line' => (int) $line
		);

		if($type !== null){
			$keyData['type'] = $type;
			$modifierKeyData['side_type'] = $type;
		}
		if($position !== null){
			$keyData['position'] = (int) $position;
			$modifierKeyData['side_position'] = (int) $position;
		}

		$this->db->query("SET foreign_key_checks = 0");
		$this->db->delete('table_item_side_modifiers', $modifierKeyData);
		$success = $this->db->delete('table_item_sides', $keyData);
		$this->db->query("SET foreign_key_checks = 1");

		return $success;
	}

	// Retrieve all items associated with a specific table
	function get_items($sale_id = false, $lines = null){
		$this->load->library('Sale_lib');
		$this->load->model('modifier');
		$cart = array();

		if(empty($sale_id)){
			return $cart;
		}

		// Get list of item taxes for this table
		$this->db->select('tax.item_id, tax.percent, tax.cumulative, tax.name');
		$this->db->from('items_taxes AS tax');
		$this->db->join('table_items AS table_item', 'table_item.item_id = tax.item_id', 'inner');
		$this->db->where('table_item.sale_id', (int) $sale_id);
		$this->db->order_by('tax.item_id, tax.cumulative');
		$result = $this->db->get();
		$rows = $result->result_array();
		$taxes = array();

		// Organize array of taxes by line number
		foreach($rows as $row){
			$taxes[$row['item_id']][] = array(
				'percent' => $row['percent'],
				'name' => $row['name'],
				'cumulative' => $row['cumulative']
			);
		}
		unset($rows);

		// Get list of items that have been sent to kitchen (ordered)
		$this->db->select('ticket_item.line');
		$this->db->from('table_tickets AS ticket');
		$this->db->join('table_ticket_items AS ticket_item', 'ticket.ticket_id = ticket_item.ticket_id', 'inner');
		$this->db->where('ticket.sale_id', $sale_id);
		$this->db->group_by('ticket_item.line');
		$this->db->order_by('ticket_item.line ASC');
		$result = $this->db->get();
		$rows = $result->result_array();
		$items_ordered = array();

		// Organize array of items ordered by line number
		foreach($rows as $row){
			$items_ordered[(int) $row['line']]= (int) $row['line'];
		}
		unset($rows);

		if(!empty($lines) && !is_array($lines)){
			$lines = array($lines);
		}

		// Select all food and beverage items with associated modifiers
		// and sides
		$this->db->select('cart_item.line, cart_item.item_id, i.course_id,
			i.name, i.department, i.category, i.subcategory, i.supplier_id,
			i.item_number, cart_item.description, i.max_discount,
			i.number_of_sides AS number_sides, 0 AS number_salads, 0 AS number_soups,
			cart_item.discount_percent AS discount,
			cart_item.is_paid, cart_item.is_ordered, i.quantity, i.is_unlimited,
			cart_item.item_unit_price AS price, cart_item.seat,
			cart_item.quantity_purchased AS quantity,

			modifier.modifier_id,
			modifier.name AS modifier_name,
			modifier.default_price AS modifier_default_price,
			modifier.options AS modifier_options,
			item_modifier.price AS modifier_selected_price,
			item_modifier.option AS modifier_selected_option', false);
		$this->db->from('table_items AS cart_item');
		$this->db->join('items AS i', 'i.item_id = cart_item.item_id', 'left');
		$this->db->join('table_items_modifiers AS item_modifier',
			'cart_item.line = item_modifier.line AND item_modifier.sale_id = cart_item.sale_id', 'left');
		$this->db->join('modifiers AS modifier', 'modifier.modifier_id = item_modifier.modifier_id', 'left');
		$this->db->where('cart_item.sale_id', $sale_id);
		$this->db->group_by('cart_item.line, modifier.modifier_id');
		$this->db->order_by('cart_item.line ASC');

		if(!empty($lines)){
			$this->db->where_in('cart_item.line', $lines);
		}

		$result = $this->db->get();
		$rows = $result->result_array();

		// Organize items with modifiers as sub array
		foreach($rows as $key => $row){
			$line = (int) $row['line'];
			$item_id = (int) $row['item_id'];
			$food_item =& $cart[$line];

			if(!isset($cart[$line])){
				$sides = array();
				$food_item = $row;
				$food_item['modifiers'] = array();

				// Add taxes to item as array list
				$food_item['taxes'] = array();
				if(!empty($taxes[$item_id])){
					$food_item['taxes'] = $taxes[$item_id];
				}

				$food_item['number_sides'] = (int) $food_item['number_sides'];
				$food_item['number_salads'] = (int) $food_item['number_salads'];
				$food_item['number_soups'] = (int) $food_item['number_soups'];
				$food_item['is_ordered'] = (bool) (int) $food_item['is_ordered'];
				$food_item['is_paid'] = (bool) (int) $food_item['is_paid'];
				$food_item['modifier_total'] = 0;

				// Get any sides associated with item
				$sides = $this->get_item_sides($sale_id, $line, null, null, array(
					'quantity' => $food_item['quantity'],
					'discount' => $food_item['discount']
				));
				$food_item += $sides;

				unset($food_item['modifier_name'], $food_item['modifier_id'],
					$food_item['modifier_default_price'], $food_item['modifier_options'],
					$food_item['modifier_selected_option'], $food_item['modifier_selected_price']);
			}

			// Add modifier(s) to item
			if(!empty($row['modifier_id'])){
				$modifier = array(
					'modifier_id' => $row['modifier_id'],
					'name' => $row['modifier_name'],
					'price' => $row['modifier_default_price'],
					'options' => $row['modifier_options'],
					'selected_option' => $row['modifier_selected_option'],
					'selected_price' => $row['modifier_selected_price']
				);

				$structured_modifier = $this->modifier->structure_options($modifier);
				$cart[$line]['modifiers'][] = $structured_modifier;

				// Add price of modifiers to subtotal of item
				$food_item['modifier_total'] += (float) $structured_modifier['selected_price'];
			}

			// Calculate tax and final total of item
			$food_item['subtotal'] = $this->sale_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], $food_item['quantity'], $food_item['discount']);
			$food_item['tax'] = $this->sale_lib->calculate_tax($food_item['subtotal'], $food_item['taxes']);

			// Add in total cost of sides
			$food_item['total'] = $food_item['subtotal'] + $food_item['tax'];
		}

		unset($rows);
		return array_values($cart);
	}

	// Retrieves all sides for a cart line item
	// Sides include (type): soups, salads, sides
	function get_item_sides($sale_id, $line, $type = null, $positions = null, $inherit = null){
		$this->load->library('Sale_lib');
		$this->load->model('modifier');
		$sides = array();

		if(empty($sale_id)){
			return $cart;
		}

		// Get list of item taxes for sides
		$this->db->select('tax.item_id, tax.percent, tax.cumulative, tax.name');
		$this->db->from('items_taxes AS tax');
		$this->db->join('table_item_sides AS side', 'side.item_id = tax.item_id', 'inner');
		$this->db->where('side.sale_id', (int) $sale_id);
		$this->db->where('side.cart_line', (int) $line);
		$this->db->order_by('tax.item_id, tax.cumulative');
		$result = $this->db->get();
		$rows = $result->result_array();
		$taxes = array();

		// Organize array of taxes by item id
		foreach($rows as $row){
			$taxes[$row['item_id']][] = array(
				'percent' => $row['percent'],
				'name' => $row['name'],
				'cumulative' => $row['cumulative']
			);
		}
		unset($rows);

		if(!empty($positions) && !is_array($positions)){
			$positions = array($positions);
		}

		// Select all item sides with associated modifiers
		$this->db->select('side.position, side.type, i.item_id,
			i.name, i.department, i.category, i.subcategory,
			i.item_number, i.max_discount, i.is_unlimited,
			i.unit_price AS price,

			modifier.modifier_id,
			modifier.name AS modifier_name,
			modifier.default_price AS modifier_default_price,
			modifier.options AS modifier_options,
			side_modifier.price AS modifier_selected_price,
			side_modifier.option AS modifier_selected_option', false);
		$this->db->from('table_item_sides AS side');
		$this->db->join('items AS i', 'i.item_id = side.item_id', 'left');
		$this->db->join('table_item_side_modifiers AS side_modifier',
			'side_modifier.sale_id = side.sale_id AND side_modifier.line = side.cart_line AND side.type = side_modifier.side_type AND side.position = side_modifier.side_position', 'left');
		$this->db->join('modifiers AS modifier', 'modifier.modifier_id = side_modifier.modifier_id', 'left');
		$this->db->where('side.sale_id', (int) $sale_id);
		$this->db->where('side.cart_line', (int) $line);
		if(!empty($type)){
			$this->db->where('side.type', $type);
		}
		$this->db->group_by('side.cart_line, side.position, side.type, modifier.modifier_id');
		$this->db->order_by('side.type, side.position ASC');
		if(!empty($positions)){
			$this->db->where_in('side.position', $positions);
		}

		$result = $this->db->get();
		$rows = $result->result_array();

		// Organize sides with modifiers and taxes in multi-dimensional array
		$keys = array('soups' => 0, 'salads' => 0, 'sides' => 0);
		foreach($rows as $key => $row){
			$position = (int) $row['position'];
			$side_type = $row['type'];
			$item_id = (int) $row['item_id'];
			$sideKey = $keys[$side_type];
			$food_item =& $sides[$side_type][$sideKey];

			if(!isset($sides[$side_type][$position])){
				$food_item = $row;
				$food_item['modifiers'] = array();

				if(!empty($inherit)){
					$food_item = array_merge($inherit, $food_item);
				}

				if($item_id == 0){
					$food_item['name'] = 'None';
					$food_item['item_id'] = 0;
					$food_item['price'] = 0;
					$food_item['tax'] = 0;
					$food_item['total'] = 0;
					$food_item['discount'] = 0;
					$food_item['quantity'] = 0;
				}

				// Add taxes to side as array list
				$food_item['taxes'] = array();
				if(!empty($taxes[$item_id])){
					$food_item['taxes'] = $taxes[$item_id];
				}
				$food_item['modifier_total'] = 0;

				unset($food_item['modifier_name'], $food_item['modifier_id'],
					$food_item['modifier_default_price'], $food_item['modifier_options'],
					$food_item['modifier_selected_option'], $food_item['modifier_selected_price']);
				$keys[$side_type]++;
			}

			// Add modifier(s) to side
			if(!empty($row['modifier_id'])){
				$modifier = array(
					'modifier_id' => $row['modifier_id'],
					'name' => $row['modifier_name'],
					'price' => $row['modifier_default_price'],
					'options' => $row['modifier_options'],
					'selected_option' => $row['modifier_selected_option'],
					'selected_price' => $row['modifier_selected_price']
				);

				$structured_modifier = $this->modifier->structure_options($modifier);
				$food_item['modifiers'][] = $structured_modifier;

				// Add price of modifiers to subtotal of side
				$food_item['modifier_total'] += (float) $structured_modifier['selected_price'];
			}

			// Calculate tax and final total of side
			$food_item['subtotal'] = $this->sale_lib->calculate_subtotal($food_item['price'] + $food_item['modifier_total'], 1, 0);
			$food_item['tax'] = $this->sale_lib->calculate_tax($food_item['subtotal'], $food_item['taxes']);
			$food_item['total'] = $food_item['subtotal'] + $food_item['tax'];
		}
		unset($rows);
		unset($taxes);

		if(!empty($type)){
			return $sides[$type];
		}else{
			return $sides;
		}
	}

	function get_payments($sale_id, $receipt_id = null, $payment_type = null){
		if(empty($sale_id)){
			return false;
		}

		$this->db->select('sale_id, receipt_id, payment_type, payment_amount, credit_card_invoice_id');
		$this->db->from('table_payments');
		$this->db->where('sale_id', (int) $sale_id);
		if(!empty($receipt_id)){
			$this->db->where('receipt_id', (int) $receipt_id);
		}
		if(!empty($payment_type)){
			$this->db->where('payment_type', strtolower($payment_type));
		}
		$this->db->order_by('receipt_id');
		$query = $this->db->get();

		$rows = $query->result_array();

		if(!empty($payment_type)){
			return $rows[0];
		}

		return $rows;
	}

	function add_payment($sale_id, $receipt_id, $type, $amount, $credit_card_id = null){
		if(empty($sale_id) || empty($receipt_id) || empty($type) || empty($amount)){
			return false;
		}
		$amount = (float) round($amount, 2);

		$success = $this->db->query("INSERT INTO foreup_table_payments (sale_id, receipt_id, payment_type, payment_amount, credit_card_invoice_id)
			VALUES (".(int) $sale_id.",".(int) $receipt_id.",".$this->db->escape($type).",".$amount.",".(int) $credit_card_id.")
				ON DUPLICATE KEY UPDATE
					payment_amount = payment_amount + VALUES(payment_amount)");
		return $success;
	}

	function delete_payment($sale_id, $receipt_id, $type){
		if(empty($sale_id) || empty($receipt_id) || empty($type)){
			return false;
		}

		$success = $this->db->delete('table_payments', array('sale_id'=>(int) $sale_id, 'receipt_id'=>(int) $receipt_id, 'payment_type'=>$type));
		return $success;
	}

	function mark_items_paid($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_paid' => 1));
	}

	function mark_items_unpaid($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_paid' => 0));
	}

	function mark_items_ordered($sale_id, $items){

		if(empty($items)){
			return false;
		}
		if(!is_array($items)){
			$items = array($items);
		}

		$this->db->where_in('line', $items);
		$this->db->where('sale_id', $sale_id);
		return $this->db->update('table_items', array('is_ordered' => 1));
	}
}
?>

<?php
class Invoice extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($customer_id = false, $limit=10000, $offset=0, $join = false)
	{
		$this->db->from('invoices');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invoices.deleted !=', 1);
		if ($join)
			$this->db->join('people', 'people.person_id = invoices.person_id');
		if ($customer_id)
			$this->db->where('invoices.person_id', $customer_id);
		$this->db->offset($offset);
		$this->db->limit($limit);
		$this->db->order_by('month_billed DESC');		
		$all = $this->db->get();
		//echo $this->db->last_query();
		return $all;
	}
	function count_all()
	{
		$this->db->from('invoices');
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('invoice_items');
		$this->db->join('invoices', 'invoice_items.charge_id = invoices.charge_id');
		$this->db->join('customer_credit_cards', 'customer_credit_cards.credit_card_id = invoices.credit_card_id');
		$this->db->where('customer_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, invoice_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}
	/*function get_info_by_teesheet_id($teesheet_id)
	{
		$this->db->from('billing');
		$this->db->where("teesheet_id = '$teesheet_id'");
		
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}*/
	/*function get_monthly_limits($course_id = '')
	{
		$this->db->from('billing');
		$this->db->select('sum(email_limit) AS email_limit, sum(text_limit) AS text_limit, sum(annual_amount) AS annual_amount, sum(monthly_amount) AS monthly_amount');
		$this->db->where('deleted', 0);
		$this->db->where('(email_limit > 0 OR text_limit > 0)');
		if ($course_id != '')
			$this->db->where('course_id', $course_id);
		
		$results = $this->db->get()->result_array();
		
		$data = array(
			'email_limit'=>$results[0]['email_limit'],
			'text_limit'=>$results[0]['text_limit'],
			'annual_amount'=>to_currency($results[0]['annual_amount']),
			'monthly_amount'=>to_currency($results[0]['monthly_amount'])
		);
		return $data;
	}	*/
	
	/*function get_teetime_stats()
	{
		$this->db->from('billing');
		$this->db->select('COUNT(billing_id) AS courses');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		$teetime_course_count = $this->db->get()->result_array();
		
		$this->db->from('billing');
		$this->db->select('teetimes_daily, teetimes_weekly, start_date');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		//Teetime trade data
		$ttd = $this->db->get()->result_array();
		$potential_teetimes = 0;
		foreach ($ttd as $td)
		{
			$now = time(); // or your date as well
			$date = strtotime($td['start_date']);
			$date_diff = $now - $date;
			$days = floor($date_diff/(60*60*24));
			
			$potential_teetimes += $days * $td['teetimes_daily'];
			$potential_teetimes += floor($days/7 * floor($td['teetimes_weekly']));
		}
		
		
		$this->db->from('teetimes_bartered');
		$this->db->select('COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue');
		$this->db->join('sales_payments_credit_cards', 'sales_payments_credit_cards.invoice = teetimes_bartered.invoice_id');
		
		$sold_teetimes = $this->db->get()->result_array();
		
		$data = array(
			'courses'=>$teetime_course_count[0]['courses'],
			'potential_teetimes'=>$potential_teetimes,
			'sold_teetimes'=>$sold_teetimes[0]['sold'],
			'revenue'=>to_currency($sold_teetimes[0]['teetime_revenue'])
		);
		
		return $data;
	}*/
	/*
	function get_teetime_course_stats()
	{
		$this->load->model('Communication');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		
		$this->db->from('billing');
		$this->db->select('course_id, teetimes_daily, teetimes_weekly, start_date');
		$this->db->where('teetimes', 1);
		$this->db->where('deleted', 0);
		
		//Teetime trade data
		$ttd = $this->db->get()->result_array();
		//echo '<br/><br/>'.$this->db->last_query();
		//print_r($ttd);
		$potential_teetimes = 0;
		$return_array = array();
		foreach ($ttd as $td)
		{
			if (!isset($return_array[$td['course_id']]))
				$return_array[$td['course_id']] = $td;
			$now = strtotime(date('Y-m-d')); // or your date as well
			$date = strtotime($td['start_date']);
			$date_diff = $now - $date;
			$days = floor($date_diff/(60*60*24));
			
			$return_array[$td['course_id']]['potential_teetimes'] += $days * $td['teetimes_daily'];
			$return_array[$td['course_id']]['potential_teetimes'] += floor($days/7 * floor($td['teetimes_weekly']));
			if ($date < strtotime($end_date))
			{
				$date_diff = ($date > strtotime($start_date))?strtotime($end_date) - $date:strtotime($end_date) - strtotime($start_date);
				$days = floor($date_diff/(60*60*24));
			
				$return_array[$td['course_id']]['potential_this_month'] += $days * $td['teetimes_daily'];
				$return_array[$td['course_id']]['potential_this_month'] += floor($days/7 * floor($td['teetimes_weekly']));
			}
			if ($date < strtotime($pm_end_date))
			{
				$date_diff = ($date > strtotime($pm_start_date))?strtotime($pm_end_date) - $date:strtotime($pm_end_date) - strtotime($pm_start_date);
				$days = floor($date_diff/(60*60*24));
			
				$return_array[$td['course_id']]['potential_last_month'] += $days * $td['teetimes_daily'];
				$return_array[$td['course_id']]['potential_last_month'] += floor($days/7 * floor($td['teetimes_weekly']));
			}
		}
		
		
//		$this->db->from('teetimes_bartered');
//		$this->db->select('course_id, COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue');
//		$this->db->join('sales_payments_credit_cards', 'sales_payments_credit_cards.invoice = teetimes_bartered.invoice_id');
		
		$query = $this->db->query("SELECT foreup_courses.course_id, COUNT(teetime_id) AS sold, SUM(auth_amount) AS teetime_revenue,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', 1, 0 )) AS sold_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', 1, 0 )) AS sold_last_month,
			sum(IF (date_booked >= '$start_date' AND date_booked <= '$end_date', auth_amount, 0 )) AS revenue_this_month, 
			sum(IF (date_booked >= '$pm_start_date' AND date_booked <= '$pm_end_date', auth_amount, 0 )) AS revenue_last_month 
			FROM foreup_teetimes_bartered
			JOIN foreup_sales_payments_credit_cards ON foreup_sales_payments_credit_cards.invoice = foreup_teetimes_bartered.invoice_id
			JOIN foreup_courses ON foreup_courses.course_id = foreup_sales_payments_credit_cards.course_id
			GROUP BY course_id
			ORDER BY foreup_courses.name
		");
		//echo $this->db->last_query();
		//$sold_array = array();
		foreach($query->result_array() as $result)
			if ($result['course_id'] > 0)
				$return_array[$result['course_id']] = (isset($return_array[$result['course_id']]))? array_merge($return_array[$result['course_id']], $result): $result;
		//echo '<br/>Return Array<br/>';
		//print_r($return_array);
		//echo '<br/>Sold Array<br/>';
		//print_r($sold_array);
		//$data = count($sold_array) > 0 ? array_merge_recursive($return_array, $sold_array):$return_array;
		//echo '<br/>Last query<br/>'.$this->db->last_query();
		//print_r($data);
		return $return_array;
	}*/
	/*
	function have_sellable_teetimes($date)
	{
		$teesheet_id = $this->session->userdata('teesheet_id');
		$this->db->select('SUM(teetimes_daily) AS teetimes_daily');
		$this->db->from('billing');
		$this->db->where('teesheet_id', $teesheet_id);
		$result = $this->db->get();
		$results = $result->result_array();	
		
		$sold_count = $this->get_daily_bartered_teetime_count($date);
		
		if ($result->num_rows() > 0)
			return $results[0]['teetimes_daily'] > $sold_count;
		else 
			return false;
	}
	 */
	/*
	function get_daily_bartered_teetime_count($date)
	{
		$teesheet_id = $this->session->userdata('teesheet_id');
		
		$this->db->from('teetimes_bartered');
		$this->db->like('start', substr($date, 0, 8), 'left');
		$this->db->where('teesheet_id', $teesheet_id);
		$query = $this->db->get();
		return $query->num_rows();
	}
	 */
	/*
	Gets invoice by course_id and invoice number
	*/
	function get_by_invoice_number($invoice_number)
	{				
		$this->db->from('invoices');
		$this->db->where('invoice_number', $invoice_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
	/*
	Gets information about a particular item
	*/
	function get_info($invoice_id)
	{
		$this->db->from('invoices');
		$this->db->where("invoice_id", $invoice_id);
		
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->result_array();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('invoices');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return (array) $item_obj;
		}
	}
	
	/*
	Gets existing invoice for recurring billing
	*/
	function get_recurring_billing_invoice($billing_info, $start_date, $end_date)
	{		
		$this->db->from('invoices');
		$this->db->where('month_billed >=', $start_date);
		$this->db->where('month_billed <=', $end_date);	
		$this->db->where('billing_id',$billing_info['billing_id']);
		$this->db->limit(1);
		$this->db->where('deleted', 0);
		$result = $this->db->get()->row_array();
		
		return $result;
	}
	
	/*
	Inserts or updates an item
	*/
	function save(&$invoice_data,$invoice_id=false)
	{
		if (!$invoice_id or !$this->exists($invoice_id))
		{
			//set the next invoice_number
			$this->db->select_max('invoice_number');
			$this->db->from('invoices');
			$this->db->where('course_id', $invoice_data['course_id']);
			$result = $this->db->get()->row_array();
			$invoice_data['invoice_number'] = (int)$result['invoice_number'] + 1;
			
			if($this->db->insert('invoices',$invoice_data))
			{
				$invoice_data['invoice_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('invoice_id', $invoice_id);
        $val = $this->db->update('invoices',$invoice_data);
		//echo json_encode(array('sql'=>$this->db->last_query()));
		//return;
        
		return $val;
	}
	function save_items($items, $invoice_id)
	{
		// echo "<pre>";
		// print_r($items);
		// echo "</pre>";
		if (count($items) > 0)
		{
			//if invoice exists 1)blast invoice items and resave all items
			$this->db->where('invoice_id',$invoice_id);
			$this->db->delete('invoice_items');
			
			return $this->db->insert_batch('invoice_items', $items);	
		} else {
			return true;
		}
		
	}
	function get_items($invoice_id)
	{
		$this->db->from('invoice_items');
		$this->db->where('invoice_id', $invoice_id);
		$this->db->order_by('line_number');
		$result = $this->db->get()->result_array();
		return $result;
	}
	function get_overdue_items($invoice_id, $customer_id, $date = false)
	{
		return array();
		$this->db->from('invoice_items');
		$this->db->join('invoices', "invoices.invoice_id = invoice_items.invoice_id");
		$this->db->where('invoice_items.invoice_id !=', $invoice_id);
		$this->db->where('person_id', $customer_id);
		$this->db->where('paid_off <', date('Y-m-01 00:00:00', strtotime($date)));
		$this->db->where('amount > paid_amount');
		$this->db->order_by('invoice_items.invoice_id DESC, line_number ASC');
		$result = $this->db->get()->result_array();
		return $result;
	}
	/*
	Deletes one item
	*/
	function delete($invoice_id)
	{
		$this->db->where('invoice_id',$invoice_id);
		return $this->db->update('invoices', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('billing_id',$item_ids);
		return $this->db->update('billing', array('deleted' => 1));
 	}

	/*
	Email out the invoice
 	*/
	function send_email($invoice_id)
	{		
		$this->load->model('Customer_credit_card');
		$invoice_data = $this->get_info($invoice_id);	
		$data = $invoice_data[0];				
		$course_info = $this->Course->get_info($data['course_id']);			
		$data['items']=$this->get_items($invoice_id);
		$person_info = $this->Customer->get_info($data['person_id']);
		$data['person_info']= $person_info;
		$data['course_info']=$course_info;		
		$data['credit_cards']= $this->Customer_credit_card->get($data['person_id']);
		$data['is_invoice'] = true;
		$data['sent'] = true;
		$data['popup'] = 1;	
		$data['emailing_invoice'] = true;
			
		$success = send_sendgrid(		
			$person_info->email,			
			"Invoice From {$course_info->name}",
			$this->load->view('customers/invoice', $data, true),			
			$course_info->email,
		 	$course_info->name		 	
		);		
		
		if ($success)
		{
			$data = array('emailed'=> 1, 'send_date'=>date('Y-m-d'));
			$this->save($data, $invoice_id);
		}
		return ($success ? true : false);
	}	
	
	function get_todays_invoices_to_bill($days = false, $limit = 10)
	{
		$month = date('n');
		if ($days === false)
		{
			$day = date('j');
			$today = date('Y-m-d');
			
		}
		else 
		{
			$day = date('j', strtotime($days.' day'));
			$today = date('Y-m-d', strtotime($days.' day'));
		}
		
		return $this->db->query("SELECT
					token,
					total,
					paid,
					cardholder_name,
					invoice_id,
					invoice_number,
					billing_id,
					person_id,
					employee as employee_id,
					email_invoice,
					foreup_invoices.course_id AS course_id,
					foreup_invoices.credit_card_id AS credit_card_id
					FROM (`foreup_invoices`) 
					LEFT OUTER JOIN `foreup_customer_credit_cards` ON `foreup_invoices`.`credit_card_id` = `foreup_customer_credit_cards`.`credit_card_id` 
					WHERE `foreup_invoices`.`deleted` != 1 
					AND day = $day 
					AND MONTH(month_billed) = $month  
					AND foreup_invoices.credit_card_id != ''
					AND foreup_invoices.credit_card_id != 0
					AND started = 0
					AND total - paid > 0
					ORDER BY `foreup_invoices`.`course_id`
					LIMIT $limit");
		
	}
	
	function mark_as_started($invoice_id)
	{
		$data = array(
			'last_billing_attempt'=>date('Y-m-d 00:00:00'),
			'started'=>1,
			'charged'=>0,
			'emailed'=>0
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}
	
	function mark_as_charged($invoice_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}
	
	function mark_as_emailed($billing_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('foreup_invoices', $data);
	}
	  
}
?>

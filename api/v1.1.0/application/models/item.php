<?php
class Item extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($item_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('items');
		$this->db->where("item_id = '$item_id' $course_id");
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function item_number_exists($item_number, $item_id)
	{
		if ($item_number == NULL)
			return false;
		$this->db->from('items');
		$this->db->where("item_number", $item_number);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('item_id !=', $item_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit=10000, $offset=0, $minus_teetimes=true, $quickbuttons=false, $food_or_pro = 0)
	{
        $course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

		$this->db->from('items');
		$this->db->where("deleted = 0 $course_id");
        //if ($minus_teetimes) {
        $this->db->where('invisible !=', 1);
        $this->db->where('food_and_beverage', $food_or_pro);
//            $this->db->where('category !=', 'Old Green Fees');
  //          $this->db->where('category !=', 'Old Carts');
    //        $this->db->where('category !=', 'Green Fees');
      //      $this->db->where('category !=', 'Carts');
       // }

		if ($quickbuttons || $food_or_pro) {
			$this->db->order_by("category", "asc");
			$this->db->order_by("subcategory", "asc");
		}
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_item_sides($item_id = false){
		return $this->db->query("
			SELECT *
				FROM foreup_items
				LEFT JOIN (
					SELECT side_id, not_available, `default`, upgrade_price
						FROM foreup_item_sides
						WHERE item_id = $item_id
					)
				AS i_s ON foreup_items.item_id = i_s.side_id
				WHERE course_id = {$this->session->userdata('course_id')}
					AND deleted = 0
					AND is_side = 1
				ORDER BY category ASC,
					subcategory ASC,
					name ASC");
	}

	function save_side_options($side_options, $item_id)
	{
		$this->delete_side_options($item_id);
		foreach($side_options as $side_id => $side_option)
		{
			$side_option['side_id'] = $side_id;
			$side_option['item_id'] = $item_id;
			$this->db->insert('item_sides', $side_option);
		}
		return true;
	}
	function delete_side_options($item_id)
	{
		$this->db->where('item_id', $item_id);
		$this->db->delete('item_sides');
	}

	function get_all_food($sides = false){

		$this->load->model('modifier');
		$food_items = array();

		// Select all food and beverage items with associated modifiers
		// and available sides
		$this->db->select("i.item_id, i.course_id, i.name, i.department,
			i.category, i.category, i.subcategory, i.supplier_id, i.item_number,
			i.description, i.unit_price AS price, i.max_discount,
			i.quantity AS quantity_available, i.is_unlimited,
			i.number_of_sides AS number_sides,

			modifier.modifier_id,
			modifier.name AS modifier_name,
			modifier.default_price AS modifier_default_price,
			modifier.options AS modifier_options,

			GROUP_CONCAT(DISTINCT CONCAT(tax.name,'^:^',tax.percent,'^:^',tax.cumulative)) AS taxes", false);
		$this->db->from('items AS i');
		$this->db->join('item_modifiers AS item_modifier', 'i.item_id = item_modifier.item_id', 'left');
		$this->db->join('modifiers AS modifier', 'modifier.modifier_id = item_modifier.modifier_id', 'left');
		$this->db->join('items_taxes AS tax', 'tax.item_id = i.item_id', 'left');
		//$this->db->join('item_sides AS hidden_side', 'hidden_side.item_id = i.item_id AND hidden_side.not_available = 1', 'left');
		$this->db->where('i.food_and_beverage', 1);
		$this->db->where('i.deleted', 0);
		if($sides){
			$this->db->where('i.is_side', 1);
		}else{
			$this->db->where('i.is_side', 0);
		}
		$this->db->where('i.invisible !=', 1);
		$this->db->where('i.course_id', $this->session->userdata('course_id'));
		$this->db->group_by('i.item_id, modifier.modifier_id');

		$result = $this->db->get();
		$rows = $result->result_array();

		// Organize items with modifiers as sub array
		foreach($rows as $key => $row){
			$item_id = (int) $row['item_id'];
			$food_item =& $food_items[$item_id];

			if(!isset($food_items[$item_id])){
				$food_item = $row;
				$food_item['modifiers'] = array();

				// Break apart tax column into array
				$taxes = array();
				$taxesArray = explode(',', $food_item['taxes']);
				foreach($taxesArray as $tax){
					$taxArray = explode('^:^', $tax);
					$taxes[] = array('name' => $taxArray[0], 'percent' => $taxArray[1], 'cumulative' => $taxArray[2]);
				}
				$food_item['taxes'] = $taxes;

				// Turn hidden side list into array
				$food_item['hidden_sides'] = explode(',', $food_item['hidden_sides']);

				$food_item['number_sides'] = (int) $food_item['number_sides'];
				$food_item['subtotal'] = $this->sale_lib->calculate_subtotal($food_item['price'], 1, 0);
				$food_item['tax'] = $this->sale_lib->calculate_tax($food_item['subtotal'], $food_item['taxes']);
				$food_item['total'] = $food_item['subtotal'] + $food_item['tax'];

				// Placeholder to test categories displayed in item edit view
				if($sides){
					$food_item['view_category'] = 5;
				}

				unset($food_item['modifier_name'], $food_item['modifier_id'],
					$food_item['modifier_default_price'], $food_item['modifier_options'],
					$food_item['taxes_percentage'], $food_item['taxes_cumulative']);
			}

			// Add modifier(s) to item
			if(!empty($row['modifier_id'])){
				$modifier = array(
					'modifier_id' => $row['modifier_id'],
					'name' => $row['modifier_name'],
					'price' => $row['modifier_default_price'],
					'options' => $row['modifier_options']
				);

				$structured_modifier = $this->modifier->structure_options($modifier);
				$food_items[$item_id]['modifiers'][] = $structured_modifier;

				$food_item['subtotal'] += (float) $structured_modifier['selected_price'];
				$food_item['total'] += (float) $structured_modifier['selected_price'];
			}
		}
		unset($rows);
		return array_values($food_items);
	}

	function count_all($minus_teetimes=true)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('items');
		$this->db->where("deleted = 0 $course_id");
        if ($minus_teetimes) {
        	$this->db->where('invisible !=', 1);
//            $this->db->where('category !=', 'Green Fees');
  //          $this->db->where('category !=', 'Carts');
        }
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular item
	*/
	function get_info($item_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('items');
		$this->db->where("item_id = '$item_id' $course_id");

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('items');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->from('items');
		$this->db->where("item_number = '$item_number' $course_id");

		$query = $this->db->get();
//echo $this->db->last_query().'<br/>';
		if($query->num_rows()==1)
		{
	//		print_r($query->row());
			return $query->row()->item_id;
		}

		return false;
	}

	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('items');
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('item_id',$item_ids);
		$this->db->order_by("item_id", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates an item
	*/
	function save(&$item_data,$item_id=false)
	{
		if ($item_data['quantity'] == '-')
			$item_data['quantity'] == 0;
		if ($item_data['reorder_level'] == '-')
			$item_data['reorder_level'] == 0;
		if ($this->item_number_exists($item_data['item_number'], $item_id))
		{
			$item_data['error'] = 'UPC # is already in use. Please select a different one.';
			return false;
		}
		else if (!$item_id or !$this->exists($item_id))
		{
			//$this->db->ignore();
        	if($this->db->insert('items',$item_data))
			{
				$item_data['item_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('item_id', $item_id);
		return $this->db->update('items',$item_data);
	}

	function save_image($item_id, $image_id){
		return $this->db->update('items',
			array('image_id' => (int) $image_id),
			"item_id = ".(int) $item_id."
				AND course_id = ".(int) $this->session->userdata('course_id')
			);
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data,$item_ids)
	{
		$this->db->where_in('item_id',$item_ids);
		return $this->db->update('items',$item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";

        if (!$this->get_item_category($item_id)) {
            $this->db->where("item_id = '$item_id' $course_id");
            return $this->db->update('items', array('deleted' => 1,'item_number'=>null));
        }
        return false;
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
        if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        //foreach ($item_ids as $index => $item_id)
        //{
         //   if ($this->get_item_category($item_id) == 'Green Fees' || $this->get_item_category($item_id) == 'Carts')// Fees')
          //      array_splice($item_ids, $index);

        //}
        $this->db->where_in('item_id',$item_ids);
		return $this->db->update('items', array('deleted' => 1,'item_number'=>null));
 	}

 	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($search,$limit=25,$minus_teetimes=true, $food_and_beverage=0)
	{
		$course_id = '';
                log_message('error', "GET SUGGESTION $food_and_beverage");
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$suggestions = array();

		$this->db->from('items');
		$this->db->like('name', $search);
		$this->db->where("deleted = 0 $course_id");
                $this->db->where("food_and_beverage",$food_and_beverage);
		if ($minus_teetimes) {
			$this->db->where('invisible !=', 1);
//            $this->db->where("((category != 'Green Fees' AND category != 'Carts') OR (category = 'Green Fees' AND subcategory = 'Regular') OR (category = 'Carts' AND subcategory = 'Regular'))");
		}
        $this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label' => $row->name);
		}

		$this->db->select('category');
		$this->db->from('items');
		$this->db->where("deleted = 0 $course_id");
                $this->db->where("food_and_beverage",$food_and_beverage);
        if ($minus_teetimes) {
			$this->db->where('invisible !=', 1);
//            $this->db->where("((category != 'Green Fees' AND category != 'Carts') OR (category = 'Green Fees' AND subcategory = 'Regular') OR (category = 'Carts' AND subcategory = 'Regular'))");
		}
		$this->db->distinct();
		$this->db->like('category', $search);
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=array('label' => $row->category);
		}

		$this->db->from('items');
		$this->db->like('item_number', $search);
		$this->db->where("deleted = 0 $course_id");
                $this->db->where("food_and_beverage",$food_and_beverage);
        if ($minus_teetimes) {
			$this->db->where('invisible !=', 1);
//            $this->db->where("((category != 'Green Fees' AND category != 'Carts') OR (category = 'Green Fees' AND subcategory = 'Regular') OR (category = 'Carts' AND subcategory = 'Regular'))");
		}
		$this->db->order_by("item_number", "asc");
		$by_item_number = $this->db->get();
		foreach($by_item_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->item_number);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;
	}

	function get_item_search_suggestions($search,$limit=25,$minus_teetimes=true)
	{
		$course_id = '';
		$mobile = $this->session->userdata('mobile');
        $teesheet_id = $this->session->userdata('teesheet_id');
		$suggestions = array();

		$this->db->from('items');
		$this->db->where("deleted", 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('invisible !=', 1);
//		$this->db->where('category !=', 'Old Green Fees');
//		$this->db->where('category !=', 'Old Carts');
//		$this->db->where('category !=', 'Green Fees');
//		$this->db->where('category !=', 'Carts');
        //$this->db->where("(item_number NOT LIKE CONCAT(CID,'%') OR item_number IS NULL)");
//		$this->db->where("(item_number LIKE '$search%' OR item_number IS NULL)");
        $this->db->where("(name LIKE '".$this->db->escape_like_str($search)."%' or
			name LIKE '% ".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		//echo $this->db->last_query();
		foreach($by_name->result() as $row)
		{
			//add this for calculating the price without tax on the tournament edit form for inventory items
			$tax_info = $mobile ? $this->Item_taxes->get_info($row->item_id) : array();

			$suggestions[]=array('value' => $row->item_id, 'label' => $row->name, 'price' => $row->unit_price, 'tax_info' => $tax_info, 'type'=>'item');
		}
		if ($this->config->item('simulator'))
		{
			$max = 8;
			$spacing = 0;
		}
		else
		{
			$max = 4;
			$spacing = 2;
		}
		for ($j = 1; $j <= $max; $j++)
		{
			$i = $j;
			if ($i > 2)
				$i += $spacing;
			$cart_categories = ($this->permissions->course_has_module('reservations')?$this->Fee->get_cart_types($i, $teesheet_id, false):$this->Green_fee->get_cart_types($i, $teesheet_id, false));
			$green_fee_categories = ($this->permissions->course_has_module('reservations')?$this->Fee->get_teetime_types($i+$spacing, $teesheet_id, false):$this->Green_fee->get_teetime_types($i+$spacing, $teesheet_id, false));
			//echo 'cart<br/>';
			//print_r($cart_categories);
			//echo 'grf';
			//print_r($green_fee_categories);
			foreach ($cart_categories as $index => $title)
			{
				$price_index = substr($index, 0, strpos($index, '_'));
				$item_id = $this->get_item_id($this->session->userdata('course_id').'_'.$i);
				$tax_info = $mobile ? $this->Item_taxes->get_info($item_id) : array();
				if (strpos(strtolower($title), strtolower($search)) === 0 || strpos(strtolower($title), ' '.strtolower($search)) !== false)
					$suggestions[]=array('value' => $item_id, 'label'=>$title, 'price_index'=>$price_index, 'teetime_type'=>$i, 'tax_info'=>$tax_info, 'type'=>'item');
			}
			foreach ($green_fee_categories as $index => $title)
			{
				$price_index = substr($index, 0, strpos($index, '_'));
				$item_id = $this->get_item_id($this->session->userdata('course_id').'_'.($i+$spacing));
				$tax_info = $mobile ? $this->Item_taxes->get_info($item_id) : array();
				if (strpos(strtolower($title), strtolower($search)) === 0 || strpos(strtolower($title), ' '.strtolower($search)) !== false)
					$suggestions[]=array('value' => $item_id, 'label'=>$title, 'price_index'=>$price_index, 'teetime_type'=>$i+$spacing, 'tax_info'=>$tax_info, 'type'=>'item');
			}
		}

		//only return $limit suggestions
		if(count($suggestions > $limit) && $limit != 0)
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_department_suggestions($search, $all = false)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('department');
		$this->db->from('items');
		$this->db->like('department', $search);
		$this->db->where("deleted = 0 $course_id");
		if (!$all)
			$this->db->where('invisible !=', 1);
//		$this->db->where('category !=', 'Green Fees');
//		$this->db->where('category !=', 'Carts');
		$this->db->order_by("department", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			if ($row->department != '')
				$suggestions[]=array('label' => $row->department, 'type'=>'department', 'value' => $row->department);
		}

		return $suggestions;
	}


	function get_category_suggestions($search)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('category');
		$this->db->from('items');
		$this->db->like('category', $search);
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('invisible !=', 1);
//		$this->db->where('category !=', 'Green Fees');
//		$this->db->where('category !=', 'Carts');
		$this->db->order_by("category", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			$suggestions[]=array('label' => $row->category, 'type'=>'category', 'value' => $row->category);
		}

		return $suggestions;
	}


	function get_subcategory_suggestions($search)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$suggestions = array();
		$this->db->distinct();
		$this->db->select('subcategory');
		$this->db->from('items');
		$this->db->like('subcategory', $search);
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('invisible !=', 1);
//		$this->db->where('category !=', 'Green Fees');
//		$this->db->where('category !=', 'Carts');
		$this->db->order_by("subcategory", "asc");
		$by_category = $this->db->get();
		foreach($by_category->result() as $row)
		{
			if ($row->subcategory != '')
				$suggestions[]=array('label' => $row->subcategory, 'type'=>'subcategory', 'value' => $row->subcategory);
		}

		return $suggestions;
	}

	/*
	Preform a search on items
	*/
        function item_type_choice($food_or_proshop = 0, $limit=20, $minus_teetimes=true, $offset=0)
        {
            $offset = ($offset == 'undefined')?0:$offset;

		$invisible = '';
		if ($minus_teetimes)
			$invisible = 'AND invisible != 1';

		$offset_sql = '';
		if ($limit != 0)
			$offset_sql = "LIMIT $limit OFFSET $offset";
                log_message('error', 'FOOD: ' . $food_or_proshop);
            $query = $this->db->query("SELECT * FROM foreup_items WHERE food_and_beverage='$food_or_proshop' AND deleted = 0 AND
				course_id = '{$this->session->userdata('course_id')}'$invisible
			ORDER BY name asc
			$offset_sql");
            if ($limit == 0)
                return $query->num_rows();
            return $query;
        }
	function search($search, $limit=20, $minus_teetimes=true, $offset=0, $food_and_beverage=0)
	{
            //log_message('error', "I AM IN SEARCH!!");
		$offset = ($offset == 'undefined')?0:$offset;

		$invisible = '';
		if ($minus_teetimes)
			$invisible = 'AND invisible != 1';


		$offset_sql = '';
		if ($limit != 0)
			$offset_sql = "LIMIT $limit OFFSET $offset";

		$query = $this->db->query("
			SELECT *
			FROM foreup_items
			WHERE
				(name LIKE '%".$this->db->escape_like_str($search)."%' OR
				item_number LIKE '%".$this->db->escape_like_str($search)."%' OR
				category LIKE '%".$this->db->escape_like_str($search)."%') AND
				deleted = 0 AND
                                food_and_beverage = $food_and_beverage AND
                                course_id = '{$this->session->userdata('course_id')}'
				$invisible
			ORDER BY name asc
			$offset_sql
		");

		//echo $this->db->last_query();
		// Just return a count of all search results
		if ($limit == 0)
			return $query->num_rows();

		return $query;
	}
    function get_category_items($category)
    {
	    $item_array = array();
        $this->db->from('items');
        $this->db->where("category", $category);
        $this->db->where("course_id", $this->session->userdata('course_id'));
        $this->db->order_by("name", 'asc');
        $result = $this->db->get();
        while ($row = $result->fetch_assoc()) {
            $item_array[$row['item_id']] = $row['name'];
        }

        return $item_array;
    }
    function get_categories()
	{
        /*return array(
            'Accessories'=>'Accessories',
            'Apparel'=>'Apparel',
            //'Carts'=>'Carts',
            'Equipment'=>'Equipment',
            'Food'=>'Food',
            'Misc'=>'Misc',
            'Services'=>'Services'
            //'Tee Times'=>'Tee Times'
        );*/
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
		$this->db->select('category');
		$this->db->from('items');
		$this->db->where("deleted = 0 $course_id");
		$this->db->where('invisible !=', 1);
//		$this->db->where('category !=', "Green Fees");
//		$this->db->where('category', "Carts");
		$this->db->distinct();
		$this->db->order_by("category", "asc");

		return $this->db->get();
	}
    function cleanup()
	{
		$item_data = array('item_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('items',$item_data);
	}
    function get_item_category($item_id)
    {
        $this->db->select('category');
		$this->db->from('items');
		$this->db->where("item_id", $item_id);
        $this->db->limit(1);

        $results = $this->db->get();
        if ($results->num_rows() > 0) {
            $row = $results->row();
            return $row->category;
        }

        return false;
    }
    function create_teetimes($course_id = '')
    {
    	if ($course_id == '')
            $course_id = $this->session->userdata('course_id');
		$course_info = $this->Course->get_info($course_id);
        //for ($i = 1; $i <= 30; $i++) {//Column of tee time price settings
//            	echo 'here';
		$simulator_names = array(
			1=>'30 Min',
			2=>'1 Hour',
			3=>'1 Hour 30 Min',
			4=>'2 Hours',
			5=>'2 Hours 30 Min',
			6=>'3 Hours',
			7=>'3 Hours 30 Min',
			8=>'4 Hours'
		);

        for ($j = 1; $j <= 8; $j++){//Row of tee time price settings
            if ($j == 1 || $j == 2 || $j == 5 || $j == 6) {
                $type = 'Cart';
                $category = 'Carts';// Fees';
            }
            else {
                $type = 'GF';
                $category = 'Green Fees';
            }
            if ($j < 5)
                $holes = 9;
            else
                $holes = 18;
			if ($j % 2 == 1)
				$day_type == 'Weekday';
			else
				$day_type == 'Weekend';

			if ($course_info->simulator)
			{
				$name = $simulator_names[$j];
			}
			else
			{
				$name = "$holes $day_type $type";
			}
            $item_data = array(
                'name'=>$name, 'description'=>'', 'item_number'=>"{$course_id}_{$j}",
                'category'=>$category, 'cost_price'=>'0', 'unit_price'=>'0', 'quantity'=>'0', 'reorder_level'=>'0', 'is_unlimited'=>'1',
                'location'=>'', 'allow_alt_description'=>'0', 'is_serialized'=>'0', 'course_id'=>$course_id, 'invisible' =>'1'
            );
            $this->save($item_data);
		}
        //}
    }
	function get_teetime_department($teesheet_id = '')
	{
        if ($teesheet_id == '')
            $teesheet_id = $this->session->userdata('course_id');
		$this->db->select('department');
        $this->db->from('items');
        $this->db->where('course_id', $teesheet_id);
        //$this->db->where('category', 'Green Fees');
		//$this->db->where('department !=', '');
		$this->db->where('item_number', $this->session->userdata('course_id').'_3');
		//$this->db->group_by('department');
		$result = $this->db->get();

		return $result->result_array();
    }
	function get_teetime_tax_rate($teesheet_id = '')
	{
        if ($teesheet_id == '')
            $teesheet_id = $this->session->userdata('course_id');
		$this->db->select('percent');
        $this->db->from('items_taxes');
		$this->db->join('items', 'items_taxes.item_id = items.item_id');
		$this->db->where("item_number REGEXP '{$teesheet_id}_[3-4,7-8]'");
        $this->db->where('items_taxes.course_id', $teesheet_id);
        $this->db->group_by('items_taxes.course_id');
		return $this->db->get()->result_array();
    }
    function get_cart_department($teesheet_id = '')
	{
        if ($teesheet_id == '')
            $teesheet_id = $this->session->userdata('course_id');
		$this->db->select('department');
        $this->db->from('items');
        $this->db->where('course_id', $teesheet_id);
        //$this->db->where('category', 'Carts');// Fees');
		//$this->db->where('department !=', '');
		$this->db->where('item_number', $this->session->userdata('course_id').'_1');
		$this->db->group_by('department');

		return $this->db->get()->result_array();
    }
	function get_cart_tax_rate($teesheet_id = '')
	{
        if ($teesheet_id == '')
            $teesheet_id = $this->session->userdata('course_id');
		$this->db->select('percent');
        $this->db->from('items_taxes');
        $this->db->join('items', 'items_taxes.item_id = items.item_id');
		$this->db->where("item_number REGEXP '{$teesheet_id}_[1-2,5-6]'");
        $this->db->where('items_taxes.course_id', $teesheet_id);
        $this->db->group_by('items_taxes.course_id');
		return $this->db->get()->result_array();
    }
    function get_teetime_prices($teesheet_id = '')
    {
        if ($teesheet_id == '')
            $teesheet_id = $this->session->userdata('course_id');

        $this->db->select('item_number, unit_price, item_id');
        $this->db->from('items');
        $this->db->where('course_id', $teesheet_id);
        $this->db->where('category', 'Green Fees');
        $this->db->or_where('category', 'Carts');// Fees');
        $this->db->like('item_number', "$teesheet_id_", 'after');
        $this->db->order_by('item_id');
        $teetime_prices = $this->db->get();

		$price_array = array();
        while ($teetime_price = $teetime_prices->fetch_assoc()) {
            $price_array[$teetime_price['item_number']] = array('price'=>$teetime_price['unit_price'], 'id'=>$teetime_price['item_id']);
        }
        return $price_array;
    }
    function is_teetime($item_id)
    {
    	$item_info = $this->get_info($item_id);
		if ($this->config->item('simulator') && $item_info->category == 'Green Fees' && preg_match('/_[1-8]$/', $item_info->item_number))
			return true;
        if ($item_info->category == 'Green Fees' && preg_match('/_[3,4,7,8]$/', $item_info->item_number))
            return true;

        return false;
    }
    function is_cart($item_id)
    {
    	$item_info = $this->get_info($item_id);
        if ($item_info->category == 'Carts' && preg_match('/_[1,2,5,6]$/', $item_info->item_number))
            return true;

        return false;
    }
	function Avery5160($x, $y, &$pdf, $text, $barcode) {
	    $left = 4.926; // 0.19" in mm
	    $top = 14.7; // 0.5" in mm
	    $width = 66.802; // 2.63" in mm
	    $height = 25.4; // 1.0" in mm
	    $hgap = 3.048; // 0.12" in mm
	    $vgap = 0.0;

	    $x = $left + (($width + $hgap) * $x);
	    $y = $top + (($height + $vgap) * $y);
		if ($text != '' && $barcode != '')
		{
		    $pdf->SetXY($x, $y);
			$course_name =  character_limiter($this->session->userdata('course_name'),30);
		    $pdf->MultiCell($width, 5, $course_name, 0, 'C');
		    $pdf->SetXY($x+3, $y+5);
			$pdf->Image(site_url('barcode')."?barcode=$barcode&text=&scale=2&thickness=20&font_size=10.png");
			$pdf->SetXY($x, $y+16);
			$pdf->MultiCell($width, 5, $text, 0, 'C');
		}

	}
	function Avery5267($x, $y, &$pdf, $text, $barcode) {
	    $left = 4.826; // 0.19" in mm
	    $top = 11.7; // 0.5" in mm
	    $width = 44; // 2.63" in mm
	    $height = 12.75; // 1.0" in mm
	    $hgap = 8.048; // 0.12" in mm
	    $vgap = 0.0;

    	$x = $left + (($width + $hgap) * $x);
	    $y = $top + (($height + $vgap) * $y);
		if ($text != '' && $barcode != '')
		{
		    $pdf->SetXY($x+7, $y);
			$pdf->Image(site_url('barcode')."?barcode=$barcode&text=$text&scale=1.5&thickness=20&font_size=7.png");
		}
	    //$pdf->MultiCell($width, 5, $text, 1, 'C');
	}
  function add_take_out_menu_items($items_array, $menu_id)
  {

      $this->db->query("DELETE FROM foreup_menu_items WHERE menu_id='$menu_id'");
      foreach ($items_array as $item)
      {
          //log_message('error', 'BACKEND: ' . $item);
          $this->db->query("INSERT INTO foreup_menu_items (menu_id, item_id) VALUES ('$menu_id', '". $item .'\')');
      }
      return true;
  }
  function has_restaurant()
  {
      $course_id = $this->config->item('course_id');
      $result = $this->db->query('SELECT food_and_beverage FROM foreup_courses WHERE course_id=\''.$course_id.'\'');
      $has_food = $result->result_array();
      //log_message('error', "HAS FOOD $has_food " . $has_food[0]['food_and_beverage']);
      return $has_food[0]['food_and_beverage'];
  }
}
?>

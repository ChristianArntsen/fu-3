<?php
class Inventory extends CI_Model 
{	
	function insert($inventory_data)
	{
		return $this->db->insert('inventory',$inventory_data);
	}
	
	function get_inventory_data_for_item($item_id)
	{
		$result =$this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date FROM (`foreup_inventory`) WHERE `trans_items` = '$item_id' ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
}

?>
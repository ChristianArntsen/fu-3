<?php 

/**
 *
 * Site Config Class, used for config variables that change per deployment but are static across all hosts
 * 	- DO NOT OVERWRITE FILE when delopying using SVN or git, this file defines the enviroment it is running in
 *
 * ++++++++ DEVELOPERS SHOULD ONLY EDIT LOCAL COPY OF THIS FILE +++++++
 *
 * @version     site_conf v1
 * @level1      ./
 * 
 * @category    Controller
 * @author      Noah Spirakus
 * @Create      2011/11/11
 * @Modify      2011/11/11
 * @Project     ---
 * @link	required from  ./www/index.php
 * 			- require_once __DIR__.'/../conf/conf.php';
 */

	


/*
 *---------------------------------------------------------------
 * Identification Variable
 *---------------------------------------------------------------
 *
 * Used for Logging identification of the server it is running on
 *
 */
	define('IDEN', 'WEB_SERVER_1');




/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     staging
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');



/*
 *---------------------------------------------------------------
 * LOGGING
 *---------------------------------------------------------------
 *
 * Where are the logs located at?
 * 
 */

	switch (ENVIRONMENT)
	{
		case 'development':
			define('SERVER_LOG', '/var/www/html/logs_folder');
			define('SERVER_DIR', '/var/www/html/domain_folder');
			break;
                
		case 'staging':			
			define('SERVER_LOG', '/var/www/html/logs_folder');
			define('SERVER_DIR', '/var/www/html/domain_folder');
			break;
		
		case 'production':			
			define('SERVER_LOG', '/var/www/html/logs_folder');
			define('SERVER_DIR', '/var/www/html/domain_folder');
			break;
                
		default:
			exit('The application environment is not set correctly.');
                
	}
        
/* End of file conf.php */
/* Location: ./conf.php */
var EditItemView = Backbone.View.extend({
	tagName: "div",
	attributes: {id: "edit-item"},
	template: _.template( $('#template_edit_item').html() ),
	options: {active_section: false},

	events: {
		"click a.submit_button": "saveItem",
		"click a.delete_button": "deleteItem",
		"click li.tab > a": "clickTab"
	},

	render: function() {
		var data = this.model.attributes;

		// Create array to mark which section are incomplete
		data.incomplete = {};
		data.incomplete['1'] = !this.model.isModifiersComplete(1);
		data.incomplete['2'] = !this.model.isModifiersComplete(2);
		data.incomplete['3'] = !this.model.isModifiersComplete(3);
		data.incomplete['4'] = !(this.model.isSoupsComplete() && this.model.isSaladsComplete());
		data.incomplete['5'] = !this.model.isSidesComplete();

		// Create array of sections (tabs) to display in item edit window
		data.sections = _.clone(App.item_categories);
		var modifiers = this.model.get('modifiers');

		// Remove any sections that don't have any options
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':1})){ delete data.sections['1']; }
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':2})){ delete data.sections['2']; }
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':3})){ delete data.sections['3']; }
		if(this.model.get('number_salads') == 0 && this.model.get('number_soups') == 0){ delete data.sections['4']; }
		if(this.model.get('number_sides') == 0){ delete data.sections['5']; }

		console.debug(data.sections); //JBDEBUG
		console.debug(this.model.attributes); //JBDEBUG

		this.$el.html(this.template(data));
		this.$el.find('#edit_item_seat').keypad({position: 'right', formatMoney: false});
		this.$el.find('#edit_item_price, #edit_item_discount').keypad({position: 'bottom'});
		this.renderSection(1);
		return this;
	},

	clickTab: function(event){
		var button = $(event.currentTarget);
		var category_id = button.data('category-id');
		var tab_pane = $(button.attr('href'));

		// Mark tab as active
		button.parents('li').addClass('active').siblings().removeClass('active');

		this.renderSection(category_id);

		// Show appropriate tab pane
		tab_pane.addClass('active').siblings().removeClass('active');
		return false;
	},

	renderSection: function(category_id){
		var viewData = {};
		viewData.category_id = category_id;
		viewData.model = this.model;

		// Modifiers
		var item = this.model;
		if(category_id <= 3){
			var view = new ItemModifiersView(viewData);

		// Soups/salads
		}else if(category_id == 4){
			var view = new ItemSidesView(viewData);

		// Sides
		}else if(category_id == 5){
			var view = new ItemSidesView(viewData);
		}

		// Close last section
		if(this.options.active_section){
			this.options.active_section.remove();
		}

		this.$el.find('#item-edit-' + category_id).html( view.render().el );
		return true;
	},

	saveItem: function(event){
		// Get new data from fields
		var seat = $('#edit_item_seat').val();
		var price = $('#edit_item_price').val();
		var discount = $('#edit_item_discount').val();

		// Update item with new properties
		this.model.set({"seat":seat, "price":price, "discount":discount});
		this.model.save();

		this.remove();
		$.colorbox.close();
		return false;
	},

	deleteItem: function(event){
		this.model.destroy();
		this.remove();
		$.colorbox.close();
		return false;
	}
});

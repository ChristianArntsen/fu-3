var ItemSide = Side.extend({
	idAttribute: "position",
	default: {
		'item_id': 0,
		'price': 0,
		'tax': 0,
		'total': 0,
		'subtotal': 0
	}
});
var Payment = Backbone.Model.extend({
	idAttribute: "type",
	defaults: {
		"type": null,
		"amount": 0.00,
		"date": "",
		"card_number": ""
	},

	initialize: function(attrs, options){
		this.listenTo(this, 'invalid', this.error, this);
	},

	error: function(){
		this.collection.trigger('invalid', this);
		this.destroy();
	},

	parse: function(response, options){
		if(this.collection.get(response.type)){
			this.collection.get(response.type).set(response);
		}
	},

	validate: function(attributes, options){
		if(typeof(attributes.amount) == 'string'){
			var amount = parseFloat(attributes.amount.replace(/[^0-9/.]+/g, ''));
		}else{
			var amount = attributes.amount;
		}

		if(!amount || amount < 0){
			return "Payment amount must be greater than 0";
		}
	}
});

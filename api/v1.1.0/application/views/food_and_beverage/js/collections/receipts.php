var ReceiptCollection = Backbone.Collection.extend({
	url: App.api_table + "receipts",

	model: function(attrs, options){
		return new Receipt(attrs, options);
	},

	// Get all items selected within receipts
	getSelectedItems: function(){
		var selectedItems = [];

		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				selectedItems = selectedItems.concat(
					items.where({"selected":true})
				);
			}
		});

		return selectedItems;
	},

	unSelectItems: function(){
		// Loop through each receipt
		_.each(this.models, function(receipt){
			var items = receipt.get('items');
			if(items){
				var selectedItems = items.where({"selected":true});
				_.each(selectedItems, function(item){
					item.unset("selected");
				});
			}
		});

		return this;
	},

	getNextNumber: function(){
		var receiptNum = 0;
		_.each(this.models, function(receipt){
			var curNum = parseInt(receipt.get('receipt_id'));

			if(curNum && curNum > receiptNum){
				receiptNum = curNum;
			}
		});

		return receiptNum + 1;
	},

	getSplitItems: function(){
		var itemSplits = {};
		if(this.length == 0){
			return itemSplits;
		}

		// Loop through each receipt
		_.each(this.models, function(receipt){

			// Loop through each item in receipt
			var items = receipt.get('items');
			_.each(items.models, function(item){
				var line = item.get('line');

				// Store counts of split items in array
				if(itemSplits[line]){
					itemSplits[line]++;
				}else{
					itemSplits[line] = 1;
				}
			});
		});

		return itemSplits;
	},

	allReceiptsPaid: function(){
		if(this.where({'status':'pending'}).length == 0){
			return true;
		}else{
			return false;
		}
	}
});

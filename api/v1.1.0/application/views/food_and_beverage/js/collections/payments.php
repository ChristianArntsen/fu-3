var PaymentCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new Payment(attrs, options);
	},

	getTotal: function(){
		var total = 0.00;

		_.each(this.models, function(payment){
			total += parseFloat(payment.get('amount'));
		});

		return parseFloat(accounting.toFixed(total, 2));
	}
});
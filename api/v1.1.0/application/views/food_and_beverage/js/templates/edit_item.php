<style>
#modifiers_form {
	margin: 20px;
	font-size: 18px;
}

#modifiers_form input {
	padding: 5px;
	font-size: 20px;
	width: 100px;
}

#modifiers_form label {
	display: block;
	float: left;
	line-height: 35px;
	height: 35px;
	margin-right: 15px;
}

#modifiers_form div.form_field {
	margin-bottom: 10px;
	display: block;
	float: left;
}

#sales_item_modifiers {
	display: block;
	margin: 10px 0px 0px 0px;
	padding: 0px;
	overflow: hidden;
	width: auto;
}

#sales_item_modifiers li {
	display: block;
	padding: 0px 0px 5px 0px;
	margin: 0px;
	list-style-type: none;
	font-size: 18px;
	overflow: hidden;
}

#sales_item_modifiers span {
	float: left;
	display: block;
	height: 42px;
	line-height: 42px;
}

#sales_item_modifiers span.value {
	float: none;
	display: inline;
}

#sales_item_modifiers span.name {
	width: 280px;
}

#sales_item_modifiers span.options {
	width: 450px;
	float: right;
	display: block;
	overflow: hidden;
}

#sales_item_modifiers span.options a {
	float: right;
}

#sales_item_modifiers span.price {
	width: 65px;
	text-align: right;
}

div.sides {
	padding: 15px;
}

#edit-item .option_button,
#edit-item .delete_button,
#edit-item .submit_button {
    background: -moz-linear-gradient(center top , #349AC5, #4173B3) repeat scroll 0 0 transparent;
    background: -webkit-gradient(linear, center top, center bottom, color-stop(0%, #349AC5), color-stop(100%, #4173B3));
    border: 1px solid #232323;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px 0 rgba(255, 255, 255, 0.5) inset, 0 3px 1px -2px rgba(255, 255, 255, 0.2);
    color: white;
    display: block;
    font-size: 16px;
	float: left;
    font-weight: normal;
    height: 40px !important;
    line-height: 40px !important;
    margin: 0px;
    padding: 0 20px;
    text-align: center;
    text-shadow: 0 -1px 0 black;
}

#edit-item a.selected {
	box-shadow: 0 0px 20px 5px black inset;
}

#edit-item a.delete_button {
	background: #d14d4d;
	background: -moz-linear-gradient(top,  #d14d4d 0%, #c03939 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d14d4d), color-stop(100%,#c03939));
	background: -webkit-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: -o-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: -ms-linear-gradient(top,  #d14d4d 0%,#c03939 100%);
	background: linear-gradient(to bottom,  #d14d4d 0%,#c03939 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d14d4d', endColorstr='#c03939',GradientType=0 );
    float: left;
}

#modifiers_form .submit_button {
	float: right !important;
	margin: 20px 0px 0px 0px !important;
	display: block !important;
}

#item_sides li {
	display: block;
	float: none;
	overflow: hidden;
	width: 300px;
}

#item_sides li a.choose_side {
	line-height: 25px;
	height: 25px;
	padding: 10px 0px;
}

ul.breadcrumb {
	display: block;
	width: auto;
	padding: 0px;
	margin: 0px;
	float: none;
	overflow: hidden;
}

ul.breadcrumb > li {
	display: block;
	float: left;
	padding: 0px;
	margin: 0px;
	height: 50px;
	width: 20%;
	position: relative;
	border-bottom: 1px solid #444;
	background: #54b2e5;
	background: -moz-linear-gradient(top,  #54b2e5 0%, #2474b4 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#54b2e5), color-stop(100%,#2474b4));
	background: -webkit-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: -o-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: -ms-linear-gradient(top,  #54b2e5 0%,#2474b4 100%);
	background: linear-gradient(to bottom,  #54b2e5 0%,#2474b4 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#54b2e5', endColorstr='#2474b4',GradientType=0 );
}

ul.breadcrumb > li.incomplete {
	background: #d14d4d;
	background: -moz-linear-gradient(top,  #d14d4d 0%, #ad2b2b 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d14d4d), color-stop(100%,#ad2b2b));
	background: -webkit-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: -o-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: -ms-linear-gradient(top,  #d14d4d 0%,#ad2b2b 100%);
	background: linear-gradient(to bottom,  #d14d4d 0%,#ad2b2b 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d14d4d', endColorstr='#ad2b2b',GradientType=0 );
}

ul.breadcrumb > li.active,
ul.breadcrumb > li.incomplete.active  {
	border-bottom: none;
	background: #EFEFEF;
}

ul.breadcrumb > li.active > a {
	color: #444;
	text-shadow: none;
}

ul.breadcrumb > li.incomplete.active > a {
	color: #d14d4d;
}

ul.breadcrumb > li > a {
	font-size: 16px;
	color: white;
	line-height: 50px;
	display: block;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.5);
	font-weight: bold;
	text-align: center;
	border-left: 1px solid rgba(0,0,0,0.2);
	border-right: 1px solid rgba(255,255,255,0.25);
}

#edit-item div.tab-pane {
	padding: 15px;
}

div.tab-content {
	display: block;
	width: auto;
	min-height: 325px;
	overflow: auto;
}

div.tab-content > div.tab-pane {
	display: none;
	overflow: auto;
	float: none;
}

div.tab-content > div.tab-pane.active {
	display: block;
}

#edit-item div.form_field {
	float: left;
	overflow: auto;
}

#edit-item div.row {
	display: block;
	overflow: hidden;
	padding: 15px;
}

#edit-item div.side {
	display: block;
	overflow: hidden;
	float: left;
	width: 425px;
}

#edit-item div.form_field input {
	width: 75px;
}

#edit-item div.form_field label {
	line-height: 35px;
	padding: 0px 5px;
	font-size: 16px;
}
</style>

<script type="text/html" id="template_edit_item">
<ul class="breadcrumb">
	<%
	_.each(sections, function(name, cat_id){
	var incompleteClass = '';
	if(incomplete[cat_id]){ incompleteClass = ' incomplete'; } %>
	<li class="tab<%-incompleteClass %><% if(cat_id == 1){ print(' active') }; %>" style="width: <%-accounting.toFixed(100 / _.size(sections), 2)%>%">
		<a data-category-id="<%-cat_id%>" href="#item-edit-<%-cat_id%>"><%-name%></a>
	</li>
	<% }); %>
</ul>
<div class="tab-content">
	<% _.each(App.item_categories, function(name, cat_id){ %>
	<div class="tab-pane <% if(cat_id == 1){ print('active') }; %>" id="item-edit-<%-cat_id%>">
		<%-name%>
	</div>
	<% }); %>
</div>
<div class="row">
	<div class="form_field">
		<label>Price</label>
		<input type="text" name="item[price]" id="edit_item_price" value="<%=accounting.formatMoney(price,'')%>" />
	</div>
	<div class="form_field" style="margin-left: 35px;">
		<label>Discount (%)</label>
		<input type="text" name="item[discount]" id="edit_item_discount" value="<%=discount%>" />
	</div>
	<div class="form_field">
		<label>Seat Number</label>
		<input type="text" name="item[discount]" id="edit_item_seat" value="<%=seat%>" />
	</div>
</div>
<div class="row">
	<a class="submit_button" href="#" style="float: right;">Save Item</a>
	<a class="delete_button" href="#">Delete Item</a>
</div>
</script>
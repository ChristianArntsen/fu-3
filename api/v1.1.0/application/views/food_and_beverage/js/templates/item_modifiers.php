<script type="text/html" id="template_item_modifiers">
<ul id="sales_item_modifiers">
<% if(modifiers){ %>
<% _.each(modifiers.models, function(modifier){
if(modifier.get('category_id') != category_id){
	return true;
}

if(modifier.get('selected_option') == 'no') {
	var is_selected = false;
	var selected_price = "0.00";
} else {
	var selected_price = modifier.get('selected_price');
	var is_selected = true;
} %>
	<li data-selected-price="<%= selected_price %>" data-selected-option="<%= modifier.get('selected_option') %>">
		<span class="name"><%= modifier.get('name') %></span>
		<span class="price">
			$<span class="value"><%= selected_price %></span>
		</span>
		<span class="options">
		<% _.each(modifier.get('options'), function(option){ %>
		<% if(modifier.get('selected_option') == option.label) {
			var btnClass = ' selected';
		}else{
			var btnClass = '';
		} %>
			<a data-modifier-id="<%= modifier.get('modifier_id') %>" data-price="<%= modifier.get('price') %>" class="option_button<%= btnClass %>" data-value="<%= option.label %>" href="#">
				<%-_.capitalize(option.label)%>
			</a>
		<% }); %>
		</span>
	</li>
<% }); %>
<% }else{ %>
<li>No modifiers available</li>
<% } %>
</ul>
</script>
<?php
function generate_periods($period, $years = 3, $name = '', $id = '', $selected = 0){
	$periods = array();
	$selectOptions = array();

	switch($period){
		case 'week':
			$periodCount = 52;
			$currentPeriod = date('W');

			$periodNum = $currentPeriod;
			for($x = 0; $x < $periodCount; $x++){
				if($periodNum == 0){
					$periodNum = $periodCount;
				}

				$periods[] = 'Week '. (int) $periodNum;
				$periodNum--;
			}
		break;
		case 'month':
			$periodCount = 12;
			$currentPeriod = date('n');

			$periodNum = $currentPeriod;
			for($x = 0; $x < $periodCount; $x++){
				if($periodNum == 0){
					$periodNum = $periodCount;
				}

				$periods[] = date('M', mktime(0, 0, 0, $periodNum));
				$periodNum--;
			}
		break;
	}

	$html = '<select name="'.$name.'" id="'.$id.'" style="display: none;">';
	if($period == 'day'){
		for($p = 0; $p < 31; $p++){
			$isSelected = '';
			if($selected == -$p){
				$isSelected = 'selected="selected"';
			}

			$extra = '';
			if($p == 0){
				$extra = '(Today)';
			}else if($p == 1){
				$extra = '(Yesterday)';
			}
			$html .= '<option '.$isSelected.' value="'.-$p.'">'.$p.' days ago '.$extra.'</option>';
		}

	}else if($period != 'year'){
		$totalPeriods = ($periodCount * $years) - ($periodCount - $currentPeriod);

		$y = date('Y');
		$html .= '<optgroup label="'.$y.'">';

		for($p = 0; $p < $totalPeriods ; $p++){
			if($p % $periodCount == $currentPeriod){
				$y--;
				$html .= '</optgroup><optgroup label="'.$y.'">';
			}

			$isSelected = '';
			if($selected == -$p){
				$isSelected = 'selected="selected"';
			}
			$html .= '<option '.$isSelected.' value="'.-$p.'">'.$y .' - '. $periods[$p % $periodCount].'</option>';
		}
		$html .= '</optgroup>';

	}else{
		$curYear = $x = (int) date('Y');
		$startYear = $curYear - $years;

		for($x = $curYear; $x > $startYear; $x--){
			$p = $x - $curYear;
			$isSelected = '';
			if($selected == $p){
				$isSelected = 'selected="selected"';
			}
			$html .= '<option '.$isSelected.' value="'.$p.'">'.abs($x).'</option>';
		}
	}
	$html .= '</select>';

	return $html;
}
?>
<style type="text/css">
label {
	font-size: 14px;
	margin-bottom: 2px;
	display: block;
}

div.field_row {
	margin-top: 10px;
}

select, input {
	padding: 5px !important;
}

ul.metrics {
	display: block;
	overflow: hidden;
	background-color: white;
	width: 250px;
	float: left;
	height: 300px;
	border: 1px solid #E0E0E0;
	margin-right: 25px;
}

ul.metrics > li {
	display: block;
	margin: 0px;
	padding: 5px;
	float: none;
	font-weight: normal;
}

ul.metrics > li.category {
	border-top: #DDD;
	border-bottom: #888;
	padding: 8px 5px;
	font-weight: bold;
	background: #d8d8d8;
	color: #222;
	background: -moz-linear-gradient(top,  #d8d8d8 0%, #cecece 50%, #afafaf 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d8d8d8), color-stop(50%,#cecece), color-stop(100%,#afafaf));
	background: -webkit-linear-gradient(top,  #d8d8d8 0%,#cecece 50%,#afafaf 100%);
	background: -o-linear-gradient(top,  #d8d8d8 0%,#cecece 50%,#afafaf 100%);
	background: -ms-linear-gradient(top,  #d8d8d8 0%,#cecece 50%,#afafaf 100%);
	background: linear-gradient(to bottom,  #d8d8d8 0%,#cecece 50%,#afafaf 100%);
}

ul.metrics > li a.remove {
	float: right;
	color: red;
	padding-top: 5px;
}

ul.metrics > li select {
	display: inline-block !important;
	margin-left: 10px !important;
	float: none !important;
}

ul.metrics > li.category:hover {
	background-color: gray;
	color: #222;
}

ul.metrics > li:hover {
	cursor: pointer;
	background-color: #E8EFFF;
}

#selected-metrics > li:hover {
	cursor: arrow !important;
}

#selected-metrics > li > a:hover {
	cursor: pointer !important;
}

div.field_row {
	display: block;
	overflow: hidden;
	float: left;
}

div.field_row.clearfix {
	float: none;
}

ul.widget-type {
	display: block;
	margin: 10px 0px 0px 0px;
	padding: 0px;
	width: auto;
	position: relative;
}

ul.widget-type li {
	padding: 5px 5px 5px 5px;
	margin: 1px 18px 6px 1px;
	float: left;
	list-style-type: none;
	position:relative;
}

ul.widget-type li label {
	width: auto;
	display: inline;
}

ul.widget-type li label:hover {
	cursor: pointer;
}

ul.widget-type li h6 {
	text-transform: none;
	float: left;
	line-height: 140%;
	margin-left: 5px;
	font-size: 12px;
	color: #666;
}

ul.widget-type li input {
	float: left;
	line-height: 120%;
}

ul.widget-type li.selected {
	border: 1px solid #D1DDEC;
	background-color: #EEF5FD;
	margin: 0px 17px 5px 0px;
}

ul.widget-type li img {
	display: block;
	margin-top: 20px;
}
</style>
<script>
function highlight_widget_type(){
	$('ul.widget-type').find('li').removeClass('selected');
	$('ul.widget-type li input[type="radio"]:checked').parents('li:first').addClass('selected');
}

function set_period(){
	var period = $('#period').val();
	$('#selected-metrics select').each(function(e){
		$(this).html( $('#metric_period_' + period).html() );
	});
}

function filter_periods(){
	var widgetType = $('ul.widget-type li input[type="radio"]:checked').val();
	var period = $('#period').val();

	switch(period){
		case 'day':
			var allowedGroupings = new Array('hour');
			var defaultGrouping = 'hour';
		break;
		case 'week':
			var allowedGroupings = new Array('day', 'dayname', 'hour');
			var defaultGrouping = 'day';
		break;
		case 'month':
			var allowedGroupings = new Array('day', 'dayname', 'hour', 'week');
			var defaultGrouping = 'day';
		break;
		case 'year':
			var allowedGroupings = new Array('dayname', 'hour', 'month', 'week');
			var defaultGrouping = 'month';
		break;
	}

	if(widgetType == 'number' || widgetType == 'pie'){
		var allowedGroupings = new Array('total');
		var defaultGrouping = 'total';
	}

	$('#data_grouping option').each(function(i){
		if( $.inArray($(this).val(), allowedGroupings) == -1){
			$(this).attr('disabled', 'disabled');
		}else{
			$(this).attr('disabled', null);
		}
	});

	$('#data_grouping option[value="'+defaultGrouping+'"]').attr('selected', 'selected');
}

$(function(){
	$('#available-metrics li:not(li.category)').click(function(e){

		$('#selected-metrics li.empty').remove();
		var metricCount = parseInt($('#selected-metrics').attr('data-metric-count'));
		var selectedMetric = $(this).clone();

		selectedMetric.append( '<a href="#" class="remove">[X]</a>' );

		var period = $('#period').val();
		var periodMenu = $('#metric_period_' + period).clone().css('display', 'block').attr('id', '').attr('name', 'metrics[' + metricCount + '][relative_period]');
		selectedMetric.append( periodMenu );

		var inputMetricId = $('<input name="metrics['+ metricCount +'][metric]" value="' + selectedMetric.attr('data-metric') +'" type="hidden" />');
		var inputPosition = $('<input name="metrics['+ metricCount +'][position]" value="' + metricCount +'" type="hidden" />');

		selectedMetric.append( inputMetricId );
		selectedMetric.append( inputPosition );

		$('#selected-metrics').append( selectedMetric );
		$('#selected-metrics').attr('data-metric-count', parseInt(parseInt($('#selected-metrics').attr('data-metric-count')) + 1));

		return false;
	});

	$('#widget-form').submit(function(e){
		var data = $(this).serialize();
		var url = $(this).attr('action');
		var form = $(this);

		$(form).mask("<?php echo lang('common_wait'); ?>");

		$.post(url, data, function(response){
			if(response.success){
				render_widget(response.widget.widget_id, response.widget.name, response.widget.type, response.widget.width, response.widget.height, response.widget.pos_x, response.widget.pos_y, 100);
				$('li.widget.empty').hide();
				init_widget_drag();
				init_widget_resize();
				set_feedback('Widget created','success_message', false);
				$.colorbox.close();
			}else{
				set_feedback(response.message,'error_message', false);
			}

			$(form).unmask();
		},'json');

		return false;
	});

	$('#selected-metrics a.remove').live('click', function(e){
		$(this).parent('li:first').remove();
		return false;
	});

	$('ul.widget-type li input').live('click', function(){
		highlight_widget_type();
		filter_periods();
	});

	$('#period').change(function(e){
		set_period();
		filter_periods();
	});

	$('#dashboard_width').val( $('ul.widgets').width() );

	highlight_widget_type();
	filter_periods();
});
</script>
<?php
echo generate_periods('week', 3, '', 'metric_period_week');
echo generate_periods('month', 3, '', 'metric_period_month');
echo generate_periods('day', 3, '', 'metric_period_day');
echo generate_periods('year', 3, '', 'metric_period_year');
?>
<div style="width: 800px; display: block; padding: 15px;">
	<form name="widget" id="widget-form" method="post" action="<?php echo site_url('dashboards/save_widget'); ?>/<?php echo $widget['widget_id']; ?>">
		<input name="dashboard_id" type="hidden" value="<?php echo $dashboard_id; ?>" />
		<input name="dashboard_width" id="dashboard_width" type="hidden" value="0" />

		<div class="clearfix">
			<div class="field_row" style="width: 450px;">
				<label for="widget-name">Widget Title</label>
				<?php $input_options = array(
					'name' => 'name',
					'value' => $widget['name'],
					'id' => 'widget-name',
					'style' => 'width: 425px; display: block;'
				);
				echo form_input($input_options); ?>
			</div>
			<div class="field_row" style="width: 90px;">
				<label for="widget-period">Period</label>
				<?php echo form_dropdown('period', array('day'=>'Day','week'=>'Week','month'=>'Month','year'=>'Year'), $widget['period'], 'id="widget-period" style="width: 70px;"'); ?>
			</div>
			<div class="field_row" style="width: 160px;">
				<label for="widget-group-by">Group By</label>
				<?php echo form_dropdown('data_grouping', array('hour'=>'Hour','dayname'=>'Day of Week','day'=>'Day','week'=>'Week','month'=>'Month','year'=>'Year','total'=>'Total'), $widget['data_grouping'], 'id="widget-group-by"'); ?>
			</div>
		</div>
		<div class="clearfix">
			<h3 style="margin-top: 10px; font-weight: bold; color: #222;">Widget Type</h3>
			<?php
			$widget_types = array('line' => 'Line Chart', 'column' => 'Column Chart', 'number' => 'Basic Number', 'bar' => 'Bar Graph', 'table' => 'Table');
			if(empty($widget['type']) || $widget['type'] == ''){
				$widget['type'] = 'line';
			} ?>
			<ul class="widget-type">
				<li>
					<label>
						<?php
						if($widget['type'] == 'line'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'line',
							'checked' => $checked
						);
						echo form_radio($input_options); ?>
						<h6>Line Chart</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-line.png" alt="Line Chart" />
					</label>
				</li>
				<li>
					<label>
						<?php
						if($widget['type'] == 'column'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'column',
							'checked' => $checked
						);
						echo form_radio($input_options); ?>
						<h6>Column Chart</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-column.png" alt="Bar Graph" />
					</label>
				</li>
				<li>
					<label>
						<?php
						if($widget['type'] == 'number'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'number',
							'checked' => $checked,
						);
						echo form_radio($input_options); ?>
						<h6>Basic Number</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-number.png" alt="Basic Number" />
					</label>
				</li>
				<li>
					<label>
						<?php
						if($widget['type'] == 'bar'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'bar',
							'checked' => $checked
						);
						echo form_radio($input_options); ?>
						<h6>Bar Chart</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-bar.png" alt="Horizontal Bar Graph" />
					</label>
				</li>
				<li>
					<label>
						<?php
						if($widget['type'] == 'pie'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'pie',
							'checked' => $checked
						);
						echo form_radio($input_options); ?>
						<h6>Pie Chart</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-pie.png" alt="Pie Chart" />
					</label>
				</li>
				<li>
					<label>
						<?php
						if($widget['type'] == 'table'){ $checked = true; } else { $checked = false; }
						$input_options = array(
							'name' => 'type',
							'value' => 'table',
							'checked' => $checked
						);
						echo form_radio($input_options); ?>
						<h6>Table</h6>
						<img src="<?php echo base_url(); ?>images/dashboard/widget-table.png" alt="Table" />
					</label>
				</li>
			</ul>
		</div>
		<div class="clearfix" style="margin-top: 15px;">
			<ul class="metrics" id="available-metrics">
				<?php foreach($metrics as $category_key => $cat_metrics){ ?>
				<li class="category"><?php echo $categories[$category_key]; ?></li>
				<?php foreach($cat_metrics as $metric_key => $metric){ ?>
					<li data-metric="<?php echo $category_key; ?>/<?php echo $metric_key; ?>">
						<?php echo $metric['name']; ?>
					</li>
				<?php } } ?>
			</ul>
			<ul class="metrics" id="selected-metrics" style="width: 350px;" data-metric-count="<?php echo (int) count($widget['metrics']); ?>">
			<?php if(!empty($widget['metrics'])){ ?>
			<?php foreach($widget['metrics'] as $key => $metric){ ?>
				<li>
					<?php echo $metrics[$metric['metric_category']][$metric['metric']]['name']; ?>
					<?php echo generate_periods($widget['period'], 3, "metrics[".$key."][relative_period]", '', $metric['relative_period_num']); ?>
					<a href="" class="remove">[X]</a>
					<input type="hidden" value="<?php echo $metric['metric_category']; ?>/<?php echo $metric['metric']; ?>" name="metrics[<?php echo $key; ?>][metric]" />
					<input type="hidden" value="<?php echo $metric['position']; ?>" name="metrics[<?php echo $key; ?>][position]" />
				</li>
			<?php } ?>
			<?php } ?>
			</ul>
		</div>
		<div class="field_row clearfix">
			<input type="submit" name="submit" value="Save" class="submit_button" />
		</div>
	</form>
</div>
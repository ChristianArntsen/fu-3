<?php 
//print_r($suspended_sales);
	$suspended_sales_html = '';
	$selected_table = '';
	foreach($suspended_sales->result_array() as $ss)
	{
		$selected_class = '';
		if ("{$ss['sale_id']}" == "$selected_suspended_sale_id")
		{
		//	print_r($ss);
			$selected_class = 'selected';
			$selected_table = $ss['table_id'];
		}
		$total = $this->Sale_suspended->get_sale_total($ss['sale_id']);
		$ss_customer = $this->Sale_suspended->get_customer($ss['sale_id']);
		//echo $this->db->last_query();
		$suspended_sales_html .= "<a class='table_number $selected_class' id='ss_{$ss['sale_id']}' href='javascript:void(0)' onclick='sales.unsuspend_sale({$ss['sale_id']}, {$ss['table_id']})'><div><div class='table_number_label'>Table {$ss['table_id']}</div><div class='price_box'>".to_currency($total)."</div><div class='clear'></div><div class='ss_customer_name'>".($ss_customer->last_name?$ss_customer->last_name.', '.$ss_customer->first_name:'')."</div></div></a>";
	}
	$suspended_sales_html = ($suspended_sales_html != '' ? $suspended_sales_html : '<div class="no_suspended_sales">No Suspended Sales</div>'); 
?>
<?php echo $suspended_sales_html;?>
<div class='clear'></div>
<div class='contextMenu' id='myMenu' style='display:none'>
	<ul>
	    <li id="delete_suspended_sale">Delete</li>            
	</ul>
</div>
<script type="text/javascript">
var bindings = {
	'delete_suspended_sale': function(t) {
		var sale_id = $(t).attr('id').replace('ss_','');
		//console.log(sale_id);
		//return;
		$.ajax({
           type: "POST",
           url: "index.php/sales/delete_suspended_sale/"+sale_id,
           data: "",
           success: function(response){
		   	//Delete suspended sale button
		   	$(t).remove();
		   },
           dataType:'json'
        });
    }
};
$('.table_number').contextMenu('myMenu',{
	bindings:bindings,
	onShowMenu: function(e, menu) {
        return menu;
      }
});
</script>
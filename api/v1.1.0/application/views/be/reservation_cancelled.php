<?php $this->load->view("partial/course_header"); ?>
<style>
	#menubar_background, #menubar_full{
		background:url('<?php echo base_url(); ?>images/header/header_piece.png') repeat-x 0 -2px transparent;
	}
</style>
<div id="course_name"><?php echo $course_info->name?></div>
<div id="thank_you_box"></div>
<div id="home_module_list">
	<?php if ($success) { ?>
		<div></div>
		<div>Your reservation has been successfully cancelled.</div>
	<?php } else { ?>
		<div>Your reservation could not be cancelled.</div>
		<div>Please email or call ForeUP for further assistance at 801.215.9487 or support@foreup.com</div>
	<?php } ?>
	<div>
		<div class='booking_details'>

		</div>
	</div>
    <!--p>An email has been sent to your email address with your teetime details.</p-->
    <br/><br/>

</div>
<?php $this->load->view("partial/course_footer"); ?>
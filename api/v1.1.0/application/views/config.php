<?php $this->load->view("partial/header"); ?>
<style>
    .ui-widget {
        font-size:.8em;
    }
    .ui-buttonset .ui-button {
        margin-right:-0.5em;
    }
    #slider { 
        margin: 10px 10px 10px 20px; 
        width:250px;
    }
    .ui-slider-handle {
        font-size:.8em;
        padding:1px 0 0 2px;
    }
    .tab_container {
	border: 1px solid #999;
	border-top: none;
	overflow: hidden;
	clear: both;
	float: left; 
        width: 900px;
	background: #fff;
        height:400px;
        margin:0 26px
}
.tab_content {
	padding: 20px;
	font-size: 1.2em;
        height:360px;
        overflow: auto;
}
    ul.tabs {
	margin: 0 26px;
	padding: 0;
	float: left;
	list-style: none;
	height: 46px; /*--Set height of tabs--*/
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
	width: 900px;
}
ul.tabs li {
	float: left;
	margin: 0;
	padding: 0;
	height: 45px; /*--Subtract 1px from the height of the unordered list--*/
	line-height: 45px; /*--Vertically aligns the text within the tab--*/
	border: 1px solid #999;
	border-left: none;
	margin-bottom: -1px; /*--Pull the list item down 1px--*/
	overflow: hidden;
	position: relative;
	background: #e0e0e0;
}
ul.tabs li a {
	text-decoration: none;
	color: #000;
	display: block;
	font-size: .9em;
	padding: 0 10px;
	border: 1px solid #fff; /*--Gives the bevel look with a 1px white border inside the list item--*/
	outline: none;
        width:114px;
        text-align: center;
}
ul.tabs li a div {
    height:12px;
}
ul.tabs li a span {
    font-size:.7em
}
ul.tabs li a:hover {
	background: #ccc;
}
html ul.tabs li.active, html ul.tabs li.active a:hover  { /*--Makes sure that the active tab does not listen to the hover properties--*/
	background: #fff;
	border-bottom: 1px solid #fff; /*--Makes the active tab look like it's connected with its content--*/
}
#time_button_set {
    float:left;
    margin:10px 0 0 16px;
}
#player_button_set {
    float:right;
    margin:10px 21px 0 0;
}
.search_controls {
    padding:5px;
}
.teetime {
    border:1px solid #dedede;
    padding:8px 15px;
    margin-bottom:5px;
}
.teetime .left {
    float:left;
    width:10%;
}
.teetime .center {
    float:left;
    width:75%;
    text-align: center;
}
.teetime .right {
    float:right;
    width:10%;
}
.teetime .left .players span {
    float: left;
}
.teetime .left .players span.ui-button-text {
    font-size: .6em;
    margin: 1px 0 0 2px;
}
.centered_submit {
    text-align:center;
}
.settings_note {
    color:#666;
    font-size:.8em;
    margin-left:10px;
}
.tee_time_label {
    font-size: 12px;
    text-align: right;
    width:15%;
    padding-right: 10px;
}
.price_cell {
    width:10%;
}
.price_label {
    width:70px;
    margin-right:5px;
}
fieldset tr.field_row {
    height:35px;
}
.small_note {
    font-size:.6em;
}
.first_cell {
    width:25%;
}
.day_cell {
    width:13%;
}
#green_fee_prices {
	scroll:auto;
}
</style>
<table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/<?php echo $controller_name; ?>.png' alt='title icon' />
		</td>
		<td id="title">
			<?php echo lang('module_'.$controller_name); ?>
		</td>
	</tr>
</table>
<?php
	echo form_open_multipart('config/save/',array('id'=>'config_form'));
?>
<!--div id="config_wrapper">
	<div id="dbBackup">
		<?php echo anchor('config/backup', lang('config_backup_database'), array('class' => 'dbBackup')); ?>
	</div-->
	
	
<fieldset id="config_info">
	<legend><?php echo lang("config_info"); ?></legend>

	<ul id="error_message_box"></ul>
<ul class="tabs">
        <li><a href="#tab1">Details</a></li>
        <li><a href="#tab2">Hours/Booking</a></li>
        <li><a href="#tab3">Green Fees</a></li>
        <li><a href="#tab4">Teesheet</a></li>
        <li><a href="#tab5">POS</a></li>
        <li><a href="#tab6">Misc</a></li>
    </ul>

    <div class="tab_container">
        <div id="tab1" class="tab_content">
        	<?php $this->load->view("config/details"); ?>
        </div>    
	    <div id="tab2" class="tab_content">
	        <?php $this->load->view("config/hours"); ?>
	    </div>
	    <div id="tab3" class="tab_content">
	        <?php $this->load->view("config/green_fees"); ?>
	    </div>    
	    <div id="tab4" class="tab_content">
		    <?php $this->load->view("config/teesheets"); ?>
	    </div>    
	    <div id="tab5" class="tab_content">
	        <?php $this->load->view("config/pos"); ?>
	    </div>
	    <div id="tab6" class="tab_content">
	        <?php $this->load->view("config/misc"); ?>
	    </div>
	</div>
        <div class="centered_submit">
<?php 
echo form_submit(array(
	'name'=>'submitf',
	'id'=>'submitf',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
        </div>
</fieldset>
</div>
<?php
echo form_close();
?>
<div id="feedback_bar"></div>
<script type='text/javascript'>
var price_table = {
    initialize:function(){
    	for (var i = 2; i < 32; i ++) {
	        this.hide_column(i);
	        this.set_header_callback('price_header_'+i, i);
    	}
        
        // Check if twilight or super twilight is active, and hide rows if not.
        if ($('select:[name=twilight_hour]').val() == '2399')
            this.deactivate_twilight_prices();
        if ($('select:[name=super_twilight_hour]').val() == '2399')
            this.deactivate_super_twilight_prices();
    },
    set_header_callback:function(header_input, index){
        $("."+header_input).keyup(function(){
            if ($("."+header_input).val() != '')
                price_table.show_column(index);
        });
        $("."+header_input).blur(function(){
            if ($("."+header_input).val() == '')
                price_table.hide_column(index);
        });
    },
    show_column:function(index){
        $('.price_col_'+index).show();
        if ($('.price_header_'+(index+1)))
            $('.price_header_'+(index+1)).show();
    },
    hide_column:function(index){
        if ($('.price_header_'+(index)).val() == '') {
            $('.price_col_'+index).hide();
            if ($('.price_header_'+(index+1)) && $('.price_header_'+(index+1)).val() == '')
                $('.price_header_'+(index+1)).hide();
        }
    },
    deactivate_twilight_prices:function(){
        $('.twilight_col').hide();
    },
    activate_twilight_prices:function(){
        $('.twilight_col').show();
    },
    deactivate_super_twilight_prices:function(){
        $('.super_twilight_col').hide();
    },
    activate_super_twilight_prices:function(){
        $('.super_twilight_col').show();
    }
}
//validation and submit handling
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$( "#teetime_department" ).autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
    price_table.initialize();
    //$('.price_box').decimalMask({separator: ".",
    //  decSize: 2,
    //  intSize: 8});
	var submitting = false;
	$('#config_form').validate({
		submitHandler:function(form)
		{
                    trace('about to ajax submit');
			if (submitting) { trace('already submitting');return;}
			submitting = true;
                    $(form).ajaxSubmit({
			success:function(response)
			{
                            if(response.success)
                            {
                                    set_feedback(response.message,'success_message',false);		
                            }
                            else
                            {
                                    set_feedback(response.message,'error_message',true);		
                            }
                            submitting = false;
			},
                        failure:function(response)
                        {
                            trace('failed');
                        },
			dataType:'json'
                    });

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			company: "required",
			address: "required",
    		phone: "required",
    		email:"email"
    		},
		messages: 
		{
     		company: "<?php echo lang('config_company_required'); ?>",
     		address: "<?php echo lang('config_address_required'); ?>",
     		phone: "<?php echo lang('config_phone_required'); ?>",
     		email: "<?php echo lang('common_email_invalid_format'); ?>"
     		
		}
	});
        $(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
});
</script>
<?php $this->load->view("partial/footer"); ?>



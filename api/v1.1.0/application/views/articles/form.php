<ul id="error_message_box"></ul>
<?php
	echo form_open('home/save_article/',array('id'=>'article_form', 'enctype'=>'multipart/form-data'));
?>
<fieldset id="article_basic_info">

<div class="field_row clearfix">
<?php echo form_label('Title', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'title',
		'size'=>'24',
		'maxlength'=>'16',
		'id'=>'title',
		'value'=>$article_info->title)
	);?>
	</div>
</div>

<div class="field_row clearfix">	
    <?php echo form_label('Image (360x260):', 'img',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_upload(array(
            'name'=>'img',
            'id'=>'img',
            'value'=>$article_info->img));?>		
    </div>	
</div>

<div class="field_row clearfix">	
    <?php echo form_label('Column:', 'column',array('class'=>'wide')); ?>
    <div class='form_field'>
    <?php echo form_dropdown('column', array(1=>1,2=>2,3=>3), $article_info->column);?>		
    </div>	
</div>
                          

<div class="field_row clearfix">
<?php echo form_label('Text:', 'text',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo form_textarea(array('name'=>'text','id'=>'text','value'=>$article_info->text,'rows'=>2));?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#article_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				$.colorbox.close();
                submitting = false;
                $('#home_page_container').replaceWith(response.home_page);
			},
			dataType:'json'
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>
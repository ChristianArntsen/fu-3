<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/login.css?<?php echo APPLICATION_VERSION; ?>" />
<!--link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'-->
<title>ForeUP <?php echo lang('login_login'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#login_form input:first").focus();
});
</script>
</head>
<body>
	<!--div id="welcome_message" class="top_message">
		<?php echo lang($welcome_message); ?>
	</div-->
	<?php if (validation_errors()) {?>
		<div id="welcome_message" class="top_message_error">
			<?php echo validation_errors(); ?>
		</div>
	<?php } ?>
	<?php 
	$this->load->helper('url');
	$this->load->library('user_agent');
	if ($this->agent->browser() != 'Chrome')
	{	
	?>
	<div id='chrome_message'>ForeUP software is only fully supported on 
		<a href="http://google.com/intl/en/chrome/"><img align='middle' alt="Chrome" data-g-event="nav-logo" data-g-label="consumer-home" id="logo" src="../images/login/chrome.png" style="width:40px"> Chrome</a> 
		Please open or download Google Chrome (free) for the best experience 
		<a href='http://google.com/chrome'>here</a>.
	</div>
	<?php
	}
	?>
<?php echo form_open('login') ?>
<div id="container">
	<div id="top">
		<?php echo img(array('src' => '../images/login/login_logo.png'));?>
	</div>
	<div id='middle'>
		<div class='login_header'>
			Sign In
		</div>
		<div id="form_field_username">	
			<span class='username_icon'></span>
			<?php echo form_input(array(
			'name'=>'username', 
			'value'=>'',
			'placeholder'=>lang('login_username'),
			'size'=>'20')); ?>
		</div>
		<div id="form_field_password">	
			<span class='password_icon'></span>
			<?php echo form_password(array(
			'name'=>'password', 
			'value'=>'',
			'placeholder'=>lang('login_password'),
			'size'=>'20')); ?>
		</div>
		<div id="form_field_submit">	
			<div id="submit_button">
				<?php echo form_submit('login_button',lang('login_continue')); ?>
			</div>
		</div>
	</div>
	<div id="bottom">
		<div id="right">
			<?php echo anchor('login/reset_password', lang('login_forgot_password')); ?> 
		</div>
	</div>
</div>
<?php echo form_close(); ?>
</body>
</html>
<?php
echo form_open('config/save/teesheet',array('id'=>'teesheet_settings_form'));
//print_r($price_classes);
?>
<ul id="error_message_box"></ul>
<fieldset id="sales_settings_info">
<legend><?php echo lang("config_sales_settings"); ?></legend>
    <div class="field_row clearfix">	
        <?php echo form_label(lang('config_automatic_update').':', 'teesheet_updates_automatically',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'teesheet_updates_automatically',
                    'id'=>'teesheet_updates_automatically',
                    'value'=>'1',
                    'checked'=>$this->config->item('teesheet_updates_automatically')));?>
            </div>
    </div>
    <div class="field_row clearfix">	
        <?php echo form_label(lang('config_teetime_confirmations').':', 'teetime_confirmations',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'teetime_confirmations',
                    'id'=>'teetime_confirmations',
                    'value'=>'1',
                    'checked'=>$this->config->item('send_reservation_confirmations')));?>
            </div>
    </div>
    <div class="field_row clearfix">	
        <?php echo form_label(lang('config_teetime_reminders').':', 'teetime_reminders',array('class'=>'wide')); ?>
            <div class='form_field'>
            <?php echo form_checkbox(array(
                    'name'=>'teetime_reminders',
                    'id'=>'teetime_reminders',
                    'value'=>'1',
                    'checked'=>$this->config->item('teetime_reminders')));?>
            </div>
    </div>
    
<?php
echo form_submit(array(
       'name'=>'submit',
       'id'=>'submit',
       'value'=>lang('common_save'),
       'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php 
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
       var submitting = false;
    $('#teesheet_settings_form').validate({
               submitHandler:function(form)
               {
                       if (submitting) return;
                       submitting = true;
                       $(form).mask("<?php echo lang('common_wait'); ?>");
                       $(form).ajaxSubmit({
                       success:function(response)
                       {
                               $.colorbox.close();
                               //post_person_form_submit(response);
                submitting = false;
                       },
                       dataType:'json'
               });

               },
               errorLabelContainer: "#error_message_box",
               wrapper: "li",
              rules: 
               {
               },
               messages: 
               {
       }
       });
});
</script>
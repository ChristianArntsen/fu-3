<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <base href="<?php echo base_url();?>" />
	<title><?php echo $this->session->userdata('course_name').' -- '.lang('common_powered_by').' ForeUP' ?></title>
	<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
  <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/help/layout.css?<?php echo APPLICATION_VERSION; ?>"/>
  <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/help/style.css?<?php echo APPLICATION_VERSION; ?>"/>
  <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/redmond.css?<?php echo APPLICATION_VERSION; ?>"/>
  <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/jquery.wysiwyg.css?<?php echo APPLICATION_VERSION; ?>"/>

  <script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
	<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
  <script src="<?php echo base_url();?>js/jquery.wysiwyg.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
</head>
<body>
  <?= $contents ?>
</body>
</html>
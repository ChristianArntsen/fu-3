<?php
echo form_open('/sales/save_raincheck/',array('id'=>'raincheck_form'));
//echo "teesheet holes: ".$teesheet_holes.'<br/>';
//print_r($customer_info);
//print_r($green_fees);
?>
<style>
	.totals_label {
		text-align:right;
	}
	#green_fee_total, #cart_fee_total, #tax_total, #total_credit_total, #player_total, #subtotal_total {
		text-align:right;
		width:100px;
		padding-right:32px;
	}
	fieldset div.field_row label {
		width:50px;
		float:none;
		height:25px;
	}
	fieldset div.field_row label.dropdown {
		width: 175px;
		padding: 5px;
		float: left;
	}
	fieldset div.field_row label span.ui-button-text{
		line-height:15px;
	}
	.player_buttons .ui-buttonset .ui-button .ui-button-text, .cart_buttons .ui-buttonset .ui-button .ui-button-text {
	    height: 12px;
	    line-height: 0.6em;
	    width: 22px;
	}
	.player_buttons .ui-buttonset .ui-button, .cart_buttons .ui-buttonset .ui-button {
	    height: 20px;
	    /*width:30px;*/
	}
	#raincheck_basic_info {
		padding: 5px;
		border:none;
	}
	fieldset {
		border: 1px solid #DDD;
	}
</style>
<fieldset id="raincheck_basic_info">
<legend>Raincheck Info</legend>
    	<!--div id='teetime_people_row_1'  class="field_row clearfix">
        		Person: <?php echo ($teetime_info->person_name)?$teetime_info->person_name:$teetime_info->title; ?>
        		<?php echo $customer_info[$teetime_info->person_id]->phone_number; ?>
        		<?php echo $customer_info[$teetime_info->person_id]->email; ?>
            	<span>
            		Holes: <?php echo $teetime_info->holes;?>
	            </span>
            </div-->
              	<div class="field_row clearfix">
        		<?php echo form_label('Course:', 'teesheet_id',array('class'=>'wide dropdown')); ?>
				<div class='form_field'>
				<?php echo form_dropdown('teesheet_id', $teesheet_options, $this->session->userdata('teesheet_id')); ?>
				</div>
				</div>
              	<div class="field_row clearfix">
        		<?php echo form_label('Green Fee:', 'green_fee',array('class'=>'wide dropdown')); ?>
				<div class='form_field'>
				<?php echo $green_fee_dropdown;?>
				</div>
				</div>
              	<div class="field_row clearfix">
				<?php echo form_label('Cart Fee:', 'cart_fee',array('class'=>'wide dropdown')); ?>
				<div class='form_field'>
				<?php echo $cart_fee_dropdown;?>
				</div>
				</div>
				<div class="field_row clearfix">
				<?php echo form_label('Customer:', 'customer_name',array('class'=>'wide dropdown')); ?>
				<div class='form_field'>
				<?php echo form_input(array('name' => 'customer_name', 'id' => 'customer_name', 'value' => ''));?>
				<?php echo form_hidden('raincheck_customer_id', '');?>
				</div>
				</div>
			    <div id='players'>
                	<div class="field_row clearfix">
                	<div>Players: </div>
                	</div>
                	<div class="field_row clearfix">
                	<span class='players_buttonset'>
	        			<input type='radio' id='players_1' name='players' value='1' <?php echo ($teetime_info->player_count == '1' || !$teetime_info->player_count)?'checked':''; ?>/>
	        			<label for='players_1' id='players_1_label'>1</label>
	        			<input type='radio' id='players_2' name='players' value='2' <?php echo ($teetime_info->player_count == '2')?'checked':''; ?>/>
	        			<label for='players_2' id='players_2_label'>2</label>
	        			<input type='radio' id='players_3' name='players' value='3' <?php echo ($teetime_info->player_count == '3')?'checked':''; ?>/>
	        			<label for='players_3' id='players_3_label'>3</label>
	        			<input type='radio' id='players_4' name='players' value='4' <?php echo ($teetime_info->player_count == '4')?'checked':''; ?>/>
	        			<label for='players_4' id='players_4_label'>4</label>
	        			<input type='radio' id='players_5' name='players' value='5' <?php echo ($teetime_info->player_count == '5')?'checked':''; ?>/>
	        			<label for='players_5' id='players_5_label'>5</label>
        			</span>
        			</div>
        		</div>
                <div id='holes'>
                	<div class="field_row clearfix">
                	<div>Number of Holes Completed?</div>
                	</div>
                	<div class="field_row clearfix">
	               	<span class='holes_buttonset'>
        				<input type='radio' id='holes_0' name='holes' value='0' checked />
        				<label for='holes_0' id='holes_0_label'>0</label>
        				<input type='radio' id='holes_1' name='holes' value='1' />
        				<label for='holes_1' id='holes_1_label'>1</label>
        				<input type='radio' id='holes_2' name='holes' value='2' />
        				<label for='holes_2' id='holes_2_label'>2</label>
        				<input type='radio' id='holes_3' name='holes' value='3' />
        				<label for='holes_3' id='holes_3_label'>3</label>
        				<input type='radio' id='holes_4' name='holes' value='4' />
        				<label for='holes_4' id='holes_4_label'>4</label>
        				<input type='radio' id='holes_5' name='holes' value='5' />
        				<label for='holes_5' id='holes_5_label'>5</label>
        				<input type='radio' id='holes_6' name='holes' value='6' />
        				<label for='holes_6' id='holes_6_label'>6</label>
        				<input type='radio' id='holes_7' name='holes' value='7' />
        				<label for='holes_7' id='holes_7_label'>7</label>
        				<input type='radio' id='holes_8' name='holes' value='8' />
        				<label for='holes_8' id='holes_8_label'>8</label>
        				<?php //if ($teetime_info->holes == 18) { ?>
        					<br/>
        				<span id='last_9'>
        				<input type='radio' id='holes_9' name='holes' value='9' />
        				<label for='holes_9' id='holes_9_label'>9</label>
        				<input type='radio' id='holes_10' name='holes' value='10' />
        				<label for='holes_10' id='holes_10_label'>10</label>
        				<input type='radio' id='holes_11' name='holes' value='11' />
        				<label for='holes_11' id='holes_11_label'>11</label>
        				<input type='radio' id='holes_12' name='holes' value='12' />
        				<label for='holes_12' id='holes_12_label'>12</label>
        				<input type='radio' id='holes_13' name='holes' value='13' />
        				<label for='holes_13' id='holes_13_label'>13</label>
        				<input type='radio' id='holes_14' name='holes' value='14' />
        				<label for='holes_14' id='holes_14_label'>14</label>
        				<input type='radio' id='holes_15' name='holes' value='15' />
        				<label for='holes_15' id='holes_15_label'>15</label>
        				<input type='radio' id='holes_16' name='holes' value='16' />
        				<label for='holes_16' id='holes_16_label'>16</label>
        				<input type='radio' id='holes_17' name='holes' value='17' />
        				<label for='holes_17' id='holes_17_label'>17</label>
        				</span>
        				<?php //} ?>
        			</span>
        			</div>
        		</div>
        <table><tbody>
        <tr>
        	<td class='totals_label' colspan="2">Green Fee:</td>
        	<td id='green_fee_total'>$0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Cart Fee:</td>
        	<td id='cart_fee_total'>$0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Players:</td>
        	<td id='player_total'>x 1</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Subtotal:</td>
        	<td id='subtotal_total'>$0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Tax:</td>
        	<td id='tax_total'>$0.00</td>
        </tr>
        <tr>
        	<td class='totals_label' colspan="2">Total Credit:</td>
        	<td id='total_credit_total'>$0.00</td>
        </tr>
        <tr>
        	<td colspan=3>
        		<?php
        			echo form_hidden(array('green_fee'=>0,'cart_fee'=>0,'tax'=>0,'total_credit'=>0));
				?>
        		<?php
				echo form_submit(array(
					'name'=>'submitButton',
				 	'id'=>'submitButton',
					'value'=> 'Issue Raincheck',
					'class'=>'submit_button float_right')
				);
				?>
            </td>
        </tr>
    </tbody>
</table>
</fieldset>
<?php form_close(); ?>
<script>
	var raincheck = {
		course_id:'<?= $this->session->userdata('course_id')?>',
		fees:{},//$.parseJSON('<?= json_encode($fees)?>'),
		taxes:{},//$.parseJSON('<?= json_encode($taxes)?>'),
		get_raincheck_info: function () {
			var teesheet_id = $('#teesheet_id').val();
			$.ajax({
               type: "POST",
               url: "index.php/sales/get_raincheck_info/"+teesheet_id,
               data: "",
               success: function(response){
               		console.dir(response);
               		$('#green_fee_dropdown').replaceWith($(response.green_fee_dropdown));
               		$('#cart_fee_dropdown').replaceWith($(response.cart_fee_dropdown));
               		raincheck.initialize_dropdowns();
               		raincheck.fees = response.fees;
               		raincheck.update_totals();
               },
               dataType:'json'
            });
		},
		initialize_dropdowns:function (){
			$('#green_fee_dropdown, #cart_fee_dropdown').change(function(){
				var gfv = $('#green_fee_dropdown').val();
				var holes_completed = $('input[name=holes]:checked').val();
				var parts = gfv.split('_');
				console.dir(parts);
				console.log(parts[1]+' - hc - '+holes_completed);
				if (parts[1] < 5)
				{
					if (holes_completed > 8)
					{
						$('#holes_0_label').click();
						$('#holes_0').click();
					}
					$('#last_9').hide();
				}
				else
					$('#last_9').show();
				raincheck.update_totals();
			});
		},
		update_totals: function() {
			var teesheet_id = $('#teesheet_id').val();
	//		if (holes[teesheet_id] != 18)
				//Hide half of holes completed and select 0
//			else
				//Show both halves of holes completed
			var players = $('input[name=players]:checked').val();
			var holes_completed = $('input[name=holes]:checked').val();
			var gf_index = $('#green_fee_dropdown').val();
			var c_index = $('#cart_fee_dropdown').val();
			var gf_price = c_price = taxes = total = 0;
			if (gf_index == '' && c_index == '')
			{
			
			}
			else{
				if (gf_index != '')
				{
					var gf_indexes = gf_index.split('_');
					var gf_price_cat = gf_indexes[0];
					var gf_item = gf_indexes[1];
					var gf_holes = (gf_item>=5)?18:9;
					console.log(teesheet_id+' '+this.course_id+'_'+gf_item+' price_category_'+gf_price_cat);
					console.log(this.fees[teesheet_id][this.course_id+'_'+gf_item]['price_category_'+gf_price_cat]);
					console.log(gf_holes+' '+holes_completed+' '+(gf_holes - holes_completed)/gf_holes);
					gf_price = this.fees[teesheet_id][this.course_id+'_'+gf_item]['price_category_'+gf_price_cat] * (gf_holes - holes_completed)/gf_holes;
					
				}
				if (c_index != '')
				{
					var c_indexes = c_index.split('_');
					var c_price_cat = c_indexes[0];
					var c_item = c_indexes[1];
					var c_holes = (c_item>=5)?18:9;
					c_price = this.fees[teesheet_id][this.course_id+'_'+c_item]['price_category_'+c_price_cat] * (c_holes - holes_completed)/c_holes;
				}
				console.dir(this.taxes.gf[0]);
				taxes = (this.taxes.gf[0]?((gf_price *players) * this.taxes.gf[0].percent / 100):0) + (this.taxes.c[0]?((c_price * players) * this.taxes.c[0].percent / 100):0);
				total = parseFloat(parseFloat((gf_price + c_price) * players).toFixed(2)) + parseFloat(parseFloat(taxes).toFixed(2));
				
			}
			
			//Update inputs
			$('#green_fee').val(gf_price.toFixed(2));
			$('#cart_fee').val(c_price.toFixed(2));
			$('#tax').val(taxes.toFixed(2));
			$('#total_credit').val(total.toFixed(2));
			//Update visible
			$('#green_fee_total').html('$'+gf_price.toFixed(2));
			$('#cart_fee_total').html('$'+c_price.toFixed(2));
			$('#subtotal_total').html('$'+((gf_price+c_price) * players).toFixed(2));
			$('#tax_total').html('$'+taxes.toFixed(2));
			$('#player_total').html('x '+players);
			$('#total_credit_total').html('$'+total.toFixed(2));
		}
	};
	$(document).ready(function(){
		raincheck.fees = <?= json_encode($fees)?>;
		raincheck.taxes = <?= json_encode($taxes)?>;
		
		$( "#customer_name" ).autocomplete({
			source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				$("#customer_name").val(ui.item.label);
				$('#raincheck_customer_id').val(ui.item.value);
			}
		});
		console.log('running players_buttonset');
		$('.players_buttonset').buttonset();
		$('.holes_buttonset').buttonset();
		$('#teesheet_id').change(function(){
			raincheck.get_raincheck_info();
		});
		raincheck.initialize_dropdowns();
		$('input[name=players], input[name=holes]').change(function(){
			raincheck.update_totals();
		});
		console.dir(raincheck.fees);
		//raincheck.update_totals();
    	var submitting = false;
    	$('#raincheck_form').validate({
			submitHandler:function(form)
			{
	      		if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
			        success:function(response)
			        {
			        	var data = response.data;
			                
			        	//Print raincheck
		                if ('<?php echo $this->config->item('webprnt')?>' == '1')
		                {
			                var builder = new StarWebPrintBuilder();
							var receipt_data = builder.createTextElement({data:'\nRaincheck Credit'});
			                // Items
							
			                // Totals
			                subtotal = (parseFloat(data.green_fee) + parseFloat(data.cart_fee))*parseInt(data.players);
			                if (data.customer != '')
				                receipt_data += builder.createTextElement({data:'\nCustomer: '+data.customer+'\n\n'});
			                receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Green Fee: ','$'+parseFloat(data.green_fee).toFixed(2))+'\n'});
			                receipt_data += builder.createTextElement({data:add_white_space('Cart Fee: ','$'+parseFloat(data.cart_fee).toFixed(2))+'\n'});
			                receipt_data += builder.createTextElement({data:'\n'+add_white_space('Players: ','x '+(data.players))+'\n'});
			                receipt_data += builder.createTextElement({data:add_white_space('Subtotal: ','$'+parseFloat(subtotal).toFixed(2))+'\n'});
			                receipt_data += builder.createTextElement({data:'\n'+add_white_space('Tax: ','$'+parseFloat(data.tax*data.players).toFixed(2))+'\n'});
			                receipt_data += builder.createTextElement({data:'\n'+add_white_space('Total Credit: ','$'+parseFloat(data.total).toFixed(2))+'\n'});
			                
			                // Sale id and barcode
			                //receipt_data += chr(27)+chr(97)+chr(49);
			                //receipt_data += "\x1Dh" +chr(80);  // ← Set height
					        receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
					        var sale_num = String(data.raincheck_number);
					        
					        // Sale id and barcode
			                receipt_data = webprnt.add_barcode(receipt_data, 'Raincheck ID', 'RID '+data.raincheck_number);

						    var header_data = {
						    	course_name:'<?php echo $this->config->item('name')?>',
						    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
						    	address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
						    	phone:'<?php echo $this->config->item('phone')?>',
						    	employee_name:'<?php echo $user_info->last_name.', '.$user_info->first_name; ?>',
						    	customer:''// Need to load this in from data
						    };
						    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
    						console.log('about to print raincheck');
			                //print_receipt(receipt_data);
			                receipt_data = webprnt.add_paper_cut(receipt_data);
						    
						    webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
			                //print_webprnt_receipt(receipt_data, false, data.raincheck_number, response.receipt_data);
			                
		                } 
		                else if ('<?php echo $this->config->item('print_after_sale')?>' == '1')
		                {
				        	var receipt_data = '\nRaincheck Credit';
			                // Items
							
			                // Totals
			                subtotal = (parseFloat(data.green_fee) + parseFloat(data.cart_fee))*parseInt(data.players);
			                if (data.customer != '')
				                receipt_data += '\nCustomer: '+data.customer+'\n\n';
			                receipt_data += '\n\n'+add_white_space('Green Fee: ','$'+parseFloat(data.green_fee).toFixed(2))+'\n';
			                receipt_data += add_white_space('Cart Fee: ','$'+parseFloat(data.cart_fee).toFixed(2))+'\n';
			                receipt_data += '\n'+add_white_space('Players: ','x '+(data.players))+'\n';
			                receipt_data += add_white_space('Subtotal: ','$'+parseFloat(subtotal).toFixed(2))+'\n';
			                receipt_data += '\n'+add_white_space('Tax: ','$'+parseFloat(data.tax*data.players).toFixed(2))+'\n';
			                receipt_data += '\n'+add_white_space('Total Credit: ','$'+parseFloat(data.total).toFixed(2))+'\n';
			                
			                // Sale id and barcode
			                receipt_data += chr(27)+chr(97)+chr(49);
			                receipt_data += "\x1Dh" +chr(80);  // ← Set height
					        receipt_data += "\n\n";// Some spacing at the bottom
					        var sale_num = String(data.raincheck_number);
					        var len = sale_num.length;
							if (len == 2)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x09\x7B\x41\x52\x49\x44\x20"+sale_num;
							else if (len == 3)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x52\x49\x44\x20"+sale_num;
							else if (len == 4)
						        receipt_data += "\x1D\x77\x01\x1D\x6B\x49\x0B\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 5)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 6)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 7)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 8)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 9)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x52\x49\x44\x20"+sale_num;
					        else if (len == 10)
						        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x52\x49\x44\x20"+sale_num;
						    console.log('adding barcode with '+sale_num+' stuff '+len);
			                // Sale id and barcode
			                receipt_data += '\n\nRaincheck ID: RID '+data.raincheck_number+'\n';
			                console.log('about to print raincheck');
			                print_receipt(receipt_data);
			            }
			            else
			            {
				            window.location = 'index.php/sales/raincheck/'+data.raincheck_number;
				        }
			            
			            $.colorbox.close();
			            //post_campaign_form_submit(response);
			            submitting = false;
			        },
			        dataType:'json'
	  	        });
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
	
		    },
			messages:
			{
				
			}
		});
	});
</script>
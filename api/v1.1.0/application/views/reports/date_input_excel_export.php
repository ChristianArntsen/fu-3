<?php $this->load->view("partial/header"); ?>
<div id="page_title" style="margin-bottom:8px;"><?php echo lang('reports_report_input'); ?></div>
<?php
if(isset($error))
{
	echo "<div class='error_message'>".$error."</div>";
}
?>
	<?php if ($type == 'sales' && $this->uri->segment(2) == 'detailed_sales') { ?>
	<?php echo form_label(lang('reports_sale_id'), 'report_sale_id', array('class'=>'required')); ?>
	<div id='report_sale_id'>
		<?php echo form_input(array(
	            'name'=>'sale_id', 
	            'id'=>'sale_id', 
	            'value'=>'',
	            'size'=>'20')); ?>
	</div>
	<br/>
	<p>- OR -</p>
	<br/>
	<?php } ?>
	<?php echo form_label(lang('reports_date_range'), 'report_date_range_label', array('class'=>'required')); ?>
	<div id='report_date_range_simple'>
		<input type="radio" name="report_type" id="simple_radio" value='simple' checked='checked'/>
		<?php echo form_dropdown('report_date_range_simple',$report_date_range_simple, '', 'id="report_date_range_simple"'); ?>
	</div>
	
	<div id='report_date_range_complex'>
		<input type="radio" name="report_type" id="complex_radio" value='complex' />
		<?php echo form_dropdown('start_month',$months, $selected_month, 'id="start_month"'); ?>
		<?php echo form_dropdown('start_day',$days, $selected_day, 'id="start_day"'); ?>
		<?php echo form_dropdown('start_year',$years, $selected_year, 'id="start_year"'); ?>
		-
		<?php echo form_dropdown('end_month',$months, $selected_month, 'id="end_month"'); ?>
		<?php echo form_dropdown('end_day',$days, $selected_day, 'id="end_day"'); ?>
		<?php echo form_dropdown('end_year',$years, $selected_year, 'id="end_year"'); ?>
	</div>
	<?php 
	if ($type == 'sales' || $type == 'payments' || $type == 'categories') {
	echo form_label(lang('reports_department'), 'reports_department_label', array('class'=>'required')); ?>
	<div id='report_sale_type'>
		<?php echo form_dropdown('sale_department', $departments, 'all', 'id="sale_department"'); ?>
	</div>
	<?php
	}
	else if ($type == 'items') {
	echo form_label(lang('reports_filter_by'), 'reports_department_label', array('class'=>'required')); ?>
	<div id='report_department_filter'>
		<input type="radio" name="report_filter" id="department_radio" value='department' checked/>
		<?php echo lang('reports_department').' '.form_dropdown('department', $departments, 'all', 'id="department"'); ?>
	</div>
	<div id='report_category_filter'>
		<input type="radio" name="report_filter" id="category_radio" value='category' />
		<?php echo lang('reports_category').' '.form_dropdown('category', $categories, 'all', 'id="category"'); ?>
	</div>
	<div id='report_subcategoroy_filter'>
		<input type="radio" name="report_filter" id="subcategory_radio" value='subcategory' />
		<?php echo lang('reports_subcategory').' '.form_dropdown('subcategory', $subcategories, 'all', 'id="subcategory"'); ?>
	</div>
	<script>
		$('#department, #category, #subcategory').change(function(e){
			var id = $(e.target).attr('id');
			$('#'+id+'_radio').click();
		})
	</script>
	<?php
	}
	echo form_label(lang('reports_sale_type'), 'reports_sale_type_label', array('class'=>'required')); ?>
	<div id='report_sale_type'>
		<?php echo form_dropdown('sale_type',array('all' => lang('reports_all'), 'sales' => lang('reports_sales'), 'returns' => lang('reports_returns')), 'all', 'id="sale_type"'); ?>
	</div>
	
	<div>
		<?php echo lang('reports_export'); ?>: 
		<input type="radio" name="export_excel" id="export_excel_no" value='0' checked='checked' /> <?php echo lang('common_no'); ?>
		<input type="radio" name="export_excel" id="export_excel_yes" value='1' /> <?php echo lang('common_excel_export'); ?>
		<?php if(false && $type == 'sales'){ ?>
		<input type="radio" name="export_excel" id="quickbook_connect" value='3' /> <?php echo "Quickbooks Connect"; 
		}?>
		<?php if ($type != 'receivings' && $type != 'taxes' && $type != 'sales') { ?>
		<input type="radio" name="export_excel" id="export_pdf_yes" value='2' /> <?php echo lang('common_pdf_export'); 
		}
		?>
	</div>

<?php
echo form_button(array(
	'name'=>'generate_report',
	'id'=>'generate_report',
	'content'=>lang('common_submit'),
	'class'=>'submit_button')
);
?>

<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
$(document).ready(function()
{
	<?php if ($type == 'items') { ?>
	$("#generate_report").click(function()
	{
		var sale_type = $("#sale_type").val();
		var department = $('#sale_department').val();
		var export_excel = $('input[name=export_excel]:checked').val();
		var filter = $('input[name=report_filter]:checked').attr('id').replace('_radio', '');
		var value = $('#'+filter).val();
		
		if ($("#simple_radio").attr('checked'))
		{
			window.location = window.location+'/'+$("#report_date_range_simple option:selected").val() + '/'+sale_type+'/'+export_excel+'/'+encodeURIComponent(filter)+'/'+encodeURIComponent(encodeURIComponent(value));
		}
		else
		{
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();
	
			window.location = window.location+'/'+start_date + '/'+ end_date + '/'+sale_type+'/'+ export_excel+'/'+encodeURIComponent(filter)+'/'+encodeURIComponent(encodeURIComponent(value));
		}
	});
	<?php } else { ?>
	$("#generate_report").click(function()
	{
		var sale_type = $("#sale_type").val();
		var department = $('#sale_department').val();
		var export_excel = $('input[name=export_excel]:checked').val();
		var sale_id = $('#sale_id').val();
		if (sale_id != '' && sale_id != undefined)
		{
			window.location = window.location+'/0000-00-00/3000-01-01/sales/0/all/'+encodeURIComponent(sale_id);
		}
		else if ($("#simple_radio").attr('checked'))
		{
			window.location = window.location+'/'+$("#report_date_range_simple option:selected").val() + '/'+sale_type+'/'+export_excel+'/'+encodeURIComponent(department);
		}
		else
		{
			var start_date = $("#start_year").val()+'-'+$("#start_month").val()+'-'+$('#start_day').val();
			var end_date = $("#end_year").val()+'-'+$("#end_month").val()+'-'+$('#end_day').val();
	
			window.location = window.location+'/'+start_date + '/'+ end_date + '/'+sale_type+'/'+ export_excel+'/'+encodeURIComponent(department);
		}
	});
	<?php } ?>
	$("#start_month, #start_day, #start_year, #end_month, #end_day, #end_year").change(function()
	{
		$("#complex_radio").attr('checked', 'checked');
	});
	
	$("#report_date_range_simple").change(function()
	{
		$("#simple_radio").attr('checked', 'checked');
	});
	
});
</script>
<?php
if($export_excel == 1)
{
	//$excelXml = new Excel_XML();
	//$excelXml->setWorksheetTitle($title);
	$rows = array();
	#row = array();
	
	foreach($data['dep'] as $datakey => $datarow)
	{
		$row= array();
		foreach ($headers['department'] as $header) 
		{
			$row[] = strip_tags($header['data']);
		}
		$rows[] = $row;
		
		$row = array();
		foreach($datarow as $cell_index => $cell)
		{
			if ($cell_index != 0)
				$row[] = "";
			else
				$row[] = 'Department - '.strip_tags($cell['data']);
		}
		$rows[] = $row;
		foreach($data['cat'][$datakey] as $cat_key => $cat_row) {
			$cat_data = array();
			foreach($cat_row as $cat_cell_index => $cat_cell)
			{
				if ($cat_cell_index == 0)
					$cat_data[] = 'Category - '.strip_tags($cat_cell['data']);
				else
					$cat_data[] = strip_tags($cat_cell['data']);
			}
			$rows[] = $cat_data;
			
			foreach($data['subcat'][$datakey][$cat_key] as $subcat_key => $subcat_row) {
				if ($subcat_row[0]['data'] != '') 
				{
					$subcat_data = array();
					foreach($subcat_row as $subcat_cell_index => $subcat_cell)
					{
						if ($subcat_cell_index == 0)
							$subcat_data[] = 'Subcategory - '.strip_tags($subcat_cell['data']);
						else 
							$subcat_data[] = strip_tags($subcat_cell['data']);
					}
					$rows[] = $subcat_data;
				}
				foreach($data['items'][$datakey][$cat_key][$subcat_key] as $item_key => $item_row) {
					$item_data = array();
					foreach($item_row as $item_cell_index => $item_cell)
					{
						if ($item_cell_index == 0)
							$item_data[] = ' - '.strip_tags($item_cell['data']);
						else
							$item_data[] = strip_tags($item_cell['data']);
					}
					$rows[] = $item_data;	
				}
			}
		}
		$row = array();
		foreach($datarow as $cell_index => $cell)
		{
			if ($cell_index == 0)
				$row[] = "Totals:";
			else
				$row[] = strip_tags($cell['data']);
		}
		$rows[] = $row;
	}
	//$excelXml->addArray($rows);
	//$excelXml->generateXML($title);
	$content = array_to_csv($rows);
	
	force_download(strip_tags($title) . '.csv', $content);
	exit;
}
else if($export_excel == 2)
{
	// EXPORTS TO PDF
	//$this->load->library('cezpdf');
	$this->load->library('fpdf');
	$pdf = new PDF('P', 'pt', 'A4');
	$pdf->SetDates($dates['start_date'], $dates['end_date']);
	//$this->load->helper('pdf');
	//$CI = & get_instance();
	//$CI->cezpdf->Cezpdf('a4','landscape');
    //$CI->cezpdf->ezSetMargins(30,65,30,30);
    //prep_pdf('landscape'); // creates the footer for the document we are creating.
	
	$col_array_keys = array(
		'item_number',
		'description',
		'blank',
		'quantity_sold',
		'subtotal',
		'tax',
		'total',
		'cost',
		'profit'
	);
	
	$rows = array();
	foreach($data['dep'] as $datakey => $datarow)
	{
		$row = array();
		foreach($datarow as $cell_index => $cell)
		{
			if ($cell_index < 2)
				$row[$col_array_keys[$cell_index]] = array('v'=>$cell['data']);
			else
				$row[$col_array_keys[$cell_index]] = array('v'=>"");
			if ($cell_index == 1)
				$row[$col_array_keys[$cell_index]]['w'] = 55;
		}
		$rows[] = $row;
		foreach($data['cat'][$datakey] as $cat_key => $cat_row) 
		{
			$cat_data = array();
			foreach($cat_row as $cat_cell_index => $cat_cell)
			{
				if ($cat_cell_index < 2)
					$cat_data[$col_array_keys[$cat_cell_index]] = array('v'=>$cat_cell['data'], 'p'=>0);
				else
					$cat_data[$col_array_keys[$cat_cell_index]] = array('v'=>'');//strip_tags($cat_cell['data']);
				if ($cat_cell_index == 1)
					$cat_data[$col_array_keys[$cat_cell_index]]['w'] = 55;
			}
			$rows[] = $cat_data;
			
			foreach($data['subcat'][$datakey][$cat_key] as $subcat_key => $subcat_row) {
				if ($subcat_row[0]['data'] != lang('reports_subcategory').': ') 
				{
					$subcat_data = array();
					foreach($subcat_row as $subcat_cell_index => $subcat_cell)
					{
						if ($subcat_cell_index < 2)
							$subcat_data[$col_array_keys[$subcat_cell_index]] = array('v'=>$subcat_cell['data'],'p'=>10);
						else 
							$subcat_data[$col_array_keys[$subcat_cell_index]] = array('v'=>'');//strip_tags($subcat_cell['data']);
						if ($subcat_cell_index == 1)
							$subcat_data[$col_array_keys[$subcat_cell_index]]['w'] = 55;
					}
					$rows[] = $subcat_data;
				}
				foreach($data['items'][$datakey][$cat_key][$subcat_key] as $item_key => $item_row) {
					$item_data = array();
					foreach($item_row as $item_cell_index => $item_cell)
					{
						$item_data[$col_array_keys[$item_cell_index]] = array('v'=>strip_tags($item_cell['data']));
						if ($item_cell_index < 2)
							$item_data[$col_array_keys[$item_cell_index]]['p'] = 10;
						if ($item_cell_index == 1)
							$item_data[$col_array_keys[$item_cell_index]]['w'] = 55;
						else if ($item_cell_index >= 2) { 
							$item_data[$col_array_keys[$item_cell_index]]['a'] = 'R';
							$item_data[$col_array_keys[$item_cell_index]]['w'] = 55;
						}
					}
					$rows[] = $item_data;	
				}
				if ($subcat_row[0]['data'] != lang('reports_subcategory').': ') 
				{
					$subcat_data = array();
					foreach($subcat_row as $subcat_cell_index => $subcat_cell)
					{
						if ($subcat_cell_index < 2)
							$subcat_data[$col_array_keys[$subcat_cell_index]] = array('v'=>'');//$subcat_cell['data'];
						else 
							$subcat_data[$col_array_keys[$subcat_cell_index]] = array('v'=>strip_tags($subcat_cell['data']), 'a'=>'R','b'=>'T','w'=>55);
						if ($subcat_cell_index == 1)
							$subcat_data[$col_array_keys[$subcat_cell_index]]['w'] = 55;
					}
					$rows[] = $subcat_data;
				}
			}
			$cat_data = array();
			foreach($cat_row as $cat_cell_index => $cat_cell)
			{
				if ($cat_cell_index < 2)
					$cat_data[$col_array_keys[$cat_cell_index]] = array('v'=>'');//'<b>Category:</b> '.strip_tags($cat_cell['data']);
				else
					$cat_data[$col_array_keys[$cat_cell_index]] = array('v'=>$cat_cell['data'],'a'=>'R','b'=>'T','w'=>55);
				if ($cat_cell_index == 1)
					$cat_data[$col_array_keys[$cat_cell_index]]['w'] = 55;
			}
			$rows[] = $cat_data;
		}
		$row = array();
		foreach($datarow as $cell_index => $cell)
		{
			if ($cell_index < 2)
				$row[$col_array_keys[$cell_index]] = array('v'=>'');//$cell['data'];
			else {
				$row[$col_array_keys[$cell_index]] = array('v'=>$cell['data'],'a'=>'R','b'=>'T','w'=>55);
			}
			if ($cell_index == 1)
				$row[$col_array_keys[$cell_index]]['w'] = 55;
		}
		$rows[] = $row;
	}
	foreach($summary_data as $name=>$value) {
		$rows[] = array(
			'item_number'=>array('w'=>55,'v'=>''),
			'description'=>array('v'=>''),
			'blank'=>array('w'=>55),
			'quantity_sold'=>array('w'=>55),
			'subtotal'=>array('w'=>55),
			'tax'=>array('w'=>55),
			'total'=>array('w'=>55),
			'cost'=>array('w'=>55,'a'=>'R','v'=>lang('reports_'.$name).":",'border'=>1),
			'profit'=>array('w'=>55,'a'=>'R','v'=>to_currency($value),'border'=>1)
		);
	}
	
	$col_names = array(
		array('v' => lang('reports_item_number'), 'b'=>'B'),
		array('v' => lang('reports_description'),'w'=>55, 'b'=>'B'),
		array('v' => '', 'w'=>55, 'b'=>'B'),
		array('v' => lang('reports_quantity'), 'a'=>'R','w'=>55, 'b'=>'B'),
		array('v' => lang('reports_subtotal'), 'a'=>'R','w'=>55, 'b'=>'B'),
		array('v' => lang('reports_tax'), 'a'=>'R','w'=>55, 'b'=>'B'),
		array('v' => lang('reports_total'), 'a'=>'R','w'=>55, 'b'=>'B'),
		array('v' => lang('reports_unit_price'), 'a'=>'R','w'=>55, 'b'=>'B')//,
//		array('v' => lang('reports_profit'), 'a'=>'R','w'=>55, 'b'=>'B')
	);
	$pdf->SetHeaderLabels($col_names);
	$pdf->SetFontStyle('normal');
		$pdf->AddPage();
		$pdf->SetMargins(30,30,30);
		$pdf->SetAutoPageBreak('on', 65);
		//$col_array_keys = array('one', 'two');
		//$rows = array(array('three','four'),array('five','six'));
		$pdf->ImprovedTable($col_names, $rows);
		$pdf->Output('Detailed Departments Report.pdf', 'I');
	//$CI->cezpdf->ezStream(array('Content-Disposition'=>'DetailedCategoriesReport.pdf'));
	exit;
}
?>
<?php //$this->load->view("partial/header"); ?>
<!--table id="title_bar">
	<tr>
		<td id="title_icon">
			<img src='<?php echo base_url()?>images/menubar/reports.png' alt='<?php echo lang('reports_reports'); ?> - <?php echo lang('reports_welcome_message'); ?>' />
		</td>
		<td id="title"><?php echo lang('reports_reports'); ?> - <?php echo $title ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><small><?php echo $subtitle ?></small></td>
	</tr>
</table>
<br /-->
<?php 
if ($export_excel == 3)
{
?>
<style>
	
</style>
<?php	
	$columns = count($headers);
	$cell_style = 'width="'.(765/6).'" height="40"';
}
else 
{
//$this->load->view("partial/header");
?>
<style>
	#table_holder {
		width:100%;
	}
</style>
<?php	
}
?>
<table id="report_contents">
	<tr>
		<td id="item_table">
			<!--div class='fixed_top_table'>
			<div class='header-background'></div-->
			<div id="table_holder" style="width: 100%;">
				<table class="tablesorter report" id="sortable_table">
						<?php if ($export_excel == 3) {?>
					<thead>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center;font-size:20px;font-weight:bold;'>
									<?=$title?>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan=<?=$columns?> align='center' width='765'>
								<div style='text-align:center; font-size:14px;'>
									<?=$subtitle?>
								</div>
							</th>
						</tr>
					</thead>
						<?php } ?>
						<!--tr>
							<?php foreach ($headers['department'] as $header) { ?>
							<th align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
							<?php } ?>
						</tr-->
						<?php foreach ($data['dep'] as $key=>$row) { ?>
					<thead>
						<tr>
						<?php foreach ($headers['department'] as $header) { ?>
						<th <?=$cell_style?> align="<?php echo $header['align'];?>"><div class="header_cell header"><?php echo $header['data']; ?><?php if ($export_excel!=3) {?><span class="sortArrow">&nbsp;</span><?php } ?></div></th>
						<?php } ?>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td <?=$cell_style?> align="<?php echo $row[0]['align'];?>"><b><?php echo $row[0]['data']; ?></b></td>
							<td <?=$cell_style?>></td>
							<td <?=$cell_style?>></td>
							<td <?=$cell_style?>></td>
							<td <?=$cell_style?>></td>
							<td <?=$cell_style?>></td>
						</tr>
						<?php if (count($data['cat'][$key]) < 0) { ?>
							<tr>
								<?php foreach ($headers['category'] as $header) { ?>
								<th <?=$cell_style?> align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
								<?php } ?>
							</tr>	
						<?php } ?>
						<?php
						$row_class = 'even'; 
						foreach($data['cat'][$key] as $ckey=>$crow) {	
							$row_class = ($row_class== 'even')?'odd':'even';
							?>
							<tr class='<? echo $row_class ?> category_row'>
								<?php foreach ($crow as $ccell) {?>
								<td <?=$cell_style?> style='padding-left:30px;' align="<?php echo $ccell['align'];?>"><?php echo $ccell['data']; ?></td>
								<?php } ?>
							</tr>
							<?php if (count($data['subcat'][$key][$ckey]) < 0) { ?>
								<tr class='<? echo $row_class ?> subcategory_row'>
									<?php foreach ($headers['subcategory'] as $header) { ?>
									<th <?=$cell_style?> align="<?php echo $header['align'];?>"><?php echo $header['data']; ?></th>
									<?php } ?>
								</tr>	
							<?php } ?>
							<?php foreach($data['subcat'][$key][$ckey] as $skey=>$srow) {	?>
								<?php if ($srow[0]['data'] != '') { ?>
								<tr class='<? echo $row_class ?> subcategory_row'>
									<?php foreach ($srow as $scell) {?>
									<td <?=$cell_style?> style='padding-left:60px;' align="<?php echo $scell['align'];?>"><?php echo $scell['data']; ?></td>
									<?php } ?>
								</tr>
								<?php } ?>
								<?php foreach($data['items'][$key][$ckey][$skey] as $ikey=>$irow) {	?>
									<tr class='<? echo $row_class ?>'>
										<?php foreach ($irow as $icell) {?>
										<td <?=$cell_style?> style='padding-left:30px;' align="<?php echo $icell['align'];?>"><?php echo $icell['data']; ?></td>
										<?php } ?>
									</tr>
									<?php } ?>
								<?php } ?>
							<?php } ?>
							<tr style='border-top:solid #999 2px' class='<? echo $row_class ?>'>
								<?php foreach ($row as $cell_index => $cell) { if ($cell_index == 0) { ?>
								<td <?=$cell_style?> align="right"><b>Totals:</b></td>
								<?php } else {?>
								<td <?=$cell_style?> align="<?php echo $cell['align'];?>"><?php echo $cell['data']; ?></td>
								<?php }} ?>
							</tr>
					</tbody>
						<?php } ?>
				</table>
			</div>	
			<!--/div-->
			<div id="report_summary" class="tablesorter report">
			<?php 
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				foreach($summary_data as $name=>$value) { 
					if ($name == 'account_charges')
						$label = $cdn;
					else if ($name == 'member_balance')
						$label = $mbn;
					else 
						$label = lang('reports_'.$name);
				?>
				<?php if ($export_excel == 3)
				{ ?>
					<table><tr><td width='475'>-</td><td align='' width='200'><?php echo "<strong>".$label. '</strong>: '.to_currency($value); ?></td></tr></table>
				<?php }
				else
				{?>
					<div class="summary_row"><?php echo "<strong>".$label. '</strong>: '.to_currency($value); ?></div>
				<?php } ?>
			<?php }?>
			</div>
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>
<?php if ($export_excel != 3) { //$this->load->view("partial/footer"); ?>

<script type="text/javascript" language="javascript">
function init_table_sorting()
{
	//Only init if there is more than one row
	if($('.tablesorter tbody tr').length >1)
	{
		$("#sortable_table").tablesorter(); 
	}
}
$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	init_table_sorting();
});
</script>
<?php } ?>
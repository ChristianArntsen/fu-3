<?php
require_once ("secure_area.php");
class Teesheets extends Secure_area
{
	function __construct()
	{
		parent::__construct('teesheets');
		$this->load->model('teesheet');
        $this->load->model('teetime');
        $this->load->model('standby');
        $this->load->model('customer');
        $this->load->model('sale');
		$this->load->model('course');
        $this->load->library('sale_lib');
        $this->load->helper('url');
	}
	function test_weather() {
		$this->load->model('weather');
		$this->weather->zip_code = '84663';
		$results = $this->weather->getLatest();
		print_r($results);
	}
	function index()
	{
		$this->load->model('note');
		$dataArray = array();
		$tsMenu = '';
        if ($this->input->post('teesheetMenu'))
            $this->teesheet->switch_tee_sheet();
		$current_teesheet = $this->session->userdata('teesheet_id');
		//build a menu with the tee sheet id's
		$tsMenu = $this->teesheet->get_tee_sheet_menu($current_teesheet);
		//echo (date('Ymd', strtotime('-1 week'))-100).'0000';
        $JSONData = $this->teesheet->getJSONTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000', (date('Ymd', strtotime('+3 week'))-100).'0000');
		$dataArray = array(
            'tsMenu'=>$tsMenu,
            'current_teesheet'=>$current_teesheet,
            'JSONData'=>$JSONData,
            'openhour'=> $this->session->userdata('openhour'),
            'closehour'=> $this->session->userdata('closehour'),
            'increment'=> $this->session->userdata('increment'),
            'holes'=> $this->session->userdata('holes'),
            'fntime'=> $this->session->userdata('frontnine'),
            //'notes'=>$this->note->get_all()->result_array(),
            //'fdweather'=> $this->teesheet->make_5_day_weather(),
            'user_level'=> $this->session->userdata('user_level'),
            'associated_courses'=>$this->session->userdata('associated_courses'),
            'user_id'=> $this->session->userdata('user_id'),
            'purchase'=>$this->session->userdata('sales'),
			'controller_name'=>strtolower(get_class())
        );
        $this->load->view("teetimes/manage", $dataArray);
	}
	function facebook_page_select()
	{
		return $this->load->view('customers/facebook_page_select');
	}
	function clear_facebook_settings()
	{
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = '';
		$course_info->facebook_page_name = '';
		$course_info->facebook_extended_access_token = '';
		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', '');
			$this->session->set_userdata('facebook_page_name', '');
			$this->session->set_userdata('facebook_extended_access_token', '');
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));
	}

	function update_facebook_page_id()
	{
		$page_id = $this->input->post('page_id');
		$page_name = $this->input->post('page_name');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_page_id = $page_id;
		$course_info->facebook_page_name = $page_name;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_page_id', $page_id);
			$this->session->set_userdata('facebook_page_name', $page_name);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result));

	}

	function update_facebook_access_token()
	{
		$access_token = $this->input->post('extended_access_token');
		$course_info = $this->course->get_info($this->session->userdata('course_id'));
		$course_info->facebook_extended_access_token = $access_token;

		if ($this->course->save($course_info, $course_info->course_id))
		{
			$this->session->set_userdata('facebook_extended_access_token', $access_token);
			$result = true;
		}
		else
		{
			$result = false;
		}
		echo json_encode(array('success'=>$result,'new token'=>$access_token));

	}

	function make_5_day_weather()
	{
		echo json_encode($this->teesheet->make_5_day_weather());
	}
	function zero_teed_off($teetime_id)
	{
		echo json_encode(array('teetimes'=> $this->teetime->zero_teed_off($teetime_id)));
	}
	function zero_turn($teetime_id)
	{
		echo json_encode(array('teetimes'=>$this->teetime->zero_turn($teetime_id)));
	}
	function check_in($teetime_id, $count = 0)
	{
		echo json_encode(array('teetimes'=>$this->teetime->check_in($teetime_id, $count)));
	}
    function get_stats()
    {
    	$view = $this->input->post('view');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $dow = $this->input->post('dow');
        echo $this->teesheet->get_stats($view, $year, $month, $day, $dow);
    }
	function get_teetime_hover_data()
	{
		$teetime_id = substr($this->input->post('teetime_id'), 0, 20);
		//echo 'trying to get here';
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		$sale_ids = $this->sale->get_sale_ids_by_teetime($teetime_id);
		$data['person_info'] = array();
		if ($data['teetime_info']->person_id != 0)
		{
			$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id);
			if ($data['teetime_info']->person_id_2 != 0)
			{
				$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_2);
				if ($data['teetime_info']->person_id_3 != 0)
				{
					$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_3);
					if ($data['teetime_info']->person_id_4 != 0)
					{
						$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_4);
						if ($data['teetime_info']->person_id_5 != 0)
							$data['person_info'][] = $this->customer->get_info($data['teetime_info']->person_id_5);
					}
				}
			}
		}
		//print_r($sale_ids);
		//echo $this->db->last_query();
		echo $this->load->view('teesheets/teetime_info', $data);
		foreach ($sale_ids->result_array() as $result)
		//while ($result = $sale_ids->fetch_assoc())
		{
			$sale_id = $result['sale_id'];
			//echo $result['sale_id'].'<br/>';
			$sale_info = $this->Sale->get_info($sale_id)->row_array();
			$this->sale_lib->copy_entire_sale($sale_id);
			$data['cart']=$this->sale_lib->get_cart();
			$data['payments']=$this->sale_lib->get_payments();
			$this->load->view("teesheets/teetime_sales_info",$data);
			$this->sale_lib->clear_all();
			//echo $this->sales->receipt($result['sale_id']);
		}
	}
	/*
	Inserts/updates a teesheet
	*/
	function save($teesheet_id=false)
	{
		$teesheet_data = array(
			'teesheet_id' => $teesheet_id,
			'title' => $this->input->post('name'),
			'holes' => $this->input->post('holes'),
			'increment' => $this->input->post('increment'),
			'frontnine' => $this->input->post('frontnine'),
			'default' => $this->input->post('default'),
			'online_booking' => $this->input->post('online_booking'),
			'send_thank_you' => $this->input->post('send_thank_you'),
			'thank_you_campaign_id' => $this->input->post('thank_you_campaign_id'),
			'days_out' => 0,//$this->input->post('days_out'), // NOT YET PUSHED OUT
			'days_in_booking_window' => $this->input->post('days_in_booking_window'),
			'minimum_players' => $this->input->post('minimum_players'),
			'limit_holes' => $this->input->post('limit_holes'),
			'booking_carts' => $this->input->post('booking_carts'),
			'online_open_time' => $this->input->post('online_open_time'),
			'online_close_time' => $this->input->post('online_close_time'),
			'course_id' => $this->session->userdata('course_id')
		);

		if($this->teesheet->save($teesheet_data, $teesheet_id))
		{
			//If online booking is on, let's make sure that automatic updates is turned on for the teesheet
			//if ($teesheet_data['online_booking']) {
				//$this->load->model('course');
				//$mt_array = array();
				//echo 'before save attempt';
				//$course_data = array('teesheet_updates_automatically'=> 1);
				//$this->course->save($course_data, $this->session->userdata('course_id'), $mt_array);
				//echo $this->db->last_query();
			//}
			if ($this->session->userdata('increment') != $this->input->post('increment'))
	            $this->teesheet->adjust_teetimes($teesheet_id);
        	if($teesheet_id==false)
				$teesheet_id = $teesheet_data['teesheet_id'];
			if ($teesheet_data['default'] == 1)
				$this->teesheet->switch_tee_sheet($teesheet_id);
			echo json_encode(array('success'=>true,'message'=>lang('teesheets_successful_updating').' '.
			$teesheet_data['title'],'teesheet_id'=>$teesheet_id));
		}
		else//failure
		{
                    
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_error_adding_updating').' '.
			$teesheet_data['teesheet_title'],'teesheet_id'=>-1,'sql'=>$this->db->last_query()));
		}
	}
	/*
	Inserts/updates a teetime
	*/
        function standby_to_teetime()
        {
            $this->load->library('name_parser');
            $standby_id = $this->input->post('id');
            $destination_date = substr($this->input->post('date'), 0, 25);
            log_message('error', "COURSEID: " . $this->config->item('course_id'));
            $side = $this->input->post('side');
            $destination_start_time = date('YmdHi', strtotime($destination_date))-1000000;
            $month = intval(substr($destination_start_time, 4, 2));
            // $month--;
            // if ($month < 10)
            // {
                // $destination_start_time[4] = 0;
                // $destination_start_time[5] = $month;
            // }
            // else
            // {
                // $destination_start_time[4] = 1;
                // $destination_start_time[5] = $month - 10;
            // }
            log_message('error', 'START TIME: ' . $destination_start_time);
            $destination_end_time = date('YmdHi', strtotime($destination_date.' +'. intval($this->session->userdata('increment')) . ' minutes'))-1000000;
            // $month = intval(substr($destination_end_time, 4, 2));
            // $month--;
            // if ($month < 10)
            // {
                // $destination_end_time[4] = 0;
                // $destination_end_time[5] = $month;
            // }
            // else
            // {
                // $destination_end_time[4] = 1;
                // $destination_end_time[5] = $month - 10;
            // }
            log_message('error', 'STANDBY TEETIME: ' . $destination_start_time . ' END TIME: ' . $destination_end_time . ' increment ' . $this->session->userdata('increment'));
            //log_message('error', 'END TIME' . $destination_start_time . ' ' .$destination_end_time);
            $this->load->model('teetime');
            $this->load->model('item');
	    $this->load->library('sale_lib');
            $go_to_register = false;
            $data = ($this->standby->get_by_id($standby_id));
            
            $teetime_id = $this->teesheet->generateID('tt');
            log_message('error', 'Generate teetime ID: ' . $teetime_id);
            $json_ready_events = array();
            $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
            $person_data = array();
			if ($this->input->post('save_customer_1') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $data['name']));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data['first_name'] = $np->getFirstName();
					$person_data['last_name'] = $np->getLastName();
					$person_data['phone_number'] = $this->input->post('phone');
					$person_data['email'] = $this->input->post('email');
					$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data, $customer_data, $person_id); 
				}
			}
            
            $existing_teetime_info = $this->teetime->get_info($teetime_id);
            $event_data = array(
			'TTID'=>$teetime_id,
			'teesheet_id'=>$this->session->userdata('teesheet_id'),//$this->input->post('teesheet_id'),
			'start'=>$destination_start_time,
			'end'=>$destination_end_time,
			'side'=>$this->input->post('side'),
			'allDay'=>'false'
            );
            $event_data['details'] = 'Standby: ' . $data['details'];
            $event_data['holes'] = $data['holes'];
            $event_data['player_count'] = $data['players'];
            $event_data['paid_player_count'] = 0;
            $event_data['title'] = $data['name'];
            $event_data['type'] = 'teetime';
            $event_data['booking_source'] = 'standby';
            $event_data['booker_id'] = $employee_id;
            $event_data['send_confirmation'] = 1;
            $event_data['person_id'] = $data['person_id'];
            $event_data['person_name'] = str_replace($replaceables, '', $this->input->post('teetime_title'));
            //$this->standby->delete($standby_id);
            $save_response = $this->teetime->save($event_data,$teetime_id, $json_ready_events); 
            //log_message('error', 'RESPONSE: ' . $save_response);
        if($save_response['success'])
        {
                $this->standby->delete($standby_id);   
                $standby_list = $this->standby->get_list();
			//New teetime
                if(!$teetime_id)
				{
		                    echo json_encode(array('standby_change'=>$standby_list, 'success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
				}
				else //previous teetime
				{
		                    echo json_encode(array('standby_change'=>$standby_list, 'success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
				}
        }
	    else//failure
	    {	
                //log_message('error', 'FAILURE');
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_teetime_saving_error'), 'teetimes'=>$json_ready_events));
	    }
    }
	function save_teetime($existing_teetime_id=false)
	{
		$split_teetimes = $this->input->post('split_teetimes');
		$title_array = $person_id_array = $person_name_array = array();

		// GENERATE TEE TIME ID
		if (!$existing_teetime_id)
			$teetime_id = $this->teesheet->generateID('tt');
		else
			$teetime_id = $existing_teetime_id;

		// LOAD LIBRARIES AND MODELS
		$this->load->library('name_parser');
		$this->load->model('item');
		$this->load->library('sale_lib');

		$go_to_register = false;
		$employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
		$existing_teetime_info = $this->teetime->get_info($teetime_id);

		$type = $this->input->post('event_type');
		$replaceables = array("'", '"', "\\", "/");

		// CREATE THE PRIMARY EVENT (TEE TIME, EVENT, ETC)
		$event_data = array(
			'TTID'=>$teetime_id,
			'teesheet_id'=>($this->input->post('new_teesheet')?$this->input->post('new_teesheet'):$this->session->userdata('teesheet_id')),//$this->input->post('teesheet_id'),
			'start'=>($this->input->post('start')<1000?$existing_teetime_info->start:$this->input->post('start')),
			'end'=>($this->input->post('end')<1000?$existing_teetime_info->end:$this->input->post('end')),
			'side'=>$this->input->post('side'),
			'allDay'=>'false'
		);
                //log_message('error', "Start time: " . $this->input->post('start'));
		if ($this->config->item('simulator') && $this->input->post('teetime_time'))
		{
			$event_data['end'] = $event_data['start'] + $this->input->post('teetime_time');
			if ($event_data['end'] % 100 > 59)
                        $event_data['end'] += 40;
		}
		if ($type)
			$event_data['type'] = $type;


		// ADD DATA FOR A BLOCK EVENT TYPE
		if ($type == 'closed') {
			$event_data['title'] = str_replace($replaceables, '', $this->input->post('closed_title'));
			$event_data['details'] = $this->input->post('closed_details');
			$event_data['holes'] = 9;
		}
		// ADD DATA FOR A TOURNAMENT, EVENT, OR LEAGUE EVENT TYPE
		else if ($type == 'tournament' || $type == 'league' || $type == 'event') {
			$event_data['title'] = str_replace($replaceables, '', $this->input->post('event_title'));
			$event_data['details'] = $this->input->post('event_details');
			$event_data['holes'] = $this->input->post('event_holes');
			$event_data['player_count'] = $this->input->post('event_players');
			$event_data['carts'] = $this->input->post('event_carts');
			$event_data['paid_player_count'] = $this->input->post('event_players');
			$event_data['paid_carts'] = $this->input->post('event_carts');
			$event_data['status'] = 'checked in';
		}
		// ADD DATA FOR A TEE TIME EVENT TYPE
		else if ($type == 'teetime') {
			$person_data = $person_data_2 = $person_data_3 = $person_data_4 = $person_data_5 = array();
			// SAVE CUSTOMERS (UP TO 5)
			if ($this->input->post('save_customer_1') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data['first_name'] = $np->getFirstName();
					$person_data['last_name'] = $np->getLastName();
					$person_data['phone_number'] = $this->input->post('phone');
					$person_data['email'] = $this->input->post('email');
					$person_data['zip'] = $this->input->post('zip');
					$person_id = ($this->input->post('person_id') == '')?false:$this->input->post('person_id');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_2') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_2')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_2['first_name'] = $np->getFirstName();
					$person_data_2['last_name'] = $np->getLastName();
					$person_data_2['phone_number'] = $this->input->post('phone_2');
					$person_data_2['email'] = $this->input->post('email_2');
					$person_id = ($this->input->post('person_id_2') == '')?false:$this->input->post('person_id_2');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_2, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_3') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_3')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_3['first_name'] = $np->getFirstName();
					$person_data_3['last_name'] = $np->getLastName();
					$person_data_3['phone_number'] = $this->input->post('phone_3');
					$person_data_3['email'] = $this->input->post('email_3');
					$person_id = ($this->input->post('person_id_3') == '')?false:$this->input->post('person_id_3');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_3, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_4') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_4')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_4['first_name'] = $np->getFirstName();
					$person_data_4['last_name'] = $np->getLastName();
					$person_data_4['phone_number'] = $this->input->post('phone_4');
					$person_data_4['email'] = $this->input->post('email_4');
					$person_id = ($this->input->post('person_id_4') == '')?false:$this->input->post('person_id_4');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_4, $customer_data, $person_id);
				}
			}
			if ($this->input->post('save_customer_5') == 'on') {
				$np = new Name_parser();
				$np->setFullName(str_replace($replaceables, '', $this->input->post('teetime_title_5')));
				$np->parse();
				if (!$np->notParseable()) {
					$person_data_5['first_name'] = $np->getFirstName();
					$person_data_5['last_name'] = $np->getLastName();
					$person_data_5['phone_number'] = $this->input->post('phone_5');
					$person_data_5['email'] = $this->input->post('email_5');
					$person_id = ($this->input->post('person_id_5') == '')?false:$this->input->post('person_id_5');
					$customer_data = array('course_id'=>$this->session->userdata('course_id'));
					$this->Customer->save($person_data_5, $customer_data, $person_id);
				}
			}
			// ADD ALL TEE TIME DATA
			$event_data['details'] = $this->input->post('teetime_details');
			$event_data['holes'] = $this->input->post('teetime_holes');
			$event_data['player_count'] = ($this->input->post('players')?$this->input->post('players'):$this->input->post('current_player_count'));
			$event_data['carts'] = $this->input->post('carts');
			$event_data['person_id'] = (isset($person_data['person_id']))?$person_data['person_id']:$this->input->post('person_id');
			$event_data['price_class_1'] = str_replace('price_category_', '', $this->input->post('price_class_1'));
			$event_data['person_name'] = str_replace($replaceables, '', $this->input->post('teetime_title'));
			$event_data['person_id_2'] = (isset($person_data_2['person_id']))?$person_data_2['person_id']:$this->input->post('person_id_2');
			$event_data['price_class_2'] = str_replace('price_category_', '', $this->input->post('price_class_2'));
			$event_data['person_name_2'] = str_replace($replaceables, '', $this->input->post('teetime_title_2'));
			$event_data['person_id_3'] = (isset($person_data_3['person_id']))?$person_data_3['person_id']:$this->input->post('person_id_3');
			$event_data['price_class_3'] = str_replace('price_category_', '', $this->input->post('price_class_3'));
			$event_data['person_name_3'] = str_replace($replaceables, '', $this->input->post('teetime_title_3'));
			$event_data['person_id_4'] = (isset($person_data_4['person_id']))?$person_data_4['person_id']:$this->input->post('person_id_4');
			$event_data['price_class_4'] = str_replace('price_category_', '', $this->input->post('price_class_4'));
			$event_data['person_name_4'] = str_replace($replaceables, '', $this->input->post('teetime_title_4'));
			$event_data['person_id_5'] = (isset($person_data_5['person_id']))?$person_data_5['person_id']:$this->input->post('person_id_5');
			$event_data['price_class_5'] = str_replace('price_category_', '', $this->input->post('price_class_5'));
			$event_data['person_name_5'] = str_replace($replaceables, '', $this->input->post('teetime_title_5'));
			$event_data['teed_off_time'] = ($this->input->post('teed_off_time')?date('Y-m-d', strtotime($existing_teetime_info->teed_off_time)).' '.date('H:i:00', strtotime($this->input->post('teed_off_time'))):$existing_teetime_info->teed_off_time);
			$event_data['turn_time'] = ($this->input->post('turn_time')?date('Y-m-d', strtotime($existing_teetime_info->turn_time)).' '.date('H:i:00', strtotime($this->input->post('turn_time'))):$existing_teetime_info->turn_time);
			$event_data['send_confirmation'] = $this->input->post('send_confirmation_emails') ? '1' : '2';
			$teetime_title = str_replace($replaceables, '', $this->input->post('teetime_title'));
			$event_data['credit_card_id'] = $this->input->post('credit_card_id');
			//$event_data['zip'] = $this->input->post('zip');
			//$person_info = $person_info_2 = $person_info_3 = $person_info_4 = $person_info_5 = stdClass;
			
			// CREATE TITLE FOR TEE TIME FROM CUSTOMER NAMES
			if ($event_data['person_id'] != 0)
			{
				if ($event_data['credit_card_id'] != 0)
				{
					$this->load->model("Customer_credit_card");
					$card_data = array('customer_id'=>$event_data['person_id']);
					$this->Customer_credit_card->save($card_data, $event_data['credit_card_id']);
				}
				$person_info = $this->Customer->get_info($event_data['person_id']);
				$title_array[1] = ($person_info->last_name != '')?$person_info->last_name:$person_info->first_name;
				$person_id_array[1] = $event_data['person_id'];
				$person_name_array[1] = $event_data['person_name'];
			}
			else if ($event_data['person_name'] != '')
				$title_array[1] = $person_name_array[1] = $event_data['person_name'];
			if ($event_data['person_id_2'] != 0)
			{
				$person_info_2 = $this->Customer->get_info($event_data['person_id_2']);
				$title_array[2] = ($person_info_2->last_name != '')?$person_info_2->last_name:$person_info_2->first_name;
       			$person_id_array[2] = $event_data['person_id_2'];
				$person_name_array[2] = $event_data['person_name_2'];
			}
			else if ($event_data['person_name_2'] != '')
				$title_array[2] = $person_name_array[2] = $event_data['person_name_2'];
			if ($event_data['person_id_3'] != 0)
			{
				$person_info_3 = $this->Customer->get_info($event_data['person_id_3']);
				$title_array[3] = (($person_info_3->last_name != '')?$person_info_3->last_name:$person_info_3->first_name);
				$person_id_array[3] = $event_data['person_id_3'];
				$person_name_array[3] = $event_data['person_name_3'];
			}
			else if ($event_data['person_name_3'] != '')
				$title_array[3] = $person_name_array[3] = $event_data['person_name_3'];
			if ($event_data['person_id_4'] != 0)
			{
				$person_info_4 = $this->Customer->get_info($event_data['person_id_4']);
				$title_array[4] = (($person_info_4->last_name != '')?$person_info_4->last_name:$person_info_4->first_name);
				$person_id_array[4] = $event_data['person_id_4'];
				$person_name_array[4] = $event_data['person_name_4'];
			}
			else if ($event_data['person_name_4'] != '')
				$title_array[4] = $person_name_array[4] = $event_data['person_name_4'];
			if ($event_data['person_id_5'] != 0)
			{
				$person_info_5 = $this->Customer->get_info($event_data['person_id_5']);
				$title_array[5] = (($person_info_5->last_name != '')?$person_info_5->last_name:$person_info_5->first_name);
				$person_id_array[5] = $event_data['person_id_5'];
				$person_name_array[5] = $event_data['person_name_5'];
			}
			else if ($event_data['person_name_5'] != '')
				$title_array[5] = $person_name_array[5] = $event_data['person_name_5'];
			$teetime_title = count($title_array) > 0 ? implode(' - ', $title_array):$teetime_title;
			$event_data['title'] = $teetime_title;
			// END MAKE TITLE FROM CUSTOMER NAMES
			
			$event_data['booking_source'] = 'POS';
			$event_data['booker_id'] = $employee_id;
			
			// AUTOMATICALLY ADD TEE TIMES TO SHOPPING CART
			if ($this->input->post('purchase_quantity')) {
				$go_to_register = true;
				//  THESE ARE PRICE CLASSES ASSIGNED ON THE TEE TIME 
				$price_class_1 = $this->input->post('price_class_1') ? $this->input->post('price_class_1') : '';
				$price_class_2 = $this->input->post('price_class_2') ? $this->input->post('price_class_2') : '';
				$price_class_3 = $this->input->post('price_class_3') ? $this->input->post('price_class_3') : '';
				$price_class_4 = $this->input->post('price_class_4') ? $this->input->post('price_class_4') : '';
				$price_class_5 = $this->input->post('price_class_5') ? $this->input->post('price_class_5') : '';
				// THESE ARE THE PRICE CLASSES ASSIGNED TO EACH CUSTOMER IN THE TEE TIME
				$ppc_1 = (isset($person_info->price_class) && $person_info->price_class !== 0)?$person_info->price_class:'';
				$ppc_2 = (isset($person_info_2->price_class) && $person_info_2->price_class !== 0)?$person_info_2->price_class:'';
				$ppc_3 = (isset($person_info_3->price_class) && $person_info_3->price_class !== 0)?$person_info_3->price_class:'';
				$ppc_4 = (isset($person_info_4->price_class) && $person_info_4->price_class !== 0)?$person_info_4->price_class:'';
				$ppc_5 = (isset($person_info_5->price_class) && $person_info_5->price_class !== 0)?$person_info_5->price_class:'';
				// THE TEE TIME PRICE CLASSES TAKE PRECEDENCE
				$price_class_1 = $price_class_1 ? $price_class_1 : $ppc_1;
				$price_class_2 = $price_class_2 ? $price_class_2 : $ppc_2;
				$price_class_3 = $price_class_3 ? $price_class_3 : $ppc_3;
				$price_class_4 = $price_class_4 ? $price_class_4 : $ppc_4;
				$price_class_5 = $price_class_5 ? $price_class_5 : $ppc_5;
				$person_price_class_array = array(
					0=>$price_class_1,
					1=>$price_class_2,
					2=>$price_class_3,
					3=>$price_class_4,
					4=>$price_class_5
				);
				$this->add_green_fees_to_cart($teetime_id, $person_price_class_array);
				$this->sale_lib->delete_customer();
				$this->sale_lib->delete_customer_quickbuttons();
				$this->sale_lib->set_customer($event_data['person_id']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id'], $event_data['person_name']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_2'], $event_data['person_name_2']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_3'], $event_data['person_name_3']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_4'], $event_data['person_name_4']);
				$this->sale_lib->set_customer_quickbutton($event_data['person_id_5'], $event_data['person_name_5']);
			}
			// IF WE'RE JUST CHECKING IN, THEN ADD CHECKIN DATA
			else if ($this->input->post('checkin')) {
				$event_data['paid_player_count'] = $this->input->post('players');
				$event_data['paid_carts'] = $this->input->post('carts');
				$event_data['status'] = 'checked in';
				//$this->teesheet->record_teetime_checkin($teetime_id, $this->input->post('players'));
			}
		}
		if ($this->input->post('delete_teetime') == 1)
			$event_data['status'] = 'deleted';
		//else 
		//	$event_data['title'] = $event_data['title'] || $event_data['title'] == '' ? 'Tee Time' : $event_data['title'];
		// TODO: Needs revamping ... changing to teetime	
		$json_ready_events = array();
		// SPLIT THE TEE TIME
		if ($split_teetimes && !$this->input->post('purchase_quantity'))
		{
			$cur_teetime_info = (array)$this->teetime->get_info($teetime_id);
			$cur_teetime_info['status'] = 'deleted';
			$save_response = $this->teetime->save($cur_teetime_info,$teetime_id, $json_events);
			$json_ready_events = array_merge($json_ready_events, $json_events);

			//$this->teetime->delete($existing_teetime_id);
			//$json_ready_events[] = array('TTID'=>$existing_teetime_id,'status'=>'deleted');

			$json_events = array();
			$carts = $event_data['carts'];
			$paid = $cur_teetime_info['paid_player_count'];
			$paid_carts = $cur_teetime_info['paid_carts'];
			//foreach($title_array as $index => $title)
			$player_count = ($event_data['player_count'] > count($title_array))?$event_data['player_count']:count($title_array);
			for ($i = 1; $i <= $player_count; $i++)
			{
				$event_data['person_id_2'] = $event_data['person_id_3'] = $event_data['person_id_4'] = $event_data['person_id_5'] = 0;
				$event_data['person_name_2'] = $event_data['person_name_3'] = $event_data['person_name_4'] = $event_data['person_name_5'] = '';
				$event_data['person_id'] = $person_id_array[$i];
				$event_data['person_name'] = $person_name_array[$i];
				$event_data['title'] = ($title != '')? $title : $person_name_array[$i];
				if ($event_data['title'] == '')
				{
					$event_data['title'] = $person_name_array[1];
					$event_data['person_id'] = ($person_id_array[1])?$person_id_array[1]:0;
					$event_data['person_name'] = $person_name_array[1];
				}
				else if (!$event_data['person_id'])
					$event_data['person_id'] = 0;
                //$event_data['title'] = ($title_array[$index] != '') ? $title_array[$index]:($title_array[0]);
				$event_data['player_count'] = 1;
				$event_data['carts'] = $carts > 0 ? 1:0;
				$event_data['paid_player_count'] = $paid > 0 ? 1:0;
				$event_data['paid_carts'] = $paid_carts > 0 ? 1:0;
				$teetime_id = $this->teesheet->generateID('tt');
				$event_data['TTID'] = $teetime_id;
				$save_response = $this->teetime->save($event_data,$teetime_id, $json_events);
				$sql_array[] = $this->db->last_query();
				$json_ready_events = array_merge($json_ready_events, $json_events);
				$carts--;
				$paid--;
				$paid_carts--;
			}
		}
		// SAVE TEE TIME WITHOUT SPLITTING
		else
			$save_response = $this->teetime->save($event_data,$teetime_id, $json_ready_events);

		$message = '';
		if ($this->session->userdata('teesheet_id') != $this->session->userdata('default_teesheet_id') && $existing_teetime_id)// && $this->session->userdata('teesheet_reminder_count') < 3)
		{
			$message = lang('teesheets_not_default_teesheet');
			$this->session->set_userdata('teesheet_reminder_count', $this->session->userdata('teesheet_reminder_count')+1);
		}
		if($save_response['success'])
		{
                    foreach(array_keys($json_ready_events) as $keys)
                    {
                        foreach(array_keys($json_ready_events[$keys]) as $new_data)
                        log_message('error', 'JSON DATA: ' . $new_data . ' ' . $json_ready_events[$keys][$new_data]);
                    }
                    //New teetime
			if(!$teetime_id)
			{
				echo json_encode(array('success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
			}
			else //previous teetime
			{
				echo json_encode(array('success'=>true,'message'=>$message, 'teetimes'=>$json_ready_events, 'go_to_register'=>$go_to_register, 'send_confirmation'=>$save_response['send_confirmation'], 'second_time_available'=>$save_response['second_time_available']));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('teesheets_teetime_saving_error'), 'teetimes'=>$json_ready_events));
		}
	}
	function customer_search($type='')
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}

	function teetime_search()
	{
		$customer_id = $this->input->post('customer_id');
		//echo json_encode(array('search_results'=>$customer_id));
		//return;
		$search_results = $this->teetime->search($customer_id);
		echo json_encode(array('search_results'=>$search_results));
	}
	function view_note()
	{
		$note_data = array();
		$this->load->view('employees/form_message', $note_data);
	}
	function save_note()
	{
		$note_data = array(
			'course_id'=>$this->session->userdata('course_id'),
			'author'=>$this->session->userdata('person_id'),
			'recipient'=>$this->input->post('recipient_id') ? $this->input->post('recipient_id') : null,
			'date'=>date('Y-m-d H:i:s'),
			'message'=>$this->input->post('message')
		);
		$this->load->model('note');
		$this->note->save($note_data);
		
		$notes = $this->note->get_all()->result_array();
		echo json_encode(array('note_html' => $this->load->view('employees/notes', array('notes'=>$notes), true)));
	}
	function get_notes($limit = 5)
	{
		$this->load->model('note');
		$notes = $this->note->get_all($limit)->result_array();
		//echo $this->db->last_query();
		echo json_encode(array('note_html' => $this->load->view('employees/notes', array('notes'=>$notes), true)));
	}
	function delete_note($note_id)
	{
		$this->load->model('note');
		echo json_encode('success', $this->note->delete($note_id));
	}
	function view_standby()
	{
		$data = array();
	    $data['no_delete'] = $this->input->get('id') ? FALSE : TRUE;
	    //$this->load->helper('sale_helper');
	    $data['standby_name'] = $this->input->get('name');
	    $data['standby_id'] = $this->input->get('id');
	    $data['my_standby_time'] = $this->input->get('time');
            
	    //log_message('error', 'standby time: ' . $data['my_standby_time']);
	    $data['standby_holes'] = $this->input->get('holes');
	    $data['standby_players'] = $this->input->get('players');
	    $data['standby_details'] = $this->input->get('details');
	    //log_message('error', 'Details ' . $standby_details);
	    $data['standby_email'] = $this->input->get('email');
	    $data['standby_phone'] = $this->input->get('phone');
        $data['teesheet_info'] = $this->teesheet->get_info($this->session->userdata('teesheet_id'));
        
		//echo 'hi';
        $teetimes = $this->teesheet->get_teetime_array($this->config->item('open_time'), $this->config->item('close_time'), $data['teesheet_info']->increment, $this->session->userdata('teesheet_id'));
        //log_message('error', 'TEEtimes: ' . $teetimes);
		$data['teetimes'] = $teetimes;
        $this->load->view('teetimes/standby', $data);
	}
        function teetime_standby_get_list()
        {
            $standby_list = $this->standby->get_list();
            echo json_encode($standby_list);
        }
        function teetime_save_standby($array)
        {          
            $time = $this->input->post('standby_time');
            $holes = $this->input->post('teetime_holes');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $name = $this->input->post('teetime_title');
            $num_players = $this->input->post('players');
            $details = 'Standby: ' .$this->input->post('standby_details');
            $person_id = $this->input->post('person_id');
            $this->standby->save($name, $email, $phone, $holes, $num_players, $time, $details, $person_id);
            $this->teetime_standby_get_list();          
        }
        function teetime_edit_standby($array)
        {
            
            $time = $this->input->post('standby_time');
            $holes = $this->input->post('teetime_holes');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $name = $this->input->post('teetime_title');
            $num_players = $this->input->post('players');
            $details = $this->input->post('standby_details');
            $id = $this->input->post('id');
            $delete = $this->input->post('delete');
            $person_id = $this->input->post('person_id');
            if ($delete === 'false')
            {
                ($this->standby->edit($name, $email, $phone, $holes, $num_players, $time, $details, $id, $person_id)); 
                $this->teetime_standby_get_list();
                
            }
            else
            {               
                ($this->standby->delete($id));   
                $this->teetime_standby_get_list();
            }
                
        }
        
	function teesheet_updates($course_id, $teetimes = 'stu ff')
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

		function sendMsg($id, $msg) {
		  echo "id: $id" . PHP_EOL;
		  echo "retry: 20000" . PHP_EOL;
		  echo "data: $msg" . PHP_EOL;
		  echo PHP_EOL;
		  ob_flush();
		  flush();
		}


		//if(isset($_GET['user']) && isset($_GET['msg'])){
		//	$fp = fopen($course_id."log.txt", 'a');
		//    fwrite($fp, "<div class='msgln'><b>stuff</b>: other stuff<br></div>");
		//    fclose($fp);
		//}

		//if(file_exists($course_id."log.txt") && filesize($course_id."log.txt") > 0){
		//    $handle = fopen($course_id."log.txt", "r");
		//    $contents = fread($handle, filesize($course_id."log.txt"));
		//    fclose($handle);

		//deleting file when it get bigger
		//	if(filesize($course_id."log.txt")>1100){
		//		@unlink($course_id."log.txt");
		//	}
		//}
		$contents = 'stuff2';
		$return_teetimes = array();
		$return_teetimes = $this->getJSONTeeTimes2();
		sendMsg(time(),$return_teetimes);
	}
	/*
	 * fetch_all_data is used by local_teesheet.js to grab all the data needed to populate the local database
	 */
	function fetch_all_data()
	{
		$teetimes = $this->teesheet->getTeeTimes((date('Ymd', strtotime('-1 week'))-100).'0000')->result_array();
		$teesheets = $this->teesheet->get_all()->result_array();

		echo json_encode(array('teetimes'=>$teetimes, 'teesheets'=>$teesheets));

	}
	function switch_teetime_sides($teetime_id)
	{
		$this->teetime->switch_sides($teetime_id);
	}
    function split_by_time($teetime_id)
    {
        $events = $this->teetime->split_by_time($teetime_id);               
        echo json_encode(array('teetimes'=>$events));
    }
	function mark_teetime_teedoff($teetime_id)
	{
		$events = $this->teetime->mark_as_teedoff($teetime_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function mark_teetime_turned($teetime_id)
	{
		$events = $this->teetime->mark_as_turned($teetime_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function mark_teetime_finished($teetime_id)
	{
		$events = $this->teetime->mark_as_finished($teetime_id);
		echo json_encode(array('teetimes'=>$events));
	}
	function view_move($teetime_id, $side)
	{
		$data = array('teetime_id'=>$teetime_id, 'side'=>$side);
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		$teesheet_info = $this->teesheet->get_info($this->session->userdata('teesheet_id'));
                
		$data['teetimes'] = $this->teesheet->get_teetime_array($this->config->item('open_time'), $this->config->item('close_time'), $teesheet_info->increment, $this->session->userdata('teesheet_id'));
	 	$data['teesheets'] = $this->teesheet->get_teesheet_array();
	 	$data['difference'] = (strtotime($data['teetime_info']->end) - strtotime($data['teetime_info']->start))/60;
		$data['frontnine'] = $teesheet_info->frontnine;
			//print_r($teesheet_info);
			//print_r($data['teetimes']);
		$this->load->view('teetimes/move', $data);
	}
	function get_teetime_dropdown()
	{
		$teesheet_id = $this->input->post('teesheet_id');
		$teesheet_info = $this->teesheet->get_info($teesheet_id);
		//echo 'heyo '.$this->config->item('open_time').' - '.$this->config->item('close_time').' - '.$teesheet_info->increment.' - '.$teesheet_id;
		//return;
		$teetimes = $this->teesheet->get_teetime_array($this->config->item('open_time'), $this->config->item('close_time'), $teesheet_info->increment, $teesheet_id);
		$frontnine = $teesheet_info->frontnine;
		$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $teesheet_info->increment);
		$frontnine += $teesheet_info->increment - $dif;

		echo json_encode(
			array(
				'teetime_dropdown' => form_dropdown('new_time', $teetimes, ''),
				'difference_input' => form_hidden('difference', $teesheet_info->increment),
				'frontnine_input' => form_hidden('frontnine', $frontnine)
			)
		);
	}
	function view_repeat($teetime_id)
	{
		$data = array('teetime_id'=>$teetime_id);
		$data['teetime_info'] = $this->teetime->get_info($teetime_id);
		$this->load->view('teetimes/repeat', $data);
	}
	function move($teetime_id, $player_count)
	{
		$new_date = $this->input->post('new_date');
		$new_time = $this->input->post('new_time');
		/*
		 * Added new_teesheet parameter, but still need to adjust the difference value...
		 * We'll use the same functionality that save uses to adjust the 'difference' value to normalize if for the new teesheet. Once we've adjusted the difference, we should be done.
		 */
		$teesheet_id = $this->input->post('new_teesheet');
		$difference = $this->input->post('difference');
		$frontnine = $this->input->post('frontnine');
		$time_1 = ($new_time+$difference < 1000)?'0'.($new_time+$difference):$new_time+$difference;
		$time_2 = ($new_time+$frontnine < 1000)?'0'.($new_time+$frontnine):$new_time+$frontnine;
		$time_3 = ($new_time+$frontnine+$difference < 1000)?'0'.($new_time+$frontnine+$difference):$new_time+$frontnine+$difference;
		$date_string = date('YmdHi', strtotime($new_date.' '.$new_time.' -1 month'));
		$end_date_string = date('YmdHi', strtotime($new_date.' '.$time_1.' -1 month'));
		$back_date_string = date('YmdHi', strtotime($new_date.' '.$time_2.' -1 month'));
		$back_end_date_string = date('YmdHi', strtotime($new_date.' '.$time_3.' -1 month'));
		$available = $this->teetime->check_availability(array('start'=>$date_string,'player_count'=>$player_count, 'teesheet_id'=>$teesheet_id));
		if (strtotime($new_date) === false)
			echo json_encode(array('success'=>false, 'message'=>'Not a valid date', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string)));
		else if (!$available)
			echo json_encode(array('success'=>false, 'message'=>'Teetime', 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string), 'frontnine'=>$frontnine));
		else
		{
			$this->teetime->change_date($teetime_id, $date_string, $end_date_string, $back_date_string, $back_end_date_string, $teesheet_id);
			echo json_encode(array('success'=>true, 'message'=>'Successfully changed date', 'sql'=>$this->db->last_query(), 'date_strings'=>array($date_string,$end_date_string,$back_date_string,$back_end_date_string),'teesheet_id'=>$teesheet_id, 'available'=>$available));
		}
	}
	function repeat($teetime_id)
	{
		$repeats = $this->input->post('repeats');
		$sunday = $this->input->post('sunday');
		$monday = $this->input->post('monday');
		$tuesday = $this->input->post('tuesday');
		$wednesday = $this->input->post('wednesday');
		$thursday = $this->input->post('thursday');
		$friday = $this->input->post('friday');
		$saturday = $this->input->post('saturday');
		$start_date = date('Ymd', strtotime($this->input->post('start_date')));
		$end_date = ($repeats != 'once')?date('Ymd', strtotime($this->input->post('end_date'))):date('Ymd', strtotime($this->input->post('start_date')));

		$teetime_info = $this->teetime->get_info($teetime_id);
		$event_data = (array)$teetime_info;
		$event_data['paid_player_count'] = 0;
		$event_data['paid_carts'] = 0;
		$event_data['status'] = '';
		$event_data['date_booked'] = date('Y-m-d H:i:s');
		unset($event_data['TTID']);
		$start_time = date('Hi', strtotime($teetime_info->start));
		$end_time = date('Hi', strtotime($teetime_info->end));
		$event_date = date('Ymd', strtotime("{$teetime_info->start} +1 month +1 day"));
		$event_date = ($start_date > $event_date)?$start_date:$event_date;
		$json_ready_events = array();
		while ($event_date <= $end_date)
		{
			$event_data['start'] = (int)($event_date.$start_time) - 1000000;
			$event_data['end'] = (int)($event_date.$end_time) - 1000000;
			if ($repeats == 'once' || $repeats == 'daily')
			{
				$this->teetime->save($event_data, false, $json_ready_events);
			}
			else if ($repeats == 'weekly')
			{
				$dow = date('w',strtotime($event_date));
				if (($sunday && $dow == 0) ||
					($monday && $dow == 1) ||
					($tuesday && $dow == 2) ||
					($wednesday && $dow == 3) ||
					($thursday && $dow == 4) ||
					($friday && $dow == 5) ||
					($saturday && $dow == 6))
						$this->teetime->save($event_data,false, $json_ready_events);
			}
			$event_date = date('Ymd', strtotime("$event_date +1 day"));
		}

		echo json_encode(array('success'=>true, 'repeats'=>$repeats, 'sunday'=>$sunday, 'monday'=>$monday, 'end_date'=>$end_date, 'teetime_info'=>$teetime_info, 'teetimes'=>$json_ready_events));
	}
	function add_green_fees_to_cart ($teetime_id, $person_price_class_array = array())
	{
		$item_num = '7';
        $cart_num = '5';
        $holes = $this->input->post('teetime_holes');
        $quantity = $this->input->post('purchase_quantity');
        $start_time = (int)substr($this->input->post('start'),8);
		$adjusted_start_time = $this->input->post('start') + 1000000;
        $dow = date('w',strtotime($adjusted_start_time));
		$carts = $this->input->post('carts')-$this->input->post('paid_carts');

		$this->sale_lib->empty_cart();
	    $this->sale_lib->empty_basket();

	    if ($this->config->item('simulator'))
		{
			$play_time_length = $this->input->post('teetime_time');
			if ($play_time_length == '30')
				$item_num = '1';
			if ($play_time_length == '100')
				$item_num = '2';
			if ($play_time_length == '130')
				$item_num = '3';
			if ($play_time_length == '200')
				$item_num = '4';

			$item_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$item_num);

			$this->sale_lib->add_item($item_id);
	        $this->sale_lib->add_item_to_basket($item_id);
		}
		else
		{
			$type = 'regular';
			$price_category_index = null;
			$not_holiday = true;
			if ($this->config->item('holidays') && $this->Appconfig->is_holiday())//TODO: build is_holiday function
			{
				$price_category_index = 7;
				$not_holiday = false;
			}
			else if ((int)$this->config->item('early_bird_hours_begin') <= $start_time && (int)$this->config->item('early_bird_hours_end') > $start_time)
	        	$price_category_index = 2;
			else if ((int)$this->config->item('morning_hours_begin') <= $start_time && (int)$this->config->item('morning_hours_end') > $start_time)
	        	$price_category_index = 3;
			else if ((int)$this->config->item('afternoon_hours_begin') <= $start_time && (int)$this->config->item('afternoon_hours_end') > $start_time)
	        	$price_category_index = 4;
			else if ((int)$start_time >= $this->config->item('super_twilight_hour'))
		    	$price_category_index = 6;
			else if ((int)$start_time >= $this->config->item('twilight_hour'))
		    	$price_category_index = 5;

			// Check against settings for if it is currently the weekend
			if (($dow == 5 && $this->config->item('weekend_fri'))|| ($dow == 6 && $this->config->item('weekend_sat')) || ($dow == 0 && $this->config->item('weekend_sun')))
	            $type = 'weekend';
	        if ($holes == 9) {
	            $cart_num = '1';
				$item_num = '3';
			}
	        else if ($holes == 18) {
	            $cart_num = '5';
				$item_num = '7';
			}
			if ($type=='weekend')
			{
				$cart_num += 1;
				$item_num += 1;
			}

	        $prices = $this->Green_fee->get_info();
	        $item_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$item_num);
	        $cart_id = $this->Item->get_item_id($this->session->userdata('course_id').'_'.$cart_num);
			$teesheet_id = $this->session->userdata('teesheet_id');
			$course_id = $this->session->userdata('course_id');
			for ($i=0;$i<$quantity;$i++) {
				//TODO: Handle person_price_class_array here... the prices don't trump the holiday, but they trump the time of day
				if (isset($person_price_class_array[$i]) && $person_price_class_array[$i] != '' && $not_holiday && $person_price_class_array[$i] != 'price_category_1')
				{
					$p_cat = $person_price_class_array[$i];
					$pci = str_replace('price_category_', '', $p_cat);
				}
				else
				{
					$pci = $price_category_index;
					$p_cat = 'price_category_'.$pci;
				}
				$this->sale_lib->add_item($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	            $this->sale_lib->add_item_to_basket($item_id,1,0,$prices[$teesheet_id][$course_id.'_'.$item_num]->$p_cat,null,null,$pci,$item_num);
	            if ($i < $carts) {
	                $this->sale_lib->add_item($cart_id,1,0,$prices[$teesheet_id][$course_id.'_'.$cart_num]->$p_cat,null,null,$pci,$cart_num);
	                $this->sale_lib->add_item_to_basket($cart_id,1,0,$prices[$teesheet_id][$course_id.'_'.$cart_num]->$p_cat,null,null,$pci,$cart_num);
	            }
	        }
		}
        //echo $item_id.' - '.$cart_id;
		$this->sale_lib->set_teetime($teetime_id);
	}
	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_teesheets_manage_table_data_rows($this->teesheet->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
	}
	/*
	Gives search suggestions based on what is being searched for

	function suggest()
	{
		$suggestions = $this->teesheet->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	 */
	function get_row()
	{
		$teesheet_id = $this->input->post('row_id');
		$data_row=get_teesheet_data_row($this->teesheet->get_info($teesheet_id),$this);
		echo $data_row;
	}

	/*
	This deletes teetimes from the teetimes table
	*/
	function delete_teetime($teetime_to_delete = -1)
	{
		if ($teetime_to_delete == -1)
			$teetime_to_delete=$this->input->post('id');

		if($this->teetime->delete($teetime_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	/*
	This deletes teesheet from the teesheet table
	*/
	function delete_teesheets()
	{
		$teesheets_to_delete=$this->input->post('ids');

		if($this->teesheet->delete_list($teesheets_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('customers_successful_deleted')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_cannot_be_deleted')));
		}
	}
	function getJSONTeeTimes($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
        echo $this->teesheet->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true);
    }
	function getJSONTeeTimes2($when = '', $side = '') {
		$end = ($when == 'today')? (date('Ymd')-100).'2399':'';
        return $this->teesheet->getJSONTeeTimes((date('Ymd')-100).'0000',$end, $side, true, true);
    }
	function get_json_teetimes($start, $end) {
	    echo $this->teesheet->getJSONTeeTimes($start,$end, '');
    }
	function logout()
	{
            $this->Employee->logout();
	}

	/*
	Loads the teesheet edit form
	*/
	function view_teesheet($teesheet_id=-1)
	{
		$this->load->model('Booking_class');
		$data['teesheet_info']=$this->teesheet->get_info($teesheet_id);
		$data['thank_you_campaign'] = '';
		if ($data['teesheet_info']->thank_you_campaign_id)
		{
			$mc = $this->Marketing_campaign->get_info($data['teesheet_info']->thank_you_campaign_id);
			$data['thank_you_campaign'] = $mc->name;
		}
		$data['obcs'] = $this->Booking_class->get_all($teesheet_id);
		
		$this->load->view("teesheets/form",$data);
	}
	function view_booking_class($teesheet_id, $booking_class_id = -1)
	{
		$this->load->model('Booking_class');
		$data = array(
			'booking_class_info' => $this->Booking_class->get_info($booking_class_id, $teesheet_id),
			'price_classes' => ($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types()),
			'teesheet_id' => $teesheet_id
		);
		$this->load->view('teesheets/booking_class_form', $data);
	}
	function hide_back_nine($value)
	{
		$this->session->set_userdata('hide_back_nine', $value);
	}
	function save_booking_class($booking_class_id = false)
	{
		$this->load->model('Booking_class');
		$booking_class_data = array(
			'teesheet_id' => $this->input->post('teesheet_id'),
			'name' => $this->input->post('bc_name'),
			'active' => $this->input->post('active'),
			'price_class' => $this->input->post('bc_price_class'),
			'online_open_time' => $this->input->post('booking_class_open_time'),
			'online_close_time' => $this->input->post('booking_class_close_time'),
			'online_booking_protected' => $this->input->post('online_booking_protected'),
			'days_in_booking_window' => $this->input->post('bc_days_in_booking_window'),
			'minimum_players' => $this->input->post('bc_minimum_players'),
			'limit_holes' => $this->input->post('bc_limit_holes'),
			'booking_carts' => $this->input->post('bc_booking_carts')
		);
		$this->Booking_class->save($booking_class_data, $booking_class_id);
		// $validators = $this->input->post('booking_class_validators');
		// $validator_data = array();
		// foreach ($validators as $validator)
		// {
			// $validator_data[] = array(
				// 'booking_class_id'=>$booking_class_id,
				// 'type' => 'price_class',
				// 'value' => $validator
			// );
		// }
		// $this->Booking_class->save_validators($booking_class_id, $validator_data);
		
		$data = array();
		$data['teesheet_info']=$this->teesheet->get_info($booking_class_data['teesheet_id']);
		$data['obcs'] = $this->Booking_class->get_all($booking_class_data['teesheet_id']);
		echo $this->load->view('teesheets/booking_classes', $data);
	}
	function delete_booking_class($teesheet_id, $booking_class_id)
	{
		$this->load->model('Booking_class');
		$success = $this->Booking_class->delete($teesheet_id, $booking_class_id);
		
		echo json_encode(array('success'=>$success, 'booking_class_id'=>$booking_class_id));
	}
	/*
	Loads the teetime edit form
	*/
	function view_teetime($teetime_id=-1)
	{
$this->benchmark->mark('start_view_teetime');
		$this->load->model('customer');
		$this->load->model('Customer_credit_card');
$this->benchmark->mark('accessing_session');
		$data['teesheet_holes'] = $this->session->userdata('holes');
$this->benchmark->mark('start_teetime_get_info');
		$data['teetime_info']=$this->teetime->get_info($teetime_id);
		$data['customer_info'] = array();
$this->benchmark->mark('start_customer_get_info');
		if ($data['teetime_info']->person_id != 0 && $data['teetime_info']->person_id != '')
			$data['customer_info'][$data['teetime_info']->person_id] = $this->Customer->get_info($data['teetime_info']->person_id, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_2 != 0 && $data['teetime_info']->person_id_2 != '')
			$data['customer_info'][$data['teetime_info']->person_id_2] = $this->Customer->get_info($data['teetime_info']->person_id_2, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_3 != 0 && $data['teetime_info']->person_id_3 != '')
			$data['customer_info'][$data['teetime_info']->person_id_3] = $this->Customer->get_info($data['teetime_info']->person_id_3, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_4 != 0 && $data['teetime_info']->person_id_4 != '')
			$data['customer_info'][$data['teetime_info']->person_id_4] = $this->Customer->get_info($data['teetime_info']->person_id_4, $this->session->userdata('course_id'));
		if ($data['teetime_info']->person_id_5 != 0 && $data['teetime_info']->person_id_5 != '')
			$data['customer_info'][$data['teetime_info']->person_id_5] = $this->Customer->get_info($data['teetime_info']->person_id_5, $this->session->userdata('course_id'));
		if ($data['teetime_info']->title == '') {
			$data['checkin_text'] = 'Walk In';
			$data['save_text'] = 'Reserve';
		}
		else {
			$data['checkin_text'] = 'Check In';
			$data['save_text'] = 'Update';
		}
$this->benchmark->mark('start_get_greenfees');
		$data['price_classes']=($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());
		$data['price_classes'] = array_merge(array('0'=>'Default Price'), $data['price_classes']);
		// FIX THE PRICE CLASS IDs
		if ($data['teetime_info']->price_class_1 != 0)
			$data['teetime_info']->price_class_1 = 'price_category_'.$data['teetime_info']->price_class_1;
		if ($data['teetime_info']->price_class_2 != 0)
			$data['teetime_info']->price_class_2 = 'price_category_'.$data['teetime_info']->price_class_2;
		if ($data['teetime_info']->price_class_3 != 0)
			$data['teetime_info']->price_class_3 = 'price_category_'.$data['teetime_info']->price_class_3;
		if ($data['teetime_info']->price_class_4 != 0)
			$data['teetime_info']->price_class_4 = 'price_category_'.$data['teetime_info']->price_class_4;
		if ($data['teetime_info']->price_class_5 != 0)
			$data['teetime_info']->price_class_5 = 'price_category_'.$data['teetime_info']->price_class_5;
		//echo $this->db->last_query().'<br/>';
		//echo 'person_id '.$data['teetime_info']->person_id;
		//print_r($this->Customer->get_info($data['teetime_info']->person_id));
		//$data['cross_booking'] = $this->;
$this->benchmark->mark('start_load_view');
		
		if ($this->config->item('simulator'))
			$this->load->view("teetimes/form_simulator",$data);
		else
			$this->load->view("teetimes/form",$data);
$this->benchmark->mark('end_view_teetime');
if ($this->config->item('display_benchmarks')) {
	echo '<br/>'.$this->benchmark->elapsed_time('start_view_teetime','accessing_session');
	echo '<br/>'.$this->benchmark->elapsed_time('accessing_session','start_teetime_get_info');
	echo '<br/>'.$this->benchmark->elapsed_time('start_teetime_get_info','start_customer_get_info');
	echo '<br/>'.$this->benchmark->elapsed_time('start_customer_get_info','start_get_greenfees');
	echo '<br/>'.$this->benchmark->elapsed_time('start_get_greenfees','start_load_view');
	echo '<br/>'.$this->benchmark->elapsed_time('start_load_view','end_view_teetime');
	echo '<br/>'.$this->benchmark->elapsed_time();
}

	}
	function credit_card_dropdown($person_id, $credit_card_id)
	{
		$this->load->model('Customer_credit_card');
		echo $this->Customer_credit_card->dropdown($person_id, $credit_card_id);
	}
	function send_confirmation_email($teetime_id = ''){
		if ($teetime_id != '')
		{
			$person_array = array();
			$this->load->model('Course');
			$course_info = $this->Course->get_info($this->session->userdata('course_id'));
			$teetime_data = $this->teetime->get_info($teetime_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_2);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_3);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_4);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_5);
			// Send emails to each golfer
			foreach ($person_array as $person_info)
			{
				if ($person_info->email)
				{
					$email_data = array(
						'person_id'=>$teetime_data->person_id,
						'course_name'=>$this->session->userdata('course_name'),
						'course_phone'=>$course_info->phone,
						'course_id'=>$this->session->userdata('course_id'),
						'first_name'=>$person_info->first_name,
						'booked_date'=>date('n/j/y', strtotime($teetime_data->start+1000000)),
						'booked_time'=>date('g:ia', strtotime($teetime_data->start+1000000)),
						'booked_holes'=>$teetime_data->holes,
						'booked_players'=>$teetime_data->player_count,
						'TTID'=> $teetime_data->TTID
					);
					$this->teetime->send_confirmation_email($person_info->email, 'Tee Time Reservation Confirmation', $email_data, $course_info->email, $this->session->userdata('course_name'));
				}
			}
		}
		echo json_encode(array());
	}
    function print_teesheet($date)
	{
        $this->teesheet->print_teesheet($date);
	}
	function get_message()
	{
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache');

		echo "id: 12345".PHP_EOL;
		echo "retry: 20000" . PHP_EOL;
		echo "data: message3".PHP_EOL;
		echo PHP_EOL;
		ob_flush();
		flush();
	}
	function generate_stats()
	{
		$results = $this->Dash_data->fetch_tee_sheet_data();
		echo json_encode($results);
	}
	
	/*
	 * BEGIN CREDIT CARD FUNCTIONS
	 */
	function open_add_credit_card_window($tee_time_id, $person_id) {
		$course_id = $this->session->userdata('course_id');

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('isSave', 'true')
					   ->send();
			if ($session->id)
			{
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array(
					'user_message'=>$user_message, 
					'return_code'=>$return_code, 
					'session'=>$session, 
					'url' => 'index.php/teesheets/card_captured/'.$tee_time_id.'/'.$person_id,
					'tee_time_card' => true);
				$this->load->view('sales/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('sales/cant_load', $data);
			}
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
			$this->load->library('Hosted_checkout_2');

			$HC = new Hosted_checkout_2();
			$HC->set_default_swipe('Manual');
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			$HC->set_response_urls('teesheets/card_captured/'.$tee_time_id.'/'.$person_id, 'credit_cards/process_cancelled');				
		
			$initialize_results = $HC->initialize_payment('1.00','0.00','PreAuth','POS','Recurring');
			//print_r($initialize_results);
			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'PreAuth','frequency'=>'Recurring'));
				$this->session->set_userdata('invoice', $invoice);

				$user_message = (string)$initialize_results->Message;
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>'$1.00 authorization in order to save the credit card', 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('sales/hc_pos_iframe.php', $data);
			}
		}
	}
	
	function card_captured($tee_time_id, $person_id) {
		$approved = false;
		$this->load->model('Customer_credit_card');
		$this->load->model('sale');

		if($this->config->item('ets_key')){

			$response = $this->input->post('response');
			$ets_response = json_decode($response);
			//print_r($ets_response);
			$transaction_time = date('Y-m-d H:i:s', strtotime($ets_response->created));
			// VERIFYING A POSTed TRANSACTION
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));
			$session_id = $payment->get("session_id");

			//$transaction_id = $ets_response->transactions->id;

			$account_id = $ets_response->customers->id;
			$payment->set("action", "verify")
				->set("sessionID", $ets_response->id)
				//->set("transactionID", $transaction_id)
				->set('accountID', $account_id);

			$verify = $payment->send();
			// Convert card type to match mercury card types
			if ((string)$ets_response->customers->cardType != 'UNKNOWN') {
				$ets_card_type = $ets_response->customers->cardType;
				$card_type = '';
				switch($ets_card_type){
					case 'MasterCard':
						$card_type = 'M/C';
					break;
					case 'Visa':
						$card_type = 'VISA';
					break;
					case 'Discover':
						$card_type = 'DCVR';
					break;
					case 'American Express':
						$card_type = 'AMEX';
					break;
					case 'Diners':
						$card_type = 'DINERS';
					break;
					case 'JCB':
						$card_type = 'JCB';
					break;
					default:
						$card_type = $ets_card_type;
					break;
				}
				$masked_account = str_replace('*', '', (string) $verify->customers->cardNumber);
				$expiration = DateTime::createFromFormat('my', $ets_response->customers->cardExpiration);
			}
			else {
				$card_type = 'Bank Acct';
				$masked_account = str_replace('*', '', (string) $verify->customers->accountNumber);
				$expiration = DateTime::createFromFormat('my', date('my', strtotime('+2 years')));
			}

			if((string)$ets_response->status == 'success'){
				$credit_card_data = array(
					'course_id' 		=> $this->session->userdata('course_id'),
					'card_type' 		=> $card_type,
					'masked_account' 	=> $masked_account,
					'cardholder_name' 	=> '',
					'customer_id'		=> $person_id,
					'token' 			=> (string) $ets_response->customers->id,
					'expiration' 		=> $expiration->format('Y-m-01')
				);
				$this->Customer_credit_card->save($credit_card_data);

				$approved = true;
			}

		}else if($this->config->item('mercury_id')){

			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			//$data = $HC->payment_made();
			$payment_id = $this->session->userdata('payment_id');
			$this->session->unset_userdata('payment_id');
			$HC->set_payment_id($payment_id);
			$verify_results = $HC->verify_payment();
			//echo 'about to complete<br/>';
			$HC->complete_payment();
			//echo 'completed <br/>';
			$invoice = $this->session->userdata('invoice');
			$this->session->unset_userdata('invoice');

			//Add card to billing_credit_cards
			$credit_card_data = array(
				'course_id'=>$this->session->userdata('course_id'),
				'customer_id'=>$person_id,
				'token'=>(string)$verify_results->Token,
				'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
				'card_type'=>(string)$verify_results->CardType,
				'masked_account'=>(string)$verify_results->MaskedAccount,
				'cardholder_name'=>(string)$verify_results->CardholderName
			);
			$this->Customer_credit_card->save($credit_card_data);
			//echo 'saved credit card data';
			//Update credit card payment data
			$payment_data = array (
				'course_id'=>$this->session->userdata('course_id'),
				'mercury_id'=>$this->config->item('mercury_id'),
				'tran_type'=>(string)$verify_results->TranType,
				'amount'=>(string)$verify_results->Amount,
				'auth_amount'=>(string)$verify_results->AuthAmount,
				'card_type'=>(string)$verify_results->CardType,
				'frequency'=>'Recurring',
				'masked_account'=>(string)$verify_results->MaskedAccount,
				'cardholder_name'=>(string)$verify_results->CardholderName,
				'ref_no'=>(string)$verify_results->RefNo,
				'operator_id'=>(string)$verify_results->OperatorID,
				'terminal_name'=>(string)$verify_results->TerminalName,
				'trans_post_time'=>(string)$verify_results->TransPostTime,
				'auth_code'=>(string)$verify_results->AuthCode,
				'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
				'payment_id'=>$payment_id,
				'acq_ref_data'=>(string)$verify_results->AcqRefData,
				'process_data'=>(string)$verify_results->ProcessData,
				'token'=>(string)$verify_results->Token,
				'response_code'=>(int)$verify_results->ResponseCode,
				'status'=>(string)$verify_results->Status,
				'status_message'=>(string)$verify_results->StatusMessage,
				'display_message'=>(string)$verify_results->DisplayMessage,
				'avs_result'=>(string)$verify_results->AvsResult,
				'cvv_result'=>(string)$verify_results->CvvResult,
				'tax_amount'=>(string)$verify_results->TaxAmount,
				'avs_address'=>(string)$verify_results->AVSAddress,
				'avs_zip'=>(string)$verify_results->AVSZip,
				'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
				'customer_code'=>(string)$verify_results->CustomerCode,
				'memo'=>(string)$verify_results->Memo
			);
			$this->sale->update_credit_card_payment($invoice, $payment_data);
			//echo 'saved credit card payment data';

			if ($payment_data['response_code'] === 0 && $payment_data['status'] === "Approved"){
				$approved = true;
			}
		}

		$credit_cards =$this->Customer_credit_card->get($person_id);
		//$person_info = $person_id ? $this->Customer->get_info($person_id) : $this->Customer->get_info($data['person_id']);

		$data = array ('controller_name'=>'teesheets', 'card_captured'=>true, 'course_id'=>$credit_card_data['course_id'], 'open_billing_box'=>'true', 'credit_card_id'=>$credit_card_data['credit_card_id'], 'credit_card_name'=>$credit_card_data['card_type'].' '.$credit_card_data['masked_account'], 'credit_cards'=>$credit_cards);
		if ($approved){
			$this->load->view('customers/card_captured', $data);
		}
	}
	function add_new_charge($cc_id) {
		$amount = $this->input->post('amount');
		$description = $this->input->post('description');
		$subscription = $this->input->post('subscription');
		$start_date = $this->input->post('start_date');
		$month = $this->input->post('month');
		$day = $this->input->post('day');
		
		$data = array (
			'credit_card_id'=>$cc_id,
			'amount'=>$amount,
			'description'=>$description,
			'subscription'=>$subscription,
			'start_data'=>date('Y-m-d', strtotime($start_date)),
			'month'=>$month,
			'day'=>$day
		);
		//echo 'about to save';
		$this->load->model('Customer_billing');
		if ($this->Customer_billing->save($data))
		echo $this->db->last_query();
			echo $this->Customer_billing->create_billing_row($data);
		
		echo false;
	}
	function view_charge_card($tee_sheet_id = '')
	{
		$data = array();
		// GET DATA CONCERNING PREVIOUS CHARGES
		$data['charges'] = $this->sale->get_balance_item_sales('tee_time_charge', $tee_sheet_id);
		$this->load->view('teesheets/charge_credit_card', $data);
	}
	function charge_card()
	{
		$error_message = array();
		$credit_card_id = $this->input->post('card_to_charge');
		$amount = $this->input->post('charge_amount');
		$tee_time_id = $this->input->post('tee_time_id');
		$person_id = $this->input->post('charge_person_id');
		$this->load->model('Customer_credit_card');
		$this->load->helper('general');
		//$this->load->library("sale_lib");
		// VALIDATE VALUES
		if (!$credit_card_id)
			$error_message[] = 'No credit card id received.';
		if (!$tee_time_id)
			$error_message[] = 'No tee time id received.';
		if (!$amount || $amount <= 0 || !is_numeric($amount))
			$error_message[] = 'Invalid amount, please enter a dollar amount to charge.';
		if (count($error_message) > 0)
		{
			echo json_encode(array('success'=>false,'error_array'=>$error_message));
			return;
		}
		// CHARGE THE CARD
		$amount = to_currency_no_money($amount);
		$charge_data = array(
			'credit_card_id'=>$credit_card_id,
			'course_id'=>$this->session->userdata('course_id')
		);
		$charged = $this->Customer_credit_card->charge($charge_data, $amount);
		
		if ($charged)
		{
			// CLEAR OUT THE CART
			$this->sale_lib->clear_all();
			// MAKE SURE CHARGE ITEM EXISTS
			$item_id = $this->sale->get_balance_item('tee_time_charge');
			// ADD ITEM TO SALE
			if(!$this->sale_lib->add_item($item_id,1,0, $amount) || !$this->sale_lib->add_item_to_basket($item_id,1,0, $amount))
				$error_message[] = lang('sales_unable_to_add_item');
	 		// ADD CUSTOMER TO SALE
			if ($person_id && !$this->Customer->exists($person_id, $this->session->userdata('course_id')))
				$error_message[] = lang('sales_unable_to_add_customer').$this->db->last_query();
			// ADD CUSTOMER
			$this->sale_lib->set_customer($person_id);
			// ADD TEE TIME TO SALE
			$this->sale_lib->set_teetime($tee_time_id);
			// ADD PAYMENT TO SALE
			if( !$this->sale_lib->add_payment( $charge_data['payment_type'], $amount, $amount, false, $charge_data['credit_card_payment_id']) )
				$error_message[] = lang('sales_unable_to_add_payment');
			// COMPLETE SALE
			if (count($error_message) == 0)
			{
				$sale_id = complete_sale(NULL, false, true);
				if ($sale_id > 0)
				{
					echo json_encode(array('success'=>true,'sale_id'=>$sale_id));
					return;
				}
			}
		}
		else {
			$error_message[] = 'Was not able to charge the card';
		}

		// IF WE HAVE ERRORS, WE END UP HERE
		echo json_encode(array('success'=>false,'error_array'=>$error_message));
	}
	/*
	 * END CREDIT CARD FUNCTIONS
	 */
	
        function get_standby()
        {
            
            $id = $this->input->post('id');
            
            $this->load->model('teetime');
            $url['urlData'] = ($this->standby->get_by_id2($id));
            
            echo json_encode($url);
            
            
        }
}
?>
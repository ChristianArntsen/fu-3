<?php
class Sendgrid
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();
	}
	function check_bounce($email) {
		$params = array (
			'email'=>$email
		);
		$result = json_decode($this->request('check_bounce', $params));
		return count($result) > 0;		
	}
	function delete_bounce($email) {
		$params = array (
			'email'=>$email
		);
		$result = json_decode($this->request('delete_bounce', $params));
		return true;
	}
	function request($type, $params)
	{
		$parameters = "api_user=foreup&api_key=GolfCompete#17";
		foreach ($params AS $index => $param)
		{
			$parameters .= "&{$index}={$param}";
		}
				
		switch ($type) {
			case 'check_bounce':
				$url = 'https://api.sendgrid.com/api/bounces.get.json';
        		break;
			case 'delete_bounce':
				$url = 'https://api.sendgrid.com/api/bounces.delete.json';
        		break;
		}
		$ch2=curl_init($url);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch2, CURLOPT_POST,1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS,$parameters);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 30);
	    curl_setopt($ch2, CURLOPT_TIMEOUT, 300);
		$data2 = curl_exec($ch2);
		//print_r($data2);
		return $data2;
	}
}
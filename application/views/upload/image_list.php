<ul class="files">
<?php if(!empty($files)){ ?>
<?php foreach($files as $file){ ?>
	<li id="file_<?php echo $file['image_id']; ?>" class="file" data-image-id="<?php echo $file['image_id']; ?>" title="View this image">
		<div class="thumb">
			<img src="<?php echo $thumb_url.'/'.$file['filename']; ?>?ts=<?php echo strtotime($file['date_updated']); ?>" />
		</div>
		<div class="details">
			<h4><?php echo $file['label']; ?></h4>
			<span><?php echo date('M jS, Y', strtotime($file['date_created'])); ?></span>
		</div>
	</li>
<?php } } else { ?>
	<li class="empty">No images found</li>
<?php } ?>
</ul>
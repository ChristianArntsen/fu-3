<style>
	#printers th, #printers td {
		padding:5px 20px;
		text-align:left;
	}
	#printers .centered {
		text-align:center;
	}
	.edit_printer, .delete_printer, #add_printer {
		color:blue;
		cursor:pointer;
	}
</style>
<div style='min-height:400px;'>
<table id='printers'>
	<tbody>
		<tr>
			<th valign="top">Label</th>
			<th valign="top">IP Address</th>
			<th valign="top" class='centered'><span id='add_printer'>Add Printer</span></th>
		</tr>
		<?php foreach($printers as $printer) { ?>
			<tr id='printer_<?=$printer['printer_id'];?>'>
				<td><input type='text' value='<?php echo $printer['label']; ?>' disabled/></td>
				<td><input type='text' value='<?php echo $printer['ip_address']; ?>' disabled/></td>
				<td class='centered'><span class='edit_printer'>Edit</span> - <span class='delete_printer'>Delete</span></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
</div>
<script>
	$(document).ready(function(){
		initialize_printer_links();
		$('#add_printer').click(function(){
			printer.add();
		});
	});
	function initialize_printer_links()
	{
		$('.edit_printer').click(function(e){
			e.preventDefault();
			// IF IT'S EDITABLE, WE SAVE, OTHERWISE, 
			printer.edit(this);
		});
		$('.delete_printer').click(function(e){
			e.preventDefault();
			printer.remove(this);
		});
	}
	var printer = {
		remove : function(link){
			// AJAX CALL TO DELETE PRINTER
			
			// REMOVE PRINTER FROM LIST
			var row = $(link).closest("tr");
			console.dir(row);
			$(row).remove();
			
		},
		edit : function(link){
			// ENABLE ROW FOR EDITING
			var inputs = $(link).closest("tr").children('input').each(function(){
				$(this).removeAttr('disabled');
			});
			console.dir(inputs);
		},
		save : function(){
			var inputs = $(link).closest("tr").children('input').each(function(){
				$(this).attr('disabled', 'disabled');
			});
		},
		add : function(){
			$('#printers tbody').append("<tr id='new_printer'>"+
				"<td><input type='text' value=''/></td>"+
				"<td><input type='text' value=''/></td>"+
				"<td class='centered'><span class='edit_printer'>Save</span> - <span class='delete_printer'>Delete</span></td>"+
			"</tr>");
			initialize_printer_links();        
		}
	};
</script>
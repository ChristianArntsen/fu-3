<?php
//print_r($teesheets->result());
//print_r($teetime_prices);
//print_r($teetime_type);
?>

<span class="small_note">*Note - Early Bird, Morning, Afternoon, Twilight and Super-Twilight hours may be set in the hours/booking tab.</span>
        <div>
        	<span class="small_note">Green Fee Department: </span>
        	<? echo form_input(array(
                'name'=>"teetime_department",
                'id'=>"teetime_department",
                'value'=>(isset($teetime_department[0]) && isset($teetime_department[0]['department']))?$teetime_department[0]['department']:''));
			?>
       		<span class="small_note"> Tax Rate: </span>
        	<? echo form_input(array(
                'name'=>"teetime_tax_rate",
                'id'=>"teetime_tax_rate",
                'value'=>(isset($teetime_tax_rate[0]) && isset($teetime_tax_rate[0]['percent']))?$teetime_tax_rate[0]['percent']:''));
			?>
       		<span class="small_note">Cart Department: </span>
        	<? echo form_input(array(
                'name'=>"cart_department",
                'id'=>"cart_department",
                'value'=>(isset($cart_department[0]) && isset($cart_department[0]['department']))?$cart_department[0]['department']:''));
			?>
       		<span class="small_note"> Cart Tax Rate: </span>
        	<? echo form_input(array(
                'name'=>"cart_tax_rate",
                'id'=>"cart_tax_rate",
                'value'=>(isset($cart_tax_rate[0]) && isset($cart_tax_rate[0]['percent']))?$cart_tax_rate[0]['percent']:''));
			 ?>
        </div>
        <select id='price_grid_select' name='price_grid_select'>
        <?php
    	foreach($teesheets->result() as $teesheet)
		{
			echo "<option value='$teesheet->teesheet_id'>$teesheet->title</option>";
		}
        ?>
        </select>
        <div id='green_fee_prices'>
	            <?php
	            foreach($teesheets->result() as $teesheet)
				{
				?>
				<table id='price_grid_<?php echo $teesheet->teesheet_id; ?>' class='price_grid' style="display: none;">
	        	<thead>
	        		<tr class="field_row default_price_class_row">
	                    <td>Default Price</td>
	                    <td class="price_cell"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_1_'.$teesheet->teesheet_id,'value'=>1,'checked'=>(1 == $teesheet->default_price_class || !$this->session->userdata('default_price_class'))));	?> </td>
	                    <td class="price_cell early_bird_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_2_'.$teesheet->teesheet_id,'value'=>2,'checked'=>2 == $teesheet->default_price_class));?></td>
	                    <td class="price_cell morning_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_3_'.$teesheet->teesheet_id,'value'=>3,'checked'=>3 == $teesheet->default_price_class));?> </td>
	                    <td class="price_cell afternoon_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_4_'.$teesheet->teesheet_id,'value'=>4,'checked'=>4 == $teesheet->default_price_class));?></td>
	                    <td class="price_cell twilight_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_5_'.$teesheet->teesheet_id,'value'=>5,'checked'=>5 == $teesheet->default_price_class));?></td>
	                    <td class="price_cell super_twilight_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_6_'.$teesheet->teesheet_id,'value'=>6,'checked'=>6 == $teesheet->default_price_class));?></td>
	                    <td class="price_cell holiday_col"><?php echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_7_'.$teesheet->teesheet_id,'value'=>7,'checked'=>7 == $teesheet->default_price_class));?></td>
	                    <?php for($j = 8; $j <= 50; $j++) {?>
		                    <td class="price_cell"><? echo form_radio(array('name'=>'default_price_class_'.$teesheet->teesheet_id,'id'=>'default_price_class_'.$j.'_'.$teesheet->teesheet_id,'value'=>$j,'checked'=>$j == $teesheet->default_price_class)); ?></td>
	                 	<?php }?>
	                </tr>
	                <tr class="field_row ">
	                    <td>9 Holes</td>
	                    <td>Regular</td>
	                    <td class="price_cell early_bird_col">Early Bird</td>
	                    <td class="price_cell morning_col">Morning</td>
	                    <td class="price_cell afternoon_col">Afternoon</td>
	                    <td class="price_cell twilight_col">Twilight</td>
	                    <td class="price_cell super_twilight_col">Super Twilight</td>
	                    <td class="price_cell holiday_col">Holiday</td>
	                    <?php for($j = 8; $j <= 50; $j++) {?>
		                    <td class="price_cell">
		                       <?
		                       	$price_category = 'price_category_'.$j;
								$value = (isset($teetime_type[$teesheet->teesheet_id]))?$teetime_type[$teesheet->teesheet_id]->$price_category:'';
		                       	echo form_input(array(
		                           'tabindex'=>$j,
								   'name'=>'price_category_'.$j.'_'.$teesheet->teesheet_id,
		                           'id'=>'price_category_'.$j.'_'.$teesheet->teesheet_id,
		                           'class'=>'price_label price_class price_header_'.$j,
		                           'value'=>$value,
		                           'disabled'=>'disabled'
		                       ));?>
		                    </td>
	                 	<?php }?>
	                </tr>
	            </thead>
	            <tbody>
	                <?php for ($i = 1; $i <=9;$i++) {
	                		  if ($no_weekend && ($i == 2 || $i == 7 || $i == 4 || $i == 9))
								  continue;

	                          echo "<tr class='field_row'>";
	                          if ($i == 5) {
	                              echo "<td>18 Holes</td><td></td>";
	                          }
	                          else {
	                              $index = ($i > 5)? $i-1:$i;
	                              for($j = 1; $j <= 51; $j++) {
	                              	  $jindex = $j-1;
	                                  if ($j == 1){
	                                      if ($i == 1 || $i == 6)
	                                          $label = 'Weekday Cart';
										  else if ($i == 2 || $i == 7)
										  	  $label = 'Weekend Cart';
	                                      else if ($i == 3 || $i == 8)
	                                          $label = 'Weekday';
	                                      else
	                                          $label = 'Weekend';

	                                      echo "<td class='tee_time_label'>$label</td>";
	                                  }
	                                  else {
	                                      if ($jindex == 2)
	                                          $extra_class = 'early_bird_col';
										  else if ($jindex == 3)
	                                          $extra_class = 'morning_col';
	                                      else if ($jindex == 4)
	                                          $extra_class = 'afternoon_col';
										  else if ($jindex == 5)
	                                          $extra_class = 'twilight_col';
	                                      else if ($jindex == 6)
	                                          $extra_class = 'super_twilight_col';
	                                      else if ($jindex == 7)
	                                          $extra_class = 'holiday_col';
	                                      else
	                                          $extra_class = '';
	                                      echo "<td class='price_cell $extra_class'>";
										  $price_category = 'price_category_'.$jindex;
										  $price = (isset($teetime_prices[$teesheet->teesheet_id]))?$teetime_prices[$teesheet->teesheet_id]["{$this->session->userdata('course_id')}_{$index}"]->$price_category:'0.00';
	                                      echo form_input(array(
	                                      	'tabindex'=>$jindex,
	                                        'name'=>"{$teesheet->teesheet_id}_{$jindex}_{$index}",
	                                        'id'=>"{$teesheet->teesheet_id}_{$jindex}_{$index}",
	                                        'class'=>"price_label price_box price_col_{$jindex} price_category_{$jindex}_{$teesheet->teesheet_id}",
	                                        'value'=>$price,
	                                        'disabled'=>'disabled'
										  ));
	                                      echo "</td>";
	                                  }
	                              }
	                          }
	                          echo "</tr>";
	                      }
	                ?>
	            </tbody>
	            </table>
	            <?php } ?>
			<table style="width: 250px; margin-top: 15px;">
				<thead>
					<tr>
						<th style="text-align: left; padding-bottom: 5px;">Price Category</th>
						<th style="text-align: left; padding-bottom: 5px;">Color</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($price_categories as $category_id => $category_name){
					$category_id = explode('_', $category_id);
					$category_id = array_pop($category_id);
					$selected_color = '';
					if(!empty($price_colors[$category_id])){
						$selected_color = $price_colors[$category_id];
					}
					?>
					<tr>
						<td><?php print_r($category_name); ?></td>
						<td><input name="price_colors[<?php echo $category_id; ?>]" value="<?php echo $selected_color; ?>" class="color-picker" type="text" /></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
	    </div>
	<script type='text/javascript'>
		$(document).ready(function(){
			var selectedPrice = '#price_grid_' + $('#price_grid_select').val();
			$(selectedPrice).show().find('input').attr('disabled', null);
			$('#price_grid_select').change(function(e){
				$(selectedPrice).hide().find('input').attr('disabled', 'disabled');
				selectedPrice = '#price_grid_' + $('#price_grid_select').val();
				$(selectedPrice).show().find('input').attr('disabled', null);
			});
		})
	</script>

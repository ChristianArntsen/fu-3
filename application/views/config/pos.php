<!--div class="field_row clearfix">	
                <?php echo form_label(lang('config_receipt_printer').':', 'receipt_printer',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    	<?php echo form_dropdown('receipt_printer', 
                         array()
                         , $this->config->item('receipt_printer') ? $this->config->item('receipt_printer') : '');
                        ?>
                    </div>
            </div-->
            <div class="field_row clearfix">	
                <?php echo form_label(lang('config_default_tax_rate_1').':', 'default_tax_1_rate',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'default_tax_1_name',
                            'id'=>'default_tax_1_name',
                            'size'=>'10',
                            'value'=>$this->config->item('default_tax_1_name')!==FALSE ? $this->config->item('default_tax_1_name') : lang('items_sales_tax_1')));?>

                    <?php echo form_input(array(
                            'name'=>'default_tax_1_rate',
                            'id'=>'default_tax_1_rate',
                            'size'=>'4',
                            'value'=>$this->config->item('default_tax_1_rate')));?>%
                    </div>
            </div>

            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_default_tax_rate_2').':', 'default_tax_1_rate',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'default_tax_2_name',
                            'id'=>'default_tax_2_name',
                            'size'=>'10',
                            'value'=>$this->config->item('default_tax_2_name')!==FALSE ? $this->config->item('default_tax_2_name') : lang('items_sales_tax_2')));?>

                    <?php echo form_input(array(
                            'name'=>'default_tax_2_rate',
                            'id'=>'default_tax_2_rate',
                            'size'=>'4',
                            'value'=>$this->config->item('default_tax_2_rate')));?>%
                            <?php echo form_checkbox('default_tax_2_cumulative', '1', $this->config->item('default_tax_2_cumulative') ? true : false);  ?>
                        <span class="cumulative_label">
                            <?php echo lang('common_cumulative'); ?>
                        </span>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_auto_gratuity').':', 'auto_gratuity',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'auto_gratuity',
                            'id'=>'auto_gratuity',
                            'size'=>'4',
                            'value'=>$this->config->item('auto_gratuity')));?>%
                        <span class="cumulative_label">
                            (<?php echo lang('config_food_and_beverage'); ?>)
                        </span>
                    </div>
            </div>
            <!--div class="field_row clearfix">   
                <?php echo form_label(lang('config_unit_price_includes_tax').' :', 'unit_price_includes_tax',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_checkbox(array(
                    'name'=>'unit_price_includes_tax',
                    'id'=>'unit_price_includes_tax',
                    'value'=>'1',
                    'checked'=>$this->config->item('unit_price_includes_tax')));?>
                </div>
            </div-->

			<div class="field_row clearfix">	
                <?php echo form_label(lang('config_customer_credit_nickname').':', 'customer_credit_nickname',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'customer_credit_nickname',
                            'id'=>'customer_credit_nickname',
                            'size'=>'20',
                            'value'=>$this->config->item('customer_credit_nickname')));?>

                    </div>
            </div>

			<div class="field_row clearfix">	
                <?php echo form_label(lang('config_credit_department_category').':', 'customer_credit_department',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    	<?php echo form_radio(array(
                            'name'=>'credit_department_category',
                            'id'=>'credit_department',
                            'value'=>'0',
                            'checked'=>$this->config->item('credit_category')==0));?>
                    <?php echo form_dropdown('department', $departments, $this->config->item('credit_department_name'), 'id="department"');?>

                    </div>
            </div>
			<div class="field_row clearfix">	
                <?php echo form_label('', 'customer_credit_department',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    	<?php echo form_radio(array(
                            'name'=>'credit_department_category',
                            'id'=>'credit_category',
                            'value'=>'1',
                            'checked'=>$this->config->item('credit_category')==1));?>
                    <?php echo form_dropdown('category', $categories, $this->config->item('credit_category_name'), 'id="category"');?>

                    </div>
            </div>

			<div class="field_row clearfix">	
                <?php echo form_label(lang('config_member_balance_nickname').':', 'member_balance_nickname',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'member_balance_nickname',
                            'id'=>'member_balance_nickname',
                            'size'=>'20',
                            'value'=>$this->config->item('member_balance_nickname')));?>

                    </div>
            </div>

            
            <!--div class="field_row clearfix">	
            <?php echo form_label(lang('config_currency_symbol').':', 'currency_symbol',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'currency_symbol',
                            'id'=>'currency_symbol',
                            'value'=>$this->config->item('currency_symbol')));?>
                    </div>
            </div-->
			<div id='printing_options'>
	            <div class="field_row clearfix">	
	            <?php echo form_label(lang('config_print_after_sale').':', 'print_after_sale',array('class'=>'wide')); ?>
	                    <div class='form_field'>
	                    <?php echo form_checkbox(array(
	                            'name'=>'print_after_sale',
	                            'id'=>'print_after_sale',
	                            'value'=>'1',
	                            'checked'=>$course_info->print_after_sale));?>
	                    </div>
	            </div>
	            <div class="field_row clearfix">	
	            		<a href='index.php/config/view_printers' id='manage_printers'>Manage Printers</a>
	            		<script>
	            			$(document).ready(function(){
	            				$('#manage_printers').colorbox2({'title':'Manage Printers','width':650});
	            			});
	            		</script>
	                    <div class='form_field'>
	                    
	                    </div>
	            </div>
	            <div class="field_row clearfix">	
	            <?php echo form_label(lang('config_use_webprnt').':', 'webprnt',array('class'=>'wide')); ?>
	                    <div class='form_field'>
	                    <?php echo form_checkbox(array(
	                            'name'=>'webprnt',
	                            'id'=>'webprnt',
	                            'value'=>'1',
	                            'checked'=>$course_info->webprnt));?>
	                    </div>
	            </div>
	            <div class="field_row clearfix">	
                <?php echo form_label(lang('config_webprnt_ip').':', 'webprnt_ip',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'webprnt_ip',
                            'id'=>'webprnt_ip',
                            'size'=>'15',
                            'value'=>$course_info->webprnt_ip));
                    ?>
                    </div>
            </div>
			      <div class="field_row clearfix">	
                <?php echo form_label(lang('config_webprnt_hot_ip').':', 'webprnt_hot_ip',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'webprnt_hot_ip',
                            'id'=>'webprnt_hot_ip',
                            'size'=>'15',
                            'value'=>$course_info->webprnt_hot_ip));
                    ?>
                    </div>
            </div>
			      <div class="field_row clearfix">	
                <?php echo form_label(lang('config_webprnt_cold_ip').':', 'webprnt_cold_ip',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'webprnt_cold_ip',
                            'id'=>'webprnt_cold_ip',
                            'size'=>'15',
                            'value'=>$course_info->webprnt_cold_ip));
                    ?>
                    </div>
            </div>
			<div class="field_row clearfix">	
	            <?php echo form_label(lang('config_print_credit_card_receipt').':', 'print_credit_card_receipt',array('class'=>'wide')); ?>
	                    <div class='form_field'>
	                    <?php echo form_checkbox(array(
	                            'name'=>'print_credit_card_receipt',
	                            'id'=>'print_credit_card_receipt',
	                            'value'=>'1',
	                            'checked'=>$course_info->print_credit_card_receipt));?>
	                    </div>
	            </div>
	            <div class="field_row clearfix">	
	            <?php echo form_label(lang('config_print_sales_receipt').':', 'print_sales_receipt',array('class'=>'wide')); ?>
	                    <div class='form_field'>
	                    <?php echo form_checkbox(array(
	                            'name'=>'print_sales_receipt',
	                            'id'=>'print_sales_receipt',
	                            'value'=>'1',
	                            'checked'=>$course_info->print_sales_receipt));?>
	                    </div>
	            </div>
	            <div class="field_row clearfix">	
	            <?php echo form_label(lang('config_print_tip_line').':', 'print_tip_line',array('class'=>'wide')); ?>
	                    <div class='form_field'>
	                    <?php echo form_checkbox(array(
	                            'name'=>'print_tip_line',
	                            'id'=>'print_tip_line',
	                            'value'=>'1',
	                            'checked'=>$course_info->print_tip_line));?>
	                    </div>
	            </div>
	            <div class="field_row clearfix">        
			    <?php echo form_label(lang('config_print_two_receipts').':', 'print_two_receipts',array('class'=>'wide')); ?>
			            <div class='form_field'>
			            <?php echo form_checkbox(array(
			                    'name'=>'print_two_receipts',
			                    'id'=>'print_two_receipts',
			                    'value'=>'1',
			                    'checked'=>$course_info->print_two_receipts));?>
			           </div>
			    </div>
			    <div class="field_row clearfix">        
			    <?php echo form_label(lang('config_print_two_signature_slips').':', 'print_two_signature_slips',array('class'=>'wide')); ?>
			            <div class='form_field'>
			            <?php echo form_checkbox(array(
			                    'name'=>'print_two_signature_slips',
			                    'id'=>'print_two_signature_slips',
			                    'value'=>'1',
			                    'checked'=>$course_info->print_two_signature_slips));?>
			           </div>
			    </div>
			    <div class="field_row clearfix">        
			    <?php echo form_label(lang('config_print_two_receipts_other').':', 'print_two_receipts_other',array('class'=>'wide')); ?>
			            <div class='form_field'>
			            <?php echo form_checkbox(array(
			                    'name'=>'print_two_receipts_other',
			                    'id'=>'print_two_receipts_other',
			                    'value'=>'1',
			                    'checked'=>$course_info->print_two_receipts_other));?>
			           </div>
			    </div>
			    <div class="field_row clearfix">        
			    <?php echo form_label(lang('config_cash_drawer_on_cash').':', 'cash_drawer_on_cash',array('class'=>'wide')); ?>
			            <div class='form_field'>
			            <?php echo form_checkbox(array(
			                    'name'=>'cash_drawer_on_cash',
			                    'id'=>'cash_drawer_on_cash',
			                    'value'=>'1',
			                    'checked'=>$course_info->cash_drawer_on_cash));?>
			           </div>
			    </div>
	        </div>
            <script type='text/javascript'>
				$('#printing_options').expandable({title:'Printing Options:'});
			</script>
            <!--div class="field_row clearfix">	
            <?php echo form_label(lang('config_updated_printing').':', 'updated_printing',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'updated_printing',
                            'id'=>'updated_pringing',
                            'value'=>'1',
                            'checked'=>$this->config->item('updated_printing')));?>
                    </div>
            </div-->
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_after_sale_load').':', 'after_sale_load',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_radio(array(
                            'name'=>'after_sale_load',
                            'id'=>'after_sale_load_0',
                            'value'=>'0',
                            'checked'=>$course_info->after_sale_load==0));?>
                    <?php echo ($this->permissions->course_has_module('reservations') ? 'Reservations' : 'Tee Sheet');  ?>
                    <?php echo form_radio(array(
                            'name'=>'after_sale_load',
                            'id'=>'after_sale_load_1',
                            'value'=>'1',
                            'checked'=>$course_info->after_sale_load==1));?>
                    Sales
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_automatic_update').':', 'teesheet_updates_automatically',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'teesheet_updates_automatically',
                            'id'=>'teesheet_updates_automatically',
                            'value'=>'1',
                            'checked'=>$this->config->item('teesheet_updates_automatically')));?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_send_reservation_confirmations').':', 'send_reservation_confirmations',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'send_reservation_confirmations',
                            'id'=>'send_reservation_confirmations',
                            'value'=>'1',
                            'checked'=>$this->config->item('send_reservation_confirmations')));?>
                    </div>
            </div>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_auto_split_teetimes').':', 'auto_split_teetimes',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'auto_split_teetimes',
                            'id'=>'auto_split_teetimes',
                            'value'=>'1',
                            'checked'=>$this->config->item('auto_split_teetimes')));?>
                    </div>
            </div>
            
			<div class="field_row clearfix">	
                <?php echo form_label(lang('config_track_cash').':', 'track_cash',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'track_cash',
                            'id'=>'track_cash',
                            'value'=>'1',
                            'checked'=>$course_info->track_cash));?>
                    </div>
            </div>
            <div class="field_row clearfix">	
                <?php echo form_label(lang('config_blind_close').':', 'blind_close',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'blind_close',
                            'id'=>'blind_close',
                            'value'=>'1',
                            'checked'=>$this->config->item('blind_close')));?>
                    </div>
            </div>
            <?php if ($this->permissions->course_has_module('food_and_beverage')) { ?>
            <div class="field_row clearfix">	
            <?php echo form_label(lang('config_fnb_login_required').':', 'fnb_login',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'fnb_login',
                            'id'=>'fnb_login',
                            'value'=>'1',
                            'checked'=>$this->config->item('fnb_login')));?>
                    </div>
            </div>
            <?php } ?>
            <div class="field_row clearfix">	
                <?php echo form_label(lang('config_separate_courses').':', 'separate_courses',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_checkbox(array(
                            'name'=>'separate_courses',
                            'id'=>'separate_courses',
                            'value'=>'1',
                            'checked'=>$this->config->item('separate_courses')));?>
                    </div>
            </div>
            <!--div class="field_row clearfix">    
                <?php echo form_label(lang('config_minimum_food_spend').':', 'minimum_food_spend',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_input(array(
                            'name'=>'minimum_food_spend',
                            'id'=>'minimum_food_spend',
                            'size'=>'20',
                            'value'=>$this->config->item('minimum_food_spend')));?>

                    </div>
            </div-->
            <div class="field_row clearfix">	
            <?php echo form_label(lang('common_return_policy').':', 'return_policy',array('class'=>'wide')); ?>
                    <div class='form_field'>
                    <?php echo form_textarea(array(
                            'name'=>'return_policy',
                            'id'=>'return_policy',
                            'rows'=>'4',
                            'cols'=>'30',
                            'value'=>$this->config->item('return_policy')));?>
                    </div>
            </div>
<script>
	$('#department').change(function(e){
		$('#credit_department').attr('checked', true);
	});
	$('#category').change(function(e){
		$('#credit_category').attr('checked', true);
	});
	$('#member_balance_nickname, #customer_credit_nickname').bind('keyup change', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		 	//Do something
			var box = $(this);
			box.val(box.val().replace(/'/g, '').replace(/"/g, ''));
	});
</script>            
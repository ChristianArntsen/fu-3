<?php 
$teetime_dept = '';
$teetime_tax = '';
$cart_dept = '';
$cart_tax = '';

if(!empty($teetime_department[0]['department'])){
	$teetime_dept = $teetime_department[0]['department'];
}
if(!empty($teetime_tax_rate[0]['percent'])){
	$teetime_tax = $teetime_tax_rate[0]['percent'];
}
if(!empty($cart_department[0]['department'])){
	$cart_dept = $cart_department[0]['department'];
}
if(!empty($cart_tax_rate[0]['percent'])){
	$cart_tax = $cart_tax_rate[0]['percent'];
}
?>
<style>
div.row {
	display: block;
	overflow: hidden;
	margin-top: 10px;
}

div.row label {
	display: block;
	margin-bottom: 4px;
	color: #666;
	font-size: 12px;
}

div.row > div.field {
	overflow: hidden;
	float: left;
	width: 175px;
}
</style>
<div class="teesheet_select">
	<div class="breadcrumbs">
		<span class="crumb_link">
			<a rel="crumb_teesheets" href="#">Teesheets</a>
		</span>
	</div>
</div>
<div class="columns_container">
	<div class="column_left">
		<p style="margin-top: 10px;">Select a teesheet on the right to set pricing options.</p>
		
		<h3 class="price-classes">Taxes</h3>
		<form id="seasonal_pricing_taxes">
		<div class="row">
			<div class="field">
				<label>Green Fee Department</label>
				<? echo form_input(array(
					'name'=>"teetime_department",
					'id'=>"teetime_department",
					'value'=>$teetime_dept));
				?>
			</div>
			<div class="field">
				<label>Tax Rate</label>
				<? echo form_input(array(
					'name'=>"teetime_tax_rate",
					'id'=>"teetime_tax_rate",
					'value'=>$teetime_tax));
				?> %
			</div>
		</div>
		<div class="row">
			<div class="field">
				<label>Cart Department</label>
				<? echo form_input(array(
					'name'=>"cart_department",
					'id'=>"cart_department",
					'value'=>$cart_dept));
				?>
			</div>
			<div class="field">
				<label>Tax Rate</label>
				<? echo form_input(array(
					'name'=>"cart_tax_rate",
					'id'=>"cart_tax_rate",
					'value'=>$cart_tax));
				 ?> %
			</div>
			<a class="terminal_button" id="save_seasonal_taxes" style="display: block; float: left; margin-top: 10px;" href="">Save Tax Settings</a>
			</form>
		</div>
		
		<h3 class="price-classes">
			Price Classes 
			<a href="#" id="add-price-class" class="new">Add Price Class</a>
		</h3>
		<ul class="price-classes">
		<?php foreach($price_classes as $price_class){ ?>
			<li class="pricing" data-price-class='<?php echo json_encode($price_class); ?>'>
				<span class="color" style="background-color: <?php if(empty($price_class['color'])){ echo 'none'; } else { echo $price_class['color']; } ?>"></span>
				<span class="name">
					<?=$price_class['name']?>
					<?php if($price_class['default'] == 1){ ?>
					<em style="font-weight: normal; font-size: 12px;">(Default)</em>
					<?php } ?>
				</span>
				<?php 
				$visibility = '';
				if($price_class['default'] == 1){
					$visibility = 'visibility: hidden;';
				} ?>
				<a href="#" style="<?php echo $visibility; ?>" class="delete">Delete</a>
				<a href="#" class="edit">Edit</a>
			</li>
		<?php } ?>
		</ul>
	</div>
	<div class="column_main">
		<div class="list-header">
			<h2>Teesheets</h2> <a class="terminal_button" id="new-teesheet" href="#" style="float: left;">New Teesheet</a>
		</div>
		<ul class="teesheets">
			<? foreach ($teesheets->result() as $teesheet){ ?>
			<li data-teesheet-id="<?=$teesheet->teesheet_id?>" class="teesheet">
				<h3><?=$teesheet->title?></h3>
				<p>Holes: <?=$teesheet->holes?></p>
				<? if ($teesheet->default){?><em>Default</em><? } ?>
			</li>
			<? } ?>			
		</ul>
	</div>
</div>
<script>
function price_class_row(data, editing){
	
	if(!data){
		data = {};
	}
	var row = $('<li></li>');
	row.data('price-class', data);
	
	if(!editing){
		if(!data.color){
			data.color = 'none';
		}
		row.prepend('<span class="color" style="background-color:'+data.color+'"></span>'+
			'<span class="name">' +data.name+ '</span><a href="#" class="edit">Edit</a>');
		
		if(data.default == 0){
			row.find('a.edit').before('<a href="#" class="delete">Delete</a>');
		}	
	
	}else{
		row.addClass('editing');
		row.prepend('<input name="color" value="'+ data.color +'" class="color color-picker" type="text" />' +
			'<input name="name" value="'+ data.name +'" />' +
			'<a class="terminal_button save" href="#">Save</a>' +
			'<a class="cancel" href="#">Cancel</a>');	
	}

	return row;
}
	
$(function(){
	
	$('#save_seasonal_taxes').off().on('click', function(e){
		var params = $('#seasonal_pricing_taxes').serialize();

		$.post('<?php echo site_url('config/save/seasonal_pricing_taxes'); ?>', params, function(response){
			if(response.success){
				set_feedback(response.message, 'success_message');
			}else{
				set_feedback('Error saving settings', 'error_message');
			}
		},'json');
		e.preventDefault();
	});
	
	$('#new-teesheet').on('click', function(e){
		var teesheet_id = $(this).data('teesheet_id');
		$.colorbox({href: "index.php/teesheets/seasonal_teesheet", width: 675});
		e.preventDefault();		
	});
	
	$('ul.price-classes').on('click', 'a.delete', function(e){
		if(!confirm('Are you sure you want to delete this price class?')){
			return false;
		}
		var row =  $(this).parent();
		var class_id = row.data('price-class').class_id;
		
		$.post('index.php/price_classes/delete/' + class_id, null, function(response){
			if(response.success){	
				row.fadeOut('fast');
				set_feedback(response.message,'success_message');
			}else{
				set_feedback(response.message,'error_message');			
			}
		},'json');
		
		e.preventDefault();
	});	
	
	$('ul.price-classes').on('click', 'a.save', function(e){
		var row = $(this).parents('li').first();
		
		var url = 'index.php/price_classes/save/';
		var data = {};
		data.name = $(this).siblings('input[name="name"]').val();
		data.color = $(this).siblings('input[name="color"]').val();
		
		if(row.data('price-class').class_id){
			url += row.data('price-class').class_id;
		}

		$.post(url, data, function(response){
			if(response.success){
				row.replaceWith( price_class_row(response.price_class, false) );						
			}
		},'json');
		e.preventDefault();
	});
	
	$('ul.price-classes').on('click', 'a.edit', function(e){
		var row = $(this).parents('li').first();
		var data = row.data('price-class');
		
		row.replaceWith( price_class_row(data, true) );
		
		$('input.color-picker').spectrum({
			preferredFormat: 'hex6',
			allowEmpty: true,
			clickoutFiresChange: true
		});	
				
		e.preventDefault();				
	});	
	
	$('ul.price-classes').on('click', 'li.editing a.cancel', function(e){
		var row = $(this).parent();
		var data = row.data('price-class');
		
		row.replaceWith( price_class_row(data, false) );
		e.preventDefault();
	});	
	
	$('ul.price-classes').on('click', '#new-price-class a.cancel', function(e){
		$(this).parent().remove();
		e.preventDefault();
	});
	
	$('#add-price-class').on('click', function(e){	
		
		if($('#new-price-class').length <= 0){
			$('ul.price-classes').prepend('<li data-price-class="{}" id="new-price-class"><input name="name" value="" />' +
				'<a class="terminal_button save" href="#">Save</a>' +
				'<a class="cancel" href="#">Cancel</a></li>');
		}
				
		$('input.color-picker').spectrum({
			preferredFormat: 'hex6',
			allowEmpty: true,
			clickoutFiresChange: true
		});	
						
		$('ul.price-classes').find('input').focus();
		e.preventDefault();
	});
});
</script>

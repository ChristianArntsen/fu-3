<?php if (!$ajax) { ?>
<?php $this->load->view("partial/header"); ?>
<?php } ?>
<div id = 'home_page_container'>
<style>
	#software_img {
		background: url(../images/home/<?=$article_1['img']?>) no-repeat center 0px;
	}
	#websites_img {
		background: url(../images/home/<?=$article_2['img']?>) no-repeat center 0px;
	}
	#apps_img {
		background: url(../images/home/<?=$article_3['img']?>) no-repeat center 0px;
	}
	#add_article {
		cursor: pointer;
		background: #336699;
		width: 100px;
		text-align: center;
		position: absolute;
		margin-top: 10px;
		margin-left: 20px;
		color: white;
		padding: 10px;
		border-radius: 4px;
		box-shadow: 1px 1px 7px 1px rgb(162, 162, 162);
		border: 1px solid black;
	}
</style>
<?php if ($this->permissions->is_super_admin()) { ?>
<div id='add_article'>Add Article</div>
<script>
	$('#add_article').click(function(){
		$.colorbox({href:'index.php/home/add_article', width:600, 'title':'Make new article'});
	});
</script>
<?php } ?>
<div id="home_module_list">
	<div id='top_logo'>
		
	</div>
	<div>
		<div class='one_third'>
			<div id='apps_img'></div>
			<div class='home_title'><?=$article_3['title']?></div>
			<div class='home_text_box'>
				<?=$article_3['text']?>
			</div>
		</div>
		<div class='one_third'>
			<div id='websites_img'></div>
			<div class='home_title'><?=$article_2['title']?></div>
			<div class='home_text_box'>
				<?=$article_2['text']?>
			</div>
		</div>
		<div class='one_third'>
			<div id='software_img'></div>
			<div class='home_title'><?=$article_1['title']?></div>
			<div class='home_text_box'>
				<?=$article_1['text']?>
			</div>
		</div>
		<div class='clear'></div>
	</div>
	
	<?php
	/*
	foreach($allowed_modules->result() as $module)
	{
        if ($this->permissions->is_super_admin() || $this->permissions->course_has_module($module->module_id))
        {

	?>
	<div class="module_item">
		<a href="<?php echo site_url("$module->module_id");?>">
			<img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" />
			<span><?php echo lang("module_".$module->module_id) ?></span>
		</a>
		- <span><?php echo lang('module_'.$module->module_id.'_desc');?></span>
	</div>
	<?php
	}
        }*/
	?>
</div>
</div>
<?php if (!$ajax) { ?>
<?php $this->load->view("partial/footer"); ?>
<?php } ?>
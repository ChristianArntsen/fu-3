<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta charset="utf-8">
	<title><?php echo $course['name']; ?> - Online Booking</title>
	<link href="<?php echo base_url(); ?>css/dist/reservations.min.css" type="text/css" rel="stylesheet" />
	
	<script>
	BASE_API_URL = '<?php echo site_url('api/booking'); ?>/';
	BASE_URL = '<?php echo site_url('booking'); ?>/index/';
	SITE_URL = '<?php echo site_url('booking'); ?>/';
	URL = '<?php echo base_url(); ?>';
	COURSE_ID = <?php echo (int) $this->session->userdata('course_id'); ?>;
	API_KEY = 'no_limits';
	
	COURSE = <?php echo json_encode($course); ?>;
	DEFAULT_FILTER = <?php echo json_encode($filter); ?>;
	SCHEDULES = <?php echo json_encode($schedules); ?>;
	USER = <?php echo json_encode($user); ?>;
	SETTINGS = <?php echo json_encode($settings); ?>;
	</script>
	<style>
	body {
		background: url('<?php echo base_url(); ?>images/grid_noise.png');
	}

	#notification {
		margin: 1em;
		position: absolute;
		top: 1em;
		left: 1em;
		right: 1em;
		display: block;
		z-index: 10000;
	}
	
	nav.navbar {
		margin: 0px;
	}
	
	.module, .module-mobile {
		box-shadow: 1px 1px 1px 0px #CCC;
		border-top: 1px solid #DFDFDF; 
		border-left: 1px solid #DFDFDF;
		background-color: #FFFFFF;
		padding-bottom: 4em;
	}	
	
	@media (min-width: 768px) {
		.module-mobile {
			background-color: transparent;	
			border: none;
			box-shadow: none;
		}
	}
	
	div.booking-classes button.btn {
		padding: 0.5em;
		font-size: 1.2em;
		margin-top: 1em;
	}
	
	#booking_class {
		padding-top: 1em;
		padding-bottom: 1em;
		font-size: 1.2em;
	}
	
	#times .loading {
		position: absolute;
		top: 0px; 
		right: 0px;
		bottom: 0px;
		left: 0px;
		display: block;
		z-index: 10;
		text-align: center;
		background: white;
		padding-top: 5em;
	}

	#times .loading h1 {
		color: #CCC;
	}
	
	.time {
		display: block;
		list-style-type: none;
		margin: 0px;
		padding: 0px;
	}
	
	.time > div {
		display: block;
		list-style-type: none;		
		border: 1px solid #DFDFDF;
		border-bottom: 1px solid #CCC;
		display: block;
		float: none;
		width: auto;
		padding: 10px;
		overflow: hidden;
		margin: 5px 5px 0px 0px;
		
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		border-radius: 5px;
		
		background: #fcfcfc;
		background: -moz-linear-gradient(top,  #fcfcfc 1%, #efefef 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,#fcfcfc), color-stop(100%,#efefef));
		background: -webkit-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: -o-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: -ms-linear-gradient(top,  #fcfcfc 1%,#efefef 100%);
		background: linear-gradient(to bottom,  #fcfcfc 1%,#efefef 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#efefef',GradientType=0 );
	}
	
	.time:hover > div {
		background: #eff9ff;
		background: -moz-linear-gradient(top,  #eff9ff 0%, #d3e3ed 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eff9ff), color-stop(100%,#d3e3ed));
		background: -webkit-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: -o-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: -ms-linear-gradient(top,  #eff9ff 0%,#d3e3ed 100%);
		background: linear-gradient(to bottom,  #eff9ff 0%,#d3e3ed 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eff9ff', endColorstr='#d3e3ed',GradientType=0 );
		cursor: pointer;
	}
	
	.time.empty:hover {
		background-color: transparent !important;
	}		
	
	.time.empty {
		text-align: center;
		padding-top: 5em;
		color: #BBB;
	}
	
	.time.empty p {
		font-size: 1.5em;
	}
	
	.time.empty h1 {
		font-weight: bold;
		font-size: 3em;
		color: #888;
	}
	
	.time:last-child {
		border-bottom: none;
	}
		
	.time h4 {
		font-size: 1.4em;
		font-weight: bold;
		display: block;
		margin: 0 0 0.5em 0;	
	}
	
	.time div.price {
		float: right;
		margin: 0;
	}
	
	.time div.price > span {
		display: block;
		overflow: hidden;
		float: none;
		margin-top: 0;
		margin-bottom: 0.25em;
		color: #222;
	}
	
	.time div.pull-left, .time div.pull-right {
		margin: 0px;
		padding: 0px;
		float: left;
		overflow: hidden;	
	}
	
	.time span {
		float: left;
		display: block;
		font-size: 1.2em;
		line-height: 1.2em;
		margin-left: 0.25em;
		overflow: hidden;
		color: #666;
	}
	
	.time .holes, .time .spots {
		overflow: hidden;
		float: left;
	}
	
	.time .holes {
		margin-right: 0.75em;
	}
	
	.time i.glyphicon {
		font-size: 1em;
		float: left;
		display: block;
		color: #666 !important;
	}
	
	.time i.icon-golf-cart, .time i.icon-golf-ball-tee {
		margin-right: 5px;
	}	
	
	#nav div.row > div {
		padding-bottom: 1em;
	}
	
	#nav > div.row {
		padding-top: 1em;
	}
	
	#date-menu {
		text-align: center;
	}
	
	#date {
		display: block;
		overflow: hidden;
		margin: 0px auto 0px auto;
		width: 230px;
	}
	
	#date > div.datepicker-inline {
		margin: 0px !important;
	}
	
	.time-details label, div.confirmation label {
		font-weight: bold;
		display: block;
		color: #BBB;
		margin-bottom: 2px;
	}
	
	div.confirmation label {
		color: #222;
	}
	
	div.confirmation span.field {
		margin-bottom: 1em;
		display: block;
	}
	
	div.confirmation div.well {
		margin-bottom: 0px;
	}
	
	@media (max-width: 767px) {
		div.confirmation .thanks {
			margin-top: 1em;
		}
	}
	
	.time-details span.field, div.confirmation span.field {
		font-size: 1.3em;
		line-height: 1.3em;
	}
	
	.time-details span.total {
		font-size: 1.8em;
	}
	
	.time-details.purchase span.total, .time-details.purchase span.subtotal, .time-details.purchase span.discount {
		font-size: 1.2em;
	}
	
	.time-details div.row > div {
		margin-bottom: 1em;
	}
	
	.time-details div.row.purchase > div {
		margin-bottom: 0.5em;
	}
	
	.time-details div.row.purchase label {
		color: #444;
		font-weight: normal;
		float: left;
		width: 100px;
		font-size: 1.2em;
	}	

	@media (max-width: 768px) {	
		.time-details .modal-footer .btn {
			display: block;
			width: 100%;
		}
	}
	
	address {
		display: block;
		overflow: hidden;
	}
	
	ul.nav span.glyphicon {
		margin-right: 0.5em;
	}
	
	ul.reservations, ul.credit-cards {
		display: block;
		overflow: hidden;
		margin: 0px;
		padding: 0px;
	}
	
	ul.reservations > li, ul.credit-cards > li {
		list-style-type: none;
		display: block;
		overflow: hidden;
		padding: 8px 0px 8px 0px;
		border-bottom: 1px solid #E0E0E0;
	}
	
	ul.credit-cards > li img.type {
		margin-right: 1em;
		display: block;
	}
	
	ul.reservations > li h4, ul.credit-cards > li h4 {
		display: block;
		margin-top: 0px;
		margin-bottom: 2px;
	}
	
	ul.reservations > li span.details {
		color: #888;
		margin-right: 25px;
	}
	
	ul.reservations > li .holes, ul.reservations > li .players {
		display: block;
		float: left;
		font-size: 1em;
		color: #666;
		margin-right: 25px;
	} 
	
	form.account label {
		font-weight: bold;
		color: #444;
		margin-bottom: 3px;
		margin-top: 6px;
	}
	
	form.account input {
		margin-bottom: 5px;
	}
	
	table.purchases .total {
		text-align: right;
		font-weight: bold;
	}
	
	#account-balances div.panel div.panel-body {
		padding: 5px 15px;
	}
	
	div.address {
		display: block;
		padding-left: 20px;
	}
	
	address > span, address > a {
		display: block;
		margin-bottom: 5px;
	}
	
	address span.glyphicon {
		margin-right: 0.5em;
	}
	
	div.address > span {
		display: block;
		margin-left: 0.5em;
	}
	
	button + span.success-msg {
		margin-left: 10px;
	}
	
	span.success-msg > span.glyphicon {
		margin-right: 5px;
	}
	
	#mercury_loader {
		display: block;
		z-index: 100000;
		position: absolute;
		text-align: center;
		padding-top: 75px;
		bottom: 0px;
		top: 0px;
		left: 0px;
		right: 0px;
		margin: 0px;
		background: rgba(255,255,255,0.25);
		padding: 50px;
	}
	
	#mercury_loader h2 {
		margin-bottom: 50px;
	}
	
	div.invoice-details div.dates label {
		float: left;
		display: block;
	}
	
	div.invoice-details div.dates span.line {
		display: block;
		overflow: hidden;
	}
	
	div.invoice-details div.dates span.date {
		float: right;
		display: block;
	}
	
	div.invoice-details table .price {
		text-align: right;
	}
	
	div.invoice-details table thead, div.invoice-details table.totals th {
		background-color: #F0F0F0;
	}
	
	div.invoice-details table.totals th {
		border-right: 1px solid #E0E0E0;
	}
	
	div.invoice-details table {
		border: 1px solid #E0E0E0;
	}
	
	div.invoice-details table.totals td {
		text-align: right;
	}
	
	#payment_methods {
		display: block;
		overflow: hidden;
		margin: 0px;
		padding: 0px 0px 0px 2px;
		width: auto;
	}
	
	#payment_methods > li {
		display: block;
		margin: 0px;
		padding: 0px;
		list-style-type: none;
		float: none;
	}
	
	#payment_methods > li > label {
		display: block;
		margin: 0px;
		padding: 10px;
		float: none;
		width: auto;
		cursor: pointer;
		overflow: hidden;
	}
	
	#payment_methods > li > label.selected {
		border-left: 2px solid #37B455;
		margin-left: -2px;
		background-color: #F6F6F6;
	}
	
	#payment_methods input {
		float: left;
		display: block;
		margin: 5px 10px 5px 0px;
		padding: 0px;
	}
	
	#payment_methods h4 {
		display: block;
		margin: 0px;
		float: left;
		clear: right;
		color: #222;
	}
	
	#payment_methods h4.price {
		float: right;
		clear: none;
		font-weight: bold;
	}
	
	#payment_methods span.details {
		display: block;
		float: left;
		margin-left: 23px;
		font-weight: normal;
		color: #999;
	}
	
	#payment_methods span.discount {
		float: right;
		font-weight: normal;
		font-size: 1em;
	}
	
	#payment_methods div.method, #payment_methods div.details {
		overflow: hidden;
		display: block;
		margin: 0px;
		padding: 0px;
		height: auto;
		width: auto;
	}
	</style>	
</head>
<body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-24484723-1', 'auto');
ga('send', 'pageview');
</script>
<script type="text/html" id="template_tee_time_log_in">
<div style="text-align: center; padding-top: 2em; padding-bottom: 4em;">	
	<h3 class="center-block" style="text-align: center;">Please log in to view tee times for <em><%-booking_class_name%></em></h3>
	<p class="center-block" style="margin-top: 2em">
		<button class="btn btn-lg btn-primary login">Log In</button> 
		<span class="muted" style="padding: 0px 10px 0px 10px;">OR</span>
		<button class="btn btn-lg btn-primary register">Register</button>
	</p>
	<p style="margin-top: 2em;">
		<a href="" class="change-class">Change booking class</a>
	</p>
</div>
</script>	
	
<script type="text/html" id="template_course_info">
	<h2><%-name%></h2>
	<address class="well">
		<% if(phone){ %><a class="phone" href="tel:<%-phone%>"> <span class="glyphicon glyphicon-phone-alt"></span> <%-phone%></a><% } %>
		<% if(email){ %><a class="email" href="mailto:<%-email%>"> <span class="glyphicon glyphicon-envelope"></span> <%-email%></a><% } %>
		<% if(website){ %><a class="web" href="<%-website%>"> <span class="glyphicon glyphicon-globe"></span> <%-website%></a><% } %>
		<div class="address">
			<span class="line" style="margin-left: -20px;">
				<span class="glyphicon glyphicon-home"></span>
				<%-address%>
			</span>
			<span class="line"><%-city%>, <%-state%></span>
			<span class="line"><%-zip%> <%-country%></span>
		</div>
	</address>
	
	<div class="row">
		<% var addressStr = address +','+ city +','+ state +','+ zip;
		addressStr = address.replace(' ', '+'); %>
		<iframe class="col-md-12 col-xs-12 col-sm-12" style="display: block; height: 400px; border: none; margin-bottom: 1em;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAeSdTtDQRykSrrC9KEzfZHUaljeHO_tCE&q=<%-addressStr%>" />
	</div>
</script>

<script type="text/html" id="template_user_settings">
	<h2>Account Information</h2>
	<form name="information" class="information">
		<div class="row">
			<div id="user-info-error" class="alert alert-danger" style="display: none; margin-left: 15px; margin-right: 15px;"></div>
			<div class="form-group col-md-3">
				<label for="account_first_name" class="control-label">First Name</label>
				<input type="text" name="first_name" placeholder="First Name" class="form-control" id="account_first_name" value="<%-first_name %>"
					data-bv-notempty data-bv-notempty-message="First name is required" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_last_name" class="control-label">Last Name</label>
				<input type="text" name="last_name" placeholder="Last Name" class="form-control" id="account_last_name" value="<%-last_name %>"
					data-bv-notempty data-bv-notempty-message="Last name is required" />									
			</div>			
			<div class="form-group col-md-3">
				<label for="account_email" class="control-label">Email</label>
				<input type="text" name="email" placeholder="Email" class="form-control" id="account_email" value="<%-email %>" 
					data-bv-notempty data-bv-notempty-message="Email address required" 
					data-bv-emailaddress data-bv-emailaddress-message="Email address is invalid" />									
			</div>				
			<div class="form-group col-md-2">
				<label for="account_phone" class="control-label">Phone Number</label>
				<input type="number" name="phone" placeholder="Phone Number" class="form-control" id="account_phone" value="<%-phone_number %>" 
					data-bv-notempty data-bv-notempty-message="Phone number is required" />									
			</div>							
		</div>
		<div class="row">
			<div class="form-group col-md-3">
				<label for="account_address" class="control-label">Address</label>
				<input type="text" name="address" placeholder="Address" class="form-control" id="account_address" value="<%-address_1 %>" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_city" class="control-label">City</label>
				<input type="text" name="city" placeholder="City" class="form-control" id="account_city" value="<%-city %>" />									
			</div>
			<div class="form-group col-md-3">
				<label for="account_state" class="control-label">State</label>
				<input type="text" name="state" placeholder="State" class="form-control" id="account_state" value="<%-state %>" />									
			</div>
			<div class="form-group col-md-2">
				<label for="account_zip" class="control-label">Zip</label>
				<input type="number" name="zip" placeholder="Zip" class="form-control" id="account_zip" value="<%-zip %>" />									
			</div>												
		</div>
		<div class="row">
			<div class="form-group col-md-2">
				<label for="account_birthday" class="control-label">Birthday</label>
				<input type="text" name="birthday" placeholder="mm/dd/yyyy" class="form-control" id="account_birthday" value="<% if(birthday != '0000-00-00'){ print(moment(birthday).format('MM/DD/YYYY')) } %>" />									
			</div>		
		</div>
		<div class="row" style="padding-top: 1em;">
			<div class="form-group col-md-12">
				<button class="btn btn-primary save-account" data-loading-text="Saving...">Save Account Info</button>
			</div>
		</div>
	</form>	
	<form name="account" class="account">			
		<div class="row">
			<fieldset class="col-md-12" style="margin-top: 2em;">	
				<legend>Login Information</legend>
				<div class="row">
					<div id="user-credentials-error" class="alert alert-danger" style="display: none; margin-left: 15px; margin-right: 15px;"></div>
					<div class="form-group col-md-3">
						<label for="account_username" class="control-label hidden-xs">Username</label>
						<input type="text" name="username" placeholder="Username" class="form-control" id="account_username" value="<%-username%>"
							data-bv-notempty data-bv-notempty-message="Username" />									
					</div>
					<div class="form-group col-md-3">
						<label for="account_current_password" class="control-label hidden-xs">Current Password</label>
						<input type="password" name="current_passsword" placeholder="Current Password" class="form-control" id="account_current_password" />									
					</div>
					<div class="form-group col-md-3">
						<label for="account_password" class="control-label hidden-xs">New Password</label>
						<input type="password" name="passsword" placeholder="New Password" class="form-control" id="account_password" />									
					</div>
				</div>
			</fieldset>	
		</div>
		<div class="row" style="padding-top: 1em;">
			<div class="col-md-12">
				<button class="btn btn-primary save-login" data-loading-text="Saving...">Save Login Info</button>
			</div>
		</div>
	</form>
</script>

<script type="text/html" id="template_reservation">
	<% if(paid_player_count == 0){ %><button class="btn btn-danger pull-right cancel" data-loading-text="Cancelling...">Cancel</button><% } %>
	<div class="pull-left">
		<h4 style="margin-top: 0px; margin-bottom: 2px;"><%-moment.unix(start_timestamp).format('ddd, MMM Do @ h:mma')%></h4>
		<span class="details"><%-course_name%> - <%-teesheet_title%></span>
	</div>
	<div class="pull-left">
		<div class="holes">
			<span class="glyphicon glyphicon-flag"></span> <%-holes%> holes
		</div>
		<div class="players">
			<span class="glyphicon glyphicon-user"></span> <%-player_count%> players
		</div>		
	</div>
</script>

<script type="text/html" id="template_purchase">
	<td class="sale-num"><%-sale_id%></td>
	<td class="date"><%-moment(sale_date).format('MMM Do, YYYY')%></td>
	<td class="items"><%-items_purchased%></td>
	<td class="total"><%-accounting.formatMoney(total)%></td>
</script>

<script type="text/html" id="template_invoice">
	<td class="invoice-num"><%-invoice_number%></td>
	<td class="date"><%-moment(date).format('MMM Do, YYYY')%></td>
	<td class="total"><%-accounting.formatMoney(total)%></td>
	<td class="due"><%-accounting.formatMoney(total - paid)%></td>
	<td class="status">
		<% if(_.round(total - paid) <= 0){ %>
		<span class="label label-success">PAID</span>
		<% }else if(moment(due_date).startOf('day').unix() < moment().startOf('day').unix()){ %>
		<span class="label label-danger">OVERDUE</span>
		<% }else{ %>
		<span class="label label-warning">UNPAID</span>
		<% } %>
	</td>
	<td style="text-align: right;">
		<% if(_.round(total - paid) > 0){ %><button class="btn btn-success pay" data-loading-text="Loading...">Pay</button><% } %>
		<button class="btn btn-primary view" data-loading-text="Loading...">View</button>	
	</td>
</script>

<script type="text/html" id="template_gift_card">
	<td class="number"><%-giftcard_number%></td>
	<td class="expiration"><% if(expiration_date = '0000-00-00'){ %><em>No Expiration</em><% }else{ print(moment(expiration_date).format('MMM Do, YYYY')) } %></td>
	<td class="status">
		<% if(expiration_date = '0000-00-00' || moment(expiration_date).timestamp() > moment().timestamp()){ %>
			<span class="label label-success">Active</span>
		<% }else{ %>
			<span class="label label-danger">Expired</span>
		<% } %>
	</td>
	<td class="balance"><%-accounting.formatMoney(value)%></td>
</script>

<script type="text/html" id="template_credit_card">
	<% var cc_images = {
		'M/C' : 'images/sales/mastercard.png',
		'AMEX' : 'images/sales/amex.png',
		'DISCOVER' : 'images/sales/discover.png',
		'DCVR' : 'images/sales/discover.png',
		'VISA' : 'images/sales/visa.png',
		'DINERS' : 'images/sales/diners.png',
		'JCB' : 'images/sales/jcb.png'
	}; %>
	<button class='btn btn-danger delete pull-right' data-loading-text="Deleting...">Delete</button>
	<div class="pull-left">
		<img class="type" src="<%-URL%>/<%=cc_images[card_type]%>" />
	</div>
	<div class="pull-left">
		<h4 class="number"><%-card_type.replace('M/C', 'MASTERCARD')%> - <%-masked_account.replace(/[^\d.]/g, "")%></h4>
		<span class="expiration">Expiration: <strong><% if(expiration == '0000-00-00'){ print('N/A') }else{ print(moment(expiration).format('MM/YY')) } %></strong></span>
	</div>
</script>

<script type="text/html" id="template_user_profile">
	<nav class="col-md-3">
		<ul class="nav nav-pills nav-stacked" style="margin-top: 25px;">
			<li class="active">
				<a href="#account-reservations" data-toggle="tab"><span class="glyphicon glyphicon-flag"></span> Reservations</a>
			</li>
			<li>
				<a href="#account-balances" data-toggle="tab"><span class="glyphicon glyphicon-usd"></span> Account Balances/Gift Cards</a>
			</li>			
			<li>
				<a href="#account-purchases" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Purchase History</a>
			</li>
			<li>
				<a href="#account-invoices" data-toggle="tab"><span class="glyphicon glyphicon-inbox"></span> Invoices</a>
			</li>			
			<li>
				<a href="#account-creditcards" data-toggle="tab"><span class="glyphicon glyphicon-credit-card"></span> Credit Cards</a>
			</li>
			<li>
				<a href="#account-settings" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Account Information</a>
			</li>			
		</ul>
	</nav>
	<section class="col-md-9 tab-content" style="min-height: 600px;">
		<div id="account-reservations" class="tab-pane active"></div>
		<div id="account-purchases" class="tab-pane"></div>
		<div id="account-invoices" class="tab-pane table-responsive"></div>
		<div id="account-creditcards" class="tab-pane"></div>
		<div id="account-settings" class="tab-pane"></div>
		<div id="account-balances" class="tab-pane">
			<h2>Account Balances</h2>
			<div class="row">
				<div class="col-md-3">
					<div class="panel <% if(member_account_balance < 0){ print('panel-danger') }else{ print('panel-default') } %>">
						<div class="panel-heading"><% if(App.data.course.get('member_balance_nickname')) { print(App.data.course.get('member_balance_nickname')) }else{ print('Member Balance') } %></div>
						<div class="panel-body <% if(member_account_balance < 0){ print('bg-danger') } %>"><h4><%-accounting.formatMoney(member_account_balance)%></h4></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel <% if(account_balance < 0){ print('panel-danger') }else{ print('panel-default') } %>">
						<div class="panel-heading"><% if(App.data.course.get('customer_credit_nickname')) { print(App.data.course.get('customer_credit_nickname')) }else{ print('Customer Credit') } %></div>
						<div class="panel-body <% if(account_balance < 0){ print('bg-danger') } %>"><h4><%-accounting.formatMoney(account_balance)%></h4></div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading">Loyalty Points</div>
						<div class="panel-body"><h4><%-loyalty_points%></h4></div>
					</div>
				</div>
			</div>
			<h2>Gift Cards</h2>
			<div id="account-giftcards"></div>					
		</div>			
	</section>
</script>

<script type="text/html" id="template_time">
<div>	
	<div class="pull-left">
		<h4 class="start"><%=moment(reservation_time).format('h:mma')%></h4>
		<div class="holes" style="margin-right: 0.5em;">
			<i class="glyphicon glyphicon-flag"></i> <span class="holes"><%=holes%></span>
		</div>
		<div class="spots">
			<i class="glyphicon glyphicon-user"></i> <span class="spots"><%=available_spots%></span>
		</div>
	</div>
	<div class="pull-right">
		<div class="price">
			<span title="Green Fee"><i class="icon-golf-ball-tee" style="font-size: 1em;"></i>  <% if(cart_fee !== false){ %>&nbsp;&nbsp;<% } %><%=accounting.formatMoney(green_fee)%></span>
			<% if(cart_fee !== false){ %><span title="Cart Fee"><i class="icon-golf-cart" style="font-size: 1em;"></i> +<%=accounting.formatMoney(cart_fee)%></span><% } %>		
		</div>
	</div>
</div>
</script>

<script type="text/html" id="template_book_time">
<div class="modal-content time-details">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Book Time</h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="booking-error" class="alert alert-danger" style="display: none;"></div>
		<div class="row">
			<div class="col-md-12">
				<h4 style="margin: 0px; color: #666;"><%-App.data.course.get('name') %> - <%-App.data.schedules.findWhere({'selected':true}).get('title') %></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<label>Date</label>
				<span class="field"><%=reservation_day%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Time</label>
				<span class="field"><%=reservation_time%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Holes</label>
				<span class="field"><%=holes%></span>
			</div>																	
		</div>
		<div class="row">		
			<div class="col-sm-6 col-md-4">
				<label>Players</label>
				<div class="btn-group btn-group-justified players">
					<% for(var x = 1; x <= available_spots; x++){
					if(minimum_players <= x) { %>
					<a class="btn btn-primary <% if(players == x){ %>active<% } %>" data-value="<%-x%>"><%-x%></a>
					<% } } %>												
				</div>
			</div>
			<% if(booking_carts == 1){ %>
			<div class="col-sm-6 col-md-4">
				<label>Carts</label>
				<div class="btn-group btn-group-justified carts">
					<a class="btn btn-primary <% if(!carts){ %>active<% } %>" data-value="no">No</a>
					<a class="btn btn-primary <% if(carts){ %>active<% } %>" data-value="yes">Yes</a>							
				</div>
			</div>
			<% } %>								
		</div>
		<div class="row">
			<div class="col-md-2">
				<label>Total</label>
				<span class="field total"><%=accounting.formatMoney(total)%></span>
			</div>						
		</div>									
	
	</div>
	<div class="modal-footer">
		<button type="button" data-loading-text="Booking time..." class="btn btn-success book pull-left">Book Time</button>
		<button type="button" class="btn btn-default hidden-xs pull-left" data-dismiss="modal">Close</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_invoice_details">
<div class="modal-content invoice-details">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">
			Invoice #<%-invoice_number%> 
			<% if(_.round(total - paid) <= 0){ %>
			<span class="label label-success">PAID</span>
			<% }else if(moment(due_date).startOf('day').unix() < moment().startOf('day').unix()){ %>
			<span class="label label-danger">OVERDUE</span>
			<% }else{ %>
			<span class="label label-warning">UNPAID</span>
			<% } %>
		</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-md-4" style="margin-bottom: 1em;">
				<h4 style="margin-top: 0px;">To</h4>
				<%-person_info.first_name%> <%-person_info.last_name%>
			</div>
			<div class="col-md-4" style="margin-bottom: 1em;">
				<h4 style="margin-top: 0px;">From</h4>
				<%-course_info.name%>
				<%-course_info.phone%>
			</div>
			<div class="col-md-4 dates" style="margin-bottom: 1em;">
				<% if(_.round(total - paid) > 0){ %>
				<span class="line">
					<button class="btn btn-success pay" style="float: right; margin-bottom: 10px;">Pay Online</button>
				</span>
				<% } %>	
				<span class="line">
					<label>Invoiced</label> <span class="date"><%-moment(date).format('MM/DD/YYYY') %></span>
				</span>
				<span class="line">
					<label>Due On</label> <span class="date"><%-moment(due_date).format('MM/DD/YYYY') %></span>
				</span>
			</div>						
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th class="name">Item</th>
								<th class="qty">Qty</th>
								<th class="price">Price</th>
							</tr>
						</thead>
						<tbody>
						<% _.each(current_items, function(item){ %>
							<tr>
								<td class="name"><%-item.name%></td>
								<td class="qty"><%-item.quantity%></td>
								<td class="price"><%-accounting.formatMoney(item.total)%></td>
							</tr>
						<% }); %>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-offset-6 col-md-6">
				<table class="table totals">
					<tbody>
						<tr>
							<th>Total</th>
							<td><%-accounting.formatMoney(total)%></td>
						</tr>
						<tr>
							<th>Paid</th>
							<td><%-accounting.formatMoney(paid)%></td>
						</tr>
						<tr>
							<th>Due</th>
							<td><strong><%-accounting.formatMoney(total - paid)%></strong></td>
						</tr>
					</tbody>												
				</table>
			</div>
		</div>
		<% if(show_account_transactions == 1){ %>
		<div class="row">
			<div class="col-md-12">
				<h4>Account Transactions</h4>
				<table class="table transactions">
					<thead>
						<tr>
							<th>#</th>
							<th class="date">Date</th>
							<th class="name">Item</th>
							<th class="price">Amount</th>
						</tr>
					</thead>					
					<tbody>
						<% if(account_transactions.length > 0){
						_.each(account_transactions, function(transaction){ %>
						<tr>
							<td><%-transaction.trans_comment %></td>
							<td><%-transaction.date %></td>
							<td><%=transaction.trans_description %></td>
							<td><%-accounting.formatMoney(transaction.total)%></td>
						</tr>
						<% }); }else{ %>
						<tr>
							<td colspan="4"><span class="muted">No transactions available</span></td>
						</tr>
						<% } %>
					</tbody>												
				</table>				
			</div>
		</div>	
		<% } %>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-default close-invoice" data-dismiss="modal">Close</button>
			</div>
		</div>			
	</div>
</div>
</script>

<script type="text/html" id="template_payment_selection">
<div class="modal-content time-details purchase">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Payment Method</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-md-9">
				<ul id="payment_methods">
					<li>
						<label>
							<div class="method">
								<input type="radio" name="payment_method" value="course" />
								<h4>Pay at Course</h4><h4 class="price"><%-accounting.formatMoney(_.round(total))%></h4>
							</div>
							<div class="details">
								<span class="muted details">Regular Price</span>
							</div>
						</label>
					</li>
					<li>
						<label class="selected">
							<div class="method">
								<input type="radio" name="payment_method" value="online" checked />
								<h4>Pay Online</h4><h4 class="price"><%-accounting.formatMoney(_.round(pay_total))%></h4>
							</div>
							<div class="details">
								<span class="muted details"><%-accounting.formatNumber(discount_percent)%>% OFF - Limited Time!</span>
								<span class="label label-warning discount">Save <%-accounting.formatMoney(discount)%></span>
							</div>
						</label>
					</li>
				</ul>
			</div>																	
		</div>
		<% if(App.data.course.get('include_tax_online_booking') == "0"){ %>
		<div class="row">
			<div class="col-md-12">	
				<small class="text-muted">* Prices displayed may not include tax</small>	
			</div>						
		</div>
		<% } %>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-success continue">Continue</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_purchase_reservation">
<div class="modal-content time-details purchase">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Purchase Your Reservation</h4>
	</div>
	<div class="modal-body container-fluid">
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<label>Date</label>
				<span class="field"><%=reservation_day%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Time</label>
				<span class="field"><%=reservation_time%></span>
			</div>
			<div class="col-xs-6 col-md-3">
				<label>Holes</label>
				<span class="field"><%=holes%></span>
			</div>																	
		</div>
		<div class="row">		
			<div class="col-sm-6 col-md-4">
				<label>Players</label>
				<div class="btn-group btn-group-justified players">
					<% for(var x = min_required_paying; x <= available_spots; x++){ %>
					<a class="btn btn-primary <% if(parseInt(pay_players) == x){ %>active<% } %>" data-value="<%-x%>"><%-x%></a>
					<% } %>												
				</div>
			</div>
			<% if(can_book_carts){ %>
			<div class="col-sm-6 col-md-4">
				<label>Carts</label>
				<div class="btn-group btn-group-justified carts">
					<a class="btn btn-primary <% if(!carts || carts == 0){ %>active<% } %>" data-value="no">No</a>
					<a class="btn btn-primary <% if(carts || carts > 0){ %>active<% } %>" data-value="yes">Yes</a>							
				</div>
			</div>	
			<% } %>						
		</div>
		<div class="row purchase" style="margin-top: 1em;">
			<div class="col-md-2 col-xs-4">
				<label>Subtotal</label>
			</div>
			<div class="col-md-2 col-xs-4">
				<span class="field subtotal"><%=accounting.formatMoney(pay_subtotal)%></span>
			</div>
		</div>
		<div class="row purchase">
			<div class="col-md-2 col-xs-4">
				<label>Discount</label>
			</div>
			<div class="col-md-2 col-xs-4">	
				<span class="field discount"><%=accounting.formatMoney(discount)%></span>
			</div>
		</div>
		<div class="row purchase">
			<div class="col-md-2 col-xs-4">
				<label><strong>Total</strong></label>
			</div>
			<div class="col-md-2 col-xs-4">
				<span class="field total"><strong><%=accounting.formatMoney(pay_total)%></strong></span>
			</div>													
		</div>
		<div class="row purchase">
			<div class="col-md-12">
				<small class="text-muted">This tee time is non-cancelable and non-refundable unless the course is closed due to weather.
				Booking fees are non-refundable. Fees are 100% due online to ForeUP Inc.
				</small>
			</div>
		</div>		
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-success purchase pull-left">Purchase</button>
		<button type="button" class="btn btn-default hidden-xs pull-left" data-dismiss="modal">Close</button>		
	</div>
</div>
</script>

<script type="text/html" id="template_log_in">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Log In/Register</h4>
	</div>
	<div class="modal-body container-fluid">
		<div id="login-error" class="alert alert-danger" style="display: none"></div>
		<form role="form" id="login_form" class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="form-group col-md-12">
						<label for="login_email" class="control-label hidden-xs">Email</label>
						<input type="text" name="email" placeholder="Email" class="form-control" id="login_email" data-bv-notempty="true" data-bv-notempty-message="Email is required" />									
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label for="login_password" class="control-label hidden-xs">Password</label>
						<input type="password" name="password" placeholder="Password" class="form-control" id="login_password" data-bv-notempty="true" data-bv-notempty-message="Password is required" />									
					</div>
				</div>	
			</div>
			<div class="col-md-5 col-md-offset-1 hidden-xs hidden-sm">
				<p style="margin-top: 2em; text-align: center">Don't have an account yet?</p>
				<div class="form-group">
					<button class="btn btn-primary register center-block">Sign up for free</button>	
				</div>
			</div>						
		</form>
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-primary login col-xs-12 col-md-2" data-loading-text="Logging In...">Log In</button>
			<button type="button" class="btn btn-default hidden-xs hidden-sm pull-left" data-dismiss="modal">Close</button>											
		</div>		
		<div class="form-group hidden-md hidden-lg">
			<h4 style="text-align: center;" class="col-xs-12 text-muted">Or</h4>				
		</div>
		<div class="form-group">
			<button class="btn btn-primary register col-xs-12 col-md-3 hidden-md hidden-lg">Register</button>	
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_register">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Register</h4>
	</div>
	<div class="modal-body container-fluid">
		<form role="form" id="register_form">
			<div id="signup-error" class="alert alert-danger" style="display: none"></div>	
			<div class="row">
				<div class="form-group col-md-5">
					<label for="register_first_name" class="control-label hidden-xs">First Name</label>
					<input type="text" name="first_name" placeholder="First Name" class="form-control" id="register_first_name" 
						data-bv-notempty data-bv-notempty-message="First name is required" />											
				</div>
				<div class="form-group col-md-5">
					<label for="register_last_name" class="control-label hidden-xs">Last Name</label>
					<input type="text" name="last_name" placeholder="Last Name" class="form-control" id="register_last_name" 
						data-bv-notempty data-bv-notempty-message="Last name is required" />											
				</div>				
			</div>
			<div class="row">								
				<div class="form-group col-md-5">
					<label for="register_phone" class="control-label hidden-xs">Phone Number</label>
					<input type="text" name="phone" placeholder="Phone Number" class="form-control" id="register_phone" 
						data-bv-notempty data-bv-notempty-message="Phone number required" />											
				</div>
			</div>
			<div class="row">				
				<div class="form-group col-md-5">
					<label for="register_email" class="control-label hidden-xs">Email/Username</label>
					<input type="text" name="email" placeholder="Email" class="form-control" id="register_email" 
						data-bv-notempty data-bv-notempty-message="Email address required" 
						data-bv-emailaddress data-bv-emailaddress-message="Email address is invalid"
						/>											
				</div>
				<div class="form-group col-md-5">
					<label for="register_password" class="control-label hidden-xs">Password</label>
					<input type="password" name="password" placeholder="Password" class="form-control" id="register_password" 
						data-bv-notempty data-bv-notempty-message="Password required"
						data-bv-stringlength data-bv-stringlength-min="6" data-bv-stringlength-message="Must be at least 6 characters"
						/>											
				</div>				
			</div>			
		</form>	
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success complete col-xs-12 col-md-3" data-loading-text="Creating account...">Register</button>												
		</div>	
	</div>

</div>
</script>

<script type="text/html" id="template_select_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Select Credit Card</h4>
	</div>
	<div class="modal-body container-fluid">
		<form role="form" id="register_form">
			<div class="row">
				<div class="form-group col-md-12">
					<% if(App.data.course.get('no_show_policy')){ %>
					<h4 style="margin-top: 0px; padding-top: 0px;">No Show Policy</h4>
					<p><%=App.data.course.get('no_show_policy') %></p>
					<% } %>										
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="booking_credit_card" class="control-label hidden-xs">Credit Card</label>
					<select id="booking_credit_card" class="form-control">
						<% if(!credit_cards || credit_cards.length == 0){ %>
						<option value="">- No Credit Cards -</option>
						<% }else{ %>
						<% _.each(credit_cards, function(credit_card){ %>
						<option value="<%-credit_card.get('credit_card_id')%>" <% if(credit_card.get('selected')){ %>selected<% } %>><%-credit_card.get('card_type')%> <%-credit_card.get('masked_account')%></option>
						<% }); } %>
					</select>											
				</div>
				<div class="col-md-2">
					<div style="height: 22px; display: block"></div>
					<button class="btn btn-primary add-card">Add Card</button>
				</div>			
			</div>		
		</form>		
	</div>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success set-card col-xs-12 col-md-3" data-loading-text="Booking tee time...">Book Time</button>												
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_invoice_select_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Invoice #<%-invoice_number%> - Select Credit Card</h4>
	</div>
	<div class="modal-body container-fluid">
		<form role="form" id="pay_invoice_form">
			<div id="invoice-payment-error" class="alert alert-danger" style="display: none;"></div>
			<div class="row" style="margin-bottom: 1em;">
				<div class="col-md-12">
					<h2 style="margin-top: 0px;"><%-accounting.formatMoney(total - paid) %></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="invoice_credit_card" class="control-label hidden-xs">Credit Card</label>
					<select id="invoice_credit_card" class="form-control">
						<% if(!credit_cards || credit_cards.length == 0){ %>
						<option value="">- No Credit Cards -</option>
						<% }else{ %>
						<% _.each(credit_cards.models, function(credit_card){ %>
						<option value="<%-credit_card.get('credit_card_id')%>" <% if(credit_card.get('selected')){ %>selected<% } %>><%-credit_card.get('card_type')%> <%-credit_card.get('masked_account')%></option>
						<% }); } %>
					</select>											
				</div>
				<div class="col-md-2">
					<div style="height: 22px; display: block"></div>
					<button class="btn btn-primary add-card">Add Card</button>
				</div>			
			</div>		
		</form>		
	</div>
	<div class="modal-footer">
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-success pay" style="float: left;" data-loading-text="Completing Payment...">Pay Invoice</button>												
			</div>
		</div>	
	</div>
</div>
</script>

<script type="text/html" id="template_add_credit_card">
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Add Credit Card</h4>
	</div>
	<div class="modal-body container-fluid">
		<% if(App.data.course.get('credit_card_provider') == 'ets'){ %>
		<form role="form" id="add_credit_card_form">
			<div class="row">
				<div class="form-group col-md-4">
					<label for="credit_card_first_name" class="control-label hidden-xs">First Name</label>
					<input type="text" class="form-control ETSFirstName" placeholder="First Name" name="first_name" id="credit_card_first_name"
						data-bv-notempty data-bv-notempty-message="First name is required" />											
				</div>
				<div class="form-group col-md-4">
					<label for="credit_card_last_name" class="control-label hidden-xs">Last Name</label>
					<input type="text" class="form-control ETSLastName" placeholder="Last Name"  name="last_name" id="credit_card_last_name" 
						data-bv-notempty data-bv-notempty-message="Last name is required"
					/>											
				</div>			
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					<label for="credit_card_name" class="control-label hidden-xs">Name on Card</label>
					<input type="text" class="form-control ETSNameOnCard" placeholder="Name on card"  name="credit_card_name" id="credit_card_name"
						data-bv-notempty data-bv-notempty-message="Name on card is required"
					 />											
				</div>
				<div class="form-group col-md-2">
					<label for="credit_card_cvv" class="control-label hidden-xs">CVV</label>
					<input type="number" class="form-control ETSCVVCode" placeholder="CVV"  name="cvv" id="credit_card_cvv" style="padding-right: 0px;"
						data-bv-notempty data-bv-notempty-message="CVV required" />											
				</div>									
			</div>
			<div class="row">
				<div class="form-group col-md-5">
					<label for="booking_credit_card" class="control-label hidden-xs">Credit Card Number</label>
					<input type="number" class="form-control ETSCardNumber" placeholder="Number"  name="number" id="credit_card_number"
						data-bv-notempty data-bv-notempty-message="Credit card number is required"
					 />											
				</div>
				<div class="form-group col-md-3">
					<label for="booking_credit_card" class="control-label hidden-xs">Expiration</label>
					<select name="expiration_month" class="form-control ETSExpirationMonth" style="width: 55px; float: left; padding: 0px 0px 0px 5px;">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>								
					</select>
					<select name="expiration_year" class="form-control ETSExpirationYear" style="width: 55px; float: left; padding: 0px 0px 0px 5px; margin-left: 5px;">
						<% var date = new Date();
						var year = date.getFullYear();
						for(var y = year + 20; year < y; year++){ %> 
						<option value="<%-year%>"><%-(year - 2000) %></option>
						<% } %>							
					</select>											
				</div>		
			</div>						
		</form>
		<% }else{ %>
			<div id="mercury_loader">
				<h2>Loading form...</h2>
				<div class="progress progress-striped active">
					<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
				</div>
			</div>			
			<iframe id="mercury_iframe" src="<%=SITE_URL %>credit_card_window" onLoad="$('#mercury_loader').remove();" style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe>
		<% } %>	
	</div>
	<% if(App.data.course.get('credit_card_provider') == 'ets'){ %>
	<div class="modal-footer">
		<div class="form-group">
			<button class="btn btn-success add-card col-xs-12 col-md-3 disabled" disabled="disabled" data-loading-text="Verifying Card...">Add Card</button>												
		</div>	
	</div>
	<% } %>
</div>
</script>

<script type="text/html" id="template_filters">
	<!-- Teesheet -->
	<div class="row" style="margin-top: 1em;">
		<div class="col-md-12">
			<label class="hidden-xs">Course</label>
			<select id="schedule_select" name="schedules" class="form-control schedules">
				<% _.each(App.data.schedules.models, function(schedule){ %>
				 <option value="<%=schedule.get('teesheet_id') %>" <%if(schedule_id == schedule.get('teesheet_id')){%>selected<%}%>><%=schedule.get('title') %></option>
				<% }); %>
			</select>
		</div>
	</div>
	
	<!-- Date -->
	<div class="row">
		<div class="col-md-12">
			<div id="date" class="datepicker hidden-xs hidden-sm input-append">
				<input value="06-24-2014" type="hidden" value="<%=date%>" />
			</div>		
			<div class="row hidden-md hidden-lg">
				<div class="col-xs-3" style="padding-bottom: 0px;">
					<button class="btn btn-primary prevday" style="display: block; width: 100%;" type="button">&lt;</button>	
				</div>
				<div class="col-xs-6" style="padding: 0px;">
					<select name="date" id="date-menu" class="datepicker form-control"></select>
				</div>
				<div class="col-xs-3" style="padding-bottom: 0px;">
					<button class="btn btn-primary nextday" type="button" style="display: block; width: 100%;">&gt;</button>
				</div>					
			</div>
		</div>
	</div>

	<!-- Players -->
	<div class="row">
		<div class="col-md-12 hidden-xs">
			<label>Players</label>
			<div class="btn-group btn-group-justified hidden-xs players">
				<% if(minimum_players == 1){ %>
				<a class="btn btn-primary <% if(players == '1'){ %>active<% } %>" data-value="1">1</a>
				<% } if(minimum_players <= 2){ %>
				<a class="btn btn-primary <% if(players == '2'){ %>active<% } %>" data-value="2">2</a>
				<% } if(minimum_players <= 3){ %>
				<a class="btn btn-primary <% if(players == '3'){ %>active<% } %>" data-value="3">3</a>
				<% } if(minimum_players <= 4){ %>
				<a class="btn btn-primary <% if(players == '4'){ %>active<% } %>" data-value="4">4</a>	
				<% } %>						
			</div>
		</div>
	</div>
	
	<div class="row">
		<!-- Time of day -->
		<div class="col-sm-6 col-md-12">
			<label class="hidden-xs">Time of Day</label>
			<div class="btn-group btn-group-justified time">
				<a class="btn btn-primary <% if(time == 'morning'){ %>active<% } %>" data-value="morning">Morning</a>
				<a class="btn btn-primary <% if(time == 'midday'){ %>active<% } %>" data-value="midday">Midday</a>
				<a class="btn btn-primary <% if(time == 'evening'){ %>active<% } %>" data-value="evening">Evening</a>
			</div>
		</div>
	
		<!-- Holes -->
		<div class="col-sm-6 col-md-6">
			<label class="hidden-xs">Holes</label>		
			<div class="btn-group btn-group-justified holes">
				<% if(limit_holes == 9 || limit_holes == '0'){ %>
				<a class="btn btn-primary <% if(holes == '9'){ %>active<% } %>" data-value="9">9</a>
				<% } if(limit_holes == 18 || limit_holes == '0'){ %>
				<a class="btn btn-primary <% if(holes == '18'){ %>active<% } %>" data-value="18">18</a>
				<% } %>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 hidden-xs">
			<h4>Booking Rules</h4>	
			<%=App.data.course.get('booking_rules')%>
		</div>
	</div>
</script>

<script type="text/html" id="template_navigation">

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="#"><%-course.name%></a>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<ul class="nav navbar-nav">
		<li><a href="#teetimes"><span class="glyphicon glyphicon-flag"></span> Tee Times</a></li>
		<li><a href="#course"><span class="glyphicon glyphicon-map-marker"></span> Course Info</a></li>
	</ul>

	<ul class="nav navbar-nav navbar-right">
		<% if(!user.logged_in){ %>
		<li><a class="login" href=""><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>
		<% }else{ %>
		<li>
			<a href="#account"><span class="glyphicon glyphicon-briefcase"></span> My Account</a>
		</li>		
		<li>
			<a class="logout" href=""><span class="glyphicon glyphicon-log-out"></span> Logout</a>
		</li>	
		<% } %>
	</ul>
</div><!-- /.navbar-collapse -->
</script>

<script type="text/html" id="template_confirmation">
<h1 style="font-weight: bold; text-align: center;">Congratulations!</h1>
<h3 style="margin-top: 0px; text-align: center;">Your tee time has been booked</h3>
<div class="panel panel-default confirmation" style="margin-top: 15px;">
	<div class="panel-heading">
		<h3 class="panel-title">Reservation Details</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="well">
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<label>Date</label>
							<span class="field"><%-reservation_day%></span>
						</div>
						<div class="col-xs-6 col-md-6">
							<label>Time</label>
							<span class="field"><%-reservation_time%></span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<label>Players</label>
							<span class="field"><%-players%></span>
						</div>
						<div class="col-xs-6 col-md-6">
							<label>Holes</label>
							<span class="field"><%-holes%></span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<label>Green Fee</label>
							<span class="field" style="margin-bottom: 0px;"><%-accounting.formatMoney(green_fee)%></span>
						</div>
						<div class="col-xs-6 col-md-6">
							<label>Total</label>
							<span class="field" style="margin-bottom: 0px;"><%-accounting.formatMoney(total)%></span>
						</div>													
					</div>								
				</div>
			</div>
			<div class="col-md-6 thanks">
				<h4>Thank you!</h4>
				<% if(can_purchase && available_spots >= 2){ %>
				<p>
					<% if(!purchased){ %>
					<button class="btn btn-lg btn-success pay-now">
						<span class="glyphicon glyphicon-tag" style="top: 2px"></span> Pay Now and Save <%-_.round(foreup_discount_percent,0) %>%
					</button> 
					<% }else{ %>
					<h3>Your tee time has been purchased</h3>
					<% } %>
				</p>
				<% } %>
				<p>A confirmation email has been sent to <strong><%-user_email%></strong>.
				For questions, call <%-course_name%> at <a href="tel:<%-course_phone%>"><%-course_phone%></a></p>				
			</div>
		</div>
	</div>
</div>
</script>


<script type="text/html" id="template_two_col">
<div class="row hidden-xs">
	<div id="header" class="col-md-8"></div>
</div>
<div class="row">
	<div id="nav" class="col-xs-12 col-md-3"></div>
	<div class="col-xs-12 col-md-9">
		<div id="booking_class" class="row"></div>
		<div id="content" class="row"></div>
	</div>
</div>
</script>

<script type="text/html" id="template_one_col">
<div class="row hidden-xs">
	<div id="header" class="col-md-8"></div>
</div>
<div class="row">
	<div id="content" class="col-xs-12 col-md-12"></div>
</div>
</script>

<nav class="navbar navbar-default" role="navigation" id="navigation"></nav>
<div class="container module" id="page" style="margin-bottom: 2em;"></div>
<div class="modal" id="modal" role="dialog" aria-hidden="true"></div>
<div class="alert" style="display: none;" id="notification"></div>

<script src="<?php echo base_url(); ?>js/dist/reservations.min.js"></script>
</body>
</html>

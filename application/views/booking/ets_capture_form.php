<style>
.etsFormGroup { margin: 10px 0; }
#ets_img { margin:40px auto;}

.etsButton {
	background: #3D80B9;
	color: white !important;
	border: 1px solid #232323 !important;
	display: block;
	font-size: 14px;
	font-weight: normal;
	width: 150px;
	padding: 0.75em 1.25em;
	text-align: center;
	border-radius: 4px;
	margin-top: 1em;
	border: 1px solid #232323;
}

.etsMessage {
	color: #c44b00;
	font-weight: bold;
	font-size: 20px;
}

#ets_iframe_container {
	height: 0px;
	overflow: hidden;
	display: none !important;
}
</style>

<div id="ets_iframe_container">
	<div id='ets_iframe_replace' data-ets-key="<?php echo trim($session->id); ?>"></div>
</div>

<script>
if (typeof ETSPayment == 'undefined')
{
	var e = document.createElement('script');
	e.src = "<?php echo ECOM_ROOT_URL ?>/init";
	document.getElementsByTagName('head')[0].appendChild(e);
}

var interval_id = '';
var count = 0;
$(document).ready(function(){
	add_ets_handlers();
	interval_id = setInterval(add_ets_handlers, 50);
});

function add_ets_handlers()
{
	if ($('#ETSIFrame').length > 0)
	{
		ETSPayment.useHiddenSDK("<?php echo trim($session->id); ?>");
		ETSPayment.addResponseHandler("success", function(etsResponse){
			
			var params = {};
			params.number = etsResponse.customers.cardNumber;
			params.expiration = etsResponse.customers.cardExpiration;
			params.type = etsResponse.customers.cardType;
			params.ets_id = etsResponse.id;
			params.ets_session_id = "<?php echo trim($session->id); ?>";
			params.ets_customer_id = etsResponse.customers.id;
			
			// Add new credit card to user's profile
			App.data.user.get('credit_cards').create(params, {wait: true});
		});
		
		ETSPayment.addResponseHandler("validation", function(message){
			alert(message);
			$('button.add-card').button('reset');
		});	
		
		$('button.add-card').attr('disabled', null).removeClass('disabled');

		clearInterval(interval_id);
	}
	else if (count > 500)
	{
		clearInterval(interval_id);
	}
	count ++;
} 
</script>

<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<style>
	* {
		font-family:"Lucida Grande", Arial;
	}
</style>
<div>
	Payment successful.
</div>
<script>
	$(document).ready(function(){
		window.parent.booking.close_popup();
		window.parent.booking.hide_pay_now_info(window.parent.booking.show_purchase_thank_you());
		//window.parent.booking.update_booking_details('<?=$booked_players?>','<?=$total?>');
	});
</script>
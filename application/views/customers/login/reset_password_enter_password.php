<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/login.css?<?php echo APPLICATION_VERSION; ?>" />
<title>ForeUP <?php echo lang('login_reset_password'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#login_form input:first").focus();
});
</script>
</head>
<body>

	<?php if (isset($error_message)) {?>
		<div id="welcome_message" class="top_message_error">
			<?php echo $error_message; ?>
		</div>
	<?php } ?>
<?php echo form_open('customer_login/do_reset_password/'.$key) ?>
<div id="container">
	<div id="top">
		<?php //echo img(array('src' => '../images/login/login_logo.png'));?>
		<?php 
			$logo = $this->Appconfig->get_logo_image('',$this->session->userdata('course_id'));
			echo $logo ? img(array('src' => $logo,'width' =>144)) : ''; 
		?>
		<?php //echo img(array('src' => $this->Appconfig->get_logo_image()));?>
	</div>
	<div id='middle'>
		<div class='login_header'>
			<?php echo lang('login_reset_password').': '.$username;?>
		</div>
		<div id="login_form">
			<div id="form_field_password">	
				<span class='username_icon'></span>
				<?php echo form_password(array(
				'name'=>'password', 
				'placeholder'=> lang('login_password'),
				'size'=>'20')); ?>
			</div>
			<div id="form_field_password_confirm">	
				<span class='username_icon'></span>
				<?php echo form_password(array(
				'name'=>'confirm_password', 
				'placeholder'=>lang('login_confirm_password'),
				'size'=>'20')); ?>
			</div>
		</div>
		<div id="form_field_submit">	
			<div id="submit_button">
				<?php echo form_submit('login_button',lang('login_reset_password')); ?>
			</div>
		</div>
	</div>
	<div id="bottom">
		<div id="right">
			<?php echo anchor('be/reservations', lang('login_login')); ?> 
		</div>
	</div>
</div>
<?php echo form_close(); ?>
</body>
</html>
<div style="padding: 15px;">
<?php
$cc_header = ($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');
echo form_open('customers/save_member_account_transaction/'.$customer_info->person_id);?>
<fieldset id="account_basic_info">
<legend><?php echo $cc_header; ?></legend>
<div class='account_history' style='padding-top:10px;'>
	<a href="index.php/customers/member_account_adjustment/<?=$customer_info->person_id?>/width~700" title='Member Account Adjustment' class='colbox'>Show Member Account Adjustment</a>
	<script>
		$('.colbox').colorbox();
	</script>
</div>

<table align="center" border="0" bgcolor="">
<div class="field_row clearfix">
<tr>
<td>
<?php echo form_label(lang('customers_account_number').':', 'name',array('class'=>'wide')); ?>
</td>
<td>
	<?php $anumber = array (
		'name'=>'account_number',
		'id'=>'account_number',
		'value'=>$customer_info->account_number,
		'style'       => 'border:none',
		'readonly' => 'readonly'
	);

		echo form_input($anumber)
	?>
</td>
</tr>
<tr>
<td>
<?php echo form_label(lang('common_name').':', 'name',array('class'=>'wide')); ?>
</td>
<td>
	<?php $cname = array (
		'name'=>'name',
		'id'=>'name',
		'value'=>$customer_info->last_name.', '.$customer_info->first_name,
		'style'       => 'border:none',
		'readonly' => 'readonly'
	);
		echo form_input($cname);
		?>
</td>
</tr>
<tr>
<td>
<?php echo form_label($cc_header.':', 'account_balance',array('class'=>'wide')); ?>
</td>
<td>
	<?php $acc_bal = array (

		'name'=>'account_balance',
		'id'=>'account_balance',
		'value'=>$customer_info->member_account_balance,
		'style'       => 'border:none',
		'readonly' => 'readonly'
		);

		echo form_input($acc_bal);
	?>
</td>
</tr>
</div>
</table>

<div class="field_row clearfix">
  <div class='form_field'></div>
</div>

<div class="field_row clearfix">
  <div class='form_field'></div>
</div>
</fieldset>
<?php
echo form_close();
?>
<table border="0" align="center" style='margin-bottom:15px;'>
<tr bgcolor="" align="center" style="font-weight:bold; color:#333; font-size: 14px;"><td colspan="4" style='padding:10px;'><?= $cc_header?> Data</td></tr>
<tr align="center" style="font-weight:bold">
	<td width="15%">Date</td>
	<td width="15%">Employee</td>
	<td width="30%">Description</td>
	<td width="20%">Household</td>
	<td width="10%">Amount</td>
	<td width="10%">Balance</td>
</tr>
<?php
$transactions = $this->Account_transactions->get_account_transaction_data_for_customer('member', $customer_info->person_id, '0000-00-00 00:00:00');
foreach($transactions as $row)
{
?>
<tr bgcolor="" align="center">
<td><span style='font-size:smaller'><?php echo $row['date'];?></span></td>
<td><?php
	$person_id = $row['trans_user'];
	$employee = $this->Employee->get_info($person_id);
	echo $employee->first_name." ".$employee->last_name;
	?>
</td>
<td><?php echo $row['trans_comment'];?><div style='font-size:small;'><?php echo $row['trans_description']; ?></div></td>
<td><?php
	if ($row['trans_customer'] != $customer_info->person_id) {
		$customer = $this->Customer->get_info($row['trans_customer']);
		echo 'Charge from '.$customer->first_name.' '.$customer->last_name;
	}
	else if ($row['trans_household'] && $row['trans_household'] != $customer_info->person_id) {
		$customer = $this->Customer->get_info($row['trans_household']);
		echo 'Charged to '.$customer->first_name.' '.$customer->last_name;
	}
	?>
</td>
<td align="right" class="<?php echo ($row['trans_amount'] < 0)?'red':''; ?>"><?php echo $row['trans_amount'];?></td>
<td style="text-align: right;">
<?php if($row['running_balance']){ 
	echo $row['running_balance'];
} ?>
</td>
</tr>

<?php
}
?>
</table>
</div>

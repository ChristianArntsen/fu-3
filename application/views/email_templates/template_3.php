<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
      <title>Skyline - Newsletter with Template Builder</title>
      <script async="" src="//analysis.revaxarts.com/piwik.js">
    </script>
      <script async="" src="//revaxarts-themes.com/_js/mod.js?1325629471">
    </script>
      <script type="text/javascript">!window.jQuery && document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
        <\/script>')</script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
      </script>
        <link href="/_css/mod.css?1325629471" rel="stylesheet" type="text/css">
          <link href="/skyline/_mod/mod.css?1325629471" rel="stylesheet" type="text/css">
            <script type="text/javascript">var _mod=[];(function(u,d,t){_mod.push("skyline", 1325629471);_mod.push(true);var g=d.createElement(t),s=d.getElementsByTagName(t)[0];window.rx=g.async=true;g.src="//"+u+"/_js/mod.js?1325629471";s.parentNode.insertBefore(g,s);})("revaxarts-themes.com",document,"script");</script>
          </head>
          <body bgcolor="#F4F4F4">
            <style type="text/css"> body{margin:0;padding:0;} .bodytbl{margin:0;padding:0;-webkit-text-size-adjust:none;} table{font-family:Helvetica, Arial, sans-serif;font-size:12px;color:#787878;} div{line-height:24px;color:#787878;} img{display:block;} td,tr{padding:0;} ul{margin-top:24px;margin-bottom:24px;} li{list-style:disc;line-height:24px;} a{color:#4B9515;text-decoration:none;padding:2px 0px;} .headertbl{border-bottom:1px solid #E1E1E1;} .contenttbl{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;} .footertbl{border-top:1px solid #E1E1E1;} .sheet{background-color:#f8f8f8;border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;border-bottom:1px solid #E1E1E1;line-height:1px;} .h1 div{font-family:Helvetica,Arial,sans-serif;font-size:30px;color:#4B9515;font-weight:bold;letter-spacing:-1px;margin-bottom:22px;margin-top:2px;line-height:36px;} .h2 div{font-family:Helvetica,Arial,sans-serif;font-size:24px;color:#787878;letter-spacing:0;margin-bottom:22px;margin-top:2px;line-height:30px;} .h div{font-family:Helvetica,Arial,sans-serif;font-size:24px;color:#4B9515;letter-spacing:-2px;margin-bottom:22px;margin-top:2px;line-height:24px;} .invert div, .invert .h{color:#F4F4F4;} .invert div a{color:#FFFFFF;} .line{border-top:1px dotted #D1D1D1;} .logo{border-right:1px dotted #D1D1D1;} .small div{font-size:10px; line-height:16px;} .btn{margin-top:10px;display:block;} .btn img,.social img{display:inline;} .subline div{font-family:Georgia,"Times New Roman",Times,serif;font-style:italic;text-align:right;} div.preheader{line-height:1px;font-size:1px;height:1px;color:#F4F4F4;display:none!important;} </style>
            <table class="bodytbl" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
              <tbody>
                <tr>
                  <td align="center">
                    <table width="560" cellspacing="0" cellpadding="0">
                      <tbody>
                        <tr>
                          <td height="42" align="center" valign="top">
                            <div class="preheader">
                              <!-- PREHEADER -->
                            </div>
                            <div class="small">
                              <div>
                                Having trouble seeing this email? <a href="#" target="_blank">view it online</a> or <a href="#" target="_blank">unsubscribe</a>
                              </div>
                              <a name="top">
                            </a>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                    <table width="600" cellpadding="0" cellspacing="0" class="headertbl">
                      <tbody>
                        <tr>
                          <td valign="top" align="center">
                            <table width="552" cellpadding="0" cellspacing="0">
                              <tbody>
                                <tr>
                                  <!-- CONTENT start -->
                                  <td valign="top" align="left">
                                    <img src="http://revaxarts-themes.com/skyline/9/img/logo.png" alt="" title="" width="190" height="48" border="0" style="max-width:264px;">
                                  </td>
                                    <td width="24">&nbsp;</td>
                                    <td valign="middle" align="right" class="subline">
                                      <div>Newsletter with Template Builder</div>
                                    </td>
                                    <!-- CONTENT end -->
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table width="600" cellpadding="0" cellspacing="0" bgcolor="#4B9515">
                        <tbody>
                          <tr>
                            <td height="6">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                      <table width="600" cellpadding="0" cellspacing="0" class="contenttbl">
                        <tbody>
                          <tr>
                            <td height="24">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- ~ header block ends here -->
                      <!-- Full Size Image start -->
                      <table width="600" cellpadding="0" cellspacing="0" class="contenttbl">
                        <tbody>
                          <tr>
                            <td width="24">&nbsp;</td>
                            <td valign="top">
                              <!-- CONTENT start -->
                              <img src="http://images.revaxarts-themes.com/201456/552x312.jpg?gray" alt="" title="" width="552" height="312" border="0" style="max-width:552px;">
                                <!-- CONTENT end -->
                              </td>
                              <td width="24">&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="24" colspan="3">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                        <!-- Full Size Image end -->
                        <!-- Seperator start -->
                        <table width="600" cellpadding="0" cellspacing="0" class="contenttbl">
                          <tbody>
                            <tr>
                              <td valign="top" align="center">
                                <table width="552" cellspacing="0" cellpadding="0" class="line">
                                  <tbody>
                                    <tr>
                                      <td height="23">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!-- Seperator end -->
                        <!-- 1/1 Column start -->
                        <table width="600" cellpadding="0" cellspacing="0" class="contenttbl">
                          <tbody>
                            <tr>
                              <td width="24">&nbsp;</td>
                              <td valign="top" align="left">
                                <!-- CONTENT start -->
                                <div class="h">
                                  <div>Headline goes here</div></div>
                                  <div> Li Europan lingu es es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam <a target="_blank">vocabular</a>. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. </div>
                                  <div class="btn">
                                    <a target="_blank">
                                      <img src="http://revaxarts-themes.com/skyline/9/img/more.png" width="94" height="30" alt="read more" title="read more" border="0">
                                    </a>
                                  </div>
                                    <!-- CONTENT end -->
                                  </td>
                                  <td width="24">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td height="24" colspan="3">&nbsp;</td>
                                </tr>
                              </tbody>
                            </table>
                            <!-- 1/1 Column end -->
                            <!-- Footer start -->
                            <table width="600" cellpadding="0" cellspacing="0" class="footertbl">
                              <tbody>
                                <tr>
                                  <td valign="top" align="center">
                                    <table width="594" height="2" cellpadding="0" cellspacing="0" class="sheet">
                                      <tbody>
                                        <tr>
                                          <td height="2">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                    <table width="588" height="2" cellpadding="0" cellspacing="0" class="sheet">
                                      <tbody>
                                        <tr>
                                          <td height="2">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                    <table width="552" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td height="8">
                                        </td>
                                      </tr>
                                        <tr>
                                          <td class="small" align="left" valign="bottom">
                                            <div>You have received this email because you have subscribed to <a href="http://rxa.li/skyline" target="_blank">Skyline Media</a> as <a href="#" target="_blank">some@example.com</a>.<br>If you no longer wish to receive emails please <a href="#" target="_blank">unsubscribe</a>
                                          </div>
                                          <div>©2012 Your Company here, All rights reserved</div>
                                        </td>
                                        <td width="24">&nbsp;</td>
                                        <td class="small social" align="right" valign="top" width="187">
                                          <div>
                                            <a href="https://twitter.com">
                                              <img src="http://revaxarts-themes.com/skyline/9/img/twitter.png" alt="" title="" width="32" height="32" border="0" style="max-width:32px;max-height:32px;display:inline;">
                                            </a>
                                              <a href="https://facebook.com">
                                                <img src="http://revaxarts-themes.com/skyline/9/img/facebook.png" alt="" title="" width="32" height="32" border="0" style="max-width:32px;max-height:32px;display:inline;">
                                              </a>
                                                <a href="https://linkedin.com">
                                                  <img src="http://revaxarts-themes.com/skyline/9/img/linkedin.png" alt="" title="" width="32" height="32" border="0" style="max-width:32px;max-height:32px;display:inline;">
                                                </a>
                                              </div>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                    <tr>
                                      <td height="24">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table>
                                <!-- Footer end -->
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <script type="text/javascript"> var _paq = _paq || []; (function(u,d,t){ _paq.push(['setSiteId', 106],['setTrackerUrl', u+'piwik.php']); var g=d.createElement(t), s=d.getElementsByTagName(t)[0]; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s); })('//analysis.revaxarts.com/',document,'script'); </script>
                        <noscript>&lt;h1&gt;Your Javascript is turned off!&lt;/h1&gt;&lt;p&gt;Please turn it on to see this page.&lt;/p&gt;</noscript>
                        <a id="mod_scroll_top_btn" title="scroll to top" style="display: none; ">scroll to top</a>
                        <div id="mod_container" style="opacity: 0.7; left: -60px; ">
                          <ul id="mod_list">
                            <li>
                              <a data-version="1">
                                <img src="http://revaxarts-themes.com/skyline/_img/v1.png" alt="version 1" width="30" height="30">
                              </a>
                            </li>
                              <li>
                                <a data-version="2">
                                  <img src="http://revaxarts-themes.com/skyline/_img/v2.png" alt="version 2" width="30" height="30">
                                </a>
                              </li>
                                <li>
                                  <a data-version="3">
                                    <img src="http://revaxarts-themes.com/skyline/_img/v3.png" alt="version 3" width="30" height="30">
                                  </a>
                                </li>
                                  <li>
                                    <a data-version="4">
                                      <img src="http://revaxarts-themes.com/skyline/_img/v4.png" alt="version 4" width="30" height="30">
                                    </a>
                                  </li>
                                    <li>
                                      <a data-version="5">
                                        <img src="http://revaxarts-themes.com/skyline/_img/v5.png" alt="version 5" width="30" height="30">
                                      </a>
                                    </li>
                                      <li>
                                        <a data-version="6">
                                          <img src="http://revaxarts-themes.com/skyline/_img/v6.png" alt="version 6" width="30" height="30">
                                        </a>
                                      </li>
                                        <li>
                                          <a data-version="7">
                                            <img src="http://revaxarts-themes.com/skyline/_img/v7.png" alt="version 7" width="30" height="30">
                                          </a>
                                        </li>
                                          <li>
                                            <a data-version="8">
                                              <img src="http://revaxarts-themes.com/skyline/_img/v8.png" alt="version 8" width="30" height="30">
                                            </a>
                                          </li>
                                            <li>
                                              <a data-version="9">
                                                <img src="http://revaxarts-themes.com/skyline/_img/v9.png" alt="version 9" width="30" height="30">
                                              </a>
                                            </li>
                                              <li>
                                                <a data-version="10">
                                                  <img src="http://revaxarts-themes.com/skyline/_img/v10.png" alt="version 10" width="30" height="30">
                                                </a>
                                              </li>
                                                <li>
                                                  <a data-version="11">
                                                    <img src="http://revaxarts-themes.com/skyline/_img/v11.png" alt="version 11" width="30" height="30">
                                                  </a>
                                                </li>
                                                  <li>
                                                    <a data-version="12">
                                                      <img src="http://revaxarts-themes.com/skyline/_img/v12.png" alt="version 12" width="30" height="30">
                                                    </a>
                                                  </li>
                                                </ul>
                                                  <div id="mod_buttons">
                                                    <a id="mod_home" href="/skyline" title="back to homepage">&nbsp;</a>
                                                    <a id="mod_close" href="#" title="toggle options">&nbsp;</a>
                                                  </div></div>
                                                </body>
                                              </html>

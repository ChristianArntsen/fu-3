<?php $this->load->view("partial/header"); ?>
<style>
	#register_container {
    margin: auto;
    width: 962px;
}
#register_container #cart_contents .warning_message
{
	background: none;
	font-size: 16px;
}
#title_section td {
    vertical-align: middle;
}
#title_section, .title_section {
    font-size: 1.75em;
}
#title {
    font-size: 0.915em;
}
td#register_wrapper {
    background: url("../images/pieces/reg_mode.png") no-repeat scroll center center transparent;
    font-size: 0.8em;
    height: 49px;
    line-height: 0;
    text-align: center;
    width: 250px;
}
#register_wrapper form {
}
#register_wrapper span {
    font-size: 0.55em;
}
#customer_name .remove_customer,#supplier_name .remove_customer, .delete_item {
	background: url("../images/pieces/x2.png") no-repeat scroll center center transparent;
    height: 11px;
    width: 11px;
    display:block;
    overflow: hidden;
    text-indent: -10000px;
    float:left;
    margin:3px;
    padding:0px;
}
#customer_name .remove_customer:hover,#supplier_name .remove_customer:hover, .delete_item:hover {
	background: url("../images/pieces/x_red2.png") no-repeat scroll center center transparent;
}
.delete_item {
	padding:5px;
}
#customer_name .edit_customer, #supplier_name .edit_customer {
	font-size:10px;
}
#show_suspended_sales_button {
    background: url("../images/pieces/view_suspended.png") no-repeat scroll right center transparent;
    font-size: 0.8em;
    height: 49px;
    line-height: 0;
    text-align: center;
    width: 230px;
}
#show_suspended_sales_button a {
    color: #FFFFFF;
    font-size: 0.7em;
}
#suspended_sales_table {
	font-size: 1.2em;
}

#suspended_sales_table  input[type="submit"]{
    background: url("../images/pieces/btnUpdate.png") no-repeat top left transparent;
    color: #FFFFFF;
    display: block;
    height: 24px;
	font-size: 12px;
	margin: 0 auto;
	border: 0px;
	width: 70px;
    text-align: center;
}

#register_items_container {
    width: 732px;
}
#over_all_sale_container {
    width: 220px;
    height: 590px;
}
#reg_item_search {
    background: url("../images/pieces/reg_item_search.png") no-repeat scroll -9px -9px transparent;
    height: 30px;
    padding: 0px;
}
#reg_item_search form {
    vertical-align: middle;
}
#reg_item_search input {
    background: url("../images/pieces/reg_item_search.png") no-repeat scroll -20px -20px transparent;
    border: medium none;
    font-size: 1.1em;
    margin-top: 5px;
	width: 430px;
	padding: 6px 10px;
	margin-left: 5px;
	background: transparent;
	color:#666464;
}
div#new_item_button_register {
    background:none;/* url("../images/pieces/reg_item_search.png") repeat scroll -693 -80px transparent;*/
    float: right;
    font-size: 0.8em;
    height: 30px;
    width: 35px;
}
#new_item_button_register a {
    color: #FFFFFF;
}
#overall_sale {
	box-shadow:inset 0px 0px 0px 1px rgba(0,0,0,0.4), 0px 2px -1px 0px rgba(255,255,255,0.2);
	border:none;
	background:rgba(0,0,0,0.2);
	padding:6px;
	width:220px;
}
#new_item_button_register a div {
    padding-left: 20px;
    padding-top: 5px;
}
#register_items_container, #over_all_sale_container {
    vertical-align: top;
}
#receivings #register_holder {
    background: white;/*url("../images/pieces/item_empty_large.png") repeat scroll 0 -30px transparent;*/
    height: auto;
	overflow: hidden;
    width: 732px;
    box-shadow:0 1px 0 0 #CDCDCD, 0 3px 0 -1px white, 0 4px 0 -1px #CDCDCD, 0 7px 0 -3px white, 0 8px 0 -3px #CDCDCD, 0 12px 0 -5px white, 0 3px 14px -2px black, 0 14px 14px 0px black;
    width:100%;
}
#register th {
    background-image: url("../images/pieces/table_body.png");
    background-position: -15px -80px;
    border-right: 1px solid #D0D8DF;
    color: #FFFFFF;
    font-size: 0.7em;
    height: 30px;
    text-align: left;
    text-indent: 10px;
    vertical-align: middle;
}
th.reg_item_select {
    width: 5%;
}
.reg_item_select input {
    margin: 0 10px 0 0;
}
th.reg_item_del {
    width: 60px;
}
th.reg_item_name {
    width: 48%;
}
th.reg_item_number {
    width: 110px;
}
th.reg_item_stock {
    width: 8%;
}
th.reg_item_price {
    width: 12.5%;
}
th.reg_item_qty {
    width: 7%;
}
th.reg_item_discount {
    width: 8.5%;
}
th.reg_item_total {
    width: 11%;
}
th.reg_item_update {
    border-right: medium none;
    width: 90px;
}
#register td {
    border-right: 1px solid #BBBBBB;
    color: #555555;
    font-size: 0.7em;
    height: 30px;
    /*text-align: left;*/
    text-indent: 10px;
    vertical-align: middle;
}
#register td.reg_item_price, #register td.reg_item_qty, #register td.reg_item_discount {
    text-indent:3px;
}
.reg_item_bottom td {
    height:20px;
}
tr.reg_item_top {
    background: url("../images/pieces/item_full_large.png") repeat scroll left top transparent;
    border-bottom: 1px solid #BBBBBB;
}
tr.reg_item_top.even {
    background-position:left bottom;
}
tr.reg_item_bottom {
    background: url("../images/pieces/item_full_large.png") repeat scroll left bottom transparent;
    border-bottom: 1px solid #BBBBBB;
}
td.reg_item_del {
}
td.reg_item_name select, td.reg_item_type select {
    width:332px;
}
td.reg_item_number {
}
td.reg_item_stock {
}
td.reg_item_price {
    text-indent: 3px;
}
td.reg_item_price input {
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    padding: 4px 7px;
    width: 68px;
    text-align: right;
}
td.reg_item_qty {
    text-indent: 3px;
}
td.reg_item_qty input {
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    padding: 4px 7px;
    width: 28px;
}
td.reg_item_discount {
    text-indent: 3px;
}
td.reg_item_discount input {
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    padding: 4px 7px;
    width: 38px;
}
td.reg_item_total {
    background: none repeat scroll 0 0 #CCFFCC;
    font-size: 0.8em;
    font-weight: bold;
    padding: 0 10px 0 0;
    text-align: right;
}
td.reg_item_update {
    border-right: medium none;
}
td.reg_item_descrip_label {
    border-right: medium none;
    padding-left:40px;
}
td.reg_item_descrip {
    text-indent: 3px;
}
td.reg_item_descrip input {
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    padding: 4px 7px;
    width: 350px;
}
td.reg_item_serial_label {
    border-right: medium none;
}
td.reg_item_serial {
    border-right: medium none;
    text-indent: 3px;
}
td.reg_item_serial input {
    border: 1px solid #CCCCCC;
    border-radius: 5px 5px 5px 5px;
    padding: 4px 7px;
    width: 210px;
}
.reg_item_base {
    background: url("../images/pieces/item_base.png") no-repeat scroll left bottom transparent;
    height: 10px;
}
#receivings #cancel, #receivings #customer_remove a, #receivings #supplier_remove a {
    color: #FFFFFF;
    background:-webkit-linear-gradient(top,#d14d4d,#c03939);
}
.receiving #cancel {
	height:40px;
}
#suspend {
    background: url("../images/pieces/sus_can.png") no-repeat scroll left top transparent;
    float: left;
    height: 20px;
    width: 110px;
}
#cancel {
    background: url("../images/pieces/sus_can.png") no-repeat scroll right top transparent;
    float: left;
    height: 20px;
    width: 110px;
}
#suspend div, #cancel div {
    /*cursor: pointer;
    height: 20px;
    text-indent: 10px;
    width: 110px;*/
}
#suspend div {
    background: url("../images/pieces/sus_can.png") no-repeat scroll left bottom transparent;
}
#cancel div {
    /*background: url("../images/pieces/sus_can.png") no-repeat scroll right bottom transparent;*/
}
#customer_account_balance {
	font-weight:bold;
}
.alt_payment_button {
	color:white;
	background:#336699;
	border:1px solid white;
	border-radius:6px;
	cursor:pointer;
	font-size:10px;
	padding:5px 10px;
	margin-right:6px;
}
#customer_info_filled, #supplier_info_filled {
    background: white;/*url("../images/pieces/add_customer_info_filled.png") no-repeat scroll -1px -1px transparent;*/
    font-size: 0.8em;
    height: auto;
    margin-bottom: 10px;
    padding: 5px 10px;
    width: 200px;
    border:none;
}
#customer_info_filled {
}
#customer_edit a, #customer_remove a, #supplier_edit a, #supplier_remove a {
	background: #349ac5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349ac5, #4173b3);
	background: -moz-linear-gradient(top, #349ac5, #4173b3);
	color: white;
	display: block;
	font-size: 14px;
	font-weight: normal;
	height: 30px;
	line-height: 30px;
	padding: 0px 20px 0 20px;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	margin-bottom: 10px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
	margin: 0px auto 10px;
	width: 150px;
	cursor: pointer;
}
#customer_info_empty, #supplier_info_empty {
	padding:0px;
    background: none;/*url("../images/pieces/add_customer_info.png") no-repeat scroll -1px -1px transparent;*/
    /*font-size: 0.8em;
    height: 90px;
    margin-bottom: 10px;
    padding: 5px 10px;
    width: 202px;*/
}
#customer_info_empty {
	/*background-image:url("../images/pieces/add_customer_info4.png");
	height:50px;*/
	background:none;
}
#customer_label, #supplier_label {
    display: block;
    margin-bottom: 10px;
    font-size: 0.9em;
}
#customer_info_shell #customer, #supplier_info_shell #supplier {
    background: white;/* none repeat scroll 0 0 transparent;*/
    border: medium none;
    padding: 0 5px;
    width: 163px;
}
#add_customer_info {
}
#add_customer_info #common_or, #add_supplier_info .common_or {
    float: left;
    margin-top: 20px;
    width: 30px;
}
#add_customer_info a, #add_supplier_info a {
    color: #FFFFFF;
    height: 20px;
    margin: 0 0 0;
    padding: 0px;
}
#customer_email, #supplier_email {
	height:auto;
}
#customer_email{
}
#email_customer{
    font-size: 0.75em;
    padding-left: 10px;
    /*position: absolute;*/
    top: 155px;
    width: 200px;
/*	margin-top: 30px;*/
	height:50px;
}

#supplier, #customer {
    background: none repeat scroll 0 0 transparent;
    border: medium none;
    padding: 0 5px;
    width: 163px;
}

#customer_edit a, #supplier_edit a{
	float: left;
    text-align: center;
    width: 50px;
	color:#fff;
	display:block;
	height:25px;
	line-height:25px;
}
#customer_remove{
}
#customer_remove a, #supplier_remove a{
	float: left;
    text-align: center;
    width: 50px;
    margin-left:10px;
	color:#fff;
	display:block;
	height:25px;
	line-height:25px;
}
.payment_button {
	display:block;
	height:30px;
	width:44px;
	background:transparent url("../images/sales/payment.png") no-repeat;
	float:left;
	margin:4px 0 4px 8px;
	cursor:pointer;
}
#payment_credit_card {
	background-position:0px 0px;
}
#payment_cash {
	background-position:-53px 0px;
}
#payment_gift_card {
	background-position:-105px 0px;
}
#payment_check {
	background-position:-157px 0px;
}
#sale_details {
    float:right;
    width:220px;
}
.receivings_total #sale_details {
	float:none;
}
#sale_details #sales_items {
    background: url("../images/pieces/subtotals.png") no-repeat scroll left top transparent;
    font-size: 0.7em;
}
.sales #sale_details #sales_items_total {
    background: url("../images/pieces/subtotals.png") no-repeat scroll left bottom transparent;
    border-top: 1px solid #AAAAAA;
    font-size: 1.1em;
    font-weight: bold;
    margin-bottom: 10px;
    width: 220px;
}
.receiving #sale_details #sales_items_total {
    background: white;
    border-bottom: 1px solid #AAAAAA;
    font-size: 1.1em;
    font-weight: bold;
    margin-bottom: 0px;
    width: 220px;
}
#sale_details .left {
    height: 20px;
    padding: 0 0 0 10px;
    text-align: left;
    vertical-align: bottom;
}
#sale_details .right {
    height: 20px;
    padding: 0 10px 0 0;
    text-align: right;
    vertical-align: bottom;
}
#sale_details #sales_items_total td {
    height: 30px;
    padding: 0 10px;
    vertical-align: middle;
}
#Payment_Types #register {
    background: url("../images/pieces/payments.png") no-repeat scroll left top transparent;
}
#Payment_Types #register th {
    background: none repeat scroll 0 0 transparent;
    font-size: 0.6em;
    font-weight: normal;
    height: 20px;
}
th#pt_amount {
    border: medium none;
}
#Payment_Types #register td {
    background: none repeat scroll 0 0 #FFFFFF;
    height: 20px;
    padding-bottom: 0;
    padding-top: 0;
}
td#pt_delete {
    width: 20px;
}
td#pt_delete a {
    background: url("../images/pieces/x.png") no-repeat scroll center center transparent;
    display: block;
    height: 20px;
    text-indent: -9999px;
    width: 20px;
}
td#pt_delete a:hover {
    background: url("../images/pieces/x_red.png") no-repeat scroll center center transparent;
    display: block;
    height: 20px;
    text-indent: -9999px;
    width: 20px;
}
td#pt_amount {
    border: medium none;
    padding-right: 10px;
    text-align: right;
}
#amount_due {
    border-bottom: 1px solid #BBBBBB;
    border-top: 1px solid #BBBBBB;
    width: 220px;
}
#amount_due td {
    background: none repeat scroll 0 0 #FFFF88;
    height: 25px;
    padding: 0 10px;
}
#amount_due .covered td {
    background: none repeat scroll 0 0 #88FF88;
}
#make_payment, #shipping_invoice {
    background:none;/* url("../images/pieces/payments.png") no-repeat scroll left bottom transparent;*/
    font-size: 0.95em;
}
#shipping_invoice {
	padding:0px;
	margin-bottom:0px;
	border-radius:0px;
	background:none;
	border:none;
	width:100%;
}
#make_payment_table, #shipping_invoice_table {
    margin: 0px;
    width: 200px;
}
#make_payment_table td {
    vertical-align: middle;
}
#mpt_top {
    height: 30px;
}
#mpt_top td {
    vertical-align: middle;
}
#mpt_bottom {
    height: 40px;
}
#mpt_bottom td {
    vertical-align: top;
}
#encrypted_data {
	color:white;
}
#add_payment_button span {
    background: url("../images/pieces/add_payment.png") no-repeat scroll -10px -10px transparent;
    color: #FFFFFF;
    display: block;
    height: 25px;
    margin: 0 auto;
    padding-top: 5px;
    text-align: center;
    width: 200px;
    cursor:pointer;
}
a#add_payment_button {
    background: url("../images/pieces/add_payment.png") no-repeat scroll -10px -10px transparent;
    color: #FFFFFF;
    display: block;
    height: 25px;
    margin: 0 auto;
    padding-top: 5px;
    text-align: center;
    width: 200px;
    cursor:pointer;
}
#add_payment_button {
    padding-bottom: 10px;
}
#make_payment #tender {
    text-align: center;
}
#make_payment #tender input, #shipping_invoice input {
    border: 1px solid #999999;
    border-radius: 4px 4px 4px 4px;
    font-size: 1.2em;
    padding: 2px 5px;
    text-align: right;
    width: 190px;
}
#invoice_number {
	margin-bottom:5px;
}
#comment_label{
    font-size: 0.75em;
    display:none;
}
textarea#comment{
	width: 220px;
	border-radius: 5px 5px 5px 5px;
	border: 1px solid #CCCCCC;
	background: none repeat scroll 0 0 transparent;
	min-height: 60px;
	display:none;
}

#finish_sale_button span{
    /*background: url("../images/pieces/complete_sale.png") no-repeat scroll -10px -10px transparent;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    height: 25px;
    padding-top: 5px;
    text-align: center;
    width: 220px;*/
}
#finish_sale_no_receipt_button span{
    background: url("../images/pieces/complete_sale.png") no-repeat scroll -10px -10px transparent;
    color: #FFFFFF;
    cursor: pointer;
    display: block;
    height: 25px;
    padding-top: 5px;
    text-align: center;
    width: 220px;
}

/* Sales */
#suspend_cancel > #suspend{
	visibility: hidden;
}

#suspend_cancel > #cancel{
	visibility: hidden;
        width:90%;
}

.reg_item_update input[type="submit"]{
    background: url("../images/pieces/btnUpdate.png") no-repeat top left transparent;
    color: #FFFFFF;
    display: block;
    height: 24px;
	margin: 0 auto;
	border: 0px;
	width: 70px;
    text-align: center;
}
#delete_button, #update_all_button, #cancel{
    background:none;/* url("../images/pieces/buttons.png") no-repeat -10px -210px ;*/
    color: #FFFFFF;
    display: block;
    font-size: 0.7em;
    font-weight: bold;
    height: 31px;
    padding: 7px 0 0 10px;
    width: 160px;
}
#cancel, #delete_button {
    background-position: -10px -170px;
    font-size:12px;
}
#delete_button {
    margin-top:10px;
}
#update_all_button {
    background-position: -10px -290px;
}
#add_payment_text
{
	font-size: .85em;
}
#payment_types
{
    font-size: 1.50em;
}
#customer_info_shell #customer {
	font-size:.8em;
	background:white;
}
/* INPUTS */
#receivings #supplier, #receivings #customer, #receivings #invoice_number, #receivings #shipping_cost, #receivings #amount_tendered, #receivings #make_payment #tender input {
	box-shadow:inset 0px 6px 24px -12px black;
	border-radius:5px;
	border:1px solid #b1b1b1;
	padding:4px;
	line-height:20px;
	color:#666464;
	font-size:14px;
	font-weight:lighter;
	font-family: "Quicksand", Helvetica, sans-serif;
	width:210px;
	height:20px;
	margin:auto;
}
/* BUTTONS */
#receivings #cancel {
	line-height:0px;
	width: 208px;
	margin-top: 5px;
	line-height: 20px;
	height: 25px;
}
#receivings #supplier {
	margin-bottom:6px;
}
#receivings #add_supplier_info, #receivings #finish_sale_button span {
	width:178px;
	margin:0px;
}
#sale_details {
	padding:0px;
	margin:0px;
}
#reg_item_top {
	border-bottom: 1px solid #ccc;
	font-size: 21px;
}
#add_item_form {
	width:500px;
}
#mode_form {
	float:right;
}
#mode {
	margin:9px 20px 0px 0px;
}
#receivings .add_icon_blue {
	margin:5px 0px 0px 0px;
}
#customer_name, #supplier_name {
	font-size:18px;
	margin-bottom:5px;
}
#receivings #invoice_number, #receivings #shipping_cost {
	margin-bottom:6px;
}
.clear {
	clear:both;
}
#register {
	margin-top:0px;
}
#register th {
	background-image:none;
	background:#818181;
	font-size:12px;
	height:20px;
	line-height:25px;
	border-right:1px solid #bbb;
}
</style>
<div id="register_container" class="receiving">
<table>
	<tr>
		<td id="register_items_container">
			<div id="reg_item_search">
				<?php echo form_open("receivings/add",array('id'=>'add_item_form')); ?>
					<?php echo form_input(array('name'=>'item','id'=>'item','size'=>'30','placeholder'=>'Type item name or scan barcode...'));?>
					<div id="new_item_button_register" >
						<?php echo anchor("items/view/-1/width~1100",
						"<div class='small_button add_icon_blue'><span></span></div>",
						array('class'=>'colbox none','title'=>lang('sales_new_item')));?>
					</div>
				</form>
				<?php echo form_open("receivings/change_mode",array('id'=>'mode_form')); ?>
				<?php echo form_dropdown('mode',$modes,$mode,'onchange="$(\'#mode_form\').submit();"'); ?>
				</form>
			</div>
			<div id="register_holder">
			<table id="register">
				<thead>
					<tr>
<th id="reg_item_del"></th>
<th id="reg_item_name"><?php echo lang('receivings_item_name'); ?></th>
<th id="reg_item_price"><?php echo lang('receivings_cost'); ?></th>
<th id="reg_item_qty"><?php echo lang('receivings_quantity'); ?></th>
<th id="reg_item_discount"><?php echo lang('receivings_discount'); ?></th>
<th id="reg_item_total"><?php echo lang('receivings_total'); ?></th>
<th id="reg_item_update"></th>
					</tr>
				</thead>
				<tbody id="cart_contents">
<?php
if(count($cart)==0)
{
?>
<tr><td colspan='7' style="height:60px;border:none;">
<div class='warning_message' style='padding:7px;'><?php echo lang('sales_no_items_in_cart'); ?></div>
</td></tr>
<?php
}
else
{
	foreach(array_reverse($cart, true) as $line=>$item)
	{
		$cur_item_info = $this->Item->get_info($item['item_id']);
		echo form_open("receivings/edit_item/$line");
	?>
		<tr id="reg_item_top">
			<td id="reg_item_del"><?php echo anchor("receivings/delete_item/$line",lang('common_delete'));?></td>
			<td id="reg_item_name"><?php echo $item['name']; ?></td>
		<?php if ($items_module_allowed){ ?>
			<td id="reg_item_price"><?php echo form_input(array('name'=>'price','value'=>$item['price'],'size'=>'6'));?></td>
		<?php }else{ ?>
			<td id="reg_item_price"><?php echo $item['price']; ?></td>
		<?php echo form_hidden('price',$item['price']); ?>
		<?php }	?>
			<td id="reg_item_qty">
		<?php echo form_input(array('name'=>'quantity','value'=>$item['quantity'],'size'=>'2'));?>
			</td>
			<td id="reg_item_discount"><?php echo form_input(array('name'=>'discount','value'=>$item['discount'],'size'=>'3'));?></td>
			<td id="reg_item_total"><?php echo to_currency($item['price']*$item['quantity']-$item['price']*$item['quantity']*$item['discount']/100); ?></td>
			<td id="reg_item_update"><?php echo form_submit("edit_item", lang('sales_update_item'));?></td>
		</tr>
		<!--tr id="reg_item_bottom">
			<td id="reg_item_descrip_label"><?php echo lang('sales_description_abbrv').':';?></td>
			<td id="reg_item_descrip" colspan="6">
		<?php
			echo $item['description'];
        	echo form_hidden('description',$item['description']);
        ?>
			</td>
		</tr-->
	</form>
	<?php
	}
}
?>
				</tbody>
			</table>
			</div>
			<div id="reg_item_base"></div>
		</td>
		<td style="width:8px;"></td>
		<td id="over_all_sale_container">
			<div id="overall_sale">


				<div id="supplier_info_shell">
					<?php
					if(isset($supplier))
					{
						echo "<div id='supplier_info_filled'>";
							echo '<div id="supplier_label_box">Supplier:</div>';
							echo '<div id="supplier_name">'.character_limiter($supplier, 25).'</div>';
							echo '<div id="supplier_email"></div>';
							echo '<div id="supplier_edit">'.anchor("suppliers/view/$supplier_id/width~550", lang('common_edit'),  array('class'=>'colbox none','title'=>lang('suppliers_update'))).'</div>';
							echo '<div id="supplier_remove">'.anchor("receivings/delete_supplier", lang('sales_detach')).'</div>';
							echo '<div class="clear"></div>';
						echo "</div>";
					}
					else
					{ ?>
						<div id='supplier_info_empty'>
							<?php echo form_open("receivings/select_supplier",array('id'=>'select_supplier_form')); ?>
							<?php echo form_input(array('name'=>'supplier','id'=>'supplier','size'=>'30','value'=>'', 'placeholder'=>lang('receivings_start_typing_supplier_name')));?>
							</form>
							<!--div id="add_supplier_info">
								<?php
									echo anchor("suppliers/view/-1/width~550",
									"<div class='small_button' style='margin:0 auto;'><span>".lang('receivings_new_supplier')."</span></div>",
									array('class'=>'colbox none','title'=>lang('receivings_new_supplier')));
								?>
							</div-->
						</div>
					<?php } ?>
				</div>
				<div id="customer_info_shell">
					<?php
					if(isset($customer))
					{
						echo "<div id='customer_info_filled'>";
							echo '<div id="reference">Reference:</div>';
							echo '<div id="customer_name">'.character_limiter($customer, 25).'</div>';
							echo '<div id="customer_email"></div>';
							echo '<div id="customer_edit">'.anchor("customers/view/$customer_id/width~550", lang('common_edit'),  array('class'=>'colbox none','title'=>lang('suppliers_update'))).'</div>';
							echo '<div id="customer_remove">'.anchor("receivings/delete_reference", lang('sales_detach')).'</div>';
							echo '<div class="clear"></div>';
						echo "</div>";
					}
					else
					{ ?>
						<div id='customer_info_empty'>
							<?php echo form_open("receivings/select_reference",array('id'=>'select_reference_form')); ?>
							<?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','value'=>'','placeholder'=>lang('receivings_start_typing_reference_name')));?>
							</form>

							<div class="clearfix">&nbsp;</div>
						</div>
					<?php } ?>
				</div>
				<?php echo form_open("receivings/complete",array('id'=>'finish_sale_form')); ?>
				<?php echo form_hidden('reference_name', '');?>
				<div id='shipping_invoice'>
					<table id="shipping_invoice_table">
						<tr>
							<td class="left">
								<?php echo form_input(array('name'=>'invoice_number','id'=>'invoice_number','value'=>'','placeholder'=>lang('receivings_invoice_number'),'size'=>'5')); ?></td>
						</tr>
						<tr>
							<td class="left">
								<?php echo form_input(array('name'=>'shipping_cost','id'=>'shipping_cost','value'=>'','placeholder'=>lang('receivings_shipping_cost'),'size'=>'5')); ?></td>
						</tr>
					</table>
				</div>
				<div class='receivings_total'>
					<div id='sale_details'>
						<table id="sales_items_total">
							<tr>
								<td class="left"><?php echo lang('sales_total'); ?>:</td>
								<td class="right"><?php echo to_currency($total); ?></td>
							</tr>
						</table>
					</div>
				</div>
				<?php
				// Only show this part if there are Items already in the Table.
				if(count($cart) > 0){ ?>

					<div id="finish_sale">
						<div id="make_payment" >
							<table id="make_payment_table">
								<tr id="mpt_top">
									<td>
										<?php echo form_dropdown('payment_type',$payment_options);?>
									</td>
								</tr>
								<tr id="mpt_bottom">
									<td id="tender" colspan="2">
										<?php echo form_input(array('name'=>'amount_tendered','value'=>'','placeholder'=>'Payment Amount','size'=>'10')); ?>
									</td>
								</tr>
							</table>
						</div>

						<!--label id="comment_label" for="comment"><?php echo lang('common_comments'); ?>:</label-->
						<?php //echo form_textarea(array('name'=>'comment', 'id' => 'comment', 'value'=>'','rows'=>'4'));?>

						<?php echo "<div class='small_button' id='finish_sale_button' style='float:right;margin-top:5px;'><span>".lang('receivings_complete_receiving')."</span></div>"; ?>
					</div>
					<div id="suspend_cancel">
						<div id="cancel" <?php if(count($cart) > 0){ echo "style='visibility: visible;'";}?>>
							<?php
							// Only show this part if there are Items already in the sale.
							if(count($cart) > 0){ ?>
								<?php echo form_open("receivings/cancel_receiving",array('id'=>'cancel_sale_form')); ?>
									<div class='small_button' id='cancel_sale_button'>
										<span>
											<?php
											echo lang('receivings_cancel_receiving');
											?>
										</span>
									</div>
								</form>
							<?php } ?>
						</div>
					</div>

				<?php } ?>



				<div class='clear'></div>
			</div><!-- END OVERALL-->
		</td>
	</tr>
</table>
<div id="feedback_bar"></div>

<script type="text/javascript">
<?php
if(isset($error))
{
	echo "set_feedback('$error','error_message',false);";
}

if (isset($warning))
{
	echo "set_feedback('$warning','warning_message',false);";
}

if (isset($success))
{
	echo "set_feedback('$success','success_message',false);";
}
?>
</script>

</div>

<?php $this->load->view("partial/footer"); ?>


<script type="text/javascript" language="javascript">
var item_total = '<?php echo $total ?>';

$(document).ready(function()
{
	$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
	$('.colbox').colorbox({'maxHeight':700, 'width':550});
	$( "#item" ).autocomplete({
		source: '<?php echo site_url("receivings/item_search"); ?>',
		delay: 150,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)
		{
			$( "#item" ).val(ui.item.value);
			$("#add_item_form").submit();
		}
	});

	$('#item').focus();

	$('#shipping_cost').keyup(function () {
		$('#sales_items_total .right').html('$'+(parseFloat(item_total)+parseFloat($('#shipping_cost').val())).toFixed(2));
	});
	$('#supplier').click(function()
    {
    	$(this).attr('value','');
    });
    $('#customer').click(function()
    {
    	if ($(this).val() == "<?php echo lang('receivings_start_typing_reference_name'); ?>")
    		$(this).attr('value','');
    	else
    		console.log($(this).val()+ "<?php echo lang('receivings_start_typing_reference_name');?>");
    });


	$( "#supplier" ).autocomplete({
		source: '<?php echo site_url("receivings/supplier_search"); ?>',
		delay: 150,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)
		{
			$( "#supplier" ).val(ui.item.value);
			$("#select_supplier_form").submit();
		}
	});
	$( "#customer" ).autocomplete({
		source: '<?php echo site_url("receivings/reference_search"); ?>',
		delay: 200,
		autoFocus: false,
		minLength: 1,
		select: function(event, ui)
		{
			$( "#customer" ).val(ui.item.value);
			$("#select_reference_form").submit();
		}
	});

    $('#supplier').blur(function()
    {
    	$(this).attr('value',"<?php echo lang('receivings_start_typing_supplier_name'); ?>");
    });
    $('#customer').blur(function()
    {
    	if ($('#customer').val().trim() == '')
	    	$(this).attr('value',"<?php echo lang('receivings_start_typing_reference_name'); ?>");
    });

    $("#finish_sale_button").click(function()
    {
    	if (confirm('<?php echo lang("receivings_confirm_finish_receiving"); ?>'))
    	{
    		if ($('#customer').val() !== "<?php echo lang('receivings_start_typing_reference_name'); ?>")
    			$('input[name=reference_name]').val($('#customer').val());
    		$('#finish_sale_form').submit();
    	}
    });

    $("#cancel_sale_button").click(function()
    {
    	if (confirm('<?php echo lang("receivings_confirm_cancel_receiving"); ?>'))
    	{
    		$('#cancel_sale_form').submit();
    	}
    });


});

function post_item_form_submit(response)
{
	if(response.success)
	{
		$("#item").attr("value",response.item_id);
		$("#add_item_form").submit();
	}
}

function post_person_form_submit(response)
{
	if(response.success)
	{
		$("#supplier").attr("value",response.person_id);
		$("#select_supplier_form").submit();
	}
}
function post_reference_form_submit(response)
{
	if(response.success)
	{
		$("#customer").attr("value",response.person_id);
		$("#select_customer_form").submit();
	}
}

</script>
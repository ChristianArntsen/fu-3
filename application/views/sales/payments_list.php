<div class='payments_and_tender'>
	<?php
	foreach($payments as $payment_id=>$payment)
	{
		$invoice_id = $payments[$payment_id]['invoice_id'];
		if ($invoice_id != '')
		{
			echo "<a href='javascript:mercury.delete_payment(\"".$payment_id."\", \"".to_currency($payment['payment_amount'])."\")'><span class='payment_currency'>".to_currency($payment['payment_amount'])."</span><span class='payment_label'>".$payment['payment_type'].":</span><div class='clear'></div></a>";
		}
		else if (stripos($payment_id, 'Gift') !== false && $this->config->item('use_ets_giftcards') == 1)
		{
			echo form_open("sales/edit_payment/$payment_id",array('id'=>'edit_payment_form'.$payment_id));
			echo "<a class='delete_item' href='javascript:mercury.delete_ets_giftcard_payment(\"".$payment_id."\", \"".$payment['payment_amount']."\")'></a><span class='payment_currency'>".to_currency($payment['payment_amount'])."</span><span class='payment_label'>".$payment['payment_type'].":</span><div class='clear'></div>";
		}		
		else if ($payment_id != 'Raincheck')
		{
			echo form_open("sales/edit_payment/$payment_id",array('id'=>'edit_payment_form'.$payment_id));
			echo "<a class='delete_item' href='javascript:mercury.delete_payment(\"".$payment_id."\", \"".$payment['payment_amount']."\")'></a><span class='payment_currency'>".to_currency($payment['payment_amount'])."</span><span class='payment_label'>".$payment['payment_type'].":</span><div class='clear'></div>";
		?>
			</form>
	<?php
		}
	}
	if ($raincheck_info) {?>
		<?php echo anchor("sales/remove_raincheck/",
			'<div><span class="payment_currency">&nbsp;</span><span class="payment_label">Raincheck:</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['gf_used']).'</span><span class="payment_label">Green Fees ('.to_currency($raincheck_info['gf_total']).')</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['cf_used']).'</span><span class="payment_label">Cart Fees ('.to_currency($raincheck_info['cf_total']).')</span><div class="clear"></div></div>'.
			'<div><span class="payment_currency">'.to_currency($raincheck_info['tax_used']).'</span><span class="payment_label">Taxes ('.to_currency($raincheck_info['tax_total']).')</span><div class="clear"></div></div>'
			);
 	} ?>
 	<div class='due_amount' id='due_amount'><?php echo to_currency($basket_amount_due);?></div>
 	<div class="float_left due_amount_label"><?php echo lang('sales_amount_due'); ?>:</div>
	<?php
	if ($mode == 'return' && count($sale_payments) > 0)
	{
		echo "<div class='header'>Payments collected for sale $sale_id:</div>";
		foreach ($sale_payments as $sale_payment)
		{
			echo "<div class='form_field'>".$sale_payment['payment_type'].' - $'.$sale_payment['payment_amount'];
			if ($sale_payment['invoice_id'] && strpos($sale_payment['payment_type'], 'Tip') === false)
				echo " (<a title='Refund to CC' class='colbox' href='index.php/sales/confirm_refund/{$sale_payment['invoice_id']}'>Refund to CC</a>)";
			echo "</div>";
		}
	}
	?>
</div>

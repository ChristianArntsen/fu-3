<div id="add_tip_wrapper">
	<?php echo form_hidden('sale_id', $payment_info['sale_id']);?>
	<fieldset>
	<?php echo form_open("sales/add_tip/".$sale_id."/".$payment_info['invoice_id'],array('id'=>'add_tip_form')); ?>
	<ul id="error_message_box"></ul>
	<?php
	// HIDE CREDIT CARD DATA IF NO INVOICE ID IS PROVIDED & ALLOW TIP PAY TYPE
	if ($payment_info['invoice_id']) {
	?>
	<?php if ($credit_card_info['cardholder_name'] != '') { ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_cardholder_name').':', 'cardholder_name'); ?>
		<div class='form_field'>
			<?php echo $credit_card_info['cardholder_name'];?>
		</div>
	</div>
	<?php } ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_credit_card').':', 'credit_card'); ?>
		<div class='form_field'>
			<?php echo $credit_card_info['card_type'].' '.$credit_card_info['masked_account'];//form_input(array('name'=>'credit_card','value'=>$credit_card_info['cardholder_name'], 'id'=>'credit_card'));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_total').':', 'total'); ?>
		<div class='form_field'>
			<?php echo $credit_card_info['amount'];//form_input(array('name'=>'credit_card','value'=>$credit_card_info['cardholder_name'], 'id'=>'credit_card'));?>
		</div>
	</div>
	<?php } else { ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_credit_card').':', 'credit_card'); ?>
		<div class='form_field'>
			<span class='tip_type_buttonset'>
	            <input type='radio' id='tip_cash' name='tip_type' value='Cash' <?php echo (!$payment_info['type'] || $payment_info['type'] == 'cash')?'checked':''; ?>/>
	            <label for='tip_cash' id='tip_cash_label'>Cash</label>
	            <?php if($this->config->item('mercury_id') == '' && $this->config->item('ets_key') == '') { ?>
	            <input type='radio' id='tip_credit' name='tip_type' value='Credit Card'  <?php echo ($payment_info['type'] == 'cc')?'checked':''; ?>/>
	            <label for='tip_credit' id='tip_credit_label'>Card</label>
	            <?php } ?>
	            <input type='radio' id='tip_check' name='tip_type' value='Check'  <?php echo ($payment_info['type'] == 'check')?'checked':''; ?>/>
	            <label for='tip_check' id='tip_check_label'>Check</label>
	        </span>
	 	</div>
	</div>
	<script>
		$('#tip_cash').button();
    	$('#tip_credit').button();
    	$('#tip_check').button();
    	$('.tip_type_buttonset').buttonset();
    </script>
    <div class="field_row clearfix">
	<?php echo form_label(lang('sales_existing_tips').':', 'tip_amount'); ?>
		<div class='form_field'>
		</div>
	</div>
    <?php
     foreach ($tip_info->result_array() as $tip) { ?>
    <div class="field_row clearfix">
    	<span class='tip_type_label'>
	<?php echo form_label($tip['payment_type'].':', 'tip_amount'); ?>
		</span>
		<div class='form_field'>
			<?php echo form_label($tip['payment_amount'], 'tip_amount');?>
		</div>
	</div>
 	<?php } }
	// END HIDE CREDIT CARD DATA
	?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_tip_amount').':', 'tip_amount'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'tip_amount','value'=>$credit_card_info['gratuity_amount'], 'id'=>'tip_amount'));?>
		</div>
	</div>
	<?php
	// HIDE CREDIT CARD DATA IF NO INVOICE ID IS PROVIDED & ALLOW TIP PAY TYPE
	if ($payment_info['invoice_id']) {
	?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_new_total').':', 'new_total'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'new_total','value'=>$credit_card_info['amount']+$credit_card_info['gratuity_amount'], 'readonly'=>'readonly', 'id'=>'new_total'));?>
		</div>
	</div>
	<?php } ?>
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_tip_recipient').':', 'tip_recipient'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'employee','value'=>$employee->first_name.' '.$employee->last_name, 'id'=>'employee'));?>
			<?php echo form_hidden('tip_recipient', $employee->person_id); ?>
		</div>
	</div>

	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('sales_add_tip'),
		'class'=>'submit_button float_left')
	);
	?>
	</form>
</fieldset>

</div>
<style>
#tip_cash_label {
	border-radius: 4px 0 0 4px;
}
label.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
	box-shadow: -2px 2px 30px 3px black inset;
}
</style>
<script>
	console.log('setting up validate');
	$(document).ready(function(){
		$( "#employee" ).autocomplete({
			source: '<?php echo site_url("sales/employee_search/"); ?>',
			delay: 200,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui)
			{
				event.preventDefault();
				$("#employee").val(ui.item.label);
				$("#tip_recipient").val(ui.item.value);
			},
			focus: function(event, ui) {
				event.preventDefault();
				//$("#teetime_title").val(ui.item.label);
			}
		});
	   $('#tip_amount').keyup(function(e){
			var tip_amount = $(e.target).val();
			var new_total = (parseFloat((tip_amount == '' ? 0 : tip_amount)) + parseFloat('<?=$credit_card_info['amount']?>')).toFixed(2);
			$('#new_total').val(new_total);
		});
		$('#add_tip_form').validate({
			submitHandler:function(form)
			{
				$(form).mask('Please wait...');
				$(form).ajaxSubmit({
				success:function(response)
				{
					if(response.success)
					{
						set_feedback(response.message,'success_message',false);
						$.colorbox2.close();
	                }
					else
					{
						set_feedback(response.message,'error_message',true);
						$.colorbox2.close();
	                }
				},
				dataType:'json'
			});

			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
	   		},
			messages:
			{
			}
		});
		console.log('inside doc ready');
	});
</script>
<tr>
	<td id="customer_wrapper" colspan=4>
        <div id="customer_info_shell">
            <div id='customer_info_empty'>
                <form id="select_customer_form">
                <label id="customer_label" for="customer">
                </label>
                <?php echo form_input(array('name'=>'customer','id'=>'customer','size'=>'30','maxlength'=>'16','placeholder'=>lang('sales_start_typing_customer_name'),  'accesskey' => 'c'));?>
                <?php
                    echo anchor("sales/view_customer/-1/width~1100",
                    '&nbsp;&nbsp;&nbsp;', array('class'=>'colbox none add_icon_blue','id'=>'new_customer_button','title'=>lang('sales_new_customer')));
                ?>
                <div id='customers_quickbuttons'>
					<?php
						foreach ($customer_quickbuttons as $customer_quickbutton)
							if ($customer_quickbutton['id'] != 0)
								if ($customer_quickbutton['id'] == $customer_id)
									echo '<a class="customer_quickbutton selected_customer" onclick="removeCustomer('.$customer_quickbutton["id"].')" href="javascript:void(0)">'.$customer_quickbutton["name"].'</a>';
								else
									echo '<a class="customer_quickbutton" onclick="selectCustomer('.$customer_quickbutton["id"].')" href="javascript:void(0)">'.$customer_quickbutton["name"].'</a>';
					?>
				</div>
				</form>
		    </div>
        </div>
    </td>
</tr>
<tr>
<?php if(isset($customer_id) && $customer_id != -1) { ?>
	<td id='customer_info_container' rowspan="3" style='display:block;'>
		<div class='profile_info'>
            <div id='profile_picture'><img class="<?php echo $status_class; ?>" src='<?php echo $this->Image->get_thumb_url($customer_image_id, 'person');?>' width=100 height=100 /></div>
			<div class='contact_info'>
				<?php echo anchor("customers/view/$customer_id/width~1100", "<span id='loaded_customer_name'>".character_limiter($customer, 25)."</span>",  array('class'=>'colbox none','title'=>lang('customers_update')));?>
				<?php if ($customer_account_number != '') {?><div id='profile_account_number'>#<?php echo $customer_account_number; ?></div><?php } ?>
				<div id='customer_email'><?php echo ($customer_email != '' ? character_limiter($customer_email, 25) : anchor("customers/view/$customer_id/width~1100", "Add email",  array('class'=>'colbox none','title'=>'Add email'))); ?></div>
				<div id='customer_phone'><?php echo ($customer_phone != '' ? character_limiter($customer_phone, 25) : anchor("customers/view/$customer_id/width~1100", "Add phone",  array('class'=>'colbox noner','title'=>'Add phone'))); ?></div>
				<?php if (count($customer_groups) > 0) {?>
				<div>
					<div class='groups_title'>Groups:</div>
					<div class='groups_list'>
						<?php foreach ($customer_groups as $group) { ?>
							<div><?=$group['label']?></div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
				<?php if (count($customer_passes) > 0) { ?>
				<div>
					<div class='groups_title'>Passes:</div>
					<div class='groups_list'>
						<?php foreach ($customer_passes as $pass) { ?>
							<div><?=$pass['label']?> Exp: <?php echo '<span style="'.(strtotime($pass['expiration']) < time() ? 'color:red;' : '').'">'.($pass['expiration'] == '0000-00-00 00:00:00' ? 'None Set' : date('Y-m-d', strtotime($pass['expiration']))).'</span>'?></div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		   	<div class='payment_info'>
				<!--span id="customer_account_number">Acct #: <?php echo $customer_account_number; ?></span><br/-->
				<div id="customer_account_balance"><?php echo $cab_name;?>: <span class='<?= ($customer_account_balance < 0 ? 'red' : 'black');?>'>$<?php echo $customer_account_balance; ?></span></div>
				<?php if ($is_member) {?>
				<div id="customer_member_balance"><?php echo $cmb_name;?>: <span class='<?= ($customer_member_balance < 0 ? 'red' : 'black');?>'>$<?php echo $customer_member_balance; ?></span></div>
				<?php } ?>
				<div id="invoice_balance">Billing Account: <span class='<?= ($invoice_balance < 0 ? 'red' : 'black');?>'>$<?php echo $invoice_balance; ?></span></div>
				<?php if ($this->config->item('use_loyalty')) { // GOLF COURSE SETTING ?>
					<?php if ($use_loyalty) { // CUSTOMER SETTING?>
						<div id="loyalty_balance"><?php echo lang('customers_loyalty_points');?>: <?php echo $loyalty_points; ?></div>
					<?php } else { ?>
						<div id="loyalty_off">Loyalty Inactive</div>						
					<?php } ?>
				<?php } ?>
				<?php if ($giftcard) {?>
				<div id="customer_giftcard">Giftcard: $<?php echo $giftcard_balance; ?></div>
				<?php } ?>
			</div>
			<div class='clear'></div>
		</div>
    </td>
    <!--td>
    	<div class='edit_delete_button_holder'>
             echo anchor("sales/delete_customer", lang('sales_detach'), array('class'=>'remove_customer'));
             echo anchor("customers/view/$customer_id/width~1100", "(".lang('common_edit').")",  array('class'=>'colbox none edit_customer','title'=>lang('customers_update')));
        </div>
   </td>
  </tr>
  <tr>
  	<td>
		<div id='account_button_holder'><span id='payment_from_account' class='alt_payment_button'>Charge <?=$cab_name?></span></div>
	</td>
</tr>
	<tr>
	<td>
		<div id='member_account_button_holder'><span id='payment_from_member_account' class='alt_payment_button'>Charge <?=$cmb_name?></span></div>
    </td>
</tr-->
<?php } else { ?>
		<td style='padding:10px; text-align:center; font-size:16px;' class='search_for_customer_message'></td>
<?php } ?>
</tr>
<style>
	#image-info img, #profile_picture img{
		border-radius:300px;
	}
</style>
<script type="text/javascript">
function selectCustomer(customerID) {
	$.ajax({
       type: "POST",
       url: "index.php/sales/select_customer/"+customerID,
       data: "",
       success: function(response) {
       		sales.update_page_sections(response);
	   },
       dataType:'json'
    });
}
function removeCustomer(customerID) {
	$.ajax({
       type: "POST",
       url: "index.php/sales/remove_customer/"+customerID,
       data: "",
       success: function(response) {
       		sales.update_page_sections(response);
	   },
       dataType:'json'
    });
}

// Onload
var gcn = $('#customer');
gcn.keydown(function(event){
	// Allow: backspace, delete, tab, escape, and enter
    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
        	console.log('kc '+event.keyCode);
        if (event.keyCode == 186 || event.keyCode == 187 || event.keyCode == 191 || (event.shiftKey && event.keyCode == 53)) {
            event.preventDefault();
        }
    }
});

$( "#customer" ).autocomplete({
	source: '<?php echo site_url("sales/customer_search/ln_and_pn"); ?>',
	delay: 200,
	autoFocus: false,
	minLength: 1,
	select: function(event, ui)
	{
		customer_flag_teesheet(ui.item.status_flag, ui.item.comments);
		$("#customer").val('');
		selectCustomer(ui.item.value);
	}
});

// Setup color boxes again...
$('.colbox').colorbox();

$("#select_customer_form").submit(function(e) {
	e.preventDefault();

	selectCustomer($("#customer").val());
});
$('#customer_account_balance').click(function(){
	$.ajax({
       type: "POST",
       url: "index.php/sales/add/account_balance",
       data: '',
       success: function(response){
       		sales.update_page_sections(response);
	    },
        dataType:'json'
     });
});
$('#customer_member_balance').click(function(){
	$.ajax({
       type: "POST",
       url: "index.php/sales/add/member_balance",
       data: '',
       success: function(response){
       		sales.update_page_sections(response);
	    },
        dataType:'json'
     });
});
$('#invoice_balance').click(function(){
	$.ajax({
       type: "POST",
       url: "index.php/sales/add/invoice_balance",
       data: '',
       success: function(response){
       		sales.update_page_sections(response);
	    },
        dataType:'json'
     });
});
$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
</script>
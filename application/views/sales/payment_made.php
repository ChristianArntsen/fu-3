<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/jquery-ui.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/StarWebPrintBuilder.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/StarWebPrintTrader.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/webprnt.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/taffy.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script src="<?php echo base_url();?>js/db.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<style>
	* {
		font-family:"Lucida Grande", Arial;
	}
</style>
<?php if ($status == 'cancelled') { ?>
<script>
	$(document).ready(function(){
		//$('#status_box').html('Starting');
		//$('#test_box').html('Amount Tendered: '+$('#amount_tendered', window.parent.document).val());
		//$('#status_box').html('Done');
		window.parent.set_feedback('Payment failed or timed out','success_message',false,1500);
		window.parent.mercury.close_window();
	});
</script>
	
<?php } else if ($status == 'declined') { ?>
<script>
	$(document).ready(function(){
		//$('#status_box').html('Starting');
		//$('#test_box').html('Amount Tendered: '+$('#amount_tendered', window.parent.document).val());
		//$('#status_box').html('Done');
		window.parent.set_feedback('Card declined','success_message',false,1500);
		window.parent.mercury.payment_window(true);
	});
</script>
<?php } else {
?>
<div>
	Payment successful.
	<?php 
	//echo $test_value;
	//echo $payment_amount;
	//echo $payment_type;
	?>
</div>
<script>
	$(document).ready(function(){
		//$('#status_box').html('Starting');
		//$('#test_box').html('Amount Tendered: '+$('#amount_tendered', window.parent.document).val());
		//$('#status_box').html('Done');
		if ('<?php echo $auth_amount; ?>' != '' && $('#amount_tendered', window.parent.document).val() > '<?php echo $auth_amount; ?>') {
			window.parent.set_feedback('Only $<?php echo $auth_amount; ?> was available, and was charged to card <?php echo $payment_type; ?>','success_message',false,1500);
		}
		$('#amount_tendered', window.parent.document).val('<?php echo $auth_amount; ?>');
		$('input:[name=payment_type]', window.parent.document).val('<?php echo $payment_type; ?>');
 		<?php 
 		if ($this->config->item('print_credit_card_receipt')) {
	 		if ($this->config->item('webprnt')) { ?>
	 			var receipt_data = '';
	 			var header_data = {
			    	course_name:'<?php echo $this->config->item('name')?>',
			    	address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
			    	address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
			    	phone:'<?php echo $this->config->item('phone')?>',
			    	employee_name:'<?php echo addslashes($user_info->last_name).', '.addslashes($user_info->first_name); ?>',
			    	customer:''// Need to load this in from data
			    };
			    var card_data = {
			    	card_type:'<?php echo $card_type; ?>',
			    	masked_account:'<?php echo $masked_account; ?>',
			    	auth_code:'<?php echo $auth_code; ?>',
			    	auth_amount:'<?php echo number_format($auth_amount, 2); ?>',
			    	print_tip_line:'<?php echo $this->config->item('print_tip_line'); ?>',
			    	cardholder_name:'<?php echo $cardholder_name; ?>',
			    	print_two_signature_slips:'<?php echo $this->config->item('print_two_signature_slips');?>'
			    };
	 			
	 			receipt_data = webprnt.build_credit_card_slip(receipt_data, card_data, header_data);
	 			console.log('webprnt.print credit card receipt *************************************************');
	 			console.log(receipt_data);
	 			<?php if ($this->config->item('print_two_signature_slips')) { ?>
		 			receipt_data += receipt_data;
	 			<?php } ?>
	 			window.parent.webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
		 		// var builder = new StarWebPrintBuilder();
				// var receipt_data = '';
		        // receipt_data += builder.createTextElement({data:'Card Type: <?php echo $card_type; ?>\n'});
		        // receipt_data += builder.createTextElement({data:'Card No.: <?php echo $masked_account; ?>\n'});
		        // //receipt_data += 'Card Exp.: \n';
		        // receipt_data += builder.createTextElement({data:'Auth: <?php echo $auth_code; ?>\n\n'});
		        // receipt_data += builder.createTextElement({data:'Amount: $<?php echo number_format($auth_amount, 2); ?>\n\n'});
		        // //receipt_data += 'Amount: $<?php echo number_format($auth_amount, 2); ?>\n\n';
		        // <?php if ($this->config->item('print_tip_line')) { ?>
		        // receipt_data += builder.createTextElement({data:'\n'+window.parent.add_white_space('Tip: ','$________.____')});
			    // receipt_data += builder.createTextElement({data:'\n\n'+window.parent.add_white_space('TOTAL CHARGE: ','$________.____')});
			    // <?php } ?>					        
		        // //receipt_data += window.parent.chr(27)+window.parent.chr(97)+window.parent.chr(49);
				// receipt_data += builder.createTextElement({data:'\n\nI agree to pay the above amount according to the card issuer agreement.\n\n\n'});
		        // receipt_data += builder.createTextElement({data:'X_____________________________________________\n'});
		        // receipt_data += builder.createTextElement({data:'  <?php echo $cardholder_name; ?>'});
		        // var i = 0;
		        // var cust_merch = ['','\n\n**********************************************\nCustomer Copy\n**********************************************\n','\n\n**********************************************\nMerchant Copy\n**********************************************\n'];
		        // <?php if ($this->config->item('print_two_signature_slips')) {  // WE'RE NOT READY FOR DOUBLE CC SIGNING SLIPS YET?>
				     // window.parent.print_webprnt_receipt(receipt_data, '', '', '', true);
// 			   
		        // <?php } else { ?>	
		        	// receipt_data += builder.createTextElement({data:cust_merch[i]});
			        // window.parent.print_webprnt_receipt(receipt_data);
			    // <?php }
	      	} else if (!$this->config->item('updated_printing')) { ?>
		 		var receipt_data = '';
		        receipt_data += 'Card Type: <?php echo $card_type; ?>\n';
		        receipt_data += 'Card No.: <?php echo $masked_account; ?>\n';
		        //receipt_data += 'Card Exp.: \n';
		        receipt_data += 'Auth: <?php echo $auth_code; ?>\n\n';
		        receipt_data += 'Amount: $<?php echo number_format($auth_amount, 2); ?>\n\n';
		        //receipt_data += 'Amount: $<?php echo number_format($auth_amount, 2); ?>\n\n';
		        <?php if ($this->config->item('print_tip_line')) { ?>
		        receipt_data += '\n'+window.parent.add_white_space('Tip: ','$________.____')
			    receipt_data += '\n\n'+window.parent.add_white_space('TOTAL CHARGE: ','$________.____')
			    <?php } ?>					        
		        receipt_data += window.parent.chr(27)+window.parent.chr(97)+window.parent.chr(49);
				receipt_data += '\n\nI agree to pay the above amount according to the card issuer agreement.\n\n\n';
		        receipt_data += 'X_____________________________________________\n';
		        receipt_data += '  <?php echo $cardholder_name; ?>';
		        var i = 0;
		        var cust_merch = ['','\n\n**********************************************\nCustomer Copy\n**********************************************\n','\n\n**********************************************\nMerchant Copy\n**********************************************\n'];
		        <?php if ($this->config->item('print_two_signature_slips')) {  // WE'RE NOT READY FOR DOUBLE CC SIGNING SLIPS YET?>
		        for(var i = 1; i <= 2; i++)
		        <?php } ?>
			        window.parent.print_receipt(receipt_data+cust_merch[i]);
	      <?php } else { ?>
		      console.log('printing cc receipt');
		        var receipt_html = '';
		        receipt_html += '<div style="font-size:6px;">Card Type: <?php echo $card_type; ?></div>';
		        receipt_html += '<div style="font-size:6px;">Card No.: <?php echo $masked_account; ?></div>';
		      //receipt_data += 'Card Exp.: \n';
		        receipt_html += '<div style="font-size:6px;">Auth: <?php echo $auth_code; ?></div><br/>';
		        receipt_html += '<div style="font-size:6px;">Total: $<?php echo number_format($auth_amount, 2); ?></div><br/>';
		        receipt_html += '<div style="font-size:6px;">'+'I&nbsp;agree&nbsp;to&nbsp;pay&nbsp;the&nbsp;above&nbsp;amount&nbsp;according&nbsp;to<br/>the&nbsp;card&nbsp;issuer&nbsp;agreement.'+'</div><br/>';
		        receipt_html += '<div style="font-size:6px;">X_____________________________________________</div>';
		        receipt_html += '<div>  <?php echo $cardholder_name; ?></div>';
		        var i = 0;
		        var cust_merch = ['','\n\n**********************************************\nCustomer Copy\n**********************************************\n','\n\n**********************************************\nMerchant Copy\n**********************************************\n'];
		        <?php if ($this->config->item('print_two_signature_slips')) {  // WE'RE NOT READY FOR DOUBLE CC SIGNING SLIPS YET?>
		        for(var i = 1; i <= 2; i++)
		        <?php } ?>
			        window.parent.print_postscript_receipt(receipt_html+cust_merch[i]);
	        <?php } 
	    }?>
        //$('#add_payment_button', window.parent.document).click();
		window.parent.mercury.add_payment();
		window.parent.mercury.payments_window();
	});
</script>
<?php } ?>
<?php
//print_r($admins_managers);

?>
<style>
	#individual-list {
		margin:10px;
	}
</style>
<ul id="error_message_box"></ul>
<?php
	echo form_open('sales/override/',array('id'=>'override_form'));
?>
<fieldset id="override_basic_info">
<ul id="individual-list" class="campaign-groups">
<?php
foreach ($admins_managers as $person)
{?>
      <li>
        <?php 
        	echo form_radio(array('id'=>'employee_id_'.$person['person_id'], 'name'=>'employee_id', 'value'=>$person['person_id']));
            echo form_label("&nbsp;&nbsp;{$person['first_name']} {$person['last_name']}",'employee_id_'.$person['person_id']);
        ?>
      </li>
<?php 
} ?>
<div class='clear'></div>
</ul>
<div class="field_row clearfix">
<?php echo form_label('', 'password',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_password(array(
		'name'=>'password',
		'size'=>'24',
		'maxlength'=>'16',
		'id'=>'password',
		'placeholder'=>'Password',
		'value'=>'')
	);?>
	</div>
</div>

<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('input[name=employee_id]').change(function(){
		$('#password').focus();
	})
	var submitting = false;
    $('#override_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					if (!response.success) {
						$('#is_taxable').attr('checked','checked');
						set_feedback('Password is incorrect', 'error_message',true);
					}else{
                        $('#taxable_verify').val(response.person_id);
						if (!$('#is_taxable').is(":checked")){
						 	sales.toggle_taxable($('#is_taxable'));
						}
					}
					//$.colorbox({href:'index.php/giftcards/view/'+response[0].giftcard_id, title:'Giftcard Information'});
					$.colorbox.close();
	                submitting = false;
                	console.dir(response[0]);
				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
		},
		messages:
		{
		}
	});
});
</script>
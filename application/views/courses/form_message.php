<ul id="error_message_box"></ul>
<?php
echo form_open('courses/save_message/'.$message_info->message_id,array('id'=>'message_form'));
?>
<fieldset id="item_basic_info">
<legend>Course Message</legend>
<div id='course_message_info'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_name').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'course_name',
			'id'=>'course_name',
			'value'=>$message_info->course_name)
		);?>
		</div>
		<input type='checkbox' id='all_courses' name='all_courses' /> Send to all courses (with Sales active)
		<?php
			echo form_hidden('course_id', $message_info->course_id);
		?>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('employees_employee').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'autocomplete'=>'off',
			'name'=>'employee_name',
			'id'=>'employee_name',
			'value'=>$message_info->employee_name)
		);?>
		</div>
		<?php
			echo form_hidden('employee_id', $message_info->employee_id);
			echo form_hidden('employee_course_id', $message_info->course_id);
		?>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('courses_message').':', 'name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_textarea(array(
			'name'=>'message',
			'id'=>'message',
			'value'=>$message_info->message)
		);?>
		</div>
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_send'),
		'class'=>'submit_button float_right')
	);
	?>
</div>
</fieldset>
</form>
<script>
$(document).ready(function(){
	$( "#course_name" ).autocomplete({
		source: "<?php echo site_url('courses/suggest');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$('#course_name').val(ui.item.label);
			$('#course_id').val(ui.item.value);
		}
	});
	$( "#employee_name" ).autocomplete({
		source: "<?php echo site_url('employees/suggest');?>",
		delay: 150,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$('#course_name').val(ui.item.course_id);
			$('#employee_name').val(ui.item.label);
			$('#employee_id').val(ui.item.value);
			$('#employee_course_id').val(ui.item.course_id);
		}
	});
	$('#message_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
				success:function(response)
				{
					var message = $('#message').val();
					var now = new Date();
					var strDateTime = [[AddZero(now.getDate()), AddZero(now.getMonth() + 1), now.getFullYear()].join("/"), [AddZero(now.getHours()), AddZero(now.getMinutes())].join(":"), now.getHours() >= 12 ? "PM" : "AM"].join(" ");

					//Pad given value to the left with "0"
					function AddZero(num) {
					    return (num >= 0 && num < 10) ? "0" + num : num + "";
					}
					var message_html = '';
					message_html = "<div class='message'>"+message+"</div>";
					message_html += "<div class='date_posted'>"+strDateTime+"</div>";
					$('#message_box').prepend(message_html);
					//console.log()
					$.colorbox.close();
					//ADD MESSAGE TO SIDE COLUMN

				},
				dataType:'json'
			});
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{

   		},
		messages:
		{

		}
	});
});
</script>
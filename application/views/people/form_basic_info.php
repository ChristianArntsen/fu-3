<style>
	#image-info img{
		border-radius:300px;
	}
</style>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_first_name') . ':'.($controller_name == 'suppliers' ? '' : '<span class="required">*</span>'), 'first_name', array());?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'first_name', 'id' => 'first_name', 'value' => $person_info -> first_name));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_last_name') . ':'.($controller_name == 'suppliers' ? '' : '<span class="required">*</span>'), 'last_name', array());?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'last_name', 'id' => 'last_name', 'value' => $person_info -> last_name));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_email') . ':'.($controller_name == 'employees' ? '<span class="required">*</span>' : ''), 'email');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'email', 'id' => 'email', 'size' => '40', 'value' => $person_info -> email));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(($controller_name != 'suppliers' ? lang('common_cell_phone_number') : lang('common_phone_number')) . ':', 'phone_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'phone_number', 'id' => 'phone_number', 'value' => $person_info -> phone_number));?>
	</div>
</div>
<div class="field_row clearfix">
	<?php echo form_label(($controller_name != 'suppliers' ? lang('common_phone_number') : lang('common_phone_number_2')) . ':', 'cell_phone_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'cell_phone_number', 'id' => 'cell_phone_number', 'value' => $person_info -> cell_phone_number));?>
	</div>
</div>
<script>
	$(document).ready(function() {
		console.log('masking phone');
		$("#phone_number").mask2("(999) 999-9999");
		$("#cell_phone_number").mask2("(999) 999-9999");
	})
</script>
<?php if ($controller_name == 'suppliers') {
?>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_fax_number') . ':', 'fax_number');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'fax_number', 'id' => 'fax_number', 'value' => $person_info -> fax_number));?>
	</div>
</div>
<?php }?>
<?php if ($controller_name != 'suppliers') {
?>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_birthday') . ':', 'birthday');?>
	<div class='form_field'>
		<?php echo form_input(array('name' => 'birthday', 'id' => 'birthday', 'value' => $person_info -> birthday));?>
	</div>
</div>
<div class="field_row clearfix">
	<label>Image</label>
	<div id="image-info">
		<img src="<?php echo $image_thumb_url; ?>" />
		<a id="select-image" title="Modify this image" href="<?php echo site_url('upload'); ?>/index/<?php echo $this->uri->segment(1); ?>?crop_ratio=1&image_id=<?php echo $person_info->image_id; ?>">Edit Image</a>
		<a id="remove-image" title="Disconnect image from person" href="<?php echo site_url($this->uri->segment(1)); ?>/save_image/<?php echo $person_info->person_id; ?>">Reset Image</a>
	</div>
</div>
<? }?>
<script>
	$('#birthday').datepicker({
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		yearRange : "1920:2000",
		defaultDate : "1970-01-01"
	});

</script>
<div id='address_info_box' style='display:none'>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_address_1') . ':', 'address_1');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'address_1', 'id' => 'address_1', 'value' => $person_info -> address_1));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_address_2') . ':', 'address_2');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'address_2', 'id' => 'address_2', 'value' => $person_info -> address_2));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_city') . ':', 'city');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'city', 'id' => 'city', 'value' => $person_info -> city));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_state') . ':', 'state');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'state', 'id' => 'state', 'value' => $person_info -> state));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_zip') . ':', 'zip');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'zip', 'id' => 'zip', 'value' => $person_info -> zip));?>
		</div>
	</div>
	<div class="field_row clearfix">
		<?php echo form_label(lang('common_country') . ':', 'country');?>
		<div class='form_field'>
			<?php echo form_input(array('name' => 'country', 'id' => 'country', 'value' => $person_info -> country));?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#address_info_box').expandable({
		title : 'Address Information:'
	});
</script>

<div id='marketing_info_box' style='display:none'>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_email_unsubscribed').':', 'email_unsubscribed'); ?>
		<div class='form_field'>
		<?php echo form_checkbox('email_unsubscribed', '1', ($person_info->opt_out_email ? true : false));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_text_unsubscribed').':', 'text_unsubscribed'); ?>
		<div class='form_field'>
		<?php echo form_checkbox('text_unsubscribed', '1', ($person_info->opt_out_text ? true : false));?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('customers_email_suspended').':', ''); ?>
		<div class='form_field'>
		<?php if ($bounced) { ?>
		<span id='blocked_email_box'>Yes <span class='unblock_email'>Unsuspend</span></span>
		<style>
			.unblock_email {
				cursor:pointer;
				color:rgb(92, 147, 168);
			}
		</style>
		<script>
			$(document).ready(function(){
				$('.unblock_email').click(function(){
					$.ajax({
			            type: "POST",
			            url: "index.php/customers/unblock_email?email=<?=$person_info->email?>",
			            data: '',
			            success: function(response){
			           		console.dir(response);
			           		if (response.success) {
			           			$('#blocked_email_box').replaceWith("No");
			           		}
					    },
			            dataType:'json'
			        });
				});
			});
		</script>
		<?php } else { ?>
		No
		<?php } ?>
		</div>
	</div>
</div>
<script type='text/javascript'>
	$('#marketing_info_box').expandable({
		title : 'Marketing:'
	});
</script>
<div class="field_row clearfix">
	<?php echo form_label(lang('common_comments') . ':', 'comments');?>
	<div class='form_field'>
		<?php echo form_textarea(array('name' => 'comments', 'id' => 'comments', 'value' => $person_info -> comments, 'rows' => '1', 'cols' => '12'));?>
	</div>
</div>
<?php
if ($this->config->item('mailchimp_api_key') && false)
{
?>
<div class="field_row clearfix">
	<div class="column">
		<?php echo form_label(lang('common_mailing_lists') . ':', 'mailchimp_mailing_lists');?>
	</div>
	<div class="column">
		<ul style="list-style: none;">
			<?php
			foreach (get_all_mailchimps_lists() as $list) {
				echo '<li>';
				echo form_checkbox(array('name' => 'mailing_lists[]', 'id' => $list['id'], 'value' => $list['id'], 'checked' => email_subscribed_to_list($person_info -> email, $list['id']), 'label' => $list['id']));
				echo form_label($list['name'], $list['id'], array('style' => 'float: none;'));
				echo '</li>';
			}
			?>
		</ul>
	</div>
	<div class="cleared"></div>
</div>
<?php
}
?>
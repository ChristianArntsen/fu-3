<style>
div.side > h1 {
	display: block;
	overflow: hidden;
	border-bottom: 1px solid #C5C5C5;
	padding-bottom: 5px;
	line-height: 32px;
	height: 32px;
	margin: 0px;
}

#food_and_beverage div.side > h1 a.fnb_button.side_modifiers {
	height: 32px !important;
	line-height: 32px !important;
	padding: 0px 10px !important;
	display: inline-block;
	margin: 0px !important;
	float: right !important;
}

div.side > ul {
	border-top: 1px solid rgba(255,255,255,0.8);
	padding-top: 8px;
}

div.side {
	margin-bottom: 10px;
}
</style>
<script type="text/html" id="template_item_sides">
<%
if(sides.length > number_sides){
	number_sides = sides.length;
}

for(var position = 1; position < number_sides + 1; position++){

var extraText = '';
if(position > number_sides){
	extraText = '(Extra)';
}

var selectedSide = false;
if(sides.length > 0 && sides.get(position)){
	var selectedSide = sides.get(position);
}

var showModifierButton = false;
if(selectedSide && selectedSide.get('modifiers').length > 0){
	var showModifierButton = true;
}
%>
<div class="side" style="float: none; width: auto;">
	<h1>
		Side #<%-position %> <%-extraText%>
		<a class="fnb_button side_modifiers" style="<%if(!showModifierButton){ print('display: none !important'); }%>" data-position="<%-position%>">Modifiers</a>
	</h1>
	<ul style="overflow: hidden; display: block;">
	<% _.each(App.sides.models, function(side){
	if(side.get('category') == 'Soups' || side.get('category') == 'Salads'){ return true; }
	if(hidden_sides && hidden_sides[side.get('item_id')] == 1){ return true; }

	var selected = '';
	if(selectedSide && selectedSide.get('item_id') == side.get('item_id')){
		selected = ' selected';
	}
	%>
		<li>
			<a class="fnb_button side <%-selected%>" href="#" data-type="side" data-position="<%-position%>" data-side-id="<%-side.get('item_id')%>">
				<%-_.capitalize(side.get('name'))%>
			</a>
		</li>
	<% }); %>
	</ul>
</div>
<% } %>
</script>
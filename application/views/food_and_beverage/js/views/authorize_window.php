var AuthorizeWindowView = Backbone.View.extend({
	
	tagName: "div",
	id: "authorize_window",
	template: _.template( $('#template_authorize').html() ),

	initialize: function(options) {
		if(options && options.action){
			this.action = options.action;
		}
	},

	events: {
		"click a.close": "closeWindow",
		"click div.number_pad a": "numberPress"
	},

	render: function() {
		this.$el.html(this.template());
		return this;
	},
	
	numberPress: function(e){
		var value = $(e.currentTarget).data('char');
		var field = this.$el.find('input.pin');
		
		if(value == 'del'){
			field.val( field.val().substr(0, field.val().length - 1) );
		}else if(value == 'enter'){
			this.submit();
		}else{
			field.val( field.val() + '' + value);
		}
	},
	
	closeWindow: function(e){
		$(document).off('cbox2_closed', function(e){
			view.closeWindow();
		});		
		$.colorbox2.close();
		this.remove();
		
		if(e){
			e.preventDefault();
		}
	},
	
	submit: function(){
		var pin = this.$el.find('input.pin').val();
		var view = this;
		
		$.post(App.api + 'authorize', {'pin': pin}, function(response){
			if(response.success){
				view.completeAction();
			}
		},'json').fail(function(){
			set_feedback('Invalid login', 'error_message', false, 2500);
		});
	},
	
	completeAction: function(){
		if(this.action == 'cancel_sale'){
			if(!App.closeTable(false)){
				set_feedback('Please finish paying receipts or refund payments to cancel sale', 'error_message', 5000);
			}
		}
		this.closeWindow();
	}
});

var EditItemView = Backbone.View.extend({
	tagName: "div",
	attributes: {id: "edit-item"},
	template: _.template( $('#template_edit_item').html() ),
	options: {active_section: false},

	events: {
		"click a.submit_button": "saveItem",
		"click a.delete_button": "deleteItem",
		"click li.tab > a": "clickTab",
		"click button.sub": "subField",
		"click button.add": "addField"
	},

	addField: function(event){
		var button = $(event.currentTarget);
		var field = button.siblings('input');
		var value = field.val();

		var increment = 1;
		if(field.attr('name') == 'item[discount]'){
			increment = 5;
		}

		field.val( parseFloat(value) + increment );
	},

	subField: function(event){
		var button = $(event.currentTarget);
		var field = button.siblings('input');
		var value = field.val();

		if(value <= 0){
			return false;
		}

		var increment = 1;
		if(field.attr('name') == 'item[discount]'){
			increment = 5;
		}

		field.val( value - increment );
	},

	render: function() {
		var data = this.model.attributes;

		if(data.incomplete){
			data.show_incomplete = true;
		}else{
			data.show_incomplete = false;
		}

		// Create list of sections (tabs) to display in item edit window
		data.sections = {
			1: {
				name: "Cook Temp",
				incomplete: !this.model.isModifiersComplete(3)
			},
			2: {
				name: "Customize",
				incomplete: !this.model.isModifiersComplete(1)
			},
			3: {
				name: "Condiments",
				incomplete: !this.model.isModifiersComplete(2)
			},
			4: {
				name: "Soup/Salad",
				incomplete: !(this.model.isSoupsComplete() && this.model.isSaladsComplete())
			},
			5: {
				name: "Sides",
				incomplete: !this.model.isSidesComplete()
			}
		};

		var modifiers = this.model.get('modifiers');

		// Remove any sections that don't have any options
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':3})){ delete data.sections[1]; } // Cook Temp
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':1})){ delete data.sections[2]; } // Customize
		if(modifiers.length == 0 || !modifiers.findWhere({'category_id':2})){ delete data.sections[3]; } // Condiments
		if(this.model.get('number_salads') == 0 && this.model.get('number_soups') == 0){ delete data.sections[4]; }
		if(this.model.get('number_sides') == 0){ delete data.sections[5]; }

		// Set the first tab to display by default
		data.default_section = false;
		_.every(data.sections, function(section, section_id){
			data.default_section = section_id;
			return false;
		});

		this.$el.html(this.template(data));
		this.$el.find('#edit_item_seat').keypad({position: 'right', formatMoney: false});
		this.$el.find('#edit_item_quantity').keypad({position: 'right', formatMoney: false});
		this.$el.find('#edit_item_price, #edit_item_discount').keypad({position: 'right'});

		if(data.default_section){
			this.renderSection(data.default_section);
		}

		if(data.sections[1] || data.sections[2] || data.sections[3] || data.sections[4] || data.sections[5]){
			this.$el.find('.tab-content').css({'min-height': '325px'});
		}
		
		return this;
	},

	clickTab: function(event){
		var button = $(event.currentTarget);
		var section_id = button.data('section-id');
		var tab_pane = $(button.attr('href'));

		// Mark tab as active
		button.parents('li').addClass('active').siblings().removeClass('active');

		this.renderSection(section_id);

		// Show appropriate tab pane
		tab_pane.addClass('active').siblings().removeClass('active');
		event.preventDefault();
	},

	renderSection: function(section_id){
		var viewData = {};
		viewData.section_id = section_id;
		viewData.model = this.model;

		// Modifiers
		var item = this.model;
		if(section_id <= 3){
			var view = new ItemModifiersView(viewData);

		// Soups/salads
		}else if(section_id == 4){
			var view = new ItemSidesView(viewData);

		// Sides
		}else if(section_id == 5){
			var view = new ItemSidesView(viewData);
		}

		// Close last section
		if(this.options.active_section){
			this.options.active_section.remove();
		}

		this.$el.find('#item-edit-' + section_id).html( view.render().el );	
		return true;
	},

	saveItem: function(event){
		// Get new data from fields
		var seat = $('#edit_item_seat').val();
		var price = $('#edit_item_price').val();
		var quantity = $('#edit_item_quantity').val();
		var discount = $('#edit_item_discount').val();
		var comments = $('#edit_item_comments').val();

		// Update item with new properties
		this.model.set({"quantity": quantity, "seat":seat, "price":price, "discount":discount, "comments":comments}, {'validate':true});

		if(!this.model.validationError){
			this.model.save();
			this.remove();
			$.colorbox.close();
		}

		event.preventDefault();
	},

	deleteItem: function(event){
		this.model.destroy();
		this.remove();
		$.colorbox.close();
		event.preventDefault();
	}
});

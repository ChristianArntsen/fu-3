var ItemsView = Backbone.View.extend({
	tagName: "div",
	className: "item-list",
	render: function() {
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');

		_.each(this.collection.models, this.addItem, this);
		return this;
	},

	addItem: function(item){
		var html = "<button style='float: left;' data-item-id='" +item.get('item_id')+ "' class='fnb_button menu_item'>" +item.get('name')+ "</button>";
		this.$el.append(html);
	}
});

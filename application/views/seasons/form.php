<?php
$disabled = '';
if($season_info->default == 1){
	$disabled = 'disabled';
}
?>
<style type="text/css">
.delete_button{
	color: red;
	display: inline;
	float: right;
	margin-top: -35px;
	margin-right: 80px;
	font-size: 14px;
	cursor: pointer;
}
/*
select.ui-datepicker-year{
	display:none;
}
select.ui-datepicker-month{
	width:63%;
}
*/
.ui-widget-content .ui-state-active{
	color:#349AC5;
}
</style>
<ul id="error_message_box"></ul>
<form action="<?php echo site_url('seasons/save'); ?>/<?php if(!empty($season_info->season_id)){ echo (int) $season_info->season_id; } ?>" method="post" name="season_form" id="season_form" style="padding: 10px;"> 
	<fieldset id="season_basic_info">
	<legend>Season Information</legend>

	<?php echo form_hidden('teesheet_id', $teesheet_info->teesheet_id);?>
	<?php echo form_hidden('season_id', $season_info->season_id ? $season_info->season_id : 0, array('id'=>"season_id"));?>

	<div class="field_row clearfix">
	<?php echo form_label('Name:', 'season_name',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'season_name',
			'size'=>'20',
			'id'=>'name',
			'value'=>$season_info->season_name)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('Start Date:', 'start_date',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'start_date',
			'style'=>'width: 75px;',
			'id'=>'start_date',
			'value'=>substr($season_info->start_date,5),
			$disabled=>$disabled)
		);?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label('End Date:', 'end_date',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'end_date',
			'style'=>'width: 75px;',
			'id'=>'end_date',
			'value'=>substr($season_info->end_date,5),
			$disabled=>$disabled
		));?>
		</div>
	</div>

	<div class="field_row clearfix">
	<?php echo form_label('Holiday:', 'holiday',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_checkbox(array(
			'name' => "holiday", 
			'id' => 'holiday',
			'value' => 1, 
			'checked' => $season_info->holiday,
			$disabled=>$disabled
		)); ?>
		</div>
	</div>
	
	<?php echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button',
		'style' => 'width: 100px; margin: 0 auto;')
	); ?>
	<? if (!$season_info->default){ ?><a class="delete_button" rel="delete_season">Delete Season</a><? } ?>
	</fieldset>
</form>
<script>
//validation and submit handling
$(document).ready(function()
{
	function post_season_form_submit()
	{
		if ($('#season_id').val() == 0)
			$('#config_green_fees_form').load('index.php/config/view/teesheet/' + $('#teesheet_id').val());
		else
			$('#config_green_fees_form').load('index.php/config/view/season/' + $('#season_id').val());
	}

	$('#start_date').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'mm-dd'
    });
	$('#end_date').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'mm-dd'
    });

	$('a[rel=delete_season]').on('click',function(){
		if (confirm("Are you sure you would like to delete this season?"))
		{
			var season_id = $('#season_id').val();
			$.ajax({
			  type: "POST",
			  url: "index.php/seasons/delete_season/"+season_id,
			  complete: function( msg ) {
				 $.colorbox.close();
				 set_feedback('Season deleted successfully', 'success_message');
				 $('#tab3').load('index.php/config/view/teesheet/<?php echo $teesheet_info->teesheet_id; ?>');
			  }
			});
		}
		return false;
	});
	
	var submitting = false;
    $('#season_form').validate({
		submitHandler:function(form)
		{
			if ($.trim($('#name').val()) != '')
			{
				if (submitting) return;
				submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				$(form).ajaxSubmit({
					success:function(response)
					{
						if(response.success){
							$.colorbox.close();
							$('#tab3').load('index.php/config/view/season/' + response.season_id);	
							set_feedback(response.message, 'success_message');						
						}else{
							set_feedback(response.message, 'error_message');								
						}

		                submitting = false;
					},
					dataType:'json'
				});
			}
			else
			{
				alert('Please enter a season name');
			}
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
});
</script>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php $this->load->view("partial/header"); ?>
<?php if(isset($dashboards[$active_dashboard]['editable']) && $dashboards[$active_dashboard]['editable'] == false)
{
	$editable = false;
}else{
	$editable = true;
} ?>
<script src="<?php echo base_url('js/highcharts.js'); ?>"></script>
<script src="<?php echo base_url('js/exporting.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/gray.js'); ?>"></script>
<script type="text/javascript">
var editable = <?php if($editable){ echo 'true'; } else { echo 'false'; } ?>

Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator == undefined ? "." : decSeparator,
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

function render_widget(widget_id, name, type, width, height, pos_x, pos_y, z_index){
	if(!z_index){
		z_index = 0;
	}

	window['widget_' + widget_id] = null;
	$('#widget_' + widget_id).remove();

	if(type == 'table'){
		height_offset = 50;
	}else{
		height_offset = 30;
	}

	var widget_html = "<li id='widget_"+widget_id+"' class='widget "+type+"' data-widget-id='" + widget_id + "' style='width:" + width + "px; height:" + height + "px; top:" + pos_y + "px; left:" + pos_x + "px; z-index: "+z_index+"'>" +
		"<div class='widget-settings'>" +
			"<span class='widget-label'>"+ name +"</span><span class='widget-buttons'>";
		if(editable){
			widget_html += "<a href='#' class='delete'>Delete</a> | <a href='#' class='edit'>Edit</a>";
		}
		widget_html += "</span></div>" +
		"<div class='widget-content' id='widget_content_"+widget_id+"' style='height:" + (height - height_offset) + "px'><div class='loading'>Loading...</div></div>" +
		"</li>";
	$('ul.widgets').append(widget_html);
	var form = $('#filter_form');
	var params = form.serialize();
	$.get('<?php echo site_url('dashboards/widget'); ?>/' + widget_id, params, function(response){
		render_widget_visual(response);
	}, 'json');

	$('ul.widgets').height( $('ul.widgets')[0].scrollHeight );
}

function render_widget_visual(response){
	var widget_id = response.widget_id;
	if(response.type == 'bar' || response.type == 'column' || response.type == 'line' || response.type == 'pie'){

		if(response.type == 'pie'){
			var chart_config = pie_chart;
		}else{
			var chart_config = default_chart;
		}
		chart_config.series = response.data;
		chart_config.chart.type = response.type;
		chart_config.xAxis.categories = response.categories;
		chart_config.chart.renderTo = $('#widget_content_' + response.widget_id)[0];

		window['widget_' + widget_id] = new Highcharts.Chart(chart_config);

	}else if(response.type == 'number'){
		$('#widget_' + response.widget_id).find('div.widget-content').html( generate_number_widget(response) );

	}else if(response.type == 'table'){
		$('#widget_' + response.widget_id).find('div.widget-content').html( generate_table_widget(response) );
	}
}

var default_chart = {
	chart: {
		renderTo: null, // Variable
		type: 'line',
		borderRadius: 0,
		spacingTop: 20,
		backgroundColor: 'none'
	},
	title: null,
	subtitle: {
		text: '',
		x: -20
	},
	xAxis: {
		categories: null // Variable
	},
	yAxis: {
		title: null,
		plotLines: [{
			value: 0,
			width: 1,
			color: '#808080'
		}]
	},
	tooltip: {
		formatter: function() {
			return '<b>'+ this.series.name +'</b><br/>'+
			this.x +': '+ get_number(this.y, this.series.options.number_type);
		}
	},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'top',
		x: -10,
		y: 100,
		borderWidth: 0
	},
	credits: {
		enabled: false
	},
	series: null //Variable
};

var pie_chart = {
	chart: {
		renderTo: null, // Variable
		type: 'pie',
		borderRadius: 0,
		spacingTop: 0,
		spacingBottom: 5,
		spacingRight: 0,
		spacingLeft: 0,
		backgroundColor: 'none'
	},
	title: null,
	subtitle: {
		text: '',
		x: -20
	},
	xAxis: {
		categories: null // Variable
	},
	yAxis: {
		title: null,
		plotLines: [{
			value: 0,
			width: 1,
			color: '#808080'
		}]
	},
	tooltip: {
		formatter: function() {
			return '<b>'+ this.point.name +'</b><br/>'+
			get_number(this.y, this.series.options.number_type) + ' (' + this.point.percentage.toFixed(2) +'%)';
		}
	},
	plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,
				color: '#FFFFFF',
				connectorColor: '#FFFFFF',
				format: '<b>{point.name}</b>: {point.percentage:.1f} %'
			}
		}
	},
	credits: {
		enabled: false
	},
	series: null //Variable
};

function generate_number_widget(widget){
	var html = '';

	for(var i = 0; i < widget.data.length; i++){
		var data = widget.data[i];
		html += '<div class="number"><span>' + data.name + '</span><h2>' + get_number(data.data[0], data.number_type) + '</h2></div>';
	}
	return html;
}

function generate_table_widget(widget){
	var html = '<table id="dashboard_table" class="tablesorter"><thead><tr>';
	if (widget.categories_header) {
		for(var i = 0; i < widget.categories_header.length; i++){
			var category = widget.categories_header[i];
			if (i > 4) {
				html += '<td class="week_day week_days" colspan="2">' + category + '</td>';
			}else{
				html += '<td>' + category + '</td>';
			}
		}
		html += '</tr><tr class="column_header">';

		for(var i = 0; i < widget.categories.length; i++){
			var category = widget.categories[i];
			if((i > 3) &&(i == 4 || i == 6 || i == 8 || i == 10 || i == 12 || i == 14 || i == 16 || i == 18 )){
				html += '<th class="sorting_asc week_days" data-sort="int">' + category + '</th>';	
			}else{
				html += '<th class="sorting_asc" data-sort="int">' + category + '</th>';
			}
		}

		html += '</tr></thead><tbody>';
		var row_color = 0;
		for(var i = 0; i < widget.data.length; i++){
			var metric = widget.data[i];
			html += '<tr class="_' +row_color+'">';
			for(ii = 0; ii < 19; ii++){
				if(ii == 4 || ii == 6 || ii == 8 || ii == 10 || ii == 12 || ii == 14 || ii == 16 || ii == 18){
					html += '<td class=" week_days">' + get_number(metric[ii].data, metric[ii].number_type) + '</td>';
				}else{
					html += '<td>' + get_number(metric[ii].data, metric[ii].number_type) + '</td>';
				}
			}
			html += '</tr>';
			row_color = 1 - row_color;
		}
		html += '</tbody></table>';
	}
	else{
			for(var i = 0; i < widget.categories.length; i++){
				var category = widget.categories[i];
				html += '<th class="sorting_asc" data-sort="int">' + category + '</th>';
			}
			html += '</tr></thead><tbody>';
			var row_color = 0;
			for(var i = 0; i < widget.data.length; i++){
				var metric = widget.data[i];
				html += '<tr class="_' +row_color+'">';
				for(ii = 0; ii < metric.length; ii++){
					html += '<td>' + get_number(metric[ii].data, metric[ii].number_type) + '</td>';
				}
				html += '</tr>';
				row_color = 1 - row_color;
			}
			html += '</tbody></table>';
	}
	return html;
}

function resize_dashboard(widget){
	var dashboard = $('ul.widgets');

	if( (widget.position.left + widget.helper.width()) >= dashboard.width()){
		widget.position.left = dashboard.width() - widget.helper.width();
	}

	if(widget.position.left <= 0){
		widget.position.left = 0;
	}

	if (widget.position.top <= 0) {
		widget.position.top = 0;
	}

	if (widget.position.top >= (dashboard.height() - widget.helper.height() + 20)) {
		dashboard.height(dashboard.height() + 20);
	}
}

function init_widget_resize(){

	if(!editable){ return false; }

	$("ul.widgets li.widget").resizable({
		grid: [20, 20],
		stop: function(event, ui){
			var width = ui.helper.width();
			var height = ui.helper.height();
			var widget_id = ui.helper.attr('data-widget-id');

			$.post('<?php echo site_url('dashboards/save_widget_size'); ?>/' + widget_id, {width:width, height:height});
		},
		resize: function(event, ui){
			widget_id = ui.helper.attr('data-widget-id');

			if(window['widget_' + widget_id]){
				window['widget_' + widget_id].setSize(
					this.offsetWidth,
					this.offsetHeight - 30,
					false
				);
			}
			resize_dashboard(ui);
		}
	});
}

function get_number(value, type){
	if(type == 'total'){
		return 'Total';
	}else if(type == 'avg'){
		return (Math.round(value*100)/100).toFixed(2);
	}else if(value == null){
		return '0';
	}else if(type == 'money'){
		return '$' + (Math.round(value*100)/100).toFixed(2);
	}else if(type == 'number'){
		return (Math.round(value*100)/100).toLocaleString();	
	}else if(type == 'string'){
		return value;
	}else if(type == 'percentage' && value != null){
		return Math.round(value*100)/100 + '%';
	}
}

function init_widget_drag(){
	if(!editable){ return false; }

	$("ul.widgets li.widget").draggable({
		cursor: 'move',
		tolerance: 'pointer',
		scroll: true,
		grid: [20, 20],
		stack: 'li.widget',
		stop: function(event, ui){
			var widgetId = ui.helper.attr('data-widget-id');
			$.post('<?php echo site_url('dashboards/save_widget_position'); ?>/' + widgetId, {pos_x:ui.position.left, pos_y:ui.position.top});
		},
		drag: function(event, ui){
			resize_dashboard(ui);
		}
	});
}

$(document).ready(function(){

	$("#dashboard_table").tablesorter(); 

	<?php if(!empty($widgets)){
	foreach($widgets as $widget){ ?>
	render_widget('<?php echo $widget['widget_id']; ?>', '<?php echo addslashes($widget['name']); ?>', '<?php echo $widget['type']; ?>', <?php echo (int) $widget['width']; ?>, <?php echo (int) $widget['height']; ?>, <?php echo (int) $widget['pos_x']; ?>, <?php echo (int) $widget['pos_y']; ?>);
	<?php } } ?>

	$('#new-dashboard').colorbox({
		href:"<?php echo site_url('dashboards/form'); ?>",
		initialWidth: "500px",
		initialHeight: "400px",
		width: "500px",
		height: "400px",
	});

	$('#new-widget').colorbox({
		href:"<?php echo site_url('dashboards/widget_form'); ?>?dashboard_id=<?php echo $active_dashboard; ?>",
		initialWidth: "700px",
		initialHeight: "600px"
	});

	$('li.widget a.delete').live('click', function(e){
		var widget = $(this).parents('li.widget');
		var widget_id = widget.attr('data-widget-id');

		if(confirm('Are you sure you delete this widget?')){
			$.post('<?php echo site_url('dashboards/delete_widget'); ?>', {widget_id:widget_id}, function(response){
				if(response.success){
					widget.remove();
				}
			},'json');
		}
		return false;
	});

	$('li.widget a.edit').live('click', function(e){
		var widget = $(this).parents('li.widget');
		var widget_id = widget.attr('data-widget-id');

		$.colorbox({
			href:"<?php echo site_url('dashboards/widget_form'); ?>/" + widget_id + "?dashboard_id=<?php echo $active_dashboard; ?>",
			initialWidth: "600px",
			initialHeight: "500px",
			title: "Edit Widget"
		});
		return false;
	});

	$('#delete_dashboard').live('click', function(e){
		var widget = $(this).parents('li.widget');
		var dashboard_id = <?php echo (int) $active_dashboard; ?>;

		if(confirm('Are you sure you delete this entire dashboard, including all widgets?')){
			$.post('<?php echo site_url('dashboards/delete'); ?>', {dashboard_id:dashboard_id}, function(response){
				if(response.success){
					window.location = '<?php echo site_url('dashboards'); ?>';
				}
			},'json');
		}
		return false;
	});

	$('#global_filters select').change(function(e){
		var form = $(this).parent('form');
		var params = form.serialize();

		$('ul.widgets > li.widget').each(function(index){
			var widget_id = $(this).attr('data-widget-id');
			$(this).find('div.widget-content').html("<div class='loading'>Loading...</div>");

			$.get('<?php echo site_url('dashboards/widget'); ?>/' + widget_id, params, function(response){
				render_widget_visual(response);
			}, 'json');
		});
	});

	init_widget_drag();
	init_widget_resize();

	$('ul.widgets').height( $('ul.widgets')[0].scrollHeight );

	$('#start_date').datetimepicker({
		defaultDate:<?php if ($js_start) { ?>new Date('<?=$js_start?>') <?php } else { ?> new Date() <?php } ?>,
		dateFormat:'mm/dd/yy',
		altField: '#start_date_actual',
		altFormat: 'yy-mm-dd',
		altTimeFormat: 'HH:mm:00',
		altFieldTimeOnly: false,
		controlType: 'select',
		timeFormat: 'h:mmTT',
		defaultValue: '<?php echo date('m/d/Y'); ?> 12:00AM',
		onSelect:function(){
			$('#complex_radio').attr('checked','checked');
		}
	});
	$('#end_date').datetimepicker({
		defaultDate:<?php if ($js_end) { ?>new Date('<?=$js_end?>') <?php } else { ?> new Date() <?php } ?>,
		dateFormat:'mm/dd/yy',
		altField: '#end_date_actual',
		altFormat: 'yy-mm-dd',
		altTimeFormat: 'HH:mm:00',
		altFieldTimeOnly: false,
		controlType: 'select',
		timeFormat: 'h:mmTT',
		defaultValue: '<?php echo date('m/d/Y'); ?> 11:59PM',
		onSelect:function(){
			$('#complex_radio').attr('checked','checked');
		}
	});

	$('#apply_filter').on('click', function(){
		var form = $(this).parent('form');
		var params = form.serialize();

		$('ul.widgets > li.widget').each(function(index){
			var widget_id = $(this).attr('data-widget-id');
			$(this).find('div.widget-content').html("<div class='loading'>Loading...</div>");
			$.get('<?php echo site_url('dashboards/widget'); ?>/' + widget_id, params, function(response){
				render_widget_visual(response);
			}, 'json');
		});
	});

	$('.sorting_asc').live('click', function(){
		$("#dashboard_table").tablesorter();
	});

});
</script>
<style>
#dashboard-container {
	width: auto;
	display: block;
	margin: 0px;
	padding: 0px;
}

#dashboards {
	display: block;
	height: 50px;
	width: auto;
	padding: 0px;
	margin: 0px;
}

a.button {
	display: block;
	float: left;
	margin: 0px 5px 0px 0px;
	background: #349AC5;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#349ac5', endColorstr='#4173b3');
	background: -webkit-linear-gradient(top, #349AC5, #4173B3);
	background: -moz-linear-gradient(top, #349AC5, #4173B3);
	color: white;
	font-size: 14px;
	font-weight: normal;
	height: 32px;
	line-height: 32px;
	padding: 0px 20px;
	width: auto;
	text-align: center;
	text-shadow: 0px -1px 0px black;
	border-radius: 4px;
	box-shadow: inset 0px 1px 1px 0px rgba(255, 255, 255, 0.5), 0px 3px 1px -2px rgba(255, 255, 255, .2);
	border: 1px solid #232323;
}

ul.widgets {
	display: block;
	position: relative;
	margin: 0px;
	padding: 0px;
}

li.widget {
	display: block;
	float: none;
	list-style-type: none;
	width: 300px;
	height: 200px;
	margin: 0px 20px 20px 0px;
	top: 0px;
	left: 0px;
	padding: 0px;
	box-shadow: 0px 0px 6px #090909;
	overflow: hidden;
	position: absolute;
	background: #333333;
	background: -moz-linear-gradient(-45deg,  #333333 0%, #333333 49%, #2c2c2c 49%, #262626 100%);
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#333333), color-stop(49%,#333333), color-stop(49%,#2c2c2c), color-stop(100%,#262626));
	background: -webkit-linear-gradient(-45deg,  #333333 0%,#333333 49%,#2c2c2c 49%,#262626 100%);
	background: -o-linear-gradient(-45deg,  #333333 0%,#333333 49%,#2c2c2c 49%,#262626 100%);
	background: -ms-linear-gradient(-45deg,  #333333 0%,#333333 49%,#2c2c2c 49%,#262626 100%);
	background: linear-gradient(135deg,  #333333 0%,#333333 49%,#2c2c2c 49%,#262626 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#333333', endColorstr='#262626',GradientType=1 );
}

li.widget div.widget-content {
	display: block;
	width: auto;
	margin: 0px;
	padding: 0px;
	background: transparent;
	overflow: auto;
}

div.widget-content div.loading {
	margin: 0 auto;
	width: 100px;
	text-align: center;
	position: relative;
	font-size: 20px;
	padding-top: 60px;
	color: white;
}

li.widget div.widget-settings {
	cursor: move;
	height: 30px;
	line-height: 30px;
	background-color: #292929;
	color: white;
	display: block;
	margin: 0px;
	text-shadow: 1px 1px 1px #111;
	box-shadow: 0px -3px 4px -4px black inset !important;
}

li.widget.empty {
	background: none !important;
	text-shadow: 1px 1px 1px #111;
	box-shadow: none;
	display: block;
	float: none !important;
	width: 100% !important;
	padding: 100px 0px 0px 0px;
	margin: 0px;
	height: 400px;
	text-align: center;
	font-size: 24px;
	font-weight: normal;
	color: white;
}

li.widget div.number {
	display: block;
	float: left;
	width: 200px;
	margin: 5px 10px;
}

li.widget div.number span {
	display: block;
	font-size: 14px;
	color: white;
	margin-bottom: 2px;
	text-shadow: 1px 1px 1px #111;
}

li.widget div.number h2 {
	display: block;
	font-size: 36px;
	color: white;
	text-shadow: 1px 1px 1px #111;
}

span.widget-label{
	float: left;
	display: block;
	padding-left: 10px;
	font-weight: normal;
	font-size: 16px;
}

span.widget-buttons {
	float: right;
	display: block;
	padding-right: 5px;
}

span.widget-buttons a {
	color: white;
}

#dashboards > a.button.new-widget, #dashboards > a.new {
	float: right !important;
	background: #4dad47;
	background: -moz-linear-gradient(top,  #4dad47 0%, #398235 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4dad47), color-stop(100%,#398235));
	background: -webkit-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -o-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: -ms-linear-gradient(top,  #4dad47 0%,#398235 100%);
	background: linear-gradient(to bottom,  #4dad47 0%,#398235 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4dad47', endColorstr='#398235',GradientType=0 );
}

#dashboards > a.new {
	float: left !important;
}

a.button.active {
	box-shadow: 0 5px 20px -5px black inset !important;
}
a.button.active:hover {
	cursor: arrow !important;
}
#delete_dashboard {
	float: left;
	margin-top: 20px;
	background: #d14d4d; /* for non-css3 browsers */
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d14d4d', endColorstr='#c03939'); /* for IE */
	background: -webkit-linear-gradient(top, #d14d4d, #c03939);
	background: -moz-linear-gradient(top,  #d14d4d,  #c03939); /* for firefox 3.6+ */
}

div.widget-content table {
	color: white;
	font-size: 14px;
	border-collapse: collapse;
}

div.widget-content table td {
	padding: 5px;
	text-align: right;
}

div.widget-content table th {
	padding: 5px;
	text-align: right;
	cursor: pointer;
}

div.widget-content table td.metric {
	font-weight: bold;
	text-align: left;
	padding-left: 0px;
}

div.widget-content table thead tr:first-child {
	border-bottom: 1px solid #666;
}

div.widget-content table thead tr td {
	font-weight: bold;
	text-align:center;
}

div.widget-content table tbody tr:first-child {
	border-top: 1px solid #000;
	font-weight: bold;
}

div.widget-content table tbody tr._0 {
	background-color: transparent;
}

div.widget-content table tbody tr._1 {
	background-color: rgba(0, 0, 0, 0.15);
}

li.widget.table div.widget-content {
	padding: 10px;
}

#time, select {
	padding: 6px;
	margin-top: 2px;
	margin-left: 5px;
}

#global_filters select {
	margin-bottom: 2px;
}

#global_filters label {
	font-weight: normal;
	color: white;
	font-size: 16px;
	text-shadow: 1px 1px 1px #111;
	margin-right: 5px;
	float:left;
}
#global_filters select {
	float:left;
}
#global_filters {
	margin-bottom: 10px;
}
.column_header{
	border-bottom: 1px solid #666666;
}
.column_header_top{
	text-align: right;
	margin-left: 10px;
}
#apply_filter{
	cursor: pointer;
}
.date_filter{
	min-height: 30px;
	margin-top: 3px;
	min-width: 140px;
}
.week_days{
	border-right: 1px solid #666;
}
.week_day{
	border-left: 1px solid #666;
}
</style>
<div id="global_filters">
<form method="post" id='filter_form'>

	<?php if($dashboards_id == '' || $dashboards_id == overall) { ?>
			<?php if(!empty($courses)){ ?>
			<label for="course_id">Course</label>
			<?php echo form_dropdown('course_id', array_flip($courses), $active_course, "id='course_id'"); ?>
			<?php } ?>
			<?php if(!$editable){ ?>
			<label style="margin-left: 25px;" for="time">Time Period</label>
			<?php echo form_dropdown('time', array(
				'today' => 'Today',
				'past_7_days' => 'Last 7 Days',
				'past_30_days' => 'Last 30 Days',
				'past_year' => 'Past Year',
				'all_time' => 'All Time'
			), 'past_30_days',  "id='time'"); ?>
		<?php } ?>
	<?php } else { ?>
			<a class="button" id="apply_filter" title="Apply Filter"> Apply Filter</a>
			<?php if(!empty($filter_courses)){?>
			<label for="course_id">Course</label>
			<?php 
			echo form_dropdown('course_filter_id', $filter_courses, $this->session->userdata('filter_course')); ?>
			<?php } ?>
			<div id='report_date_range_complex'>
				<input type='text' name='start_date' id='start_date' class="date_filter" value='<?=$this->session->userdata('filter_start_date')?>' placeholder='From'/>
				<input type='text' name='end_date' id='end_date' class="date_filter" value='<?=$this->session->userdata('filter_end_date')?>' placeholder='To'/>
			</div>
	<?php } ?>

</form>
<div class='clear'></div>
</div>
<div id="dashboards">
<?php if(!empty($dashboards)){
	foreach($dashboards as $dashboard){
	if($dashboard['dashboard_id'] == $active_dashboard){
		$class = " active";
	}else{
		$class = "";
	} 

	?>
	<a class="button<?php echo $class; ?>" href="<?php echo site_url('dashboards/index/'.$dashboard['dashboard_id']); ?>"><?php echo $dashboard['name']; ?></a>
<?php } } ?>
	<a class="button new" id="new-dashboard" href="#" title="Add New Dashboard">+ New</a>
	<?php if(!empty($active_dashboard) && $editable){ ?>
	<a class="button new-widget" id="new-widget" href="#" title="New Widget">+ Add Widget</a>
	<?php } ?>
</div>
<div id="dashboard-container">
	<ul class="widgets">
		<?php if(empty($active_dashboard)){ ?>
			<li class="widget empty">No dashboards yet, click the button above to create one</li>
		<?php }else if(empty($widgets)){ ?>
			<li class="widget empty">No widgets yet, click "Add Widget" above to create one</li>
		<?php } ?>
		<li style="clear: both; position: relative; display: block; height: 0px; background: none; float: none;"></li>
	</ul>
	<?php if($editable){ ?>
	<a id="delete_dashboard" class="button" title="Delete this dashboard" href="#">Delete Dashboard</a>
	<?php } ?>
</div>
<div id="feedback_bar"></div>
<?php $this->load->view("partial/footer"); ?>
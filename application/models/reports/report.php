<?php
abstract class Report extends CI_Model 
{
	var $params	= array();
	
	function __construct()
	{
		parent::__construct();

		//Make sure the report is not cached by the browser
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");		
	}
	
	public function setParams(array $params)
	{
		$this->params = $params;
	}
	
	//Returns the column names used for the report
	public abstract function getDataColumns();
	
	//Returns all the data to be populated into the report
	public abstract function getData();
	
	//Returns key=>value pairing of summary data for the report
	//public abstract function getSummaryData();
	//public abstract function get_quickbooks_data();
	public function getPaymentData()
	{
		$sales_totals = array();
		
		$this->db->select('sale_id, SUM(total) as total', false);
		$this->db->from('sales_items_temp');
		$this->db->group_by('sale_id');
		$result = $this->db->get();
		//echo $this->db->last_query();
		foreach($result->result_array() as $sale_total_row)
		{
			$sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		}
//echo 'about to load view';
		
		$this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		$this->db->where('date(sale_time) BETWEEN "'. $this->params['start_date']. '" and "'. $this->params['end_date'].'"');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('payment_amount > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('payment_amount < 0');
		}
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		$this->db->order_by('sale_id, payment_type');
		$sales_payments = $this->db->get()->result_array();
		
		foreach($sales_payments as $row)
		{
        	$payments_by_sale[$row['sale_id']][] = $row;
		}
		
		$payment_data = array();
		
		foreach($payments_by_sale as $sale_id => $payment_rows)
		{
			$total_sale_balance = $sales_totals[$sale_id];
			
			foreach($payment_rows as $row)
			{
				$payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				//echo $row['payment_type'].' - '.$row['payment_amount'].'<br/>';
				//Consolidating Credit Cards,  Account Payments, and Giftcards
				if (strpos($row['payment_type'], 'Tip') !== false)
				{
					$row['payment_type'] = 'Tips';
				}
				else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false 
					|| strpos($row['payment_type'], 'DCVR') !== false || strpos($row['payment_type'], 'DINERS') !== false || strpos($row['payment_type'], 'JCB') !== false)
				{
					$row['payment_type'] = 'Credit Card';
				}
				else if (strpos($row['payment_type'], $cdn) !== false)
				{
						$row['payment_type'] = $cdn;
						//echo $payment_amount.'<br/>';
				}
				else if (strpos($row['payment_type'], $mbn) !== false)
				{
						$row['payment_type'] = $mbn;
						//echo $payment_amount.'<br/>';
				}
				else if (strpos($row['payment_type'], 'Gift Card') !== false)
					$row['payment_type'] = 'Gift Card';
				else if (strpos($row['payment_type'], 'Punch Card') !== false)
					$row['payment_type'] = 'Punch Card';
				else if (strpos($row['payment_type'], 'Raincheck') !== false)
					$row['payment_type'] = 'Raincheck';
				else if (strpos($row['payment_type'], 'Change') !== false)
					$row['payment_type'] = 'Cash';
				else if (strpos($row['payment_type'], 'Bank Acct') !== false)
					$row['payment_type'] = 'Bank Account';
				
				//if($row['payment_type'] == 'Credit Card')
					//echo 'Sale '.$row['sale_id'].' - '.$payment_amount."<br/>";
				
				if (!isset($payment_data[$row['payment_type']]))
					$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
				if ($total_sale_balance != 0 || $row['payment_type'] == 'Tips')
					$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
				
				if (!isset($payment_data['Total']))
					$payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
				if ($total_sale_balance != 0)
					$payment_data['Total']['payment_amount'] += $payment_amount;
				
				$total_sale_balance-=$payment_amount;
			}
//echo $row['payment_amount'].' '.$total_sale_balance.'<br/>';
		}
		//echo $this->db->last_query();
		return $payment_data;
	}
	public function getSummaryData()
	{
		
		//if (!$this->permissions->is_super_admin())
                  //  $this->db->where('course_id', $this->session->userdata('course_id'));
		/*$this->db->select('payment_type');
		$this->db->from('sales_items_temp');
		$this->db->group_by('sale_id');
		$results = $this->db->get()->result_array();
		$account_charges = 0;
		foreach($results as $result) {
			//TODO:Account for multiple payments on account in one transaction
			if (preg_match('/Account- [^$]+: \$(.+)</',$result['payment_type'], $match)) {
				$account_charges += (float)$match[1];
			}
		}*/
		
		$totals = $this->getPaymentData();
		//echo 'Account total: '.$account_charges;
		//print_r($results);
		//echo $this->db->last_query();
		$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
		$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
		$account_charges = (isset($totals[$cdn]['payment_amount']))?$totals[$cdn]['payment_amount']:0;
		$member_balance = (isset($totals[$mbn]['payment_amount']))?$totals[$mbn]['payment_amount']:0;
		$giftcards = (isset($totals['Gift Card']['payment_amount']))?$totals['Gift Card']['payment_amount']:0;
		$punch_cards = (isset($totals['Punch Card']['payment_amount']))?$totals['Punch Card']['payment_amount']:0;
		$rainchecks = (isset($totals['Raincheck']['payment_amount']))?$totals['Raincheck']['payment_amount']:0;
		$tips = (isset($totals['Tips']['payment_amount']))?$totals['Tips']['payment_amount']:0;
		$total_payments = (isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		$credit_card = (isset($totals['Credit Card']['payment_amount']))?$totals['Credit Card']['payment_amount']:0;
		$check = (isset($totals['Check']['payment_amount']))?$totals['Check']['payment_amount']:0;
		$cash = (isset($totals['Cash']['payment_amount']))?$totals['Cash']['payment_amount']:0;
		$bank_account = (isset($totals['Bank Account']['payment_amount']))?$totals['Bank Account']['payment_amount']:0;
		$total_payments = $credit_card + $check + $cash + $bank_account + $tips;//(isset($totals['Total']['payment_amount']))?$totals['Total']['payment_amount']-$account_charges-$member_balance-$giftcards:0;		//echo 'Account total: '.$account_charges;
		
		//This section was implemented for East Bay, it is a temporary fix, and must be updated
		//TODO: Remove this and fix the problem of tracking where account balances are spent.	
		$this->db->select('sum(total) as ps_total');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		$this->db->where('department = "Pro Shop"');
		
		$this->db->where('deleted', 0);
		$summary_data = array();
		foreach ($this->db->get()->row_array() as $key => $value)
			if($value > 0 && ($this->session->userdata('course_id') == 6279 || $this->session->userdata('course_id') == 6263 || $this->session->userdata('course_id') == 4857 || $this->session->userdata('course_id') == 4858)) 	
				$summary_data[$key] = $value - $account_charges - $member_balance;
		//END EAST BAY CUSTOM FIX
		//$summary_data['profit'] = 
		$summary_data['subtotal'] = 0;
		//$summary_data['rounding_error'] = 
		if ($this->session->userdata('course_id') == 6279 || $this->session->userdata('course_id') == 6263 || $this->session->userdata('course_id') == 4857 || $this->session->userdata('course_id') == 4858)
		{
			$summary_data['rainchecks'] = 0;
			$summary_data['account_charges'] = 0;
			$summary_data['member_balance'] = 0;
			$summary_data['punch_cards'] = 0;
			$summary_data['giftcards'] = 0;
		}
		$summary_data['tips'] = 0;
		$summary_data['tax'] = 0;
		$summary_data['total'] = 0;
		//Tax totaling fix
        // $this->db->select('sum(subtotal) as subtotal, percent');
		// $this->db->from('sales_items_temp');
		// $this->db->where("percent != ''");
		// if ($this->params['sale_type'] == 'sales')
		// {
			// $this->db->where('quantity_purchased > 0');
		// }
		// elseif ($this->params['sale_type'] == 'returns')
		// {
			// $this->db->where('quantity_purchased < 0');
		// }
		// if ($this->params['department'] != 'all')
			// $this->db->where('department = "'.$this->params['department'].'"');
		// if ($this->params['terminal'] != 'all')
			// $this->db->where('terminal_id = "'.$this->params['terminal'].'"');
// 		
		// $this->db->where('deleted', 0);
		// $this->db->group_by('percent');
		// foreach ($this->db->get()->result_array() as $key => $row)
		// {
			// //print_r($row);
			// $percent = ($row['percent'] != '')?$row['percent']:0;
			// //$summary_data['tax'] += $row['subtotal']*($percent/100);
		// }
		//$summary_data['tax'] = round($summary_data['tax'], 2);
		//End tax totaling fix
		
        $this->db->select('sum(subtotal) as subtotal, sum(tax) as collected_tax, sum(item_cost_price) as cost, sum(total) as total, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		if ($this->params['department'] != 'all' && $this->params['department'] != '')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		
		$this->db->where('deleted', 0);
		//$summary_data = array();
		$totals_array = $this->db->get()->row_array();
//		foreach ($this->db->get()->row_array() as $key => $value) 
	//		if ($key != 'tax' && $key!='subtotal')
		//		$summary_data[$key] = $value - $account_charges;
			//else if ($key == 'subtotal') {
				//$summary_data[$key] = $value;
				//$summary_data['account_charges'] = -$account_charges;
			//}
			//else 
				//$summary_data[$key] = $value;
		if ($this->params['department'] != 'all')
		{
			$summary_data['total'] = $totals_array['total'];
			unset($summary_data['account_charges']);
			unset($summary_data['member_balance']);
			unset($summary_data['giftcards']);
			unset($summary_data['tips']);
		}
		else {
			if ($this->session->userdata('course_id') == 6279 || $this->session->userdata('course_id') == 6263 || $this->session->userdata('course_id') == 4857 || $this->session->userdata('course_id') == 4858)
			{
				$summary_data['total'] = $total_payments;
				$summary_data['account_charges'] = -$account_charges;
				$summary_data['member_balance'] = -$member_balance;
				$summary_data['giftcards'] = -$giftcards;
				$summary_data['punch_cards'] = -$punch_cards;
				$summary_data['rainchecks'] = -$rainchecks;
			}
			else
			{
				$summary_data['total'] = $totals_array['subtotal'] + $tips + $totals_array['collected_tax'];
			}	
			$summary_data['tips'] = $tips;
		}

		
		$summary_data['subtotal'] = $totals_array['subtotal'];
		//$summary_data['rounding_error'] = $totals_array['collected_tax'] - $summary_data['tax'];
		//$summary_data['profit'] = $summary_data['subtotal'] - $totals_array['cost'] + $summary_data['rounding_error'] - $account_charges;
		$summary_data['tax'] = $totals_array['collected_tax'];		
		return $summary_data;
	}

	public function getShortSummaryData()
	{
		$this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}
		
		if ($this->params['department'] != 'all' && $this->params['department'] != '')
			$this->db->where('department = "'.$this->params['department'].'"');
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);

		return $this->db->get()->row_array();
	}
}
?>

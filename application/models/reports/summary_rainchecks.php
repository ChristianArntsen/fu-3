<?php
require_once("report.php");
class Summary_rainchecks extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
        $arr_columns = array(array('data'=>lang('reports_raincheck_id'), 'align'=>'left'), array('data'=>lang('reports_customer_name'), 'align'=>'left'), array('data'=>lang('raincheck_tee_sheet'), 'align'=>'left'), array('data'=>lang('reports_date_issued'), 'align'=>'left'),array('data'=>lang('expiry_date'), 'align'=>'left'), array('data'=>lang('reports_value'), 'align'=> 'right'), array('data'=>lang('reports_status'), 'align'=> 'left'));
        if($this->session->userdata('user_level') == 5) {
            $arr_columns[] = array('data'=>'', 'align'=>'center');
        }
		return $arr_columns;
	}
	
	public function getData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('raincheck_id, title, expiry_date, raincheck_number, date_issued, total, customer_id, first_name, last_name, IF(date_redeemed > "0000-00-00 00:00:00", "<span style=\'color:green\'>Redeemed</span>", "<span style=\'color:red\'>Issued</span>") AS status', false);
		$this->db->from('rainchecks');
		//$this->db->where('deleted', 0);
		$this->db->join('teesheet', 'teesheet.teesheet_id = rainchecks.teesheet_id', 'left');
		$this->db->join('people', 'rainchecks.customer_id = people.person_id', 'left');
		$this->db->order_by('date_issued DESC');

		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		if (!$this->permissions->is_super_admin())
        	$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sum(total) as total, sum(IF(date_redeemed > "0000-00-00 00:00:00", total, 0)) AS redeemed', false);
		$this->db->from('rainchecks');
		//$this->db->where('deleted', 0);
		$this->db->join('teesheet', 'teesheet.teesheet_id = rainchecks.teesheet_id', 'left');
		$this->db->order_by('date_issued DESC');

		$results = $this->db->get()->result_array();		
		
		return $results[0];
	}
}
?>
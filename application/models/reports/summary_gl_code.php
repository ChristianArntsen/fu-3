<?php
require_once("report.php");
class Summary_gl_code extends Report
{
	function __construct()
	{
		parent::__construct();
	}

	public function getDataColumns()
	{
		return array(array('data'=>'GL Code', 'align'=> 'left'), array('data'=>lang('reports_subtotal'), 'align'=> 'right'), array('data'=>lang('reports_total'), 'align'=> 'right'), array('data'=>lang('reports_tax'), 'align'=> 'right'), array('data'=>lang('reports_profit'), 'align'=> 'right'));
	}

	public function getData()
	{
		$this->db->select("IFNULL(gl_code, 'no code') AS gl_code, sum(subtotal) as subtotal, sum(total) as total,
			sum(tax) as tax, sum(profit) as profit, sum(quantity_purchased) as count", false);
		$this->db->from('sales_items_temp');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('quantity_purchased > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('quantity_purchased < 0');
		}

		if ($this->params['department'] != 'all'){
			$this->db->where('department = "'.$this->params['department'].'"');
		}
		$this->db->where($this->db->dbprefix('sales_items_temp').'.deleted', 0);
		$this->db->group_by('gl_code');
		$this->db->order_by('gl_code');

		return $this->db->get()->result_array();
	}
}
?>
<?php
class Ad extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get($ad_id)
	{
		$this->db->from('ads');
		$this->db->where('ad_id', $ad_id);
		$this->db->limit(1);

		return $this->db->get()->row_array();
	}

	function view($campaign_id, $person_id)
	{
		// Get ad to display
		$ad_id = $this->select_ad();
		if(!$ad_id){
			return false;
		}
		$this->add_view($ad_id, $campaign_id, $person_id);

		$ad_info = $this->get($ad_id);
		$filename = $ad_info['image_url'];

		if(file_exists($filename)){
			header('Content-Length: '.filesize($filename)); //<-- sends filesize header
			header('Content-Type: image/jpg'); //<-- send mime-type header
			header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
			readfile($filename); //<--reads and outputs the file onto the output buffer
			die(); //<--cleanup
			exit; //and exit
		}

		return false;
	}

	function click($campaign_id, $person_id)
	{
		$ad_id = (int) $this->get_last_displayed_ad($campaign_id, $person_id);
		$this->add_click($ad_id, $campaign_id, $person_id);

		$ad_info = $this->get($ad_id);
		return $ad_info;
	}

	// Select an ad to display based on least amount of views and if
	// the ad has reached any display or click limits
	function select_ad()
	{
		$this->db->select('ad_id');
		$this->db->from('ads');
		$this->db->where('(view_limit IS NULL OR view_total < view_limit) AND (click_limit IS NULL OR click_total < click_limit)');
		$this->db->order_by('view_total', 'ASC');
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() <= 0)
		{
			return false;
		}

		$row = $query->row_array();
		return $row['ad_id'];
	}

	// Find which ad was displayed to a customer last, so we know which
	// ad was clicked on.
	function get_last_displayed_ad($campaign_id, $person_id){

		if(empty($person_id)){
			return false;
		}

		$this->db->select('ad_id');
		$this->db->from('ad_views');
		$this->db->where(array('person_id' => $person_id, 'campaign_id' => $campaign_id));
		$this->db->order_by('date', 'DESC');
		$this->db->limit(1);

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$row = $query->row_array();
			return $row['ad_id'];
		}

		return false;
	}

	function add_view($ad_id, $campaign_id, $person_id)
	{
		$data = array(
			'ad_id' 		=> (int) $ad_id,
			'campaign_id' 	=> (int) $campaign_id,
			'person_id'		=> (int) $person_id,
			'ip_address' 	=> $this->input->ip_address(),
			'user_agent' 	=> $this->input->user_agent()
		);
		$this->db->insert('ad_views', $data);
		$this->db->query('UPDATE '.$this->db->dbprefix('ads').' SET view_total = view_total + 1 WHERE ad_id = '.(int) $ad_id);
	}

	function add_click($ad_id, $campaign_id, $person_id)
	{
		$data = array(
			'ad_id' 		=> (int) $ad_id,
			'campaign_id' 	=> (int) $campaign_id,
			'person_id'		=> (int) $person_id,
			'ip_address' 	=> $this->input->ip_address(),
			'user_agent' 	=> $this->input->user_agent()
		);
		$this->db->insert('ad_clicks', $data);
		$this->db->query('UPDATE '.$this->db->dbprefix('ads').' SET click_total = click_total + 1 WHERE ad_id = '.(int) $ad_id);
	}
}
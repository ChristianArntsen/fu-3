<?php
class Sale extends CI_Model
{
	public function get_info($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get();
	}
	public function get_total($sale_id){
		$sale_id = (int) $sale_id;

		// Retrieve sale dollar amount including total paid
		$query = $this->db->query("SELECT
			(
				IFNULL((SELECT SUM(total) FROM foreup_sales_items WHERE sale_id = {$sale_id} GROUP BY sale_id), 0) +
				IFNULL((SELECT SUM(total) FROM foreup_sales_item_kits WHERE sale_id = {$sale_id} GROUP BY sale_id), 0) +
				IFNULL((SELECT SUM(total) FROM foreup_sales_invoices WHERE sale_id = {$sale_id} GROUP BY sale_id), 0)
			) AS total,
			IFNULL((SELECT SUM(payment_amount) AS paid FROM foreup_sales_payments WHERE sale_id = {$sale_id} AND payment_amount < 0 GROUP BY sale_id), 0) AS refunded,
			IFNULL((SELECT SUM(payment_amount) AS paid FROM foreup_sales_payments WHERE sale_id = {$sale_id} AND payment_amount > 0 GROUP BY sale_id), 0) AS paid");

		$row = $query->row_array();
		return $row;
	}
	public function get_sale_by_guid($guid)
	{
		$this->db->from('sales');
		$this->db->where('mobile_guid',$guid);
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}
	public function get_sale_ids_by_teetime($teetime_id)
	{
		$this->db->select('sale_id');
		$this->db->from('sales');
		$this->db->where('teetime_id',$teetime_id);
		return $this->db->get();
	}

	function get_cash_sales_total_for_shift($shift_start, $shift_end, $person_id = false, $terminal_id = false, $persisting_log = false)
    {
    	//echo $this->db->last_query();
        $employee_id = $person_id ? $person_id : $this->Employee->get_logged_in_employee_info()->person_id;
		$terminal_id = $terminal_id ? $terminal_id : $this->session->userdata('terminal_id');
		$terminal_id = $terminal_id == '' ? 0 : $terminal_id;
		$sales_totals = $this->get_sales_totaled_by_id($shift_start, $shift_end, $employee_id, $terminal_id, $persisting_log);
		$this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount', false);
        $this->db->from('sales_payments');
        $this->db->join('sales','sales_payments.sale_id=sales.sale_id');
		$this->db->where('sale_time >=', $shift_start);
		$this->db->where('sale_time <=', $shift_end);
		if (!$persisting_log)
			$this->db->where('employee_id', $employee_id);
		$this->db->where('terminal_id', $terminal_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		//$this->db->where("(payment_type IN ('".lang('sales_cash')."','".lang('sales_change_issued')."'))");
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);

		$sales_payments = $this->db->get();
		//echo '<br/><br/>'.$this->db->last_query().'<br/><br/>';
		//print_r($sales_payments->result_array());
		$payments_by_sale = array();
		while($row = $sales_payments->fetch_assoc())
		{
        	$payments_by_sale[$row['sale_id']][] = $row;
		}
		$payment_data = array();
		$cash_tips = 0;
		$non_cash_tips = 0;
		foreach($payments_by_sale as $sale_id => $payment_rows)
		{
			if (isset($sales_totals[$sale_id]))
				$total_sale_balance = $sales_totals[$sale_id];
			else
				$total_sale_balance = 0;

			foreach($payment_rows as $row)
			{
				if (strpos($row['payment_type'], 'Tip') !== false)
				{
					if (strpos($row['payment_type'], 'Cash') !== false)
						$cash_tips += $row['payment_amount'];
					else
						$non_cash_tips += $row['payment_amount'];
				}
				$payment_amount = $row['payment_amount'] <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;
				if ($row['payment_type'] == lang('sales_change_issued') || $row['payment_type'] == 'Cash Refund')
					$row['payment_type'] = lang('sales_cash');
				if (!isset($payment_data[$row['payment_type']]))
				{
					$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
				}

				if ($total_sale_balance != 0)
				{
					$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
				}

				$total_sale_balance-=$payment_amount;
			}
		}
		if (isset($payment_data[lang('sales_cash')]))
		{
			if ($this->config->item('deduct_tips'))
				return $payment_data[lang('sales_cash')]['payment_amount'] - $non_cash_tips;
			else
				return $payment_data[lang('sales_cash')]['payment_amount'] + $cash_tips;
		}

		return 0.00;
    }

	function get_sales_totaled_by_id($shift_start, $shift_end, $employee_id, $terminal_id = 0, $persisting_log = false)
	{
		$employee_sql = $persisting_log ? "AND s.course_id = ".$this->session->userdata('course_id') : "AND s.employee_id = $employee_id ";
		$where = 'WHERE sale_time BETWEEN "'.$shift_start.'" AND "'.$shift_end.'" '.$employee_sql.' AND terminal_id ="'.$terminal_id.'"';
		$this->_create_sales_items_temp_table_query($where);
	//	echo $this->db->last_query();
		$sales_totals = array();

		$this->db->select('sale_id, SUM(total) as total', false);
		$this->db->from('sales_items_temp');
		$this->db->group_by('sale_id');
		$result = $this->db->get()->result_array();
		//echo $this->db->last_query();
//echo '<br/><br/>';
//print_r($result);
		foreach ($result AS $sale_total_row)
		{
			$sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		}

		return $sales_totals;
	}

	/**
	 * added for cash register
	 * insert a log for track_cash_log
	 * @param array $data
	 */

	function insertRegisterLog($data) {
		return $this->db->insert('register_log', $data) ? $this->db->insert_id() : false;
	}
	function getUnfinishedRegisterLog($employee_id = false, $terminal_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$terminal_id = $terminal_id ? $terminal_id : $this->session->userdata('terminal_id');
		$this->db->from('register_log');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('terminal_id', $terminal_id);
		$this->db->where("(employee_id = $employee_id OR persist = 1)");
		$this->db->where('shift_end', '0000-00-00 00:00:00');
		$this->db->limit(1);
		return $this->db->get();
	}
	function getUnfinishedRegisterLogs() {
		$person_id = $this->session->userdata('person_id');
		$this->db->from('register_log');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where("(employee_id = $person_id OR persist = 1)");
		$this->db->where('shift_end', '0000-00-00 00:00:00');
		$this->db->group_by('terminal_id');
		$this->db->order_by('shift_start desc');
		return $this->db->get();
	}
	function updateRegisterLog($data, $employee_id = false, $terminal_id = false, $register_log_id = false) {
		$employee_id = $employee_id ? $employee_id : $this->session->userdata('person_id');
		$terminal_id = $terminal_id ? $terminal_id : $this->session->userdata('terminal_id');
		if ($register_log_id || $data->persist)
		{
			$register_log_id = $register_log_id ? $register_log_id : $data->register_log_id;
			$this->db->where('register_log_id', $register_log_id);
		}
		else
		{
			$this->db->where('course_id', $this->session->userdata('course_id'));
			$this->db->where('terminal_id', $terminal_id);
			$this->db->where('employee_id', $employee_id);
			$this->db->where('shift_end', '0000-00-00 00:00:00');
		}
		$this->db->limit(1);
		unset($data->register_log_id);
		return $this->db->update('register_log', $data);
	}
	function get_register_log_info($register_log_id)
	{
		$this->db->from('register_log');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('register_log_id', $register_log_id);
		$this->db->limit(1);
		return $this->db->get();
	}
	function save_register_log($data, $register_log_id)
	{
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('register_log_id', $register_log_id);
		$this->db->limit(1);
		return $this->db->update('register_log', $data);
	}
	function saveRegisterLogCounts($counts)
	{
		$this->db->insert('register_log_counts', $counts);
	}
	function exists($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	function raincheck_exists($raincheck_id)
	{
		$this->db->from('rainchecks');
		$this->db->where('raincheck_id',$raincheck_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function update($sale_data, $sale_id)
	{
		$this->db->where('sale_id', $sale_id);
		$success = $this->db->update('sales',$sale_data);

		return $success;
	}
	function get_mercury_stats()
	{
		$this->load->model('Communication');

		$cc_sales = $this->db->query("SELECT SUM(payment_amount) AS total FROM foreup_sales_payments WHERE payment_type = 'Credit Card' OR payment_type LIKE 'VISA%' OR payment_type LIKE 'M/C%' OR payment_type LIKE 'DCVR%' OR payment_type LIKE 'AMEX%'")->result_array();

		$foreup_percent = 0.4;
		$results = array();
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$q_results = $this->db->query("SELECT memo, SUM(auth_amount) AS total, COUNT(DISTINCT mercury_id) AS courses,
			sum(IF (trans_post_time >= '$start_date' AND trans_post_time <= '$end_date', auth_amount, 0 )) AS this_month,
			sum(IF (trans_post_time >= '$pm_start_date' AND trans_post_time <= '$pm_end_date', auth_amount, 0 )) AS last_month
			FROM foreup_sales_payments_credit_cards WHERE status = 'Approved' AND mercury_id != 494691720 AND mercury_id != 88430119384 AND mercury_id != '' GROUP BY memo");
		while($result = $q_results->fetch_assoc())
			$results[$result['memo']] = $result;

		$data = array(
			'cc_sales'=>to_currency($cc_sales[0]['total']),
			'courses'=> empty($results['ForeUP v.1.0']) ? '' : $results['ForeUP v.1.0']['courses'],
			'mercury_sales'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['total']),
			'last_month'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['last_month'] * $foreup_percent/100),
			'this_month'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['this_month'] * $foreup_percent/100),
			'total_revenue'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['total'] * $foreup_percent/100)
		);
		return $data;
	}
	function get_mercury_course_stats()
	{
		$this->load->model('Communication');

		//$cc_sales = $this->db->query("SELECT name, foreup_courses.course_id AS course_id, SUM(payment_amount) AS total FROM foreup_sales_payments JOIN foreup_sales ON foreup_sales_payments.sale_id = foreup_sales.sale_id JOIN foreup_courses ON foreup_courses.course_id = foreup_sales.course_id WHERE foreup_sales_payments.payment_type = 'Credit Card' OR foreup_sales_payments.payment_type LIKE 'VISA%' OR foreup_sales_payments.payment_type LIKE 'M/C%' OR foreup_sales_payments.payment_type LIKE 'DCVR%' OR foreup_sales_payments.payment_type LIKE 'AMEX%' GROUP BY course_id")->result_array();
		$foreup_percent = 0.25;
		$results = array();
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$q_results = $this->db->query("SELECT name, foreup_courses.course_id AS course_id, foreup_courses.mercury_id AS mercury_id, SUM(auth_amount) AS total, SUM(auth_amount) * $foreup_percent/100 AS total_revenue,
			sum(IF (trans_post_time >= '$start_date' AND trans_post_time <= '$end_date', auth_amount, 0 )) AS this_month,
			sum(IF (trans_post_time >= '$start_date' AND trans_post_time <= '$end_date', auth_amount, 0 )) * $foreup_percent/100 AS this_month_revenue,
			sum(IF (trans_post_time >= '$pm_start_date' AND trans_post_time <= '$pm_end_date', auth_amount, 0 )) AS last_month,
			sum(IF (trans_post_time >= '$pm_start_date' AND trans_post_time <= '$pm_end_date', auth_amount, 0 )) * $foreup_percent/100 AS last_month_revenue
			FROM foreup_sales_payments_credit_cards JOIN foreup_courses ON foreup_courses.course_id = foreup_sales_payments_credit_cards.course_id
			WHERE status = 'Approved' AND foreup_courses.mercury_id != '' AND foreup_courses.mercury_id = foreup_sales_payments_credit_cards.mercury_id
			AND foreup_courses.mercury_id != 494691720 AND foreup_courses.mercury_id != 88430119384 AND foreup_courses.mercury_id != ''
			GROUP BY course_id");
		//echo $this->db->last_query();

		//foreach($cc_sales as $cc_sale)
		//	$sales[$cc_sale['course']]
		//foreach($q_results as $result)
		//	$results[$result['course_id']] = $result;

		/*$data = array(
			'cc_sales'=>to_currency($cc_sales[0]['total']),
			'courses'=>$results['ForeUP v.1.0']['courses'],
			'mercury_sales'=>to_currency($results['ForeUP v.1.0']['total']),
			'last_month'=>to_currency($results['ForeUP v.1.0']['last_month'] * $foreup_percent/100),
			'this_month'=>to_currency($results['ForeUP v.1.0']['this_month'] * $foreup_percent/100),
			'total_revenue'=>to_currency($results['ForeUP v.1.0']['total'] * $foreup_percent/100)
		);*/
		return $q_results;
	}
	function save($items, $customer_id, $employee_id, $comment, $payments, $sale_id=false, $teetime_id=-1, $course_id=false, $guid = NULL, $sale_date = false, $table_id = '', $terminal_id = false, $gratuity = 0)
	{
		log_debug_message($this->config->item('course_id'), "STARTING Sale->save sale_id ".
			' items: '.json_encode($items).
			' customer_id: '.$customer_id.
			' employee_id: '.$employee_id.
			' payments: '.json_encode($payments).
			' sale_id: '.$sale_id.
			' teetime_id: '.$teetime_id,
			$this->session->userdata('tracker_sale_id'));
			
		$this->session->unset_userdata('additional_receipt_data');
		$additional_receipt_data = array('giftcards'=>array(), 'punchcards'=> array(), 'loyalty'=>array(), 'accounts'=>array());

		if(count($items) == 0){
			return -1;
		}

		// Load necessary models for validation below
		$this->load->model('item_taxes');
		$this->load->model('item_kit_taxes');
		$this->load->model('fee');
		$this->load->model('green_fee');
		$this->load->model('Customer_loyalty');
		$this->load->model('teesheet');
		$this->load->model('schedule');
		$this->load->model('Giftcard');
		$this->load->model('Price_class');
		$this->load->model('Account_transactions');
		$this->load->model('Member_account_transactions');

		$tax_included = $this->config->item('unit_price_includes_tax');
		$loyalty_points = 0;
		$qualified_dollars = 0;
		$total_dollars = 0;
		$item_points = 0;
		$item_dollar_points = 0;
		$item_list = '';
		$payment_types='';
		$total_paid=0;
		$added_gratuity=0;

		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		$course_id = (int) $course_id;

		$customer = $this->Customer->get_info($customer_id);
		if ($this->config->item('use_loyalty') && $customer_id != -1 && $customer->use_loyalty) {
			$additional_receipt_data['loyalty']['balance'] = $customer->loyalty_points;
		}
		// HANDLE ALL LOYALTY STUFF IN THIS LOOP
		foreach($payments as $payment_id => $payment)
		{
			if ($payment['payment_type'] != 'Change issued' && $payment['payment_type'] != 'Tip')
	            $total_paid += $payment['payment_amount'];

			// IF ANY FORM OF PAYMENT IS USED, WE'RE AWARDING LOYALTY POINTS
			if ($this->config->item('use_loyalty') && $customer_id != -1 && $customer->use_loyalty &&
				($payment['payment_type'] == lang('sales_cash') ||
				$payment['payment_type'] == lang('sales_check') ||
				$payment['payment_type'] == lang('sales_credit') ||
				strpos($payment['payment_type'], 'VISA') !== false ||
				strpos($payment['payment_type'], 'M/C') !== false ||
				strpos($payment['payment_type'], 'AMEX') !== false ||
				strpos($payment['payment_type'], 'DISC') !== false ||
				strpos($payment['payment_type'], 'DINERS') !== false ||
				strpos($payment['payment_type'], 'JCB') !== false ||
				strpos($payment['payment_type'], 'Bank Acct') !== false))
			{
				$qualified_dollars += ($payment['payment_amount']);
			}
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
		}

		$sales_data = array(
			//'sale_number'=>$this->get_next_sale_number(),
			'sale_time' => ($sale_date ? date('Y-m-d H:i:s', strtotime($sale_date)) : date('Y-m-d H:i:s')),
			'customer_id'=> $this->Customer->exists($customer_id) ? $customer_id : null,
			'teetime_id' => ($teetime_id == -1)?'':$teetime_id,
			'employee_id' => $employee_id,
			'override_authorization_id' => $this->session->userdata('purchase_override'),
			'terminal_id' => $terminal_id ? $terminal_id : $this->session->userdata('terminal_id'),
			'payment_type' => $payment_types,
			'comment' => $comment,
            'course_id' => $course_id?$course_id:$this->session->userdata('course_id'),
            'mobile_guid' => $guid,
            'table_id' => $table_id
		);

		// Check if sale is taxable, if no session variable exists,
		// assume it is taxable
		$is_taxable = $this->session->userdata('taxable');
		if(empty($is_taxable) || $is_taxable == 'true'){
			$is_taxable = true;
		}else{
			$is_taxable = false;
		}

		$taxable_basket = ($customer_id == -1 or $customer->taxable) && $is_taxable;

		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();

		$this->db->insert('sales', $sales_data);
		$sale_id = $this->db->insert_id();
		log_debug_message($this->config->item('course_id'), "INSERTED SALE # ".$sale_id.": ".$this->db->last_query(), $this->session->userdata('tracker_sale_id'));
		//$sale_number  = $sales_data['sale_number'];
		if ($sale_id == -1 && $guid != NULL)
		{
			$guid_sale = $this->get_sale_by_guid($guid);
			$sale_id = $guid_sale['sale_id'];
		}
		// else if ($sale_id == -1)
		// {
			// return;
		// }

		$price_types = array();
		if ($comment != 'Auto Billed'){
			$price_types = ($this->permissions->course_has_module('reservations')?$this->fee->get_type_info():$this->green_fee->get_type_info());
		}
		foreach($items as $line => $item)
		{
			$invoice_id = false;
			if(!empty($item['invoice_id'])){
				$invoice_id = (int) $item['invoice_id'];
			}
			$p = $item['price'];
            $q = $item['quantity'];
            $d = $item['discount'];
			if (isset($item['item_id']) && $item['item_id'] != 0)
			{
				// LOYALTY TOTALS IF WE'RE USING LOYALTY
				if ($this->config->item('use_loyalty') && $customer_id != -1 && $customer->use_loyalty)
				{
					$item_points = $this->Customer_loyalty->get_points_per_dollar($item);
					$item_dollar_points += $p * $item_points['points_per_dollar'] * $q * (100-$d)/100;
				}
				$cur_item_info = $this->Item->get_info($item['item_id']);
				$cur_item_taxes = $this->item_taxes->get_info($item['item_id']);
				$item_list .= '('.$q.') '.$cur_item_info->name.'<br/>';

				$price_category_label = '';
				if ($item['price_category'] && $item['price_category'] != '')
				{
					if($this->config->item('seasonal_pricing') == 1){
						$price_class = $this->Price_class->get($item['price_category']);
						$price_category_label = $price_class['name'];
					}else{
						$price_category = 'price_category_'.$item['price_category'];
						$price_category_label = $price_types[$this->session->userdata('teesheet_id')]->$price_category;
					}
				}

				// If item was split, split the cost
				$itemCost = (float) $cur_item_info->cost_price;
				if(!empty($item['num_splits']) && (int) $item['num_splits'] > 1){
					$itemCost = round($itemCost / (int) $item['num_splits'], 2);
				}

				$num_splits = 1;
				if(!empty($item['num_splits'])){
					$num_splits = (int) $item['num_splits'];
				}

				$is_side = 0;
				if(!empty($item['is_side'])){
					$is_side = 1;
				}

				if($table_id === ''){
					$item['tax'] = 0;
					foreach ($cur_item_taxes as $item_tax)
					{
						if ($tax_included) {
                            $actual_price = round(($p / (1 + $item_tax['percent'] /100)) * $q, 2);
                            $item['tax'] += ($p * $q - $actual_price);
                            //$item['subtotal'] = $actual_price;
                        }
						else {
                            $item['tax'] += round($item_tax['percent']/100 * $p * $q * (100 - $d)/100,2);
						}
					}
				}
				
				// If item is to pay down customer or member account balance
				if ($item['item_number'] == 'account_balance' || $item['item_number'] == 'member_balance' || $item['item_number'] == 'invoice_balance')
				{
					$account_type = false;
					if ($item['item_number'] == 'account_balance'){
						$account_type = 'customer';
						$item['serialnumber'] = 'CustomerBalance';
						// Apply payment amount to account
						$this->Account_transactions->save($account_type, $customer_id, 'Balance Payment', $p, lang('sales_point_of_sale').' '.$sale_id, $sale_id, (int) $item['invoice_id'], $employee_id);
					}else if ($item['item_number'] == 'member_balance'){
						$account_type = 'member';
						$item['serialnumber'] = 'MemberBalance';
						// Apply payment amount to account
						$this->Account_transactions->save($account_type, $customer_id, 'Balance Payment', $p, lang('sales_point_of_sale').' '.$sale_id, $sale_id, (int) $item['invoice_id'], $employee_id);
					}else if ($item['item_number'] == 'invoice_balance'){
						$account_type = 'invoice';
						$item['serialnumber'] = 'InvoiceBalance';
						$most_recent_invoice = $this->Invoice->get_all($customer_id, $limit=1)->row_array();
						$this->Invoice->pay_invoice((int) $sale_id, (int) $most_recent_invoice['invoice_id'], (int) $employee_id, $item['price']);
						$this->Account_transactions->save($account_type, $customer_id, 'Invoice Payment', $p, lang('sales_point_of_sale').' '.$sale_id, $sale_id, (int) $item['invoice_id'], $employee_id);
					}
				}

				$sales_items_data = array(
					'sale_id' => $sale_id,
					'item_id' => $item['item_id'],
					'invoice_id' => (int) $item['invoice_id'],
					'line' => $item['line'],
					'description' => $item['description'],
					'teesheet' => $this->permissions->course_has_module('reservations') ? $this->schedule->get_info($this->session->userdata('schedule_id'))->title : $this->teesheet->get_info($this->session->userdata('teesheet_id'))->title,
					'price_category' => $price_category_label?$price_category_label:'',
					'serialnumber' => $item['serialnumber'],
					'quantity_purchased' => $q,
					'discount_percent' => $d,
					'item_cost_price' => $itemCost,
					'item_unit_price'=> $p,
					'unit_price_includes_tax' => $item['unit_price_includes_tax'] != null ? $item['unit_price_includes_tax'] : 0,
					'num_splits' => $num_splits,
					'is_side' => $is_side,
					'subtotal' => $item['subtotal'],
					'tax' => $taxable_basket == 'true' ? $item['tax'] : 0,
					'total' => $taxable_basket == 'true' ? $item['total'] : $item['subtotal']
				);
				$sales_items_data['teesheet'] = $sales_items_data['teesheet']==NULL ? '' : $sales_items_data['teesheet'];
				$sales_items_data['profit'] = round($q * (($p * ((100 - $d) / 100)) - $itemCost), 2);
				$sales_items_data['total_cost'] = round($q * $itemCost, 2);

				$item_success = $this->db->insert('sales_items', $sales_items_data);
				log_debug_message($this->config->item('course_id'), "INSERTED SALES ITEMS: ".($item_success?"Y":"N")." ".$this->db->last_query(), $this->session->userdata('tracker_sale_id'));

				// Insert any modifiers
				if(!empty($item['modifiers'])){
					foreach($item['modifiers'] as $key => $modifier){
						$modifierData = array();
						$modifierData['sale_id'] = $sale_id;
						$modifierData['item_id'] = $item['item_id'];
						$modifierData['line'] = $item['line'];
						$modifierData['modifier_id'] = $modifier['modifier_id'];
						$modifierData['selected_option'] = $modifier['selected_option'];

						if(empty($modifier['selected_price'])){
							$modifier['selected_price'] = '0.00';
						}
						$modifierData['selected_price'] = $modifier['selected_price'];

						$this->db->insert('sales_items_modifiers', $modifierData);
					}
				}

				//Create purchased giftcards
				if ($item['is_giftcard'] && $this->config->item('use_ets_giftcards') == 0)
				{
					$giftcard_data = $item['giftcard_data'];
					$customer_name = $giftcard_data['customer_name'];
					if ($giftcard_data['action'] == 'reload')
					{
						$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $giftcard_data['giftcard_number'] );
						$this->Giftcard->update_giftcard_value( $giftcard_data['giftcard_number'], $cur_giftcard_value + $item['price'] );
					}
					else
					{
						//Removing customer name before we try and save it with the giftcard data
						unset($giftcard_data['customer_name']);
						unset($giftcard_data['action']);
						$giftcard_data['value'] = $item['price'];
						$giftcard_data['customer_id'] = ($giftcard_data['customer_id'])?$giftcard_data['customer_id']:null;
						$giftcard_data['course_id'] = $this->session->userdata('course_id');
						if ($giftcard_data['customer_id'] === null && $customer_name != '' && $customer_name != 'No Customer')
						{
							$this->load->library('name_parser');
							$np = new Name_parser();
							$np->setFullName($customer_name);
							$np->parse();
							if (!$np->notParseable()) {
								$person_data = array(
									'first_name'=>$np->getFirstName(),
									'last_name'=>$np->getLastName()
								);
								$customer_data = array(
									'course_id'=>$this->session->userdata('course_id')
								);
								if ($this->Customer->save($person_data, $customer_data))
								{
									$giftcard_data['customer_id'] = $customer_data['person_id'];
								}
							}
						}
						$this->Giftcard->save($giftcard_data);
					}
				}

				// Only update inventory if item is from regular POS
				// F&B inventory is deducted when the items are added to
				// an order
				if($table_id === ''){

					//Update stock quantity
					if (!$this->Item->is_teetime($item['item_id'])){
						$item_data = array('quantity'=>$cur_item_info->quantity - $q);
						$this->Item->save($item_data,$item['item_id']);
					}

					//Ramel Inventory Tracking
					//Inventory Count Details
					$qty_buy = -$q;
					$sale_remarks ='POS '.$sale_id;
					$inv_data = array(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item['item_id'],
						'trans_user'=>$employee_id,
						'trans_comment'=>$sale_remarks,
						'trans_inventory'=>$qty_buy
					);
					$this->Inventory->insert($inv_data);
				}
			}
			else if(isset($item['tournament_id']))
			{
				if ($item['tournament_id'] == '')
					continue;
				$tournament_info = $this->Tournament->get_info($item['tournament_id']);
				$tournament_price_before_taxes = $this->Tournament->tournament_price_before_taxes($tournament_info);

				//get all items associated with the tournament and update their quantity. items table
				$tournament_inventory_items = $this->Tournament_inventory_items->get_info($tournament_info->tournament_id);
				$inventory_items_at_cost = 0;

				foreach($this->Tournament_inventory_items->get_info($tournament_info->tournament_id) as $tournament_inventory_item)
				{
					//make sql statement to reduce the quantity on the item table
					$item_id = $tournament_inventory_item->item_id;
					$inventory_items_at_cost += $this->Item->get_info($item_id)->cost_price;
					$quantity_sold = $tournament_inventory_item->quantity * $item['quantity'];
					$this->db->query("UPDATE foreup_items SET quantity = quantity - $quantity_sold WHERE item_id = $item_id ");

					//log the inventory change transaction
					//Ramel Inventory Tracking
					//Inventory Count Details
					$trans_comment ='POS '.$sale_id.' tournament '.$tournament_info->tournament_id;
					$inv_data = array
					(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item_id,
						'trans_user'=>$employee_id,
						'trans_comment'=>$trans_comment,
						'trans_inventory'=>-$quantity_sold
					);
					$this->Inventory->insert($inv_data);
				}

				//update the tournament pot
				$this->Tournament->update_tournament_pot($tournament_info, $item['quantity']);

				//post to customer account
				$add = $tournament_info->customer_credit_fee * $item['quantity'];
				$person_id = $this->sale_lib->get_customer();
				$person_id = $person_id ? $person_id : $customer_id;
				$trans_description = 'Customer Credit From Tournament Purchase';
				$trans_details = 'Amount to add to credit';
				$this->Account_transactions->save('customer', $person_id, $trans_description, $add, $trans_details);

				//post to member account
				$add = $tournament_info->member_credit_fee * $item['quantity'];
				$trans_description = 'Member Credit From Tournament Purchase';
				$trans_details = 'Amount to add to member credit';
				$this->Account_transactions->save('member', $person_id, $trans_description, $add, $trans_details);

				//post the sale to sales_tournaments table
				// $sales_tournament_data = array
				// (
					// 'sale_id'=>$sale_id,
					// 'tournament_id'=>$item['tournament_id'],
					// 'teesheet'=>$this->permissions->course_has_module('reservations') ? $this->schedule->get_info($this->session->userdata('schedule_id'))->title : $this->teesheet->get_info($this->session->userdata('teesheet_id'))->title,
					// 'price_category'=>'',
					// 'line'=>$item['line'],
					// 'quantity_purchased'=>1,
					// 'tournament_cost_price'=>$inventory_items_at_cost,
					// 'tournament_unit_price'=>$tournament_price_before_taxes,
					// 'discount_percent'=>$d,
					// 'taxes_paid'=>$tournament_info->total_cost - $tournament_price_before_taxes
				// );
				// $this->db->insert('sales_tournaments', $sales_tournament_data);

				//INSTEAD OF SAVING AS A SALE TOURNAMENT, WE'RE GOING TO HAVE AN ITEM CREATED FOR THIS TOURNAMENT AND SAVE IT THAT WAY
				$item['cost_price'] = $inventory_items_at_cost;
				$item['unit_price'] = $tournament_price_before_taxes;
				$item['tax_rate'] = round(($tournament_info->total_cost - $tournament_price_before_taxes) / $tournament_price_before_taxes * 100, 3);
				$tourn_item_id = $this->Item->get_tournament_item($item);
				$item['item_id'] = $tourn_item_id;
				$sales_items_data = array(
					'sale_id' => $sale_id,
					'item_id' => $tourn_item_id,
					'line' => $item['line'],
					'description' => $item['description'],
					'teesheet' => $this->permissions->course_has_module('reservations') ? $this->schedule->get_info($this->session->userdata('schedule_id'))->title : $this->teesheet->get_info($this->session->userdata('teesheet_id'))->title,
					'price_category' => '',
					'serialnumber' => null,
					'quantity_purchased' => $item['quantity'],
					'discount_percent' => $d,
					'item_cost_price' => $inventory_items_at_cost,
					'item_unit_price'=> $tournament_price_before_taxes,
					'num_splits' => 0,
					'is_side' => 0,
					'subtotal' => $tournament_price_before_taxes * $item['quantity'],
					'tax' => ($taxable_basket == 'true' ? $tournament_info->total_cost - $tournament_price_before_taxes : 0) * $item['quantity'],
					'total' => ($taxable_basket == 'true' ? $tournament_info->total_cost : $tournament_price_before_taxes) * $item['quantity']
				);
				$sales_items_data['teesheet'] = $sales_items_data['teesheet']==NULL ? '' : $sales_items_data['teesheet'];
				$sales_items_data['profit'] = round($item['quantity'] * (($tournament_price_before_taxes * ((100 - 0) / 100)) - $inventory_items_at_cost), 2);
				$sales_items_data['total_cost'] = round($item['quantity'] * $inventory_items_at_cost, 2);

				$this->db->insert('sales_items', $sales_items_data);
			}
			// If item is a custom line (not inventory or item kit) added to an invoice
			else if(!empty($item['invoice_id']) && $item['item_id'] === 0 && $item['item_kit_id'] === 0)
			{
				$sales_item = array(
					'sale_id' => $sale_id,
					'item_id' => 0,
					'invoice_id' => (int) $item['invoice_id'],
					'line' => $item['line'],
					'description' => $item['description'],
					'teesheet' => '',
					'price_category' => $price_category_label ? $price_category_label:'',
					'serialnumber' => $item['serialnumber'],
					'quantity_purchased' => $item['quantity'],
					'discount_percent' => $item['discount'],
					'item_cost_price' => 0,
					'total_cost' => 0,
					'profit' => $item['subtotal'],
					'item_unit_price'=> $item['price'],
					'num_splits' => 1,
					'is_side' => 0,
					'subtotal' => $item['subtotal'],
					'tax' => $item['tax'],
					'total' => $item['total']
				);

				$item_list .= '('.$item['quantity'].') '.$item['description'].'<br/>';

				// If item is to pay down customer or member account balance
				if ($item['serialnumber'] == 'MemberBalance' || $item['serialnumber'] == 'CustomerBalance')
				{
					$account_type = false;
					if ($item['serialnumber'] == 'CustomerBalance'){
						$account_type = 'customer';
					}else if ($item['serialnumber'] == 'MemberBalance'){
						$account_type = 'member';
					}

					// Apply payment amount to account
					$this->Account_transactions->save($account_type, $customer_id, 'Invoiced Balance Transfer', $item['price'], lang('sales_point_of_sale').' '.$sale_id, $sale_id, $item['invoice_id'], $employee_id, $course_id);
				}

				$this->db->insert('sales_items', $sales_item);
			}
			// If item is an invoice being paid for
			else if(!empty($item['invoice_id']))
			{
				// Apply payment to invoice, if payment is over invoice total, go back and pay old invoices
				$this->Invoice->pay_invoice((int) $sale_id, (int) $item['invoice_id'], (int) $employee_id, $item['price']);
				$this->Account_transactions->save('invoice', $customer_id, 'Invoice Payment', $item['price'], lang('sales_point_of_sale').' '.$sale_id, $sale_id, (int) $item['invoice_id'], $employee_id, $course_id);

				// Add invoice as its own item connected to sale
				$sales_item = array(
					'sale_id' => $sale_id,
					'invoice_id' => $item['invoice_id'],
					'line' => $item['line'],
					'description' => $item['description'],
					'teesheet' => '',
					'price_category' => $price_category_label ? $price_category_label:'',
					'quantity_purchased' => 1,
					'discount_percent' => 0,
					'invoice_cost_price' => 0,
					'total_cost' => 0,
					'profit' => $item['price'],
					'invoice_unit_price'=> $item['price'],
					'subtotal' => $item['price'],
					'tax' => 0,
					'total' => $item['price']
				);

				$this->db->insert('sales_invoices', $sales_item);
			}
			else
			{
				// LOYALTY TOTALS IF WE'RE USING LOYALTY
				if ($this->config->item('use_loyalty') && $customer_id != -1 && $customer->use_loyalty)
				{
					$item_points = $this->Customer_loyalty->get_points_per_dollar($item);
					$item_dollar_points += $p * $item_points['points_per_dollar'] * $q * (100-$d)/100;
				}

				$cur_item_kit_info = $this->Item_kit->get_info($item['item_kit_id']);
				$cur_item_kit_taxes = $this->item_kit_taxes->get_info($item['item_kit_id']);

				$sales_item_kits_data = array
				(
					'sale_id'=>$sale_id,
					'item_kit_id'=>$item['item_kit_id'],
					'invoice_id' => (int) $item['invoice_id'],
					'line'=>$item['line'],
					'description'=>$item['description'],
					'quantity_purchased'=>$q,
					'discount_percent'=>$d,
					'item_kit_cost_price' => $cur_item_kit_info->cost_price,
					'item_kit_unit_price'=>$p
				);

				$sales_item_kits_data['subtotal'] = round($q * ($p * ((100 - $d) / 100)), 2);

				// Calculate item kit taxes
				$tax_amount = 0;
				if (($customer_id == -1 or $customer->taxable) && $this->session->userdata('taxable') == 'true')
	 			{
		 			if(!empty($cur_item_kit_taxes)){
						foreach($cur_item_kit_taxes as $tax){
							$t = $tax['percent'];
							if ($tax_included) {
                                $actual_price = round(($p / (1 + $t /100)) * $q, 2);
                                $tax_amount = ($p * $q - $actual_price);
                                $sales_item_kits_data['subtotal'] = $actual_price;
                            }
                            else if($tax['cumulative'] == 1){
								$tax_amount += round(($sales_item_kits_data['subtotal'] + $tax_amount) * ($t / 100), 2);
							}else{
								$tax_amount += round($sales_item_kits_data['subtotal'] * ($t / 100), 2);
							}
						}
					}
				}
				$sales_item_kits_data['tax'] = $tax_amount;
				$sales_item_kits_data['total'] = round($tax_amount + $sales_item_kits_data['subtotal'], 2);
				$sales_item_kits_data['profit'] = round($q * (($p * ((100 - $d) / 100)) - $cur_item_kit_info->cost_price), 2);
				$sales_item_kits_data['total_cost'] = round($q * $cur_item_kit_info->cost_price, 2);

				$this->db->insert('sales_item_kits',$sales_item_kits_data);
				$item_kit_items = $this->Item_kit_items->get_info($item['item_kit_id']);
				foreach($item_kit_items as $item_kit_item)
				{
					$cur_item_info = $this->Item->get_info($item_kit_item->item_id);

					//Update stock quantity
					$item_data = array('quantity'=>$cur_item_info->quantity - ($item['quantity'] * $item_kit_item->quantity));
					$this->Item->save($item_data,$item_kit_item->item_id);

					//Ramel Inventory Tracking
					//Inventory Count Details
					$qty_buy = -$item['quantity'] * $item_kit_item->quantity;
					$item_list .= '('.-$item['quantity'] * $item_kit_item->quantity.') '.$cur_item_info->name.'<br/>';
					$sale_remarks ='POS '.$sale_id;
					$inv_data = array
					(
						'trans_date'=>date('Y-m-d H:i:s'),
						'trans_items'=>$item_kit_item->item_id,
						'trans_user'=>$employee_id,
						'trans_comment'=>$sale_remarks,
						'trans_inventory'=>$qty_buy
						);
					$this->Inventory->insert($inv_data);
				}
				//Create purchased punch cards
				if ($item['is_punch_card'])
				{
					$punch_card_data = $item['punch_card_data'];
					$customer_name = $punch_card_data['customer_name'];
					$item_kit_id = $punch_card_data['item_kit_id'];
					//Removing customer name before we try and save it with the giftcard data
					unset($punch_card_data['customer_name']);
					unset($punch_card_data['item_kit_id']);
					//$punch_card_data['value'] = $item['price'];
					$punch_card_data['customer_id'] = ($punch_card_data['customer_id'])?$punch_card_data['customer_id']:null;
					$punch_card_data['course_id'] = $this->session->userdata('course_id');

					// SAVE PUNCH CARD
					$this->Punch_card->save($punch_card_data);
					// SAVE PUNCH CARD ITEMS
					$punch_card_items = array();
					foreach($item_kit_items as $item_kit_item)
					{
						$punch_card_items[] = array(
							'punch_card_id' => $punch_card_data['punch_card_id'],
							'item_id' => $item_kit_item->item_id,
							'punches' => $item_kit_item->quantity,
							'used'	  => 0
						);
					}
					$this->Punch_card->save_items($punch_card_items);
				}

			}

			if (($customer_id == -1 or $customer->taxable) && $this->session->userdata('taxable') == 'true')
 			{
 				if (isset($item['item_id']))
				{
					foreach($this->Item_taxes->get_info($item['item_id']) as $row)
					{
						$this->db->insert('sales_items_taxes', array(
							'sale_id' 	=>$sale_id,
							'item_id' 	=>$item['item_id'],
							'line'      =>$item['line'],
							'name'		=>$row['name'],
							'percent' 	=>$row['percent'],
							'cumulative'=>$row['cumulative']
						));
					}
				}
				else
				{
					foreach($this->Item_kit_taxes->get_info($item['item_kit_id']) as $row)
					{
						$this->db->insert('sales_item_kits_taxes', array(
							'sale_id' 		=>$sale_id,
							'item_kit_id'	=>$item['item_kit_id'],
							'line'      	=>$item['line'],
							'name'			=>$row['name'],
							'percent' 		=>$row['percent'],
							'cumulative'	=>$row['cumulative']
						));
					}
				}

				//INSERT TOURNAMENT TAXES HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			}
		}

		$credit_card_invoice = array();
		$payment_types = '';
		foreach($payments as $payment_id=>$payment)
		{
			if ($payment['payment_type'] == 'Tip')
            {
                unset($payments[$payment_id]);
                continue;
            }
			$cab_name=($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
			$cmb_name=($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');

			if ( substr( $payment['payment_type'], 0, strlen( lang('sales_giftcard') ) ) == lang('sales_giftcard') )
			{
				/* We have a gift card and we have to deduct the used value from the total value of the card. */
				$splitpayment = explode( ':', $payment['payment_type'] );
				$cur_giftcard_value = $this->Giftcard->get_giftcard_value( $splitpayment[1] );
				$this->Giftcard->update_giftcard_value( $splitpayment[1], $cur_giftcard_value - $payment['payment_amount'] );
				$additional_receipt_data['giftcards'][] = array('number'=>$splitpayment[1], 'new_balance'=>$cur_giftcard_value - $payment['payment_amount']);
			}
			else if ( substr( $payment['payment_type'], 0, strlen( lang('sales_punch_card') ) ) == lang('sales_punch_card') )
			{
				/* We have a gift card and we have to deduct the used value from the total value of the card. */
				$splitpayment = explode( ':', $payment['payment_type'] );
                $punch_card_items = $this->sale_lib->get_punch_card_items();
                if(count($punch_card_items) > 0) {
                    $punch_card_item_id = $punch_card_items[$splitpayment[1]];
                    $current_count = $this->Punch_card->update_punch_card_value($splitpayment[1], 1, $punch_card_item_id);
                }
                // Not to update punch card used when item not found
				//$current_count = $this->Punch_card->update_punch_card_value($splitpayment[1], 1, $punch_card_item_id);
				// JUST NEED TO SET UP RETURNING THE CURRENT COUNT
				//$additional_receipt_data['punchcards'][] = array('number'=>$splitpayment[1], 'new_balance'=>$current_count);
			}
			else if (strpos($payment['payment_type'], $cab_name)!==false)
			{
				$this->Account_transactions->save('customer', $payment['customer_id'], 'POS '.$sale_id, -$payment['payment_amount'], $item_list, $sale_id, $invoice_id, $employee_id, $course_id);
			}
			else if (strpos($payment['payment_type'], $cmb_name)!==false)
			{
				$this->Account_transactions->save('member', $payment['customer_id'], 'POS '.$sale_id, -$payment['payment_amount'], $item_list, $sale_id, $invoice_id, $employee_id, $course_id);
			}
			else if ($payment['payment_type'] == 'Invoice Charge' || stripos($payment['payment_type'], 'Billing Account Balance') !== false)
			{
				$this->Account_transactions->save('invoice', $payment['customer_id'], 'POS '.$sale_id, -$payment['payment_amount'], $item_list, $sale_id, $payment['invoice_id'], $employee_id, $course_id);
			}
			//IF ANY FORM OF PAYMENT IS LOYALTY, DEBIT THE POINTS FROM THE ACCOUNT
			else if ($payment['loyalty_point_value'] > 0 && $customer_id != -1 && $customer->use_loyalty)
			{
				$additional_receipt_data['loyalty']['spent'] = $payment['loyalty_point_value'];
				$additional_receipt_data['loyalty']['balance'] -= $payment['loyalty_point_value'];
				$this->Customer_loyalty->save_transaction($customer_id, 'POS '.$sale_id, -$payment['loyalty_point_value'], '', $sale_id, $employee_id);
			}
            // If gratuity is added, add that payment here
            if ($gratuity > 0 && $added_gratuity < 1)
            {
                if ($payment['payment_amount'] > $gratuity)
				{
				    $sales_payments_data = array
				    (
				        'sale_id'=>$sale_id,
				        'payment_type'=>$payment['payment_type'].' Tip',
				        'payment_amount'=>$gratuity,
				        'invoice_id'=>$payment['invoice_id']
				    );
				    $payments_insert = $this->db->insert('sales_payments',$sales_payments_data);
				    $added_gratuity = 1;
				    // Reduce actual payment by the amount of the tip
				    $payment['payment_amount'] -= $gratuity;
					$payment_types=$payment_types.$payment['payment_type'].' Tip: '.to_currency($gratuity).'<br />';
				}
			}
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>$payment['payment_type'],
				'payment_amount'=>$payment['payment_amount'],
				'invoice_id'=>$payment['invoice_id']
			);
			$payment_types=$payment_types.$payment['payment_type'].': '.to_currency($payment['payment_amount']).'<br />';
			$payments_insert = $this->db->insert('sales_payments',$sales_payments_data);
			log_debug_message($this->config->item('course_id'), "INSERTED PAYMENTS: ".($payments_insert?"Y":"N")." ".$this->db->last_query(), $this->session->userdata('tracker_sale_id'));
			if ($payment['invoice_id'])
			{
				$credit_card_invoice[] = array('invoice_id' => $payment['invoice_id'], 'payment_type' => $payment['payment_type']);
			}
		}
		if ($gratuity > 0) {
			$this->db->where('sale_id', $sale_id);
			$this->db->update('sales', array('payment_type'=>$payment_types));
		}
		// LOYALTY POINTS
		if ($this->config->item('use_loyalty') && $customer_id != -1)
		{
			$adjusted_item_points = 0;
			if ($item_dollar_points > 0)
			{
				if ($qualified_dollars < $total_paid)
				{
				    // DUE TO PAYMENTS OF GIFTCARDS, CUSTOMER CREDIT, MEMBER BALANCE, WE NEED TO REDUCE THE ITEM POINTS
					$adjusted_item_points = floor($item_dollar_points * $qualified_dollars / $total_paid);
				}
				else
				{
					$adjusted_item_points = $item_dollar_points;
				}
			}

			// RUN TRANSACTIONS ON LOYALTY
			if ($adjusted_item_points > 0 && $customer_id != -1 && $customer->use_loyalty){
				$additional_receipt_data['loyalty']['earned'] += $adjusted_item_points;
				$additional_receipt_data['loyalty']['balance'] += $adjusted_item_points;
				$this->Customer_loyalty->save_transaction($customer_id, 'POS '.$sale_id, $adjusted_item_points, '', $sale_id, $employee_id);
			}
		}
		//$this->db->insert('sales', array('sale_id'=>1));
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return -1;
		}

		// NO LONGER STORING RECENT TRANSACTIONS IN SESSION
		$recent_transactions = (array) $this->session->userdata('recent_transactions');
        $customer_name = $customer->last_name == '' ? $customer->first_name : $customer->last_name;
        $sale_info = array('sale_id'=>$sale_id, 'customer_name'=>$customer_name, 'total'=>$total_paid, 'sale_time'=>$sales_data['sale_time'], 'credit_card_invoice'=>$credit_card_invoice);
        array_unshift($recent_transactions, $sale_info);
        $this->session->set_userdata('recent_transactions', $recent_transactions);
        $this->session->set_userdata('last_sale_id', array('sale_id'=>$sale_id,'sale_name'=>$sale_id));
		if (isset($additional_receipt_data['loyalty']['balance']))
		{
			$additional_receipt_data['loyalty']['spent'] = round($additional_receipt_data['loyalty']['spent'],0);
			$additional_receipt_data['loyalty']['earned'] = round($additional_receipt_data['loyalty']['earned'],0);
			$additional_receipt_data['loyalty']['balance'] = round($additional_receipt_data['loyalty']['balance'],0);
		}
		$this->session->set_userdata('additional_receipt_data', array("POS $sale_id" => $additional_receipt_data));

		return $sale_id;
	}

	function get_balance_item($type = 'account_balance') {
		$item_id = $this->Item->get_item_id($type);
		if (!$item_id)
		{
			$name = lang("customers_{$type}");
			$ccn = $this->config->item('customer_credit_nickname');
			$mbn = $this->config->item('member_balance_nickname');
			$department = lang("sales_account_payments");
			$category = lang("sales_account_payments");
			if ($type == 'account_balance' && trim($ccn) != '' && $ccn !== 0)
				$name = $ccn;
			else if ($type == 'member_balance' && trim($mbn) != '' && $mbn !== 0)
				$name = $mbn;
			else if ($type == 'invoice_balance')
			{
				$department = lang('sales_invoice_payments');
				$category = lang('sales_invoice_payments');
			}
			else if ($type == 'tee_time_charge')
			{
				$name = 'Tee Time Charge';
				$departments = $this->Item->get_teetime_department();
				$department = $departments[0]['department'];
				$category =lang('teesheets_no_shows');
			}
			else if ($type == 'tournament_payout')
			{
				$name = 'Tournament Payout';
				$department = 'Tournaments';
				$category = 'Tournaments';
			}
			// CREATE ITEM
			$item_data = array(
				'name'=>$name,
				'department'=>$department,
				'category'=>$category,
				'item_number'=>$type,
				'cost_price'=>0,
				'unit_price'=>0,
				'max_discount'=>100,
				'quantity'=>0,
				'is_unlimited'=>1,
				'reorder_level'=>0,
				'is_serialized'=>1,
				'invisible'=>1,
				'course_id'=>$this->session->userdata('course_id')
			);
			$this->Item->save($item_data);
			$item_id = $item_data['item_id'];
		}
		return $item_id;
	}
	function get_balance_item_sales($type, $tee_time_id = false)
	{
		$item_id = $this->get_balance_item($type);
		$this->db->select('sales.sale_id as sale_id, sale_time, total');
		$this->db->from('sales_items');
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		$this->db->where('item_id', $item_id);
		if ($tee_time_id)
			$this->db->where('teetime_id', $tee_time_id);

		return $this->db->get()->result_array();
	}
	function get_recent_transactions($limit = 5, $employee_id = null) {
		$this->db->select("sales.sale_id AS sale_id, last_name AS customer_name, sum(payment_amount) AS total, sale_time, GROUP_CONCAT(invoice_id SEPARATOR '|') AS invoice_ids");
		$this->db->from('sales');
		$this->db->join('sales_payments', 'sales_payments.sale_id = sales.sale_id');
		$this->db->join('people', 'people.person_id = sales.customer_id', 'left');
		$this->db->limit($limit);
		$this->db->group_by('sales.sale_id');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->where('sale_time >',  date('Y-m-d 00:00:00'));
		if(!empty($employee_id)){
			$this->db->where('sales.employee_id', (int) $employee_id);
		}
		$this->db->order_by('sale_time desc');
		$sales = $this->db->get()->result_array();
		foreach($sales AS $index => $sale)
		{
			$cc_payments = '';
			if ($sale['invoice_ids'] && trim(str_replace("|", "", $sale['invoice_ids'])) != '')
			{
				$cc_payments = $this->db->query("SELECT invoice AS invoice_id, CONCAT(card_type, ' ', masked_account) AS payment_type FROM foreup_sales_payments_credit_cards WHERE invoice IN (".trim(str_replace("|", ",", $sale['invoice_ids']),',').")")->result_array();
			}
			$sales[$index]['credit_card_invoice'] = $cc_payments;
		}

		return $sales;
	}

	function delete($sale_id)
	{
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;

		$this->db->select('item_id, quantity_purchased');
		$this->db->from('sales_items');
		$this->db->where('sale_id', $sale_id);

		foreach($this->db->get()->result_array() as $sale_item_row)
		{
			// Only adjust inventory if line has item id associated with it
			if(!empty($sale_item_row['item_id'])){

				$cur_item_info = $this->Item->get_info($sale_item_row['item_id']);
				$item_data = array('quantity'=>$cur_item_info->quantity + $sale_item_row['quantity_purchased']);
				$this->Item->save($item_data,$sale_item_row['item_id']);

				$sale_remarks ='POS '.$sale_id;
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$sale_item_row['item_id'],
					'trans_user'=>$employee_id,
					'trans_comment'=>$sale_remarks,
					'trans_inventory'=>$sale_item_row['quantity_purchased']
					);
				$this->Inventory->insert($inv_data);
			}
		}

		$this->db->select('item_kit_id, quantity_purchased');
		$this->db->from('sales_item_kits');
		$this->db->where('sale_id', $sale_id);

		foreach($this->db->get()->result_array() as $sale_item_kit_row)
		{
			foreach($this->Item_kit_items->get_info($sale_item_kit_row['item_kit_id']) as $item_kit_item)
			{
				$cur_item_info = $this->Item->get_info($item_kit_item->item_id);

				$item_data = array('quantity'=>$cur_item_info->quantity + ($sale_item_kit_row['quantity_purchased'] * $item_kit_item->quantity));
				$this->Item->save($item_data,$item_kit_item->item_id);

				$sale_remarks ='POS '.$sale_id;
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$item_kit_item->item_id,
					'trans_user'=>$employee_id,
					'trans_comment'=>$sale_remarks,
					'trans_inventory'=>$sale_item_kit_row['quantity_purchased'] * $item_kit_item->quantity
				);
				$this->Inventory->insert($inv_data);
			}
		}

		// Reverse loyalty payments made
		$loyalty_transactions = $this->Customer_loyalty->get_all_by_sale_id($sale_id);
		if(!empty($loyalty_transactions)){
			foreach($loyalty_transactions as $lt)
			{
				$this->Customer_loyalty->save_transaction($lt['trans_customer'], 'Deleted POS '.$sale_id, -$lt['trans_amount'], '', $sale_id, $employee_id);
			}
		}

		// Reverse any account charges made
		$transactions = $this->Account_transactions->get_all_by_sale_id($sale_id);
		if(!empty($transactions)){
			foreach($transactions as $transaction)
			{
				$this->Account_transactions->save($transaction['account_type'], (int) $transaction['trans_customer'], 'Deleted POS '.$sale_id, -$transaction['trans_amount'], '', $sale_id, $transaction['invoice_id'], $employee_id);
			}
		}

		$this->db->where('sale_id', $sale_id);
		return $this->db->update('sales', array('deleted' => 1, 'deleted_by' => $employee_id, 'deleted_at' => date('Y-m-d H:i:s')));
	}

	function undelete($sale_id)
	{
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;

		$this->db->select('item_id, quantity_purchased');
		$this->db->from('sales_items');
		$this->db->where('sale_id', $sale_id);

		foreach($this->db->get()->result_array() as $sale_item_row)
		{
			$cur_item_info = $this->Item->get_info($sale_item_row['item_id']);
			$item_data = array('quantity'=>$cur_item_info->quantity - $sale_item_row['quantity_purchased']);
			$this->Item->save($item_data,$sale_item_row['item_id']);

			$sale_remarks ='POS '.$sale_id;
			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$sale_item_row['item_id'],
				'trans_user'=>$employee_id,
				'trans_comment'=>$sale_remarks,
				'trans_inventory'=>-$sale_item_row['quantity_purchased']
				);
			$this->Inventory->insert($inv_data);
		}

		$this->db->select('item_kit_id, quantity_purchased');
		$this->db->from('sales_item_kits');
		$this->db->where('sale_id', $sale_id);

		foreach($this->db->get()->result_array() as $sale_item_kit_row)
		{
			foreach($this->Item_kit_items->get_info($sale_item_kit_row['item_kit_id']) as $item_kit_item)
			{
				$cur_item_info = $this->Item->get_info($item_kit_item->item_id);

				$item_data = array('quantity'=>$cur_item_info->quantity - ($sale_item_kit_row['quantity_purchased'] * $item_kit_item->quantity));
				$this->Item->save($item_data,$item_kit_item->item_id);

				$sale_remarks ='POS '.$sale_id;
				$inv_data = array
				(
					'trans_date'=>date('Y-m-d H:i:s'),
					'trans_items'=>$item_kit_item->item_id,
					'trans_user'=>$employee_id,
					'trans_comment'=>$sale_remarks,
					'trans_inventory'=>-$sale_item_kit_row['quantity_purchased'] * $item_kit_item->quantity
				);
				$this->Inventory->insert($inv_data);
			}
		}

		$this->db->where('sale_id', $sale_id);
		return $this->db->update('sales', array('deleted' => 0));
	}

	function get_sale_items($sale_id)
	{
		$this->db->from('sales_items');
		$this->db->where('sale_id',$sale_id);
		$this->db->order_by('line ASC');
		return $this->db->get();
	}

	function get_sale_invoices($sale_id){
		$this->db->from('sales_invoices');
		$this->db->where('sale_id',$sale_id);
		$this->db->group_by('invoice_id');
		$results = $this->db->get();
		return $results;
	}

	function get_sale_item_kits($sale_id)
	{
		$this->db->from('sales_item_kits');
		$this->db->where('sale_id',$sale_id);
		$this->db->order_by('line ASC');
		return $this->db->get();
	}

	function get_sale_items_taxes($sale_id)
	{
		$query = $this->db->query('SELECT name, percent, cumulative, item_unit_price as price, quantity_purchased as quantity, discount_percent as discount, unit_price_includes_tax '.
		'FROM '. $this->db->dbprefix('sales_items_taxes'). ' JOIN '.
		$this->db->dbprefix('sales_items'). ' USING (sale_id, item_id, line) WHERE '.$this->db->dbprefix('sales_items_taxes').".sale_id = '$sale_id' ORDER BY cumulative");
		return $query->result_array();
	}

	function get_sale_item_kits_taxes($sale_id)
	{
		$query = $this->db->query('SELECT name, percent, cumulative, item_kit_unit_price as price, quantity_purchased as quantity, discount_percent as discount '.
		'FROM '. $this->db->dbprefix('sales_item_kits_taxes'). ' JOIN '.
		$this->db->dbprefix('sales_item_kits'). ' USING (sale_id, item_kit_id, line) WHERE '.$this->db->dbprefix('sales_item_kits_taxes').".sale_id = '$sale_id' ORDER BY cumulative");
		return $query->result_array();
	}

	function get_sale_payments($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id',$sale_id);
		return $this->db->get();
	}
	function get_sale_tips($sale_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id',$sale_id);
		$this->db->like('payment_type', lang('sales_tip'), 'right');
		return $this->db->get();
	}
	function get_sale_payment($sale_id, $invoice_id)
	{
		$this->db->from('sales_payments');
		$this->db->where('sale_id', $sale_id);
		$this->db->where('invoice_id', $invoice_id);
		return $this->db->get();
	}
	function add_ets_refund_payment($invoice, $payment_amount, $response)
	{
		// $payment_info = array(
			// 'course_id'=>$this->session->userdata('course_id'),
			// 'ets_id'=>$this->config->item('ets_id'),
			// 'operator_id'=>$this->session->userdata('person_id'),
			// 'tran_type'=>'CreditReturnToken',
			// 'frequency'=>'OneTime',
			// 'status_message'=>(string)$response->transactions->message,
			// 'status'=>(string)$response->status
		// );
//
		// $this->add_credit_card_payment($payment_info);
		//$this->add_credit_card_payment($payment_info);
		$this->db->where('invoice', $invoice);
		$this->db->update('sales_payments_credit_cards', array('token_used'=>1,'amount_refunded'=>$payment_amount));
		return $sql;
	}
	function add_refund_payment($invoice, $payment_amount, $response)
	{
		//Get information about original payment
		/*$this->db->select('sale_id, payment_type');
		$this->db->from('sales_payments');
		$this->db->where('invoice_id', $invoice);
		$this->db->limit(1);
		$payment_info = $this->db->get()->result_array();
		 *
		 */
		//nevermind, new payments will be added during refund
		//Add to sales table under payment_type...
		//$this->db->query("UPDATE sales SET payment_type = CONCAT(payment_type, 'Refund: {$payment_info[0]['payment_type']}: {$payment_amount}<br/>') WHERE sale_id = '$sale_id'");
		//Add to sales_payments as a payment
		//$this->db->insert('sales_payments', array('sale_id'=>$payment_info[0]['sale_id'], 'payment_type'=>$payment_info[0]['payment_type'], 'payment_amount'=>$payment_amount));
		//Update cc payment to record that the token is used
		$payment_info = array(
			'course_id'=>$this->session->userdata('course_id'),
			'mercury_id'=>$this->config->item('mercury_id'),
			'operator_id'=>$this->session->userdata('person_id'),
			'tran_type'=>'CreditReturnToken',
			'frequency'=>'OneTime',
			'acq_ref_data'=>(string)$response->AcqRefData,
			'auth_code'=>(string)$response->AuthCode,
			'status_message'=>(string)$response->Message,
			'ref_no'=>(string)$response->RefNo,
			'status'=>(string)$response->Status,
			'card_type'=>(string)$response->CardType,
			'token'=>(string)$response->Token,
			'auth_amount'=>(string)$response->AuthorizeAmount,
			'process_Data'=>(string)$response->ProcessData,
			'amount'=>(string)$response->PurchaseAmount,
			'gratuity_amount'=>(string)$response->GratuityAmount
		);

		$this->add_credit_card_payment($payment_info);
		//$sql = $this->db->last_query();
		$this->db->where('invoice', $invoice);
		 $this->db->update('sales_payments_credit_cards', array('token_used'=>1,'amount_refunded'=>$payment_amount));
		 return $sql;
	}
	function get_customer($sale_id)
	{
		$this->db->from('sales');
		$this->db->where('sale_id',$sale_id);
		return $this->Customer->get_info($this->db->get()->row()->customer_id);
	}

	function add_credit_card_payment($payment_info) {
		$this->db->insert('sales_payments_credit_cards', $payment_info);
		return $this->db->insert_id();
	}

	function update_credit_card_payment($invoice, $payment_info) {
		$this->db->where('invoice', $invoice);
		return $this->db->update('sales_payments_credit_cards', $payment_info);
	}
	function get_credit_card_payment($invoice) {
		$this->db->from('sales_payments_credit_cards');
		$this->db->where('invoice', $invoice);
		$this->db->limit(1);
		return $this->db->get()->result_array();
	}
	function get_credit_card_payments($sale_id) {
		$this->db->from('sales_payments');
		$this->db->join('sales_payments_credit_cards','sales_payments.invoice_id=sales_payments_credit_cards.invoice');
		$this->db->where('sale_id', $sale_id);
		$result = $this->db->get();
		//echo $this->db->last_query();
		return $result->result_array();
	}
	function raincheck_redeemed() {
		//echo 'getting to raincheck redeemed';
		$raincheck_id = $this->session->userdata('raincheck_id');
		$data = array('date_redeemed'=>date('Y-m-d H:i:s'));
		$this->db->where('raincheck_id', $raincheck_id);
		//echo 'everything looking good til here';
		return $this->db->update('rainchecks', $data);
		// echo $this->db->last_query();
	}
	function get_raincheck_id($raincheck_number, $teesheet_id = '')
	{
		$teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('teesheet_id');
		$this->db->from('rainchecks');
		$this->db->where('raincheck_number', $raincheck_number);
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(1);
		$result = $this->db->get()->row_array();

		return isset($result['raincheck_id']) ? $result['raincheck_id'] : -1;
	}
	function get_next_raincheck_number($teesheet_id = '')
	{
		$teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('teesheet_id');
		$this->db->from('rainchecks');
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->order_by('raincheck_number', 'desc');
		$this->db->limit(1);
		$result = $this->db->get()->row_array();

		return isset($result['raincheck_number']) ? $result['raincheck_number'] + 1 : 1;
	}
	function get_raincheck_info($teesheet_id = '') {
		$raincheck_info = array();
		$teesheet_id = ($teesheet_id != '')?$teesheet_id:$this->session->userdata('teesheet_id');

		if($this->config->item('seasonal_pricing') == 1){

			$this->load->model('Pricing');

			$raincheck_info['green_fees'] = array('' => '- No Green Fee -');
			$raincheck_info['cart_fees'] = array('' => '- No Cart Fee -');

			$raincheck_info['green_fees'] += $this->Pricing->get_teetime_price_classes();
			$raincheck_info['cart_fees'] += $this->Pricing->get_cart_price_classes();

			$teesheet_options = $this->teesheet->get_all();
			$raincheck_info['teesheet_options'] = array();
			while ($teesheet = $teesheet_options->fetch_assoc())
			{
				$raincheck_info['teesheet_options'][$teesheet['teesheet_id']] = $teesheet['title'];
			}

		}else{

			if ($this->permissions->course_has_module('reservations'))
			{
				$schedule_options = $this->schedule->get_all();
				$raincheck_info['teesheet_options'] = array();
				while ($schedule = $schedule_options->fetch_assoc())
				{
					$raincheck_info['teesheet_options'][$schedule['schedule_id']] = $schedule['title'];
					$fees = $this->Fee->get_info('', $schedule['schedule_id']);
					$raincheck_info['green_fees'][$schedule['schedule_id']] = $fees[$schedule['schedule_id']];
				}
			}
			else
			{
				$teesheet_options = $this->teesheet->get_all();
				$raincheck_info['teesheet_options'] = array();
				while ($teesheet = $teesheet_options->fetch_assoc())
				{
					$raincheck_info['teesheet_options'][$teesheet['teesheet_id']] = $teesheet['title'];
					$green_fees = $this->Green_fee->get_info('', $teesheet['teesheet_id']);
					$raincheck_info['green_fees'][$teesheet['teesheet_id']] = $green_fees[$teesheet['teesheet_id']];
				}
			}
			$raincheck_info['fees'] = $raincheck_info['green_fees'];

			// Check against settings for if it is currently the weekend
			$dow = date('w');
			$type = 'weekday';
			$holes = 18;
			if (($dow == 5 && $this->config->item('weekend_fri'))|| ($dow == 6 && $this->config->item('weekend_sat')) || ($dow == 0 && $this->config->item('weekend_sun')))
				$type = 'weekend';
			if ($holes == 9) {
				$cart_num = '1';
				$item_num = '3';
			}
			else if ($holes == 18) {
				$cart_num = '5';
				$item_num = '7';
			}
			if ($type=='weekend')
			{
				$cart_num += 1;
				$item_num += 1;
			}

			$teetime_type_index = 7;
			$raincheck_info['green_fee_dropdown'] = form_dropdown("green_fee_dropdown", array_merge(array(''=>''),($this->permissions->course_has_module('reservations')?$this->Fee->get_teetime_types($item_num, $teesheet_id, true,true):$this->Green_fee->get_teetime_types($item_num, $teesheet_id, true,true))));
			$teetime_type_index = 4;
			$raincheck_info['cart_fee_dropdown'] = form_dropdown("cart_fee_dropdown", array_merge(array(''=>''), ($this->permissions->course_has_module('reservations')?$this->Fee->get_cart_types($cart_num, $teesheet_id, true,true):$this->Green_fee->get_cart_types($cart_num, $teesheet_id, true,true))));
		}

		return $raincheck_info;
	}
	function raincheck_info($raincheck_id)
	{
		$this->db->where('raincheck_id', $raincheck_id);
		$this->db->from('rainchecks');
		$this->db->limit(1);
		return $this->db->get();
	}
	function get_teetime_rainchecks($teetime_id)
	{
		$this->db->where('teetime_id', $teetime_id);
		$this->db->from('rainchecks');
		return $this->db->get();
	}
	function record_teetime_raincheck($teetime_id, $raincheck_count)
	{
		$teetime_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE foreup_teetime SET raincheck_players_issued = raincheck_players_issued + $raincheck_count WHERE (TTID = '$teetime_id' OR TTID = '{$teetime_id}b') LIMIT 2");
	}
	function raincheck_is_used_or_expired($raincheck_id)
	{
		$ri = $this->raincheck_info($raincheck_id)->result_array();
		if (strtotime($ri[0]['date_redeemed']) > strtotime('0000-00-00 00:00:00'))
			return true;

		return false;
	}
	function save_raincheck($data) {
		return $this->db->insert('rainchecks', $data) ? $this->db->insert_id() : false;
	}
    function get_sale_id($sale_number)
    {
    	$sale_number = str_replace('POS ', '', $sale_number);
        $this->db->from('sales');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where('sale_number', $sale_number);
        $this->db->limit(1);
        $result = $this->db->get()->row_array();

        return isset($result['sale_id']) ? $result['sale_id'] : -1;
    }
    function get_next_sale_number()
    {
        $this->db->from('sales');
        $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->order_by('sale_number', 'desc');
        $this->db->limit(1);
        $result = $this->db->get()->row_array();

        return isset($result['sale_number']) ? $result['sale_number'] + 1 : 1;
    }

	//We create a temp table that allows us to do easy report/sales queries
	public function create_sales_items_temp_table($params, $filter_course = true)
	{
    	$where = "WHERE 1=1 ";

		$course_id = (isset($params['course_id']) && $params['course_id'] != ''?$params['course_id']:$this->session->userdata('course_id'));

		if($filter_course){
			$where .= " AND "."s.course_id = ".(int) $course_id;
		}
		if (isset($params['customer_id']))
		{
			$where .= " AND "."s.customer_id = {$params['customer_id']}";
		}
        if (isset($params['start_date']) && isset($params['end_date']))
    	{
        	$where .= ' AND sale_time BETWEEN '.$this->db->escape($params['start_date']).' AND '.$this->db->escape($params['end_date']);
    	}
    	if (isset($params['filter']) && isset($params['value']) && $params['value'] != 'all')
    	{
        	$where .= " AND {$params['filter']} = '".urldecode(urldecode($params['value']))."'";
    	}
    	if (isset($params['sale_id']))
    	{
        	$where .= " AND "."s.sale_id = {$params['sale_id']}";
    	}
        if(isset($params['terminal']) && $params['terminal'] != 'all') {
            $where .= " AND "."terminal_id = {$params['terminal']}";
        }
        if(isset($params['department']) && $params['department'] != 'all') {
            $where .= " AND "."department = '" . $params['department'] . "'";
        }
        if(isset($params['cat']) && $params['cat'] != 'all') {
            $where .= " AND "."category = '" . $params['cat'] . "'";
        }
        if(isset($params['sub_cat']) && $params['sub_cat'] != 'all') {
            $where .= " AND "."subcategory = '" . $params['sub_cat'] . "'";
        }
        if(isset($params['employee_id']) && $params['employee_id'] != '') {
            $where .= " AND "."s.employee_id = '" . $params['employee_id'] . "'";
        }
        $this->_create_sales_items_temp_table_query($where);
	}

	function _create_sales_items_temp_table_query($where)
	{
		$start_time = time();
		$category = ($this->config->item('separate_courses'))?"IF((price_category != '' AND teesheet != ''), CONCAT_WS(' ', teesheet, IF(i.item_id = 0, 'Invoice Line Items', category)), IF(si.item_id = 0, 'Invoice Line Items', category)) AS category":"IF(si.item_id = 0, 'Invoice Line Items', category) AS category";
		$this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('sales_items_temp')."
			(
				KEY(sale_id),
				KEY(deleted),
				KEY(customer_id),
				KEY(employee_id),
				KEY(sale_date)
			)
			ENGINE=innodb AS (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				si.serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				si.line,
				comment,payment_type,
				customer_id,
				s.employee_id,
				i.item_id,
				NULL as item_kit_id,
				NULL as invoice_id,
				supplier_id,
				si.quantity_purchased,
				item_cost_price,
				item_unit_price,
				item_number,
				i.name as name,
				IF(si.item_id = 0, 'Invoice Line Items', department) AS department,
				$category,
				IF(price_category != '', price_category,subcategory) AS subcategory,
				gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost
			FROM ".$this->db->dbprefix('sales_items')." AS si
			INNER JOIN ".$this->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->db->dbprefix('items')." AS i
				ON si.item_id = i.item_id
			$where
			) UNION ALL (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				'' AS serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				si.line,
				comment,payment_type,
				customer_id,
				s.employee_id,
				NULL AS item_id,
				i.item_kit_id,
				NULL AS invoice_id,
				NULL AS supplier_id,
				si.quantity_purchased,
				item_kit_cost_price,
				item_kit_unit_price,
				item_kit_number,
				i.name as name,
				department, IF((price_category != '' AND teesheet != ''),
				CONCAT_WS(' ', teesheet, category), category) AS category, IF(price_category != '',
				price_category,subcategory) AS subcategory,
				NULL AS gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost
			FROM ".$this->db->dbprefix('sales_item_kits')." AS si
			INNER JOIN ".$this->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->db->dbprefix('item_kits')." AS i
				ON si.item_kit_id = i.item_kit_id
			$where
			) UNION ALL (
			SELECT
				s.deleted AS deleted,
				sale_time AS sale_date,
				'' AS serialnumber,
				si.description,
				s.teetime_id,
				terminal_id,
				s.sale_id,
				si.line,
				comment,payment_type,
				customer_id,
				s.employee_id,
				NULL AS item_id,
				NULL AS item_kit_id,
				i.invoice_id,
				NULL AS supplier_id,
				si.quantity_purchased,
				invoice_cost_price,
				invoice_unit_price,
				NULL AS item_kit_number,
				i.name as name,
				department, IF((price_category != '' AND teesheet != ''),
				CONCAT_WS(' ', teesheet, category), category) AS category, IF(price_category != '',
				price_category,subcategory) AS subcategory,
				NULL AS gl_code,
				discount_percent,
				si.subtotal,
				si.total,
				si.tax,
				si.profit,
				si.total_cost
			FROM ".$this->db->dbprefix('sales_invoices')." AS si
			INNER JOIN ".$this->db->dbprefix('sales')." AS s
				ON si.sale_id = s.sale_id
			LEFT JOIN ".$this->db->dbprefix('invoices')." AS i
				ON si.invoice_id = i.invoice_id
			$where
			)");
		$end_time = time();
		$seconds = $end_time - $start_time;
		file_put_contents("query_times.txt", "----------------------".PHP_EOL."[REPORTS] Sales Temp Table Creation Time: {$seconds} seconds".PHP_EOL, FILE_APPEND);

		return true;

		$category = ($this->config->item('separate_courses'))?"IF((price_category != '' AND teesheet != ''), CONCAT_WS(' ', teesheet, category), category) AS category":'category';
		//$category = "CONCAT_WS(' ', teesheet, category), category) AS category";
        $this->db->query("CREATE TEMPORARY TABLE ".$this->db->dbprefix('sales_items_temp')."
		(SELECT ".$this->db->dbprefix('sales').".deleted as deleted, sale_time as sale_date, ".$this->db->dbprefix('sales').".teetime_id, terminal_id, ".$this->db->dbprefix('sales_items').".sale_id, comment,payment_type, customer_id, employee_id,
		".$this->db->dbprefix('items').".item_id, NULL as item_kit_id, NULL as invoice_id, supplier_id, quantity_purchased, item_cost_price, item_unit_price, item_number, foreup_items.name as name, department, $category, IF(price_category != '', price_category,subcategory) AS subcategory,
		discount_percent, (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		".$this->db->dbprefix('sales_items').".line as line, serialnumber, ".$this->db->dbprefix('sales_items').".description as description,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)+ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
		ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) - (item_cost_price*quantity_purchased) as profit
		FROM ".$this->db->dbprefix('sales_items')."
		INNER JOIN ".$this->db->dbprefix('sales')." ON  ".$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('items')." ON  ".$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('items').'.item_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('suppliers')." ON  ".$this->db->dbprefix('items').'.supplier_id='.$this->db->dbprefix('suppliers').'.person_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('sales_items_taxes')." ON  "
		.$this->db->dbprefix('sales_items').'.sale_id='.$this->db->dbprefix('sales_items_taxes').'.sale_id'." and "
		.$this->db->dbprefix('sales_items').'.item_id='.$this->db->dbprefix('sales_items_taxes').'.item_id'." and "
		.$this->db->dbprefix('sales_items').'.line='.$this->db->dbprefix('sales_items_taxes').'.line'. "
		$where
		GROUP BY sale_id, item_id, line)
		UNION ALL
		(SELECT ".$this->db->dbprefix('sales').".deleted as deleted, sale_time as sale_date, ".$this->db->dbprefix('sales').".teetime_id, terminal_id, ".$this->db->dbprefix('sales_item_kits').".sale_id, comment,payment_type, customer_id, employee_id,
		NULL as item_id, ".$this->db->dbprefix('item_kits').".item_kit_id, NULL as invoice_id, '' as supplier_id, quantity_purchased, item_kit_cost_price, item_kit_unit_price, item_kit_number, foreup_item_kits.name as name, department, category, subcategory,
		discount_percent, (item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		".$this->db->dbprefix('sales_item_kits').".line as line, '' as serialnumber, ".$this->db->dbprefix('sales_item_kits').".description as description,
		ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)+ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
		ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
		(item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100) - (item_kit_cost_price*quantity_purchased) as profit
		FROM ".$this->db->dbprefix('sales_item_kits')."
		INNER JOIN ".$this->db->dbprefix('sales')." ON  ".$this->db->dbprefix('sales_item_kits').'.sale_id='.$this->db->dbprefix('sales').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('item_kits')." ON  ".$this->db->dbprefix('sales_item_kits').'.item_kit_id='.$this->db->dbprefix('item_kits').'.item_kit_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('sales_item_kits_taxes')." ON  "
		.$this->db->dbprefix('sales_item_kits').'.sale_id='.$this->db->dbprefix('sales_item_kits_taxes').'.sale_id'." and "
		.$this->db->dbprefix('sales_item_kits').'.item_kit_id='.$this->db->dbprefix('sales_item_kits_taxes').'.item_kit_id'." and "
		.$this->db->dbprefix('sales_item_kits').'.line='.$this->db->dbprefix('sales_item_kits_taxes').'.line'. "
		$where
		GROUP BY sale_id, item_kit_id, line)
		UNION ALL
		(SELECT ".$this->db->dbprefix('sales').".deleted as deleted, sale_time as sale_date, ".$this->db->dbprefix('sales').".teetime_id, terminal_id, ".$this->db->dbprefix('sales_invoices').".sale_id, comment,payment_type, customer_id, employee_id,
		NULL as item_id, NULL as item_kit_id, ".$this->db->dbprefix('invoices').".invoice_id as invoice_id, '' as supplier_id, quantity_purchased, invoice_cost_price, invoice_unit_price, invoice_number, invoice_number as name, 'Invoices' as department, 'Invoices' as category, '' as subcategory,
		discount_percent, (invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		".$this->db->dbprefix('sales_invoices').".line as line, '' as serialnumber, ".$this->db->dbprefix('sales_invoices').".description as description,
		ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)+ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
		ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2)
		+((ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) + (invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100))
		*(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
		(invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100) - (invoice_cost_price*quantity_purchased) as profit
		FROM ".$this->db->dbprefix('sales_invoices')."
		INNER JOIN ".$this->db->dbprefix('sales')." ON  ".$this->db->dbprefix('sales_invoices').'.sale_id='.$this->db->dbprefix('sales').'.sale_id'."
		INNER JOIN ".$this->db->dbprefix('invoices')." ON  ".$this->db->dbprefix('sales_invoices').'.invoice_id='.$this->db->dbprefix('invoices').'.invoice_id'."
		LEFT OUTER JOIN ".$this->db->dbprefix('sales_invoices_taxes')." ON  "
		.$this->db->dbprefix('sales_invoices').'.sale_id='.$this->db->dbprefix('sales_invoices_taxes').'.sale_id'." and "
		.$this->db->dbprefix('sales_invoices').'.invoice_id='.$this->db->dbprefix('sales_invoices_taxes').'.invoice_id'." and "
		.$this->db->dbprefix('sales_invoices').'.line='.$this->db->dbprefix('sales_invoices_taxes').'.line'. "
		$where
		GROUP BY sale_id, invoice_id, line) ORDER BY sale_id, line");
		//echo $this->db->last_query();
	}

	public function get_giftcard_value( $giftcardNumber )
	{
		if ( !$this->Giftcard->exists( $this->Giftcard->get_giftcard_id($giftcardNumber)))
			return 0;

		$this->db->from('giftcards');
		$this->db->where('giftcard_number',$giftcardNumber);
		return $this->db->get()->row()->value;
	}

  /**
   * get total mercury sales
   * @param boolean $last_week optional
   * @return int
   */
  public function get_total_mecury_sales($last_week = false)
  {
    $this->db->select('sum(amount) total');
    $this->db->from('sales_payments_credit_cards');
    $this->db->where("LOWER( tran_type ) =  'sale' AND lower(status) = 'approved' AND mercury_id IS NOT NULL");
    if (!$this->permissions->is_super_admin())
      $this->db->where('course_id', $this->session->userdata('course_id'));
    if($last_week === true)
        $this->db->where('YEARWEEK(trans_post_time) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');
    $query = $this->db->get();

    return $query->row()->total;
  }
  /**
   * get total sales
   * @param boolean $last_week optional
   * @return int
   */
  public function get_total_sales($last_week = false)
  {
    $course_id = '';
      if (!$this->permissions->is_super_admin())
          $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
    $this->db->select('sum(payment_amount) total');
    $this->db->from('sales_payments');
    $this->db->join('sales', 'sales_payments.sale_id = sales.sale_id');
    $this->db->where("deleted = 0 {$course_id}");
    if($last_week === true)
        $this->db->where('YEARWEEK(sale_time) = YEARWEEK(CURRENT_DATE - INTERVAL 7 DAY)');

    $query = $this->db->get();

    return $query->row()->total;
  }
  /**
   * get_purchases
   * this will retrieve current purchases
   * @param mixed $limit
   * @return mixed
   */
  public function get_purchases($limit = false)
  {
    if($limit !== false) $limit = "limit {$limit}";

    $query = "
        SELECT
         {$this->db->dbprefix('sales_items')}.description, {$this->db->dbprefix('sales')}.sale_time,{$this->db->dbprefix('sales_payments')}.payment_amount
        FROM {$this->db->dbprefix('sales')}, {$this->db->dbprefix('sales_payments')}, {$this->db->dbprefix('sales_items')}
        where  {$this->db->dbprefix('sales_payments')}.sale_id = `foreup_sales`.sale_id
        and {$this->db->dbprefix('sales')}.sale_id = {$this->db->dbprefix('sales_items')}.sale_id
        order by {$this->db->dbprefix('sales')}.`sale_time` desc
        {$limit}
      ";

    $query = $this->db->query($query);

    return $query;
  }

	// Retrieve all items,item kits, and invoices belonging to a sale
	public function get_sale_details($sale_id){

		if(empty($sale_id)){
			return false;
		}

		$query = $this->db->query("
			(SELECT item.item_number AS number, item.name, item.category, item.subcategory, sale_item.description,
				sale_item.quantity_purchased AS qty, sale_item.subtotal, sale_item.total, sale_item.tax, sale_item.profit,
				sale_item.discount_percent AS discount, sale_item.line
			FROM ".$this->db->dbprefix('sales_items')." AS sale_item
			LEFT JOIN ".$this->db->dbprefix('items')." AS item
				ON item.item_id = sale_item.item_id
			WHERE sale_item.sale_id = ".(int) $sale_id.")
			UNION ALL
			(SELECT kit.item_kit_number, kit.name, kit.category, kit.subcategory, sale_kit.description,
				sale_kit.quantity_purchased, sale_kit.subtotal, sale_kit.total, sale_kit.tax, sale_kit.profit,
				sale_kit.discount_percent, sale_kit.line
			FROM ".$this->db->dbprefix('sales_item_kits')." AS sale_kit
			LEFT JOIN ".$this->db->dbprefix('item_kits')." AS kit
				ON kit.item_kit_id = sale_kit.item_kit_id
			WHERE sale_kit.sale_id = ".(int) $sale_id.")
			UNION ALL
			(SELECT invoice.invoice_number, invoice.name, invoice.category, invoice.subcategory, sale_invoice.description,
				sale_invoice.quantity_purchased, sale_invoice.subtotal, sale_invoice.total, sale_invoice.tax, sale_invoice.profit,
				sale_invoice.discount_percent, sale_invoice.line
			FROM ".$this->db->dbprefix('sales_invoices')." AS sale_invoice
			LEFT JOIN ".$this->db->dbprefix('invoices')." AS invoice
				ON invoice.invoice_id = sale_invoice.invoice_id
			WHERE sale_invoice.sale_id = ".(int) $sale_id.")
			ORDER BY line ASC");

		$rows = $query->result_array();
		return $rows;
	}
    
    //update raincheck data
	function update_raincheck($reaincheck_data, $raincheck_id) {
        $success = false;
        if (!empty($raincheck_id)) {
            $this->db->where('raincheck_id', $raincheck_id);
            $success = $this->db->update('rainchecks', $reaincheck_data);
        }
        return $success;
    }

}
?>

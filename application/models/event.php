<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Model
{
	public function get_people($teetime_id){
		$this->db->select('p.person_id, p.first_name, p.last_name, p.email,
			p.phone_number, ep.paid, p.birthday, ep.checked_in');
		$this->db->from('event_people AS ep');
		$this->db->join('people AS p', 'p.person_id = ep.person_id', 'inner');
		$this->db->where('ep.teetime_id', $teetime_id);
		$this->db->order_by('p.last_name, p.first_name');
		$query = $this->db->get();

		$rows = $query->result_array();
		return $rows;
	}

	public function save_people($teetime_id, $people_array){

		if(empty($teetime_id) || empty($people_array)){
			return false;
		}

		$data = array();
		foreach($people_array as $person_id){
			$data[] = array('person_id' => $person_id, 'teetime_id' => $teetime_id);
		}

		$this->db->insert_batch('event_people', $data);
		return true;
	}

	public function save_person($teetime_id, $person_id){
		$success = false;
		if(!empty($teetime_id) && !empty($person_id)){
			// Check if person is already part of event
			$query = $this->db->get_where('event_people', array('teetime_id'=>$teetime_id, 'person_id'=>$person_id));
			if($query->num_rows() > 0){
				return false;
			}

			$success = $this->db->insert('event_people', array('teetime_id'=>$teetime_id, 'person_id'=>$person_id));
		}

		return $success;
	}

	public function person_checkin($teetime_id, $person_id, $checked_in = true){
		$checked_in = (bool) $checked_in;

		if(!empty($checked_in)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}

		$success = $this->db->update('event_people', array('checked_in' => $checked_in, 'date_checked_in'=>$date), array(
			'teetime_id'=>$teetime_id,
			'person_id'=>$person_id
		));
		
		$teetime_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE foreup_teetime SET paid_player_count = paid_player_count + 1 WHERE TTID LIKE '{$teetime_id}%' LIMIT 2");
		return $success;
	}

	public function person_pay($teetime_id, $person_id, $paid = true){
		
		$paid = (int) $paid;

		if(!empty($paid)){
			$date = date('Y-m-d H:i:s');
		}else{
			$date = null;
		}

		$success = $this->db->update('event_people', array('paid' => $paid, 'date_paid'=>$date), array(
			'teetime_id'=>$teetime_id,
			'person_id'=>$person_id
		));
		return $success;
	}

	public function delete_person($teetime_id, $person_id){
		$success = $this->db->delete('event_people', array('teetime_id'=>$teetime_id, 'person_id'=>(int) $person_id));
		return $success;
	}

	public function delete_people($teetime_id){
		$success = $this->db->delete('event_people', array('teetime_id'=>$teetime_id));
		return $success;
	}
}

<?php
class Customer_billing extends CI_Model
{
	function __construct(){
		$this->load->model('Item_taxes');
		$this->load->model('Item_kit_taxes');
		$this->load->model('Account_transactions');
		$this->load->library('Sale_lib');
	}

	/*
	Determines if a given item_id is an item
	*/
	function exists($billing_id)
	{
		$this->db->from('customer_billing');
		$this->db->where("billing_id", $billing_id);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($customer_id = false, $limit=10000, $offset=0, $join = false)
	{
		$this->db->from('customer_billing');
		//$this->db->order_by("name", "asc");
		$this->db->where('course_id', $this->session->userdata('course_id'));
		if ($customer_id){
			$this->db->where('customer_billing.person_id', $customer_id);
		}

		if ($join)
		{
			$this->db->join('people', 'people.person_id = customer_billing.person_id');
			$this->db->order_by('last_name, first_name');
		}

		$this->db->where('customer_billing.deleted !=', 1);
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	/*
	Returns all billings in a time range
	*/
	function get_all_in_range($start_date, $end_date, $limit = 10000, $offset = 0)
	{
		$month = date('n', strtotime($start_date));
		$day = date('j');
		$today = date('Y-m-d');
		$start_date = date('Y-m-d',strtotime($start_date));
		$start_date_day = date('j',strtotime($start_date));
		$end_date_day = date('j',strtotime($end_date));

		return $this->db->query("SELECT
					token,
					cardholder_name,
					billing_id,
					`foreup_customer_billing`.person_id AS person_id,
					pay_account_balance,
					pay_member_balance,
					email_invoice,
					foreup_customer_billing.course_id AS course_id,
					foreup_customer_billing.credit_card_id AS credit_card_id
					FROM (`foreup_customer_billing`)
					LEFT OUTER JOIN `foreup_customer_credit_cards` ON `foreup_customer_billing`.`credit_card_id` = `foreup_customer_credit_cards`.`credit_card_id`
					LEFT OUTER JOIN `foreup_customers` ON `foreup_customers`.`person_id` = `foreup_customer_billing`.`person_id`
					WHERE `foreup_customer_billing`.`deleted` != 1
					AND foreup_customer_billing.course_id = {$this->session->userdata('course_id')}
					AND ((frequency = 'monthly' AND day >= $start_date_day AND day <= $end_date_day) OR (frequency = 'yearly' AND day >= $start_date_day AND day <= $end_date_day AND month = $month))
					AND `foreup_customers`.`deleted` = 0
					AND `start_date` <= '$start_date'
					AND ((start_month <= end_month AND start_month <= $month AND end_month >= $month) OR (start_month > end_month AND (start_month <= $month OR end_month >= $month)))
					ORDER BY `foreup_customer_billing`.`credit_card_id` LIMIT $limit OFFSET $offset");
	}
	function count_all()
	{
		$this->db->from('customer_billing');
		return $this->db->count_all_results();
	}
	function get_course_payments($course_id)
	{
		$this->db->from('billing_charge_items');
		$this->db->join('billing_charges', 'billing_charge_items.charge_id = billing_charges.charge_id');
		$this->db->join('billing_credit_cards', 'billing_credit_cards.credit_card_id = billing_charges.credit_card_id');
		$this->db->where('billing_credit_cards.course_id', $course_id);
		$this->db->order_by('date DESC, billing_charge_items.charge_id');
		//$result = $this->db->get();
		//echo $this->db->last_query();
		return $this->db->get()->result_array();
	}

	/*
	Gets information about a particular item
	*/
	function get_info($billing_id)
	{
		$this->db->from('customer_billing');
		$this->db->where("billing_id", $billing_id);

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->result_array();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj = new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('customer_billing');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}
			return (array) $item_obj;
		}
	}

	/*
	Inserts or updates an customer bill
	*/
	function save($billing_data, $billing_id = false)
	{
		if(isset($billing_data['items'])){
			$items = $billing_data['items'];
			unset($billing_data['items']);
		}

		if(!empty($billing_data['start_date'])){
			$billing_data['start_date'] = date('Y-m-d', strtotime($billing_data['start_date']));
		}

		if(!empty($billing_data['frequency_on_date'])){
			$billing_data['frequency_on_date'] = date('Y-m-d', strtotime($billing_data['frequency_on_date']));
		}

		if(!empty($billing_data['end_date'])){
			if($billing_data['end_date'] != '0000-00-00'){
				$billing_data['end_date'] = date('Y-m-d', strtotime($billing_data['end_date']));
			}
		}

		if($billing_data['frequency_on'] != 'date'){
			$billing_data['frequency_on_date'] = '0000-00-00';
		}

		if(empty($billing_data['end_date'])){
			$billing_data['end_date'] = '0000-00-00';
		}

		if(empty($billing_data['credit_card_id'])){
			$billing_data['credit_card_id'] = 0;
		}
		$billing_data['credit_card_id'] = (int) $billing_data['credit_card_id'];

		// If no billing ID was passed, or the bill doesn't exist, create it
		if (empty($billing_id) or !$this->exists($billing_id))
		{
			if($this->db->insert('customer_billing', $billing_data))
			{
				$billing_id = $this->db->insert_id();

				if(isset($items)){
					$invoice_total = $this->save_items($items, $billing_id);
					$this->db->update('customer_billing', array('total' => $invoice_total), array('billing_id' => $billing_id));
				}
			}

		// If a billing ID was passed, update it
		}else{
			$this->db->where('billing_id', $billing_id);
			$response = $this->db->update('customer_billing', $billing_data);

			if(isset($items)){
				$invoice_total = $this->save_items($items, $billing_id);
				$this->db->update('customer_billing', array('total' => $invoice_total), array('billing_id' => $billing_id));
			}
		}

		return $billing_id;
	}

	function save_items($items, $billing_id)
	{
		if(!is_array($items)){
			return false;
		}
		$billing_items = array();
		$invoice_total = 0;

		// Loop through line items and calculate their totals
		if(!empty($items)){

			// Reset order of lines so there are no gaps in line numbers
			$items = array_values($items);

			foreach($items as $line => $item){
				$line = $line + 1;
				$billing_item = array(
					'billing_id' => (int) $billing_id,
					'line' => (int) $line,
					'item_id' => (int) $item['item_id'],
					'item_type' => $item['item_type'],
					'description' => $item['description'],
					'quantity' => (float) $item['quantity'],
					'price' => (float) $item['price'],
					'tax_percentage' => (float) $item['tax_percentage']
				);

				$line_total = 0;
				$subtotal = (float) ($item['quantity'] * $item['price']);

				if($item['item_type'] == 'member_balance' || $item['item_type'] == 'customer_balance'){
					$tax = 0;
					$subtotal = 0;
					$item['quantity'] = 1;

				}else if(!empty($item['item_id']) && $item['item_type'] == 'item'){
					$taxes = $this->Item_taxes->get_info($item['item_id']);
					$tax = (float) $this->sale_lib->calculate_tax($subtotal, $taxes);

				// If line is an item kit
				}else if(!empty($item['item_id']) && $item['item_type'] == 'item_kit'){
					$taxes = $this->Item_taxes->get_info($item['item_id']);
					$tax = (float) $this->sale_lib->calculate_tax($subtotal, $taxes);

				// If line is just manually entered
				}else{
					$tax = round($subtotal * ($item['tax_percentage'] / 100), 2);
				}

				// Add line total to overall invoice total
				$line_total = $subtotal + $tax;
				$invoice_total += $line_total;

				$billing_items[] = $billing_item;
			}
		}

		// Remove current items attached
		$this->db->where('billing_id', $billing_id);
		$response = $this->db->delete('customer_billing_items');

		// Save new items to bill
		if(!empty($billing_items)){
			$response = $this->db->insert_batch('customer_billing_items', $billing_items);
		}

		return $invoice_total;
	}

	// Retrieve any items associated with bill
	function get_items($billing_id){
		$this->db->from('customer_billing_items');
		$this->db->where('billing_id', $billing_id);
		$this->db->order_by('line ASC');
		$result = $this->db->get()->result_array();

		return $result;
	}

	// Retrieve any items associated with bill and calculate tax
	// and totals of each line
	function get_items_detailed($billing_id)
	{

		$this->db->from('customer_billing_items');
		$this->db->where('billing_id', $billing_id);
		$this->db->order_by('line ASC');
		$result = $this->db->get()->result_array();

		// Loop through attached billing items and calculate totals
		foreach($result as $key => $row){
			$subtotal = $row['price'] * $row['quantity'];
			$result[$key]['subtotal'] = $subtotal;

			// If line is to pay off member account balance
			if($row['item_type'] == 'member_balance' || $row['item_type'] == 'customer_balance'){

				$balance = 0;
				$tax = 0;
				$subtotal = (float) $balance;
				$result[$key]['price'] = (float) $balance;
				$result[$key]['quantity'] = 1;
				$result[$key]['subtotal'] = (float) $balance;
				$result[$key]['tax'] = 0;
				$result[$key]['total'] = (float) $balance;

			// If line is to pay off customer account balance
			}else if(!empty($row['item_id']) && $row['item_type'] == 'item'){
				$taxes = $this->Item_taxes->get_info($row['item_id']);
				$result[$key]['taxes'] = $taxes;
				$tax = (float) $this->sale_lib->calculate_tax($subtotal, $taxes);

			// If line is an item kit
			}else if(!empty($row['item_id']) && $row['item_type'] == 'item_kit'){
				$taxes = $this->Item_taxes->get_info($row['item_id']);
				$result[$key]['taxes'] = $taxes;
				$tax = (float) $this->sale_lib->calculate_tax($subtotal, $taxes);

			// If line is just manually entered
			}else{
				$tax = round($subtotal * ($row['tax_percentage'] / 100), 2);
			}

			$result[$key]['tax'] = (float) $tax;
			$result[$key]['total'] = $subtotal + $tax;
		}

		return $result;
	}

	function get_details($billing_id, $item_details = true){

		$this->db->select("bill.*, DATE_FORMAT(bill.start_date, '%m/%d/%Y') AS start_date,
			DATE_FORMAT(bill.end_date, '%m/%d/%Y') AS end_date, DATE_FORMAT(bill.frequency_on_date, '%m/%d/%Y') AS frequency_on_date,
			MAX(last_invoice.date) AS last_invoice_date, MAX(last_invoice.bill_end) AS last_invoice_bill_end,
			MAX(last_invoice.bill_start) AS last_invoice_bill_start", false);
		$this->db->from('customer_billing AS bill');
		$this->db->join('invoices AS last_invoice', 'last_invoice.billing_id = bill.billing_id AND last_invoice.deleted = 0', 'left');
		$this->db->where('bill.billing_id', $billing_id);
		$this->db->group_by('bill.billing_id');

		$data = $this->db->get()->row_array();

		if(empty($data['frequency'])){
			$data['frequency'] = 1;
		}

		if(empty($data['frequency_period'])){
			$data['frequency_period'] = 'month';
		}

		if(empty($data['frequency_on'])){
			$data['frequency_on'] = 'begin';
		}

		if($data['frequency_on_date'] == '00/00/0000'){
			$data['frequency_on_date'] = false;
		}

		if(!empty($data['person_id'])){
			$data['person_info'] = $this->Customer->get_info($data['person_id']);
		}
		$data['pay_member_balance'] = 0;
		$data['pay_account_balance'] = 0;

		if(empty($data['start_date']) || $data['start_date'] == '00/00/0000'){
			$data['start_date'] = date('m/d/Y');
		}
		if(empty($data['end_date']) || $data['end_date'] == '00/00/0000'){
			$data['end_date'] = null;
		}

		if($item_details){
			$items = $this->get_items_detailed($billing_id);
			$data['current_items'] = $items;

			$bill_subtotal = 0;
			$bill_tax = 0;
			$bill_total = $data['total'];
			foreach($data['current_items'] as $item){
				$bill_subtotal += $item['subtotal'];
				$bill_tax += $item['tax'];

				if($item['item_type'] == 'member_balance'){
					$data['pay_member_balance'] = 1;
				}
				if($item['item_type'] == 'customer_balance'){
					$data['pay_account_balance'] = 1;
				}
			}

			$data['current_subtotal'] = $bill_subtotal;
			$data['current_tax'] = $bill_tax;
			$data['current_total'] = $bill_total;

			$data['customer_current_charges'] = $data['total'];
			$data['customer_overdue_charges'] = 0;
			$data['customer_total_due'] = $data['total'];

		}else{
			$data['current_items'] = $this->get_items($billing_id);

			foreach($data['current_items'] as $item){
				if($item['item_type'] == 'member_balance'){
					$data['pay_member_balance'] = 1;
				}
				if($item['item_type'] == 'customer_balance'){
					$data['pay_account_balance'] = 1;
				}
			}
		}

		return $data;
	}

	/*
	Deletes one item
	*/
	function delete($billing_id)
	{
		$this->db->where('billing_id',$billing_id);
		return $this->db->update('customer_billing', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('billing_id',$item_ids);
		return $this->db->update('billing', array('deleted' => 1));
 	}

	function mark_invoice_generation_as_started($billing_id)
	{
		$data = array(
			'last_invoice_generation_attempt'=>date('Y-m-d 00:00:00')
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}

	function mark_as_started($billing_id)
	{
		$data = array(
			'last_billing_attempt'=>date('Y-m-d 00:00:00'),
			'started'=>1,
			'charged'=>0,
			'emailed'=>0
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}

	function mark_as_charged($billing_id)
	{
		$data = array(
			'charged'=>1
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}

	function mark_as_emailed($billing_id)
	{
		$data = array(
			'emailed'=>1
		);
		$this->db->where('billing_id', $billing_id);
		$this->db->update('foreup_customer_billing', $data);
	}

	function send_summary($email, $name, $billing_list, $headers)
	{
		$data = array();
		$data['billing_list'] = $billing_list;
		$data['headers'] = $headers;
		$data['name'] = $name;

		send_sendgrid(
			$email,
			"Customer Billing Summary ".date("Y-m-d"),
			$this->load->view('customers/billing_summary', $data, true),
			'no-reply@foreup.com',
		 	'ForeUP Auto Billing'
		);
	}

	function get_billing_products($course_id, $credit_card_id)
	{
		$month = date('n');
		$day = date('j');
		$today = date('Y-m-d');


		$this->db->from('billing');
		//$this->db->join('billing_credit_cards', 'billing.credit_card_id = billing_credit_cards.credit_card_id');
		$this->db->where('billing.deleted !=', 1);
		$this->db->where('start_date <=', $today);
		$this->db->where("(credit_card_id = $credit_card_id AND ((monthly = 1 AND monthly_day = $day) OR (annual = 1 AND annual_day = $day AND annual_month = $month)) OR free = 1)");
		$this->db->order_by('billing.credit_card_id');
		$this->db->where("((period_start <= period_end AND period_start <= $month AND period_end >= $month) OR (period_start > period_end AND (period_start <= $month OR period_end > $month)))");
		$this->db->where('course_id', $course_id);
//		$this->db->where("(free = 1 OR credit_card_id = $credit_card_id)");
		return $this->db->get();
	}

	// Retrieves any recurring invoices that need generated for a specific date
	function get_invoices_to_generate($date = null){

		// Default to today
		if(empty($date)){
			$date = date('Y-m-d');
		}else{
			$date = date('Y-m-d', strtotime($date));
		}

		$dayOfWeek = (int) date('N', strtotime($date));
		$dayOfMonth = (int) date('j', strtotime($date));
		$monthDays = (int) date('t', strtotime($date));
		$month = (int) date('n', strtotime($date));

		$filters = array();

		// Always pull daily invoices
		$filters[] = "(b.frequency_period = 'day')";

		// If billing set for this day of week
		$filters[] = "(b.frequency_period = 'week' AND b.frequency_on = {$dayOfWeek})";

		// If billing set for this date of month
		$filters[] = "(b.frequency_period = 'month'
			AND b.frequency_on = 'date' AND DAY(b.frequency_on_date) = {$dayOfMonth})";

		// If billing set for this date of the year
		$filters[] = "(b.frequency_period = 'year' AND b.frequency_on = 'date'
			AND DAY(b.frequency_on_date) = {$dayOfMonth} AND MONTH(b.frequency_on_date) = {$month})";

		// If end of month
		if($dayOfMonth == $monthDays){
			$filters[] = "(b.frequency_period = 'month' AND b.frequency_on = 'end')";
		}
		// If beginning of month
		if($dayOfMonth == 1){
			$filters[] = "(b.frequency_period = 'month' AND b.frequency_on = 'begin')";
		}
		// If beginning of year
		if($dayOfMonth == 1 && $month == 1){
			$filters[] = "(b.frequency_period = 'year' AND b.frequency_on = 'begin')";
		}
		// If end of year
		if($dayOfMonth == $monthDays && $month == 12){
			$filters[] = "(b.frequency_period = 'year' AND b.frequency_on = 'end')";
		}
		$filterSql = implode(' OR ', $filters);

		$date_clean = $this->db->escape($date);
		// $query = $this->db->query("SELECT b.billing_id, b.course_id,
				// b.frequency, b.frequency_period, b.frequency_on, 
				// b.frequency_on_date,
				// (
					// SELECT DATE(MAX(last.date)) 
					// FROM foreup_invoices AS last 
					// WHERE last.deleted = 0 AND last.billing_id = b.billing_id
				// ) AS last_invoice_date			
			// FROM foreup_customer_billing AS b
			// WHERE b.deleted = 0
				// AND (b.start_date <= {$date_clean}
					// AND (b.end_date = '0000-00-00' OR b.end_date >= {$date_clean}))
				// AND ({$filterSql})
			// GROUP BY b.billing_id
			// HAVING last_invoice_date <= {$date_clean}");
        $query = $this->db->query("SELECT b.billing_id, b.course_id,
	            DATE(MAX(last_invoice.date)) AS last_invoice_date,
	            b.frequency, b.frequency_period, b.frequency_on,
	            b.frequency_on_date
		    FROM foreup_customer_billing AS b
		    LEFT JOIN foreup_invoices AS last_invoice
	            ON last_invoice.billing_id = b.billing_id
	            AND DATE(last_invoice.date) <= {$date_clean}
	            AND last_invoice.deleted = 0
		    WHERE b.deleted = 0
        	    AND (b.start_date <= {$date_clean}
                AND (b.end_date = '0000-00-00' OR b.end_date >= {$date_clean}))
        	    AND ({$filterSql})
    	    GROUP BY b.billing_id");
		$rows = $query->result_array();

		// Loop through the rows and filter out invoices that haven't
		// passed enough time to next generation
		foreach($rows as $key => $row){

			$frequency = (int) $row['frequency'];
			$period = $row['frequency_period'];

			// If set to every period, or invoice hasn't been generated
			// before, keep it in the list
			if(empty($row['last_invoice_date']) || $row['last_invoice_date'] == '0000-00-00'){
				continue;
			}

			// Figure out how many days have passed since last generation
			$last_generation = DateTime::createFromFormat('Y#m#d', $row['last_invoice_date']);
			$curdate = DateTime::createFromFormat('Y#m#d', $date);
			$days_since_last = (int) abs($curdate->diff($last_generation, true)->format('%a'));

			$days_between = 0;
			if($period == 'week'){
				$days_between = $frequency * 7;
			}else if($period == 'month'){
				$days_between = $frequency * 28;
			}else if($period == 'year'){
				$days_between = $frequency * 365;
			}else{
				$days_between = $frequency;
			}

			// Remove invoice from list if it hasn't had enough time
			// since last generation
			if($days_since_last < $days_between){
				unset($rows[$key]);
			}
		}
		// var_dump($rows);
		// die();
		return $rows;
	}
}
?>

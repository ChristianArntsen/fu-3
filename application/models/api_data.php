<?php
class Api_data extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();	
		$this->load->model('Teetime');
		$this->load->model('Teesheet');	
		$this->load->model('teesheet');
		$this->load->model('course');
		$this->load->model('Person');
		$this->load->model('Customer');
    }
	
	function get_key_courses()
	{
		$course_ids = array();
		$api_key = $this->input->get('api_key');
		$this->db->from('api_key_permissions');
		$this->db->join('api_keys', 'api_keys.id = api_key_permissions.api_id');
		$this->db->where('key', $api_key);
		
		$results = $this->db->get();
		//echo $this->db->last_query();
		foreach ($results->result_array() as $result)
		{
			$course_ids[] = $result['course_id'];
		}
		return $course_ids;
	}
	
	function get_course_areas()
	{
		// Return a list of course areas that have active courses (and that you have permissions to)
		// And have online booking active
		$course_array = $this->get_key_courses();
		//TODO: make the above true... permissions and online booking
		$this->db->select('course_areas.area_id as area_id, label, course_areas.country AS country');
		$this->db->from('course_areas');
		$this->db->join('courses', 'course_areas.area_id = courses.area_id');
		$this->db->group_by('area_id');
		$this->db->where_in('course_id', $course_array);
		$this->db->where('active', 1);
		return $this->db->get();
	}
	
	function get_course_list($area_id)
	{
		$course_array = $this->get_key_courses();
		
		$this->db->select('course_id, name');
		$this->db->from('courses');
		$this->db->where('active', 1);
		$this->db->where('area_id', $area_id);
		$this->db->where_in('course_id', $course_array);
		$results = $this->db->get()->result_array();
		
		foreach ($results as $key => $course) {
			$results[$key]['golf_course_id'] = $course['course_id'];
			unset($results[$key]['course_id']);
		}
		
		return $results;
	}

	function get_course_info($course_id)
	{
		$this->db->select('name, address, city, state, zip, phone, online_booking_protected, min_required_players, min_required_carts, min_required_holes');
		$this->db->from('courses');
		$this->db->where('course_id', $course_id);
		$this->db->where('active', 1);
		$this->db->limit(1);
		return $this->db->get();
	}
	function get_tee_sheet_info($schedule_id)
	{
		$this->db->select('title, teesheet.increment, open_time, close_time, teesheet_id');
		$this->db->from('teesheet');
		$this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$this->db->where('teesheet_id', $schedule_id);
		$this->db->where('deleted', 0);
		$this->db->limit(1);
		$result = $this->db->get();
		return $result->row_array();
	}
	
	function get_course_links_list($course_id)
	{
		$this->db->select('teesheet_id AS schedule_id, teesheet.default AS is_default, teesheet.holes, title, online_open_time, online_close_time, days_out, days_in_booking_window, foreup_teesheet.increment, foreup_courses.booking_rules');		
		$this->db->from('teesheet');
		$this->db->where('deleted', 0);
		$this->db->where('foreup_teesheet.online_booking', 1);
		$this->db->where('foreup_teesheet.course_id', $course_id);
		$this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$results = $this->db->get();
		
		 return $results;		
	}
	
	function get_course_app_data($course_id)
	{
		//join the course and app data tables for general information
		$this->db->select('name, address, course_latitude, course_longitude,red,green,blue,alpha,home_view,gps_view,scorecard_view, teetime_view, food_and_beverage_view, last_updated');
		$this->db->from('courses');
		$this->db->where('courses.course_id', $course_id);
		$this->db->join('course_app_data', 'courses.course_id = course_app_data.course_id');
		$this->db->limit(1);
		$data = $this->db->get()->row_array(); 
		
		//get all the links for the course
		$this->db->select('link_id,name, tee_1_name, tee_2_name, tee_3_name, tee_4_name, tee_5_name, tee_6_name, tee_7_name, tee_8_name');
		$this->db->from('links');
		$this->db->where('course_id', $course_id);
		$data['links'] = $this->db->get()->result_array();
		
		//get all hole information for each link
		foreach ($data['links'] as $key => $link) {
			$this->db->select('hole_number, par, front_latitude, front_longitude, center_latitude, center_longitude, back_latitude, back_longitude, hole_latitude, hole_longitude, middle_latitude, middle_longitude, tee_1_latitude, tee_1_longitude, tee_2_latitude, tee_2_longitude, tee_3_latitude, tee_3_longitude, tee_4_latitude, tee_4_longitude, tee_5_latitude, tee_5_longitude, tee_6_latitude, tee_6_longitude, tee_7_latitude, tee_7_longitude, tee_8_latitude, tee_8_longitude');				
			$this->db->from('holes');
			$this->db->where('link_id', $link['link_id']);
			$data['links'][$key]['holes'] = $this->db->get()->result_array();
			
			//unset the link_id. It had to be included so we could get the appropriate holes based on the link idea.
			unset($data['links'][$key]['link_id']);			
		}
		
		return $data;
	}
	
	function get_customer_list($course_id, $api_id, $override = false, $offset = 0)
	{
		if (!$override)
			$this->db->select('course_id, customers.person_id as person_id, first_name, last_name');
		else
			$this->db->select('course_id, customers.person_id as person_id, first_name, last_name');
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id = people.person_id');		
		$this->db->where('course_id', $course_id);
		$this->db->limit(200);
		$this->db->offset($offset);
		if (!$override)
			$this->db->where('api_id', $api_id);
		$this->db->order_by('course_id');
		$results = $this->db->get()->result_array();
		
		foreach ($results as $key => $guest) {
			$results[$key]['guest_id'] = (int)$guest['person_id'];
			unset($results[$key]['person_id']);
			$results[$key]['golf_course_id'] = (int)$guest['course_id'];
			unset($results[$key]['course_id']);
			
		}
// 		
		return $results;
	}
	
	function get_customer_search_suggestions($search,$limit=25,$type='',$course_id,$strict=false)
	{
		$course_id = "AND foreup_customers.course_id = $course_id";
		$suggestions = array();
		
		if ($type == 'name' || $type == '') {
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');
			if ($strict)
			{
				$this->db->where("last_name = '{$search}' AND deleted=0 $course_id");
			}
			else 
			{
				$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
				last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
				CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and deleted=0 $course_id");
			}	
			$this->db->order_by("last_name", "asc");
			$this->db->limit($limit);		
			$by_name = $this->db->get();
			foreach($by_name->result() as $row)
			{
				$suggestions[]=array('guest_id'=> $row->person_id, 'last_name' => $row->last_name, 'first_name' => $row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email);		
			}
		}
		
		if (($type == 'account_number' || $type == '') && count($suggestions) < $limit) {
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");	
			if ($strict)
			{
				$this->db->where("account_number",$search);
			}
			else
			{	
				$this->db->like("account_number",$search,'after');
			}
			$this->db->order_by("account_number", "asc");		
			$this->db->limit($limit);		
			$by_account_number = $this->db->get();
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('guest_id'=> $row->person_id, 'last_name' => $row->last_name, 'first_name' => $row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email, 'account_number' => $row->account_number);		
			}
		}

			
		if (($type == 'phone') && count($suggestions) < $limit) {
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('phone_number != ""');	
			if ($strict)
			{
				$this->db->where('phone_number',$search);
			}	
			else
			{
				$this->db->like("phone_number",$search);
			}
			$this->db->order_by("phone_number", "asc");		
			$this->db->limit($limit);		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('guest_id'=> $row->person_id, 'last_name' => $row->last_name, 'first_name' => $row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email);		
			}
		}

		if ($type == 'email' && count($suggestions) < $limit) {
			$this->db->from('customers');
			$this->db->join('people','customers.person_id=people.person_id');	
			$this->db->where("deleted = 0 $course_id");
			$this->db->where('email != ""');
			if ($strict)
			{
				$this->db->where('email',$search);
			}		
			else 
			{
				$this->db->like("email",$search);
			}
			$this->db->order_by("email", "asc");		
			$this->db->limit($limit);		
			$by_account_number = $this->db->get();
			//$suggestions[] = array('sql'=>$this->db->last_query());
			foreach($by_account_number->result() as $row)
			{
				$suggestions[]=array('guest_id'=> $row->person_id, 'last_name' => $row->last_name, 'first_name' => $row->first_name, 'phone_number'=>$row->phone_number, 'email'=>$row->email);		
			}
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
	function get_reservations($customer_id, $schedule_id, $today_only = false, $title_match = false)
	{
		$today = date('Ymd0000')-1000000;
		// $this->db->where("teesheet_id", $schedule_id);
        // $this->db->where("start >= '$today'");
        // $this->db->select("TTID as reservation_id, type, STR_TO_DATE(start,'%Y%m%d%h%i'), STR_TO_DATE(end,'%Y%m%d%h%i'), player_count, holes, carts, paid_player_count, title, details, side, person_id, person_id_2, person_id_3, person_id_4, person_id_5");
        // $this->db->from('teetime');
        // if ($side != '')
            // $this->db->where('side', $side);
		// $this->db->where("status !=", 'deleted');
		// $this->db->where("(person_id = $customer_id OR person_id_2 = $customer_id OR person_id_3 = $customer_id OR person_id_4 = $customer_id OR person_id_5 = $customer_id)");
        // $this->db->order_by('end, start');
        $end_of_today_sql = '';
		$select_sql = "`TTID` as reservation_id, `type`, 
				STR_TO_DATE(start+1000000, '%Y%m%d%H%i') as start_time, STR_TO_DATE(end+1000000, '%Y%m%d%H%i') as end_time, 
				`player_count` AS guests, `holes`, `carts`, `paid_player_count` AS paid_guests, 
				`title`, `details`, `side`, 
				`person_id`, `person_id_2`, `person_id_3`, `person_id_4`, `person_id_5`";
		$match_sql = "AND (person_id = $customer_id OR person_id_2 = $customer_id OR person_id_3 = $customer_id OR person_id_4 = $customer_id OR person_id_5 = $customer_id) ";
		if ($today_only)
		{
			$end_of_today_sql = " AND `start` <= '".(date('Ymd2399')-1000000)."'";	
			$select_sql = "`TTID` as reservation_id, 
				STR_TO_DATE(start+1000000, '%Y%m%d%H%i') as start_time, 
				`player_count` AS guests, `paid_player_count` AS paid_guests, `holes`, `carts`,  
				`title`, `side`";
			if ($title_match)
				$match_sql = " AND `title` LIKE '%$title_match%'";				
		}
		
		$teetimes = $this->db->query("
			SELECT 
				$select_sql 
			FROM (`foreup_teetime`) 
			WHERE `teesheet_id` = '$schedule_id' 
				AND `start` >= '$today' 
				$end_of_today_sql
				$match_sql
				AND `status` != 'deleted' 				
			ORDER BY `start`");
        //$teetimes = $this->db->get();//createAssocArray($this->db->executeSQL($sql));
        
        return $teetimes->result_array();
	}
	
	
	function get_customer_info($person_id, $api_id, $course_id, $override = false, $include_price_class = false)
	{
		$price_class = $include_price_class ? 'price_class,' : '';
		
		$this->db->select("course_id, customers.person_id as person_id, first_name, last_name, phone_number, email, {$price_class} address_1 as address, city, state, zip, country");
		$this->db->from('customers');
		$this->db->join('people', 'customers.person_id = people.person_id');
		if (!$override)
			$this->db->where('api_id', $api_id);
		$this->db->where('course_id', $course_id);
		$this->db->where('people.person_id', $person_id);
		$this->db->order_by('course_id');
		$this->db->limit(1);
		return $this->db->get();
	}
	function customer_exists($person_id, $course_id, $api_id, $override = false)
	{
		$this->db->from('customers');
		$this->db->where('person_id', $person_id);
		$this->db->where('course_id', $course_id);
		if (!$override)
			$this->db->where('api_id', $api_id);
		$this->db->where('deleted', 0);
		$this->db->limit(1);
		
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	function save_customer_info(&$person_data, &$customer_data, $person_id = false, $api_id = false, $override = false)
	{
		// $customer_info = $this->Customer->get_info_by_email($person_data['email']);
		// if ($customer_info) {
			// //customer already exists			
			// $customer_data = $customer_info[0];
			// $customer_data['person_id'] = (int)$customer_data['person_id'];			
			// return true;
		// }
		return $this->Customer->save($person_data,$customer_data, $person_id);
	}
	function save_customer($customer_data)
	{
		$this->db->trans_start();
		
		$this->db->insert('customers',$customer_data);
				
		$group_ids = array();
		$label = 'ForeUP Online Booking';
		$gid = $this->Customer->get_group_id($label, $customer_data['course_id']);			
		$group_ids[] = $gid;		
		$groups_data = $group_ids;
						
		if (count($groups_data) != 0) {
			$this->Customer->save_group_memberships($groups_data, $customer_data['person_id']);
		}
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
	
	function get_booked_teetimes_list($course_id = false, $schedule_id = false, $date = false, $api_id, $override = 0)
	{
		//calculate tomorrows date from $date
		$tomorrow = strtotime ( '+1 day' , strtotime ( $date ) ) ;
		$tomorrow = date ( 'Y-m-j' , $tomorrow );

		if (!$override)	
			$this->db->select('TTID as reservation_id, start as start_time, player_count as guests' );
		else
			$this->db->select("`TTID` as reservation_id, `type`, 
				`start` as start_time, `end` as end_time, 
				`player_count` as guests, `holes`, `carts`, `paid_player_count` as paid_guests, 
				`title`, `details`, `side`, 
				`person_id`, `person_id_2`, `person_id_3`, `person_id_4`, `person_id_5` ");
		$this->db->from('teetime');				
		$this->db->where('teesheet_id', $schedule_id);					
		$this->db->where('start >=', (date('Ymd', strtotime($date))-100)."0000");
		$this->db->where('start <', (date('Ymd', strtotime($tomorrow))-100)."0000");
		$this->db->where('status !=', 'deleted');
		//$this->db->order_by('side, start, end');
		if (!$override)
			$this->db->where('api_id', $api_id);
		$results = $this->db->get();
		
		return $results;		
	}
	function course_permissions($course_id = false, $schedule_id = false, $api_id)
	{
		if ($course_id || $schedule_id)
		{
			$this->db->select('extended_permissions, sales, tee_time_sales');
			$this->db->from('api_key_permissions');
			if ($schedule_id)
			{
				$this->db->join('teesheet', 'teesheet.course_id = api_key_permissions.course_id');
				$this->db->where('teesheet_id', $schedule_id);
			}
			else if ($course_id)
			{
				$this->db->where('course_id', $course_id);
			}
			$this->db->where('api_id', $api_id);
			$this->db->limit(1);
		
			return $this->db->get()->row_array();
		}
		return array();
	}
	function tee_time_is_available($course_id, $tee_sheet_id, $time, $player_count, $holes = 18)
	{				
		$teetime_data = array(
			'course_id'=>$course_id,
			'teesheet_id'=> $tee_sheet_id,
			'start'=>$this->to_db_time_string($time),
			'player_count'=>$player_count,
			'side'=>'front'
		);		
		
		$back_available = true;
		if ($holes == 18)
		{
			$this->db->from('teesheet');
			$this->db->where('teesheet_id', $tee_sheet_id);
			$teesheet = $this->db->get()->row_array();
			
			$frontnine = $teesheet['frontnine'];
			$dif = fmod((floor($frontnine/100)*60+$frontnine%100), $teesheet['increment']);
			if ($dif > 0)
				$frontnine += $teesheet['increment'] - $dif;
			$min = (floor($frontnine/100)*60+$frontnine%100);
			
			$side = $teesheet['holes'] == 9 ? 'front' : 'back';
			$teetime_data_back = array(
				'course_id'=>$course_id,
				'teesheet_id'=> $tee_sheet_id,
				'start'=>$this->to_db_time_string(date('Y-m-d H:i:s', strtotime($time." + $min minutes"))),
				'player_count'=>$player_count,
				'side'=>$side
			);
			$back_available =  $this->Teetime->check_availability($teetime_data_back);
		}				
		
		return $back_available && $this->Teetime->check_availability($teetime_data);
	}
	
	function cancel_reservation($TTID, $api_id)
	{		
		$this->db->where("(`TTID` = '$TTID' OR `TTID` = '{$TTID}b')");
		$this->db->where('api_id', $api_id);
		$this->db->from('teetime');
		$this->db->limit(2);		
	    $this->db->update('teetime', array('status' => 'deleted', 'date_cancelled' => date('Y-m-d H:i:s')));
		return $this->db->affected_rows() > 0;
	}
	function get_tee_time_index($course_info, $holes)
	{
		// DETERMINE IF WE'RE IN THE WEEKEND
		$weekend = false;
		$dow = date('w');
		if (($dow == 5 && $course_info->weekend_fri)|| ($dow == 6 && $course_info->weekend_sat) || ($dow == 0 && $course_info->weekend_sun))
	        $weekend = true;
	    if ($holes == 9)
		{
			if (!$weekend)
			{
				$cart_type_index = 1;
				$teetime_type_index = 3; 	
			}
			else 
			{
				$cart_type_index = 2;
				$teetime_type_index = 4; 	
			}
		}
		else 
		{
			if (!$weekend)
			{
				$cart_type_index = 5;
				$teetime_type_index = 7; 	
			}
			else 
			{
				$cart_type_index = 6;
				$teetime_type_index = 8; 	
			}
		}
		return array('gf'=>$teetime_type_index, 'cart'=>$cart_type_index);
	}
	function get_tee_time_prices($schedule_id, $price_category_index = false)
	{
		// DETERMINE WEEKEND
		$prices = '';
		if (!$price_category_index)
		{
			$schedule_info = $this->Teesheet->get_info($schedule_id);
			$price_category_index = 'price_category_'.($schedule_info->default_price_class ? $schedule_info->default_price_class : '1');
		}
		$prices = $this->Green_fee->get_info('', $schedule_id, $price_category_index);
        
        return $prices;
        
	}
	function get_tee_time_taxes($course_id = false)
	{
		if ($course_id)
		{
			$this->load->model('item');
			return $this->item->get_teetime_tax_rate($course_id);
		}
		return array();
	}
	function get_cart_taxes($course_id = false)
	{
		if ($course_id)
		{
			$this->load->model('item');
			return $this->item->get_cart_tax_rate($course_id);
		}
		return array();
	}
	
	function get_tee_time_info($TTID, $api_id, $schedule_id, $override = false)
	{
		$this->db->from('teetime');
		$this->db->where('TTID', $TTID);
		$this->db->where('teetime.teesheet_id', $schedule_id);
		if (!$override)
			$this->db->where('api_id', $api_id);
		$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
		$this->db->join('courses', 'teesheet.course_id = courses.course_id');
		$results = $this->db->get()->row_array();
//		echo ($override?'or':'nor');	
	//	echo $this->db->last_query();		
		
		$data = array();
		if ($results) 
		{
			$data = array(
			'start_time'=>$this->Teesheet->format_time_string($results['start']),
			'available_spots'=>$this->MAX_PLAYERS_FOR_TEETIME() - $results['player_count'],	
			'schedule_id'=>$results['teesheet_id'],
			'golf_course_id'=>$results['course_id'],
			'golf_course_name'=>$results['name'],
			'golf_course_address'=>$results['address'],
			'golf_course_city'=>$results['city'],
			'golf_course_state'=>$results['state'],
			'golf_course_zip'=>$results['zip'],
			'golf_course_country'=>$results['country']
			);
		}
		
		return $data;
	}
	
	function join_reservation(&$person_data, &$customer_data, $tee_time_id)
	{
		
		//use 4 as the standard for player count
		$this->db->select('teesheet.course_id, teetime.player_count, teetime.person_id, teetime.person_id_2, teetime.person_id_3, teetime.person_id_4, teetime.person_id_5');
		$this->db->from('teetime');
		$this->db->where('TTID', $tee_time_id);
		$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
		$results = $this->db->get()->row_array();
		$player_count = $results['player_count'];
		// echo $this->db->last_query();
		//get all the customers that are associated with the teetime
		$players = $this->player_names($results);		
		
		$insertIndex = $player_count + 1; //add one because its not zero based in the db

		if ($player_count < 4) {			
			$customer_data['course_id'] = $results['course_id'];
			//create the person and get the person_id
			$response['success'] = $this->Api_data->save_customer_info($person_data, $customer_data);					
			if ($response['success']) {
				//add the new customer to the list of players
				
				$players[] = array( 
					'name'=>$person_data['first_name']." ".$person_data['last_name'],
					'person_id'=>$person_data['person_id']
				);	
				$response['players'] = $players;			
				//create the row with data to update				
				$row["person_id_{$insertIndex}"] = $customer_data['person_id'];
				$row['player_count'] = $player_count + 1;
				$this->db->where('TTID',$tee_time_id);						
				$this->db->from('teetime');					
			    $this->db->update('teetime', $row);												
			}
			else 
			{
				$response['message'] = "The customer info could not be saved";
			}
		}
		else
		{
			$response['message'] = "All spots for tee time are booked";	
		}
		
		return $response;
	}

	function tee_time_details($TTID, $api_key, $basic = false)
	{
		//format_time_string using the teesheet model
		if (!$basic)
		{
			$this->db->select('teesheet.course_id, teetime.start, teetime.player_count, teetime.person_id, teetime.person_id_2, teetime.person_id_3, teetime.person_id_4, courses.name, courses.allow_friends_to_invite, courses.payment_required');
		}
		else 
		{
			$this->db->select('title, holes, carts, start, player_count, paid_player_count, paid_carts, person_id, person_paid_1, cart_paid_1, person_id_2, person_paid_2, cart_paid_2, person_id_3, person_paid_3, cart_paid_3, person_id_4, person_paid_4, cart_paid_4, person_id_5, person_paid_5, cart_paid_5');
		}
		$this->db->from('teetime');
		$this->db->where('TTID', $TTID);
		if (!$basic)
		{
			$this->db->join('teesheet', 'teesheet.teesheet_id = teetime.teesheet_id');
			$this->db->join('courses', 'teesheet.course_id = courses.course_id');
		}
		$results = $this->db->get()->row_array();			
		
		$data = array();
		if ($results) 
		{
			if($basic)
			{
				$results['start'] = $this->Teesheet->format_time_string($results['start']);
				return $results;
			}

			$data = array(
				'start_time'=>$this->Teesheet->format_time_string($results['start']),
				'course_name'=>$results['name'],
				'course_id'=>$results['course_id'],
				'available_spots'=>$this->MAX_PLAYERS_FOR_TEETIME() - $results['player_count'],
				'players'=>$this->player_names($results),
				'payment_url'=> null,
				'booking_url'=>base_url().'index.php/be/reservation/'.$results['course_id'],
				'allow_friends_to_invite'=>$results['allow_friends_to_invite'],
				'payment_required'=>$results['payment_required']		
			);
		}
				
		return $data;		
	}

	function available_tee_times($start_time, $end_time, $teesheet_id, $player_count, $max_price, $extended_permissions = false, $holes = false, $available_for_sale = false)
	{			
		$this->db->from('teesheet');
		$this->db->where('teesheet_id', $teesheet_id);
		$teesheet = $this->db->get()->row_array();
		
		$time_range = 'full_day';                               	
        $is_api_request = true;
        		
		$this->teesheet->load_booking_settings($teesheet['course_id'], $teesheet_id, $extended_permissions); // sets timezone
		
		$days_to_future_date = $this->days_between_dates(date('Y-m-d'), $start_time);
		$start_time = $this->to_db_time_string($start_time);
		$end_time = $this->to_db_time_string($end_time);
        $holes = ($holes && ($holes == 9 || $holes = 18)) ? $holes :18;
        $this->teesheet->apply_booking_filters($holes, $player_count, $time_range, '', '', '', '');
		
        return $this->teesheet->get_available_teetimes($is_api_request, $teesheet_id, $days_to_future_date, $max_price, $start_time, $end_time, '', 'html', $available_for_sale);
	}

	// function booked_tee_times($start_time, $end_time, $teesheet_id, $player_count, $max_price)
	// {			
		// $this->db->from('teesheet');
		// $this->db->where('teesheet_id', $teesheet_id);
		// $teesheet = $this->db->get()->row_array();
// 		
		// $time_range = 'full_day';                               	
        // $is_api_request = true;
//         		
		// //$this->teesheet->load_booking_settings($teesheet['course_id'], $teesheet_id); // sets timezone
// 		
		// //$days_to_future_date = $this->days_between_dates(date('Y-m-d'), $start_time);
		// //$start_time = $this->to_db_time_string($start_time);
		// //$end_time = $this->to_db_time_string($end_time);	
// 					
		// //$this->teesheet->apply_booking_filters($teesheet['holes'], $player_count, $time_range, '', '', '', '');		      
		// $booked_times = $this->teesheet->get_trimmed_teetimes_2($start_time, $end_time, 'front', $teesheet_id);
        // return $booked_times;
	// }

	/*
	 * converts the open time to total minutes, the start_time to total minutes and then mods the difference with the teesheet increment
	 */
	function is_valid_teetime($course_info, $start_time)
	{
		$open_time_minutes = ((int)substr($course_info->open_time, 0,2) *60) + (int)substr($course_info->open_time, 2,2);
		$start_time_minutes = ((int)date('H', strtotime($start_time)) * 60) + (int)date('i', strtotime($start_time));
		$remainder = ($start_time_minutes - $open_time_minutes) % (int)$teesheet_info->increment;
		return $remainder == 0;
	}
	
	function book_teetime($teesheet_id, $start, $person_id, $player_count, $api_id, $holes, $cart_count, $api_name, &$response, $notes, $prepaid, $is_foreup_app = false)
	{		
		$status = array('success'=>false);
		$teesheet_info = $this->Teesheet->get_info($teesheet_id);
		$person_info = $this->Person->get_info($person_id);
		$course_info = $this->Course->get_info($teesheet_info->course_id);
		date_default_timezone_set($course_info->timezone);
		$holes = $holes != 18 && $holes != 9 ? $teesheet_info->holes : $holes;
		$holes = ($holes % 9 == 0) ? $holes : 9;
		$cart_count = $cart_count ? $cart_count : 0;
		$start = $this->to_db_time_string($start);
		$end  = $start + $teesheet_info->increment;
        $and_paid = $prepaid ? ' and paid for' : '';
        if ($end%100 > 59)
            $end += 40;	

		$event_data = array(
			'teesheet_id'=>$teesheet_id,
			'start'=>$start,
            'start_datetime'=>date("Y-m-d H:i:s", strtotime($start + 1000000)),
			'end'=>$end,
            'end_datetime'=>date("Y-m-d H:i:s", strtotime((int)$end + 1000000)),
			'side'=>'front',
			'allDay'=>'false',
			'holes'=>$holes,
			'player_count'=>$player_count,
			'paid_player_count'=>$prepaid ? $player_count : 0,
			'paid_carts'=>$prepaid ? $cart_count : 0,
			'type'=>'teetime',
			'carts'=>$cart_count,
			'person_id'=>$person_id,
			'person_name'=>$person_info->last_name.', '.$person_info->first_name,
			'title'=>$person_info->last_name,
			'booking_source'=>'online',
			'booker_id'=>$person_id,
			'details'=>"Reserved{$and_paid} through $api_name @ ".date('g:ia n/j T').' - '.$notes,
			'api_id'=>$api_id,
			'booking_source'=>'api'	
		);	
			
		//This is ready to go - but not currently being offered
		// $email_data = array(
			// 'person_id'=>$person_id,
			// 'course_name'=>$course_info->name,
			// 'course_phone'=>$course_info->phone,
			// 'first_name'=>$person_info->first_name,
			// 'booked_date'=>date('n/j/y', strtotime($start+1000000)),
			// 'booked_time'=>date('g:ia', strtotime($start+1000000)),
			// 'booked_holes'=>$holes,
			// 'booked_players'=>$player_count			
		// );
		
				
		if ($this->is_valid_teetime($course_info, $start)) {
			$val = $this->Teetime->save($event_data,false,$response,true, $teesheet_info->course_id);
			$amount_paid = $this->input->get('amount_paid');
            if ($prepaid && !empty($amount_paid)) {
                $payment_info = array(
                    'course_id'=>$teesheet_info->course_id,
                    'amount' => $amount_paid
                );
                $this->load->model('Sale');
                $invoice_id = $this->Sale->add_credit_card_payment($payment_info);
                $bartered_teetime = array(
                    'teetime_id' => $event_data['TTID'],
                    'teesheet_id' => $event_data['teesheet_id'],
                    'start' => $event_data['start'],
                    'end' => $event_data['end'],
                    'player_count' => $event_data['player_count'],
                    'holes' => $event_data['holes'],
                    'carts' => $event_data['carts'],
                    'date_booked' => $event_data['date_booked'],
                    'booker_id' => $event_data['booker_id'],
                    'invoice_id' => $invoice_id,
					'api_id' => $api_id
                );

                // Save bartered tee time data
                $this->db->insert('teetimes_bartered', $bartered_teetime);
            }
			if ($is_foreup_app) 
			{
				$this->load->model('billing');
				$response['discount_percent'] = $course_info->foreup_discount_percent;
				$response['available_for_purchase'] = $this->billing->have_sellable_teetimes($start, $teesheet_id);
				//echo $this->db->last_query();
				$response['min_required_carts'] = 0;
				$response['min_required_holes'] = 0;
				$response['min_required_players'] = 2;
			}
			return $val;
		}
		else 
		{			
			return array('invalid_time'=>true);
		}		
	}
	function send_confirmation_email($teetime_id = '', $course_id = '') {
		if ($teetime_id != '' && $course_id != '')
		{
			$person_array = array();
			$course_info = $this->Course->get_info($course_id);
			$teetime_data = $this->Teetime->get_info($teetime_id, true);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_2);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_3);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_4);
			$person_array[] = $this->Customer->get_info($teetime_data->person_id_5);
            
            $tee_sheet_title = $teetime_data->tee_sheet_title;
            if(empty($tee_sheet_title)) {
                $tee_sheet_title = '';
            }
			// Send emails to each golfer
			foreach ($person_array as $person_info)
			{
				if ($person_info->email)
				{
					$email_data = array(
						'confirmation_sent'=>$teetime_data->confirmation_emailed,
						'person_id'=>$teetime_data->person_id,
						'course_name'=>$course_info->name,
						'course_phone'=>$course_info->phone,
						'course_id'=>$course_id,
						'first_name'=>$person_info->first_name,
						'booked_date'=>date('n/j/y', strtotime($teetime_data->start+1000000)),
						'booked_time'=>date('g:ia', strtotime($teetime_data->start+1000000)),
						'booked_holes'=>$teetime_data->holes,
						'booked_players'=>$teetime_data->player_count,
						'reservation'=> $teetime_data->TTID,
                        'tee_sheet' => $tee_sheet_title
					);
					$subject = $teetime_data->confirmation_emailed ? 'Updated Reservation Details' : 'Tee Time Reservation Confirmation';
					$this->Teetime->send_confirmation_email($person_info->email, $subject, $email_data, $course_info->email, $course_info->name);
					// Mark as confirmed
					$teetime_id = substr($teetime_id, 0, 20);
					$this->db->query("UPDATE foreup_teetime SET confirmation_emailed = 1 WHERE (TTID = '$teetime_id' OR TTID = '{$teetime_id}b') LIMIT 2");
				}
			}
		}
		return;
	}
	function update_teetime($teesheet_id, $reservation_id, $data, &$response)
	{		
		$teesheet_info = $this->Teesheet->get_info($teesheet_id);
		if (isset($data['start']))
		{
			$course_info = $this->Course->get_info($teesheet_info->course_id);
			date_default_timezone_set($course_info->timezone);
			$data['start'] = $this->to_db_time_string($data['start']);
			$data['end'] = $data['start'] + $teesheet_info->increment;
	        if ($data['end']%100 > 59)
	            $data['end'] += 40;	
			if (!$this->is_valid_teetime($course_info, $data['start'])) 
				return array('invalid_time'=>true);
		}

		return $this->Teetime->save($data,$reservation_id,$response,false, $teesheet_info->course_id);
	}
	function update_purchased_teetime($teetime_id, $players, $cart_count, $holes, $invoice_id, $is_foreup_app = false)
	{
		$teetime_info = $this->teetime->get_info($teetime_id);
		$json = array();
		
		$booking_source = 'online booking';
		if ($is_foreup_app) {
			$booking_source = 'ForeUP App';
		}
		$data = array(
			'holes'=>$holes,
			'player_count'=>$players, 
			'paid_player_count'=>$players, 
			'carts'=>$cart_count, 
			'paid_carts'=>$cart_count,
			'booking_source'=>'online',
			'details'=>"Reserved and Paid For using $booking_source @ ".date('g:ia n/j T')
		);

		$main_id = substr($teetime_id, 0, 20);
		$this->db->query("UPDATE `foreup_teetime` SET `holes` = '{$data['holes']}', `player_count` = '{$data['player_count']}', `paid_player_count` = '{$data['paid_player_count']}', `carts` = {$data['carts']}, `paid_carts` = {$data['paid_carts']}, `booking_source` = '{$data['booking_source']}', `details` = '{$data['details']}' WHERE (TTID = '{$main_id}' OR TTID = '{$main_id}b') AND `teesheet_id` = {$teetime_info->teesheet_id} LIMIT 2");
		//$this->save($data, $teetime_id, $json);
		//echo $this->db->last_query();
		log_message('error', $this->db->last_query());
		$data['invoice_id'] = $invoice_id;
		$data['teesheet_id'] = $teetime_info->teesheet_id;
		$data['holes'] = $teetime_info->holes;
		$data['start'] = $teetime_info->start;
		$data['end'] = $teetime_info->end;
		$data['teetime_id']=$teetime_id;
		$data['booker_id']=$teetime_info->booker_id;
		$data['date_booked']=$teetime_info->date_booked;
		unset($data['paid_player_count']);
		unset($data['paid_carts']);
		unset($data['booking_source']);
		unset($data['details']);
		//Save all original data in our teetimes_bartered table
		return $this->db->insert('teetimes_bartered', $data);
		//echo $this->db->last_query();
		log_message('error', $this->db->last_query());
	}
	/*
	Private function to get the player names
	*/
	function player_names($results)
	{		
		$players = array();
		for ($i=1; $i <= $results['player_count']; $i++) {			
			$person_id = !$results["person_id_{$i}"] == ''? $results["person_id_{$i}"] : $results["person_id"];
			
			$this->db->select('first_name, last_name');
			$this->db->from('people');
			$this->db->where('person_id', $person_id);
			$name = $this->db->get()->row_array();
			
			$players[] = array(
				'name'=>$name['first_name']." ". $name['last_name'],
				'person_id'=>$person_id				
			);
		}
		
		return $players;
	}
	
	/*
	Private function to return the max player count
	*/
	function MAX_PLAYERS_FOR_TEETIME()
	{
		return 4;
	}
	
	/*
	Private function to convert dates from this format '2013-01-14T19:00:00' to a string that can compare against the db
	*/
	function to_db_time_string($time)
	{
		return date('YmdHi', strtotime($time)) - 1000000;//takes the month field down by 1
	}
	
	/*
	 * Private function to return number of days between two dates
	 */	 
	 function days_between_dates($start, $end) {
		$start_ts = strtotime($start);		
		$end_ts = strtotime(date('Y-m-d',strtotime($end)));	
		$diff = $end_ts - $start_ts;
		$milliseconds_in_day = 86400;
		
		return round($diff / $milliseconds_in_day);
	}
}

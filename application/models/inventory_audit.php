<?php
class Inventory_audit extends CI_Model
{
	public function get_info($inventory_audit_id)
	{
		$this->db->from('inventory_audits');
		//$this->db->join('inventory_audits', 'inventory_audits.inventory_audit_id = inventory_audit_items.inventory_audit_id');
		$this->db->join('people', 'inventory_audits.employee_id = people.person_id', 'left');
		$this->db->where('inventory_audit_id',$inventory_audit_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->limit(1);
		return $this->db->get();
	}
	
	/*
	Returns all the inventory audits
	*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('inventory_audits');
		$this->db->join('people', 'inventory_audits.employee_id = people.person_id', 'left');
		$this->db->where("course_id", $this->session->userdata('course_id'));
		$this->db->order_by("date desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function save(&$audit_data)
	{
		$this->db->insert('inventory_audits', $audit_data);
		$audit_data['inventory_audit_id'] = $this->db->insert_id();
		return;
	}
	function save_items($audit_items)
	{
		$this->db->insert_batch('inventory_audit_items', $audit_items);
		return;	
	}
	function get_items($inv_audit_id)
	{
		$this->db->from('inventory_audit_items');
		$this->db->join('items', 'items.item_id = inventory_audit_items.item_id');
		$this->db->where('inventory_audit_id', $inv_audit_id);
		return $this->db->get();
	}
	function update_manual_count($updated_count) {
		foreach($updated_count as $manual_count) {
			$this->db->where('inventory_audit_id', $manual_count['inventory_audit_id']);
			$this->db->where('item_id', $manual_count['item_id']);
	    	$this->db->update('inventory_audit_items' ,$manual_count);
	    }
        return 1;
	}
}
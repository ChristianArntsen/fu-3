<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupon extends CI_Model
{
  public function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
    $this->db->from('coupon_definitions');
		if(!empty($course_id)) $this->db->where("{$course_id}");
		$this->db->order_by("expiration_date", "desc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}

	public function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('coupon_definitions');
		if(!empty($course_id)) $this->db->where("{$course_id}");
		return $this->db->count_all_results();
	}

  public function search($search, $limit=20)
	{
		$course_id = '';
    if (!$this->permissions->is_super_admin())
    {
      $pr = $this->db->dbprefix('coupon_definitions');
      $course_id = "AND {$pr}.course_id = '{$this->session->userdata('course_id')}'";
    }
    $this->db->from('coupon_definitions');
    $this->db->like('lower(title)', strtolower($search));
		if(!empty($course_id)) $this->db->where("{$course_id}");
    $this->db->order_by("id", "asc");
		$this->db->limit($limit);
    return $this->db->get();
	}

  public function get_info($coupon_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->from('coupon_definitions');
		$this->db->where('id',$coupon_id);
		if(!empty($course_id)) $this->db->where("{$course_id}");

		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $campaign_id is NOT an campaign
			$campaign_obj=new stdClass();

			//Get all the fields from campaigns table
			$fields = $this->db->list_fields('coupon_definitions');

			foreach ($fields as $field)
			{
				$campaign_obj->$field='';
			}

			return $campaign_obj;
		}
	}

  function get_search_suggestions($search,$limit=25)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $suggestions = array();

		$this->db->from('coupon_definitions');
		$this->db->like('lower(title)', strtolower($search));
		if(!empty($course_id)) $this->db->where("{$course_id}");
		$this->db->order_by("id", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->title);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
        
}
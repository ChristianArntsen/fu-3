<?php
class Auto_mailer_campaign extends CI_Model
{	
	/*
	Determines if a given person_id is a customer
	*/
	function exists($person_id)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
        
		$this->db->from('customers');	
		$this->db->join('people', 'people.person_id = customers.person_id');
		$this->db->where('customers.person_id',$person_id);
		$this->db->where_in('customers.course_id', array_values($course_ids));
		$this->db->limit(1);
        $query = $this->db->get();
		//echo $this->db->last_query();
		return ($query->num_rows()==1);
	}
	
	/*
	Returns all the customers
	*/
	function get_all($auto_mailer_id = 'all')
	{
		$this->db->from('auto_mailer_campaigns');
		$this->db->join('marketing_campaigns', 'marketing_campaigns.campaign_id = auto_mailer_campaigns.campaign_id');
		if ($auto_mailer_id != 'all')
		{
			$this->db->where('auto_mailer_id', $auto_mailer_id);
		}
		$this->db->order_by("days_from_trigger", "asc");
		$campaigns = $this->db->get();
		
		return $campaigns;
	}
	
	function count_all($filter = '')
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
        	//$course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        	$course_ids = array();
			$this->get_linked_course_ids($course_ids);
	        $this->db->where_in('customers.course_id', array_values($course_ids));
        }
        $this->db->from('customers');
		if ($filter == 'email')
		{
			$this->db->join('people','people.person_id = customers.person_id');
			$this->db->where('email !=', '');
			//$this->db->group_by('email');
		}
		else if ($filter == 'phone')
		{
			$this->db->join('people','people.person_id = customers.person_id');
			$this->db->where('phone_number !=', '');
			//$this->db->group_by('email');
		}
		$this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a particular customer
	*/
	function get_info($auto_mailer_id)
	{
		$this->db->from('auto_mailer_campaigns');
		$this->db->where('auto_mailer_id', $auto_mailer_id);
		$this->db->order_by("days_from_trigger", "asc");
		$campaigns = $this->db->get();
		
		return $campaigns;
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $customer_id is NOT an customer
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from customer table
			$fields = $this->db->list_fields('customers');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}

	/*
	Inserts or updates a customer
	*/
	function save($campaign_data)
	{
		return $this->db->insert('auto_mailer_campaigns',$campaign_data);
	}
	/*
	Deletes one customer
	*/
	function delete($customer_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("person_id = '$customer_id' $course_id");
		$this->db->limit(1);
		return $this->db->update('customers', array('deleted' => 1));
	}
	
	/*
	Deletes a list of customers
	*/
	function delete_list($customer_ids)
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('person_id',$customer_ids);
		return $this->db->update('customers', array('deleted' => 1));
 	}
	
	/*
	Deletes a list of customers
	*/
	function delete_all($auto_mailer_id)
	{
        $this->db->where('auto_mailer_id',$auto_mailer_id);
		return $this->db->delete('auto_mailer_campaigns');
 	}
}
?>

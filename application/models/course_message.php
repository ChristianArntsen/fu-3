<?php
class Course_message extends CI_Model 
{
	
	function __construct()
	{
		parent::__construct();	
	}
	
	/*
	Gets information about a particular message
	*/
	function get_info($message_id)
	{
		$this->db->from('course_messages');
		$this->db->where("message_id = '$message_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('course_messages');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	
	function get_all($override = false)
	{
		$this->db->from('course_messages');
		if (!$override)
		{
			$this->db->where("((course_id = {$this->session->userdata('course_id')} AND person_id = 0) OR (course_id = {$this->session->userdata('course_id')} AND person_id = {$this->session->userdata('person_id')}))");
			$this->db->where('read', 0);
		}
		$this->db->order_by('date_posted DESC');
		
		return $this->db->get()->result_array();
	}
	
	function mark_as_read($message_id)
	{
		$data = array('read' => 1);
		$this->db->where('message_id', $message_id);
		$this->db->where('course_id', $this->session->userdata('course_id'));
		$this->db->update('course_messages', $data);
	}
	
	/*
	Inserts or updates a message
	*/
	function save(&$message_data,$message_id=false)
	{
		if (!$message_id or !$this->exists($message_id))
		{
			if($this->db->insert('course_messages',$message_data))
			{
				$message_data['message_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('message_id', $message_id);
        $val = $this->db->update('course_messages',$message_data);
		//echo json_encode(array('sql'=>$this->db->last_query()));
		//return;
        
		return $val;
	}}
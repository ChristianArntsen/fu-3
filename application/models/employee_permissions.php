<?php
class Employee_permissions extends CI_Model  
{
	function __construct(){
		
	}
   
   	function save($detailed_permissions, $employee_id)
	{
		// DELETE PERMISSIONS
		$this->db->where('person_id', $employee_id);
		$this->db->limit(1);
		$this->db->delete('employee_permissions');
		
		// SAVE PERMISSIONS
		$final_permissions = array('person_id'=>$employee_id);	
		foreach ($detailed_permissions as $permission)
		{
			$final_permissions[$permission] = 1;
		}
		$this->db->limit(1);
		$success = $this->db->insert('employee_permissions',$final_permissions);

		return $success;		
	}
	
   	function get_all($person_id = false)
	{
		if ($person_id)
		{
			$this->db->from('employee_permissions');
			$this->db->where('person_id', $person_id);
			$this->db->limit(1);
			$permissions = $this->db->get();
			if ($permissions->num_rows() == 1)
				return $permissions->row_array();
		}
		return false;
	}
}
	
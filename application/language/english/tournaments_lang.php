<?php
$lang['tournaments_tournament_name'] = 'Name';
$lang['tournaments_tournament_green_fee'] = 'Green Fee';
$lang['tournaments_tournament_carts_issued'] = 'Include Cart';
$lang['tournaments_tournament_cart_fee'] = 'Cart Fee';
$lang['tournaments_tournament_pot_fee'] = 'Pot Fee';
$lang['tournaments_tournament_accumulated_pot'] = 'Accumulated Pot';
$lang['tournaments_tournament_remaining_pot_balance'] = 'Remaining Balance';

$lang['tournaments_tournament_tax_included'] = 'Tax Included';
$lang['tournaments_tournament_total_cost'] = 'Total Cost';
$lang['tournaments_tournament_customer_credit_fee'] = 'Customer Credit';
$lang['tournaments_tournament_member_credit_fee'] = 'Member Credit';
$lang['tournaments_tournament_item_search'] = 'Inventory';
$lang['tournaments_tournament_item_price'] = ' Price';
$lang['tournaments_tournament_total_price'] = 'Total Tournament Price';
$lang['tournaments_no_tournaments_to_display'] = 'No Tournaments to display';
$lang['tournaments_new']='New Tournament';
$lang['tournaments_update']='Update Tournament';
$lang['tournaments_basic_information']='Tournament Information';
$lang['tournaments_tournament_number']='Tournament Number';
$lang['tournaments_successful_adding']='You have successfully added tournament';
$lang['tournaments_successful_updating']='You have successfully updated tournament';
$lang['tournaments_error_adding_updating'] = 'Error adding/updating tournament';
$lang['tournaments_confirm_delete']='Are you sure you want to delete the selected tournaments?';
$lang['tournaments_none_selected']='You have not selected any tournaments to edit';
$lang['tournaments_successful_deleted']='You have successfully deleted';
$lang['tournaments_one_or_multiple']='tournament(s)';
$lang['tournaments_cannot_be_deleted']='Could not deleted selected tournaments, one or more of the selected tournaments has players.';
$lang['tournaments_inventory_item']='Item';
$lang['tournaments_inventory_item_price']='Price';
$lang['tournaments_inventory_item_tax_rate']='Tax Rate';
$lang['tournaments_inventory_item_quantity']='Quantity';
$lang['tournaments_inventory_item_total_price']='Total Price';

$lang['tournaments_award_information']='Award Information';
$lang['tournaments_tournament_remaining_pot_balance']='Remaining Balance';
$lang['tournaments_tournament_customer_search']='Customer Search';
$lang['tournaments_award']='Tournament Awards';
$lang['tournaments_tournament_winner_name']='Winner Name';
$lang['tournaments_tournament_winner_description']='Description';
$lang['tournaments_tournament_winner_amount']='Amount';
$lang['tournaments_winners_updated']='You have successfully awarded tournament winners';


?>

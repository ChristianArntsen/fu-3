<?php
require_once ("secure_area.php");
class Seasons extends Secure_area
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('teesheet');
		$this->load->model('Pricing');
		$this->load->model('Season');
		$this->load->model('Price_class');		
	}
	
	function index()
	{
		redirect();
	}

	/*
	Loads the season edit form
	*/
	function view_season($season_id=-1,$teesheet_id=0)
	{
		$data['season_info'] = $this->Season->get_info($season_id);
		if ($teesheet_id == 0){
			$teesheet_id = $data['season_info']->teesheet_id;
		}
		$data['seasons_array'] = $this->Season->get_all($teesheet_id, true);
		$data['teesheet_info'] = $this->teesheet->get_info($teesheet_id);

		$this->load->helper("date");
		$this->load->view("seasons/form", $data);
	}
	
	/*
	Loads the timeframe edit window for a price class
	*/
	function view_price($class_id, $season_id)
	{
		$season = $this->Season->get_info($season_id);
		$data['season_info'] = $season;
		$data['teesheet_info'] = $season->teesheet_id;
		$data['timeframes'] = $this->Pricing->get_timeframes($season_id, $class_id);
		$data['price_info'] = $this->Price_class->get($class_id);
		
		$this->load->helper('date');
		$this->load->view('config/seasonal_timeframe_modal',$data);
	}
	
	/*
	Loads the season edit form
	*/
	function delete_price($season_id, $class_id)
	{
		$price_info = $this->Price_class->get($class_id);
		if($price_info['default'] == 1){
			echo json_encode(array('success'=>false, 'message'=>'Default price class can not be deleted'));
			return false;	
		}
		
		$response = $this->Season->delete_price_class($season_id, $class_id);
		
		if(!$response){
			echo json_encode(array('success'=>false, 'message'=>'Error removing price class'));			
		}else{
			echo json_encode(array('success'=>true, 'message'=>'Price class removed'));
		}
	}	
	
	/*
	Adds a new price class to a season (with default timeframe)
	*/
	function add_price($season_id)
	{
		$class_id = (int) $this->input->post('class_id');
		$response = $this->Season->add_price_class($season_id, $class_id);
		
		if(!$response){
			echo json_encode(array('success'=>false, 'message'=>'Error adding price class to season'));			
		}else{
			echo json_encode(array('success'=>true, 'message'=>'Price class added'));
		}
	}	
	
	/*
	 * Adds a new timeframe to a price class within a season
	 */
	function add_timeframe($season_id, $class_id){
		
		$name = $this->input->post('name');
		if(empty($name)){
			$name = '';
		}
		
		$timeframe_data = array(
				'class_id'			=> $class_id,
				'season_id'			=> $season_id,
				'timeframe_name' 	=> $name,
				'monday'			=> 1,
				'tuesday'			=> 1,
				'wednesday'			=> 1,
				'thursday'			=> 1,
				'friday'			=> 1,
				'saturday'			=> 1,
				'sunday'			=> 1,
				'start_time'		=> 0,
				'end_time' 			=> 2400
		);
		
		if($timeframe_id = $this->Pricing->add_timeframe($timeframe_data))
		{
			$timeframe = $this->Pricing->get_timeframe($timeframe_id);
			$this->load->helper('date');
			$this->load->view('pricing/timeframe',array('timeframe'=>$timeframe));
		}
		else
		{
			echo json_encode(array('success'=>false, 'message'=>'Error creating time frame'));
		}	
	}
	
	/*
	 * Deletes a timeframe from a price class associated with a season
	 */ 
	function delete_timeframe($timeframe_id)
	{
		if($this->Pricing->delete_timeframe($timeframe_id))
		{
			echo json_encode(array('success'=>true,'message'=>"Timeframe was successfully deleted."));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'Error deleting timeframe'));
		}
	}
	
	function save_timeframes($season_id, $class_id){
	
		// Pull all of the variables in from the post so that we can use their array keys
		$season_id = $this->input->post('season_id');
		$timeframe_name = $this->input->post('timeframe_name');
		$monday = $this->input->post('monday');
		$tuesday = $this->input->post('tuesday');
		$wednesday = $this->input->post('wednesday');
		$thursday = $this->input->post('thursday');
		$friday = $this->input->post('friday');
		$saturday = $this->input->post('saturday');
		$sunday = $this->input->post('sunday');
		$start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');
		$price1 = $this->input->post('price1');
		$price2 = $this->input->post('price2');
		$price3 = $this->input->post('price3');
		$price4 = $this->input->post('price4');
		$price5 = $this->input->post('price5');
		$price6 = $this->input->post('price6');
		$active = $this->input->post('active');
		
		// Iterate through each timeframe that was open
		foreach($this->input->post('timeframe') as $timeframe_id)
		{
			if(empty($timeframe_name[$timeframe_id])){
				echo json_encode(array('success'=>true, 'message' => 'Timeframe name is required'));
				return false;
			}
			
			// If timeframe is the default, only allow change of prices
			if($this->Season->is_default_timeframe($timeframe_id)){
				$timeframe_data = array(			
					'price1'			=> $price1[$timeframe_id],
					'price2'			=> $price2[$timeframe_id],
					'price3'			=> $price3[$timeframe_id],
					'price4'			=> $price4[$timeframe_id],
					'price5'			=> $price5 && !empty($price5[$timeframe_id]) ? $price5[$timeframe_id] : 0,
					'price6'			=> $price6 && !empty($price6[$timeframe_id]) ? $price6[$timeframe_id] : 0					
				);
								
			}else{
	
				// Get the info from the arrays for the specified timeframe
				if($active[$timeframe_id] == 1){
					
					$timeframe_data = array(
						'timeframe_name'	=> $timeframe_name[$timeframe_id],
						'monday'			=> $monday[$timeframe_id] ? 1 : 0,
						'tuesday'			=> $tuesday[$timeframe_id] ? 1 : 0,
						'wednesday'			=> $wednesday[$timeframe_id] ? 1 : 0,
						'thursday'			=> $thursday[$timeframe_id] ? 1 : 0,
						'friday'			=> $friday[$timeframe_id] ? 1 : 0,
						'saturday'			=> $saturday[$timeframe_id] ? 1 : 0,
						'sunday'			=> $sunday[$timeframe_id] ? 1 : 0,
						'start_time'		=> $start_time[$timeframe_id],
						'end_time'			=> $end_time[$timeframe_id],
						'price1'			=> $price1[$timeframe_id],
						'price2'			=> $price2[$timeframe_id],
						'price3'			=> $price3[$timeframe_id],
						'price4'			=> $price4[$timeframe_id],
						'price5'			=> $price5 && !empty($price5[$timeframe_id]) ? $price5[$timeframe_id] : 0,
						'price6'			=> $price6 && !empty($price6[$timeframe_id]) ? $price6[$timeframe_id] : 0,
						'active'			=> $active[$timeframe_id]
					);
				
				}else{
					$timeframe_data = array(
						'active'			=> $active[$timeframe_id]	
					);				
				}
			}
			
			//Save the info for that timeframe
			if (!$this->Pricing->save_timeframe($timeframe_id, $timeframe_data)){
				$success = false;
			}
		}
		
		echo json_encode(array('success'=>true, 'message' => 'Timeframe settings saved'));			
	}
	
	/*
	Inserts/updates a season
	*/
	function save($season_id = false)
	{
		if($this->input->post('duplicate_from')){
			$response = $this->Season->duplicate($this->input->post('duplicate_from'), $this->input->post('teesheet_id'));
		
		}else{
			$teesheet_id = $this->input->post('teesheet_id');
			$season_name = $this->input->post('season_name');
			$holiday = $this->input->post('holiday');
			
			if(empty($season_name)){
				echo json_encode(array('success'=>false, 'message' => 'Season name is required'));
				return false;
			}
			
			$season_data = array(
				'season_id' => $season_id,
				'teesheet_id' => $teesheet_id,
				'season_name' => $season_name,
				'holiday' => $holiday
			);
			
			$start_date = $this->input->post('start_date');
			if($start_date){
				$season_data['start_date'] = '0000-' . $start_date;
			}

			$end_date = $this->input->post('end_date');
			if($end_date){
				$season_data['end_date'] = '0000-' . $end_date;
			}		
			
			$response = $this->Season->save($season_data, $season_id);
		}
		
		if(!empty($response)){
			if($season_id){
				$response = $season_id;
			}
			echo json_encode(array('success' => true, 'message' => 'Season successfully saved', 'season_id' => $response));
		}else{       
			echo json_encode(array('success' => false, 'message' => 'There was an error saving the season'));
		}
	}

	function delete_season($season_id = false)
	{
		if($this->Season->delete_season($season_id))
		{
			echo json_encode(array('success'=>true,'message'=>"Season $season_id was successfully deleted."));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>'There was an error deleting the season.'));
		}
	}
	
	function get_price($time = false, $date = false)
	{
		$this->load->model('Pricing');
		var_dump($this->Pricing->get_price(83, 99, $time, $date));
	}
	
	function get_price_classes($season_id, $class_id = null){
		
	}
}

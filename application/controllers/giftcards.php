<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Giftcards extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('giftcards');
	}

	function index()
	{
		$this->session->unset_userdata('punch_cards_selected');
		$config['base_url'] = site_url('giftcards/index');
		$config['total_rows'] = $this->Giftcard->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_giftcards_manage_table($this->Giftcard->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('giftcards/manage',$data);
	}
	function exists($giftcard_number = '')
	{
		echo json_encode(array('exists'=>$this->Giftcard->get_giftcard_id($giftcard_number)));
	}
	function excel()
    {
       	$data = file_get_contents("import_giftcards.csv");
        $name = 'import_giftcards.csv';
	    force_download($name, $data);
    }

    /* added for excel expert */
	function excel_export() {
		$data = $this->Giftcard->get_all()->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("Gift Card Number", "Value", "Expiration Date", "Date Issued", "Details", "Customer ID", "Department", 'Category');
		$rows[] = $row;
		foreach ($data as $r) {
			$row = array(
				'="'.$r->giftcard_number.'"',
				$r->value,
				strtotime($r->expiration_date) < 0 ? 'No expiration':date('m/d/Y', strtotime($r->expiration_date)),
				strtotime($r->date_issued) < 0 ? 'No expiration':date('m/d/Y', strtotime($r->date_issued)),
				$r->details,
				$r->customer_id,
				$r->department,
				$r->category
			);
			$rows[] = $row;
		}
		
		$content = array_to_csv($rows);
		force_download('giftcards_export' . '.csv', $content);
		exit;
	}
	function excel_import()
   {
           $this->load->view("giftcards/excel_import", null);
   }

   function do_excel_import()
   {
           $this->db->trans_start();
           $msg = 'do_excel_import';
           $failCodes = array();
           if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
           {
                   $msg = lang('giftcards_excel_import_failed');
                   echo json_encode( array('success'=>false,'message'=>$msg) );
                   return;
           }
           else
           {
				$path_info = pathinfo($_FILES['file_path']['name']);
                if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE && strtolower($path_info['extension']) == 'csv')
                   {
                           //Skip first row
                           fgetcsv($handle);
                           
                        while (($data = fgetcsv($handle)) !== FALSE) 
						{
							       $giftcard_data = array(
                                   'giftcard_number'       =>      isset($data[0])?$data[0]:'',
                                   'value'                 =>      isset($data[1])?(float) str_replace(array('$',','), '', $data[1]):0,
                                   //'customer_id'         =>      ($data[2])?$data[2]:null,
                                   'details'			   =>	   isset($data[3])?$data[3]:'',
                					'expiration_date'      =>      isset($data[2])?$data[2]:'',
                					'department'      =>      isset($data[4]) && (!isset($data[5]) || $data[5] == '')?$data[4]:'',
                					'category'      =>      isset($data[5])?$data[5]:'',
                					'course_id'            =>  $this->session->userdata('course_id')
                                   );
                                   if (strpos($giftcard_data['giftcard_number'], 'E+'))
								   {
								   		echo json_encode(array('success'=>false, 'message'=>lang('giftcards_format_giftcard_numbers')));
										return;
								   }
								   else if($this->Giftcard->save($giftcard_data)) 
                                   {
                                           
                                   }
                                   else//insert or update item failure
                                   {
                                           echo json_encode( array('success'=>false,'message'=>lang('giftcards_duplicate_item_ids')));
                                           return;
                                   }
                           }
                           
                   }
                   else 
                   {
                           echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
                           return;
                   }
           }

           $this->db->trans_complete();
           echo json_encode(array('success'=>true,'message'=>lang('giftcards_import_successful')));
   }
								
	function cleanup()
	{
		$this->Giftcard->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('giftcards_cleanup_sucessful')));
	}
	
	function search($offset = 0)
	{
		$search=$this->input->post('search');
		if ($this->session->userdata('punch_cards_selected'))
		{
			$data_rows=get_punch_cards_manage_table_data_rows($this->Punch_card->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
			$config['total_rows'] = $this->Punch_card->search($search, 0);
        }
		else
		{
			$data_rows=get_giftcards_manage_table_data_rows($this->Giftcard->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
			$config['total_rows'] = $this->Giftcard->search($search, 0);
        }
		$config['base_url'] = site_url('giftcards/index');
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		if ($this->session->userdata('punch_cards_selected'))
			$suggestions = $this->Punch_card->get_search_suggestions($this->input->get('term'),100);
		else
			$suggestions = $this->Giftcard->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	function giftcard_search($type='')
	{
		$suggestions = $this->Giftcard->get_giftcard_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$giftcard_id = $this->input->post('row_id');
		$data_row=get_giftcard_data_row($this->Giftcard->get_info($giftcard_id),$this);
		echo $data_row;
	}

	function view($giftcard_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = $view_only;
		$data['cart_line'] = $cart_line;
		$data['giftcard_info']=$this->Giftcard->get_info($giftcard_id);
		if ($cart_line)
		{
			$giftcard_data = $this->sale_lib->get_giftcard_details($cart_line);
			$data['giftcard_info']->customer_id = $giftcard_data['giftcard_data']['customer_id'];
			$data['giftcard_info']->giftcard_number = $giftcard_data['giftcard_data']['giftcard_number'];
			$data['giftcard_info']->customer_name = $giftcard_data['giftcard_data']['customer_name'];
			$data['giftcard_info']->value = $giftcard_data['value'];
			$data['giftcard_info']->details = $giftcard_data['giftcard_data']['details'];
			$data['giftcard_info']->expiration_date = $giftcard_data['giftcard_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['giftcard_info']->customer_id);
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['customer'] = $customer_info->last_name.', '.$customer_info->first_name;
		else
			$data['customer'] = 'No Customer';
		
		$this->load->view("giftcards/form",$data);
	}
	function view_punch_card($punch_card_id=-1, $cart_line = 0, $view_only = false)
	{
		$data = array();
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['punch_card_info']=$this->Punch_card->get_info($punch_card_id);
		//print_r($data['punch_card_info']);
		$customer_info = $this->Customer->get_info($data['punch_card_info']->customer_id);
		$data['punch_card_info']->value = $this->Punch_card->get_items($punch_card_id, false);
		//print_r($data['punch_card_info']->value);
		//print_r($customer_info);
		//echo $customer_info->first_name;
		$data['customer'] = '';
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['punch_card_info']->customer_name = $customer_info->last_name.', '.$customer_info->first_name;

		$this->load->view("punch_cards/form",$data);
	}
	function view_lookup()
	{
		$data = array();
		$this->load->view("giftcards/lookup", $data);
	}
	function lookup()
	{
		$giftcard_number = $this->input->post('giftcard_number');
		$giftcard_info = $this->Giftcard->get_info($this->Giftcard->get_giftcard_id($giftcard_number));
		echo json_encode(array($giftcard_info));
	}
	function save_punch_card($punch_card_id)
	{
		$punch_card_data = array(
			'punch_card_number' => $this->input->post('punch_card_number'),
			'expiration_date' => $this->input->post('expiration_date'),
			'details' => $this->input->post('details'),
			'customer_id' => $this->input->post('customer_id')			
		);
		
		if( $this->Punch_card->save( $punch_card_data, $punch_card_id ) )
		{
			echo json_encode(array('success'=>true,'message'=>lang('giftcards_successful_updating').' '.
			$punch_card_data['punch_card_number'],'punch_card_id'=>$punch_card_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('giftcards_error_adding_updating').' '.
			$punch_card_data['punch_card_number'],'punch_card_id'=>-1));
		}
	}
	function save($giftcard_id=-1)
	{		
		$giftcard_data = array(
			'giftcard_number'=>$this->input->post('giftcard_number'),
			'value'=>$this->input->post('value'),
			'expiration_date'=>$this->input->post('expiration_date'),
			'details'=>$this->input->post('details'),
			'department'=>$this->input->post('department'),
			'category'=>$this->input->post('category'),
			'customer_id'=>$this->input->post('customer_id')=='' ? null:$this->input->post('customer_id')
		);
		$customer_name = $this->input->post('customer');

		if ($giftcard_id == -1)
			$giftcard_data['course_id'] = $this->session->userdata('course_id');

		// If a customer name is entered, but it isn't someone already in the system, save them to customers
		if ($giftcard_data['customer_id'] === null && $customer_name != '' && $customer_name != 'No Customer')
		{
			$this->load->library('name_parser');
			$np = new Name_parser();
			$np->setFullName($customer_name);
			$np->parse();
			if (!$np->notParseable()) {
				$person_data = array(
					'first_name'=>$np->getFirstName(),
					'last_name'=>$np->getLastName()
				);
				$customer_data = array(
					'course_id'=>$this->session->userdata('course_id')
				);
				if ($this->Customer->save($person_data, $customer_data))
				{
					$giftcard_data['customer_id'] = $customer_data['person_id'];
				}
				
			}
			
		}

		if( $this->Giftcard->save( $giftcard_data, $giftcard_id ) )
		{
			//New giftcard
			if($giftcard_id == -1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('giftcards_successful_adding').' '.
				$giftcard_data['giftcard_number'],'giftcard_id'=>$giftcard_data['giftcard_id']));
				$giftcard_id = $giftcard_data['giftcard_id'];
			}
			else //previous giftcard
			{
				echo json_encode(array('success'=>true,'message'=>lang('giftcards_successful_updating').' '.
				$giftcard_data['giftcard_number'],'giftcard_id'=>$giftcard_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('giftcards_error_adding_updating').' '.
			$giftcard_data['giftcard_number'],'giftcard_id'=>-1));
		}

	}
	function switch_card_type($offset = 0)
    {
        $data = array();
		$search=$this->input->post('search');
		$giftcards_or_punch_cards = $this->input->post('item_type');
		if ($giftcards_or_punch_cards)
		{		
		   	$data_rows = get_punch_cards_manage_table_data_rows($this->Punch_card->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
			$this->session->set_userdata('punch_cards_selected', true);		
		}
		else
		{
		    $data_rows = get_giftcards_manage_table_data_rows($this->Giftcard->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
			$this->session->unset_userdata('punch_cards_selected');
        }
        $data['sql']=$this->db->last_query();
        $config['base_url'] = site_url('items/index');
        $config['total_rows'] = $this->Item->item_type_choice($food_or_proshop, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
    }
    
	function delete()
	{
		if ($this->session->userdata('punch_cards_selected'))
		{
			$punch_cards_to_delete=$this->input->post('ids');
	
			if($this->Punch_card->delete_list($punch_cards_to_delete))
			{
				echo json_encode(array('success'=>true,'message'=>lang('giftcards_successful_deleted').' '.
				count($punch_cards_to_delete).' '.lang('giftcards_one_or_multiple')));
			}
			else
			{
				echo json_encode(array('success'=>false,'message'=>lang('giftcards_cannot_be_deleted')));
			}
		}
		else
		{
			$giftcards_to_delete=$this->input->post('ids');
	
			if($this->Giftcard->delete_list($giftcards_to_delete))
			{
				echo json_encode(array('success'=>true,'message'=>lang('giftcards_successful_deleted').' '.
				count($giftcards_to_delete).' '.lang('giftcards_one_or_multiple')));
			}
			else
			{
				echo json_encode(array('success'=>false,'message'=>lang('giftcards_cannot_be_deleted')));
			}
		}
	}
		
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}
	
	function generate_barcodes($giftcard_ids)
	{
		$result = array();

		$giftcard_ids = explode('~', $giftcard_ids);
		foreach ($giftcard_ids as $giftcard_id)
		{
			$giftcard_info = $this->Giftcard->get_info($giftcard_id);
			$result[] = array('name' =>'Balance: '.to_currency($giftcard_info->value), 'id'=> $giftcard_info->giftcard_number, 'number'=>$giftcard_info->giftcard_number);
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		$this->load->view("barcode_sheet", $data);
	}
	
	function generate_barcode_labels($giftcard_ids)
	{
		$result = array();

		$giftcard_ids = explode('~', $giftcard_ids);
		foreach ($giftcard_ids as $giftcard_id)
		{
			$giftcard_info = $this->Giftcard->get_info($giftcard_id);
			$result[] = array('name' =>'Balance: '.to_currency($giftcard_info->value), 'id'=> $giftcard_info->giftcard_number, 'number'=>$giftcard_info->giftcard_number);
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		$this->load->view("barcode_labels", $data);
	}
}
?>
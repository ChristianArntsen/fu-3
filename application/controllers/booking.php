<?php
// New online booking controller
class Booking extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('teesheet');
        $this->load->model('customer');
        $this->load->model('course');
        $this->load->model('Booking_class');
        $this->load->model('invoice');
        $this->load->model('user');
	}

	private function filter_course_data($course_data){
		$validParams = array(
			'course_id',
			'active',
			'area_id',
			'name',
			'address',
			'city',
			'state',
			'postal',
			'zip',
			'country',
			'website',
			'area_code',
			'timezone',
			'open_time',
			'close_time',
			'latitude_centroid',
			'longitude_centrod',
			'phone',
			'online_booking',
			'online_booking_protected',
			'booking_rules',
			'email',
			'no_show_policy',
			'include_tax_online_booking',
			'credit_card_provider',
			'foreup_discount_percent',
			'customer_credit_nickname',
			'member_balance_nickname'
		);

		if($this->config->item('ets_key')){
			$course_data['credit_card_provider'] = 'ets';
		}else{
			$course_data['credit_card_provider'] = 'mercury';
		}

		return elements($validParams, $course_data, '');
	}

	function index($course_id, $passed_schedule_id = false){

		$course_info = (array) $this->course->get_info($course_id);
		$filtered_course_info = $this->filter_course_data($course_info);
		
		if($course_info['timezone']){
			date_default_timezone_set($course_info['timezone']);
		}

		$default_schedule = array();
		$booking_class_exists = false;

		// Get schedules with online booking enabled
		$schedules = $this->teesheet->get_teesheets($course_id, array('online_only' => true));

		// If no schedules were found with online booking enabled, show
		// booking unavailable error
		if(empty($schedules)){
			$this->load->view('booking/unavailable');
			return false;
		}

		if($passed_schedule_id){
			$schedule_id = (int) $passed_schedule_id;
			$this->session->set_userdata('booking_class_id', null);
			$this->session->set_userdata('booking_class_info', null);

		}else if($this->session->userdata('schedule_id')){
			$schedule_id = (int) $this->session->userdata('schedule_id');

		}else{
			$schedule_id = $schedules[0]['teesheet_id'];
		}

		// Loop through schedules and retrieve booking classes for each
		$selected_found = false;
		foreach($schedules as &$schedule){
			$schedule['booking_classes'] = $this->Booking_class->get_all($schedule['teesheet_id']);

			foreach($schedule['booking_classes'] as $key => $booking_class){
				if($booking_class['active'] == 0){
					unset($schedule['booking_classes'][$key]);
				}
			}
			$schedule['booking_classes'] = array_values($schedule['booking_classes']);

			if(!empty($schedule['booking_classes'])){
				$booking_class_exists = true;
			}

			if($schedule_id == $schedule['teesheet_id']){
				$default_schedule = $schedule;
				$schedule['selected'] = true;
				$selected_found = true;
			}
		}

		if(!$selected_found){
			$schedules[0]['selected'] = true;
			$this->session->unset_userdata('schedule_id');
			$schedule_id = (int) $schedules[0]['teesheet_id'];
			$default_schedule = $schedules[0];
		}

		if(!$booking_class_exists){
			$this->session->set_userdata('booking_class_id', null);
			$this->session->set_userdata('booking_class_info', null);
		}

		$data = array();
		$data['schedules'] = $schedules;
		$data['course'] = $filtered_course_info;

		$user_data = array();
		$user_data['logged_in'] = false;

		// If user is logged in
		if($this->session->userdata('customer_id')){
			$this->load->model('user');
			$user_data = (array) $this->customer->get_info($this->session->userdata('customer_id'), $course_id);

			$username = $this->user->get_username($this->session->userdata('customer_id'));
			$user_data['username'] = $username;

			$user_data['user_id'] = (int) $this->session->userdata('customer_id');
			$user_data['logged_in'] = true;

			$this->load->model('Customer_credit_card');
			$user_data['credit_cards'] = $this->Customer_credit_card->get($this->session->userdata('customer_id'));

			$this->load->model('user');
			$user_data['reservations'] = $this->user->get_teetimes($user_data['user_id'], 'upcoming', $course_id);

			$purchases = $this->user->get_purchases($user_data['user_id'], $course_id);
			$user_data['purchases'] = $purchases['summary'];

			$invoices = $this->invoice->get_all($user_data['user_id']);
			$user_data['invoices'] = $invoices->result_array();

			$user_data['gift_cards'] = $this->user->get_giftcards($user_data['user_id'], $course_id);
		}
		$data['user'] = $user_data;

		$holes = 18;
		if($default_schedule['limit_holes'] != 0){
			$holes = (int) $default_schedule['limit_holes'];
		}
		$players = $default_schedule['minimum_players'];

		$time = 'morning';
		if((int) date('H') >= 14){
			$time = 'evening';
		} else if((int) date('H') >= 10){
			$time = 'midday';
		}

		$default_date = date('m-d-Y');

		// Get close time
		$close_time = (int) $default_schedule['online_close_time'];
		if($this->session->userdata('booking_class_info')){
			$close_time = (int) $this->session->userdata('booking_class_info')->online_close_time;
		}
		
		// If tee sheet close time is greater than course close time,
		// force it to course time
		if($close_time > $course_info['close_time']){
			$close_time = (int) $course_info['close_time'];
		}
		
		// If it is past the close time, default date to tomorrow
        if(($close_time - 100) <= (int) date('H').'00'){
			$default_date = date('m-d-Y', strtotime('+1 day'));
		}

		$filter['schedule_id'] = (int) $schedule_id;
		$filter['course_id'] = (int) $course_id;
		$filter['time'] = $time;
		$filter['date'] = $default_date;
		$filter['holes'] = $holes;
		$filter['players'] = (int) $default_schedule['minimum_players'];
		$filter['booking_class'] = $this->session->userdata('booking_class_id');
		$data['filter'] = $filter;

		$settings['minimum_players'] = (int) $default_schedule['minimum_players'];
		$settings['limit_holes'] = (int) $default_schedule['limit_holes'];
		$settings['days_in_booking_window'] = (int) $default_schedule['days_in_booking_window'];
		$settings['booking_carts'] = (int) $default_schedule['booking_carts'];
		$settings['online_booking_protected'] = 0;
		$settings['online_close_time'] = $close_time;

		if(!empty($filter['booking_class'])){
			$settings['online_booking_protected'] = (int) $this->session->userdata('booking_class_info')->online_booking_protected;
		}
		$data['settings'] = $settings;

		$this->load->view('booking/reservations', $data);
	}

	function credit_card_window(){

		$course_id = $this->session->userdata('course_id');
		$person_id = $this->session->userdata('customer_id');
		$viewData = '';

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key')){

			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('isSave', 'true')
					   ->send();

			if ($session->id){
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array(
					'session' => $session,
				);

				if(!empty($reservation['start'])){
					$data['no_show_policy'] = $this->config->item('no_show_policy');
				}
				$viewData = $this->load->view('booking/ets_capture_form', $data, true);

			}else{
				$data = array('processor' => 'ETS');
				$viewData = $this->load->view('booking/capture_form_error', $data, true);
			}
		}

		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
			$foreupId = $this->config->item('foreup_mercury_id');
			$foreupPass = $this->config->item('foreup_mercury_password');

			$this->load->library('Hosted_checkout_2');

			$HC = new Hosted_checkout_2();
			$HC->set_default_swipe('Manual');
			$HC->set_merchant_credentials($this->config->item('mercury_id'), $this->config->item('mercury_password'));
			$HC->set_response_urls('booking/mercury_card_capture', 'booking/credit_card_cancel');

			$initialize_results = $HC->initialize_payment('1.00', '0.00', 'PreAuth', 'POS', 'Recurring');

			if ((int) $initialize_results->ResponseCode == 0)
			{
				// Set invoice number to save in the database
				$invoice = $this->sale->add_credit_card_payment(array('tran_type' => 'PreAuth', 'frequency' => 'Recurring'));
				$this->session->set_userdata('invoice', $invoice);

				$user_message = (string) $initialize_results->Message;
				$return_code = (int) $initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string) $initialize_results->PaymentID);
				$url = $HC->base_url().'/mobile/mCheckout.aspx';

				$data = array('user_message' => $user_message, 'return_code' => $return_code, 'url' => $url, 'payment_id' => (string) $initialize_results->PaymentID);

				$viewData = $this->load->view('booking/mercury_capture_form', $data, true);
			}
		}

		echo $viewData;
	}

	function mercury_card_capture(){

		// If there was an error processing the card
		if($this->input->post('ReturnCode') != 0){
			$message = $this->input->post('ReturnMessage');
			echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
			return false;
		}

		$person_id = $this->session->userdata('customer_id');

		$this->load->model('Customer_credit_card');
		$credit_card_id = (int) $this->Customer_credit_card->capture_card($person_id);
		$credit_card = (array) $this->Customer_credit_card->get_info($credit_card_id);

		// Add new credit card to user's profile
		echo "<script>
			var card = {};
			card.credit_card_id = {$credit_card_id};
			card.masked_account = '{$credit_card['masked_account']}';
			card.expiration = '{$credit_card['expiration']}';
			card.card_type = '{$credit_card['card_type']}';

			window.parent.App.data.user.get('credit_cards').add(card);
		</script>";
	}

	function mercury_payment_capture($teetime_id){

		$this->load->model('teetime');
		$this->load->model('sale');
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();

		$foreupId = $this->config->item('foreup_mercury_id');
		$foreupPass = $this->config->item('foreup_mercury_password');
		$HC->set_merchant_credentials($foreupId, $foreupPass);

		// Retrieve data about tee time being purchased
		$person_id = $this->session->userdata('customer_id');
		$purchased_tee_time = $this->session->userdata('purchased_teetime');

		// Verify credit card payment
		$payment_id = $this->input->post('PaymentID');
		$HC->set_payment_id($payment_id);
		$verify_results = $HC->verify_payment();
		$HC->complete_payment();

		// Update credit card payment data
		$payment_data = array (
			'course_id' => $this->session->userdata('course_id'),
			'mercury_id' => $this->config->item('foreup_mercury_id'),
			'tran_type' => (string) $verify_results->TranType,
			'amount' => (string) $verify_results->Amount,
			'auth_amount' => (string) $verify_results->AuthAmount,
			'card_type' => (string) $verify_results->CardType,
			'frequency' => 'OneTime',
			'masked_account'=> (string) $verify_results->MaskedAccount,
			'cardholder_name'=> (string) $verify_results->CardholderName,
			'ref_no' => (string) $verify_results->RefNo,
			'operator_id' => (string) $verify_results->OperatorID,
			'terminal_name'=> (string) $verify_results->TerminalName,
			'trans_post_time' => (string) $verify_results->TransPostTime,
			'auth_code' => (string)	$verify_results->AuthCode,
			'voice_auth_code'=>	(string) $verify_results->VoiceAuthCode,
			'payment_id' =>	$payment_id,
			'acq_ref_data' => (string) $verify_results->AcqRefData,
			'process_data' => (string) $verify_results->ProcessData,
			'token' => (string) $verify_results->Token,
			'response_code' => (int) $verify_results->ResponseCode,
			'status' => (string) $verify_results->Status,
			'status_message' => (string) $verify_results->StatusMessage,
			'display_message' => (string) $verify_results->DisplayMessage,
			'avs_result' => (string) $verify_results->AvsResult,
			'cvv_result' => (string) $verify_results->CvvResult,
			'tax_amount' => (string) $verify_results->TaxAmount,
			'avs_address' => (string) $verify_results->AVSAddress,
			'avs_zip' => (string) $verify_results->AVSZip,
			'payment_id_expired' => (string) $verify_results->PaymendIDExpired,
			'customer_code' => (string) $verify_results->CustomerCode,
			'memo' => (string) $verify_results->Memo
		);

		$invoice_id = (int) $this->session->userdata('invoice_id');
		$this->sale->update_credit_card_payment($invoice_id, $payment_data);
		$this->session->unset_userdata('invoice_id');

		// If transaction verified successfully
		if((string) $verify_results->Status == 'Approved'){

			// Update tee time to show number players/carts were paid for
			$this->teetime->save_purchased_teetime($teetime_id, $purchased_tee_time['paid_players'], $purchased_tee_time['paid_carts'], $invoice_id);

			if($purchased_tee_time['paid_carts']){
				$num_carts = $purchased_tee_time['paid_players'];
			}else{
				$num_carts = 0;
			}

			// Send email receipt to customer
			$email_data = array(
				'course_name' => $this->session->userdata('course_name'),
				'booked_date' => date('n/j/y', strtotime($purchased_tee_time['start'] + 1000000)),
				'booked_time' => date('g:ia', strtotime($purchased_tee_time['start'] + 1000000)),
				'booked_holes' => $purchased_tee_time['holes'],
				'booked_players' => $purchased_tee_time['paid_players'],
				'booked_carts' => $num_carts,
				'customer_name' => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
				'customer_email' => $this->session->userdata('customer_email'),
				'card_type' => $payment_data['card_type'],
				'current_date' => date('g:ia n/j/y T'),
				'confirmation_number' => $teetime_id,
				'subtotal' => $purchased_tee_time['subtotal'],
				'discount' => $purchased_tee_time['discount'],
				'total' => $purchased_tee_time['total']
			);

			send_sendgrid(
				$this->session->userdata('customer_email'),
				'Tee Time Purchase Receipt',
				$this->load->view("email_templates/purchase_receipt", $email_data, true),
				'booking@foreup.com',
				'ForeUP'
			);

			$players_json = (int) $purchased_tee_time['paid_players'];
			$carts_json = 'true';
			if($num_carts == 0){
				$carts_json = 'false';
			}

			// Update purchased reservation, and close credit card window
			echo "<script>
				var data = {'can_purchase': false,
					'purchased':true,
					'players':".$players_json.",
					'carts': ".$carts_json.",
					'paid_player_count': ".(int) $purchased_tee_time['paid_players']."
				};

				window.parent.App.data.last_reservation.set(data);
				if(window.parent.App.data.user.get('reservations').get('".$teetime_id."')){
					window.parent.App.data.user.get('reservations').get('".$teetime_id."').set(data);
				}
				window.parent.App.data.times.refresh();
				window.parent.App.router.navigate('confirmation/".$teetime_id."', {trigger: true});
				window.parent.ga('send', 'event', 'Online Booking', 'Tee Time Purchase', window.parent.App.data.course.getCurrentSchedule());
				window.parent.$('#modal').modal('hide');
			</script>";

		// If there was an error verifying the transaction
		}else{
			$message = (string) $verify_results->DisplayMessage;
			if(empty($message)){
				$message = 'There was an error processing your card, please try again.';
			}
			echo "<script>window.parent.alert('{$message}'); window.parent.$('#modal').modal('hide');</script>";
			return false;
		}
	}

	function credit_card_cancel(){
		echo "<script>window.parent.$('#modal').modal('hide');</script>";
	}

	// Loads Mercury iframe to allow customer to purchase their teetime (and give revenue to ForeUp)
	function purchase_reservation_window($teetime_id = null){

		$this->load->model('billing');

		// Retrieve teetime details to calculate price of teetime
		$viewData = '';
		$this->load->model('teetime');
		$teetime_info = (array) $this->teetime->get_info($teetime_id);

		// Make sure valid tee time ID is passed
		if(empty($teetime_info['TTID'])){
			echo "Sorry, the teetime you selected could not be found.";
			return false;
		}
		$close_link = "<a style='color: #2A7CC0; text-decoration: underline; cursor: pointer'
			onClick=\"window.parent.App.router.navigate('confirmation/{$teetime_id}', {trigger: true})\">
				Close Window</a>";

		$course_id = $this->session->userdata('course_id');
		$start = DateTime::createFromFormat('YmdHi', $teetime_info['start'] + 1000000);
		$start_date = $start->format('Y-m-d');
		$start_time = $start->format('Hi');
		$start_day_of_week = $start->format('D');
		$paid_players = (int) $this->input->get('players');
		$paid_carts = $this->input->get('carts');

		// Make sure the tee time is still available for purchase
		$can_purchase = (bool) $this->billing->have_sellable_teetimes((int) $teetime_info['start']);
		if(!$can_purchase){
			echo "Sorry, that tee time is no longer available for online purchase.<br /><br />{$close_link}";
			return false;
		}

		// Make sure they are purchasing for 2 or more players
		if($paid_players < 2){
			echo "Two or more players are required to allow pre-paying.<br /><br />{$close_link}";
			return false;
		}

		if($paid_carts === 'false' || $paid_carts === 0 || $paid_carts === false){
			$paid_carts = false;
		}else{
			$paid_carts = true;
		}

		$price_class_id = (int) $teetime_info['price_class_1'];
		$holes = $teetime_info['holes'];
		$teesheet_id = (int) $teetime_info['teesheet_id'];
		$discount_percent = $this->config->item('foreup_discount_percent');

		if($price_class_id == 0){
			$price_class_id = false;
		}

		// Get price of selected teetime
		// If course is using new seasonal pricing
		if($this->config->item('seasonal_pricing') == 1){
			$this->load->model('pricing');
			$green_fee = $this->pricing->get_price($teesheet_id, $price_class_id, $start_date, $start_time, $holes, false);
			$cart_fee = $this->pricing->get_price($teesheet_id, $price_class_id, $start_date, $start_time, $holes, true);

		// If course is using old standard pricing
		}else{
			$prices = $this->Green_fee->get_info();
			$price_indexes = $this->teesheet->determine_price_indexes($start_time, $holes, $start_day_of_week);
			$booking_class_info = $this->session->userdata('booking_class_info');

			$price_category = $price_indexes['price_category'];
			if($booking_class_info){
				$price_category = $booking_class_info->price_class;
			}

			$green_fee = (float) $prices[$teesheet_id][$course_id.'_'.$price_indexes['green_fee_index']]->$price_category;
			$cart_fee = (float) $prices[$teesheet_id][$course_id.'_'.$price_indexes['cart_price_index']]->$price_category;
		}

		$include_tax = (bool) $this->config->item('include_tax_online_booking');
		$green_fee_tax = 0;
		$cart_fee_tax = 0;

		// If course is including tax in prices, add taxes to fees
		if($include_tax){
			$this->load->model('item');
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate();
			$greenfee_tax_rate = (float) $greenfee_tax_rate[0]['percent'];
			$cartfee_tax_rate = $this->Item->get_cart_tax_rate();
			$cartfee_tax_rate = (float) $cartfee_tax_rate[0]['percent'];

			$green_fee_tax = round($green_fee * ($greenfee_tax_rate / 100), 2);
			$cart_fee_tax = round($cart_fee * ($cartfee_tax_rate / 100), 2);

			$green_fee += $green_fee_tax;
			$cart_fee += $cart_fee_tax;
		}

		// Calculate final total (with ForeUp discount)
		$subtotal = (float) round($green_fee * $paid_players, 2);
		if($paid_carts){
			$subtotal += (float) round($cart_fee * $paid_players, 2);
		}
		$discount = (float) round($subtotal * ($discount_percent / 100), 2);
		$total = (float) round($subtotal - $discount, 2);

		// Save tee time purchase data to session (for receipt later)
		$purchased_tee_time = array(
			'teetime_id' => $teetime_id,
			'paid_players' => $paid_players,
			'paid_carts' => $paid_carts,
			'holes' => $holes,
			'teesheet_id' => $teesheet_id,
			'start' => $teetime_info['start'],
			'end' => $teetime_info['end'],
			'subtotal' => $subtotal,
			'discount' => $discount,
			'total' => $total
		);
		$this->session->set_userdata('purchased_teetime', $purchased_tee_time);

		$foreupId = $this->config->item('foreup_mercury_id');
		$foreupPass = $this->config->item('foreup_mercury_password');

		// Initialize Mercury credit card iframe
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		$HC->set_default_swipe('Manual');
		$HC->set_merchant_credentials($foreupId, $foreupPass);
		$HC->set_response_urls('booking/mercury_payment_capture/'.$teetime_id, 'booking/credit_card_cancel');

		$initialize_results = $HC->initialize_payment($total, '0.00', 'Sale', 'eCOM', 'OneTime');

		// If mercury initialized OK
		if ((int) $initialize_results->ResponseCode == 0){
			$user_message = (string) $initialize_results->Message;
			$return_code = (int) $initialize_results->ResponseCode;
			$this->session->set_userdata('payment_id', (string) $initialize_results->PaymentID);
			$url = $HC->base_url().'/mobile/mCheckout.aspx';

			$data = array('user_message' => $user_message, 'return_code' => $return_code, 'url' => $url, 'payment_id' => (string) $initialize_results->PaymentID);
			$viewData = $this->load->view('booking/mercury_capture_form', $data, true);
		}

		echo $viewData;
	}

	// End point for email cancellation link
	function confirm_cancellation($teetime_id, $person_id){

		$this->load->model('teetime');
		$this->load->model('_reservation');
		$this->load->model('course');
		$this->load->model('teesheet');

		$data = array (
			'teetime_id' => $teetime_id,
			'teetime_info' => array(),
			'person_id' => $person_id
		);

		// Retrieve tee time details
		$tee_time_info = (array) $this->teetime->get_info($teetime_id);
		$data['teetime_info'] = $tee_time_info;

		// Parse tee time start time/date
		$timestamp = (int) $tee_time_info['start'] + 1000000;
		$data['teetime_info']['start_date'] = date('F jS, Y', strtotime($timestamp));
		$data['teetime_info']['start_time'] = date('g:ia', strtotime($timestamp));

		// Get name of course and tee sheet
		$tee_sheet_info = (array) $this->teesheet->get_info((int) $tee_time_info['teesheet_id']);
		$data['teetime_info']['tee_sheet_title'] = $tee_sheet_info['title'];

		$course_info = (array) $this->course->get_info((int) $tee_sheet_info['course_id']);
		$data['teetime_info']['course_name'] = $course_info['name'];
		$data['course_id'] = (int) $course_info['course_id'];
		$data['course_phone'] = $course_info['phone'];

		$this->load->view('booking/confirm_cancellation', $data);
	}

	// Cancel tee time
	function cancel($teetime_id, $person_id){

		$this->load->model('teetime');
		$this->load->model('course');
		$this->load->model('teesheet');

		$data['success'] = false;
		$teetime_info = $this->teetime->get_info($teetime_id);

		$course_info = $this->Course->get_info_from_teesheet_id($teetime_info->teesheet_id);
		$customer_info = $this->Customer->get_info($person_id);
		$tee_sheet_info = $this->teesheet->get_info($teetime_info->teesheet_id);

		$data['course_id'] = $course_info->course_id;
		$data['course_name'] = $course_info->name;

		// If tee time hasn't already been cancelled
		if($teetime_info->status != 'deleted'){

			// Cancel tee time
			$data['success'] = $this->teetime->delete($teetime_id, $person_id);
			$this->db->trans_complete();

			// Send cancellation email if the cancellation succeeded
			if ($data['success']){

				$email_data = array(
					'cancelled' => true,
					'person_id' => $person_id,
					'course_name' => $course_info->name,
					'course_phone' => $course_info->phone,
					'first_name' => $customer_info->first_name,
					'booked_date' => date('n/j/y', strtotime($teetime_info->start + 1000000)),
					'booked_time' => date('g:ia', strtotime($teetime_info->start + 1000000)),
					'booked_holes' => $teetime_info->holes,
					'booked_players' => $teetime_info->player_count,
					'tee_sheet' => $tee_sheet_info->title
				);

				send_sendgrid(
					$customer_info->email,
					'Reservation Cancellation Details',
					$this->load->view("email_templates/reservation_made", $email_data, true),
					'booking@foreup.com',
					$course_info->name
				);
			}

		// Tee time already cancelled
		}else{
			$data['success'] = true;
		}

		$this->load->view('booking/cancelled', $data);
	}
}

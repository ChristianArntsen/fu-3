<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Billings extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('billings');
	}

	function index()
	{
            //if (!$this->permissions->is_super_admin())
              //  redirect('home', 'location');
		$config['base_url'] = site_url('billing/index');
		$config['total_rows'] = $this->Billing->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
		$this->pagination->initialize($config);
		
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_billings_manage_table($this->Billing->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('billing/manage',$data);
	}
	
	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Billing->find_item_info($item_number));
	}

	function search()
	{
		$search=$this->input->post('search');
		$data_rows=get_billings_manage_table_data_rows($this->Billing->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20),$this);
		echo $data_rows;
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Billing->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	function billing_search()
	{
        $suggestions = $this->Billing->get_billing_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}
	
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Billing->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_billing_data_row($this->Billing->get_info($item_id),$this);
		echo $data_row;
	}
	
	function get_info($item_id=-1)
	{
		echo json_encode($this->Billing->get_info($item_id));
	}
	
	function view($billing_id=-1)
	{
		$this->load->model('Credit_card');
		//$this->load->model('Course');
        $data['billing_info'] = $this->Billing->get_info($billing_id);
		$data['month_array'] = array(1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
		$data['day_array'] = array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18,19=>19,20=>20,21=>21,22=>22,23=>23,24=>24,25=>25,26=>26,27=>27,28=>28,29=>29,30=>30,31=>31);
		$course_info = $this->Course->get_info($data['billing_info']->course_id);
		$credit_cards = $this->Credit_card->get($data['billing_info']->course_id);
		$cc_array = array('' => '');
		foreach($credit_cards as $credit_card)
			$cc_array[$credit_card['credit_card_id']] = $credit_card['card_type'].' - '.$credit_card['masked_account'];
		$data['credit_card_list'] = $cc_array;
		$data['course_name'] = $course_info->name;
		$this->load->view("billing/form",$data);
	}
	function history($course_id = -1)
	{
		if ($course_id == -1)
			return;
		else
		{
			$data['course_info'] = $this->Course->get_info($course_id);
			$data['course_payments'] = $this->Billing->get_course_payments($course_id);
			//print_r($course_payments);
			$this->load->view('billing/history', $data);
		}
	}
	function manage_groups()
	{
		$data['groups']=$this->Billing->get_group_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('billing/manage_groups',$data);
	}
	function add_group() 
	{
		$group_label = $this->input->post("group_label");
		$group_type = $this->input->post("group_type");
		echo json_encode(array('success'=>true,'group_id'=>$this->Billing->add_group($group_label, $group_type)));
	}
	function delete_group($group_id) 
	{
		$this->Billing->delete_group($group_id);
		echo json_encode(array('success'=>true));
	}
	
	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Billing->get_info($item_id);
		$this->load->view("billing/inventory",$data);
	}
	
	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Billing->get_info($item_id);
		$this->load->view("billing/count_details",$data);
	} //------------------------------------------- Ramel

	function save($billing_id=-1)
	{
		$billing_data = array(
			'course_id'=>$this->input->post('course_id'),
			'contact_email'=>$this->input->post('contact_email'),
			'tax_name'=>$this->input->post('tax_name'),
			'tax_rate'=>$this->input->post('tax_rate'),
			'product'=>$this->input->post('product'),
			'start_date'=>$this->input->post('start_date'),
			'period_start'=>$this->input->post('period_start'),
			'period_end'=>$this->input->post('period_end'),
			'credit_card_id'=>$this->input->post('credit_card_id'),
			'annual'=>$this->input->post('annual'),
			'email_limit'=>$this->input->post('email_limit'),
			'text_limit'=>$this->input->post('text_limit'),
			'annual_amount'=>$this->input->post('annual_amount'),
			'annual_month'=>$this->input->post('annual_month'),
			'annual_day'=>$this->input->post('annual_day'),
			'monthly'=>$this->input->post('monthly'),
			'monthly_amount'=>$this->input->post('monthly_amount'),
			'monthly_day'=>$this->input->post('monthly_day'),
			'teetimes'=>$this->input->post('teetimes'),
			'teetimes_daily'=>$this->input->post('teetimes_daily'),
			'teetimes_weekly'=>$this->input->post('teetimes_weekly'),
			'teesheet_id'=>$this->input->post('teesheet_id'),
			'free'=>$this->input->post('free')
		);
		if($this->Billing->save($billing_data,$billing_id))
		{
			//New item
			if($billing_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$billing_data['name'],'course_id'=>$billing_data['billing_id']));
				$billing_id = $billing_data['billinge_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$billing_data['name'],'billing_id'=>$billing_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').$this->db->last_query().' '.
			$billing_data['name'],'billing_id'=>-1));
		}

	}
	
	
	function delete()
	{
		$items_to_delete=$this->input->post('ids');

		if($this->Billing->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}
	
	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 650;
	}
}
?>
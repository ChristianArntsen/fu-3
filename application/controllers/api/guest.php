<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Guest extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Api_data');
		$this->load->helper('email_helper');
	}
	
    function create_get()
    {
    	$first_name = trim($this->input->get('first_name'));
		$last_name = trim($this->input->get('last_name'));
		$email = $this->input->get('email');
		$phone_number = $this->input->get('phone_number');
		$zip = $this->input->get('zip');
		$course_id = ($this->input->get('course_id')?$this->input->get('course_id'):$this->input->get('golf_course_id'));							
		
		$errors = array();
		if (!$first_name || $first_name == '')
			$errors[] = 'Missing parameter: first_name';
		if (!$last_name || $last_name == '')
			$errors[] = 'Missing parameter: last_name';
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';
		else if (!valid_email($email))
			$errors[] = 'Invalid parameter: email';
		if (!$phone_number || $phone_number == '')
			$errors[] = 'Missing parameter: phone_number';
		else if (!preg_match('/^\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/', trim($phone_number)))
			$errors[] = 'Invalid parameter: phone_number';
		if (!$zip || $zip == '')
			$errors[] = 'Missing parameter: zip';
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: course_id';
		if (count($errors)>0){			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);
		}
		
        $person_data = array(
        //Required - start
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'email'=> $email,
		'phone_number'=>$phone_number,
		'zip'=>$zip,
		//Required - end
		'birthday'=>$this->input->get('birthday'),
		'address_1'=>$this->input->get('address'),
		'city'=>$this->input->get('city'),
		'state'=>$this->input->get('state'),
		'country'=>$this->input->get('country')
		);
		$customer_data=array(
			'api_id' => $this->rest->api_id,
			'course_id' => $course_id
		); 		

		$success = $this->Api_data->save_customer_info($person_data, $customer_data);		
		if ($success) {			
			$this->response(
				array(
					'status'=>201,
					'guest_id'=>$customer_data['person_id']					
				), 
				201
			);
		}
		else 
		{
			$this->response(
				array(
					'status'=>200,
					'message'=>'save incomplete',	
					'description'=>"guest information could not be saved at this time."			
				),
				200
			);
		}
    }
    
	function info_get()
	{
		$person_id = $this->input->get('guest_id');
		if (!$person_id)			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'guest_id is a required paramater',
					'code'=> 6040
				), 
				400
			);
		$course_id = $this->input->get('golf_course_id');
		$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		if (!$course_id)			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required paramater',
					'code'=> 6040
				), 
				400
			);
		
		$customer_info = $this->Api_data->get_customer_info($person_id, $this->rest->api_id, $course_id, $permissions['extended_permissions']);
		if ($customer_info->num_rows() > 0)
		{
			$customer_info = $customer_info->row_array();
			$customer_info['golf_course_id'] = $customer_info['course_id'];
			unset($customer_info['course_id']);
			$customer_info['guest_id'] = $customer_info['person_id'];
			unset($customer_info['person_id']);			
			$this->response(
					array(
						'status'=>200,
						'guest_info'=>$customer_info					
					), 
					200
				);
		}
		else {			
			$this->response(
				array(
					'status'=>200,
					'message'=>'No Information',	
					'description'=>"No information is available for the guest_id : {$person_id}"			
				),
				200
			);		
		}
		
	}
	
	function list_get()
	{		
		$course_id = $this->input->get('golf_course_id');
		$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		$offset = $this->input->get('offset');
		if (!$course_id)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		$customer_list = $this->Api_data->get_customer_list($course_id, $this->rest->api_id, $permissions['extended_permissions'], $offset);		
		if (count($customer_list) > 0){	
			$this->response(
				array(
					'status'=>200,
					'limit'=>200,
					'offset'=>$offset,
					'count'=>count($customer_list),
					'guests'=>$customer_list					
				), 
				200
			);
		}
		else{			
			$this->response(
				array(
					'status'=>200,
					'message'=>'No Information',	
					'description'=>"No information is available for guests of course_id : {$course_id}"			
				),
				200
			);
		}
	}
	
	function reservations_get()
	{
		$schedule_id = $this->input->get('schedule_id');
		$permissions = $this->Api_data->course_permissions(false, $schedule_id, $this->rest->api_id);
		if (!$permissions['extended_permissions'])
			$this->response(
				array(
					'status'=>400,
					'message'=>'Permission denied',
					'description'=>'your account does not have access to this API call',
					'code'=> 6040
				), 
				400
			);		
		
		$person_id = $this->input->get('guest_id');
		if (!$person_id)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'person_id is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		
		if (!$schedule_id)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'schedule_id is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		
		//$this->db->limit(2000);
		$results = $this->Api_data->get_reservations($person_id,$schedule_id);
		//print_r($results);
		$this->response(
			array(
				'status'=>200,
				'reservations'=>$results
			)
		);
	}
	
	function search_get()
	{
		$course_id = $this->input->get('golf_course_id');
		$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		if (!$permissions['extended_permissions'])
			$this->response(
				array(
					'status'=>400,
					'message'=>'Permission denied',
					'description'=>'your account does not have access to this API call',
					'code'=> 6040
				), 
				400
			);		
		
		if (!$course_id)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		$search = $this->input->get('value');
		if (!$search)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'value is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		$type = $this->input->get('type');
		if (!$type)
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'type is a required paramater',
					'code'=> 6040
				), 
				400
			);		
		$this->response(
			array(
				'status'=>200,
				'guests'=>$this->Api_data->get_customer_search_suggestions($search,25,$type,$course_id)
			)
		);
		
	}
	
	function update_get()
	{
		$course_id = $this->input->get('golf_course_id');
		$person_id = $this->input->get('guest_id');
		$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		$errors = array();
		
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: golf_course_id';
		if (!$person_id || $person_id == '')
			$errors[] = 'Missing parameter: guest_id';
		
		if (count($errors)>0){			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);
		}
		if (!$this->Api_data->customer_exists($person_id, $course_id, $this->rest->api_id, $permissions['extended_permissions']))
			$this->response(
				array(
					'status'=>400,
					'message'=>'Invalid guest_id',
					'description'=>'The guest_id does not exist for the specified course_id',
					'code'=> 6040
				), 
				400
			);
		//echo $this->db->last_query();
    	$first_name = trim($this->input->get('first_name'));
		$last_name = trim($this->input->get('last_name'));
		$email = $this->input->get('email');
		$phone_number = $this->input->get('phone_number');
		$zip = $this->input->get('zip');
		$birthday = $this->input->get('birthday');
		$address = $this->input->get('address');
		$city = $this->input->get('city');
		$state = $this->input->get('state');
		
		$customer_data=array(
		//	'api_id' => $this->rest->api_id
		//	'course_id' => $course_id
		); 
		
		$person_data = array();
		
		if ($person_id) $person_data['person_id'] = $person_id;
		if ($first_name) $person_data['first_name'] = $first_name;	
		if ($last_name) $person_data['last_name'] = $last_name;
		if ($email) $person_data['email'] = $email;
		if ($phone_number) $person_data['phone_number'] = $phone_number;
		if ($zip) $person_data['zip'] = $zip;
		if ($birthday) $person_data['birthday'] = $birthday;
		if ($address) $person_data['address_1'] = $address;
		if ($city) $person_data['city'] = $city;
		if ($state) $person_data['state'] = $state;				
		
		$success = $this->Api_data->save_customer_info($person_data, $customer_data, $person_id, $this->rest->api_id, $permissions['extended_permissions']);
		
		if ($success) {			
			$this->response(
				array(
					'status'=>200,
					'guest_id'=>(int)$person_id					
				), 
				200
			);
		}
		else 
		{			
			$this->response(
				array(
					'status'=>200,
					'message'=>'Not Saved',	
					'description'=>"The information could not be saved for guest_id : {$person_id}"			
				),
				200
			);
		}
	}

}

<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Golf_course extends REST_Controller
{
	function __construct() 
	{
		parent::__construct();	
		$this->load->model('Api_data');
	}
	
    function index_get()
    {
        $this->response(array('Possible requests',array('request'=>'areas','request'=>'list',array('required'=>array('area_id')),'info'=>array('required'=>array('course_id')),'rates')));
    }
	
	function areas_get()
	{
		$areas = $this->Api_data->get_course_areas();
		//echo $this->db->last_query();
		if ($areas->num_rows() > 0)
			$this->response(
				array(
					'status'=>200,
					'areas'=>$areas->result_array()					
				),
				200
			);
		else 
			$this->response(
				array(
					'status'=>200,
					'message'=>'no areas',	
					'description'=>'No areas are available'			
				),
				200
			);
	}
	
	function list_get()
	{
		$area_id = $this->input->get('area_id');
		if (!$area_id){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'area_id is a required parameter',
					'code'=> 6040
				), 
				400
				);
		}
		else {
			$course_list = $this->Api_data->get_course_list($area_id);
			if (count($course_list) > 0){											
				$this->response(
					array(
						'status'=>200,
						'golf_courses'=>$course_list					
					), 
					200
				);
			}
			else
			{				
				$this->response(
					array(
						'status'=>200,
						'message'=>'no courses',	
						'description'=>'No courses are available in this area'			
					),
					200
				);
			}
		}
	}
	
	function info_get()
	{
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id){			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required parameter',
					'code'=> 6040
				), 
				400
			);
		}
		else {
			$course_info = $this->Api_data->get_course_info($course_id);			
			if ($course_info->num_rows() > 0)
			{											
				$this->response(
					array(
						'status'=>200,
						'golf_course_info'=>$course_info->row_array()					
					), 
					200
				);
			}
			else {				
				$this->response(
					array(
						'status'=>200,
						'message'=>'no info',	
						'description'=>"No information is available for the course_id : {$course_id}"			
					),
					200
				);			
			}
		}
	}
	
	function links_get()
	{
		$course_id = $this->input->get('golf_course_id');
		$course_info = $this->Api_data->get_course_info($course_id)->row_array();			
		if (!$course_id){			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required parameter',
					'code'=> 6040
				), 
				400
			);
		}
		else {
			$course_links_list = $this->Api_data->get_course_links_list($course_id);
			if ($course_links_list->num_rows() > 0){				
				$this->response(
					array(
						'status'=>200,
						'golf_course_links'=>$course_links_list->result_array(),
						'private'=>$course_info['online_booking_protected'],
						'min_required_players'=>$course_info['min_required_players'],
						'min_required_holes'=>$course_info['min_required_holes'],
						'min_required_carts'=>$course_info['min_required_carts']					
					), 
					200
				);
			}
			else {
				$this->response(
					array(
						'status'=>200,
						'message'=>'no schedules',	
						'description'=>"No schedules are available for the course_id : {$course_id}"			
					),
					200
				);			
			}
		}
		
	}
	
	function app_data_get()
	{
		$course_id = $this->input->get('golf_course_id');
		if (!$course_id)
		{
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing parameter',
					'description'=>'golf_course_id is a required parameter',
					'code'=> 6040
				), 
				400
			);
		}			
		else 
		{
			$course_info = $this->Api_data->get_course_app_data($course_id);
				
			if ($course_info)
			{			
				$this->response(
					array(
						'status'=>200,
						'golf_course_info'=>$course_info					
					), 
					200
				);
			}
			else 
			{				
				$this->response(
					array(
						'status'=>200,
						'message'=>'no app data',	
						'description'=>"No app data is available for the course_id : {$course_id}"			
					),
					200
				);		
			}
		}
			
	}
}
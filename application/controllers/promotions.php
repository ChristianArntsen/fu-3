<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Promotions extends Secure_area implements iData_controller
{
  function __construct()
	{
		parent::__construct('promotions');

    $this->load->model('Promotion');
    $this->load->model('Item');
	}
  
  public function index()
  {
		$config['base_url'] = site_url('promotions/index');
		$config['total_rows'] = $this->Promotion->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);
		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_promotions_manage_table($this->Promotion->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('promotions/manage',$data);

  }
	public function search($offset = 0)
  {
    	$search=$this->input->post('search');
		$data_rows=get_promotions_manage_table_data_rows($this->Promotion->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
        $config['base_url'] = site_url('promotions/index');
        $config['total_rows'] = $this->Promotion->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20; 
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);

  }
  public function suggest()
  {
    $suggestions = $this->Promotion->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
  }
  public function get_needed_coupon_data($coupon_type_id)
  {
     return $this->Promotion->get_coupon_data($coupon_type_id);
  }
	public function get_row()
	{
		$promotion_id = $this->input->post('row_id');
		$data_row = get_promotions_data_row($this->Promotion->get_info($promotion_id),$this);
		echo $data_row;
	}
  
  public function view($promotion_id=-1)
  {
    $data = array();
    $new = false;
    $promotion_future_id = 0;
    if ($promotion_id == -1)
    {
        $this->load->model('promotion');
        $promotion_future_id = $this->promotion->get_last_coupon_id();
        $promotion_future_id++;
        $new = true;
    }
    else
    {
        $new = false;
    }
    
    $obj = $this->Promotion->get_info($promotion_id);
    $obj->new = $new;
    //log_message('error', 'VIEW ' . $obj->bogo);
    $obj->limit = ($obj->limit == 0) ? '' : $obj->limit;
    $obj->buy_quantity = ($obj->buy_quantity == 0) ? '' : $obj->buy_quantity;
    $obj->get_quantity = ($obj->get_quantity == 0) ? '' : $obj->get_quantity;
    $obj->min_purchase = ($obj->min_purchase == 0) ? '' : $obj->min_purchase;
    $obj->item_id = ($obj->item_id == 0) ? '' : $obj->item_id;
    
    if ($promotion_id == -1)
    {
        $obj->approximate_id = $promotion_future_id;
        $obj->id = -1;
    }
    
    $data['promotion_info'] = $obj;
   
    $data['days'] = array(
        ''        => '',
        'monday'  => 'Monday',
        'tuesday'  => 'Tuesday',
        'wednesday'  => 'Wednesday',
        'thursday'  => 'Thursday',
        'friday'  => 'Friday',
        'saturday'  => 'Staurday',
        'sunday'  => 'Sunday'
    );

    $range = array();
    $range[''] = '';
    $range['anytime'] = 'Anytime';

    
    
    $count = 1;
    while($count < 12)
    {
      $r = sprintf("%02d",$count);
      $r = round($r);
      $range[ $r . ':00am'] = $r . ':00am';
      $count++;
    }
    $range['12:00pm'] = '12:00pm';
    
    $count2 = 1;
    while($count2 < 12)
    {
      $r = sprintf("%02d",$count2);
      $r = round($r);
      $range[ $r . ':00pm'] = $r . ':00pm';
      $count2++;
    }
    $range['12:00am'] = '12:00am';
    $data['range'] = $range;

    $course = $this->Course->get_info($this->session->userdata('course_id'));
    
    $data['course'] = $course->name;

    $this->load->view("promotions/form",$data);
  }
  public function view_coupon()
  {
      
      $data['promotion_id'] = $this->input->post('promotion_id');
      $data['expiration_date'] = $this->input->post('expiration_date');
      
      $data['min_purchase'] = $this->input->post('min_purchase');
      $data['valid_from'] = $this->input->post('valid_from');
      $data['valid_to'] = $this->input->post('valid_to');
      $data['Sunday'] = $this->input->post('Sunday');
      $data['Monday'] = $this->input->post('Monday');
      $data['Tuesday'] = $this->input->post('Tuesday');
      $data['Wednesday'] = $this->input->post('Wednesday');
      $data['Thursday'] = $this->input->post('Thursday');
      $data['Friday'] = $this->input->post('Friday');
      $data['Saturday'] = $this->input->post('Saturday');
      $data['bogo'] = $this->input->post('bogo');
      $data['amount_type'] = $this->input->post('amount_type');
      $data['amount'] = $this->input->post('amount');
      $data['buy_quantity'] = $this->input->post('buy_quantity');
      $data['get_quantity'] = $this->input->post('get_quantity');
      $data['rules'] = $this->input->post('rules');
      $data['item_id'] = $this->input->post('item_id');
      $data['discount_type'] = $this->input->post('discount_type');
      $data['category_type'] = $this->input->post('category_type');
      $data['department_type'] = $this->input->post('department_type');
      $data['valid_for'] = $this->input->post('valid_for');
      $data['item_name'] = $this->Item->get_item_name($this->input->post('item_id'));
      $data['course'] = $this->Item->get_course_name($this->config->item('course_id'));
      echo $this->load->view('promotions/coupon', $data, true);
  }        
  
  public function save($promotion_id=-1)
  {
                
                $coupon_limit = 1;
                $amount = '';
                $expiration = '';
                $limit = 1;
                //log_message('error', 'Promotion Id: ' . $promotion_id);
                $item_id = $this->input->post('item_id');
                $amount_type = $this->input->post('promotion_amount_type');
                if ($this->input->post('coupon') === 'bogo')
                {
                    if ($this->input->post('choose_free_or_percent') !== 'buy_get_free')
                    {
                        $amount_type = '%';                        
                    }
                    else
                    {
                        $amount_type = 'FREE';
                    }
                }
                if ($this->input->post('limit') == 2)
                {
                    $coupon_limit = 'unlimited';
                    $limit = 0;
                }
                if ($this->input->post('coupon') === 'discount')
                {
                    $amount = $this->input->post('promotion_amount');
                }
                else
                {
                    $amount = $this->input->post('promotion_bogo_discount');
                }
                if ($this->input->post('promotion_expiration_date') !== '')
                {
                   $expiration = date('Y-m-d', strtotime($this->input->post('promotion_expiration_date')));
                }
                else
                {
                    $expiration = '4000-01-01';
                }
               $valid_days = $this->input->post('valid_promotion_days');
               $sunday_checked = 0;
               $monday_checked = 0;
               $tuesday_checked = 0;
               $wednesday_checked = 0;
               $thursday_checked = 0;
               $friday_checked = 0;
               $saturday_checked = 0;
               foreach ($valid_days as $day_checked)
               {
                   switch ($day_checked)
                   {
                       case('Sunday'):
                           $sunday_checked = 1;
                       break;
                       case('Monday'):
                           $monday_checked = 1;
                       break;
                       case('Tuesday'):
                           $tuesday_checked = 1;
                       break;
                       case('Wednesday'):
                           $wednesday_checked = 1;
                       break;
                       case('Thursday'):
                           $thursday_checked = 1;
                       break;
                       case('Friday'):
                           $friday_checked = 1;
                       break;
                       case('Saturday'):
                           $saturday_checked = 1;
                       break;
                   }
               }
    $promotion_data = array(
        'course_id' => $this->session->userdata('course_id'),
        'bogo'=> $this->input->post('coupon'),
        'expiration_date' => $expiration,
        'name'=> $this->input->post('promotion_name'),
        'amount_type'=> $amount_type,
        'amount'=> $amount,
        'valid_for'=> $this->input->post('promotion_valid_for'),
        'Sunday'=> $sunday_checked,
        'Monday'=> $monday_checked,
        'Tuesday'=> $tuesday_checked,
        'Wednesday'=> $wednesday_checked,
        'Thursday'=> $thursday_checked,
        'Friday'=> $friday_checked,
        'Saturday'=> $saturday_checked,
        'valid_between_from'=> $this->input->post('promotions_valid_between_start'),
        'valid_between_to'=> $this->input->post('promotions_valid_between_end'),
        'buy_quantity'=> $this->input->post('promotion_buy'),
        'get_quantity'=> $this->input->post('promotion_get'),
        'min_purchase'=> $this->input->post('promotion_min_purchase'),
        'additional_details'=> $this->input->post('promotion_additional_details'),
        'limit'=>$limit,
        'coupon_limit'=>$coupon_limit,
        'item_id'=>$item_id,
        'discount_type'=>$this->input->post('discount_type'),
        'category_type'=>$this->input->post('category_type'),
        'subcategory_type'=>$this->input->post('subcategory_type'),
        'department_type'=>$this->input->post('department_type')
    );

    $rules = '';
    if(!empty($promotion_data['min_purchase']))
    {
      $rules .= 'With minimum  purchase of ' . $promotion_data['min_purchase'];
    }
    if(!empty($promotion_data['limit']) && $promotion_data['limit'] == 1) 
        $rules .= " Limit {$promotion_data['limit']} per Coupon. ";
    else
    {
        $rules .= "Unlimited uses per Coupon";
    }

    
    if(!empty($promotion_data['additional_details']))
    {
      $rules .= $promotion_data['additional_details'];
    }

    $promotion_data['rules'] = trim($rules);
    
    if($this->Promotion->save($promotion_data, $promotion_id))
    {
      //New promotion
			if($promotion_id==-1)
			{
                                echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_adding').' '.
				strip_tags($promotion_data['name']),'id'=>$promotion_data['id']));
				$promotion_id = $promotion_data['id'];
			}
			else //previous promotion
			{
				echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_updating').' '.
				strip_tags($promotion_data['name']),'id'=>$promotion_id));
			}
    }
    else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('promotions_error_adding_updating').' '.
			$promotion_data['name'],'id'=>-1));
		}
    }
  
  public function delete()
  {
    $promotions_to_delete=$this->input->post('ids');

		if($this->Promotion->delete_list($promotions_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('promotions_successful_deleted').' '.
			count($promotions_to_delete).' '.lang('promotions_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('promotions_cannot_be_deleted')));
		}
  }

  public function get_form_width()
  {
    return 1024;
  }
  public function get_coupon_matches()
  {
      $array = array();
      if ($this->input->get('term') !== '')
      {
          
          $array = $this->Promotion->get_coupon_name_suggestions($this->input->get('term'), 75);
          
          
      }
      echo json_encode($array);
  }
  
  public function create_UPC()
  {
      $promotion_id = $this->input->post('value');
      $data_row = $this->Promotion->get_info($promotion_id);
      echo "<img id='BC' src='".site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}&text=CP{$promotion_info->discount_type}{$promotion_id}&scale=1' />" . 
      form_hidden('send_BC',site_url('barcode')."?barcode=CP{$promotion_info->discount_type}{$promotion_id}&text=CP{$promotion_info->discount_type}{$promotion_id}&scale=1' />");
  }
  public function create_individual_UPC($customer_id)
  {
      
  }
}
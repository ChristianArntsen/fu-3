<?php
// Client Booking Engine
class Be extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->db->trans_off();

		$this->load->model('teesheet');
	    $this->load->model('teetime');
        $this->load->model('course');
        $this->load->model('item');
		$this->load->model('billing');
		$this->load->model('Green_fee');
        $this->load->model('Fee');
        $this->load->model('user');
        $this->load->model('customer');
		$this->load->library('booking_lib');
		$this->load->model('schedule');
		$this->load->model('_reservation');
		$this->load->model('Track');
		$this->load->model('Booking_class');
		$this->load->library('Mobile_Detect');
    }
	function index($course_id = '', $schedule_id = '')
	{
		$this->reservations($course_id, $schedule_id);
	}
	function reservations($course_id = '', $schedule_id = '')
	{
		// If user is logged in with one course, then they transfer to another course, log them out
		if ($this->user->is_logged_in() && $course_id != '' && $this->session->userdata('course_id') != $course_id)
		{
			$this->session->set_userdata('course_id', $course_id);
			$this->user->logout();
		}
		// Assign course
		if ($course_id != ''){
			$this->session->set_userdata('course_id', $course_id);

		}else if($this->session->userdata('course_id')){
		    $course_id = $this->session->userdata('course_id');
		}


		$course_info = $this->course->get_info($course_id);
		if (!$course_info->reservations)
		{
			// REDIRECTING TO THE NEW ONLINE BOOKING
			redirect("booking/index/$course_id");
			return;
		}

		// IF ANYONE HAPPENED TO GET LOGGED IN WITH NO ACCOUNT BY ACCIDENT, WE'RE GOING TO LOG THEM OUT
		if ($this->user->is_logged_in() && $this->session->userdata('customer_id') == -1)
		{
			$this->user->logout();
		}
		$booking_class_id = $this->session->userdata('booking_class_id');
		$booking_class_info = $this->session->userdata('booking_class_info');

		// If user is not logged in, and the course requires it, then redirect them to login page
		if (($course_info->online_booking_protected || ($booking_class_id && $booking_class_info->online_booking_protected)) && !$this->user->is_logged_in())
		{
			$data = array();
			$data['course_info'] = $course_info;
			$this->load->view("customers/login/login", $data);
		}
		// If course does not require log in, or the user is logged in
		else
		{
	        $course_info->foreup_discount_percent = floor($course_info->foreup_discount_percent);

			if ($course_info->reservations)
			{
				if ($schedule_id != '')
				{
					$this->session->set_userdata('schedule_id', $schedule_id);
			        $schedule_obj = $this->schedule->get_info($schedule_id);
				}
				else
				{
					$schedule_obj = $this->schedule->get_all(1)->result();
					$schedule_id = $schedule_obj[0]->schedule_id;
					$this->session->set_userdata('schedule_id', $schedule_id);
				}

				if ($this->input->post('teesheetMenu')){
			    	$this->schedule->switch_tee_sheet('', true);
				}
				$booking_track = $this->schedule->get_booking_track($schedule_id);
				$has_online_booking = $booking_track['track_id'];
				$this->session->set_userdata('track_id', $booking_track['track_id']);
	        }
			else
			{
				$teesheet_id = $schedule_id;
				if ($teesheet_id != '')
				{
					$this->session->set_userdata('teesheet_id', $teesheet_id);
			        $teesheet_obj = $this->teesheet->get_info($teesheet_id);
				}
				else
				{
					$teesheet_obj = $this->teesheet->get_all(1)->result();
					$teesheet_id = $teesheet_obj[0]->teesheet_id;
					$this->session->set_userdata('teesheet_id', $teesheet_id);
				}
				if ($this->input->post('teesheetMenu'))
			    	$this->teesheet->switch_tee_sheet('', true);
				$has_online_booking = $this->teesheet->has_online_booking($course_id);
	        }

			if ($has_online_booking) {
	            //Grab course data, and save some to session
	            $this->session->set_userdata('iframe', $this->input->get('iframe'));
	            $this->session->set_userdata('course_name', $course_info->name);
				$this->session->set_userdata('zip', $course_info->zip);
	            $this->session->set_userdata('course_email', $course_info->email);
	            $this->session->set_userdata('course_phone', $course_info->phone);
	            $this->session->set_userdata('is_simulator', $course_info->simulator);
	            $this->session->set_userdata('reservations', $course_info->reservations);
	            date_default_timezone_set($course_info->timezone);

	            //Grab teesheet data and save some to session
	            $teesheet_info = $course_info->reservations ? $this->schedule->get_info($schedule_id) : $this->teesheet->get_info($teesheet_id);
				$booking_classes = $this->Booking_class->get_all($teesheet_id ? $teesheet_id : $schedule_id);
				if (!$booking_class_id && count($booking_classes) > 0)
				{
					redirect('be/select_booking_class');
					exit();
				}
				else if ($booking_class_id)
				{
					$teesheet_info->holes = $booking_class_info->holes;
					$teesheet_info->days_in_booking_window = $booking_class_info->days_in_booking_window;
					$teesheet_info->minimum_players = $booking_class_info->minimum_players;
					$teesheet_info->limit_holes = $booking_class_info->limit_holes;
					$teesheet_info->booking_carts = $booking_class_info->booking_carts;
					$teesheet_info->online_open_time = $booking_class_info->online_open_time;
					$teesheet_info->online_close_time = $booking_class_info->online_close_time;
				}
				$this->session->set_userdata('holes', $teesheet_info->holes);
	            $this->session->set_userdata('frontnine', $teesheet_info->frontnine);
	            $this->session->set_userdata('increment', $teesheet_info->increment);
	            $this->session->set_userdata('days_out', $teesheet_info->days_out);
	            $this->session->set_userdata('days_in_booking_window', $teesheet_info->days_in_booking_window);
	            $this->session->set_userdata('minimum_players', $teesheet_info->minimum_players);
	            $this->session->set_userdata('limit_holes', $teesheet_info->limit_holes);
				$this->session->set_userdata('booking_carts', $teesheet_info->booking_carts);
				$this->session->set_userdata('require_credit_card', $teesheet_info->require_credit_card);
				$this->session->set_userdata('no_show_policy', $this->config->item('no_show_policy'));
	            $result_array = array();//$this->teesheet->get_available_teetimes();

	            if ($course_info->reservations)
				{
					$this->schedule->load_booking_settings();
					$filters = $this->schedule->apply_booking_filters();
					$schedule_id = $this->session->userdata('schedule_id');
					$teesheet_menu = $this->schedule->get_tee_sheet_menu($schedule_id, true);
			     	$available_deals = $this->schedule->get_available_deals($schedule_id);
				}
				else
				{
					$this->teesheet->load_booking_settings();
					$filters = $this->teesheet->apply_booking_filters();
					if ($this->session->userdata('is_simulator'))
			        	$result_array = $this->teesheet->get_available_simulator_teetimes($course_id);
					$teesheet_id = $this->session->userdata('teesheet_id');
					$teesheet_menu = $this->teesheet->get_tee_sheet_menu($teesheet_id, true);
			     	$available_deals = $this->teesheet->get_available_deals($teesheet_id);
				}

				//TODO:Correct this so course_id is not being passed in as teesheet_id
				$detect = new Mobile_Detect();
			    $data = array(
					'teesheet_id'=>$course_id,
					'available_deals'=>$available_deals,
					'teetimes'=>$result_array,
					'course_info'=>$course_info,
					'teesheet_info'=>$teesheet_info,
					'teesheet_menu'=>$teesheet_menu,
					'filters'=>$filters,
					'date'=>'',
					'mobile_browser'=>($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS())
				);
				if ($course_info->reservations){
					$data['tracks'] = $this->Track->get_all($schedule_id)->result_array();
				}

				if ($this->session->userdata('is_simulator'))
				{
					$data['basket_info'] = $this->booking_lib->get_basket_info();
					$data['time_amount'] = number_format($data['basket_info']['items_in_basket'] * $schedule_obj->increment/60 ,2).' Hour(s)';
					$data['basket_items'] = $this->booking_lib->get_basket();
					$data['basket_html'] = $this->booking_lib->get_basket_html();
					$this->load->view('be/be_simulator', $data);
				}
				else
				{
					$this->load->view("be/be_updated", $data);
				}
	        }
	        else {
	            $this->load->view("be/booking_unavailable");
	        }
        }
	}

	function load_card_auth_window(){
		$course_id = $this->session->userdata('course_id');
		$person_id = $this->session->userdata('customer_id');
		$viewData = '';

		// Store reservation information in session
		$reservation = array(
			'start' => $this->input->post('start'),
			'holes' => $this->input->post('holes'),
			'players' => $this->input->post('players'),
			'carts' => $this->input->post('carts')
		);

		$base_url = trim($this->input->post('return_url'),'/');
		if(empty($base_url)){
			$base_url = 'be/capture_card_auth/'.$person_id;
		}
		$url = site_url($base_url);

		if(!empty($reservation['start'])){
			$this->session->set_userdata('current_reservation', $reservation);
		}

		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('isSave', 'true')
					   ->send();
			if ($session->id)
			{
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				$data = array(
					'user_message'=>$user_message,
					'return_code'=>$return_code,
					'session'=>$session,
					'url' => $url,
					'tee_time_card' => true);
				if(!empty($reservation['start'])){
					$data['no_show_policy'] = $this->config->item('no_show_policy');
				}
				$viewData = $this->load->view('be/ets_capture_form', $data, true);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$viewData = $this->load->view('sales/cant_load', $data, true);
			}
		}

		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
			$this->load->library('Hosted_checkout_2');

			$HC = new Hosted_checkout_2();
			$HC->set_default_swipe('Manual');
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			$HC->set_response_urls($base_url, 'credit_cards/process_cancelled');

			$initialize_results = $HC->initialize_payment('1.00','0.00','PreAuth','POS','Recurring');

			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'PreAuth','frequency'=>'Recurring'));
				$this->session->set_userdata('invoice', $invoice);

				$user_message = (string)$initialize_results->Message;
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>'$1.00 authorization in order to save the credit card', 'return_code'=>$return_code, 'url'=>$url);
				if(!empty($reservation['start'])){
					$data['no_show_policy'] = $this->config->item('no_show_policy');
				}
				$viewData = $this->load->view('be/mercury_capture_form', $data, true);
			}
		}

		echo $viewData;
	}

	function capture_card_auth($person_id){
		$this->load->model('Customer_credit_card');
		$credit_card_id = $this->Customer_credit_card->capture_card($person_id);

		if(!empty($credit_card_id)){
			$_POST = $this->session->userdata('current_reservation');
			$_POST['credit_card_id'] = $credit_card_id;

			$response = $this->book_tee_time(true);

			echo "<script>window.parent.reservations.show_booked_page('".$response['teetime_id']."');</script>";
		}else{
			echo "<script>window.parent.reservations.book_tee_time();</script>";
		}
	}

	function select_booking_class($booking_class_id = false) {
		$schedule_id = $this->session->userdata('schedule_id');
		$teesheet_id = $this->session->userdata('teesheet_id');
		$course_id = $this->session->userdata('course_id');
		$this->load->model('Booking_class');

		if ($booking_class_id)
		{
			$this->Booking_class->load_info($booking_class_id, $teesheet_id ? $teesheet_id : $schedule_id);
			redirect("be/reservations/$course_id/$teesheet_id");
			//print_r($this->session->userdata('booking_class_info'));
		}
		else
		{
			$data['booking_classes'] = $this->Booking_class->get_all($teesheet_id ? $teesheet_id : $schedule_id);
			$this->load->view('be/booking_classes', $data);
		}
	}

	function get_available_teetimes($is_api_request = false) {
        $time_range = $this->input->post('time_range');
        $teesheet_id = $this->input->post('teesheet_id');
        $holes = $this->input->post('holes');
        $available_spots = $this->input->post('available_spots');
		$tab_index = $this->input->post('tab_index');
		if ($this->session->userdata('reservations') || $this->session->userdata('schedule_id'))
		{
			$this->schedule->load_booking_settings();
			$this->schedule->apply_booking_filters($holes, $available_spots, $time_range, '', '', '', '', $tab_index);
			echo json_encode($this->schedule->get_available_teetimes());
		}
		else
		{
			$this->teesheet->load_booking_settings();
			$this->teesheet->apply_booking_filters($holes, $available_spots, $time_range, '', '', '', '', $tab_index);
			echo json_encode($this->teesheet->get_available_teetimes($is_api_request));
		}
    }

	function get_available_reservations() {
        $time_range = $this->input->post('time_range');
        $teesheet_id = $this->input->post('schedule_id');
		$tab_index = $this->input->post('tab_index');
		$this->schedule->load_booking_settings();
        $this->schedule->apply_booking_filters($holes, $available_spots, $time_range, '', '', '', '', $tab_index);
        echo json_encode($this->schedule->get_available_reservations());
    }

	function add_simulator_time()
	{
		$teesheet_id = ($this->session->userdata('reservations'))?$this->session->userdata('schedule_id'):$this->session->userdata('teesheet_id');
		$schedule_info = $this->schedule->get_info($teesheet_id);
		$basket_info = $this->booking_lib->get_basket_info();
		$minutes = $basket_info['items_in_basket'] * $schedule_info->increment;
		$minute_limit = $schedule_info->online_time_limit;
		if ($minutes + $schedule_info->increment <= $minute_limit)
		{
			$val = $this->input->post('time_info');
			$substring = substr($val, 4);
			$track_id = substr($substring, 0, strpos($substring,'_'));
			$track = $this->Track->get_info($track_id);//->result_array();
			$sim = substr($val, 0, 6);
			$time_string = substr($substring, strpos($substring,'_')+1);
			$year = substr($time_string, 0, 4);
			$month = (int)substr($time_string, 4, 2)+1;
			$day = substr($time_string, 6,2);
			$time = substr($time_string, 8);
			$hour = substr($time_string, 8,2);
			$min = substr($time_string, 10,2);
			$item_number = $this->session->userdata('course_id').'_1';
			$item_id = $this->item->get_item_id($item_number);
			$price_category = ($this->session->userdata('reservations')?$this->Fee->get_price_category($time):$this->Green_fee->get_price_category($time));
			$price_result = ($this->session->userdata('reservations')?$this->Fee->get_info():$this->Green_fee->get_info());
			$price = $price_result[$teesheet_id][$item_number]->$price_category;
			$description = $track->title;//"$val";// : $time_string y $year m $month d $day h $hour m $min";
		 	$this->booking_lib->add_item_to_basket($item_id, 1, 0, $price, $description, null, $price_category, null, $val);
			echo json_encode(array('prices'=>$price_result[$teesheet_id],'time'=>$time,'success'=>true, 'total_time'=>$minutes + $schedule_info->increment,'basket_info'=>$this->booking_lib->get_basket_info($price_category), 'basket_items'=>$this->booking_lib->get_basket(), 'basket_html' => $this->booking_lib->get_basket_html()));
		}
		else {
			echo json_encode(array('success'=>false,'message'=>'Only '.floor($schedule_info->online_time_limit/60).' hours allowed per reservation'));
		}
	}

	function open_payment_window($window = 'eCOM', $tran_type = 'Sale', $frequency = 'OneTime', $total_amount='1.00',$tax_amount='0.00',$override=false) {
		if ($override)
			$this->session->set_userdata('override', 1);
		$this->load->library('Hosted_checkout');

		$HC = new Hosted_checkout();

		$HC->initialize_payment($total_amount,$tax_amount,$tran_type,$window,$frequency);
	}

	function open_foreup_payment_window($teetime_id, $players, $carts)
	{
		$this->session->set_userdata('teetime_to_purchase', $teetime_id);
		$this->session->set_userdata('players_to_purchase', $players);
		$this->session->set_userdata('carts_to_purchase', $carts);

		$teetime_info = $this->teetime->get_info($teetime_id);

		$course_id = $this->session->userdata('course_id');
		$course_info = $course_info = $this->course->get_info($course_id);;
        $teesheet_id = $this->session->userdata('teesheet_id');
        $time_string = mktime(0,0,0, (int)substr($date, 4, 2)+1, substr($date, 6, 2), substr($date, 0, 4));

		$prices = ($this->session->userdata('reservations')?$this->Fee->get_info():$this->Green_fee->get_info());
		$day_of_week = date('D', strtotime($teetime_info->start+1000000));
		$time = substr($teetime_info->start, 8);
		$price_indexes = $this->session->userdata('reservations') ? $this->schedule->determine_price_indexes($time, $teetime_info->holes, $day_of_week) : $this->teesheet->determine_price_indexes($time, $teetime_info->holes, $day_of_week);
		$booking_class_info = $this->session->userdata('booking_class_info');
		$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];
		$teesheet_id = $teetime_info->teesheet_id;
		$course_id = $this->session->userdata('course_id');

		if($this->config->item('seasonal_pricing') == 1){
			$price_class = 0;
			if($booking_class_info){
				$price_class = $booking_class_info->price_class;
			}
			$this->load->model('Pricing');
			$filter_date = substr($teetime_info->start + 1000000, 0, 8);
			$filter_date = DateTime::createFromFormat('Ymd', $filter_date);
			$filter_date = $filter_date->format('Y-m-d');

			$single_price = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $teetime_info->holes, false);
			$single_cart_price = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $teetime_info->holes, true);

		}else{
			$single_price = number_format($prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category, 2);
			$single_cart_price = number_format($prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category,2);
		}

		$include_tax = (int) $this->config->item('include_tax_online_booking');
		if($include_tax == 1){
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate();
			$greenfee_tax_rate = (float) $greenfee_tax_rate[0]['percent'];
			$cartfee_tax_rate = $this->Item->get_cart_tax_rate();
			$cartfee_tax_rate = (float) $cartfee_tax_rate[0]['percent'];

			$single_price = number_format( round((($greenfee_tax_rate / 100) * $single_price) +$single_price, 2), 2);
			$single_cart_price = number_format( round((($cartfee_tax_rate / 100) * $single_cart_price ) + $single_cart_price , 2), 2);
		}

		$single_price = ($carts == 1)?$single_price+$single_cart_price:$single_price;
		$total_amount = $players * $single_price * (100 - $course_info->foreup_discount_percent) / 100;
		$tax_amount = 0;

		//Fortesting
		$teetime_info->subtotal = number_format($players * $single_price, 2);
		$teetime_info->discount = number_format($players * $single_price *  $course_info->foreup_discount_percent / 100, 2);
		$teetime_info->total = number_format($total_amount, 2);
		$this->session->set_userdata('teetime_info', $teetime_info);
		$available_for_purchase = $this->billing->have_sellable_teetimes($teetime_info->start);

		if ($available_for_purchase){
			$this->open_payment_window('FeCOM', 'Sale', 'OneTime', $teetime_info->total, number_format($tax_amount,2));
		}else{
			$this->load->view('be/sorry_not_available');
		}
	}

	function payment_made() {
		$this->load->library('Hosted_checkout');
		$HC = new Hosted_checkout();
		$teetime_info = $this->session->userdata('teetime_info');
		$available_for_purchase = ($this->session->userdata('override') ? true : $this->billing->have_sellable_teetimes($teetime_info->start));
		$email_data = array(
			'course_name'=>$this->session->userdata('course_name'),
			'booked_date'=>date('n/j/y', strtotime($teetime_info->start+1000000)),
			'booked_time'=>date('g:ia', strtotime($teetime_info->start+1000000)),
			'booked_holes'=>$teetime_info->holes,
			'booked_players'=>$this->session->userdata('players_to_purchase'),
			'booked_carts'=>($this->session->userdata('carts_to_purchase') == 1)?$this->session->userdata('players_to_purchase'):0,
			'customer_name'=>$this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
			'customer_email'=>$this->session->userdata('customer_email'),
			'card_type'=>$this->session->userdata('card_type'),
			'current_date'=>date('g:ia n/j/y T'),
			'confirmation_number'=>substr($this->session->userdata('teetime_to_purchase'), 5),
			'subtotal'=>$teetime_info->subtotal,
			'discount'=>$teetime_info->discount,
			'total'=>$teetime_info->total
		);
		$data = $HC->payment_made($email_data);

		if ($available_for_purchase && $data['success'] === true)
		{
			if (!$this->session->userdata('override'))
			{
				//Save teetime in the database
				$success = $this->teetime->update_purchased_teetime();
				//Email receipt
				if ($success)
				{
					send_sendgrid(
						$this->session->userdata('customer_email'),
						'Tee Time Receipt/Confirmation',
						$this->load->view("email_templates/purchase_receipt",$email_data, true),
						'booking@foreup.com',//$this->session->userdata('course_email'),
						'ForeUP'
					);
				}
			}
			$this->session->unset_userdata('override');
			$this->load->view('be/payment_made.php', $data);
		}
		else if ($data['success'] === true)
		{
			$HC->cancel_payment($data['invoice_id']);
			$this->load->view('be/sorry_not_available', array('payment_window'=>true));
		}
	}
	function terms_and_conditions() {
		$this->load->view('be/terms_and_conditions');
	}
	function empty_basket()
	{
		$this->booking_lib->empty_cart();
		$this->booking_lib->empty_basket();
		echo json_encode(array('prices'=>$price_result,'time'=>$time,'success'=>true, 'basket_info'=>$this->booking_lib->get_basket_info(), 'basket_items'=>$this->booking_lib->get_basket(), 'basket_html' => $this->booking_lib->get_basket_html()));
	}
	function remove_item($line)
	{
		$this->booking_lib->delete_item_from_basket($line);
		$this->reservations();
	}
	function remove_simulator_time()
	{
		$teesheet_id = ($this->session->userdata('reservations'))?$this->session->userdata('schedule_id'):$this->session->userdata('teesheet_id');
		$schedule_info = $this->schedule->get_info($teesheet_id);
		$basket_info = $this->booking_lib->get_basket_info();
		$minutes = $basket_info['items_in_basket'] * $schedule_info->increment;
		$time_info = $this->input->post('time_info');
		$line = $this->booking_lib->get_item_line($time_info);
		if ($line)
		{
			$this->booking_lib->delete_item_from_basket($line);
			echo json_encode(array('success'=>true, 'basket_info'=>$this->booking_lib->get_basket_info(), 'total_time'=>$minutes - $schedule_info->increment, 'basket_items'=>$this->booking_lib->get_basket(), 'basket_html' => $this->booking_lib->get_basket_html()));
		}
		else {
			echo json_encode(array('success'=>false,'message'=>'Only 2 hours allowed per reservation'));
		}

	}
	/*
	Loads the employee edit form
	*/
	function reserve($course_id = '', $date = '', $time = '', $spots = '', $spots_available = '', $holes = '', $w_cart = '', $wo_cart = '')
	{
		$course_info = '';
		if ($course_id != '') {
			$course_info = $this->course->get_info($course_id);
		}
		$teesheet_id = ($this->session->userdata('reservations')?$this->session->userdata('schedule_id'):$this->session->userdata('teesheet_id'));
		$time_string = mktime(0,0,0, (int)substr($date, 4, 2)+1, substr($date, 6, 2), substr($date, 0, 4));

		$t_z = '';
		if ($time < 1000){
			$t_z = '0';
		}
		$start = $date.$t_z.(int)$time;
		$end  = $start + $this->session->userdata('increment');
		if ($end%100 > 59)
			$end += 40;
		$prices = ($this->session->userdata('reservations')?$this->Fee->get_info():$this->Green_fee->get_info());
		$day_of_week = date('D', strtotime($date+100));
		$booking_class_info = $this->session->userdata('booking_class_info');

		if($this->config->item('seasonal_pricing') == 1){
			$this->load->model('Pricing');
			$filter_date = DateTime::createFromFormat('Ymd', $date + 100);
			$filter_date = $filter_date->format('Y-m-d');
			$price_class = false;

			$single_price = (float) $this->Pricing->get_price($teesheet_id, $booking_class_info->price_class, $filter_date, $time, $holes, false);
			$single_cart_price = (float) $this->Pricing->get_price($teesheet_id, $booking_class_info->price_class, $filter_date, $time, $holes, true);

		}else{
			$price_indexes = $this->session->userdata('reservations') ? $this->schedule->determine_price_indexes($time, $holes, $day_of_week) : $this->teesheet->determine_price_indexes($time, $holes, $day_of_week);
			$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];

			$single_price = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category;
			$single_cart_price = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category;
		}

		$include_tax = (int) $this->config->item('include_tax_online_booking');
		if($include_tax){
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate();
			$greenfee_tax_rate = (float) $greenfee_tax_rate[0]['percent'];
			$cartfee_tax_rate = $this->Item->get_cart_tax_rate();
			$cartfee_tax_rate = (float) $cartfee_tax_rate[0]['percent'];

			$single_price = (float) round((($greenfee_tax_rate / 100) * $single_price) + $single_price, 2);
			$single_cart_price = (float) round((($cartfee_tax_rate / 100) * $single_cart_price) + $single_cart_price, 2);
		}

		$credit_cards = false;
		$credit_card_window = false;

		// If a credit card is required to make reservation
		if($this->session->userdata('require_credit_card') == 1){
			if($this->user->is_logged_in()){
				$this->load->model('Customer_credit_card');
				$credit_cards = $this->Customer_credit_card->get($this->session->userdata('customer_id'));
			}

			$reservation_data = array(
				'start' => $start,
				'holes' => $holes,
				'players' => $spots,
				'carts' => 0
			);

			$this->session->set_userdata('current_reservation', $reservation_data);
		}

		$data = array (
			'logged_in'=>$this->user->is_logged_in(),
			'course_name'=>$course_info->name,
			'customer_credit_cards' => $credit_cards,
			'require_credit_card' => $this->session->userdata('require_credit_card'),
			'no_show_policy' => $this->session->userdata('no_show_policy'),
			'credit_card_window' => $credit_card_window,
			'course_id'=>$course_id,
			'date'=>$date,
			'start'=>$start,
			'date_string'=>date("D, M j Y", $time_string),
			'time'=>$this->session->userdata('reservations') ? $this->schedule->format_time($time) : $this->teesheet->format_time($time),
			'spots'=>$spots,
			'spots_available'=>$spots_available,
			'holes'=>$holes,
			'w_cart'=> number_format($single_cart_price + $single_price, 2),
			'wo_cart'=>number_format($single_price, 2),
			'booking_carts'=>$this->session->userdata('booking_carts')
		);

		$this->load->view("be/form", $data);
	}
	function confirm_cancellation($teetime_id, $person_id)
	{
		$teetimes = explode('|', urldecode($teetime_id));
		$data = array (
			'teetime_id' => $teetime_id,
			'teetime_info'=>array(),
			'person_id'=>$person_id,
			'reservations_module'=>false
		);
		foreach ($teetimes as $teetime)
		{
			$tee_time_info = $this->teetime->get_info($teetime);
			if ($tee_time_info->TTID != '')
				$data['teetime_info'][] = $tee_time_info;
		}
		if (count($data['teetime_info']) == 0)
		{
			foreach ($teetimes as $teetime)
				$data['teetime_info'][] = $this->_reservation->get_info($teetime);
			$data['reservations_module'] = true;
		}
		$this->load->view('be/confirm_cancellation', $data);
	}
	function cancel_reservation($teetime_id, $person_id)
	{
		$teetimes = explode('|', urldecode($teetime_id));
		$success = true;
		$simulator_times = array('front'=>array(),'back'=>array());
		$this->db->trans_start();
		if (!$this->input->post('reservations_module'))
		{
			foreach($teetimes as $teetime){
				$success = ($this->teetime->delete($teetime, $person_id))?$success:false;
					$teetime_info = $this->teetime->get_info($teetime);
				$simulator_times[$teetime_info->side][] = (array) $teetime_info;
			}
			$course_info = $this->Course->get_info_from_teesheet_id($teetime_info->teesheet_id);
		}
		else
		{
			$reservation_ids = $this->input->post('reservation_id');
			foreach($reservation_ids as $reservation_id){
				$success = ($this->_reservation->delete($reservation_id, $person_id))?$success:false;
				$teetime_info = $this->_reservation->get_info($reservation_id);
				$simulator_times[$teetime_info->side][] = (array) $teetime_info;
			}
			$course_info = $this->Course->get_info_from_track_id($teetime_info->track_id);
		}
		$this->db->trans_complete();
		$data = array('success'=>$success);
		$customer_info = $this->Customer->get_info($person_id);
		$email_data = array(
			'cancelled'=>true,
			'person_id'=>$person_id,
			'course_name'=>$course_info->name,
			'course_phone'=>$course_info->phone,
			'first_name'=>$customer_info->first_name,
			'booked_date'=>date('n/j/y', strtotime($teetime_info->start+1000000)),
			'booked_time'=>date('g:ia', strtotime($teetime_info->start+1000000)),
			'booked_holes'=>$teetime_info->holes,
			'booked_players'=>$teetime_info->player_count
		);
        if (count($teetimes) > 1){
			$email_data['simulator_times'] = $simulator_times;
		}

		if ($data['success'])
		{
			send_sendgrid(
				$customer_info->email,
				'Reservation Cancellation Details',
				$this->load->view("email_templates/reservation_made",$email_data, true),
				($this->session->userdata('course_email')?$this->session->userdata('course_email'):'booking@foreup.com'),//$this->session->userdata('course_email'),
				$course_info->name
			);

			$json = array('success'=>true, 'message'=>'Reservation Cancelled');
		}else{
			$json = array('success'=>false, 'message'=>'Error cancelling your reservation');
		}
		if ($this->input->get('json'))
		{
			echo json_encode($json);
		}
		else
		{
			$this->load->view('be/reservation_cancelled', $data);
		}
	}
    function unsubscribe($person_id, $course_id)
	{
		$data = array(
			'unsubscribed'=>$this->Customer->unsubscribe($person_id, $course_id),
			'email'=>'email'
			);

		$this->load->view('be/unsubscribed', $data);
	}
    /*
	Loads the simulator reservation form
	*/
	function reserve_simulator()
	{

        $course_info = $this->course->get_info($this->session->userdata('course_id'));

        $data = array (
            'logged_in'=>$this->user->is_logged_in(),
            'course_name'=>$course_info->name,
            'course_id'=>$course_id,
            'basket_items'=>$this->booking_lib->get_basket(),
            'basket_info'=>$this->booking_lib->get_basket_info()
        );

		$this->load->view("be/form_simulator", $data);
	}

    /*
		Inserts/updates a customer/user
	*/
	function register($customer_id = -1)
	{
		$person_data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('email'),
			'phone_number'=>$this->input->post('phone_number')
		);

		$customer_data = array(
			'account_number'=>$this->input->post('account_number')=='' ? null:$this->input->post('account_number'),
            'course_id'=>$this->session->userdata('course_id'),
            'username' => $this->input->post('email'),
            'password' => md5($this->input->post('password'))
		);

		$giftcards = array();
		$groups = array('ForeUP Online Booking');
		$passes = array();

		if($this->user->username_exists($user_data['username']))
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_registering'),'person_id'=>-1));
		}
		else if($this->customer->save($person_data, $customer_data, $customer_id, $giftcards, $groups, $passes, true))
		{

			// If a credit card is required to make reservation, load available cards this member has
			$choose_credit_card_window = false;
			if($this->session->userdata('require_credit_card') == 1){
				$this->load->model('Customer_credit_card');
				$credit_cards = $this->Customer_credit_card->get($this->session->userdata('customer_id'));

				if(!empty($credit_cards)){
					$data['customer_credit_cards'] = $credit_cards;
					$data['no_show_policy'] = $this->config->item('no_show_policy');
					$choose_credit_card_window = $this->load->view('be/choose_reservation_card', $data, true);
				}
			}

			//New customer
			if($customer_id == -1)
			{
				$this->user->login($this->input->post('email'), $this->input->post('password'));
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_registering').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id'],
				'choose_credit_card_window'=>$choose_credit_card_window));
			}
			else //previous customer
			{
				$this->user->login($this->input->post('email'), $this->input->post('password'));
				echo json_encode(array('success'=>true,'message'=>lang('customers_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id,
				'choose_credit_card_window'=>$choose_credit_card_window));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('customers_error_registering'),'person_id'=>-1));
		}
	}
	function make_5_day_weather()
	{
		if ($this->session->userdata('reservations'))
		{
			echo json_encode(array('weather'=>$this->schedule->make_5_day_weather()));
		}
		else
		{
			echo json_encode(array('weather'=>$this->teesheet->make_5_day_weather()));
		}

	}
    function login($username = '', $password = '', $courseId = '') {

        if ($username == ''){
            $username = $this->input->post('username');
		}
        if ($password == ''){
            $password = $this->input->post('password');
		}
        if ($courseId == ''){
            $courseId = $this->session->userdata('course_id');
		}

        if($this->user->login($username, $password)){

			// Add user us a customer to this course (if they are not already)
			if(!empty($courseId)){

				$user = $this->user->get_by_username($username);
				if(!empty($user->id)){
					$this->user->add_to_course($user->id, $courseId);
				}
			}

			// If a credit card is required to make reservation, load available cards this member has
			$choose_credit_card_window = false;
			if($this->session->userdata('require_credit_card') == 1){
				$this->load->model('Customer_credit_card');
				$credit_cards = $this->Customer_credit_card->get($this->session->userdata('customer_id'));

				if(!empty($credit_cards)){
					$data['customer_credit_cards'] = $credit_cards;
					$data['no_show_policy'] = $this->config->item('no_show_policy');
					$choose_credit_card_window = $this->load->view('be/choose_reservation_card', $data, true);
				}
			}

            echo json_encode(array('success'=>true,'message'=>lang('customers_logged_in'), 'choose_credit_card_window'=>$choose_credit_card_window));
        }else{
            echo json_encode(array('success'=>false,'message'=>lang('customers_error_logging_in')));
		}
    }
    function book_tee_time($return_output = false){

    	if (!$this->user->is_logged_in())
		{
			echo json_encode(array('success'=>false, 'message'=>'Please log in to reserve tee time.'));
			return;
		}
    	$status = array('success'=>false);

        $start = $this->input->post('start');
        $end  = $start + $this->session->userdata('increment');

    	if($this->session->userdata('increment') == 7.5){
			$this->load->model('Teetime');
			$end = $this->Teetime->adjust_7_8_time($end);
		}

    	if ($end%100 > 59){
            $end += 40;
		}
        $players = $this->input->post('players');

        $carts = $this->input->post('carts');
        if(empty($carts) || $carts == 'no_cart'){
			$carts = 0;
		}else{
			$carts = $players;
		}

        if ($this->permissions->course_has_module('reservations')) {
        	$event_data = array(
				'track_id'=>$this->session->userdata('track_id'),//$this->input->post('teesheet_id'),
				'start'=>$start,
				'end'=>$end,
				'side'=>'front',
				'allDay'=>'false',
				'holes'=>$this->input->post('holes'),
				'player_count'=>$players ? $players : 4,
				'type'=>'teetime',
				'carts'=>$carts,
				'person_id'=>$this->session->userdata('customer_id'),
				'person_name'=>$this->session->userdata('last_name').', '.$this->session->userdata('first_name'),
				'title'=>$this->session->userdata('last_name'),
				'booking_source'=>'online',
				'booker_id'=>$this->session->userdata('customer_id'),
				'details'=>'Reserved using online booking @ '.date('g:ia n/j T')
			);
		}
		else {
			$event_data = array(
				'teesheet_id'=>$this->session->userdata('teesheet_id'),//$this->input->post('teesheet_id'),
				'start'=>$start,
				'end'=>$end,
				'side'=>'front',
				'allDay'=>'false',
				'holes'=>$this->input->post('holes'),
				'player_count'=>$players ? $players : 4,
				'type'=>'teetime',
				'carts'=>$carts,
				'person_id'=>$this->session->userdata('customer_id'),
				'person_name'=>$this->session->userdata('last_name').', '.$this->session->userdata('first_name'),
				'title'=>$this->session->userdata('last_name'),
				'booking_source'=>'online',
				'booker_id'=>$this->session->userdata('customer_id'),
				'details'=>'Reserved using online booking @ '.date('g:ia n/j T')
			);

		}

		$credit_card_id = $this->input->post('credit_card_id');
		if(!empty($credit_card_id)){
			$event_data['credit_card_id'] = (int) $credit_card_id;
		}

		if($this->session->userdata('require_credit_card') && empty($credit_card_id)){
			$status = array('success'=>false,'message'=>'Error reserving teetime, a valid credit card is required');
			if($return_output){
				return $status;
			}else{
				echo json_encode($status);
			}
		}

		$email_data = array(
			'person_id'=>$this->session->userdata('customer_id'),
			'course_name'=>$this->session->userdata('course_name'),
			'course_phone'=>$this->session->userdata('course_phone'),
			'first_name'=>$this->session->userdata('first_name'),
			'booked_date'=>date('n/j/y', strtotime($start+1000000)),
			'booked_time'=>date('g:ia', strtotime($start+1000000)),
			'booked_holes'=>$this->input->post('holes'),
			'booked_players'=>$players ? $players : 4
		);

		$response = array();
		if ($this->permissions->course_has_module('reservations')) {
			$save_response = $this->_reservation->save($event_data,false,$response,true);
		}
		else {
			$save_response = $this->teetime->save($event_data,false,$response,true);
		}
		//print_r($save_response);
		$save_sql = $this->db->last_query();
		if ($save_response['success'] && $save_response['success'] !== 'reround_error' && $save_response['success'] !== 'error')
		{
			$status = array('success'=>true,'teetimes'=>$response);

			// If person is not a customer of course yet, add them (teesheet_id is actually course_id)
			$this->user->add_to_course($this->session->userdata('customer_id'), $this->session->userdata('teesheet_id'));

		}else if($save_response['success'] === 'reround_error'){
			$status = array('success'=>false,'message'=>'Sorry, booking 18 holes for this time is not available. The reround time is not available on the back 9.');
		}else{
			$status = array('success'=>false,'message'=>'Sorry, that teetime is no longer available');
		}
		$email_data['TTID'] = ($this->permissions->course_has_module('reservations')?$event_data['reservation_id']:$event_data['TTID']);
        $tee_sheet_id = $this->session->userdata('teesheet_id');
        if(!empty($tee_sheet_id)) {
            $teesheet_data = $this->teesheet->get_info($tee_sheet_id, true);
            $tee_sheet_title = $teesheet_data->title;
            if(empty($tee_sheet_title)) {
                $tee_sheet_title = '';
            }
            $email_data['tee_sheet'] = $tee_sheet_title;
        }
		if ($status['success'])
		{
			$this->teetime->send_confirmation_email(
				$this->session->userdata('customer_email'),
				'Tee Time Reservation Details',
				$email_data,
				($this->session->userdata('course_email')?$this->session->userdata('course_email'):'booking@foreup.com'),
				$this->session->userdata('course_name')
			);
		}
		$status['teetime_id']=($this->permissions->course_has_module('reservations')?$event_data['reservation_id']:$event_data['TTID']);
		$status['sql'] = $save_sql;
		$status['event_data'] = $event_data;
		$status['email_data'] = $email_data;

		if($return_output){
			return $status;
		}else{
			echo json_encode($status);
		}
    }
    function book_simulator() {
    	$this->db->trans_start();
    	$status = array('success'=>false);
    	$items = $this->booking_lib->get_basket();
		if ($this->session->userdata('reservations'))
		{
			foreach ($items as $item)
			{
				$substring = substr($item['sim_number'], 4);
				$track_id = substr($substring, 0, strpos($substring,'_'));

				$time_string = substr($substring, strpos($substring,'_')+1);
				$start = $time_string;
				$end = $time_string + $this->session->userdata('increment');

				if($this->session->userdata('increment') == 7.5){
					$this->load->model('Teetime');
					$end = $this->Teetime->adjust_7_8_time($end);
				}

				if ($end % 100 > 59){
		            $end += 40;
				}
				$substr = substr($item['sim_number'],4);
				$track_id = substr($substr, 0, strpos($substr, '_'));

				$event_data = array(
					'start'=>$start,
					'end'=>$end,
					'track_id'=>$track_id,
					'allDay'=>'false',
					'holes'=>'',
					'player_count'=>'',
					'type'=>'reservation',
					'carts'=>'',
					'person_id'=>$this->session->userdata('customer_id'),
					'person_name'=>$this->session->userdata('last_name').', '.$this->session->userdata('first_name'),
					'title'=>$this->session->userdata('last_name'),
					'booking_source'=>'online',
					'booker_id'=>$this->session->userdata('customer_id'),
					'details'=>'Reserved using online booking @ '.date('g:ia n/j T')
				);
				// if times are back to back, let's just adjust the end time of the original, otherwise, we'll create a new one
				if (isset($timeslot_array[$event_data['track_id']][$event_data['start']]) && $timeslot_array[$event_data['track_id']][$event_data['start']]['track_id'] == $event_data['track_id'])
				{
					$delete_index = $event_data['start'];
					$event_data['start'] = $timeslot_array[$event_data['track_id']][$event_data['start']]['start'];
					unset($timeslot_array[$event_data['track_id']][$delete_index]);
				}

				$timeslot_array[$event_data['track_id']][$event_data['end']] = $event_data;
			}

			$response = array();

			//run through the front
			$simulator_ttids = array();
			foreach($timeslot_array as $track_id => $ts)
			{
				foreach($ts as $timeslot)
				{
					$this->_reservation->save($timeslot,false,$response);
					$status = array('success'=>true,'reservations'=>$response);
					$simulator_ttids[] = $timeslot['reservation_id'];
				}
			}

	        $email_data = array(
				'link_url'=>'http://foreup.com',
				'error_link'=>'http://foreup.com',
				'unsubscribe_link'=>'http://foreup.com',
				'header_image'=>'',
				'course_name'=>$this->session->userdata('course_name'),
				'course_phone'=>$this->session->userdata('course_phone'),
				'first_name'=>$this->session->userdata('first_name'),
				'reservation_time'=>date('g:ia n/j/y T', strtotime($start+1000000)),
				'person_id'=>$this->session->userdata('customer_id'),
				'simulator_times'=>$timeslot_array,
				'reservation'=>implode('|', $simulator_ttids)
			);
	        $this->booking_lib->empty_basket();
			if ($status['success'])
			{
				//Send confirmation email
				$this->_reservation->send_confirmation_email(
					$this->session->userdata('customer_email'),
					'Reservation Details',
					$email_data,
					'booking@foreup.com',//$this->session->userdata('course_email'),
					$this->session->userdata('course_name')
				);

				// If person is not a customer of course yet, add them (teesheet_id is actually course_id)
				$this->user->add_to_course($this->session->userdata('customer_id'), $this->session->userdata('course_id'));

			}
			$status['reservation_ids'] = '?rid[]='.implode('&rid[]=', $simulator_ttids);
			$this->db->trans_complete();
			$status['success'] = $this->db->trans_status();
	        echo json_encode($status);
		}
		else
		{
			$this->db->trans_start();
			$timeslot_array = array('front'=>array(),'back'=>array());
			foreach ($items as $item)
			{
	        	$start = substr($item['description'], 6);
				$end = substr($item['description'], 6) + $this->session->userdata('increment');
				if($this->session->userdata('increment') == 7.5){
					$this->load->model('Teetime');
					$end = $this->Teetime->adjust_7_8_time($end);
				}
				if ($end % 100 > 59){
		            $end += 40;
				}
				$side = (substr($item['description'], 4, 1) == 1)?'front':'back';

				$event_data = array(
					'teesheet_id'=>$this->session->userdata('teesheet_id'),//$this->input->post('teesheet_id'),
					'start'=>$start,
					'end'=>$end,
					'side'=>$side,
					'allDay'=>'false',
					'details'=>'',
					'holes'=>'',
					'player_count'=>'',
					'type'=>'teetime',
					'carts'=>'',
					'person_id'=>$this->session->userdata('customer_id'),
					'person_name'=>$this->session->userdata('last_name').', '.$this->session->userdata('first_name'),
					'title'=>$this->session->userdata('last_name'),
					'booking_source'=>'online',
					'booker_id'=>$this->session->userdata('customer_id'),
					'details'=>'Reserved using online booking @ '.date('g:ia n/j T')
				);

				// if times are back to back, let's just adjust the end time of the original, otherwise, we'll create a new one
				if (isset($timeslot_array[$event_data['side']][$event_data['start']]) && $timeslot_array[$event_data['side']][$event_data['start']]['side'] == $event_data['side'])
				{
					$delete_index = $event_data['start'];
					$event_data['start'] = $timeslot_array[$event_data['side']][$event_data['start']]['start'];
					unset($timeslot_array[$event_data['side']][$delete_index]);
				}

				$timeslot_array[$event_data['side']][$event_data['end']] = $event_data;
			}
	    	$response = array();
			//run through the front
			$simulator_ttids = array();
			foreach($timeslot_array['front'] as $timeslot)
			{
				$this->teetime->save($timeslot,false,$response);
				$status = array('success'=>true,'teetimes'=>$response);
				$simulator_ttids[] = $timeslot['TTID'];
			}
			//run through the back
			foreach($timeslot_array['back'] as $timeslot)
			{
				$this->teetime->save($timeslot,false,$response);
				$status = array('success'=>true,'teetimes'=>$response);
				$simulator_ttids[] = $timeslot['TTID'];
			}

	        $email_data = array(
				'link_url'=>'http://foreup.com',
				'error_link'=>'http://foreup.com',
				'unsubscribe_link'=>'http://foreup.com',
				'header_image'=>'',
				'course_name'=>$this->session->userdata('course_name'),
				'course_phone'=>$this->session->userdata('course_phone'),
				'first_name'=>$this->session->userdata('first_name'),
				'tee_time'=>date('g:ia n/j/y T', strtotime($start+1000000)),
				'person_id'=>$this->session->userdata('customer_id'),
				'simulator_times'=>$timeslot_array,
				'TTID'=>implode('|', $simulator_ttids)
			);
	        $this->booking_lib->empty_basket();
			if ($status['success'])
			{
				//Send confirmation email
				$this->teetime->send_confirmation_email(
					$this->session->userdata('customer_email'),
					'Tee Time Reservation Details',
					$email_data,
					'booking@foreup.com',//$this->session->userdata('course_email'),
					$this->session->userdata('course_name')
				);
			}
	        $this->db->trans_complete();
			$status['success'] = $this->db->trans_status();
	        echo json_encode($status);
	    }
    }
    function thank_you()
    {
        $this->load->view('be/thank_you');
    }
	function finish()
	{
		$this->load->view('be/finish');
	}
    function booked($teetime_id)
    {
    	$data = array();
    	$course_id = $this->session->userdata('course_id');
		$data['course_info'] = $this->course->get_info($course_id);
		if ($this->session->userdata('reservations'))
		{
	    	$data['teetime_info'] = $this->_reservation->get_detailed_info($teetime_id);

			$prices = $this->Fee->get_info();
			$day_of_week = date('D', strtotime($data['teetime_info']->start+1000000));
			$time = substr($data['teetime_info']->start, 8);
			$price_indexes = $this->schedule->determine_price_indexes($time, $data['teetime_info']->holes, $day_of_week);
			$booking_class_info = $this->session->userdata('booking_class_info');
			$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];
			$schedule_id = $data['teetime_info']->schedule_id;

			$data['single_price'] = (float) $prices["{$schedule_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category;
			$data['single_cart_price'] = (float) $prices["{$schedule_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category;
	        $data['available_for_purchase'] = $this->billing->have_sellable_teetimes($data['reservation_info']->start);
		}
		else
		{
	    	$data['teetime_info'] = $this->teetime->get_info($teetime_id);

			$prices = $this->Green_fee->get_info();
			$day_of_week = date('D', strtotime($data['teetime_info']->start+1000000));
			$time = substr($data['teetime_info']->start, 8);
			$teesheet_id = $data['teetime_info']->teesheet_id;

			if($this->config->item('seasonal_pricing') == 1){
				$price_class = 0;
				if($booking_class_info){
					$price_class = $booking_class_info->price_class;
				}
				$this->load->model('Pricing');
				$filter_date = substr($data['teetime_info']->start + 1000000, 0, 8);
				$filter_date = DateTime::createFromFormat('Ymd', $filter_date);
				$filter_date = $filter_date->format('Y-m-d');

				$data['single_price'] = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $data['teetime_info']->holes, false);
				$data['single_cart_price'] = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $data['teetime_info']->holes, true);

			}else{
				$price_indexes = $this->teesheet->determine_price_indexes($time, $data['teetime_info']->holes, $day_of_week);
				$booking_class_info = $this->session->userdata('booking_class_info');
				$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];

				$data['single_price'] = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category;
				$data['single_cart_price'] = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category;
			}

	        $data['available_for_purchase'] = $this->billing->have_sellable_teetimes($data['teetime_info']->start);
		}

		$include_tax = (int) $this->config->item('include_tax_online_booking');
		if($include_tax){
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate();
			$greenfee_tax_rate = (float) $greenfee_tax_rate[0]['percent'];
			$cartfee_tax_rate = $this->Item->get_cart_tax_rate();
			$cartfee_tax_rate = (float) $cartfee_tax_rate[0]['percent'];

			$data['single_price'] = number_format( round((($greenfee_tax_rate / 100) * $data['single_price']) + $data['single_price'], 2), 2);
			$data['single_cart_price'] = number_format( round((($cartfee_tax_rate / 100) * $data['single_cart_price']) + $data['single_cart_price'], 2), 2);
		}
		$data['single_with_cart'] = number_format($data['single_price'] + $data['single_cart_price'], 2);

		$data['booked'] = true;
        $this->load->view('be/booked', $data);

    }
    function reservation_booked()
    {
    	$reservation_ids = $this->input->get('rid');
    	$data['schedule_info'] = $this->schedule->get_info($this->session->userdata('schedule_id'));

		$data['count'] = 0;
		foreach($reservation_ids as $reservation_id)
		{
			$res = $this->_reservation->get_detailed_info($reservation_id);
			$start = strtotime($res->start+1000000);
			$end = strtotime($res->end+1000000);
			$diff = ($end - $start)/60/$data['schedule_info']->increment;

			if ($diff){
				$data['count'] += $diff;
			}

			$data['reservations'][] = $res;
		}

		$day_of_week = date('D', strtotime($data['schedule_info']->start+1000000));
		$time = substr($data['schedule_info']->start, 8);

		$course_id = $this->session->userdata('course_id');
		$item_number = $course_id.'_1';
		$schedule_id = $this->session->userdata('schedule_id');

		$data['total_price'] = $this->booking_lib->get_simulator_amount_due($data['count']);
		$data['course_info'] = $this->course->get_info($course_id);
		$data['booked'] = true;
        $this->load->view('be/reservation_booked', $data);
    }
    function pay_now($teetime_id)
    {
    	$data['teetime_info'] = $this->teetime->get_info($teetime_id);

		$prices = ($this->session->userdata('reservations')?$this->Fee->get_info():$this->Green_fee->get_info());
		$day_of_week = date('D', strtotime($data['teetime_info']->start+1000000));
		$time = substr($data['teetime_info']->start, 8);
		$booking_class_info = $this->session->userdata('booking_class_info');
		$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];
		$teesheet_id = $data['teetime_info']->teesheet_id;
		$course_id = $this->session->userdata('course_id');
		$data['course_info'] = $this->course->get_info($course_id);

		if($this->config->item('seasonal_pricing') == 1){
			$price_class = 0;
			if($booking_class_info){
				$price_class = $booking_class_info->price_class;
			}
			$this->load->model('Pricing');
			$filter_date = substr($data['teetime_info']->start + 1000000, 0, 8);
			$filter_date = DateTime::createFromFormat('Ymd', $filter_date);
			$filter_date = $filter_date->format('Y-m-d');

			$data['single_price'] = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $data['teetime_info']->holes, false);
			$data['single_cart_price'] = (float) $this->Pricing->get_price($teesheet_id, $price_class, $filter_date, $time, $data['teetime_info']->holes, true);

		}else{
			$price_indexes = $this->session->userdata('reservations') ? $this->schedule->determine_price_indexes($time, $data['teetime_info']->holes, $day_of_week) : $this->teesheet->determine_price_indexes($time, $data['teetime_info']->holes, $day_of_week);
			$price_category = $booking_class_info ? $booking_class_info->price_class : $price_indexes['price_category'];

			$data['single_price'] = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['green_fee_index']}"]->$price_category;
			$data['single_cart_price'] = (float) $prices["{$teesheet_id}"]["{$course_id}_{$price_indexes['cart_price_index']}"]->$price_category;
		}

		$include_tax = (int) $this->config->item('include_tax_online_booking');
		if($include_tax == 1){
			$greenfee_tax_rate = $this->Item->get_teetime_tax_rate();
			$greenfee_tax_rate = (float) $greenfee_tax_rate[0]['percent'];
			$cartfee_tax_rate = $this->Item->get_cart_tax_rate();
			$cartfee_tax_rate = (float) $cartfee_tax_rate[0]['percent'];

			$data['single_price'] = number_format( round((($greenfee_tax_rate / 100) * $data['single_price']) + $data['single_price'], 2), 2);
			$data['single_cart_price'] = number_format( round((($cartfee_tax_rate / 100) * $data['single_cart_price']) + $data['single_cart_price'], 2), 2);
		}

		$data['available_for_purchase'] = $this->billing->have_sellable_teetimes($data['teetime_info']->start);
		$data['player_count'] = ($data['teetime_info']->player_count < 3)?2:$data['teetime_info']->player_count;
		$this->load->view('be/pay_now', $data);
    }

    function account_overview()
    {
		$courseId = $this->session->userdata('course_id');

		if(!$this->user->is_logged_in()){
			redirect('be/reservations/'.$courseId);
			exit();
		}
		$personId = $this->session->userdata('customer_id');

		$data['account_info'] = $this->user->get_info($personId);
		$data['giftcards'] = $this->user->get_giftcards($personId);
		$data['teetimes'] = $this->user->get_teetimes($personId, 'upcoming', $courseId);
		$data['customer_id'] = $personId;

		$this->load->view('be/account_overview', $data);
	}

	function logout()
	{
		$this->user->logout();
	}
}
?>

var Invoice = Backbone.Model.extend({
	idAttribute: 'invoice_id',
	defaults: {
		invoice_number: ''
	}
});

var InvoiceCollection = Backbone.Collection.extend({
	model: Invoice,
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.course.get('course_id') + '/users/invoices'
	}
});

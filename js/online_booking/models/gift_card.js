var GiftCard = Backbone.Model.extend({
	idAttribute: 'giftcard_id',
	defaults: {
		giftcard_number: '',
		details: '',
		expiration_date: '',
		value: 0.00
	}
});

var GiftCardCollection = Backbone.Collection.extend({
	model: GiftCard
});	

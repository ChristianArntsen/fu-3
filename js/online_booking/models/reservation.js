var Reservation = Backbone.Model.extend({
	idAttribute: 'teetime_id',

	defaults: {
		players: 4,
		holes: 18,
		carts: false,
		green_fee: 0.00,
		cart_fee: false,
		total: 0.00,
		purchased: false,
		pay_players: 0,
		pay_carts: 0,
		pay_total: 0,
		pay_subtotal: 0,
		paid_player_count: 0,
		discount_percent: 0,
		discount: 0
	},

	initialize: function(){
		this.on('change:players change:carts', this.calculateTotal);
		this.on('change:pay_players change:pay_carts', this.calculatePurchaseTotal);
		this.on('error', this.triggerError, this);
		this.calculateTotal();
	},
	
	calculateTotal: function(event){
		
		this.set({'pay_carts': this.get('carts'), 'pay_players': this.get('players')});
		var total = 0;
		var greenfee_total = this.get('players') * this.get('green_fee');
		var cart_total = 0;
		
		if(this.get('carts') == true || this.get('carts') > 0){
			cart_total = this.get('players') * this.get('cart_fee');
		}

		total = _.round(greenfee_total + cart_total);
		this.set('total', total);
	},
	
	calculatePurchaseTotal: function(event){
		
		var total = 0;
		var subtotal = 0;
		var discount = 0;
		var cart_total = 0;	
		
		if(this.get('pay_carts') == true || this.get('pay_carts') > 0){
			cart_total = this.get('pay_players') * this.get('cart_fee');
		}
		var greenfee_total = this.get('pay_players') * this.get('green_fee');

		subtotal = _.round(greenfee_total + cart_total);
		discount = _.round(subtotal * (parseFloat(this.get('discount_percent')) / 100));
		total = _.round(subtotal - discount);
		
		this.set({'pay_total': total, 'pay_subtotal': subtotal, 'discount': discount});
	},	
	
	validate: function(){
		var schedule = App.data.schedules.findWhere({'selected':true});
		
		// Check if user is logged in
		if(!App.data.user.get('logged_in')){
			return 'log_in_required';
		}		
		
		// Check if credit card is required and has been passed
		if(schedule.get('require_credit_card') == 1 && !this.get('credit_card_id')){
			return 'credit_card_required';
		}	
	},
	
	triggerError: function(model, response, options){
		App.vent.trigger('error', model, response);
	},
	
	canPurchase: function(){
		return this.get('can_purchase') && this.get('players') >= 2;
	}
});

var ReservationCollection = Backbone.Collection.extend({
	
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.filters.get('course_id') + '/users/reservations';
	},
	model: Reservation,
	
	initialize: function(){
		this.on('sync', this.reservationMade, this);
		this.on('error', this.reservationError, this);
	},
	
	reservationMade: function(reservation, response, options){
		// Remove/modify time on teetime page
		var selectedTime = App.data.times.get(reservation.get('time'));
		
		if(selectedTime){
			var availableSpots = selectedTime.get('available_spots');
			var spotsLeft = availableSpots - reservation.get('players');
		
			if(spotsLeft <= 0){
				App.data.times.remove(selectedTime);
			}else{
				selectedTime.set('available_spots', spotsLeft);
			}
		}

		App.vent.trigger('reservation', reservation);
	}	
});	

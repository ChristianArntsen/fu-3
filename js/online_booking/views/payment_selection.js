var PaymentSelectionView = Backbone.View.extend({
	
	id: 'payment_selection',
	className: 'modal-dialog',
	template: _.template( $('#template_payment_selection').html() ),
	
	events: {
		'click button.continue': 'continueReservation',	
		'click li': 'selectMethod'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		App.data.last_reservation = this.model;
	},
	
    close: function() {
		this.remove();
    },
	
	selectMethod: function(e){
		$(e.currentTarget).find('label').addClass('selected');
		$(e.currentTarget).siblings('li').find('label').removeClass('selected');
	},
	
    render: function() {
		this.model.set('discount_percent', App.data.course.get('foreup_discount_percent'));
		this.model.calculateTotal();
		this.model.calculatePurchaseTotal();
				
		var attr = this.model.attributes;
		var html = this.template(attr);
		this.$el.html(html);
		$('#modal').html(this.el);
		ga('send', 'event', 'Online Booking', 'View Payment Method Form', App.data.course.getCurrentSchedule());

		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},    
    
    continueReservation: function(e){
		var selection = this.$el.find('input[name="payment_method"]:checked').val();
		if(selection == 'online'){
			this.payNow();
		}else{
			this.payAtCourse();
		}
	},
    
	// Pre-pay and reserve tee time
    payNow: function(){	
		var view = this;
		
		if(this.model && this.model.isValid()){
			this.model.set({'is_paying': true});
			this.$el.find('button.continue').button('loading');			
			
			// Book reservation, upon booking, show pre-payment form
			App.data.user.get('reservations').create(this.model.attributes, {wait: true, success: function(model, response, options){
				
				var teetime_id = response.teetime_id;
				var players = model.get('pay_players');
				
				// If user can select carts
				if(App.settings.booking_carts == 1){
					var carts = model.get('pay_carts');			
				
				// If cart price built in to green fee
				}else if(App.settings.booking_carts == 2){
					var carts = true;
				
				// If carts can not be reserved online 
				}else{
					var carts = false;
				}				
				
				view.$el.find('.modal-body').html('<div id="mercury_loader"><h2>Loading form...</h2>'+
					'<div class="progress progress-striped active">' +
					'<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>' +
						'</div></div><iframe id="mercury_iframe" onLoad="$(\'#mercury_loader\').remove();" ' +
						'src="' +SITE_URL+ 'purchase_reservation_window/' +teetime_id+ '?carts=' +carts+ '&players=' +players+ '"'+
						'style="border: none; display: block; width: 100%; height: 540px; padding: 0px; margin: 0px;"></iframe>');
		
				view.$el.find('.modal-footer').hide();				
			}});
		}
	},
	
	// Reserve the tee time as normal
	payAtCourse: function(){
		if(this.model && this.model.isValid()){
			this.$el.find('button.continue').button('loading');
			App.data.user.get('reservations').create(this.model.attributes, {wait: true});
		}
	}
});

var ReservationView = Backbone.View.extend({
	tagName: 'li',
	className: '',
	template: _.template( $('#template_reservation').html() ),
	
	events: {
		'click .cancel': 'cancelReservation'
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},		
	
	// Cancels the user's reservation
	cancelReservation: function(){
		if(confirm("Are you sure you want to cancel this reservation?")){
			this.$el.find('button').button('loading');
			this.model.destroy({wait: true});
		}
	}
});

var ReservationListView = Backbone.View.extend({
	tagName: 'ul',
	className: 'reservations',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderReservation, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.append('<li class="reservation empty muted">No reservations available</li>');
	},
	
	renderReservation: function(reservation){
		this.$el.append( new ReservationView({model: reservation}).render().el );
	}
});

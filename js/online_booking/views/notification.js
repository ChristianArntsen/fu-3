var NotificationView = Backbone.View.extend({
	el: '#notification',
	
	events: {
		'click': 'close'
	},
	
	initialize: function(){
		var view = this;
		App.vent.on('notify', function(data){
			view.render(data);
		}); 
	},
	
    render: function(data) {
		var message = data.message;
		alert(data.message);
		/*
		if(data.type == 'error'){
			data.type = 'danger';
		}
		var cssClass = 'alert-'+data.type;

		this.$el.html(message);
		this.$el.removeClass('alert-info alert-warning alert-danger alert-success').addClass(cssClass);
		return this; */
    },
	
	close: function(){
		this.$el.hide();
	}
});

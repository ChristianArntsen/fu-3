var TwoColLayout = Backbone.Marionette.Layout.extend({
	template: "#template_two_col",

	regions: {
		header: "#header",
		nav: "#nav",
		content: "#content",
		booking_class: '#booking_class'
	}
});

var OneColLayout = Backbone.Marionette.Layout.extend({
	template: "#template_one_col",

	regions: {
		header: "#header",
		content: "#content"
	}
});

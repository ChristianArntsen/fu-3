var AddCreditCardView = Backbone.View.extend({
	id: 'add-credit-card',
	className: 'modal-dialog',
	template: _.template( $('#template_add_credit_card').html() ),
	
	events: {
		'click button.add-card': 'addCard'
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
		
		// Once a new credit card is added, finish the reservation
		this.listenTo(App.data.user.get('credit_cards'), 'add', this.finishReservation);
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		var html = this.template();
		this.$el.html(html);
		var view = this;
		
		if(App.data.course.get('credit_card_provider') == 'ets'){
			$.post(SITE_URL + 'credit_card_window', null, function(response){
				view.$el.find('div.modal-body').append(response);
			}, 'html');
		
		}else{
			view.$el.find('div.modal-body').css('padding', '0px');
		}			

		$('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	finishReservation: function(creditCardModel){	
		
		var credit_card_id = creditCardModel.get('credit_card_id');
		
		// If adding card trying to book tee time
		if(this.model instanceof Reservation){
			this.model.set('credit_card_id', credit_card_id);
			
			// Complete reservation
			App.data.user.get('reservations').create(this.model.attributes);
		
		// If adding card trying to pay invoice
		}else if(this.model instanceof Invoice){	
			var creditCardView = new InvoiceSelectCreditCardView({model: this.model, selected_card: credit_card_id });
			creditCardView.show();			
		
		// If just adding card to profile
		}else{
			$('#modal').modal('hide');
		}
	},

	// Book teetime using selected credit card
	addCard: function(event){
		
		var model = this.model;
		var form = this.$el.find('#add_credit_card_form');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}	

		// Send credit card details to ETS
		if(App.data.course.get('credit_card_provider') == 'ets'){
			if(model){
				this.$el.find('button.add-card').button('loading');
			}else{
				this.$el.find('button.add-card').data('loading-text', 'Adding card...').button('loading');
			}
			ETSPayment.submitETSPayment();
		}
		return false;
	}
});

<?php
//Adjust teetimes by hand
variables: time_off, teesheet_id, current_date, open_hour, increment

UPDATE foreup_teetime SET 
start = CASE WHEN ((substr(start,9)%100)-time_off) < 0 THEN start - 40 - time_off ELSE start - time_off END,
end = CASE WHEN ((substr(end,9)%100)-time_off) < 0 THEN end - 40 - time_off ELSE end - time_off END
WHERE 
teesheet_id = teesheet_id
AND
start > current_date
AND 
((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment != 0 



//Start time and increment both changed
start_diff = ((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment;
end_diff = ((floor(substr(end-open_hour,9)/100)*60)+substr(end-open_hour,9)%100)%increment;


UPDATE foreup_teetime SET 
start = CASE WHEN ((substr(start,9)%100)-time_off) < 0 THEN start - 40 - time_off ELSE start - time_off END,
end = CASE WHEN ((substr(end,9)%100)-time_off) < 0 THEN end - 40 - time_off ELSE end - time_off END
WHERE 
teesheet_id = teesheet_id
AND
start > current_date
AND 
((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment != 0 


// FINISHED PRODUCT: Start time and increment both changed
start_diff = ((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment;
end_diff = ((floor(substr(end-open_hour,9)/100)*60)+substr(end-open_hour,9)%100)%increment;


UPDATE foreup_teetime SET 
start = CASE WHEN ((substr(start,9)%100)-((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment) < 0 THEN start - 40 - ((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment ELSE start - ((floor(substr(start-open_hour,9)/100)*60)+substr(start-open_hour,9)%100)%increment END,
end = CASE WHEN ((substr(end,9)%100)-((floor(substr(end-open_hour,9)/100)*60)+substr(end-open_hour,9)%100)%increment) < 0 THEN end - 40 - ((floor(substr(end-open_hour,9)/100)*60)+substr(end-open_hour,9)%100)%increment ELSE end - ((floor(substr(end-open_hour,9)/100)*60)+substr(end-open_hour,9)%100)%increment END
WHERE 
teesheet_id = teesheet_id
AND
start > current_date



// ACTUAL USE CASE
UPDATE foreup_teetime SET 
start = CASE WHEN ((substr(start,9)%100)-((floor(substr(start-500,9)/100)*60)+substr(start-500,9)%100)%6) < 0 THEN start - 40 - ((floor(substr(start-500,9)/100)*60)+substr(start-500,9)%100)%6 ELSE start - ((floor(substr(start-500,9)/100)*60)+substr(start-500,9)%100)%6 END,
end = CASE WHEN ((substr(end,9)%100)-((floor(substr(end-500,9)/100)*60)+substr(end-500,9)%100)%6) < 0 THEN end - 40 - ((floor(substr(end-500,9)/100)*60)+substr(end-500,9)%100)%6 ELSE end - ((floor(substr(end-500,9)/100)*60)+substr(end-500,9)%100)%6 END
WHERE 
teesheet_id = 246
AND
start > 201304070000

// NEWEST VERSION... ACTUALLY MAKES GOOD SENSE
UPDATE foreup_teetime SET start = 
CASE 
WHEN (
// Tee Time Minutes
(substr(start,9)%100)-
// Start in minutes minus open hour in minutes ... ie diff
((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))
%increment) < 0 
// Account for 100 (hour) if reducing down
THEN start - 40 - ((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))%increment 
// 100 as hour non issue
ELSE start - ((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))%increment END,

end = 
CASE 
WHEN ((substr(end,9)%100)-((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))%increment) < 0 

THEN end - 40 - ((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))%increment 

ELSE end - ((start%100 + floor(start/100)*60) - (oh%100 + floor(oh/100)*60))%increment END
WHERE 
teesheet_id = teesheet_id
AND
start > current_date

// ACTUAL USE
UPDATE foreup_teetime SET start = CASE WHEN ((substr(start,9)%100)-((start%100 + floor(substr(start,9)/100)*60) - (630%100 + floor(630/100)*60))%9) < 0 THEN start - 40 - ((start%100 + floor(substr(start,9)/100)*60) - (630%100 + floor(630/100)*60))%9 ELSE start - ((start%100 + floor(substr(start,9)/100)*60) - (630%100 + floor(630/100)*60))%9 END, end = CASE WHEN ((substr(end,9)%100)-((end%100 + floor(substr(end,9)/100)*60) - (630%100 + floor(630/100)*60))%9) < 0 
THEN end - 40 - ((end%100 + floor(substr(end,9)/100)*60) - (630%100 + floor(630/100)*60))%9 ELSE end - ((end%100 + floor(substr(end,9)/100)*60) - (630%100 + floor(630/100)*60))%9 END WHERE teesheet_id = 271 AND start > 201304160000


// TEE TIME SALES TOTALS BY YEAR, MONTH, GOLF COURSE
SELECT YEAR(date_booked) AS year, MONTH(date_booked) AS month, foreup_courses.name AS course, count(date_booked) AS tee_time_count, sum(player_count) AS players, sum(amount) AS revenue FROM `foreup_teetimes_bartered` LEFT JOIN foreup_sales_payments_credit_cards ON foreup_teetimes_bartered.invoice_id = foreup_sales_payments_credit_cards.invoice LEFT JOIN foreup_teesheet ON foreup_teesheet.teesheet_id = foreup_teetimes_bartered.teesheet_id LEFT JOIN foreup_courses ON foreup_courses.course_id = foreup_teesheet.course_id GROUP BY year, month, foreup_teetimes_bartered.teesheet_id
ORDER BY year DESC, month DESC, foreup_teetimes_bartered.teesheet_id

// MORE SPECIFIC LIST OF PURCHASED TEE TIMES FROM A SPECIFIC TIME
SELECT c.name, pcc.amount, player_count, tb.holes, carts, date_booked FROM `foreup_teetimes_bartered` AS tb LEFT JOIN foreup_teesheet AS t ON tb.teesheet_id = t.teesheet_id LEFT JOIN foreup_courses AS c ON c. course_id = t.course_id LEFT JOIN foreup_sales_payments_credit_cards AS pcc ON pcc.invoice = tb.invoice_id WHERE date_booked > '2014-07-10 00:00:00'

// SHOWS MULTIPLE BOOKINGS ON A GIVEN DAY
SELECT c.name, SUBSTRING(start, 1, 8) AS day, group_concat(concat(date_booked, ' - $', pcc.amount) separator ', '), count(*) AS count, sum(pcc.amount), player_count, tb.holes, carts, date_booked FROM `foreup_teetimes_bartered` AS tb LEFT JOIN foreup_teesheet AS t ON tb.teesheet_id = t.teesheet_id LEFT JOIN foreup_courses AS c ON c. course_id = t.course_id LEFT JOIN foreup_sales_payments_credit_cards AS pcc ON pcc.invoice = tb.invoice_id WHERE date_booked > '2014-01-01 00:00:00' GROUP BY tb.teesheet_id, day HAVING count > 1 ORDER BY start, day


// LOGGED IN EMPLOYEES REPORT
SELECT sum(IF(last_login > '2013-9-27 00:00:00', 1, 0)) as logged_in_individuals, sum(IF(last_login < '2013-9-27 00:00:00', 1, 0)) as not_logged_individuals, foreup_employees.course_id, name FROM `foreup_employees` LEFT JOIN foreup_courses ON foreup_courses.course_id = foreup_employees.course_id WHERE woeid != '' GROUP BY course_id ORDER BY name

// NUMBER OF SALES PER COURSE
SELECT name, foreup_courses.course_id AS course_id, COUNT( sale_id ) AS sales
FROM foreup_sales
LEFT JOIN foreup_courses ON foreup_courses.course_id = foreup_sales.course_id
WHERE sale_time >  '2013-9-1 00:00:00'
GROUP BY foreup_sales.course_id
ORDER BY name

// FIND SALES ITEMS WITH TAX DISCREPENCIES
SELECT * 
FROM foreup_sales_items 
LEFT JOIN foreup_sales_items_taxes 
	ON foreup_sales_items.sale_id = foreup_sales_items_taxes.sale_id 
	AND foreup_sales_items.item_id = foreup_sales_items_taxes.item_id 
	AND foreup_sales_items.line = foreup_sales_items_taxes.line 
WHERE tax > 0 
	AND percent IS NULL
	
UPDATE  foreup_sales_items
LEFT JOIN foreup_sales_items_taxes 
	ON foreup_sales_items.sale_id = foreup_sales_items_taxes.sale_id 
	AND foreup_sales_items.item_id = foreup_sales_items_taxes.item_id 
	AND foreup_sales_items.line = foreup_sales_items_taxes.line 
SET     tax = 0, total = subtotal
WHERE   percent IS NULL

// MERCURY VS ETS CREDIT CARDS BEING PROCESSED, DIVIDED BY MONTH
SELECT CASE WHEN mercury_id !=  ''
THEN  'Mercury'
ELSE  'ETS'
END AS processor, YEAR( trans_post_time ) AS YEAR, MONTH( trans_post_time ) AS 
MONTH , SUM( auth_amount ) 
FROM  `foreup_sales_payments_credit_cards` 
WHERE  `trans_post_time` >  '2013-01-01 00:00:00'
GROUP BY YEAR, 
MONTH , processor
ORDER BY YEAR DESC , 
MONTH DESC , processor

// LISTS THE TOTALS THAT SHOULD BE BILLED FROM THE GOLF COURSES FOR ANY GIVEN MONTH
SELECT name, SUM( monthly_amount ) , SUM( annual_amount ) , credit_card_id
FROM  `foreup_billing` 
LEFT JOIN foreup_courses ON foreup_courses.course_id = foreup_billing.course_id
WHERE deleted =0
AND (
monthly_amount >0
OR annual_amount >0
)
AND (
(
annual =1
AND annual_month =2
)
OR (
monthly =1
)
)
AND (
(period_start <= period_end AND period_start <= 1 AND period_end >= 1) 
OR 
(period_start > period_end AND (period_start <= 1 OR period_end >= 1))
) 
GROUP BY name
ORDER BY name
